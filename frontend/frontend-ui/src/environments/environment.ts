export const environment = {
    production: window["env"]["production"] || false,
    API_ENDPOINT: window["env"]["API_ENDPOINT"] || "http://10.0.50.65/dmx_dev/api",
    
    client: 'default',
    modules: {
      dataQuality: 'dataQuality',
      brms: 'brms',
      catalog: 'catalog',
      admin: 'admin',
      dataOnboarding: 'dataOnboarding',
      notifications: 'notifications',
      myprofile: 'myprofile'
    },

    users: {
      LIST_USERS: '/exchange?name=get_users_v2&user_id=::user_id&user_name=null&limit=1000',
      add_user: '/exchange/add_auth?user_name=::user_name&password=::password&firstname=::firstname&lastname=::lastname&is_ldap_user=::is_ldap_user&is_local_user=::is_local_user&email=::email&is_admin=::is_admin',
      edit_user: '/exchange?name=edit_user_v2&user_id=::user_id&is_deleted=::is_deleted&username=::username&is_active=::is_active&password=::password&firstname=::firstname&lastname=::lastname&is_ldap_user=::is_ldap_user&is_local_user=::is_local_user&email=::email&is_admin=::is_admin',
      remove_user:'/exchange?name=remove_user_from_usergroup_v2&group_id=::group_id&user_ids=::user_ids&groupname=::groupname'
    },

    groups: {
      LIST_GROUPS: '/exchange?name=get_usergroups_v2&group_id=::group_id&userdetail=::userdetail&accessdetail=::accessdetail&is_deleted=::is_deleted',
      add_group: '/exchange?name=add_usergroup_v2&groupname=::groupname&description=::description',
      edit_usergroup: '/exchange?name=edit_usergroup_v2&description=::description&groupname=::groupname&group_id=::group_id&is_active=::is_active&is_deleted=::is_deleted ',
      add_user_to_usergroup: '/exchange?name=add_user_to_usergroup_v2&groupname=::groupName&group_id=::group_id&user_ids=::user_id_list',
      add_layer_to_usergroup: '/exchange?name=add_layer_to_usergroup_v2&layer_ids=::layer_ids&usergroup_id=::usergroup_id',
      remove_layer_from_usergroup: '/exchange?name=remove_layer_from_usergroup_v2&layer_ids=::layer_ids&usergroup_id=::usergroup_id',
      //add_module_to_usergroup: '/exchange?name=add_module_to_usergroup&usergroup_id=::usergroup_id&module_ids=::module_id',
      remove_module_from_usergroup: '/exchange?name=remove_module_from_usergroup_v2&usergroup_id=::usergroup_id&module_id=::module_id&mapping_id=null',
      list_functionality: '/exchange?name=get_modules_v2&accessdetail=true',
      add_usergroup_module_layer_user_functionality: '/exchange?name=add_usergroup_module_layer_user_functionality_v2&groupname_v=::groupname_v&description_v=::description_v&user_ids_v=::user_ids_v&module_ids_v=::module_ids_v&layer_ids_v=::layer_ids_v&functionality_ids_v=::functionality_ids_v&group_type_c=::group_type_c',
      add_functionality: '/exchange?name=add_functionality_to_usergroup_v2&usergroup_id=::usergroup_id&functionality_ids=::functionality_ids',
      remove_functionality: '/exchange?name=remove_functionality_from_usergroup_v2&usergroup_id=::usergroup_id&functionality_ids=::functionality_ids'

    },
    connectors: {
      connectors : '/exchange?name=list_connections_v2',
      delete_connection : '/exchange?name=delete_connection_v2&connection_id=::connection_id',
      add_s3_connection: '/exchange?name=add_connection_s3_v2&rolename=::rolename&description=::description&c_name=::c_name&secret_key=::secret_key&access_key=::access_key',
      add_snowflake_connection: '/exchange?name=add_connection_snowflake_v2&c_name=::c_name&password=::password&database=::database&description=::description&account_id=::account_id&user=::user&warehouse=::warehouse',
      add_sftp_connection: '/exchange?name=add_connection_sftp_v2&username=::user&c_name=::c_name&password=::password&host=::host&description=::description&port=::port',
      encrypt: '/operations/encrypt',
      update_sftp_connection:'/exchange?name=update_connection_sftp_v2&host=::host&username=::user&description=::description&port=::port&c_name=::c_name&password=::password&connector_id=::connector_id',
      update_s3_connection:'/exchange?name=update_connection_s3_v2&rolename=::rolename&description=::description&c_name=::c_name&access_key=::access_key&secret_key=::secret_key&connector_id=::connector_id',
      update_snowflake_connection:'/exchange?name=update_connection_snowflake_v2&account_id=::account_id&database=::database&description=::description&user=::user&c_name=::c_name&password=::password&connector_id=::connector_id&warehouse=::warehouse',
      test_sftp_connection:'/connector/test_sftp?host=::host&username=::user&port=::port&password=::password',
      test_s3_connection:'/connector/test_s3?iamrole=::rolename&access_key=::access_key&secret_key=::secret_key',
      test_snowflake_connection:'/connector/test_snowflake?account_id=::account_id&database=::database&user=::user&password=::password&warehouse=::warehouse',
      
    },
    login: {
      authenticate: '/exchange/authorize',
      auth: '/exchange/auth',
      get_user_details: '/exchange?name=get_users_v2&user_id=null&user_name=::user_name&limit=null',
    },
    layers: {
      fetch_all_layers: '/exchange?name=get_layers_v2&name_v=null&layer_id=null&type=null&limit=null&is_active=::is_active',
      add_layer: '/exchange?name=add_layer_v2&layer_type=::layer_type&name_v=::layer_name&connector_id=::connector_id&s3_path=::path&description=::description&&schema_name=::schema_name',
      update_layer_info: '/exchange?name=update_layer_v2&description=::description&is_active=::layerStatus&layer_id=::id',
      fetch_s3_path_list: '/operations/get_buckets?s3_connector_id=::connector_id&bucket_name=null',
      // fetch_sf_schema_list: '/broker/snowflake?sf_connector_id=::connector_id&name=get_sf_schemas',
      fetch_sf_schema_list: '/connector/list_snowflake_schemas?sf_connector_id=::sf_connector_id'
      // fetch_sf_schema_list: '/broker/snowflake?sf_connector_id=::connector_id'
    },
    landing: {
      get_notifications: '/exchange?name=get_notifications_v2'
    },
    notifications:{
      send_notifications: '/notification/add_notification',
      get_notifications: '/exchange?name=get_notifications_v2'
    },

    admin: {
      add_access_request:'/exchange?name=add_access_request_v2&functionality_ids=::func_ids&layer_ids=::layer_ids&type=::new_request&description=::justification',
      access_request: '/exchange?name=add_access_request_v2&group_ids=::group_ids&layer_ids=::layer_ids&module_ids=null&functionality_ids=null&type=group',
      get_access_request: '/exchange?name=get_access_request_v2&userid=::userid&requestid=::requestid',
      execute_access_request : '/exchange?name=execute_access_request_v2&request_id=::request_id&action=::action',
      get_connector_layers: '/exchange?name=get_connector_layers_v2&connector_id=::connection_id',
      
    },
    catalog: {
      display_list_view : '/catalog/display_requested_view',
      display_requested_view : '/catalog/display_requested_view',
      profile: '/catalog/redshift_profiler?node_id=::node_id&rs_connector_id=2&cached=false',
      s3_profiler: '/catalog/s3_profiler?s3_connector_id=2&node_id=::node_id',
      requested_view: '/catalog/display_requested_view?node_id=::node_id',
      catalog_search: '/catalog/catalog_object_search?search_string=::search_string&max_results=100',
      // tslint:disable-next-line: max-line-length
      catalog_adv_search: '/catalog/catalog_object_search?search_string=::search_string&layer_type=::layer_type&layer_name=::layer_name&file_size=::file_size&time_stamp=::time_stamp&max_results=100',
      // tslint:disable-next-line: max-line-length
      edit_description : '/catalog/edit_attribute?attribute_key=::attribute_key&value=::value&layer_name=::layer_name&layer_type=::layer_type&object_name=::object_name&column_name=::column_name',
      edit_attribute : '/catalog/edit_attribute?attribute_key=::attribute_key&value=::value&node_id=::node_id',
      add_tag : '/catalog/link_tag?name=::tag&user=::user&node_id=::node_id',
      delete_tag : '/catalog/unlink_tag?name=::tag&user=::user&node_id=::node_id',
      download_request_opt: '/catalog/bulk_dump?layer_name=::layer_name&layer_type::layer_type$time_stamp::time_stamp',
      download_request: '/catalog/export_catalog?layers=::layers&time_stamp=::time_stamp',
      export_catalog_column_level: '/catalog/export_catalog_column_level?layers=::layers&time_stamp=::time_stamp',
      // uploadBulkData: '/catalog/import_catalog?user_id=::user_id&user_name=::user_name',
      uploadBulkData: '/catalog/import_catalog?report_id=::report_id&user_id=::user_id&user_name=::user_name',
      validateBulkData: '/catalog/validate_import_catalog?user_id=::user_id&user_name=::user_name',
      // uploadBulkDataColumn: '/catalog/import_catalog_column_level?user_id=::user_id&user_name=::user_name',
      uploadBulkDataColumn: '/catalog/import_catalog_column_level?report_id=::report_id&user_id=::user_id&user_name=::user_name',
      view_summary: '/catalog/snowflake_profiler?node_id=::node_id&statistics=true&cached=false&timestamp=::time_stamp',
      reviews: '/catalog/view_comment_and_rating?node_id=::node_id',
      get_catalog_search_query: '/catalog/get_catalog_search_query?user_id=::user_id',
      // tslint:disable-next-line: max-line-length
      save_catalog_search_query: '/catalog/save_catalog_search_query?search_text=::search_text&user_id=::user_id&connector_type=::connector_type&layer_name=::layer_name&last_updated_with_in=::last_updated_with_in&compare_operator=::compare_operator&file_size_num=::file_size_num&size_dimension=::size_dimension&name=::name',
      add_review_comments : '/catalog/add_comment_and_rating?node_id=::node_id&rating=::rating&user=::user&comment=::comment',
      list_tags : '/catalog/list_tags',
      snowflake_profiler: '/catalog/snowflake_profiler?node_id=::node_id',
      get_attributes: '/catalog/get_attributes',
      get_attributesDetails: '/catalog/get_attributes?attr=::attr',
      catalog_object_search: '/catalog/search_by_attribute',
      search_glossary: '/catalog/search_glossary',
      list_business_glossary: '/catalog/list_business_glossary',
      object_file_profiler: '/catalog/object_file_profiler?obj_uri_id=::obj_uri_id',
      export_glossary: '/catalog/export_glossary',
      add_business_glossary: '/catalog/add_business_glossary?status=::status&name=::name&category=::category&subcategory=::subcategory&description=::description',
      update_business_glossary: '/catalog/update_business_glossary?node_id=::node_id&status=::status&name=::value&category=::category&subcategory=::subcategory&description=::description',
      import_glossary: '/catalog/import_glossary?uuid=::uuid',
      // bulk_import_report: '/catalog/bulk_import_report?report_id=::report_id&time_stamp=::time_stamp',
      bulk_import_report: '/catalog/import_catalog_report?report_id=::report_id',
      postgres_incremental_scan: '/catalog/postgres_incremental_scan?obj_id=::obj_id',
      validate_glossary: '/catalog/validate_glossary',
      get_glossary_meta: '/catalog/get_glossary_meta',
      list_data_asset: '/catalog/list_data_asset',
      link_data_asset: '/catalog/link_data_asset?file_node=::node_id&data_asset_node=::data_node_id',
      export_nodes: '/catalog/export_nodes?uris=::uriList&col_level=::columnFlag',
      get_data_asset: '/catalog/get_data_asset',
      get_search_suggestions: '/catalog/suggestions?q=::searchText'
    },
    commonUtilities:{
      uploads3File: '/operations/upload?layer_id=::layer_id&&prefix=::folder_prefix&&override=::override',
      getFileProperties: '/operations/file_properties?layer_id=::layer_id&&prefix=::folder_prefix&&delimiter=::delimiter&&column_names=::column_names&&file_name=::file_name&&file_type=::file_type&&data_header=::data_header',
      getPreviewData: '/operations/sample_file?layer_id=::layer_id&&delimiter=::delimiter&&text_qualifier=::text_qualifier&&column_names=::column_names&&prefix=::folder_prefix&&file_name=::file_name&&data_header=::data_header&&file_type=::file_type',
      deletes3File: '/operations/delete?layer_id=::layer_id&&prefix=::folder_prefix&&file_name=::file_name',
      downloads3File:'/operations/download?layer_id=::layer_id&&prefix=::folder_prefix&&file_name=::file_name',
      getTypes: '/exchange?name=::name',
      getDataFeedDetails: '/exchange?name=::name&&obj_id=::obj_id',
      checkFileStatus: '/exchange?name=::name&&obj_nm=::obj_nm&&obj_type=::obj_type&&data_layer_id=::data_layer_id',
      getObjectDetails:'/exchange?name=::name&&obj_id=::obj_id',
      getObjectInstance: '/exchange?name=::name&&obj_id=::obj_id',
      getSchedulingStatus: '/exchange?name=::name&&obj_id=::obj_id',
      saveConnectionDetails: '/exchange?name=::name&&obj_id_value=::obj_id_value&&conn_id_value=::conn_id_value&&'+
      'user_id_value=::user_id_value&&user_type_value=::user_type_value&&folder_prefix=::folder_prefix&&'+
      'created_by_value=::created_by_value&&last_updated_by_value=::last_updated_by_value',
      getFilesForConnection: '/operations/folder_sample?s3_connector_id=::s3_connector_id&&category=::category&&extension=::extension',
      saveObjectDetails: '/exchange?name=::name&&smpl_file_nm=::sample_file_name&&obj_uri_id=::obj_uri_id&&src_data_layer_id=::src_data_layer_id&&parent_obj_id=::parent_obj_id&&file_arvl_strt_dy=::file_arrival_start_day&&'+
      'data_strt_row=::data_strt_row&&src_data_layer=::src_data_layer&&obj_id=::obj_id&&has_header=::has_header&&obj_extn=::obj_extension&&cmpr_mtd=::cmpr_mtd&&freq=::freq'+
      '&&obj_typ=::obj_type&&obj_desc=::obj_desc&&file_arvl_end_dy=::file_arrival_end_day&&audit_insrt_id=::audit_insrt_id&&obj_encoding=::obj_encoding&&'+
      'footer_note=::footer_note&&obj_src_nm=::obj_src_nm&&file_arvl_time=::file_arvl_time&&obj_delimiter=::obj_delimiter&&data_persist=::data_persist&&'+
      'obj_prvdr_nm=::obj_prvdr_nm&&obj_text_qualifier=::obj_text_qualifier&&obj_ptrn_fmt=::obj_ptrn_fmt&&obj_nm=::obj_nm&&'+
      'version=::version&&mstr_obj_id=::mstr_obj_id',
      updateObjectDetails: '/exchange?name=::name&&freq=::freq&&parent_obj_id=::parent_obj_id&&cmpr_mtd=::cmpr_mtd&&smpl_file_nm=::smpl_file_nm&&'+
      'obj_typ=::obj_typ&&obj_ptrn_fmt=::obj_ptrn_fmt&&obj_text_qualifier=::obj_text_qualifier&&footer_note=::footer_note&&'+
      'obj_nm=::obj_nm&&obj_prvdr_nm=::obj_prvdr_nm&&file_arvl_time=::file_arvl_time&&obj_src_nm=::obj_src_nm&&data_strt_row=::data_strt_row&&'+
      'file_arvl_strt_dy=::file_arvl_strt_dy&&old_obj_uri_id=::old_obj_uri_id&&data_persist=::data_persist&&has_header=::has_header&&'+
      'mstr_obj_id=::mstr_obj_id&&obj_encoding=::obj_encoding&&obj_extn=::obj_extn&&new_obj_uri_id=::new_obj_uri_id&&version=::version&&'+
      'file_arvl_end_dy=::file_arvl_end_dy&&obj_desc=::obj_desc&&audit_updt_id=::audit_updt_id&&obj_delimiter=::obj_delimiter&&src_data_layer_id=::src_data_layer_id'
    },
    onboarding:{
      onboardingSampleFileLayerId : 85,
      dataFeed:{
        getDataSet: '/exchange?name=::name&&obj_prvdr_nm=::provider_name',
        getFeedNames: '/exchange?name=::name&&data_set=::data_set&&provider_name=::provider_name',
        getFeedDetails:'/exchange?name=::name&&feed_name=::feed_name',
        addFileValidation:'/exchange?name=::name&&obj_id=::obj_id&&created_by=::created_by'
      },
      provider:{
      getAgreementDetails:'/exchange?name=::name&&object_id=::object_id',
      saveProviderDetails:'/exchange?name=::name&&obj_nm_value=::obj_nm_value&&obj_id_value=::obj_id_value&&'+
      'poc_nm_value=::poc_nm_value&&poc_email_value=::poc_email_value&&poc_ph_value=::poc_ph_value&&'+
      'contract_st_dt_value=::contract_st_dt_value&&contract_end_dt_value=::contract_end_dt_value&&'+
      'data_aggrmnt_loc_value=::data_aggrmnt_loc_value&&additional_info_value=::additional_info_value&&'+
      'created_by_value=::created_by_value&&last_updated_by_value=::last_updated_by_value'
      },
      layout:{
        getAttributeInfo:'/operations/attribute_info?layer_id=::layer_id&&delimiter=::delimiter&&column_names=::column_names&&prefix=::folder_prefix&&file_name=::file_name&&text_qualifier=::text_qualifier&&file_type=::file_type&&data_header=::data_header',
        getTableObjects:'/operations/sf_table_config?layer_id=::layer_id',
        getTableObjectsMatch:'/operations/sf_table_config?layer_id=::layer_id&&match=::match',
        getMasterObjectDetails:'/exchange?name=::name&&master_object_id=::master_object_id',
        saveLayoutDetails:'/exchange?name=::name',
        savePOCDetails:'/exchange?name=::name&&conn_poc_lists=::conn_poc_lists',
        updatePOCDetails:'/exchange?name=::name&&conn_poc_list=::conn_poc_list&&id_list=::id_list',
        getTableObjectsList:'/operations/sf_table_meta_data?layer_id=::layer_id&&table_name=::table_name',
        getDataLayers: '/exchange?name=::name'
      },
      connection:{
        getSourceConnectionDetails:'/exchange?name=::name&&object_id=::object_id'
      },
      storage:{
        getStorageDetails:'/exchange?name=::name&&layer_type=::layer_type',
        getStoragePaths:'/operations/storage_locations?layer_id=::layer_id&&category=::category&&extension=::extension',
        saveStorage: '/exchange?name=::name&&old_obj_uri_id=::old_obj_uri_id&&layer_list=::layer_list',
        updateStorage: '/exchange?name=::name&&conn_list=::conn_list&&id_list=::id_list',
        getControlFile: '/exchange?name=::name'
      }
    },
    ingestion: {
      getFileTypeList: '/exchange?name=::name&&obj_layer_nm=::obj_layer_nm',
      registerDataFile:'/exchange?name=::name&&obj_id_value=::obj_id_value&&file_nm_value=::file_nm_value&&'+
      'ingestion_flag_value=::ingestion_flag_value&&sla_flag_value=::sla_flag_value&&file_notice_out_value=::file_notice_out_value&&'+
      'delay_notice_in_value=::delay_notice_in_value&&user_id_value=::user_id_value&&'+
      'file_status_code_value=::file_status_code_value&&file_source_value=::file_source_value&&'+
      'actual_arrival_time_value=::actual_arrival_time_value',
      createExternalTable:'/operations/dynamic_table?rs_connector_id=::rs_connector_id&&obj_id=::obj_id&&'+
      'create_flg=::create_flg&&schema_name=::schema_name'
    },
    workspace: {
      LIST_WORKSPACE: '/exchange?name=list_workspace',
      LIST_CONNECTOR: '/exchange/?name=get_connection_by_type&connection_type=rs',
      add_workspace: '/exchange?name=add_workspace&workspace_name=::workspace_name&description=::description&connector_id=::connector_id',
      deactivate_workspace: '/exchange?name=delete_workspace&workspace_id=::workspace_id',
      edit_workspace: '/exchange?name=update_workspace&workspace_name=::workspace_name&description=::description&workspace_id=::workspace_id',
    },
    workflow: {
      WORKFLOW_PUBLISH: '/brms/publish_workflow?workflow_id=::id',
      WORKFLOW_DUPLICATE: '/brms/copy_workflow?workflow_id=::id',
      WORKFLOW_DELETE: '/exchange?name=delete_workflow&workflow_id=::id',
      WORKFLOW_CREATE: '/brms/create_workflow?workspace_id=::workspaceId',
      WORKFLOW_LIST: '/exchange?name=list_workflow&workspace_id=::workspaceId',
      METADATA_ADD: '/brms/add_workflow_meta?workflow_id=::id&name=::name&description=::description&selected_datasets=::selected_datasets',
      GET_PILLS: '/exchange?name=list_phrases&category=ETL',
      // LIST_FILE_ADAPTER: '/exchange/?name=list_file_adaptor',
      LIST_FILE_ADAPTER: '/catalog/display_object_list_brms?connector_id=::connector_id',
      ADD_RULE: '/brms/add_rule?user_input=::rules&output=::output&workflow_id=::id&rule_name=::rule_name',
      GET_WORKFLOW: '/brms/get_workflow?workflow_id=::id',
      delete_rule: '/brms/delete_rule?&workflow_id=::workflow_id&rule_id=::rule_id',
      add_parameter_definition: '/brms/add_parameter_definition?parameter_name=::parameter_name&help_text=::help_text&description=::description&config_type=::config_type&config_value=::config_value&workflow_id=::workflow_id',
      edit_rule: '/brms/update_rule?output=::rule_output&rule_name=::rule_name&user_input=::user_input&workflow_id=::workflow_id&rule_id=::rule_id',
      visualize_workflow:'/brms/visualize_workflow?workflow_id=::id',
      scenario: {
        list_scenario: '/exchange?name=list_scenario&workflow_id=::id',
        add_scenario: '/exchange?name=add_scenario&workflow_id=::id&scenario_name=::name&scenario_meta=::meta',
        get_parameter_values: '/brms/get_parameter_values?workflow_id=::id',
        deactivate_scenario: '/exchange?name=deactivate_scenario&scenario_id=::id',
        view_scenario: '/exchange?name=get_scenario&scenario_id=::id',
        run_scenario: '/brms/execute_scenario?scenario_id=::scenario_id&workflow_id=::workflow_id',
      }
    },
    validation: {
      WORKFLOW_PUBLISH: '/brms_validation/publish_validation?validation_id=::id',
      WORKFLOW_DUPLICATE: '/brms_validation/copy_validation?validation_id=::id',
      WORKFLOW_DELETE: '/exchange?name=delete_validation&validation_id=::id',
      WORKFLOW_CREATE: '/brms_validation/create_validation',
      WORKFLOW_LIST: '/exchange?name=list_validation',
      METADATA_ADD: '/brms_validation/add_validation_meta?validation_id=::id&name=::name&description=::description&selected_datasets=::selected_datasets',
      GET_PILLS: '/exchange?name=list_phrases&category=Validation',
      LIST_FILE_ADAPTER: '/exchange/?name=list_file_adaptor',
      ADD_RULE: '/brms_validation/add_rule?user_input=::rules&validation_id=::id&rule_name=::rule_name',
      GET_WORKFLOW: '/brms_validation/get_validation?validation_id=::id',
      delete_rule: '/brms_validation/delete_rule?&workflow_id=::workflow_id&rule_id=::rule_id',
      add_parameter_definition: '/brms_validation/add_parameter_definition?parameter_name=::parameter_name&help_text=::help_text&description=::description&config_type=::config_type&config_value=::config_value&workflow_id=::workflow_id',
      edit_rule: '/brms_validation/update_rule?output=::rule_output&rule_name=::rule_name&user_input=::user_input&validation_id=::workflow_id&rule_id=::rule_id',
      scenario: {
        list_scenario: '/exchange?name=list_scenario_validation&validation_id=::id',
        add_scenario: '/exchange?name=add_scenario_validation&workflow_id=::id&scenario_name=::name&scenario_meta=::meta',
        get_parameter_values: '/brms/get_parameter_values?validation_id=::id',
        deactivate_scenario: '/exchange?name=deactivate_scenario_validation&scenario_id=::id',
        view_scenario: '/exchange?name=get_scenario_validation&scenario_id=::id',
        run_scenario: '/brms/execute_scenario_validation?scenario_id=::scenario_id&validation_id=::workflow_id'
      }
    },
    sourceConnectors: {
      getSourceConnectors: '/exchange?name=::name',
      getSourceConnectorDetails: '/exchange?name=::name&obj_id=::obj_id',
      getSourceConnectorAttributes: '/exchange?name=::name&obj_id=::obj_id'
    },
    dataQuality: {
      getBusinessLayer: '/exchange?name=::name',
      getObjects: '/exchange?name=::name&src_data_layer_id=::src_data_layer_id',
      getAllAttributes: '/exchange?name=::name&obj_id=::obj_id',
      getAttributes: '/exchange?name=::name&obj_id=::obj_id',
      getDQAttributes: '/exchange?name=::name&obj_id=::obj_id&mstr_rule_id=::mstr_rule_id',
      getCheckType: '/exchange?name=::name&rule_type=::rule_type',
      getDQDataValidationCheckType: '/exchange?name=::name',
      getDataValidationCheckType: '/exchange?name=::name&datatype_value=::datatype_value&rule_type=::rule_type&rule_granularity=::rule_granularity',
      getDataValidationRules: '/exchange?name=::name&src_data_layer_id=::src_data_layer_id&obj_id=::obj_id&attr_id_lst=::attr_id_lst&rule_id=::rule_id&latest_rule_id=::latest_rule_id',
      checkDuplicityDataValidationRule: '/exchange?name=::name&obj_id=::obj_id&attr_id=::attr_id&mstr_rule_id=::mstr_rule_id&arg_1=::arg_1',
      addDataValidationRuleV2: '/exchange?name=::name&obj_id=::obj_id&attr_id_lst=::attr_id_lst&mstr_rule_id=::mstr_rule_id&bk_attr_id=::bk_attr_id&layer_id=::layer_id&grnl_lvl=::grnl_lvl&arg_1=::arg_1&arg_2=::arg_2&arg_3=::arg_3&arg_4=::arg_4&rule_desc=::rule_desc&is_rule_actv=::is_rule_actv&rule_svrt=::rule_svrt&fltr_con=::fltr_con&audit_insrt_id=::audit_insrt_id&job_desc=::job_desc&multi_attr_select=::multi_attr_select&multi_attr_rule=::multi_attr_rule',
      addDataValidationRule: '/exchange?name=::name&obj_id=::obj_id&attr_id_lst=::attr_id_lst&mstr_rule_id=::mstr_rule_id&bk_attr_id=::bk_attr_id&layer_id=::layer_id&grnl_lvl=::grnl_lvl&arg_1=::arg_1&arg_2=::arg_2&arg_3=::arg_3&arg_4=::arg_4&rule_desc=::rule_desc&is_rule_actv=::is_rule_actv&rule_svrt=::rule_svrt&fltr_con=::fltr_con&audit_insrt_id=::audit_insrt_id&job_desc=::job_desc',
      updateDataValidationRule: '/exchange?name=::name&rule_id=::rule_id&bk_attr_id=::bk_attr_id&arg_1=::arg_1&arg_2=::arg_2&arg_3=::arg_3&arg_4=::arg_4&rule_desc=::rule_desc&is_rule_actv=::is_rule_actv&rule_svrt=::rule_svrt&fltr_con=::fltr_con&audit_updt_id=::audit_updt_id&multi_attr_select=::multi_attr_select&multi_attr_rule=::multi_attr_rule',
      addThresholdRule: '/exchange?name=::name&mstr_rule_id=::mstr_rule_id&is_rule_actv=::is_rule_actv&audit_insrt_id=::audit_insrt_id&rule_desc=::rule_desc&rule_svrt=::rule_svrt&data_layer_id=::data_layer_id&attr_id=::attr_id&agg_fn=::agg_fn&obj_id=::obj_id&fltr_con=::fltr_con&grnl_lvl=::grnl_lvl&bk_attr_id=::bk_attr_id&job_desc=::job_desc&th_val=::th_val',
      getThresholdRules: '/exchange?name=::name&src_data_layer_id=::src_data_layer_id&obj_id=::obj_id&attr_id=::attr_id&rule_id=::rule_id',
      checkDuplicityThRule: '/exchange?name=::name&mstr_rule_id=::mstr_rule_id&agg_fn=::agg_fn&bk_attr_id=::bk_attr_id&attr_id=::attr_id&obj_id=::obj_id',
      updateThresholdRules: '/exchange?name=::name&audit_updt_id=::audit_updt_id&fltr_con=::fltr_con&rule_svrt=::rule_svrt&th_val=::th_val&rule_desc=::rule_desc&is_rule_actv=::is_rule_actv&rule_id=::rule_id',      
      checkDuplicityRIRule: '/exchange?name=::name&obj_id=::obj_id&attr_id=::attr_id&mstr_rule_id=::mstr_rule_id',
      addRIRule: '/exchange?name=::name&ri_attr_id=::ri_attr_id&bk_attr_id=::bk_attr_id&ri_fltr_con=::ri_fltr_con&rule_svrt=::rule_svrt&is_rule_actv=::is_rule_actv&ri_obj_id=::ri_obj_id&obj_id=::obj_id&fltr_con=::fltr_con&rule_desc=::rule_desc&attr_id=::attr_id&mstr_rule_id=::mstr_rule_id&audit_insrt_id=::audit_insrt_id&job_desc=::job_desc&grnl_lvl=::grnl_lvl&data_layer_id=::data_layer_id',
      getRIRules: '/exchange?name=::name&src_data_layer_id=::src_data_layer_id&obj_id=::obj_id&attr_id=::attr_id&rule_id=::rule_id&latest_rule_id=::latest_rule_id',
      updateRIRules: '/exchange?name=::name&fltr_con=::fltr_con&rule_svrt=::rule_svrt&is_rule_actv=::is_rule_actv&ri_fltr_con=::ri_fltr_con&audit_updt_id=::audit_updt_id&rule_desc=::rule_desc&rule_id=::rule_id&bk_attr_id=::bk_attr_id&ri_obj_id=::ri_obj_id&ri_attr_id=::ri_attr_id',
      addKPIRule: '/exchange?name=::name&grnl_lvl=::grnl_lvl&data_layer_id=::data_layer_id&job_desc=::job_desc&mstr_rule_id=::mstr_rule_id&cstm_qry=::cstm_qry&is_rule_actv=::is_rule_actv&audit_insrt_id=::audit_insrt_id&rule_desc=::rule_desc&obj_id=::obj_id&rule_svrt=::rule_svrt',
      getKPIRules: '/exchange?name=::name&src_data_layer_id=::src_data_layer_id&obj_id=::obj_id&rule_id=::rule_id',
      updateKPIRules: '/exchange?name=::name&rule_id=::rule_id&cstm_qry=::cstm_qry&is_rule_actv=::is_rule_actv&audit_updt_id=::audit_updt_id&rule_desc=::rule_desc&rule_svrt=::rule_svrt',
      checkDuplicityKPI: '/exchange?name=::name&obj_id=::obj_id&mstr_rule_id=::mstr_rule_id',
      addDataControlRule: '/exchange?name=::name&obj_count=::obj_count&src_attr_count=::src_attr_count&job_desc=::job_desc&tgt_attr_id=::tgt_attr_id&grnl_lvl=::grnl_lvl&src_fltr_con=::src_fltr_con&rule_desc=::rule_desc&tgt_obj_id=::tgt_obj_id&src_bk_attr_id=::src_bk_attr_id&src_attr_id=::src_attr_id&tgt_bk_attr_id=::tgt_bk_attr_id&tgt_fltr_con=::tgt_fltr_con&data_layer_id=::data_layer_id&src_obj_id=::src_obj_id&tgt_attr_count=::tgt_attr_count&is_rule_actv=::is_rule_actv&audit_insrt_id=::audit_insrt_id&rule_svrt=::rule_svrt&mstr_rule_id=::mstr_rule_id',
      getSrcDataControlDetails: '/exchange?name=::name&rule_id=::rule_id',
      updateDataControlRule: '/exchange?name=::name&src_obj_id=::src_obj_id&src_attr_id=::src_attr_id&obj_count=::obj_count&audit_updt_id=::audit_updt_id&rule_id=::rule_id&rule_desc=::rule_desc&is_rule_actv=::is_rule_actv&rule_svrt=::rule_svrt&tgt_fltr_con=::tgt_fltr_con&src_fltr_con=::src_fltr_con',
      getDQRulesCount: '/exchange?name=::name',
      getDataControlRules: '/exchange?name=::name',
      getDqObjects: '/exchange?name=::name',
      getObjectRules: '/exchange?name=::name&obj_id=::obj_id' 
    }
  };