import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationComponent } from './components/authentication/authentication.component';
import { QuicklinkStrategy } from 'ngx-quicklink';
import { HomeComponent } from './components/home/home.component';
import { DataResolverService } from './shared/services/content-data-resolver.service';
import { AuthGuard } from './shared/services/auth.guard';
import { environment } from 'src/environments/environment';
import { SsoComponent } from './components/sso/sso.component';

const routes: Routes = [
    { path: '', redirectTo: 'landing', pathMatch: 'full' },
    { path: 'authentication', component: AuthenticationComponent },
    { path: 'sso', component: SsoComponent },
    //{ path: 'notifications', component: HomeComponent, canActivate: [AuthGuard]},
    { path: 'landing', component: HomeComponent, canActivate: [AuthGuard], 
    children: [
      { path: '', /* canActivateChild: [RoleGuard], */
      loadChildren: () => import('./components/container-section/main-canvas-section/main-canvas-section.module').then(m => m.MainCanvasSectionModule), 
      data: { preload: true } , resolve:[DataResolverService], runGuardsAndResolvers: 'always'
      },
      { path: 'myprofile', data: { module: environment.modules.myprofile }, resolve:[DataResolverService],
        loadChildren: () => import('./components/container-section/main-canvas-section/myprofile/myprofile.module').then(m => m.MyprofileModule) 
      }
    ] },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard], 
    children: [
      { path: 'features', /* canActivateChild: [RoleGuard], */
      loadChildren: () => import('./components/container-section/main-canvas-section/main-canvas-section.module').then(m => m.MainCanvasSectionModule), 
      data: { preload: true } , resolve:[DataResolverService], runGuardsAndResolvers: 'always'
      }
    ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload',
    preloadingStrategy: QuicklinkStrategy
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
