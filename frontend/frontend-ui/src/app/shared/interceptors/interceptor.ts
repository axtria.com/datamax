import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Observable } from 'rxjs/internal/Observable';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { LoaderService } from '../../components/loader/loader.service';
import { StorageService } from '../services/storage.service';

export const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class Interceptor implements HttpInterceptor {
    private totalRequests = 0;
    constructor(private ts: ToastPopupService,public loaderService: LoaderService,
        private storageService: StorageService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = this.storageService.getLocalItem('token');
        this.totalRequests++;
        this.loaderService._spinnerShow.emit();
        const _withoutAlert = request.headers.has(InterceptorSkipHeader);
        if (request.headers.has(InterceptorSkipHeader)) {
            const headers = request.headers.delete(InterceptorSkipHeader);
            request = request.clone({ headers });
        }

        request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        request = request.clone({ headers: request.headers.set('Accept', '*/*') });

        return next.handle(request).pipe(
            map(res => {
                if(res instanceof HttpResponse)
                    this.decreaseRequests();
                return res;
            }),
            catchError((error: HttpErrorResponse) => {
                let errorMessage = '';
                // errorMessage = error.error.message + error.error.traceback;
                if (error.error.message) {
                  // client-side error
                  errorMessage = error.error.message;//`Error: ${error.error.message}`;
                } else {
                  // server-side error
                  errorMessage = error.statusText;//`Error Code: ${error.status}\nMessage: ${error.error.traceback}`;
                }
                //window.alert(errorMessage);
                this.decreaseRequests();
                !_withoutAlert && this.ts._exception.emit({ content: '<img src="assets/images/novartis/error.svg"  class="icon-css">&nbsp;&nbsp;&nbsp;' + errorMessage, cssClass: 'e-control e-danger float-left', icon: ''});
                return throwError(errorMessage);
              })
              );
    }

    private decreaseRequests(){
        this.totalRequests--;
        if(this.totalRequests == 0) {
            this.loaderService._spinnerHide.emit();
        }
    }
    
}