import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GridModule, PageService, ToolbarService, SearchService, SortService } from '@syncfusion/ej2-angular-grids';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { FormsModule } from '@angular/forms';
import { SwitchModule, CheckBoxModule, RadioButtonModule } from '@syncfusion/ej2-angular-buttons';
import { FormatStringPipe } from '../pipes/format-string.pipe';

import { ConnectorService } from '../../components/admin/connectors/connectors.service';
import { DropDownListModule, ComboBoxModule, MultiSelectModule, ListBoxModule  } from '@syncfusion/ej2-angular-dropdowns';
import { TabAllModule, TreeViewModule, AccordionModule } from '@syncfusion/ej2-angular-navigations';
import { AngularSvgIconModule } from 'angular-svg-icon';
//import { GroupDetailComponent } from 'src/app/components/admin/usermanagement/groups/group-detail/group-detail.component';
import { UtilService } from '../services/util.service';
import { DirectivesModule } from '../directives/directives.module';
import { PaginationComponent } from 'src/app/components/pagination/pagination.component';
//import { UserDetailsComponent } from 'src/app/components/admin/usermanagement/users/user-details/user-details.component';
// import { from } from 'rxjs';



@NgModule({
  imports: [
    CommonModule,
    GridModule,
    DialogModule,
    FormsModule,
    TreeViewModule,
    DirectivesModule,
    DropDownListModule
  ],
  declarations: [PaginationComponent, FormatStringPipe],
  exports: [
    CommonModule,
    GridModule,
    DialogModule,
    FormsModule,
    SwitchModule,
    PaginationComponent,
    FormatStringPipe,
    DropDownListModule,
    TabAllModule,
    AngularSvgIconModule,
    CheckBoxModule,
    TreeViewModule,
    ComboBoxModule,
    MultiSelectModule,
    DirectivesModule,
    RadioButtonModule,
    ListBoxModule,
    AccordionModule
  ],
  providers: [PageService, SearchService, ToolbarService, SortService, ConnectorService,
    UtilService]
})
export class SyncfusionModule { }
