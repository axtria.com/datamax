import { Injectable } from '@angular/core';

import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthenticateService } from './authenticate.service';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private router: Router, private authenticateService: AuthenticateService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (route.data && route.data.module && this.authenticateService.isModuleVisible(route.data.module)) {
      // module visible in so return true
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot): boolean {
    return this.canActivate(route);
  }

  canLoad(route: ActivatedRouteSnapshot): boolean {
    return this.canActivate(route);
  }
}