/*
  This service is created to be
  used as a common data service
  for all network calls.
*/


import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType, HttpEvent, HttpBackend } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { InterceptorSkipHeader } from '../interceptors/interceptor';
import { saveAs } from "file-saver";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  url: string;
  httpOptions = {
    headers: new HttpHeaders().set(InterceptorSkipHeader, '')
  };

  constructor(private http: HttpClient, private handler: HttpBackend) {
    this.url = environment.API_ENDPOINT; // '/api';
  }
  updateQueryString(relativeParam, data) : string {
    var keys = Object.keys(data);
    var updatedUrl = relativeParam;
    for(var i=0; i<keys.length; i++){
        var key = keys[i];
        updatedUrl = updatedUrl.replace("::" + key, encodeURIComponent(data[key]));
    }
    return updatedUrl;
  }

  getData(relativeParam, data): Observable<any> {
    let updatedurl = this.updateQueryString(relativeParam, data);
    return this.http.get<any>(this.url + updatedurl );
  }

  getDataWithoutLoader(relativeParam, data): Observable<any> {
    let updatedurl = this.updateQueryString(relativeParam, data);
    const newHttpClient = new HttpClient(this.handler);
    return newHttpClient.get<any>(this.url + updatedurl );
  }

  getDataWithoutAlert(relativeParam, data): Observable<any> {
    let updatedurl = this.updateQueryString(relativeParam, data);
    return this.http.get<any>(this.url + updatedurl , this.httpOptions);
  }

  postData(relativeParam, data, request) {
    var updatedurl = this.updateQueryString(relativeParam, data);
    return this.http.post(this.url + updatedurl, request);
  }

  postDataWithProgress(relativeParam, data, request) {
    var updatedurl = this.updateQueryString(relativeParam, data);
    return this.http.post(this.url + updatedurl, request, {reportProgress: true, observe: 'events'}).pipe(
      map(event => this.getEventMessage(event))
    );
  }

  public downloadReport(relativeParam, paramdata , filename) {
    var updatedurl = this.updateQueryString(relativeParam, paramdata);
    let url = this.url + updatedurl;
  
    this.http.get(url, {
      responseType: "blob",
      //headers: new HttpHeaders().append("Content-Type", type)
    }).subscribe(data => {
      var blob = new Blob([data], { type: data.type });
      saveAs(blob, filename);
    });
  }


  private getEventMessage(event: HttpEvent<any>) {
    switch (event.type) {
      case HttpEventType.Sent:
        return {percentageComplete: 0};
  
      case HttpEventType.UploadProgress:
        const percentDone = Math.round(100 * event['loaded'] / event['total']);
        return {percentageComplete: percentDone};
  
      case HttpEventType.Response:
        return {complete : true, body: event['body']};
  
      default:
        return {percentageComplete: 100};
    }
  }
}
