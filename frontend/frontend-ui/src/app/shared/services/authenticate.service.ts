import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import { RestService } from './rest.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})

export class AuthenticateService {

  jwtHelper = new JwtHelperService();
  decodedToken: any;
  userInfo: any;
  layers; functionalityArray: Array<string>; groupsList;
  layerNames: any[];

  constructor(private restService: RestService, private router: Router,
    private storageService: StorageService) {}

  login(model: any) {
    const params = new HttpParams({
      fromObject: model
    });

    this.restService.postData(environment.login.authenticate, {}, params)
    .subscribe((response: any) => {
      const user = response;
      if (user) {
        this.storageService.setLocalItem('token', user.token);
        //this.decodedToken = this.jwtHelper.decodeToken(user.token);
        return this.fetchUserDetails(model.module_name);
      }
    });
      /* .pipe(
        map((response: any) => {
          const user = response;
          if (user) {
            this.storageService.setLocalItem('token', user.token);
            //this.decodedToken = this.jwtHelper.decodeToken(user.token);
          } 
        }),
        concatMap(() => this.restService.getData(environment.login.get_user_details, {
          user_name : model.module_name
        }))
      )
      .subscribe(res => {
        this.userInfo = res.Contents[0];
        this.storageService.setLocalItem('userinfo', JSON.stringify(res.Contents[0]));
        this.router.navigate(['/']);
      }); */

  }

  ssologin() {

    if(!this.storageService.getLocalItem('token')){
      window.open(environment.API_ENDPOINT + environment.login.auth, "_self");
    } else {
      this.decodedToken = this.jwtHelper.decodeToken(localStorage.getItem('token'));
      return this.fetchUserDetails(this.decodedToken['user']);
    }


  }

  fetchUserDetails(username) {
    return this.restService.getData(environment.login.get_user_details, {
      user_name : username
    }).subscribe(res => {
      this.userInfo = res.Contents[0];
      this.storageService.setLocalItem('userinfo', JSON.stringify(res.Contents[0]));
      this.router.navigate(['/']);
    })
  }

  validToken() {
    const token = this.storageService.getLocalItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  getUserInfo() {
    if(!this.userInfo)
      this.userInfo = JSON.parse(this.storageService.getLocalItem('userinfo'));
    return this.userInfo;
  }

  isModuleVisible(_moduleName) {
    let isVisible = false;
    if(!this.functionalityArray) {
      this.functionalityArray = [];
      let user_group = (this.getUserInfo() && this.getUserInfo().usergroupdetail) || [];
      user_group.forEach((item) => {
        
        item.modules && item.modules.forEach((_item) => {
            _item.module_name && this.functionalityArray.push(_item.module_name.toLowerCase());
            
            _item.functionality && _item.functionality.forEach( ele => {
              ele.name && this.functionalityArray.push(ele.name.toLowerCase());
            } );
            
          });


      });
    }
    isVisible = _moduleName && this.functionalityArray.includes(_moduleName.toLowerCase());
      
    return isVisible;
  }

  getLayers() {
    if(!this.layers) {
      let set = [];
      let user_group = (this.getUserInfo() && this.getUserInfo().usergroupdetail) || [];
      user_group.forEach(item => {
        item.layers && item.layers.forEach( ele => {
          set.push({'layer_id': ele.layer_id, 'is_active': ele.is_active});
        } );
      });
      this.layers = set;
    }
    return this.layers.filter((thing, index) => {
      const _thing = JSON.stringify(thing);
      return index === this.layers.findIndex(obj => {
        return JSON.stringify(obj) === _thing;
      });
    });
  }

  getLayerNames() {
    if(!this.layerNames) {
      let set = [];
      let user_group = (this.getUserInfo() && this.getUserInfo().usergroupdetail) || [];
      user_group.forEach(item => {
        item.layers && item.layers.forEach( ele => {
          set.push(ele.name);
        } );
      });
      this.layerNames = set;
    }
    return this.layerNames.filter((a, b) => this.layerNames.indexOf(a) === b);
  }

}