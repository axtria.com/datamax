import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  /**
   * to group an array of objects by key
  */
  getArrayGroupByKey(array, prop) {

    return array.reduce(function(acc, obj) {
      (acc[obj[prop]] = acc[obj[prop]] || []).push(obj);
      return acc;
    }, {});

  }

  concatCheckedValues(array, prop) {

    let selectedArray = '';
    array.forEach(arr => {
      if (arr.checked) 
        selectedArray += arr[prop] + ',';
    });

    return selectedArray.substr(0, selectedArray.length - 1);
  }

  pushToUniqueArray(array, masterArray, prop, selectedVal) {
    let foundItem = masterArray.find(o => o[prop] == selectedVal);
    if(array.indexOf(foundItem) === -1) {
      array.push(foundItem)
    }
    return array;
  }

  removeArrayPropItem(arr, prop, item) {
    return arr.filter(value => value[prop] !== item[prop]);
  }

  removeArrayItem(arr, item) {
    return arr.filter(value => value !== item);
  }

  // items is an array of values that should not be in the values corresponding to prop keys of the arr
  removeArrayPropArrayItem(arr, prop, item){
    let metrics = arr.filter((value: any) => {
      return item.some((f: any) => {
        return f === value[prop];
      });
    });
    return arr.filter((x: any) => !metrics.includes(x));
    }

}
