import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot/* , RouterStateSnapshot */ } from '@angular/router';
import { AuthenticateService } from './authenticate.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authenticateService: AuthenticateService) { }

    canActivate(route: ActivatedRouteSnapshot/* , state: RouterStateSnapshot */) {
      //console.log(route.data);
      let user_info = this.authenticateService.getUserInfo();
        if (user_info && user_info.user_id) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/authentication', route.data]/* , { queryParams: { returnUrl: state.url }} */);
        return false;
    }
}