import { ContentService } from './content.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, mergeMap } from 'rxjs/operators';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable, EMPTY, of } from 'rxjs';
import { GlobalConstants } from '../common/global-constants';

@Injectable({ 
    providedIn: 'root',
 }) 
export class DataResolverService {

  constructor(
    private _contentService: ContentService,
    private httpClient: HttpClient
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Observable<never> {
    let client = this._contentService.getClient();
    let module = route.children && route.children.length > 0 && route.children[0].data && route.children[0].data['module'];

    if(module){

      let _moduleContent = this._contentService.getContent(module);
      if(_moduleContent) {
          GlobalConstants.siteTitle = _moduleContent['title'];
          return of(_moduleContent);
      } else {
          return this.httpClient.get(`./assets/resources/content.${module}.${client}.json`).pipe(catchError(() => {
            //throwError(new Error('Error reading file'));
            this._contentService.setContent(module, {});
            return EMPTY
        }), mergeMap(result => {
              if (result) {
                  GlobalConstants.siteTitle = result['title'];
                  this._contentService.setContent(module, result);
                  return of(result)
              } else {
                  this._contentService.setContent(module, {});
                  return EMPTY;
              }
            })
          );
      }


    }
    
  }
}