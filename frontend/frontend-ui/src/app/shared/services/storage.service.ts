import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
//import { environment } from 'src/environments/environment';
var CryptoJS = require("crypto-js");

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  cipher = new Date().toLocaleDateString();

  constructor() { }

  hash(key) {
    key = environment.production ? CryptoJS.SHA256(key, this.cipher) : key;
    return key.toString();
  };
  encrypt(data) {
    data = environment.production ? CryptoJS.AES.encrypt(data.toString(), this.cipher).toString() : data.toString();
    return data;
  };
  decrypt(data) {
    try {
      data = environment.production ? CryptoJS.AES.decrypt(data, this.cipher).toString(CryptoJS.enc.Utf8) : data;
      return data;
    } catch(e) {
      sessionStorage.clear();
      localStorage.clear();
      return null;
    }
    
  }

  setSessionItem(key: string, value: any) {
    sessionStorage.setItem(this.hash(key), this.encrypt(value));
  }

  removeSessionItem(key: string) {
    sessionStorage.removeItem(this.hash(key));
  }

  getSessionItem(key: string) {
    let _item = sessionStorage.getItem(this.hash(key));
    return _item ? this.decrypt(_item) : null;
  }

  setLocalItem(key: string, value: any) {
    localStorage.setItem(this.hash(key), this.encrypt(value));
  }

  removeLocalItem(key: string) {
    localStorage.removeItem(this.hash(key));
  }

  getLocalItem(key: string) {
    let _item = localStorage.getItem(this.hash(key));
    return _item ? this.decrypt(_item) : null;
  }

}
