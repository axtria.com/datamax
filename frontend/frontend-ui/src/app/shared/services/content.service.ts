import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';

@Injectable()
export class ContentService {
  private _content: any;
  private _client: string;

  constructor(private httpClient: HttpClient) { 
    
  }

  getClient() {  
    return this._client;  
  }

  setContent(key, value) {      
    this._content[key] = value; 
  }
  
  getContent(key: any) {  
    return this._content && this._content[key] || '';  
  }
  
  getFunctionalityContent(module, key: any) {  
    return this._content && this._content[module] && this._content[module][key] || {};  
  }

  load(client) : Promise<any> {
      this._client = client;
        return this.getContentJSON(client).pipe(map(result => {
          this._content = result;
        }), catchError(() => {
          sessionStorage.clear();
          localStorage.clear();
          return throwError(new Error('Error reading file'))
        })
        )
        .toPromise();
  }

  getContentJSON(client) {
    return this.httpClient.get(`./assets/resources/content.${client}.json`);
  }

}
