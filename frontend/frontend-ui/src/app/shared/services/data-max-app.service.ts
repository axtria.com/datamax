/*
  This service is created
  to keep data in sync and
  giving ability to components
  to interact with each other.
*/

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {  
  detailed_search_params: any;
  public Table_Name : string;
  public Schema_Name : string;
  public completeDataFeed: any;
  public layerType: any;
  public ColumnInfo: any;
  public resultsFromRequestedView: any;
  public listOfTags: any[];
  public dataAssetRowData: any[];
  showSearchScreen: any;
  searchResults: any;
  searchString: any;
  listOfDataAssets: any;
  tabKeyname: any;
  //resultsFromGetUsers: any;

  constructor() { }

  /* setResultsFromGetUsers(resultsFromGetUsers) {
    this.resultsFromGetUsers = resultsFromGetUsers;
  } */
  setListOfDataAssets(listOfDataAssets) {
    this.listOfDataAssets = listOfDataAssets;
  }

  setListOfTags(listOfTags) {
    this.listOfTags = listOfTags;
  }
  setResultsFromRequestedView(resultsFromRequestedView) {
    this.resultsFromRequestedView = resultsFromRequestedView;
  }
  setDetailed_search_params(data) {
    this.detailed_search_params = data;
  }
  setDetails_Params(data){
    this.Table_Name = data.Table_Name;
    this.Schema_Name = data.Schema_Name;
    this.completeDataFeed = data.completeDataFeed;
    this.layerType = data.Layer_Type;
    this.ColumnInfo = data.ColumnInfo;
  }
  setShowSearchScreen(showSearchScreen) {
    this.showSearchScreen = showSearchScreen;
  }
  setSearchResults(searchResults) {
    this.searchResults = searchResults;
  }
  setSearchString(searchString) {
    this.searchString = searchString;
  }
  setDataAssetRowData (dataAssetRowData) {
    this.dataAssetRowData = dataAssetRowData;
  }
  setTabKeyname (tabKeyname) {
    this.tabKeyname = tabKeyname;
  }
}
