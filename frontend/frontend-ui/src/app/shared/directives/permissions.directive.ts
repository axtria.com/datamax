import { Directive, Input, ElementRef } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';

@Directive({
  selector: '[appPermissions]'
})
export class PermissionsDirective {

  @Input() functionality: string;
  @Input() action: string;

  constructor(private el: ElementRef, private authenticateService: AuthenticateService) {}

   ngOnInit() {
    if(!this.authenticateService.isModuleVisible(this.functionality)) {
      if(this.action == 'R')
        this.el.nativeElement.remove();
      else if (this.action == 'Request') {
        this.el.nativeElement.classList.add('disabled');
        this.el.nativeElement.insertAdjacentHTML('afterbegin', 
          '<span class="badge badge-secondary float-right mb-2" style="font-size: 9px; padding: 4px; border-radius: 0px; background-color: #FFE6B1; color: black; font-weight: bold; margin-right: -10px;">Request</span>');
        this.el.nativeElement.disabled = true;
     }  else {
        this.el.nativeElement.classList.add('disabled');
        this.el.nativeElement.disabled = true;
     }
    }
      
  }

}
