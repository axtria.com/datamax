import { NgModule } from '@angular/core';
import { PermissionsDirective } from './permissions.directive';
import { AuthGuard } from '../services/auth.guard';
import { RoleGuard } from '../services/role.guard';
import { OrderByAlphabeticallyPipe } from '../pipes/order-by-alphabetically.pipe';
import { FilterPipe } from '../pipes/filter.pipe';
import { ArrayFilterPipe } from '../pipes/array-filter.pipe';
import { MultiAttributeArrayFilterPipe } from '../pipes/multi-attribute-array-filter.pipe';

@NgModule({
  declarations: [
    PermissionsDirective,
    OrderByAlphabeticallyPipe,
    FilterPipe,
    ArrayFilterPipe,
    MultiAttributeArrayFilterPipe
  ],
  imports: [],
  exports: [
    PermissionsDirective,
    OrderByAlphabeticallyPipe,
    FilterPipe,
    ArrayFilterPipe,
    MultiAttributeArrayFilterPipe
  ],
  providers: [
    AuthGuard, RoleGuard
  ]
})
export class DirectivesModule { }