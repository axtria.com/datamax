import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'multiAttributeArrayFilter'
})
export class MultiAttributeArrayFilterPipe implements PipeTransform {

    transform(items: any = [], searchArray: any = [], columns: any = [], filterMetadata): any[] {
      //console.log(items, searchArray, columns, filterMetadata);
      let filteredItem: any = [];
      if (!items){
        filterMetadata.count = 0;
        return [];
      }
      if (searchArray.length < 1){
        filterMetadata.count = items.length;
        return items;
      }
  
      if (searchArray.length > 0) {
        
        searchArray.forEach(function (value: any) {
          filteredItem = filteredItem.concat(items.filter(item => columns.some(k => item[k] !== null && item[k].toString() && item[k].toString().toLowerCase().includes(value.toString().toLowerCase())) ));
        });
        filterMetadata.count = filteredItem.length;
        return filteredItem;
      } else 
      filterMetadata.count = items.length;
      return items;
  
  
    }
  
  }
  
