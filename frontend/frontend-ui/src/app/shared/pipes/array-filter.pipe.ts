import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayFilter'
})
export class ArrayFilterPipe implements PipeTransform {

  transform(items: any = [], searchArray: any = [], columns: any = [], filterMetadata): any[] {
    let filteredItem: any = [];
    if (!items){
      filterMetadata.count = 0;
      return [];
    }
    if (searchArray.length < 1){
      filterMetadata.count = items.length;
      return items;
    }

    if (searchArray.length > 0) {
      searchArray.forEach(function (value: any) {
        filteredItem = filteredItem.concat(items.filter(item =>
          columns.some(k => item[k] !== null && item[k].toString() && item[k].toString().toLowerCase() === value.toString().toLowerCase()
          )));
      });
      filterMetadata.count = filteredItem.length;
      return filteredItem;
    } else 
    filterMetadata.count = items.length;
    return items;


  }

}
