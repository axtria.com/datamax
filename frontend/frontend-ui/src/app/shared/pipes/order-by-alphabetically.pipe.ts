import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderByAlphabetically'
})
export class OrderByAlphabeticallyPipe implements PipeTransform {

  transform(value: any[], propertyName: any, reverse?: boolean): any {
    if (propertyName && value) {
      return value.sort((a: any, b: any) => (a[propertyName]- b[propertyName]) * (reverse ? -1 : 1));
    } else {
      return null;
    }
  }

}
