import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})

export class FilterPipe implements PipeTransform {

    transform(items: any = [], searchText: string, columns: any = []): any[] {
        if (!items) return [];

        if (!searchText) return items;

        return items.filter(item =>
            columns.some(k => item[k] !== null && item[k].toString() && item[k].toString().toLowerCase().includes(searchText.toString().toLowerCase())
            ));

    }

}