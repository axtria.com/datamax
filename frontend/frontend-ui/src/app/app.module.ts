import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { CollapseModule } from 'ngx-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterSectionComponent } from './components/footer-section/footer-section.component';
import { SideBarMenuComponent } from './components/container-section/side-bar-menu/side-bar-menu.component';
import { LandingViewComponent } from './components/landing-view/landing-view.component';
import { HeaderViewComponent } from './components/header-view/header-view.component';
import { FooterViewComponent } from './components/footer-view/footer-view.component';
import { AuthenticationComponent } from './components/authentication/authentication.component';
import { HomeComponent } from './components/home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SidebarViewComponent } from './components//container-section/sidebar-view/sidebar-view.component';
import { Interceptor } from './shared/interceptors/interceptor';
import { ContentService } from './shared/services/content.service';

export function initConfig(config: ContentService) {
  return () => config.load(environment.client);
}

import { ToastPopupComponent } from './components/toast-popup/toast-popup.component';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';
import { TooltipModule } from '@syncfusion/ej2-angular-popups';
import { QuicklinkModule } from 'ngx-quicklink';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderService } from './components/loader/loader.service';
import { DirectivesModule } from './shared/directives/directives.module';
import { RequestAccessComponent } from './components/request-access/request-access.component';
import { CheckBoxModule, RadioButtonModule  } from '@syncfusion/ej2-angular-buttons';
import { AccordionModule } from '@syncfusion/ej2-angular-navigations';
import { ListBoxModule, ListBoxComponent, CheckBoxSelection, CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns';
import { SsoComponent } from './components/sso/sso.component';
import { environment } from 'src/environments/environment';

ListBoxComponent.Inject(CheckBoxSelection);

@NgModule({
  declarations: [
    AppComponent,
    FooterSectionComponent,
    SideBarMenuComponent,
    ToastPopupComponent,
    AuthenticationComponent,
    HomeComponent,
    LoaderComponent,
    LandingViewComponent,
    HeaderViewComponent,
    FooterViewComponent,
    SidebarViewComponent,
    RequestAccessComponent,
    SsoComponent
  ],
  imports: [
    TooltipModule,
    ToastModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    //CollapseModule.forRoot(),
    HttpClientModule,
    FormsModule,
    AngularSvgIconModule.forRoot(),
    QuicklinkModule,
    DirectivesModule,
    CheckBoxModule,
    RadioButtonModule,
    ListBoxModule,
    AccordionModule
  ],
  providers: [ContentService, LoaderService, CheckBoxSelectionService,
    {
      provide: APP_INITIALIZER,
      useFactory: initConfig,
      multi: true,
      deps: [ContentService]
    },
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
