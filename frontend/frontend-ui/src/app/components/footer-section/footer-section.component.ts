import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-footer-section',
  templateUrl: './footer-section.component.html',
  styleUrls: ['./footer-section.component.css']
})
export class FooterSectionComponent implements OnInit {
  footerContent: any;version: string;

  constructor(cs: ContentService, private httpClient: HttpClient) { 
    this.footerContent = cs.getContent("footer");
    this.httpClient.get(`./assets/resources/version.json`).subscribe(data => {
      this.version = data['BUILD_NUMBER'];
    });
  }

  ngOnInit() {
  }

}
