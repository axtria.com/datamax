import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { ToastPopupService } from './toast-popup.service';

@Component({
  selector: 'app-toast-popup',
  templateUrl: './toast-popup.component.html',
  styleUrls: ['./toast-popup.component.css']
})
export class ToastPopupComponent implements OnInit {
  @ViewChild('toasttype',{static:false}) public toastObj: ToastComponent;
  public position: object = { X: 'Center', Y: 'Top' };
  tsubsVar: any;

  constructor(ts: ToastPopupService) {
    this.tsubsVar = ts._exception.subscribe(
      (data: any) => {
        this.show(data);
      });
  }

  ngOnInit() {
  }

  show(message) {
    this.toastObj.show(message);
  }

  ngOnDestroy() {
    if (this.tsubsVar) {
      this.tsubsVar.unsubscribe()
    }
  }

  onBeforeOpen(e) {
    e.cancel = (1 === this.toastObj.element.childElementCount);
  }

}
