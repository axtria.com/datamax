import { Injectable, EventEmitter } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
// import { ContentService } from 'src/app/shared/services/content.service';

@Injectable({
  providedIn: 'root'
})
export class ToastPopupService {
  _exception = new EventEmitter<any>();

  constructor(private cs: ContentService) { }

  getToastSuccessMessage(message, dataarray?) {
    dataarray && dataarray.forEach((data,index) => {
      message = message.replace('{' + index + '}', data);
      
    });
    return {  content: '<img src="assets/images/novartis/success.svg"  class="icon-css">&nbsp;&nbsp;&nbsp;' + this.cs.getContent('toastSuccessHeader') + message, cssClass: 'e-toast-success', icon: '' };
  }

  getToastWarningMessage(message) {
    return {  content: this.cs.getContent('toastWarningHeader') + message, cssClass: 'e-toast-warning', icon: '' };
  }
}
