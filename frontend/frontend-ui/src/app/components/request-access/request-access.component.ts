import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { forkJoin } from 'rxjs';
import { GroupsService } from '../admin/groups/groups.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { AccordionComponent } from '@syncfusion/ej2-angular-navigations';
import { ToastPopupService } from '../toast-popup/toast-popup.service';
import { ListBoxComponent } from '@syncfusion/ej2-angular-dropdowns';

@Component({
  selector: 'app-request-access',
  templateUrl: './request-access.component.html',
  styleUrls: ['./request-access.component.css']
})
export class RequestAccessComponent implements OnInit {
  public layersDropDownList: any = [];
  public functionality_list: any = [];
 // public field: Object = {};
  //public fetchModuleName: any;
  public justification: any;
  public email: any;
  public showCheckBox: boolean = true;
  public selectedFunctionality= {};
  public selectedLayers: string;
 // public selectedModules: string;
  public layer: any;
  public value: any;
  public type: string;
  public requestType = [{
    type: 'new',
    label: 'New Access'
  }, {
    type: 'update',
    label: 'Update Access'
  }];
  public selection = {
    showCheckbox: true,
    showSelectAll: true
  };
  @ViewChild('layerslist', { static: false }) public layerslist: ListBoxComponent;
  @ViewChild('closebutton', { static: false }) closebutton;
  @ViewChild ('accordion',{static:false}) accordion: AccordionComponent;
  expandPermission = false;


  public userInfo = this.authenticateService.getUserInfo();
  //@ViewChild('treeelement', { static: false }) public tree: TreeViewComponent;

  constructor(public authenticateService: AuthenticateService, private rs: RestService,
    private groupService: GroupsService, private utilService: UtilService, private ts: ToastPopupService) { }

  ngOnInit() {
    let fetch_all_layers = this.rs.getData(environment.layers.fetch_all_layers, {
      is_active: true
    });
    let get_modules = this.rs.getData(environment.groups.list_functionality, {});
    forkJoin([fetch_all_layers, get_modules]).subscribe(data => {
      this.layersDropDownList = data[0].Contents || [];
      this.functionality_list = data[1].Contents || [];
      this.functionality_list = this.groupService.getFunctionalityTreeJSON(this.functionality_list, false, false);
      this.accordion && this.accordion.refresh();
      //this.field = { dataSource: this.functionality_list, id: 'nodeId', text: 'nodeText', child: 'roles' };
    });

  }

  public submitAccessRequest() {
    this.selectedLayers = this.utilService.concatCheckedValues(this.layersDropDownList, 'layer_id');
    //this.selectedFunctionality = this.tree.checkedNodes.filter(item => !(item.startsWith('_'))).join(',');
    //this.selectedFunctionality = this.se
   // this.selectedModules = this.tree.checkedNodes.filter(item => (item.startsWith('_'))).join(',');
    var keys = Object.keys(this.selectedFunctionality);
    var tempSelectedFunctionality = [];
    for(var k in keys){
      tempSelectedFunctionality.push( this.selectedFunctionality[keys[k]]);
    };
    let obj = {
      func_ids: tempSelectedFunctionality,
      layer_ids: this.layerslist.value,
      new_request: this.type,
      justification: this.justification
    };
    this.rs.getData(environment.admin.add_access_request, obj).subscribe(() => {
      this.ts._exception.emit(this.ts.getToastSuccessMessage('Your request for access is placed.'));
    });
    this.closebutton.nativeElement.click();
  }

  /* public nodeChecked(args): void {
    if (args.action == "check") {
      var id = args.data[0].id;
      var parentId = args.data[0].parentID;
      let rolesList = [];
      this.functionality_list.forEach(e => {
        if (e.nodeId == parentId) {
          rolesList = e.roles.map(role => {
            let _id = role['nodeId'].join(",");
            if (_id != id)
              return _id;
          });
        }

      });


      this.tree.checkedNodes = this.tree.checkedNodes.filter(function (item) {
        return !rolesList.includes(item);
      });

      args.cancel = true;


    }

  } */

  selectValue(parentId, val) {
    this.selectedFunctionality[parentId] = val.join(",");
  }

  expandPermision() {
    this.expandPermission = !this.expandPermission;
  }

  public expanding(e) {
    if (!e.isExpanded)
      e.cancel = true;
  }

  change(nodeId) {
    delete this.selectedFunctionality[nodeId];
  }

}
