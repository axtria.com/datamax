import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataQualityComponent } from './data-quality.component';

const routes: Routes = [
  {
    path: '', component: DataQualityComponent, children: [
      {path: 'dqObjectsView', loadChildren: () => import('./dq-objects/dq-objects.module').then(m => m.DqObjectsModule)},
      {path: 'dqRulesView', loadChildren: () => import('./dq-rules/dq-rules.module').then(m => m.DqRulesModule)}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataQualityRoutingModule { }
