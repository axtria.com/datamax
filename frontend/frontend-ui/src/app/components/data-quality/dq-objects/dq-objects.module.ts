import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DqObjectsComponent } from './dq-objects.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports:[DqObjectsComponent],
  declarations: [DqObjectsComponent]
})
export class DqObjectsModule { }
