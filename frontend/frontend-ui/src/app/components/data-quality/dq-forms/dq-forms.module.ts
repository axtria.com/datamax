import { NgModule } from '@angular/core';
import { SyncfusionModule } from 'src/app/shared/modules/syncfusion.module';
//import { DataValidationFormComponent } from 'src/app/components/container-section/main-canvas-section/data-quality/data-validations/data-validation-form/data-validation-form.component';
import { DqFormsComponent } from './dq-forms.component';

@NgModule({
  declarations: [DqFormsComponent
    //, DataValidationFormComponent
  ],
  imports: [SyncfusionModule],
  exports: [
    //DataValidationFormComponent
  ]
})
export class DqFormsModule { }
