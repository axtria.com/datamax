import { NgModule } from '@angular/core';
import { SyncfusionModule } from 'src/app/shared/modules/syncfusion.module';
import { DqRulesComponent } from './dq-rules.component';
//import { DataValidationListComponent } from 'src/app/components/container-section/main-canvas-section/data-quality/data-validations/data-validation-list/data-validation-list.component';
//import { ThresholdValidatonsComponent } from 'src/app/components/container-section/main-canvas-section/data-quality/threshold-validatons/threshold-validatons.component';
//import { ReferentialIntegrityComponent } from 'src/app/components/container-section/main-canvas-section/data-quality/referential-integrity/referential-integrity.component';
//import { DataContComponent } from 'src/app/components/container-section/main-canvas-section/data-quality/data-cont/data-cont.component';
import { DqFormsModule } from '../dq-forms/dq-forms.module';

@NgModule({
  declarations: [DqRulesComponent
    //, DataValidationListComponent
    //, ThresholdValidatonsComponent
    //, ReferentialIntegrityComponent
    //, DataContComponent
  ],
  imports: [
    SyncfusionModule,
    DqFormsModule
  ],
  exports: [DqRulesComponent],

})
export class DqRulesModule { }
