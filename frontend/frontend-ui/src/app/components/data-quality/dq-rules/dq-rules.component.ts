import { Component, OnInit, ViewChild } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { DataQualityDetails } from '../../container-section/main-canvas-section/data-quality/DataQualityDetails';
import { DataValidationListComponent } from '../../container-section/main-canvas-section/data-quality/data-validations/data-validation-list/data-validation-list.component';
import { ThresholdValidatonsComponent } from '../../container-section/main-canvas-section/data-quality/threshold-validatons/threshold-validatons.component';
import { ReferentialIntegrityComponent } from '../../container-section/main-canvas-section/data-quality/referential-integrity/referential-integrity.component';

@Component({
  selector: 'app-dq-rules',
  templateUrl: './dq-rules.component.html',
  styleUrls: ['./dq-rules.component.css']
})
export class DqRulesComponent implements OnInit {

  dataQualityContent: any;
  headerText: any;
  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };
  objectList: [];
  objectFields: object = { value: 'obj_id', text: 'obj_nm' };

  dataQualityDetails: DataQualityDetails = {
    obj_id: 0, attr_id: 0, rule_id: 0, obj_name: null, layer_id: 0
  };
  
  @ViewChild('dataValidationList', { static: false }) objdataValidationListComponent: DataValidationListComponent;
  @ViewChild('thresholdValidations', { static: false }) thresholdValidations: ThresholdValidatonsComponent;
  @ViewChild('referencialIntegrity', { static: false }) referencialIntegrity: ReferentialIntegrityComponent;

  constructor(private dataService: RestService, cs: ContentService) { 
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.headerText = this.dataQualityContent.subMenu.headerText;
  }

  ngOnInit() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_obj_meta_data_layers' })
      .subscribe(data => {this.businessLayerList = data && data.Contents ? data.Contents : []; });
  }

  businessLayerChange(e: any) {
    this.dataQualityDetails.obj_id = 0;

    this.populateDataQualityRules();

    this.dataService.getData(environment.dataQuality.getObjects,
      { name: 'get_data_layer_objects', src_data_layer_id: e.itemData.layer_id })
      .subscribe(data => {this.objectList = data && data.Contents ? data.Contents : []; });
  }

  populateDataQualityRules() {
    setTimeout(() => {
      if (typeof (this.objdataValidationListComponent) !== 'undefined') {
        this.objdataValidationListComponent.populateDataValidationRules();
      }
      if (typeof (this.thresholdValidations) !== 'undefined') {
        this.thresholdValidations.populateThresholdRules();
        }
      if (typeof (this.referencialIntegrity) !== 'undefined') {
          this.referencialIntegrity.populateRIRules();
        }
      });
  }

  addNewRule()
  {
    if (typeof (this.objdataValidationListComponent) !== 'undefined') {
      this.objdataValidationListComponent.addDataValidationRule();
    }    
  }
}
