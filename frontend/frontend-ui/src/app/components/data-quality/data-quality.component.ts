import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-data-quality',
  templateUrl: './data-quality.component.html',
  styleUrls: ['./data-quality.component.css']
})
export class DataQualityComponent implements OnInit {

  dataQualityContent: any;
  headerText: any;
  
  constructor(cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.headerText = this.dataQualityContent.menu.headerText;
   }

  ngOnInit() {
  } 
}
