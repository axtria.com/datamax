import { NgModule } from '@angular/core';
import { SyncfusionModule } from 'src/app/shared/modules/syncfusion.module';
import { DataQualityRoutingModule } from './data-quality-routing.module';
import { DataQualityComponent } from './data-quality.component';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { DqObjectsModule } from './dq-objects/dq-objects.module';
import { DqRulesModule } from './dq-rules/dq-rules.module';

@NgModule({
  declarations: [DataQualityComponent],
  imports: [
    SyncfusionModule,
    DataQualityRoutingModule,    
    DqObjectsModule,
    DqRulesModule
  ],
  providers: [AuthenticateService]
})
export class DataQualityModule { }
