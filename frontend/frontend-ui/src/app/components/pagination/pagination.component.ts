import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  i18Labels: any;
  @Input() public show: boolean;
  @Output() changePage = new EventEmitter<any>();

  constructor(cs: ContentService) { 
    this.i18Labels = cs.getContent("pagination");
  }

  ngOnInit() {
  }

  changePageSize(e) {
    this.changePage.emit(e);
  }

}
