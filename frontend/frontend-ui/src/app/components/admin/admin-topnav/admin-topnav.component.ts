import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-admin-topnav',
  templateUrl: './admin-topnav.component.html',
  styleUrls: ['./admin-topnav.component.css']
})
export class AdminTopnavComponent implements OnInit {

  key: string;
  i18Labels: any;

  constructor(private route: ActivatedRoute, private router: Router, cs: ContentService,
    private authenticateService: AuthenticateService) {
    this.i18Labels = cs.getContent(environment.modules.admin).menu;
    this.authenticateService.getUserInfo();
  }

  ngOnInit() {
  }

  openState(state) {
    this.key = state;
    this.router.navigate(['usermanagement', state], { relativeTo: this.route.parent });
  }

}
