import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { ToastPopupService } from '../../toast-popup/toast-popup.service';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {
  requestList: any=[];
  public textWrapSettings:  Object = {wrapMode: "both"};
  pageSettings = { pageSize: 8 };
  
  i18Labels: any;

 
  arrayOne(n: number): any[] {
    return Array(n);
  }
  constructor(private connection_call_Service: RestService, private ts: ToastPopupService, cs: ContentService,) {
    this.i18Labels = cs.getContent(environment.modules.admin).requests;

   }


  request_list() {
    this.connection_call_Service.getData(environment.admin.get_access_request, {
      userid: null,
      requestid: null
    }).subscribe((data) => {
      this.requestList = data && data.Contents || [];
        });
  }

  approveRequest(e){
    
    this.connection_call_Service.getData(environment.admin.execute_access_request, {
      action: 'approve' ,
      request_id: e['request_id']
    }).subscribe(() => {
      this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18Labels.toastwsApproveSuccess)); 
      this.request_list();
     });

  }


  rejectRequest(e){

    this.connection_call_Service.getData(environment.admin.execute_access_request, {
      action: 'reject',
      request_id:  e['request_id']
    }).subscribe(() => {
      this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18Labels.toastwsRejectSuccess));  
      this.request_list();
      });


  }

  ngOnInit() {
    this.request_list();
     }

}
