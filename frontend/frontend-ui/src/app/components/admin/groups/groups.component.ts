import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { EmitType } from '@syncfusion/ej2-base';
import { ActivatedRoute, Router } from '@angular/router';
import { DataUtil } from '@syncfusion/ej2-data';
import { StorageService } from 'src/app/shared/services/storage.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';


@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GroupsComponent implements OnInit {

  public groupsCount;
  public groupsList:any=[] ;
  public CurrentDate = new Date();

  userContent: any;
  public animationSettings: Object = { effect: 'Zoom' };
  public pageSettings: Object;
  public checkedIsAdmin: boolean = false;
  public formatOptions: Object = { type: 'date', skeleton:'long' } ;
  public toolbarOptions: object[] = [{text:'Search', align: 'Left'}, {type:"Input", template: '#statusTemplate' , align: 'Right'}];
  public sortOptions: object = { columns: [{ field: 'name', direction: 'Ascending' }] };

  @ViewChild('modalDialog', { static: false }) public modalDialog: DialogComponent;
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;
  @ViewChild('confirmDialog', { static: false }) public confirmDialog: DialogComponent;

  // On Button click, modal Dialog will be shown
  public modalBtnClick: EmitType<object> = () => {
   
    this.modalDialog.show();
    this.checkedIsAdmin = false;
  }
  // On Dialog close, 'Open' Button will be shown
  public modalDlgClose: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = '';
  }
  // On Dialog open, 'Open' Button will be hidden
  public modalDlgOpen: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = 'none';
  }
  i18Labels: any;
  rowData: any;
  
  public list_groups() {
    this.rs.getData(environment.groups.LIST_GROUPS, {
      userdetail: false,
      group_id: null,
      accessdetail: false,
      is_deleted: false
    }).subscribe(listgroups => {
      this.groupsCount = listgroups && listgroups.Contents && listgroups.Contents.length || 0;
      this.groupsList = listgroups && listgroups.Contents || [];
      this.groupsList = DataUtil.parse.parseJson(this.groupsList);
      this.grid && this.grid.refresh();
    });
  }

  public attachGroup() {

  }

  public editDetails(id) {
    this.storageService.setSessionItem('groupid',id);
    this.router.navigate(['groupdetails'], {relativeTo: this.route.parent});
  }
  

  
  // inactiveModalShow(data){
  //   data.is_active = !data.is_active;
  //   this.grid && this.grid.refresh();
  //   // this.rowData = data
  // }

  markActiveInactive(data){
    this.rs
    .getData(environment.groups.edit_usergroup, 
      {'description':data['description'],  
      'groupname': data['name'],
      'group_id': data['usergroup_id'],
      'is_active': !data['is_active'],
      'is_deleted': data['is_deleted']
    })
    .subscribe(() => {
      if(data['is_active']){
        this.ts._exception.emit(this.ts.getToastSuccessMessage( data['name'] + " has been marked Inactive"));

      }
      else{
        this.ts._exception.emit(this.ts.getToastSuccessMessage(data['name'] +" has been marked active"));

      }
      this.list_groups()
   
      
    });
  }

  modalShow(data){
    this.rowData = data
    this.confirmDialog.show();
  }

  public delete_group() {
    this.rs
    .getData(environment.groups.edit_usergroup, 
      {'description':this.rowData['description'],  
      'groupname': this.rowData['name'],
      'group_id': this.rowData['usergroup_id'],
      'is_active': this.rowData['is_active'],
      'is_deleted': true
    })
    .subscribe(() => {
      
      // this.router.navigate(['groups'], {relativeTo: this.route.parent});
      this.ts._exception.emit(this.ts.getToastSuccessMessage("Group is deleted successfully"));
      this.confirmDialog.hide();
      this.list_groups()
   
      
    });
    
  }

  dont_delete_group() {
    this.confirmDialog.hide();
  }


  showGroupDetails(id) {
    this.storageService.setSessionItem('groupid',id);
    this.router.navigate(['groupdetails'], {relativeTo: this.route.parent});
  }

  constructor(private rs: RestService, cs: ContentService, private storageService: StorageService,
    private router: Router, private route: ActivatedRoute, public authenticateService: AuthenticateService,
    private ts: ToastPopupService) {
    this.i18Labels = cs.getContent(environment.modules.admin).groups;
   }

  ngOnInit() {
    this.pageSettings = { pageSizes: [ '10', '15', '20', 'All'], };
    this.list_groups();
  }

}
