import { Component, OnInit, ViewChild } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
// import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { TabComponent } from '@syncfusion/ej2-angular-navigations/src/tab/tab.component';
import { RestService } from 'src/app/shared/services/rest.service';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilService } from 'src/app/shared/services/util.service';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { NgForm } from '@angular/forms';
//import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { GroupsService } from '../groups.service';
import { ConnectorService } from '../../connectors/connectors.service';


@Component({
  selector: 'app-create-new-group',
  templateUrl: './create-new-group.component.html',
  styleUrls: ['./create-new-group.component.css']
})
export class CreateNewGroupComponent implements OnInit {
  @ViewChild('tab', { static: false }) tab: TabComponent;
  @ViewChild('grid', { static: false }) grid: GridComponent;
  public i18contents: any;
  public selectedUserID: any = '';
  public listOfUsers: any[];
  //public listOfModules: any[];
  //public field: Object = {};
  public fields: any = { text: 'firstName', value: 'user_id' };
  @ViewChild('inputForm', { static: false }) inputForm: NgForm;
  public listToDisplayUsers: any[] = [];
  //public setToAddLocal = new Set();
  public columns = ['firstname', 'lastname', 'email'];
  public layersDropDownList: any = [];
  public groupName: any = '';
  public groupDescription: any = '';
  public showCheckBox: boolean = true;
  selectedFunctionality= {};

  functionality_list: any;

  public enableSave = false;

  _connectorInfo: any;
  selectedLayers: string;

  constructor(private cs: ContentService, private rs: RestService, public connectorDetails: ConnectorService,
    public router: Router, private utilService: UtilService, private route: ActivatedRoute, 
    private ts: ToastPopupService, private groupService: GroupsService) {
    this.i18contents = this.cs.getContent(environment.modules.admin).groups;
  }

  ngOnInit() {

    this._connectorInfo = this.connectorDetails.connectorInfo;

    let LIST_USERS = this.rs.getData(environment.users.LIST_USERS, { user_id: null });
    let fetch_all_layers = this.rs.getData(environment.layers.fetch_all_layers, {
      is_active: true
    });
    let get_modules = this.rs.getData(environment.groups.list_functionality, {});
    forkJoin([LIST_USERS, fetch_all_layers, get_modules]).subscribe(data => {
      this.listOfUsers = data[0].Contents;
      this.layersDropDownList = data[1].Contents || [];
      this.functionality_list = data[2].Contents || [];

      this.functionality_list = this.groupService.getFunctionalityTreeJSON(this.functionality_list, false, false);

     // this.field = { dataSource: this.functionality_list, id: 'nodeId', text: 'nodeText', child: 'roles' };

    });

  }

  public btnClicked(e: any): void {
    switch (e.target.id) {
      case 'searchNext':
        this.tab.enableTab(1, true);
        this.tab.select(1);
        break;
      case 'usersNext':
        this.tab.enableTab(2, true);
        this.tab.select(2);
        this.enableSave = true;
        break;
    }
  }

  public selectChanged() {
    this.listToDisplayUsers = this.utilService.pushToUniqueArray(this.listToDisplayUsers, this.listOfUsers, 'user_id', this.selectedUserID);
    this.grid && this.grid.refresh();
  }

  public deleteClicked(user) {
    let _index = user.index;
    this.listToDisplayUsers.splice(_index, 1);
    this.grid.refresh();
  }



  public finalSubmit() {
    // For selected layers list
    this.selectedLayers = this.utilService.concatCheckedValues(this.layersDropDownList, 'layer_id');

    // For selected modules list
    var keys = Object.keys(this.selectedFunctionality);
    var tempSelectedFunctionality = [];
    for(var k in keys){
      tempSelectedFunctionality.push( this.selectedFunctionality[keys[k]]);
    };
    //this.selectedFunctionality = this.tree.checkedNodes.filter(item => !(item.startsWith('_'))).join(',');
    //this.selectedModules =  this.tree.checkedNodes.filter(item => (item.startsWith('_'))).join(',');
    
    //this.utilService.concatCheckedValues(this.listOfModules, 'module_id');

    let listOfUserIDs = '';
    this.listToDisplayUsers.forEach(user => { listOfUserIDs = listOfUserIDs + user.user_id + ','; });

    this.rs.getData(environment.groups.add_usergroup_module_layer_user_functionality, {
      groupname_v: this.groupName,
      description_v: this.groupDescription,
      user_ids_v: listOfUserIDs.substr(0, listOfUserIDs.length - 1),
      module_ids_v: null,
      layer_ids_v: this.selectedLayers,
      functionality_ids_v: tempSelectedFunctionality,
      group_type_c: 'C'
    }).subscribe(() => {
      this.ts._exception.emit(this.ts.getToastSuccessMessage(this.groupName + ' Group Created Successfully!'));
      this.router.navigate(['groups'], { relativeTo: this.route.parent });
    
      
    });

  }

  selectValue(parentId, val) {
    this.selectedFunctionality[parentId] = val.join(",");
  }

  public expanding(e) {
    if (!e.isExpanded)
      e.cancel = true;
  }

  change(nodeId) {
    delete this.selectedFunctionality[nodeId];
  }
}
