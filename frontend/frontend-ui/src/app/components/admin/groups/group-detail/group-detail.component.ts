import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { ContentService } from 'src/app/shared/services/content.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Location } from '@angular/common';
import { GroupsService } from '../groups.service';
import { forkJoin } from 'rxjs';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { ConnectorService } from '../../connectors/connectors.service';


@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']

})
export class GroupDetailComponent implements OnInit {

  groupinfo: any; userdetails = [];
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;
  @ViewChild('modalgridelement', { static: false }) public modalgridelement: GridComponent;
  @ViewChild('confirmDialog', { static: false }) public confirmDialog: DialogComponent;
  @ViewChild('modalDialog', { static: false }) public modalDialog: DialogComponent;
  @ViewChild('modalDialogModules', { static: false }) public modalDialogModules: DialogComponent;
  @ViewChild('treeelement', { static: false }) public tree: TreeViewComponent;
  @ViewChild('addUserGroup', { static: false }) public addUserGroup: DialogComponent;
  @ViewChild('addLayerToGroup', { static: false }) public addLayerToGroup: DialogComponent;
  @ViewChild('addModuleToGroup', { static: false }) public addModuleToGroup: DialogComponent;
  //@ViewChild('treeView', { static: false }) public treeView: TreeView;


  modules: any;
  modulesName: any[];
  public target: string = "#modalTarget";
  public animationSettings: Object = { effect: 'Zoom' };
  public listOfUsers: any[];
  public listToDisplayUsers: any[];
  public selectedUserID: any;
  layers: any;
  description: any;
  grpname: any;
  listOfLayers: any[] = [];
  displayListForLayers: any[] = [];
  public selectedLayerId: any = '';
  functionality_list = [];
  dataSource = [];
  i18Labels: any;
  public field: Object = {};
  selectedFunctionality = {};
  selectedEditFunctionality = {};
  showPermissionModal = false;


  public modalBtnClick1: EmitType<object> = () => {

    this.modalDialog.show();


  }


  public modalBtnClick: EmitType<object> = () => {

    this.confirmDialog.show();


  }

  public modalDlgOpen: EmitType<object> = () => {
    this.showPermissionModal = true;
    document.getElementById('modalbtn').style.display = 'none';
  }

  public modalDlgClose: EmitType<object> = () => {
    this.showPermissionModal = false;
    document.getElementById('modalbtn').style.display = '';


  }
  get_modules: any;
  Editfunctionality_list= [];
  //field1: { dataSource: any; id: string; text: string; child: string; };
  selectedModules: string;
  nodeIdAll: any[];


  constructor(private storageService: StorageService, private restService: RestService,
    cs: ContentService, private ts: ToastPopupService, public connectorDetails: ConnectorService,
    private router: Router, private route: ActivatedRoute, public authenticateService: AuthenticateService,
    private utilService: UtilService, private location: Location, private groupService: GroupsService) {
    this.i18Labels = cs.getContent(environment.modules.admin).groups;
  }

  removegrp(user) {
    this.restService
      .getData(environment.users.remove_user,
        {
          'group_id': this.groupinfo.usergroup_id,
          'user_ids': user.user_id,
          'groupname': this.groupinfo.name
        })
      .subscribe(() => {

        this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18Labels.toastwsRemoveSuccess, [user.firstname, user.lastname, this.groupinfo.name]));
        this.getGroupDetails();

      });
  }

  editModulesFunctionality() {

    this.selectedEditFunctionality = JSON.parse(JSON.stringify(this.selectedFunctionality));
    this.modalDialogModules.show();
  }





  ngOnInit() {

    let get_modules = this.restService.getData(environment.groups.list_functionality, {});
    let get_users_list = this.restService.getData(environment.users.LIST_USERS, { user_id: null });
    forkJoin([get_modules, get_users_list]).subscribe(data => {

      this.Editfunctionality_list = data[0].Contents || [];
      this.listOfUsers = data[1].Contents || [];


      this.Editfunctionality_list = this.groupService.getFunctionalityTreeJSON(this.Editfunctionality_list, true, false);

      //this.field1 = { dataSource: this.Editfunctionality_list, id: 'nodeId', text: 'nodeText', child: 'roles' };
    });
    this.getGroupDetails();

  }

  public getGroupDetails() {
    this.dataSource = [];
    let _id = this.storageService.getSessionItem('groupid');
    _id && this.restService.getData(environment.groups.LIST_GROUPS, {
      userdetail: true,
      group_id: _id,
      accessdetail: true,
      is_deleted: false
    }).subscribe(data => {
      if (data && data.Contents && data.Contents.length == 1) {
        this.groupinfo = data.Contents[0];
        this.grpname = this.groupinfo.name;
        this.description = this.groupinfo.description;
        this.userdetails = this.groupinfo['userdetails'];
        //this.grid && this.grid.refresh();
        this.functionality_list = this.groupinfo && this.groupinfo.moduleroles || [];

        this.functionality_list = this.groupService.getFunctionalityTreeJSON(this.functionality_list, true, false);


        this.field = { dataSource: this.functionality_list, id: 'nodeId', text: 'nodeText', child: 'roles' };

        let nodeIdAll = [];
        this.selectedFunctionality = {};
        this.functionality_list.forEach(e => {
          e['roles'].forEach(e1 => { nodeIdAll.push(e1['nodeId']) })
          this.selectedFunctionality[e['nodeId']] = e['roles'] && e['roles'].length >0 ? e['roles'][0]['nodeId'].join(',') : "";
        });
        this.nodeIdAll = nodeIdAll;
        this.selectedEditFunctionality = JSON.parse(JSON.stringify(this.selectedFunctionality));
      }
    });
  }

  editPermissions() {
    // For selected modules list
    var keys = Object.keys(this.selectedEditFunctionality);
    var tempSelectedFunctionality = [];
    for(var k in keys){
      tempSelectedFunctionality.push( this.selectedEditFunctionality[keys[k]]);
    };
    this.selectedModules = tempSelectedFunctionality.join(',');
    //this.selectedModules = this.treeView.checkedNodes.filter(item => !(item.startsWith('_'))).join(',');
    let removedList = this.nodeIdAll.join(',').split(',').filter(x => this.selectedModules.split(',').indexOf(x) == -1);
    let addedList = this.selectedModules.split(',').filter(x => this.nodeIdAll.join(',').split(',').indexOf(x) == -1);

    let add_functionality = this.restService.getData(environment.groups.add_functionality, {
      usergroup_id: this.groupinfo.usergroup_id,
      functionality_ids: addedList
    });
    let remove_functionality = this.restService.getData(environment.groups.remove_functionality, {
      usergroup_id: this.groupinfo.usergroup_id,
      functionality_ids: removedList
    });
    let apiList = [];
    if (removedList.length > 0)
      apiList.push(remove_functionality);
    if (addedList.length > 0)
      apiList.push(add_functionality);
    apiList.length > 0 && forkJoin(apiList).subscribe(() => {
      this.ts._exception.emit(this.ts.getToastSuccessMessage("Permissions are edited successfully"));
      this.modalDialogModules.hide();
      this.ngOnInit();
    });
    if (apiList.length == 0)
      this.modalDialogModules.hide();

  }

  delete_group(data) {
    this.restService
      .getData(environment.groups.edit_usergroup,
        {
          'description': data['description'],
          'groupname': data['name'],
          'group_id': data['usergroup_id'],
          'is_active': data['is_active'],
          'is_deleted': true
        })
      .subscribe(() => {

        this.router.navigate(['groups'], { relativeTo: this.route.parent });
        this.ts._exception.emit(this.ts.getToastSuccessMessage("Group is deleted successfully"));
        this.confirmDialog.hide();


      });
  }

  Closedialog() {
    this.modalDialog.hide();
  }

  ClosedialogModules() {

    this.modalDialogModules.hide();
  }

  editDescription() {
    this.restService
      .getData(environment.groups.edit_usergroup,
        {
          'description': this.description,
          'groupname': this.grpname,
          'group_id': this.groupinfo.usergroup_id,
          'is_active': this.groupinfo.is_active,
          'is_deleted': false
        })
      .subscribe(() => {
        this.modalDialog.hide();
        this.ts._exception.emit(this.ts.getToastSuccessMessage("Group description edited successfully"));
        this.getGroupDetails();


      });

  }

  dont_delete_group() {
    this.confirmDialog.hide();
  }

  openUserDetails() {
    this.location.back();
  }

  public addNewUserToGroup() {
    this.addUserGroup.show();
    this.listToDisplayUsers = [];
    this.selectedUserID = '';
    this.listOfUsers = this.listOfUsers.filter((user) => {
      let match = this.userdetails ? this.userdetails.findIndex((data) => {
        return data.user_id == user.user_id
      }) : -1;
      return match == -1;
    });
  }

  public selectChanged() {
    this.listToDisplayUsers = this.utilService.pushToUniqueArray(this.listToDisplayUsers, this.listOfUsers, 'user_id', this.selectedUserID);
    this.modalgridelement && this.modalgridelement.refresh();
  }

  public deleteFromModal(index) {
    this.listToDisplayUsers.splice(index, 1);
    this.modalgridelement.refresh();
  }

  public addToMainTable() {
    let listOfIds = '';
    this.listToDisplayUsers.forEach(item => {
      listOfIds = listOfIds + item.user_id + ',';
    });
    this.restService
      .getData(environment.groups.add_user_to_usergroup,
        {
          'groupName': this.grpname,
          'group_id': this.groupinfo.usergroup_id,
          'user_id_list': listOfIds.substring(0, listOfIds.length - 1)
        }).subscribe(() => {
          this.getGroupDetails();
          this.addUserGroup.hide();
        });
  }

  addLayersClicked() {
    this.addLayerToGroup.show();
    this.displayListForLayers = [];
    this.selectedLayerId = '';
    this.restService.getData(environment.layers.fetch_all_layers, {
      is_active: true
    }).subscribe(data => {
      this.listOfLayers = data.Contents;
    });
  }

  layerSelectChanged() {
    this.displayListForLayers = this.utilService.pushToUniqueArray(this.displayListForLayers, this.listOfLayers, 'layer_id', this.selectedLayerId);
    this.modalgridelement && this.modalgridelement.refresh();
  }

  addLayersToMainTable() {
    let listOfIds = '';
    this.displayListForLayers.forEach(item => {
      listOfIds = listOfIds + item.layer_id + ',';
    });
    this.restService
      .getData(environment.groups.add_layer_to_usergroup,
        {
          'usergroup_id': this.groupinfo.usergroup_id,
          'layer_ids': listOfIds.substring(0, listOfIds.length - 1)
        }).subscribe(() => {
          this.getGroupDetails();
          this.addLayerToGroup.hide();
        });
  }

  removelayer(layer) {
    this.restService
      .getData(environment.groups.remove_layer_from_usergroup, {
        'usergroup_id': this.groupinfo.usergroup_id,
        'layer_ids': layer.layer_id
      })
      .subscribe(() => {
        this.getGroupDetails();
      });
  }

  selectValue(parentId, val) {
    this.selectedEditFunctionality[parentId] = val.join(",");
  }

  public expanding(e) {
    if (!e.isExpanded)
      e.cancel = true;
  }

  change(nodeId) {
    delete this.selectedEditFunctionality[nodeId];
  }

}
