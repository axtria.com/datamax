import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  constructor() { }

  getTreeJSON(list, expanded, isChecked) {
    list.forEach(element => {
      element['module_id'] = '_' + element['module_id'];
      element['expanded'] = expanded;
      element['roles'] && element['roles'].forEach(element => {
        element['isChecked'] = isChecked;
        element['functionalities'] = element['functionality'].map(ele => ele.functionality_id);
      })
    });

    let str = JSON.stringify(list);
    str = str.replace(/role_id/g, "nodeId");
    str = str.replace(/role_nm/g, "nodeText");
    str = str.replace(/module_id/g, "nodeId");
    str = str.replace(/module_name/g, "nodeText");
    return JSON.parse(str);
  }

  getFunctionalityTreeJSON(list, expanded, isChecked) {
    list.forEach(element => {
      element['module_id'] = '_' + element['module_id'];
      element['nodeId'] = '_' + element['module_id'];
      element['nodeText'] = element['description'];
      element['expanded'] = expanded;
      element['level'] = 1;
      element['roles'] && element['roles'].forEach(role => {
        role['level'] = 2;
        role['isChecked'] = isChecked;
        role['nodeId'] = role['functionality'].map(ele => ele.functionality_id).sort();
        role['nodeText'] = role['role_nm'];
        
      })
    });

    return list;
    /* let str = JSON.stringify(list);
    str = str.replace(/functionalities/g, "nodeId");
    str = str.replace(/role_nm/g, "nodeText");
    str = str.replace(/module_id/g, "nodeId");
    str = str.replace(/description/g, "nodeText");
    return JSON.parse(str); */
  }
}
