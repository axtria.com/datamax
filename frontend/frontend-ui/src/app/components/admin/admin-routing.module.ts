import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { RequestsComponent } from './requests/requests.component';
import { ConnectorsComponent } from './connectors/connectors.component';
import { ConnectorDetailsComponent } from './connectors/connector-details/connector-details.component';
import { NewConnectionComponent } from './connectors/new-connection/new-connection.component';
import { LayersComponent } from './layers/layers.component';
import { CreateNewLayerComponent } from './layers/create-new-layer/create-new-layer.component';
import { LayerDetailsComponent } from './layers/layer-details/layer-details.component';
import { AnnouncementsComponent } from './announcements/announcements.component';
import { NewAnnouncementsComponent } from './announcements/new-announcements/new-announcements.component';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { UsersComponent } from './users/users.component';
import { GroupsComponent } from './groups/groups.component';
import { GroupDetailComponent } from './groups/group-detail/group-detail.component';
import { CreateNewGroupComponent } from './groups/create-new-group/create-new-group.component';
import { AdminTopnavComponent } from './admin-topnav/admin-topnav.component';

const routes: Routes = [
  { path : '', component: AdminComponent,  children: [
    //{ path : '', component: UserDetailsComponent},
    // { path : 'requests', component: RequestsComponent},
    { path : 'connectors', component: ConnectorsComponent},
    { path : 'connectorsdetails', component: ConnectorDetailsComponent},
    { path : 'newconnection', component: NewConnectionComponent},
    { path : 'layers', component: LayersComponent},
    { path : 'newlayer', component: CreateNewLayerComponent},
    { path : 'layerdetails', component: LayerDetailsComponent},
    { path : 'announcements', component: AnnouncementsComponent},
    { path : 'newannouncements', component: NewAnnouncementsComponent},
    //{ path : 'usermanagement', loadChildren: () => import('./usermanagement/usermanagement.module').then(m => m.UsermanagementModule) }
    {
      path: 'usermanagement', component: AdminTopnavComponent, children: [
        { path : 'requests', component: RequestsComponent},
        { path : '', component: UserDetailsComponent},
        { path: 'users', component: UsersComponent },
        { path: 'groups', component: GroupsComponent },
        { path: 'groupdetails', component: GroupDetailComponent },
        { path: 'newGroup', component: CreateNewGroupComponent },
        { path : 'userdetails/:type', component: UserDetailsComponent},
      ]
    }

  ]}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
