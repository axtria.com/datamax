import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConnectorService } from '../../connectors/connectors.service';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { LayerService } from '../layer.service';

@Component({
  selector: 'app-layer-details',
  templateUrl: './layer-details.component.html',
  styleUrls: ['./layer-details.component.css']
})
export class LayerDetailsComponent implements OnInit {

  public i18contents: any;
  public _layerInfo: any;
  public layerStatus: any;
  public layer_status_label;
  public isLayerDataEditable = false;
  public edit_layer_info: any;
  public description: any;
  public layerId: any;


  constructor(private cs: ContentService,  private route: ActivatedRoute,
    private rs: RestService, public connectorDetails: ConnectorService, private ts: ToastPopupService,
    private layerService: LayerService, public router: Router) { 
    this.i18contents = this.cs.getContent(environment.modules.admin).layers;
  }

  ngOnInit() {
    this._layerInfo = this.connectorDetails.connectorInfo;
    this.layerStatus = this._layerInfo.is_active;
    this.layer_status_label = this.layerStatus ? (this.i18contents.active) : (this.i18contents.in_active)
    this.isLayerDataEditable = this.layerService.isLayerDataEditable;
    this.description = this.connectorDetails.connectorInfo.description;
  }

  editLayerInfo() {
    this.isLayerDataEditable = true;
    this.edit_layer_info = this.connectorDetails.connectorInfo;
    if(this.edit_layer_info != null) {
      this.edit_layer_info.description = this.description;
      this.edit_layer_info.is_active = this.layerStatus;
      this.rs.getData(environment.layers.update_layer_info, 
        { description: this.edit_layer_info.description,
          layerStatus: this.edit_layer_info.is_active,
          id: this.edit_layer_info.layer_id
        }).subscribe(() => {
          this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18contents.toastLayerUpdateSuccess));
            this.router.navigate(['layers'], { relativeTo: this.route.parent });
        });
    }
  }

}
