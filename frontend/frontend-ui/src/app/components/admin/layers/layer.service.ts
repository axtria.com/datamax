import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LayerService {
  public isLayerDataEditable: any;

  constructor() { }

  setIsLayerDataEditable(isLayerDataEditable) {
    this.isLayerDataEditable = isLayerDataEditable;
  }

  setsFSchemaList (schemaName) {
    var keys = Object.keys(schemaName.response);
    var listOfSchema = [];
    keys.forEach((key) => listOfSchema.push(schemaName.response[key]));
    return listOfSchema;
  }

}
