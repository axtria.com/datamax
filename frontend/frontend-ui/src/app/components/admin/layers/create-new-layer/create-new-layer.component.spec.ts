import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewLayerComponent } from './create-new-layer.component';

describe('CreateNewLayerComponent', () => {
  let component: CreateNewLayerComponent;
  let fixture: ComponentFixture<CreateNewLayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNewLayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
