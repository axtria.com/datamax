import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';
import { TabComponent } from '@syncfusion/ej2-angular-navigations/src/tab/tab.component';
import { RestService } from 'src/app/shared/services/rest.service';
import { GridComponent, SelectionSettingsModel } from '@syncfusion/ej2-angular-grids';
import { ConnectorService } from '../../connectors/connectors.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { LayerService } from '../layer.service';
import { ComboBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-new-layer',
  templateUrl: './create-new-layer.component.html',
  styleUrls: ['./create-new-layer.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CreateNewLayerComponent implements OnInit {
  @ViewChild('tab', {static: false}) tab: TabComponent;
  @ViewChild('grid', {static: false}) grid: GridComponent;
  @ViewChild('selectedPath', { static: false }) selectedPath: ComboBoxComponent;
  @ViewChild('newForm', { static: false }) newLayerForm: NgForm;
  public selectionOptions: SelectionSettingsModel;
  public i18contents: any;
  public selectedS3Bucket: any;
  public layerStatus;
  public sftpPath;
  public selectedSchema;
  public rsSchemaName;
  public layersList: any = [];
  public s3PathList: any = [];
  public schemaList: any = [];
  public layerName: any = '';
  public layerDescription: any = '';
  public statusIsChecked = true;
  public enableSave = false;
  public selectedLayers: string;
  public connectorList : any = [];
  public formatOptions: any;
  public selectedConnectorName: any;
  public userSelectedConnectorID: any;
  constructor(private cs: ContentService, private rs: RestService, private route: ActivatedRoute,
      public connectorDetails: ConnectorService, private ts: ToastPopupService, private layerService: LayerService,
      public router: Router) {
      this.i18contents = this.cs.getContent(environment.modules.admin).layers;
  }

  ngOnInit() {
    this.selectionOptions = { type: 'Single' };
    this.formatOptions = {type: 'date', format: 'MM/dd/yyyy | hh:mm a'};
    this.getconnectorList();
  }


  getconnectorList() {
    this.rs.getData(environment.connectors.connectors, {}).subscribe((data) => {
      this.connectorList = data && data.Contents || [];
    });
  }

  public connectorSelected(selectedConnectorData) {
    this.selectedConnectorName =  selectedConnectorData.data;
    this.userSelectedConnectorID = this.selectedConnectorName.id;
    if(this.selectedConnectorName.connection_type == 's3') {
      this.getS3PathList();
    } else if (this.selectedConnectorName.connection_type == 'sf') {
      this.getsFSchemaList();
    }
  }

  getS3PathList() {
    this.rs.getDataWithoutAlert(environment.layers.fetch_s3_path_list, {
      'connector_id' : this.userSelectedConnectorID
    }).subscribe((s3PathInfo)=> {
      this.s3PathList = s3PathInfo.buckets;
    }, () => {
      this.s3PathList = [];
      
     });
  }

  getsFSchemaList() {
    this.rs.getDataWithoutAlert(environment.layers.fetch_sf_schema_list, {
      'sf_connector_id' : this.userSelectedConnectorID
    }).subscribe((schemaName)=> {

      this.schemaList = this.layerService.setsFSchemaList(schemaName);
    }, () => {
      this.schemaList = [];
      
     });
  }

  public btnClicked(e: any): void {
    switch (e.target.id) {
      case 'searchNext':
        this.tab.enableTab(1, true);
        this.tab.select(1);
        this.newLayerForm && this.newLayerForm.reset();
        this.tab.refresh();
        break;
      case 'usersNext': 
        this.tab.enableTab(2, true);
        this.tab.select(2);
        this.enableSave = true;
        break;
    }
  }

  public saveNewLayer() {
    let obj = {
      layer_type: this.selectedConnectorName.connection_type,
      layer_name: this.layerName,
      description: this.layerDescription,
      connector_id: this.userSelectedConnectorID,
     };
    obj['schema_name'] = this.selectedSchema == undefined ? this.rsSchemaName : this.selectedSchema;
    obj['path'] = this.selectedS3Bucket == undefined ? this.sftpPath : (this.i18contents.s3_path_begin_string) + this.selectedS3Bucket;
   this.rs.getData(environment.layers.add_layer, obj).subscribe(() => {
     this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18contents.toastLayerCreationSuccess));
     this.router.navigate(['layers'], { relativeTo: this.route.parent});
  });
  }
}