import { Component, OnInit, ViewChild} from '@angular/core';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { ContentService } from 'src/app/shared/services/content.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConnectorService } from '../connectors/connectors.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { LayerService } from './layer.service';


@Component({
  selector: 'app-layers',
  templateUrl: './layers.component.html',
  styleUrls: ['./layers.component.css']
})
export class LayersComponent implements OnInit {

  @ViewChild('rsgrid', {static: false}) public rsgrid: GridComponent;
  @ViewChild('layerDetails', { static: false }) public layerDetails;

  public layersList: any;
  public formatOptions: any;
  public listOfLayersReceived: any = [];
  public uniqueLayers: any = [];
  public rsLayerList: any = [];
  public s3LayerList: any = [];
  public sfLayerList: any = [];
  public i18Labels: any;
  public isLayerDataEditable = false;
  public rs_s3Headers = ['name', 's3_path' ,'created_date'];
  public sfGridHeaders = ['name', 'schema_name','created_date'];
  result = [];
  public searchText;
  
  constructor(private rs: RestService, cs: ContentService, private router: Router, private route: ActivatedRoute,
    public connectorService: ConnectorService, private utilService: UtilService, private layerService: LayerService) {
    this.i18Labels = cs.getContent(environment.modules.admin).layers;
   }

  ngOnInit() {
    this.rs.getData(environment.layers.fetch_all_layers, {
      is_active: null
    })
    .subscribe((layersData) => {
      this.layersList = layersData.Contents;
      
      let property = 'layer_type';
      this.result = this.utilService.getArrayGroupByKey(this.layersList, property);
      this.rsgrid && this.rsgrid.refresh();
    });
    this.formatOptions = {type: 'date', format: 'MM/dd/yyyy | hh:mm a'};
  }

  expandLayerView(e) {
    if (e.target.classList.contains('collapsible')) {
      e.target.classList.toggle('active1');
      if (e.target.nextElementSibling.style.maxHeight) {
        e.target.nextElementSibling.style.maxHeight = null;
      } else {
        e.target.nextElementSibling.style.maxHeight = e.target.nextElementSibling.scrollHeight + 'px';
      }
    }
  }

  editLayerData(layerData) {
    this.layerService.setIsLayerDataEditable(true);
    this.connectorService.connectorInfo = layerData;
    this.router.navigate(['layerdetails'], { relativeTo: this.route.parent});
  }

  viewLayerDetails(data) {
    this.layerService.setIsLayerDataEditable(false);
    this.connectorService.connectorInfo = data;
    this.router.navigate(['layerdetails'], { relativeTo: this.route.parent });
  }

}
