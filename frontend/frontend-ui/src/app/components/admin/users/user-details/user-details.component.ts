import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  email;
  public pageSettings: Object; 
  public animationSettings: Object = { effect: 'Zoom' };
  userid: any; _userInfo; type; dataSource;

  @ViewChild('modalDialog', { static: false }) public modalDialog: DialogComponent;
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;

  public modalBtnClick: EmitType<object> = () => {
    this.modalDialog.show();


  }
  i18Labels: any;
  firstname: any;
  lastname: any;
  firstName: any;
  lastName: any;

  Closedialog(){
    this.firstname = this.firstName;
    this.lastname = this.lastName;
    this.modalDialog.hide();
  }

  // On Dialog close, 'Open' Button will be shown
  public modalDlgClose: EmitType<object> = () => {

    document.getElementById('modalbtn').style.display = '';
    this.firstname = this.firstName;
    this.lastname = this.lastName;
    

  }
  // On Dialog open, 'Open' Button will be hidden
  public modalDlgOpen: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = 'none';
  }
  name: any;

  constructor(private storageService: StorageService, private gs: StorageService, private connection_call_Service: RestService,  private router: Router,
    private ts: ToastPopupService, cs: ContentService, private restService: RestService,
    private route: ActivatedRoute, private as: AuthenticateService, private location: Location) {
      this.i18Labels = cs.getContent(environment.modules.admin).users;
     }

  attachGroup() {

  }

  removegrp(id, name){
    this.restService
    .getData(environment.users.remove_user, 
      {'group_id':id,  
      'user_ids': this.userid,
      'groupname': name
    })
    .subscribe(() => {
      
      this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18Labels.toastwsRemoveSuccess, [this.firstname, this.lastname, name]));
      this.ngOnInit();
      
    });
  }



  editDetails() {
    this.firstName = this.firstname;
    this.lastName = this.lastname;
    this.connection_call_Service
      .getData(environment.users.edit_user,
        {
          'username': this.email,
          'user_id': this.userid,
          'firstname': this.firstname,
          'lastname': this.lastname,
          'is_active': true,
          'is_deleted': null,
          'is_local_user': true,
          'is_ldap_user': false,
          'email': this.email,
          'password': null,
          'is_admin': null
        })
      .subscribe(() => {

        this.ts._exception.emit(this.ts.getToastSuccessMessage('User details edited Successfully.'));

      });
    this.modalDialog.hide();
  }

  showGroupDetails(id) {
  
    // this.us.groupid = id;
    this.gs.setSessionItem('groupid', id);
    this.router.navigate(['groupdetails'], {relativeTo: this.route.parent});
  }

  goToUserList() {
    this.location.back();
  }

  fetchData() {
    this.userid = this._userInfo && this._userInfo['user_id'];
    this.firstname = this._userInfo['firstname']
    this.lastname = this._userInfo['lastname']
    this.firstName = this.firstname;
    this.lastName = this.lastname;
 

    this.email = this._userInfo.email;

    this.dataSource = [];
    this._userInfo && this._userInfo.usergroupdetail && this._userInfo.usergroupdetail.forEach((group) => {
      group.modules && group.modules.forEach((item) => this.dataSource = [ ...this.dataSource, ...item['functionality']] );
    });
  }

  ngOnInit() {
    this.pageSettings = { pageSize: 8 };
    this.route.params.subscribe(routeParams => {
      this.type = routeParams.type !== 'details';
      if(this.type) {
        this._userInfo = this.as.getUserInfo();
        this.fetchData();
      } else {
        this.userid = this.storageService.getSessionItem('userid');
        this.userid && this.connection_call_Service
          .getData(environment.users.LIST_USERS, {
              user_id: this.userid
            }).subscribe(data => {

            if (data && data.Contents && data.Contents.length == 1) {
              this._userInfo = data.Contents[0];   
                 

              this.fetchData();

            }
          });
      }
    });
    

  }

}