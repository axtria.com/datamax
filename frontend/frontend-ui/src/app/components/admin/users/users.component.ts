import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { GridComponent, ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { EmitType } from '@syncfusion/ej2-base';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { StorageService } from 'src/app/shared/services/storage.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  status: any;
  public emailId;
  public username;
  public userListCount;
  public statusData: object[] = [{text: 'All users', value: ""}, {text: 'Active users', value: "true"}, {text: 'Inactive users', value: "false"}];
  public userList:any=[];
  i18Labels: any;
  public animationSettings: Object = { effect: 'Zoom' };
  public pageSettings: Object;
  public checkedIsAdmin: boolean = false;
  public toolbarOptions: ToolbarItems[] | object; rowData;
  public sortOptions: object = { columns: [{ field: 'firstname', direction: 'Ascending' }] };

  @ViewChild('modalDialog', { static: false }) public modalDialog: DialogComponent;
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;
  @ViewChild('inactiveConfirmDialog', { static: false }) public inactiveConfirmDialog: DialogComponent;
  @ViewChild('userForm', { static: false }) userForm : NgForm;
  @ViewChild('element', { static: false }) element;

  // On Button click, modal Dialog will be shown
  public modalBtnClick: EmitType<object> = () => {
    this.modalDialog.show();
    this.emailId = '';
    this.username = '';
    this.checkedIsAdmin = false;
  }
  // On Dialog close, 'Open' Button will be shown
  public modalDlgClose: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = '';
    this.userForm.reset();
  }
  // On Dialog open, 'Open' Button will be hidden
  public modalDlgOpen: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = 'none';
  }
  firstname: any;
  lastname: any;
  list: any[];
  //public statusDropdown: any = new DropDownList({ dataSource: this.statusData, width: 120, placeholder: 'All users', value: "status", change: this.activeUsers});
  
  
  
  public list_users() {
    this.connection_call_Service.getData(environment.users.LIST_USERS, {
      user_id: null
    }).subscribe(listusers => {
      this.userListCount = listusers && listusers.Contents && listusers.Contents.length || 0;
      
      this.userList = listusers && listusers.Contents || []
      
      this.list = this.userList
     
      
      this.grid && this.grid.refresh();
    });
  }
  

  activeUsers(){
    if(this.status === "")
      this.list = this.userList;
    else {
      let _isActive = Boolean(JSON.parse(this.status));
      this.list = this.userList.filter(item => item.is_active === _isActive);
      
      
    }
   }

  public showUserDetails(_data) {

    this.storageService.setSessionItem('userid', _data['user_id']);
    this.router.navigate(['userdetails', 'details'], {relativeTo: this.route.parent});

  }

  Closedialog(){
    this.userForm.reset();
    this.modalDialog.hide();
  }


  public attachGroup() {

  }

  public editDetails() {
    
  }

 

  // Close the Dialog, while clicking "OK" Button of Dialog
  public Submit(): void {
    this.modalDialog.hide();
    
    //creating new user
    this.connection_call_Service
      .getData(environment.users.add_user, 
      { 'email' : this.emailId , 'is_ldap_user':  false, 'is_local_user' : true, 
        user_name: this.username, firstname: this.firstname, lastname: this.lastname, password: null,  'is_admin': this.checkedIsAdmin})
      .subscribe(() => {
        this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18Labels.toastUserCreationSuccess));
        this.list_users();
      });
  
  
  
  }
  inactive_user() {
    this.inactiveConfirmDialog.hide();
    this.connection_call_Service
      .getData(environment.users.edit_user,
        {
          'username': null,
          'user_id': this.rowData.user_id,
          firstname: null,
          lastname: null,
          'is_active': !this.rowData.is_active,
          'is_deleted': null,
          'is_local_user': null,
          'is_ldap_user': null,
          'email': null,
          'password': null,
          'is_admin': null
        })
      .subscribe((response) => {
        if(response && response.Contents && response.Contents.length == 1) {
          
          this.ts._exception.emit(this.ts.getToastSuccessMessage(response.Contents[0].is_active ? this.i18Labels.toastUserActiveSuccess : this.i18Labels.toastUserInActiveSuccess
            , [this.rowData.firstname, this.rowData.lastname]));
          this.list_users();
          this.grid && this.grid.refresh();
        }

      });
    
  }

  inactiveModalShow(data){
    // data.is_active = !data.is_active;

    this.grid && this.grid.refresh();
    this.rowData = data
    this.inactiveConfirmDialog.show();
  }

  dont_inactive_user() {
    this.inactiveConfirmDialog.hide();
  }

  constructor(private connection_call_Service: RestService, cs: ContentService,
    public storageService: StorageService, private router: Router, private route: ActivatedRoute,
    private ts: ToastPopupService) {
    this.i18Labels = cs.getContent(environment.modules.admin).users;
   }

  ngOnInit() {
    this.pageSettings = { pageSize: 7 };
    this.toolbarOptions = [{text:'Search', align: 'Left'}, {type:"Input", template: '#statusTemplate' , align: 'Right'}];
    this.list_users();
  }

}
