import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';


@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit {
  notificationList: any;
  filteredArray: any;
  public sortOptions: object = { columns: [{ field: 'sent_on', direction: 'Descending' }] };
  i18Labels: any;


  constructor(private rs: RestService,  cs: ContentService) {
    this.i18Labels = cs.getContent(environment.modules.admin).announcements;

   }

  list_notifications() {
    this.rs.getData(environment.notifications.get_notifications, {}).
      subscribe(data => {
        this.notificationList = data && data.Contents || [];
        this.notificationList = { records: this.notificationList }

        var type = ['announcement']
        this.filteredArray = this.notificationList.records.filter(function (itm) {
          return type.indexOf(itm.type) > -1;
        });
      });


  }



  ngOnInit() {
    this.list_notifications()
  }

}
