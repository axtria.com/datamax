import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { HttpParams } from '@angular/common/http';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-new-announcements',
  templateUrl: './new-announcements.component.html',
  styleUrls: ['./new-announcements.component.css']
})
export class NewAnnouncementsComponent implements OnInit {

  recepientsList: any = []
  ListRecepients: any = [];
  selectedAnnouncements: any
  recepients: any
  Subject: any
  description: any
  i18Labels: any;
  announcementsList: any = ['Outage', 'New Release', 'Content', 'Miscellaneous']




  constructor(private rs: RestService, private route: ActivatedRoute, private router: Router, private ts: ToastPopupService, cs: ContentService) { 
    this.i18Labels = cs.getContent(environment.modules.admin).announcements;

  }

  send_notifications() {
    const params = new HttpParams({
      fromObject: {
        name: this.selectedAnnouncements,
        details: this.description,
        title: this.Subject,
        all_users: (this.recepients.includes(-1)),
        usergroup_ids: this.recepients.join(',')
      }
    });

    this.rs.postData(environment.notifications.send_notifications, {}, params).subscribe(() => {
        this.router.navigate(['announcements'], { relativeTo: this.route.parent });
        this.ts._exception.emit(this.ts.getToastSuccessMessage('Announcement has been sent successfully'));

      })
  }

  ngOnInit() {
    this.rs.getData(environment.groups.LIST_GROUPS, {
      userdetail: false,
      group_id: null,
      accessdetail: false,
      is_deleted: false
    }).subscribe(listgroups => {
      this.ListRecepients = listgroups && listgroups.Contents || [];
      this.ListRecepients.unshift({ usergroup_id: -1, name: 'All Users' });
    });
  }

}
