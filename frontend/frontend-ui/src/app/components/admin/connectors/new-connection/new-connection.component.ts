import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { ConnectorService } from '../connectors.service';
import { ContentService } from 'src/app/shared/services/content.service';


declare var $: any;

@Component({
  selector: 'app-new-connection',
  templateUrl: './new-connection.component.html',
  styleUrls: ['./new-connection.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class NewConnectionComponent implements OnInit {
  public connection_name: any
  public description: any
  public access_key: any
  public secret_key: any
  public username: any
  public password: any
  public account: any
  public database: any
  public role_name: any;
  public warehouse: any;
  public connector_name: any
  public usernameSftp: any
  public port: any
  public host: any
  public type: any = '';
  roleDisable: boolean;
  access: boolean;
  @ViewChild('connectionForm', { static: false }) connectionForm: NgForm;
  edit_data: any;
  connection_id: any;
  i18Labels: any;
  testConnectionSuccess: boolean = false;



  constructor(private connection_call_Service: RestService, private ts: ToastPopupService,
    private route: ActivatedRoute, private router: Router, public connectorService: ConnectorService, cs: ContentService) {
    this.i18Labels = cs.getContent(environment.modules.admin).Connectors;

  }

  connection(x, y) {
    this.type = y;

    $('.selected_connection').removeClass('selected_connection');
    $(x).addClass('selected_connection');

    this.connectionForm && this.connectionForm.reset();
    this.testConnectionSuccess = false;

  }




  disableField(x) {
    if (x == 'r') {
      this.access = true
      this.secret_key = null
      this.access_key = null
    }
    else {
      this.roleDisable = true
      this.role_name = null
    }

  }

  enableField(y) {
    if (y == 'r') {
      this.access = false

    }
    else {
      this.roleDisable = false;

    }
  }

  add_update_connection(isAddFlag) {

    let connectorJson = this.connectorService.getConnectorJSON(this.type, isAddFlag, this.secret_key, 
      this.role_name, this.description, this.connection_name, this.access_key,
      this.password, this.database, this.account, this.username, this.warehouse, 
      this.host, this.usernameSftp, this.port, this.connection_id);

    if(this.type == 's3' && isAddFlag == 'T') {
      this.ts._exception.emit(this.ts.getToastSuccessMessage(connectorJson.response));
      this.testConnectionSuccess = true;
    } else {
      this.connection_call_Service.getData(connectorJson.url, connectorJson.data).subscribe(() => {
        this.ts._exception.emit(this.ts.getToastSuccessMessage(connectorJson.response));
        if(isAddFlag == 'T')
          this.testConnectionSuccess = true;
        else
          this.router.navigate(['connectors'], { relativeTo: this.route.parent });
      });

    }

    
  }

  encrypt() {
    let key = this.password;
    const params = new HttpParams({
      fromObject: { 'q': key }
    });
    this.connection_call_Service.postData(environment.connectors.encrypt, {}, params).subscribe((response: any) => {
      if (response) {
        this.password = response.encrypted;
      }
    });
    
  }

  ngOnInit() {
    this.edit_data = this.connectorService.connectorInfo
    if (this.edit_data != null) {
      this.type = this.edit_data.connection_type
      this.connection_name = this.edit_data.name
      this.description = this.edit_data.description
      this.connection_id = this.edit_data.id

      this.access_key = this.edit_data.connection.ACCESS_KEY
      this.secret_key = this.edit_data.connection.SECRET_KEY

      this.role_name = this.edit_data.connection.ROLE

      this.username = this.edit_data.connection.USER
      this.password = this.edit_data.connection.PASSWORD
      this.account = this.edit_data.connection.ACCOUNT_ID
      this.database = this.edit_data.connection.DATABASE
      this.warehouse = this.edit_data.connection.WAREHOUSE

      this.usernameSftp = this.edit_data.connection.USERNAME
      //this.passwordSftp = this.edit_data.connection.PASSWORD

      this.host = this.edit_data.connection.HOST
      this.port = this.edit_data.connection.PORT

      if (this.role_name != 'null' && this.role_name != '' && this.type === 's3')
        this.disableField('r');
      else
        this.disableField('ak');
    }

  }

}
