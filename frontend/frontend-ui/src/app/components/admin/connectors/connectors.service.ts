import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class ConnectorService {
    public connectorInfo;
    CONNECTOR_TYPES = {
        's3': { type: 's3', name: 'S3', imagePath: 'assets/images/admin_icons/S3.svg' },
        'sf': { type: 'sf', name: 'Snowflake', imagePath: 'assets/images/admin_icons/Snowflake.svg' },
        'sftp': { type: 'sftp', name: 'SFTP', imagePath: 'assets/images/sftp.svg' },
        'rs': { type: 'rd', name: 'Redshift', imagePath: 'assets/images/admin_icons/redshift.svg' }
    };
    constructor() { }

    getConnectorJSON(type, operation, secret_key, role_name, description, connection_name, access_key,
        password, database, account, username, warehouse, host, usernameSftp, port, connection_id) {
        let _data, _url;
        
        if (type == 's3') {
            _data = {
                'rolename': role_name,
                'secret_key': secret_key,
                'access_key': access_key
            };
            if(operation == 'A')
                _url = environment.connectors.add_s3_connection;
            else if(operation == 'E')
                _url = environment.connectors.update_s3_connection;
            else if(operation == 'T')
                _url = environment.connectors.test_s3_connection;

        } else if (type == 'sf') {
            _data = {
                'password': password,
                'database': database,
                'account_id': account,
                'user': username,
                'warehouse': warehouse
            };

            if(operation == 'A')
                _url = environment.connectors.add_snowflake_connection;
            else if(operation == 'E')
                _url = environment.connectors.update_snowflake_connection;
            else if(operation == 'T')
                _url = environment.connectors.test_snowflake_connection;

        } else if (type == 'sftp') {
            _data = {
                'password': password,
                'host': host,
                'user': usernameSftp,
                'port': port
            };

            if(operation == 'A')
                _url = environment.connectors.add_sftp_connection;
            else if(operation == 'E')
                _url = environment.connectors.update_sftp_connection;
            else if(operation == 'T')
                _url = environment.connectors.test_sftp_connection;

        }

        if(operation == 'E')
            _data['connector_id'] = connection_id;
    
        if(operation != 'T') {
            _data['c_name'] = connection_name;
            _data['description'] = description;
        }

        let _response = '';
        if(operation == 'T')
            _response = "Test Connection successful";
        else if(operation == 'A')
            _response = "New connection has been added successfully";
        else if(operation == 'E')
            _response = "Connection has been updated successfully";
        
        return {
            data: _data,
            url: _url,
            response: _response
        }
    }

}