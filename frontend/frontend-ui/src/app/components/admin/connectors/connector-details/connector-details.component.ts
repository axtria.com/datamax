import { Component, OnInit, ViewChild } from '@angular/core';
import { ConnectorService } from '../connectors.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitType } from '@syncfusion/ej2-base';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-connector-details',
  templateUrl: './connector-details.component.html',
  styleUrls: ['./connector-details.component.css']
})
export class ConnectorDetailsComponent implements OnInit {
  _connectorInfo: any;
  public pageSettings = { pageSize: 8 };
  public animationSettings: Object = { effect: 'Zoom' };

  layers= [];
  @ViewChild('confirmDialog', { static: false }) public confirmDialog: DialogComponent;
  i18Labels: any;


  constructor(public connectorDetails: ConnectorService, private restService: RestService,
    private ts: ToastPopupService, private route: ActivatedRoute, private router: Router, cs: ContentService) {
      this.i18Labels = cs.getContent(environment.modules.admin).Connectors;

     }


    public modalBtnClick: EmitType<object> = () => {
    
      this.confirmDialog.show();
  
      
    }
  
    public modalDlgOpen: EmitType<object> = () => {
      document.getElementById('modalbtn').style.display = 'none';
    }
  
    public modalDlgClose: EmitType<object> = () => {
  
      document.getElementById('modalbtn').style.display = '';
      
  
    }
    
  deleteConnection(id) {
    this.restService
      .getData(environment.connectors.delete_connection,
        {
          'connection_id': id
        })
      .subscribe(() => {

        this.ts._exception.emit(this.ts.getToastSuccessMessage('Connection has been removed successfully'));
        this.router.navigate(['connectors'], { relativeTo: this.route.parent });
        this.confirmDialog.hide();
      });

  }

  dont_delete_connection(){
    this.confirmDialog.hide();
  }

  ngOnInit() {
    this._connectorInfo = this.connectorDetails.connectorInfo;

    this._connectorInfo && this._connectorInfo.id && this.restService
      .getData(environment.admin.get_connector_layers,
        {
          'connection_id': this._connectorInfo.id
        })
      .subscribe((data) => {
        this.layers = data.Contents || [];

      });

  }

}
