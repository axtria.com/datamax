import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectorService } from './connectors.service';
import { ToastPopupService } from '../../toast-popup/toast-popup.service';
import { EmitType } from '@syncfusion/ej2-base';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-connectors',
  templateUrl: './connectors.component.html',
  styleUrls: ['./connectors.component.css']
})
export class ConnectorsComponent implements OnInit {
  connectorList: any;
  public pageSettings = { pageSize: 7 };
  public toolbarOptions: object[]; rowData;
  public sortOptions: object = { columns: [{ field: 'modified_on', direction: 'Descending' }] };
  public animationSettings: Object = { effect: 'Zoom' };

  @ViewChild('confirmDialog', { static: false }) public confirmDialog: DialogComponent;


  public modalBtnClick: EmitType<object> = (e) => {
    this.row_data = e
    this.confirmDialog.show();

    
  }

  public modalDlgOpen: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = 'none';
  }

  public modalDlgClose: EmitType<object> = () => {

    document.getElementById('modalbtn').style.display = '';
    

  }
  row_data: object;
  i18Labels: any;
  


  connector_list() {
    this.restService.getData(environment.connectors.connectors, {}).subscribe((data) => {
      this.connectorList = data && data.Contents || [];
      
    });
  }

  connectionDetails(_data) {

    this.connectorService.connectorInfo = _data;
    this.router.navigate(['connectorsdetails'], { relativeTo: this.route.parent });
  }

  newConnection() {
    this.connectorService.connectorInfo = null;
    this.router.navigate(['newconnection'], { relativeTo: this.route.parent });
  }

  editConnection(data){
    
    this.connectorService.connectorInfo = data;
    this.router.navigate(['newconnection'], { relativeTo: this.route.parent });


  }

  constructor(private route: ActivatedRoute, private router: Router,
    public connectorService: ConnectorService, private restService: RestService,
    private ts: ToastPopupService, cs: ContentService) {
      this.i18Labels = cs.getContent(environment.modules.admin).Connectors;

     }

  deleteConnection() {
    this.restService
      .getData(environment.connectors.delete_connection,
        {
          'connection_id': this.row_data
        })
      .subscribe(() => {

        this.ts._exception.emit(this.ts.getToastSuccessMessage('Connection has been removed successfully'));
        this.connector_list()
        this.confirmDialog.hide();


      });

  }

  dont_delete_connection(){
    this.confirmDialog.hide();
  }

  ngOnInit() {
    this.connector_list()
    this.toolbarOptions = [{ text: 'Search', align: 'Left' }];

  }

}
