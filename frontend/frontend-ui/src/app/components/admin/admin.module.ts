import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { SyncfusionModule } from 'src/app/shared/modules/syncfusion.module';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { AdminComponent } from './admin.component';
// import { GroupDetailComponent } from './groups/group-detail/group-detail.component';
import { RequestsComponent } from './requests/requests.component';
import { ConnectorsComponent } from './connectors/connectors.component';
import { ConnectorDetailsComponent } from './connectors/connector-details/connector-details.component';
import { NewConnectionComponent } from './connectors/new-connection/new-connection.component';
import { LayersComponent } from './layers/layers.component';
import { CreateNewLayerComponent } from './layers/create-new-layer/create-new-layer.component';
import { LayerDetailsComponent } from './layers/layer-details/layer-details.component';
import { AnnouncementsComponent } from './announcements/announcements.component';
import { NewAnnouncementsComponent } from './announcements/new-announcements/new-announcements.component';
import { ConnectorService } from './connectors/connectors.service';
import { AdminTopnavComponent } from './admin-topnav/admin-topnav.component';
import { CreateNewGroupComponent } from './groups/create-new-group/create-new-group.component';
import { GroupsComponent } from './groups/groups.component';
import { UsersComponent } from './users/users.component';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { GroupDetailComponent } from './groups/group-detail/group-detail.component';

@NgModule({
  declarations: [
    AdminComponent,
    // GroupDetailComponent,
    RequestsComponent,
    ConnectorsComponent,
    ConnectorDetailsComponent,
    NewConnectionComponent,
    LayersComponent,
    CreateNewLayerComponent,
    LayerDetailsComponent,
    AnnouncementsComponent,
    NewAnnouncementsComponent,
    AdminTopnavComponent,
    CreateNewGroupComponent,
    GroupsComponent,
    UsersComponent,
    UserDetailsComponent,
    GroupDetailComponent
  ],
  imports: [
    SyncfusionModule,
    AdminRoutingModule
  ],
  providers: [AuthenticateService, ConnectorService]
})
export class AdminModule { }
