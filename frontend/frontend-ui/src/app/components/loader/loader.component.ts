import { Component, OnInit } from '@angular/core';
import { createSpinner, showSpinner, hideSpinner } from '@syncfusion/ej2-angular-popups';
import { LoaderService } from './loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  showsubsVar: any;hidesubsVar: any;

  constructor(ls: LoaderService) { 
    this.showsubsVar = ls._spinnerShow.subscribe(
      () => {
        !ls.disableLoader && this.showSpinner();
      });
    this.hidesubsVar = ls._spinnerHide.subscribe(
      () => {
        this.hideSpinner();
      });
  }

  ngOnInit() {
    //createSpinner() method is used to create spinner
    createSpinner({
      target: document.getElementById('container')
    });
  }

  showSpinner() {
    showSpinner(document.getElementById('container'));
    let ele: any = document.getElementsByClassName('custom-rolling')[0];
    ele.style.display = 'block';
  }

  hideSpinner(){
    hideSpinner(document.getElementById('container'));
    let ele: any = document.getElementsByClassName('custom-rolling')[0];
    ele.style.display = 'none';
  }

  ngOnDestroy() {
    if (this.showsubsVar) {
      this.showsubsVar.unsubscribe()
    }
    if (this.hidesubsVar) {
      this.hidesubsVar.unsubscribe()
    }
  }
  
}
