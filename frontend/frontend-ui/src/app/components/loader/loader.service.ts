import { Injectable, EventEmitter } from '@angular/core';
@Injectable()
export class LoaderService {
    _spinnerShow = new EventEmitter<any>();
    _spinnerHide = new EventEmitter<any>();
    disableLoader: boolean = false;
}