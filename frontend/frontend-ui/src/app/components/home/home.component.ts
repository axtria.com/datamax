import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public displaySidePanel: boolean = true;

  constructor(public router: Router, private storageService: StorageService) {
    const currentRoute = this.router.url;
    if (currentRoute == '/landing') {
      this.storageService.setSessionItem('view', 'new');
    }
    if (currentRoute.startsWith('/landing')) {
      this.displaySidePanel = false;
    }
  }

  ngOnInit() {
  }

}
