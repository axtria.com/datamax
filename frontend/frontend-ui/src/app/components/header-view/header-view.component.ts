import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { config } from 'src/environments/config';


@Component({
  selector: 'app-header-view',
  templateUrl: './header-view.component.html',
  styleUrls: ['./header-view.component.css']
})
export class HeaderViewComponent implements OnInit {
  public headerContent: any;
  imagePath = config.imagesPath; 
  totalNotificationCount: any;
  
  constructor(public authenticateService: AuthenticateService, cs: ContentService,
    private rs: RestService, private router: Router) {
    this.headerContent = cs.getContent("Landing");
   }
  ngOnInit() {
    this.rs.getDataWithoutAlert(environment.landing.get_notifications, {}).subscribe(data => {
      this.totalNotificationCount =  data.Contents.length;

    });
    
  }

  logOut() {
    sessionStorage.clear();
    localStorage.clear();
    this.router.navigate(['/'])
    .then(() => {
      window.location.reload();
    });
  }

 /*  openAdminSection() {
    // this.tooltipControl.close();
    this.storageService.removeSessionItem('view');
  } */
  clearSession(){
    sessionStorage.clear();
    //this.router.navigate(['/landing/myprofile']);
  }

}
