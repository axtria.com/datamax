import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-landing-view',
  templateUrl: './landing-view.component.html',
  styleUrls: ['./landing-view.component.css']
})
export class LandingViewComponent implements OnInit {
  
  public landingViewContent: any;
  public requestAccess: boolean = false;
  notifications: any = [];
  displayNotifications: any = [];
  totalNotificationCount: any;


  constructor(cs: ContentService, private rs: RestService,
    public storageService: StorageService, private utilService: UtilService,
    private router: Router) {
    this.landingViewContent = cs.getContent("Landing");
   }

   ngOnInit() {
    let property = 'ui_event_name';
    this.rs.getDataWithoutAlert(environment.landing.get_notifications, {}).subscribe(data => {
      this.notifications = data.Contents;
      this.displayNotifications = this.utilService.getArrayGroupByKey(this.notifications, property);
      this.totalNotificationCount = this.notifications.length;

    });
  }

  openState(state) {
    this.storageService.removeSessionItem('view');
    this.router.navigate([this.router.url , state]);
  }

  openRequestAccess() {
    this.requestAccess = true;
  }

}
