import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalConstants } from 'src/app/shared/common/global-constants';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-sidebar-view',
  templateUrl: './sidebar-view.component.html',
  styleUrls: ['./sidebar-view.component.css']
})
export class SidebarViewComponent implements OnInit {
  public navbarOpen = false;

  constructor(private router: Router, public authenticateService: AuthenticateService) { }

  ngOnInit() {}
 
  getTitle() {
    return GlobalConstants.siteTitle;
  }

  
  showHome() {
    const currentRoute = this.router.url;
    return (currentRoute == '/landing');
  }

  expandSidebar() {
    this.navbarOpen = !this.navbarOpen;
  }

}
