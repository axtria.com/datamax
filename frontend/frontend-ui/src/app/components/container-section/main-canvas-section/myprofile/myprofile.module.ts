import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyprofileRoutingModule } from './myprofile-routing.module';
import { MyprofileComponent } from './myprofile.component';
import { RequestsComponent } from './requests/requests.component';
import { SyncfusionModule } from 'src/app/shared/modules/syncfusion.module';
import { ProfileTopNavComponent } from './profile-top-nav/profile-top-nav.component';
import { ViewRequestsComponent } from './view-requests/view-requests.component';



@NgModule({
  declarations: [MyprofileComponent, RequestsComponent, ProfileTopNavComponent, ViewRequestsComponent],
  imports: [
    CommonModule,
    MyprofileRoutingModule,
    SyncfusionModule

  ]
})
export class MyprofileModule { }
