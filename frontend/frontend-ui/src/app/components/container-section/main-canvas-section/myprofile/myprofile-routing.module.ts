import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyprofileComponent } from './myprofile.component';
import { RequestsComponent } from 'src/app/components/container-section/main-canvas-section/myprofile/requests/requests.component';

const routes: Routes = [{ path: '', component: MyprofileComponent },
{ path : 'requests', component: RequestsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyprofileRoutingModule { }
