import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-profile-top-nav',
  templateUrl: './profile-top-nav.component.html',
  styleUrls: ['./profile-top-nav.component.css']
})
export class ProfileTopNavComponent implements OnInit {
  key: any;
  searchtext: string; 
  profilecontent: any;

  constructor(private route: ActivatedRoute, private router: Router, cs:ContentService) { 
    this.profilecontent = cs.getContent(environment.modules.myprofile).profiletopnav;

  }

  ngOnInit() {
    
  }

  openState(state) {
    this.key = state;
    this.router.navigate([state], { relativeTo: this.route.parent });
  }

}
