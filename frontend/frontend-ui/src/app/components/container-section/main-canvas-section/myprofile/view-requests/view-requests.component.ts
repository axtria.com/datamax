import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { forkJoin } from 'rxjs';
import { TreeView, DrawNodeEventArgs } from '@syncfusion/ej2-navigations';
import { ContentService } from 'src/app/shared/services/content.service';
import { GroupsService } from 'src/app/components/admin/groups/groups.service';
import { ListBoxComponent } from '@syncfusion/ej2-angular-dropdowns';


@Component({
  selector: 'app-view-requests',
  templateUrl: './view-requests.component.html',
  styleUrls: ['./view-requests.component.css']
})
export class ViewRequestsComponent implements OnInit {
  //public layersDropDownList: any = [];
  public functionality_list: any = [];
  public newRequestLabel = 'New Access'
  public updateRequestLabel = 'Update Access';
  public displayLabel: any;
  public userInfo = this.authenticateService.getUserInfo();
  field: { dataSource: any; id: string; text: string; child: string; };
  nodeIdAll: any[];
  viewRequest: any;
  public selection = { showCheckbox: true };
  @Output() closeModalEvent = new EventEmitter<boolean>();
  @ViewChild('treeView', { static: false }) public treeView: TreeView;
  @Input('requestviewlist') rowData: any;
  @ViewChild('layerslist', { static: false }) public layerslist: ListBoxComponent;

  constructor(public authenticateService: AuthenticateService, private rs: RestService, private groupService: GroupsService, cs: ContentService) {
    this.viewRequest = cs.getContent(environment.modules.myprofile).viewRequest;
  }
  
  application() {
    this.treeView.checkedNodes = this.nodeIdAll.map(String);

  }
  public drawNode(args: DrawNodeEventArgs): void {
    let ele: HTMLElement = args.node.querySelector('.e-checkbox-wrapper');
    ele.classList.add('e-checkbox-disabled');
  }

  ngOnInit() {
    this.functionality_list = this.rowData['moduleroles'];
    this.functionality_list = this.groupService.getFunctionalityTreeJSON(this.functionality_list, true, true);


    this.field = { dataSource: this.functionality_list, id: 'nodeId', text: 'nodeText', child: 'roles' };

    let nodeIdAll = []
    this.functionality_list.forEach(e => {
      e['roles'].forEach(e1 => { nodeIdAll.push(e1['nodeId']) })
    });
    this.nodeIdAll = nodeIdAll;


    /* let fetch_all_layers = this.rs.getData(environment.layers.fetch_all_layers, {
      is_active: true
    }); */
    let get_modules = this.rs.getData(environment.groups.list_functionality, {});
    forkJoin([get_modules]).subscribe(data => {
      //this.layersDropDownList = data[0].Contents || [];
      this.functionality_list = data[0].Contents || [];
    });
    
  }

  onCloseModal(){
    this.closeModalEvent.emit(false);  
   }

   created() {
     this.layerslist.selectAll();
   }

}
