import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RequestsComponent implements OnInit {
  public textWrapSettings: Object = { wrapMode: "both" };
  pageSettings = { pageSize: 8 };
  gridPageSettings: Object;
  currentPage = 1;
  requestList: any;
  user_id: any;
  requestviewlist: any=[];
  public viewRequest: boolean = false;
  // public animationSettings: Object = { effect: 'Zoom' };
  public sortOptions: object = { columns: [{ field: 'requested_date', direction: 'Descending' }] };
  public toolbarOptions: object[] = [{type: 'Input', template: '#searchEle', align: 'Right'}];
  public allowPaging: boolean = true;

  rowData: any;
  requestcontent: any;
  constructor(private rs: RestService, private authService: AuthenticateService, cs: ContentService ) {
    this.requestcontent = cs.getContent(environment.modules.myprofile).requests;

   }
  @Input('searchtext') searchtext: string; 



  ngOnInit() {
    this.rs.getData(environment.admin.get_access_request, {
      userid: this.authService.getUserInfo().user_id,
      requestid: null
    }).subscribe((data) => {
      this.requestList = data && data.Contents || [];
    });
    this.gridPageSettings = { pageSizes: true, pageSize:5, currentPage: this.currentPage, template: ''};
  }

  openViewPanel(data) {
    this.rowData= data;
    this.viewRequest = true;
  }

  onClose(isVisible: boolean){
    this.viewRequest = isVisible;
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
  }

}
