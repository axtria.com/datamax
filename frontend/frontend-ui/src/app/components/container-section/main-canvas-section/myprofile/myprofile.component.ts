import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {
  i18Labels: any;

  constructor(cs: ContentService) { 
    this.i18Labels = cs.getContent(environment.modules.myprofile);
  }

  ngOnInit() {
  }

}
