import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileTopNavComponent } from './profile-top-nav.component';

describe('ProfileTopNavComponent', () => {
  let component: ProfileTopNavComponent;
  let fixture: ComponentFixture<ProfileTopNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileTopNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileTopNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
