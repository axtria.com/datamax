import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { StorageDetails } from 'src/app/components/container-section/main-canvas-section/data-onboarding/StorageDetails';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-data-storage-details',
  templateUrl: './data-storage-details.component.html'
})
export class DataStorageDetailsComponent implements OnInit {

  @ViewChild('txtStandardFileName', { static: false }) FileNamingPattern: any;

  @Input() objId: number;
  @Input() editparam: boolean;
  @Input() filePattern: string;
  @Input() fileFrequency: string;
  @Input() fileArvlStartDay: number;
  @Input() fileArvlEndDay: number;
  @Input() fileArvltime: string;

  dataStorageContent: any;
  storageTypes: any;
  storageTypesI: any;
  storageTypesL: any;
  storageTypesA: any;
  iStoragePaths: any;
  lStoragePaths: any;
  aStoragePaths: any;
  connFlags: any;
  fileFrequencyList: any[];
  weekDayNumList: any[];
  defaultTimeInterval = 30;
  freqInitial = false;
  timePickerFormat = 'HH:mm';
  systemUserId: string = null;
  systemUserType: string = null;
  fileArrivalTimeVal: string = null;
  fileNamingPattern: string = null;
  controlFilesList: any[];
  controlFileId: string = null;
  showInboundPathTextbox: boolean = false;
  showArchivalPathTextbox: boolean = false;
  showLandingPathTextbox: boolean = false;
  othersStoragePath: object = {Path: "Others"};

  inboundDetails: StorageDetails;
  landingDetails: StorageDetails;
  archivalDetails: StorageDetails;
  dataStorageDetails: StorageDetails;

  public sourceStorageFields: object = { text: 'name', value: 'layer_id' };
  public sourceStoragePathFields: object = { text: 'Path', value: 'Path' };
  fileFrequencyFields: object = { value: 'freq_val' };
  weekDayNumFields: object = { text: 'week_day', value: 'week_num' };
  controlFileFields: object = {text:'obj_nm', value: 'obj_id'};

  constructor(private authenticateService: AuthenticateService, private dataService: RestService, cs: ContentService) {
    this.dataStorageContent = cs.getContent(environment.modules.dataOnboarding).dataStorage;
    this.connFlags = this.dataStorageContent.connFlags;
    this.fileFrequencyList = this.dataStorageContent.fileFrequencyList;
    this.weekDayNumList = this.dataStorageContent.weekDayNumList;
  }

  ngOnInit() {
    this.dataStorageDetails = {
      obj_data_layer_id: null, obj_id: null, data_layer_id: null, user_id: this.systemUserId, user_type: this.systemUserType,
      abslt_path: null, layer_flg: this.connFlags[0], eff_srt_dt: null, eff_end_dt: null, is_active: null,
      file_frequency: "Adhoc", file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null
    };

    this.inboundDetails = {
      obj_data_layer_id: null, obj_id: null, data_layer_id: null, user_id: this.systemUserId, user_type: this.systemUserType,
      abslt_path: null, layer_flg: this.connFlags[0], eff_srt_dt: null, eff_end_dt: null, is_active: null,
      file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null
    };

    this.landingDetails = {
      obj_data_layer_id: null, obj_id: null, data_layer_id: null, user_id: this.systemUserId, user_type: this.systemUserType,
      abslt_path: null, layer_flg: this.connFlags[1], eff_srt_dt: null, eff_end_dt: null, is_active: null,
      file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null
    };

    this.archivalDetails = {
      obj_data_layer_id: null, obj_id: null, data_layer_id: null, user_id: this.systemUserId, user_type: this.systemUserType,
      abslt_path: null, layer_flg: this.connFlags[2], eff_srt_dt: null, eff_end_dt: null, is_active: null,
      file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null
    };

    this.dataService.getData(environment.onboarding.storage.getControlFile, { name: 'get_ctrl_files'})
      .subscribe(data => {
        this.controlFilesList = data && data.Contents ? data.Contents : [];
      });

    this.dataService.getData(environment.onboarding.storage.getStorageDetails, { name: 'get_data_layers', layer_type: 'S3' })
      .subscribe(data => {
        this.storageTypes = data && data.Contents ? data.Contents : [];

        this.storageTypesI = this.storageTypes.filter(function (item: any) {
          if (item.layer_purpose == 'Inbound') {
            return item;
          }
        });
        this.storageTypesL = this.storageTypes.filter(function (item: any) {
          if (item.layer_purpose == 'Landing') {
            return item;
          }
        });
        this.storageTypesA = this.storageTypes.filter(function (item: any) {
          if (item.layer_purpose == 'Archive') {
            return item;
          }
        });

        if (this.editparam) {
          this.fileNamingPattern = this.filePattern;
          this.dataStorageDetails.file_frequency = this.fileFrequency ? this.fileFrequency : "Adhoc";
          
          this.dataStorageDetails.file_arrival_start_day = this.fileArvlStartDay;
          this.dataStorageDetails.file_arrival_end_day = this.fileArvlEndDay;
          this.fileArrivalTimeVal = this.fileArvltime;
          this.dataService.getData(environment.onboarding.connection.getSourceConnectionDetails,
            { name: 'get_source_connection_details', object_id: this.objId }).subscribe(
              element => {
                if (element && element.Contents.length) {
                  this.connFlags.forEach(dataElement => {
                    let conn = element.Contents.find(item => item.layer_flg.toUpperCase() === dataElement),
                        _obj;
                    if(conn !== undefined) {
                       _obj = {
                        obj_id: conn.obj_id,
                        data_layer_id: conn.data_layer_id,
                        user_id: conn.user_id,
                        user_type: conn.user_type,
                        abslt_path: conn.abslt_path,
                        layer_flg: conn.layer_flg,
                        obj_data_layer_id: conn.obj_data_layer_id,
                        eff_srt_dt: conn.eff_srt_dt,
                        eff_end_dt: conn.eff_end_dt,
                        is_active: conn.is_active,
                        file_frequency: null,
                        file_arrival_start_day: null,
                        file_arrival_end_day: null,
                        file_arrival_time: null
                      };
                    } else {
                      _obj = {
                        obj_id: null,
                        data_layer_id: null,
                        user_id: null,
                        user_type: null,
                        abslt_path: null,
                        layer_flg: null,
                        obj_data_layer_id: null,
                        eff_srt_dt: null,
                        eff_end_dt: null,
                        is_active: null,
                        file_frequency: null,
                        file_arrival_start_day: null,
                        file_arrival_end_day: null,
                        file_arrival_time: null
                      };
                    }
                   
                    if (dataElement === this.connFlags[0]) {
                      this.inboundDetails = _obj;
                    }
                    if (dataElement === this.connFlags[1]) {
                      this.landingDetails = _obj;
                    }
                    if (dataElement === this.connFlags[2]) {
                      this.archivalDetails = _obj;
                    }
                  });
                }
              });
        }
      });
  }

  LayerSelected(e: any, lyrTyp: string) {
    if (e.itemData !== null) {
      this.dataService.getData(environment.onboarding.storage.getStoragePaths, {
        layer_id: e.itemData.layer_id, category: 'Folders'
      })
        .subscribe(data => {
          if (lyrTyp === this.connFlags[0]){
            this.iStoragePaths = data && data.Contents ? data.Contents : [];
            this.iStoragePaths = this.iStoragePaths.concat(this.othersStoragePath);
          }
          if (lyrTyp === this.connFlags[1]) {
            this.lStoragePaths = data && data.Contents ? data.Contents : [];
            this.lStoragePaths = this.lStoragePaths.concat(this.othersStoragePath);
          }
          if (lyrTyp === this.connFlags[2]){
            this.aStoragePaths = data && data.Contents ? data.Contents : [];
            this.aStoragePaths = this.aStoragePaths.concat(this.othersStoragePath);
          }
        });
    }
  }

  inboundPathChange(e: any) {
    this.showInboundPathTextbox= (e.itemData.Path == "Others");
  }
  landingPathChange(e: any) {
    if(!this.editparam)
    this.showLandingPathTextbox= (e.itemData.Path == "Others");
  }
  archivalPathChange(e: any) {
    this.showArchivalPathTextbox= (e.itemData.Path == "Others");
  }

  onInboundPathChangeComplete() {
    this.inboundDetails.abslt_path = this.inboundDetails.abslt_path == "Others" ? '' : this.inboundDetails.abslt_path;
  }
  onLandingPathChangeComplete() {
    this.landingDetails.abslt_path = this.landingDetails.abslt_path == "Others" ? '' : this.landingDetails.abslt_path;
  }
  onArchivalPathChangeComplete() {
    this.archivalDetails.abslt_path = this.archivalDetails.abslt_path == "Others" ? '' : this.archivalDetails.abslt_path;
  }

  onControlFileSelection(e) {
      this.controlFileId = e.value;
  }

  onFileFrequencyChange() {
    if (this.editparam && !this.freqInitial) {
      this.freqInitial = true;
    } else {
      this.dataStorageDetails.file_arrival_time = null;
      this.dataStorageDetails.file_arrival_start_day = null;
      this.dataStorageDetails.file_arrival_end_day = null;
    }
  }

  fileArrivalTimeChange(e) {
    this.dataStorageDetails.file_arrival_time = e.text;
  }

  getSchedulingStatus(obj_id) {
    return this.dataService.getData(environment.commonUtilities.getSchedulingStatus, {
      name: 'schedule_status',
      obj_id: obj_id
    });
  }

  saveStorageDetails(objId, objURIid) {
    const time = 'current_timestamp';
    const date = 'current_date';
    const is_active = true;

    const connDetailsParam = '(' + 'data_layer_seq' + '|' + objId + '|' + this.inboundDetails.data_layer_id + '|\'' + objURIid + '\'|\'' +
      this.inboundDetails.user_id + '\'|\'' + this.inboundDetails.user_type + '\'|\'' + this.inboundDetails.abslt_path + '\'|\'' + this.inboundDetails.layer_flg +
      '\'|' + date + '|\'' + '9999-12-31' + '\'|' + is_active + '|\'' + 'UI00100' + '\'|' + time + '|\'' + this.authenticateService.getUserInfo().user_id + '\'' + ')' + '^'
      + '(' + 'data_layer_seq' + '|' + objId + '|' + this.landingDetails.data_layer_id + '|\'' + objURIid + '\'|\'' +
      this.landingDetails.user_id + '\'|\'' + this.landingDetails.user_type + '\'|\'' + this.landingDetails.abslt_path + '\'|\'' + this.landingDetails.layer_flg +
      '\'|' + date + '|\'' + '9999-12-31' + '\'|' + is_active + '|\'' + 'UI00100' + '\'|' + time + '|\'' + this.authenticateService.getUserInfo().user_id + '\'' + ')' + '^'
      + '(' + 'data_layer_seq' + '|' + objId + '|' + this.archivalDetails.data_layer_id + '|\'' + objURIid + '\'|\'' +
      this.archivalDetails.user_id + '\'|\'' + this.archivalDetails.user_type + '\'|\'' + this.archivalDetails.abslt_path + '\'|\'' + this.archivalDetails.layer_flg +
      '\'|' + date + '|\'' + '9999-12-31' + '\'|' + is_active + '|\'' + 'UI00100' + '\'|' + time + '|\'' + this.authenticateService.getUserInfo().user_id + '\'' + ')'
    return this.dataService.getData(environment.onboarding.storage.saveStorage, { name: 'object_data_layer_meta', old_obj_uri_id: null, layer_list: connDetailsParam });
  }

  updateStorageDetails(oldObjUriId, objURIid) {
    const connDetailsParam =  + this.inboundDetails.obj_data_layer_id + '|' + this.inboundDetails.data_layer_id + '|\'' + objURIid + '\'|\'' +
      this.inboundDetails.user_id + '\'|\'' + this.inboundDetails.user_type + '\'|\'' + this.inboundDetails.abslt_path + '\'|\'' + this.inboundDetails.layer_flg +
      '\'|\'' + this.authenticateService.getUserInfo().user_id + '\''
      + '^' + this.landingDetails.obj_data_layer_id + '|'  + this.landingDetails.data_layer_id + '|\'' + objURIid + '\'|\'' +
      this.landingDetails.user_id + '\'|\'' + this.landingDetails.user_type + '\'|\'' + this.landingDetails.abslt_path + '\'|\'' + this.landingDetails.layer_flg +
      '\'|\'' + this.authenticateService.getUserInfo().user_id  + '\''
      + '^' + this.archivalDetails.obj_data_layer_id + '|'  + this.archivalDetails.data_layer_id + '|\'' + objURIid + '\'|\'' +
      this.archivalDetails.user_id + '\'|\'' + this.archivalDetails.user_type + '\'|\'' + this.archivalDetails.abslt_path + '\'|\'' + this.archivalDetails.layer_flg +
      '\'|\'' + this.authenticateService.getUserInfo().user_id  + '\'';

    return this.dataService.getData(environment.onboarding.storage.saveStorage, { name: 'object_data_layer_meta', old_obj_uri_id: oldObjUriId, layer_list: connDetailsParam});
  }
}
