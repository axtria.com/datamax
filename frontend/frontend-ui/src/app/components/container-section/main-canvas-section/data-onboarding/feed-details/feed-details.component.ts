import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { DataFeedDetails } from '../DataFeedDetails';
import { DropDownListComponent, ComboBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { RestService } from 'src/app/shared/services/rest.service';
import { NgForm } from '@angular/forms';
import { DialogComponent, ButtonPropsModel } from '@syncfusion/ej2-angular-popups';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';
import {DataFeedService} from './data-feed-details.service';
import { TextBoxComponent } from '@syncfusion/ej2-angular-inputs';

@Component({
  selector: 'app-feed-details',
  templateUrl: './feed-details.component.html',
  styleUrls: ['./feed-details.component.css']
})
export class FeedDetailsComponent implements OnInit {

  @Input() objId: number;
  @Input() dataFeedDetails: DataFeedDetails;
  @Input() editparam: boolean;
  @Input() cloneparam: boolean;

  fileUploadStatus: any;
  selectedFileName: string;
  fileTypeNameValid = true;
  connectionTypes: any[];
  freqInitial = false;
  editFeedDetails: any;
  dataFeedContent: any;
  onboardingContent: any;

  onboardOptions: string[];
  activeOnboardOption: string;
  layoutDisplayOptions: string[] = ['None', 'MasterObject', 'OnboardedObject', 'File'];
  activeLayoutDisplayOption: string;

  samples_file_upload_S3_layer_id = 85;
  folderPrefix = "OnboardingSampleFiles";

  defaultTimeInterval = 30;
  timePickerFormat = 'HH:mm';
  modalHidden = false;
  modalTarget = '.control-section';
  animationSettings: object = { effect: 'Zoom' };
  s3UploadmodalHidden = false;
  dlgButtons: ButtonPropsModel[];
  dlgButtonss3Upload: ButtonPropsModel[];
  ele: HTMLElement;
  
  @ViewChild('btnLibObj', { static: false }) btnLibObj: any;
  @ViewChild('btnNewObj', { static: false }) btnNewObj: any;
  @ViewChild('btnUpload', { static: false }) btnUpload: any;
  @ViewChild('dataProvider', { static: false }) dataProvider: DropDownListComponent;
  @ViewChild('dataProviderNameCB', { static: false }) dataProviderCB: ComboBoxComponent;
  @ViewChild('dataSetCB', { static: false }) dataSetCB: ComboBoxComponent;
  @ViewChild('dataSet', { static: false }) dataSet: DropDownListComponent;
  @ViewChild('fileType', { static: false }) fileType: DropDownListComponent;
  @ViewChild('fileExtension', { static: false }) fileExtension: DropDownListComponent;
  @ViewChild('fileColumnDelimiter', { static: false }) fileColumnDelimiter: DropDownListComponent;
  @ViewChild('dataFeedForm', { static: false }) dataFeedForm: NgForm;
  @ViewChild('Dialog', { static: false }) Dialog: DialogComponent;
  @ViewChild('s3UploadDialog', { static: false }) s3UploadDialog: DialogComponent;
  @ViewChild('fileUpload', { static: false }) FileUpload: any;
  @ViewChild('txtStandardFileName', { static: false }) FileNamingPattern: any;
  @ViewChild('columnHeader', { static: false }) FileColumnHeader: any;
  @ViewChild('grid', { static: false }) grid: GridComponent;
  @ViewChild('txtFileDescription', { static: false }) txtFileDescription: TextBoxComponent;

  @Output() dataFeedObjectSelected = new EventEmitter<any[]>();
  @Output() businessNameSelected = new EventEmitter<any[]>();

  dataProviderList: [];
  dataSetList: [];
  fileTypeList: [];
  fileFrequencyList: any[];
  weekDayNumList: any[];
  fileExtensionList: any[];
  fileHasHeaderList: any[];
  fileTextQualifierList: any[];
  fileEncodingList: any[];
  fileColumnDelimiterList: any[];
  fileNamingPattern: string = null;
  fileColumnDelemiter: string = null;
  fileColumnDelemiterRef: string = null;
  fileTextQualifierRef: string = null;
  fileArrivalTimeVal: string = null;
  fileColumnHeader: string = null;
  selectedFiles: FileList;
  sampleData: any[];
  sampleDataColumns: any[];
  isFileSelected: boolean = false;
  fileFormatOptions: string[];
  sourceTypeOptions: string[];
  fileColumnsName: string;
  fileDelimeter: string = null;
  fileHasDuplicateColumns: boolean = false;
  isFileHeaderProvided: boolean = false;
  isUploadSuccessFull = false;
  isUploading = false;
  isUploadConfirmed = false;
  otherProviderName : object = {obj_prvdr_nm: "Others"};
  otherDatasetName: object = {obj_src_nm: "Others"};

  dataProviderFields: object = { value: 'obj_prvdr_nm' };
  dataProviderCBFields: object = { value: 'obj_prvdr_nm' };
  dataSetFields: object = { value: 'obj_src_nm' };
  fileTypeFields: object = { text: 'obj_nm', value: 'mstr_obj_id' };
  fileFrequencyFields: object = { value: 'freq_val' };
  weekDayNumFields: object = { text: 'week_day', value: 'week_num' };
  fileExtensionFields: object = { value: 'ext_val' };
  fileColumnDelimiterFields: object = { text: 'del_txt', value: 'del_val' };
  fileHasHeaderFields: object = { text: 'header_txt', value: 'header_val' };
  fileTextQualifierFields: object = { text: 'qual_txt', value: 'qual_val' };
  fileEncodingFields: object = { value: 'encoding_type' };
  fileTypePattern: string;
  fwColumnHeaderPattern: string;
  delimitedColumnHeaderPattern: string;
  columnHeaderPattern: string;
  allProviderList: any;
  allDataSetList: any;
  feedTypeDetails: any;
  fileNameToView: any;
  progresswidth: any;
  activeSourceType: string;
  showProviderNameTextbox: boolean = false;
  showDatasetNameTextbox: boolean = false;

  constructor(private dataService: RestService, cs: ContentService, private dataFeedService: DataFeedService) {
    this.dataFeedContent = cs.getContent(environment.modules.dataOnboarding).dataFeed;
    this.onboardOptions = this.dataFeedContent.onboardOptions;
    this.fileFrequencyList = this.dataFeedContent.fileFrequencyList;
    this.weekDayNumList = this.dataFeedContent.weekDayNumList;
    this.fileExtensionList = this.dataFeedContent.fileExtensionList;
    this.fileHasHeaderList = this.dataFeedContent.fileHasHeaderList;
    this.fileTextQualifierList = this.dataFeedContent.fileTextQualifierList;
    this.fileEncodingList = this.dataFeedContent.fileEncodingList;
    this.fileFormatOptions = this.dataFeedContent.fileFormatOptions;
    this.sourceTypeOptions =  this.dataFeedContent.sourceTypeOptions;
    this.activeSourceType = this.sourceTypeOptions[0];
  }

  ngOnInit() {
    this.fileTypePattern = this.dataFeedService.getDataPattern(this.dataFeedContent.checkPattern.fileType);
    this.fwColumnHeaderPattern = this.dataFeedService.getDataPattern(this.dataFeedContent.checkPattern.fwfileheader);
    this.delimitedColumnHeaderPattern = this.dataFeedService.getDataPattern(this.dataFeedContent.checkPattern.delimitedfileheader);

    this.activeOnboardOption = this.onboardOptions.length > 0 ? this.onboardOptions[1] : '';
    this.activeLayoutDisplayOption = this.editparam ? this.layoutDisplayOptions[2] : this.layoutDisplayOptions[1];
    this.dataFeedDetails.obj_type = this.fileFormatOptions.length > 0 ? this.fileFormatOptions[0] : '';

    // if (!this.editparam) {
    //   this.dataService.getData(environment.commonUtilities.getTypes, { name: 'get_library_provider_names' })
    //     .subscribe(data => { this.dataProviderList = data && data.Contents ? data.Contents : ''; });
    // }

    this.dataService.getData(environment.commonUtilities.getTypes, { name: 'get_all_provider_names' })
      .subscribe(data => { 
        this.allProviderList = data && data.Contents ? data.Contents : ''; 
        this.allProviderList = this.allProviderList.concat(this.otherProviderName);
      });

    if (this.editparam) {
      this.dataService.getData(environment.commonUtilities.getDataFeedDetails, { name: 'get_data_feed_details', obj_id: this.objId })
        .subscribe(data => {
          this.editFeedDetails = data && data.Contents && data.Contents[0] ? data.Contents[0] : {};
          this.fileNamingPattern = this.editFeedDetails.obj_ptrn_fmt;
          this.fileColumnHeader = this.editFeedDetails.column_header ? this.editFeedDetails.column_header.toString() : '';
          this.fileColumnDelemiter = this.editFeedDetails.obj_delimiter;
          this.fileArrivalTimeVal = this.editFeedDetails.file_arrival_time;
          this.dataFeedDetails = {
            obj_id: this.objId, data_provider_name: data.Contents[0].obj_prvdr_nm, data_set_name: data.Contents[0].obj_src_nm, master_obj_id: data.Contents[0].mstr_obj_id, file_type_name: data.Contents[0].obj_nm, file_description: data.Contents[0].obj_desc,
            file_frequency: data.Contents[0].freq, file_arrival_start_day: data.Contents[0].file_arvl_strt_dy, file_arrival_end_day: data.Contents[0].file_arvl_end_dy, file_arrival_time: data.Contents[0].file_arvl_time, standard_file_name: data.Contents[0].std_file_name,
            file_suffix_date_Format: data.Contents[0].obj_ptrn_fmt, file_encoding: data.Contents[0].obj_encoding, file_extension: data.Contents[0].obj_extn, file_column_delemiter: null, file_text_qualifier: data.Contents[0].obj_text_qualifier,
            file_has_header: data.Contents[0].has_header, sample_file_name: data.Contents[0].smpl_file_nm, obj_type: data.Contents[0].obj_typ, obj_uri_id: data.Contents[0].obj_uri_id,
            src_data_layer_id: data.Contents[0].src_data_layer_id
          };
          if(this.editFeedDetails.obj_typ.toUpperCase() == this.fileFormatOptions[0].toUpperCase()) {
            this.dataFeedDetails.obj_type = this.fileFormatOptions[0];
          } else if (this.editFeedDetails.obj_typ.toUpperCase() == this.fileFormatOptions[1].toUpperCase()) {
            this.dataFeedDetails.obj_type = this.fileFormatOptions[1];
          }
          if (this.editFeedDetails.obj_extension == '') {
            this.dataFeedDetails.file_extension = 'None';
          }
          if (this.editFeedDetails.obj_text_qualifier == null) {
            this.fileTextQualifierList = [{ qual_txt: 'None', qual_val: '--' }];
            this.dataFeedDetails.file_text_qualifier = '--';
          }
          if (this.editFeedDetails.file_column_delemiter == null) {
            this.fileColumnDelimiterList = [{ del_txt: 'NA', del_val: '--' }];
            this.dataFeedDetails.file_column_delemiter = '--';
          }
          if (this.editFeedDetails.obj_delimiter == '\\t') {
            this.fileColumnDelimiterList = [{ del_txt: 'Tab(\\t)', del_val: '\t' }];
            this.dataFeedDetails.file_column_delemiter = '\t';
          }
        });
    }
    if(this.cloneparam) {
      this.dataService.getData(environment.commonUtilities.getDataFeedDetails, { name: 'get_data_feed_details', obj_id: this.objId })
        .subscribe(data => {
          this.dataFeedDetails.data_provider_name = data.Contents[0].obj_prvdr_nm;
          this.dataFeedDetails.data_set_name = data.Contents[0].obj_src_nm;
        });
    }
  }

  toggleOnboardOption(selectedOnboardOption: string) {
    this.activeOnboardOption = selectedOnboardOption;
    this.dataFeedDetails = {
      obj_id: null, data_provider_name: null, data_set_name: null, master_obj_id: null, file_type_name: null, file_description: null,
      file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null, standard_file_name: null,
      file_suffix_date_Format: null, file_encoding: null, file_extension: null, file_column_delemiter: null, file_text_qualifier: null,
      file_has_header: null, sample_file_name: null, obj_type: null, obj_uri_id: null, src_data_layer_id: null
    };

    if (this.isUploadSuccessFull) {
      this.dataService.getData(environment.commonUtilities.deletes3File, {
        layer_id: this.samples_file_upload_S3_layer_id,
        file_name: this.selectedFiles.item(0).name,
        folder_prefix: this.folderPrefix
      })
        .subscribe(() => {
        });
    }

    this.isUploadSuccessFull = false;
    this.isUploading = false;
    this.isUploadConfirmed = false;

    this.selectedFiles = null;
    this.fileColumnDelemiter = null;

    this.dataFeedDetails.file_extension = null;
    this.dataFeedDetails.file_column_delemiter = null;
    this.dataFeedDetails.file_text_qualifier = null;
    this.dataFeedDetails.file_encoding = null;

    this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[0] : '';
    this.dataFeedObjectSelected.emit([this.activeLayoutDisplayOption, null, null, null, null, null, null, null, null, null, null,null]);

    if (this.activeOnboardOption === this.onboardOptions[0]) {
      this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[1] : '';
    }
    if (this.activeOnboardOption === this.onboardOptions[1]) {
      this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 2 ? this.layoutDisplayOptions[3] : '';
    }
  }

  dataProviderChange(e: any) {
    if(e.itemData.obj_prvdr_nm == "Others") {
      this.showProviderNameTextbox = true;
    } else {
      this.showProviderNameTextbox = false;
    }
    if (this.activeOnboardOption === this.onboardOptions[0]) {
      this.dataService.getData(environment.onboarding.dataFeed.getDataSet, { name: 'get_data_set', provider_name: e.itemData.obj_prvdr_nm })
        .subscribe(data => {
          this.dataSetList = data && data.Contents ? data.Contents : [];
          if (this.editparam) {
            this.dataFeedDetails.data_set_name = this.editFeedDetails.obj_src_nm;
          }
        });
    }
    if (e.itemData) {
      this.dataService.getData(environment.onboarding.dataFeed.getDataSet, { name: 'get_all_data_set', provider_name: e.itemData.obj_prvdr_nm })
        .subscribe(data => {
          this.allDataSetList = data && data.Contents ? data.Contents : [];
          this.allDataSetList = this.allDataSetList.concat(this.otherDatasetName);
          if (this.editparam) {
            this.dataFeedDetails.data_set_name = this.editFeedDetails.obj_src_nm;
          }
        });
    } else {
      this.allDataSetList = [];
    }
  }

  onProviderChangeComplete() {
  this.dataFeedDetails.data_provider_name = this.dataFeedDetails.data_provider_name == "Others" ? '' : this.dataFeedDetails.data_provider_name;
  }

  onDatasetChangeComplete() {
    this.dataFeedDetails.data_set_name = this.dataFeedDetails.data_set_name == "Others" ? '' : this.dataFeedDetails.data_set_name;
  }

  dataSetChange(e: any) {
    if(e.itemData.obj_src_nm == "Others") {
      this.showDatasetNameTextbox = true;
    } else {
      this.showDatasetNameTextbox = false;
    }
    // setTimeout(() => {
    // this.dataService.getData(environment.onboarding.dataFeed.getFeedNames, {
    //   name: 'get_feed_names', data_set: e.itemData.obj_src_nm,
    //   provider_name: this.dataProvider.value
    // })
    //   .subscribe(data => {
    //     this.fileTypeList = data && data.Contents ? data.Contents : [];
        //                      if (this.editparam == true) {
        //   this.dataFeedDetails.master_obj_id = this.editFeedDetails.mstr_obj_id;
        // }
      // });
    // });
  }

  fileTypeChange(e) {
    if (!this.editparam) {
      this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[1] : '';
    }
    if (e.itemData.obj_nm.trim() !== '') {
      // setTimeout(() => {
      this.dataService.getData(environment.commonUtilities.checkFileStatus, { name: 'get_file_name_status', obj_nm: e.itemData.obj_nm })
        .subscribe(data => {
          if (data && data.Contents && data.Contents.length > 0 ? (data.Contents[0].message === this.dataFeedContent.fileType.fileTypeUniqueError) : false) {
            this.fileTypeNameValid = false;
            this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[0] : '';
            this.dataFeedObjectSelected.emit([this.activeLayoutDisplayOption, null, null, null, null, null, null, null, null, null, null, null, null]);
          } else {
            // setTimeout(() => {
            this.dataFeedDetails.file_type_name = e.itemData.obj_nm;
            this.fileTypeNameValid = true;
            this.dataFeedDetails.master_obj_id = e.itemData.mstr_obj_id;
            this.dataService.getData(environment.onboarding.dataFeed.getFeedDetails, { name: 'get_feed_details', feed_name: e.itemData.obj_nm })
              .subscribe(element => {
                this.feedTypeDetails = element && element.Contents && element.Contents.length > 0 ? element.Contents[0] : {};
                this.dataFeedDetails.file_description = this.feedTypeDetails.obj_desc;
                this.dataFeedDetails.file_frequency = this.feedTypeDetails.obj_freq.toUpperCase() === 'MONTHLY' ?
                  'Monthly' : (this.feedTypeDetails.obj_freq.toUpperCase() === 'WEEKLY' ? 'Weekly' : element.text_qualifier);
                this.dataFeedObjectSelected.emit([this.activeLayoutDisplayOption, null, this.dataFeedDetails.master_obj_id, null, null, null, null, null, null, null, null]);
              });
            // });
          }
        });
      // });
    }
  }

  onFileTypeChange(e) {
    if (e.value.trim() !== '') {
      this.dataService.getData(environment.commonUtilities.checkFileStatus, { 
        name: 'get_file_name_status', 
        obj_nm: e.value,
        obj_type: this.dataFeedDetails.obj_type,
        data_layer_id: ""
      }).subscribe(data => {
        this.fileTypeNameValid = !(data && data.Contents && data.Contents.length > 0 && (data.Contents[0].message === this.dataFeedContent.fileType.fileTypeUniqueError))
      });
      this.businessNameSelected.emit(e.value);
    }
  }
}
