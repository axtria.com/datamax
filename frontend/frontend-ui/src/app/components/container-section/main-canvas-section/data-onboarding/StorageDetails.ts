export class StorageDetails {
    obj_data_layer_id: number;
    obj_id: number;
    data_layer_id: number;
    user_id: string;
    user_type: string;
    abslt_path: string;
    layer_flg: string;
    eff_srt_dt: string;
    eff_end_dt: string;
    is_active: string;
    file_frequency: string;
    file_arrival_start_day: number;
    file_arrival_end_day: number;
    file_arrival_time: string;
}

