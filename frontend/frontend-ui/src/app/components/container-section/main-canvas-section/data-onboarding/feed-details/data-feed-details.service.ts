import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
  export class DataFeedService {
  
    constructor() { }
  
    getDataPattern(activeCheckName?: string) {
      let dataPattern: string;
      if (activeCheckName == 'fileTypePattern') {
        dataPattern = '^[A-Za-z][A-z0-9\\s]*';
      } else if (activeCheckName == 'fileNamePattern') {
        dataPattern = '[a-zA-Z_]*[\\[][a-z]{6,8}\\]';
      } else if (activeCheckName == 'fwFileHeaderPattern') {
        dataPattern = '([A-Za-z0-9_]+[\\(][1-9]+[\\)][\\,]?)*';
      } else if (activeCheckName == 'delimitedFileHeaderPattern') {
        dataPattern = '([A-Za-z0-9_]+[\\,]?)+';
      } 
      return dataPattern;
    }
  }
  