export class DataFeedDetails {
    obj_id: number;
    master_obj_id: number;
    data_provider_name: string;
    data_set_name: string;
    file_type_name: string;
    file_description: string;
    file_frequency: string;
    file_arrival_start_day: number;
    file_arrival_end_day: number;
    file_arrival_time: string;
    standard_file_name: string;
    file_suffix_date_Format: string;
    sample_file_name: string;
    obj_type:string;
    file_encoding: string;
    file_extension: string;
    file_column_delemiter: string;
    file_text_qualifier: string;
    file_has_header: string;
    obj_uri_id: string;
    src_data_layer_id: number;
}
