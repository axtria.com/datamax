import { Component, OnInit, ViewChild, Output, EventEmitter, ViewEncapsulation, Input, ElementRef } from '@angular/core';
import { DropDownListComponent, ComboBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { RestService } from 'src/app/shared/services/rest.service';
import { DataFeedDetails } from '../DataFeedDetails';
import { NgForm } from '@angular/forms';
import { DialogComponent, ButtonPropsModel } from '@syncfusion/ej2-angular-popups';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import {DataFeedService} from './data-feed-details.service';
import { TextBoxComponent } from '@syncfusion/ej2-angular-inputs';

@Component({
  selector: 'app-data-feed-details',
  templateUrl: './data-feed-details.component.html',
  styleUrls: ['./data-feed-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DataFeedDetailsComponent implements OnInit {
  fileUploadStatus: any;
  selectedFileName: string;
  fileTypeNameValid = true;
  connectionTypes: any[];
  freqInitial = false;
  editFeedDetails: any;
  dataFeedContent: any;
  onboardingContent: any;

  onboardOptions: string[];
  activeOnboardOption: string;
  layoutDisplayOptions: string[] = ['None', 'MasterObject', 'OnboardedObject', 'File'];
  activeLayoutDisplayOption: string;

  samples_file_upload_S3_layer_id = 85;
  folderPrefix = "OnboardingSampleFiles";

  defaultTimeInterval = 30;
  timePickerFormat = 'HH:mm';
  modalHidden = false;
  modalTarget = '.control-section';
  animationSettings: object = { effect: 'Zoom' };
  s3UploadmodalHidden = false;
  dlgButtons: ButtonPropsModel[];
  dlgButtonss3Upload: ButtonPropsModel[];
  ele: HTMLElement;

  // dataFeedDetails: DataFeedDetails = new DataFeedDetails();
  dataFeedDetails: DataFeedDetails = {
    obj_id: null, data_provider_name: null, data_set_name: null, master_obj_id: null, file_type_name: null, file_description: null,
    file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null, standard_file_name: null,
    file_suffix_date_Format: null, file_encoding: null, file_extension: null, file_column_delemiter: null, file_text_qualifier: null,
    file_has_header: null, sample_file_name: null, obj_type: null, obj_uri_id: null, src_data_layer_id: null
  };

  @ViewChild('btnLibObj', { static: false }) btnLibObj: any;
  @ViewChild('btnNewObj', { static: false }) btnNewObj: any;
  @ViewChild('btnUpload', { static: false }) btnUpload: any;
  @ViewChild('dataProvider', { static: false }) dataProvider: DropDownListComponent;
  @ViewChild('dataProviderNameCB', { static: false }) dataProviderCB: ComboBoxComponent;
  @ViewChild('dataSetCB', { static: false }) dataSetCB: ComboBoxComponent;
  @ViewChild('dataSet', { static: false }) dataSet: DropDownListComponent;
  @ViewChild('fileType', { static: false }) fileType: DropDownListComponent;
  @ViewChild('fileExtension', { static: false }) fileExtension: DropDownListComponent;
  @ViewChild('fileColumnDelimiter', { static: false }) fileColumnDelimiter: DropDownListComponent;
  @ViewChild('dataFeedForm', { static: false }) dataFeedForm: NgForm;
  @ViewChild('Dialog', { static: false }) Dialog: DialogComponent;
  @ViewChild('s3UploadDialog', { static: false }) s3UploadDialog: DialogComponent;
  @ViewChild('fileUpload', { static: false }) FileUpload: any;
  @ViewChild('txtStandardFileName', { static: false }) FileNamingPattern: any;
  @ViewChild('columnHeader', { static: false }) FileColumnHeader: any;
  @ViewChild('grid', { static: false }) grid: GridComponent;
  @ViewChild('txtFileDescription', { static: false }) txtFileDescription: TextBoxComponent;
  @ViewChild('dropArea', { static: false }) dropArea: ElementRef;

  @Output() dataFeedObjectSelected = new EventEmitter<any[]>();

  @Input() objId: number;
  @Input() editparam: boolean;
  @Input() cloneparam: boolean;

  dataProviderList: [];
  dataSetList: [];
  fileTypeList: [];
  fileFrequencyList: any[];
  weekDayNumList: any[];
  fileExtensionList: any[];
  fileHasHeaderList: any[];
  fileTextQualifierList: any[];
  fileEncodingList: any[];
  fileColumnDelimiterList: any[];
  fileNamingPattern: string = null;
  fileColumnDelemiter: string = null;
  fileColumnDelemiterRef: string = null;
  fileTextQualifierRef: string = null;
  fileArrivalTimeVal: string = null;
  fileColumnHeader: string = null;
  selectedFiles: FileList;
  sampleData: any[];
  sampleDataColumns: any[];
  isFileSelected: boolean = false;
  fileFormatOptions: string[];
  fileColumnsName: string;
  fileDelimeter: string = null;
  fileHasDuplicateColumns: boolean = false;
  isFileHeaderProvided: boolean = false;
  isUploadSuccessFull = false;
  isUploading = false;
  isUploadConfirmed = false;

  dataProviderFields: object = { value: 'obj_prvdr_nm' };
  dataProviderCBFields: object = { value: 'obj_prvdr_nm' };
  dataSetFields: object = { value: 'obj_src_nm' };
  fileTypeFields: object = { text: 'obj_nm', value: 'mstr_obj_id' };
  fileFrequencyFields: object = { value: 'freq_val' };
  weekDayNumFields: object = { text: 'week_day', value: 'week_num' };
  fileExtensionFields: object = { value: 'ext_val' };
  fileColumnDelimiterFields: object = { text: 'del_txt', value: 'del_val' };
  fileHasHeaderFields: object = { text: 'header_txt', value: 'header_val' };
  fileTextQualifierFields: object = { text: 'qual_txt', value: 'qual_val' };
  fileEncodingFields: object = { value: 'encoding_type' };
  fileTypePattern: string;
  fwColumnHeaderPattern: string;
  delimitedColumnHeaderPattern: string;
  columnHeaderPattern: string;
  allProviderList: any;
  allDataSetList: [];
  feedTypeDetails: any;
  fileNameToView: any;
  progresswidth: any;

  constructor(private dataService: RestService, private authenticateService: AuthenticateService, cs: ContentService, private dataFeedService: DataFeedService) {
    this.dataFeedContent = cs.getContent(environment.modules.dataOnboarding).dataFeed;
    this.onboardOptions = this.dataFeedContent.onboardOptions;
    this.fileFrequencyList = this.dataFeedContent.fileFrequencyList;
    this.weekDayNumList = this.dataFeedContent.weekDayNumList;
    this.fileExtensionList = this.dataFeedContent.fileExtensionList;
    this.fileHasHeaderList = this.dataFeedContent.fileHasHeaderList;
    this.fileTextQualifierList = this.dataFeedContent.fileTextQualifierList;
    this.fileEncodingList = this.dataFeedContent.fileEncodingList;
    this.fileFormatOptions = this.dataFeedContent.fileFormatOptions;
    this.dlgButtons = [
      { click: this.dlgRejectBtnClick.bind(this, 'preview'), buttonModel: { content: this.dataFeedContent.uploadFile.buttons.reject, isPrimary: true, cssClass: 'actionButton btn btn-secondary' } },
      { click: this.dlgConfirmBtnClick.bind(this), buttonModel: { content: this.dataFeedContent.uploadFile.buttons.confirm, isPrimary: true, cssClass: 'actionButton btn btn-primary actionButton' } }
    ];

    this.dlgButtonss3Upload = [
      { click: this.dlgRejectBtnClick.bind(this, 's3Upload'), buttonModel: { content: this.dataFeedContent.uploadFile.buttons.reject, isPrimary: true, cssClass: 'actionButton btn btn-secondary' } },
      { click: this.uploadSampleFile.bind(this, 'True'), buttonModel: { content: this.dataFeedContent.uploadFile.buttons.confirm, isPrimary: true, cssClass: 'actionButton btn btn-primary actionButton' } }
    ];
  }

  ngOnInit() {
    this.fileTypePattern = this.dataFeedService.getDataPattern(this.dataFeedContent.checkPattern.fileType);
    this.fwColumnHeaderPattern = this.dataFeedService.getDataPattern(this.dataFeedContent.checkPattern.fwfileheader);
    this.delimitedColumnHeaderPattern = this.dataFeedService.getDataPattern(this.dataFeedContent.checkPattern.delimitedfileheader);

    this.activeOnboardOption = this.onboardOptions.length > 0 ? this.onboardOptions[1] : '';
    this.activeLayoutDisplayOption = this.editparam ? this.layoutDisplayOptions[2] : this.layoutDisplayOptions[1];
    this.dataFeedDetails.obj_type = this.fileFormatOptions.length > 0 ? this.fileFormatOptions[0] : '';

    // if (!this.editparam) {
    //   this.dataService.getData(environment.commonUtilities.getTypes, { name: 'get_library_provider_names' })
    //     .subscribe(data => { this.dataProviderList = data && data.Contents ? data.Contents : ''; });
    // }

    this.dataService.getData(environment.commonUtilities.getTypes, { name: 'get_all_provider_names' })
      .subscribe(data => { this.allProviderList = data && data.Contents ? data.Contents : ''; });

    if (this.editparam) {
      this.dataService.getData(environment.commonUtilities.getDataFeedDetails, { name: 'get_data_feed_details', obj_id: this.objId })
        .subscribe(data => {
          this.editFeedDetails = data && data.Contents && data.Contents[0] ? data.Contents[0] : {};
          this.fileNamingPattern = this.editFeedDetails.obj_ptrn_fmt;
          this.fileColumnHeader = this.editFeedDetails.column_header ? this.editFeedDetails.column_header.toString() : '';
          this.fileColumnDelemiter = this.editFeedDetails.obj_delimiter;
          this.fileArrivalTimeVal = this.editFeedDetails.file_arrival_time;
          this.dataFeedDetails = {
            obj_id: this.objId, data_provider_name: data.Contents[0].obj_prvdr_nm, data_set_name: null, master_obj_id: data.Contents[0].mstr_obj_id, file_type_name: data.Contents[0].obj_nm, file_description: data.Contents[0].obj_desc,
            file_frequency: data.Contents[0].freq, file_arrival_start_day: data.Contents[0].file_arvl_strt_dy, file_arrival_end_day: data.Contents[0].file_arvl_end_dy, file_arrival_time: data.Contents[0].file_arvl_time, standard_file_name: data.Contents[0].std_file_name,
            file_suffix_date_Format: data.Contents[0].obj_ptrn_fmt, file_encoding: data.Contents[0].obj_encoding, file_extension: data.Contents[0].obj_extn, file_column_delemiter: null, file_text_qualifier: data.Contents[0].obj_text_qualifier,
            file_has_header: data.Contents[0].has_header, sample_file_name: data.Contents[0].smpl_file_nm, obj_type: data.Contents[0].obj_typ, obj_uri_id: data.Contents[0].obj_uri_id,
            src_data_layer_id: data.Contents[0].src_data_layer_id
          };
          if(this.editFeedDetails.obj_typ.toUpperCase() == this.fileFormatOptions[0].toUpperCase()) {
            this.dataFeedDetails.obj_type = this.fileFormatOptions[0];
          } else if (this.editFeedDetails.obj_typ.toUpperCase() == this.fileFormatOptions[1].toUpperCase()) {
            this.dataFeedDetails.obj_type = this.fileFormatOptions[1];
          }
          if (this.editFeedDetails.obj_extension == '') {
            this.dataFeedDetails.file_extension = 'None';
          }
          if (this.editFeedDetails.obj_text_qualifier == null) {
            this.fileTextQualifierList = [{ qual_txt: 'None', qual_val: '--' }];
            this.dataFeedDetails.file_text_qualifier = '--';
          }
          if (this.editFeedDetails.file_column_delemiter == null) {
            this.fileColumnDelimiterList = [{ del_txt: 'NA', del_val: '--' }];
            this.dataFeedDetails.file_column_delemiter = '--';
          }
          if (this.editFeedDetails.obj_delimiter == '\\t') {
            this.fileColumnDelimiterList = [{ del_txt: 'Tab(\\t)', del_val: '\t' }];
            this.dataFeedDetails.file_column_delemiter = '\t';
          }
        });
    }
    if(this.cloneparam) {
      this.dataService.getData(environment.commonUtilities.getDataFeedDetails, { name: 'get_data_feed_details', obj_id: this.objId })
        .subscribe(data => {
          this.dataFeedDetails.data_provider_name = data.Contents[0].obj_prvdr_nm;
          this.dataFeedDetails.data_set_name = data.Contents[0].obj_src_nm;
        });
    }
  }

  toggleOnboardOption(selectedOnboardOption: string) {
    this.activeOnboardOption = selectedOnboardOption;
    this.dataFeedDetails = {
      obj_id: null, data_provider_name: null, data_set_name: null, master_obj_id: null, file_type_name: null, file_description: null,
      file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null, standard_file_name: null,
      file_suffix_date_Format: null, file_encoding: null, file_extension: null, file_column_delemiter: null, file_text_qualifier: null,
      file_has_header: null, sample_file_name: null, obj_type: null, obj_uri_id: null, src_data_layer_id: null
    };

    if (this.isUploadSuccessFull) {
      this.dataService.getData(environment.commonUtilities.deletes3File, {
        layer_id: this.samples_file_upload_S3_layer_id,
        file_name: this.selectedFiles.item(0).name,
        folder_prefix: this.folderPrefix
      })
        .subscribe(() => {
        });
    }

    this.isUploadSuccessFull = false;
    this.isUploading = false;
    this.isUploadConfirmed = false;

    this.selectedFiles = null;
    this.fileColumnDelemiter = null;

    this.dataFeedDetails.file_extension = null;
    this.dataFeedDetails.file_column_delemiter = null;
    this.dataFeedDetails.file_text_qualifier = null;
    this.dataFeedDetails.file_encoding = null;

    this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[0] : '';
    this.dataFeedObjectSelected.emit([this.activeLayoutDisplayOption, null, null, null, null, null, null, null, null, null, null,null]);

    if (this.activeOnboardOption === this.onboardOptions[0]) {
      this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[1] : '';
    }
    if (this.activeOnboardOption === this.onboardOptions[1]) {
      this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 2 ? this.layoutDisplayOptions[3] : '';
    }
  }

  dataProviderChange(e: any) {
    if (this.activeOnboardOption === this.onboardOptions[0]) {
      this.dataService.getData(environment.onboarding.dataFeed.getDataSet, { name: 'get_data_set', provider_name: e.itemData.obj_prvdr_nm })
        .subscribe(data => {
          this.dataSetList = data && data.Contents ? data.Contents : [];
          if (this.editparam) {
            this.dataFeedDetails.data_set_name = this.editFeedDetails.obj_src_nm;
          }
        });
    }
    if (e.itemData) {
      this.dataService.getData(environment.onboarding.dataFeed.getDataSet, { name: 'get_all_data_set', provider_name: e.itemData.obj_prvdr_nm })
        .subscribe(data => {
          this.allDataSetList = data && data.Contents ? data.Contents : [];
          if (this.editparam) {
            this.dataFeedDetails.data_set_name = this.editFeedDetails.obj_src_nm;
          }
        });
    } else {
      this.allDataSetList = [];
    }
  }

  dataSetChange(e: any) {
    // setTimeout(() => {
    this.dataService.getData(environment.onboarding.dataFeed.getFeedNames, {
      name: 'get_feed_names', data_set: e.itemData.obj_src_nm,
      provider_name: this.dataProvider.value
    })
      .subscribe(data => {
        this.fileTypeList = data && data.Contents ? data.Contents : [];
        //                      if (this.editparam == true) {
        //   this.dataFeedDetails.master_obj_id = this.editFeedDetails.mstr_obj_id;
        // }
      });
    // });
  }

  fileTypeChange(e) {
    if (!this.editparam) {
      this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[1] : '';
    }
    if (e.itemData.obj_nm.trim() !== '') {
      // setTimeout(() => {
      this.dataService.getData(environment.commonUtilities.checkFileStatus, { name: 'get_file_name_status', obj_nm: e.itemData.obj_nm })
        .subscribe(data => {
          if (data && data.Contents && data.Contents.length > 0 ? (data.Contents[0].message === this.dataFeedContent.fileType.fileTypeUniqueError) : false) {
            this.fileTypeNameValid = false;
            this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[0] : '';
            this.dataFeedObjectSelected.emit([this.activeLayoutDisplayOption, null, null, null, null, null, null, null, null, null, null, null, null]);
          } else {
            // setTimeout(() => {
            this.dataFeedDetails.file_type_name = e.itemData.obj_nm;
            this.fileTypeNameValid = true;
            this.dataFeedDetails.master_obj_id = e.itemData.mstr_obj_id;
            this.dataService.getData(environment.onboarding.dataFeed.getFeedDetails, { name: 'get_feed_details', feed_name: e.itemData.obj_nm })
              .subscribe(element => {
                this.feedTypeDetails = element && element.Contents && element.Contents.length > 0 ? element.Contents[0] : {};
                this.dataFeedDetails.file_description = this.feedTypeDetails.obj_desc;
                this.dataFeedDetails.file_frequency = this.feedTypeDetails.obj_freq.toUpperCase() === 'MONTHLY' ?
                  'Monthly' : (this.feedTypeDetails.obj_freq.toUpperCase() === 'WEEKLY' ? 'Weekly' : element.text_qualifier);
                this.dataFeedObjectSelected.emit([this.activeLayoutDisplayOption, null, this.dataFeedDetails.master_obj_id, null, null, null, null, null, null, null, null]);
              });
            // });
          }
        });
      // });
    }
  }

  onFileTypeChange(e) {
    if (e.value.trim() !== '') {
      this.dataService.getData(environment.commonUtilities.checkFileStatus, { 
        name: 'get_file_name_status', 
        obj_nm: e.value,
        obj_type: this.dataFeedDetails.obj_type,
        data_layer_id: ""
      }).subscribe(data => {
        this.fileTypeNameValid = !(data && data.Contents && data.Contents.length > 0 && (data.Contents[0].message === this.dataFeedContent.fileType.fileTypeUniqueError))
      });
    }
  }

  filePatternNameChange() {
    this.dataFeedDetails.standard_file_name = this.fileNamingPattern.split('[')[0];

    const fileNamePatternDateSuffixValTemp = this.fileNamingPattern.split('[')[1];
    if (fileNamePatternDateSuffixValTemp) {
      const fileNamePatternDateSuffixVal = fileNamePatternDateSuffixValTemp.substring(0, fileNamePatternDateSuffixValTemp.length - 1);
      this.dataFeedDetails.file_suffix_date_Format = fileNamePatternDateSuffixVal;
    }
  }

  fileExtensionChange() {
    setTimeout(() => {
      switch (this.fileExtension.value) {
        case 'csv': {
          this.fileColumnDelimiterList = [{ del_txt: 'Comma(,)', del_val: ',' }];
          this.fileColumnDelemiter = ',';
          break;
        }
        case 'tsv': {
          this.fileColumnDelimiterList = [{ del_txt: 'Tab(\\t)', del_val: '\t' }];
          this.fileColumnDelemiter = '\t';
          break;
        }
        case 'psv': {
          this.fileColumnDelimiterList = [{ del_txt: 'Pipe(|)', del_val: '|' }];
          this.fileColumnDelemiter = '|';
          break;
        }
        case 'txt': {
          if (!this.editparam) {
            this.fileColumnDelimiterList = [{ del_txt: 'Tab(\\t)', del_val: '\t' }, { del_txt: 'Comma(,)', del_val: ',' },
            { del_txt: 'Pipe(|)', del_val: '|' }];
          }
          break;
        }
        case 'xls': 
        case 'xlsx': {
          this.fileColumnDelimiterList = [{ del_txt: 'NA', del_val: '--' }];
          this.fileColumnDelemiter = '--';
          break;
        }
        default: {
          break;
        }
      }
      this.dataFeedDetails.file_column_delemiter = this.fileColumnDelemiter;
    });
  }

  fileArrivalTimeChange(e) {
    this.dataFeedDetails.file_arrival_time = e.text;
  }

  onFileFrequencyChange() {
    if (this.editparam && !this.freqInitial) {
      this.freqInitial = true;
    } else {
      this.dataFeedDetails.file_arrival_time = null;
      this.dataFeedDetails.file_arrival_start_day = null;
      this.dataFeedDetails.file_arrival_end_day = null;
    }
  }

  onFileHeaderChange() {
    if (this.dataFeedDetails.obj_type == this.fileFormatOptions[1]) {
      this.columnHeaderPattern = this.fwColumnHeaderPattern;
      this.fileColumnsName = this.fileColumnHeader.split(/\(.*?\)\,?/).join().replace(/,\s*$/, "");
      if(this.fileColumnHeader.match(/\(([^)]+)\)/g)) {
        this.fileDelimeter = this.fileColumnHeader.match(/\(([^)]+)\)/g).join().split('(').join('').split(/\)\,?/).join().replace(/,\s*$/, "");
      }
    } else {
      this.columnHeaderPattern = this.delimitedColumnHeaderPattern;
      this.fileColumnsName = this.fileColumnHeader;
    }
    if(this.FileColumnHeader.valid) {
      this.isFileHeaderProvided = true;
    } else {
      this.isFileHeaderProvided = false;
    }
  }

  onResetDataClick() {
    if (this.isUploadConfirmed) {
      this.replaceUploadedFile(false);
    } else {
      this.FileUpload.nativeElement.value = null;
      this.setupNewFileUpload(false);
    }
    this.progresswidth = 0;
    this.isFileSelected = false;
    this.selectedFileName = "";
    this.fileHasDuplicateColumns = false;
  }

  selectSampleFile(event, isUploadConfirmed) {
    if (event.target.files.length) {
      if (isUploadConfirmed) {
        this.replaceUploadedFile(true);
      }
      this.isFileSelected = true;
      this.selectedFiles = event.target.files;
      this.selectedFileName = this.selectedFiles.item(0).name;
    }
  }

  uploadSampleFile(override) {
    this.fileNameToView = this.FileUpload.nativeElement.files[0].name;
    this.progresswidth = 0;
    if (override) {
      this.s3UploadDialog.hide();
    }
    this.isUploading = true;
    // this.FileUpload.nativeElement.disabled = true;

    const formData = new FormData();
    formData.append('file', this.selectedFiles.item(0));

    /* comment start */
    this.dataService.postDataWithProgress(environment.commonUtilities.uploads3File, {
      layer_id: this.samples_file_upload_S3_layer_id,
      folder_prefix: this.folderPrefix,
      override
    }, formData)
      .subscribe(data => {
        if (data.percentageComplete) {
          this.progresswidth = data.percentageComplete;
        } else if(data.complete && data.body) {
                  this.fileUploadStatus = data.body.Status;
          if (this.fileUploadStatus === 'Success') {
    /* comment end */
          this.dataService.getData(environment.commonUtilities.getFileProperties, {
            layer_id: this.samples_file_upload_S3_layer_id,
            folder_prefix: this.folderPrefix+'/',
            delimiter: this.dataFeedDetails.obj_type == this.fileFormatOptions[1] ? this.fileDelimeter : null,
            column_names: this.dataFeedDetails.obj_type == this.fileFormatOptions[1] ? this.fileColumnsName : null,      
            file_name: this.selectedFileName,
            file_type: this.dataFeedDetails.obj_type == this.fileFormatOptions[1] ? 'F' : 'D',
            data_header: this.dataFeedDetails.file_has_header
          })
            .subscribe(element => {
              this.dataFeedDetails.file_encoding = element.encoding.toUpperCase();
              this.fileColumnDelemiterRef = element.delimiter;
              this.dataFeedDetails.file_text_qualifier = element.text_qualifier.toUpperCase() === 'NONE' ?
                '--' : element.text_qualifier;
              this.dataFeedDetails.file_extension = element.file_extension.toLowerCase();
              this.fileTextQualifierRef = this.dataFeedDetails.file_text_qualifier;
              this.onSampleFilePreview();              
            });
          this.isUploadSuccessFull = true;
          this.isUploading = false;
         /* comment start */
          } else {
            this.s3UploadDialog.show();
          }
         /* comment End */
        }
      });
  }

  onSampleFilePreview() {
    this.dataService.getData(environment.commonUtilities.getPreviewData, {
      layer_id: this.samples_file_upload_S3_layer_id,
      delimiter: this.dataFeedDetails.obj_type == this.fileFormatOptions[1] ? this.fileDelimeter : this.fileColumnDelemiterRef,
      text_qualifier: this.fileTextQualifierRef === '--' ? 'None' : this.fileTextQualifierRef,
      column_names: (this.dataFeedDetails.obj_type == this.fileFormatOptions[1] || this.dataFeedDetails.file_has_header=="N") ? this.fileColumnsName : null,
      folder_prefix: this.folderPrefix+'/',
      file_name: this.selectedFiles.item(0).name,
      data_header: this.dataFeedDetails.file_has_header,
      file_type: this.dataFeedDetails.obj_type == this.fileFormatOptions[1] ? 'F' : 'D'
    }).subscribe(data => {
        this.sampleData = data.Contents;
        this.sampleDataColumns = data.Columns;
        this.fileHasDuplicateColumns = this.sampleDataColumns.some(x => this.sampleDataColumns.indexOf(x) !== this.sampleDataColumns.lastIndexOf(x));
        this.grid.columns = [];
        this.Dialog.show();
      });
  }

  dataBound() {
    this.grid.autoFitColumns(this.sampleDataColumns);
  }

  dlgConfirmBtnClick() {
    let isFixedWidthFile = (this.dataFeedDetails.obj_type===this.fileFormatOptions[1]);

    this.activeLayoutDisplayOption = this.layoutDisplayOptions[3];
    this.isUploadConfirmed = true;
    this.Dialog.hide();
    this.dataFeedDetails.sample_file_name = this.selectedFiles.item(0).name;

    if(!this.fileHasDuplicateColumns) {
      this.dataFeedObjectSelected.emit([this.activeLayoutDisplayOption, null, null,
        this.dataFeedDetails.sample_file_name,
        this.fileColumnDelemiterRef,
        this.fileTextQualifierRef === '--' ? 'None' : this.fileTextQualifierRef,
        this.samples_file_upload_S3_layer_id,
        this.folderPrefix,
        isFixedWidthFile, this.fileColumnsName, this.fileDelimeter,this.dataFeedDetails.file_has_header]);
    }
  }

  dlgRejectBtnClick(modalType) {
    if (modalType == 'preview') {
      this.replaceUploadedFile(false);
      this.Dialog.hide();
    } else {
      this.FileUpload.nativeElement.disabled = false;
      this.FileUpload.nativeElement.value = null;
      this.setupNewFileUpload(false);
      this.s3UploadDialog.hide();
    }
    this.isFileSelected = false;
    this.selectedFileName = "";
    this.fileHasDuplicateColumns = false;
    // const s3FileDeleteParams = {
    //   Bucket: (this.connectionTypes.find(item => item.conn_id === this.samples_file_upload_S3_layer_id)).bucket,
    //   Key: (this.connectionTypes.find(item => item.conn_id === this.samples_file_upload_S3_layer_id)).folder_prefix + '/' + this.selectedFiles.item(0).name,
    // };
    // this.uploadService.deletefile(s3FileDeleteParams);
  }

  replaceUploadedFile(isReplace: boolean) {
    this.activeLayoutDisplayOption = this.layoutDisplayOptions[0];
    this.dataService.getData(environment.commonUtilities.deletes3File, {
      layer_id: this.samples_file_upload_S3_layer_id,
      file_name: this.selectedFiles.item(0).name,
      folder_prefix: this.folderPrefix+'/'
    })
      .subscribe(data => {
        if (data.Status == 'Success') {
          this.setupNewFileUpload(isReplace);
          this.dataFeedObjectSelected.emit([this.activeLayoutDisplayOption, null, null, null, null, null, null, null, null, null, null, null]);
        }
      });
  }

  setupNewFileUpload(isReplace: boolean) {
    this.isUploadSuccessFull = false;
    this.isUploading = false;
    this.isUploadConfirmed = false;
    this.progresswidth = 0;
    // this.FileUpload.nativeElement.disabled = false;

    if (!isReplace) {
      this.selectedFiles = null;
      // this.FileUpload.nativeElement.value = null;
    }
    this.fileColumnDelemiter = null;
    this.dataFeedDetails.file_extension = null;
    this.dataFeedDetails.file_column_delemiter = null;
    this.dataFeedDetails.file_text_qualifier = null;
    this.dataFeedDetails.file_encoding = null;
  }

  addFileValidation(objId) {
    return this.dataService.getData(environment.onboarding.dataFeed.addFileValidation, {
      name: 'add_file_validation_rule', obj_id: objId, 
      created_by: this.authenticateService.getUserInfo().user_id
    });
  }

  getObjectInstance(obj_id) {
    return this.dataService.getData(environment.commonUtilities.getObjectInstance, {
      name: 'get_object_instance',
      obj_id: obj_id
    });
  }

  saveLdgFeedDetails(name,parentID, objID, objUriID, version, dataLayerId, objectName) {
    return this.dataService.getData(environment.commonUtilities.saveObjectDetails, {
      name,  obj_nm: objectName,
      src_data_layer_id: dataLayerId, obj_src_nm: null, src_data_layer: 'Staging', obj_delimiter: null,
      obj_id: objID, obj_desc: null, obj_text_qualifier: null, obj_extension: null, freq: null,
      has_header: null, data_strt_row: null, mstr_obj_id: null, version: version,
      cmpr_mtd:null, footer_note: null, obj_encoding: null, obj_ptrn_fmt: null,
      obj_type: 'Target Table', obj_prvdr_nm: null, sample_file_name: null,
      file_arrival_start_day: null, file_arrival_end_day: null,
      file_arvl_time: null, audit_insrt_id: this.authenticateService.getUserInfo().user_id,
      data_persist: null, parent_obj_id: parentID, obj_uri_id: objUriID
    });
  }

  saveDataFeedDetails(apiName, objId, version, objUriID, schedulingDetails) {
    return this.dataService.getData(environment.commonUtilities.saveObjectDetails,
      {
        name: apiName, sample_file_name: this.dataFeedDetails.sample_file_name,parent_obj_id: schedulingDetails.controlFileId, 
        file_arrival_start_day: schedulingDetails.fileStDay, obj_uri_id: objUriID, src_data_layer_id: schedulingDetails.landingLayerID,
        data_strt_row: null, src_data_layer: 'Landing', obj_id: objId, has_header: this.dataFeedDetails.file_has_header, 
        obj_extension: this.dataFeedDetails.file_extension, cmpr_mtd:null, freq: schedulingDetails.fileFreq,
        obj_type: this.dataFeedDetails.obj_type.toUpperCase(), obj_desc: this.dataFeedDetails.file_description,
        file_arrival_end_day: schedulingDetails.fileEndDay,
        audit_insrt_id: this.authenticateService.getUserInfo().user_id, 
        obj_encoding: this.dataFeedDetails.file_encoding, footer_note: null,
        obj_src_nm: this.dataFeedDetails.data_set_name, file_arvl_time: schedulingDetails.fileArvlTime, 
        obj_delimiter: this.dataFeedDetails.file_column_delemiter == '--' ? null : (this.dataFeedDetails.file_column_delemiter == '\t' ? '\\t' : this.dataFeedDetails.file_column_delemiter),
        data_persist: null, obj_prvdr_nm: this.dataFeedDetails.data_provider_name,
        obj_text_qualifier: this.dataFeedDetails.file_text_qualifier == '--' ? null : this.dataFeedDetails.file_text_qualifier,
        obj_ptrn_fmt: schedulingDetails.fileNamingPtrn, obj_nm: this.dataFeedDetails.file_type_name,
        version: version, mstr_obj_id: this.dataFeedDetails.master_obj_id
      });
  }

  updateDataFeedDetails(apiName, version, objUriID, schedulingDetails) {
    return this.dataService.getData(environment.commonUtilities.updateObjectDetails,
      {
        name: apiName, freq:schedulingDetails.fileFreq ? schedulingDetails.fileFreq : this.dataFeedDetails.file_frequency, parent_obj_id: schedulingDetails.controlFileId ? schedulingDetails.controlFileId : null,
        cmpr_mtd: null, smpl_file_nm: this.dataFeedDetails.sample_file_name, obj_typ: this.dataFeedDetails.obj_type.toUpperCase(), 
        obj_ptrn_fmt: schedulingDetails.fileNamingPtrn ? schedulingDetails.fileNamingPtrn : this.fileNamingPattern,
        obj_text_qualifier: this.dataFeedDetails.file_text_qualifier == '--' ? null : this.dataFeedDetails.file_text_qualifier,
        footer_note: null, obj_nm: this.dataFeedDetails.file_type_name, obj_prvdr_nm: this.dataFeedDetails.data_provider_name, file_arvl_time: schedulingDetails.fileArvlTime,
        obj_src_nm: this.dataFeedDetails.data_set_name, data_strt_row: null, 
        file_arvl_strt_dy: schedulingDetails.fileStDay ? schedulingDetails.fileStDay : this.dataFeedDetails.file_arrival_start_day, old_obj_uri_id: this.dataFeedDetails.obj_uri_id,
        data_persist: null, has_header: this.dataFeedDetails.file_has_header, mstr_obj_id: this.dataFeedDetails.master_obj_id, obj_encoding: this.dataFeedDetails.file_encoding,
        obj_extn: this.dataFeedDetails.file_extension, new_obj_uri_id: objUriID, version: version,
        file_arvl_end_dy: schedulingDetails.fileEndDay ? schedulingDetails.fileEndDay : this.dataFeedDetails.file_arrival_end_day,
        obj_desc: this.dataFeedDetails.file_description, audit_updt_id: this.authenticateService.getUserInfo().user_id,
        obj_delimiter: this.dataFeedDetails.file_column_delemiter == '--' ? null : (this.dataFeedDetails.file_column_delemiter == '\t' ? '\\t' : this.dataFeedDetails.file_column_delemiter),
        src_data_layer_id: schedulingDetails.landingLayerID ? schedulingDetails.landingLayerID : this.dataFeedDetails.src_data_layer_id
      });  
  }
}
