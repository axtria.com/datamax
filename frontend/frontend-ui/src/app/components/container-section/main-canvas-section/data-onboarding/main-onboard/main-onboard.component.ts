import { Component, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DataFeedDetails } from '../DataFeedDetails';
import { DataFeedDetailsComponent } from '../data-feed-details/data-feed-details.component';
import { FeedDetailsComponent } from '../feed-details/feed-details.component';
import { FileDetailsComponent } from '../file-details/file-details.component';

// import { DataProviderDetailsComponent } from '../data-provider-details/data-provider-details.component';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
// import { DataConnectionDetailsComponent } from '../data-connection-details/data-connection-details.component';
import { DataLayoutDetailsComponent } from '../data-layout-details/data-layout-details.component';
import { ContentService } from 'src/app/shared/services/content.service';
import { Router, ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { environment } from 'src/environments/environment';
import { DataStorageDetailsComponent } from '../data-storage-details/data-storage-details.component';
import { PocDetailsComponent } from '../poc-details/poc-details.component';

@Component({
  selector: 'app-main-onboard',
  templateUrl: './main-onboard.component.html',
  styleUrls: ['./main-onboard.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class MainOnboardComponent implements OnInit {
  editParamRoute = false;
  cloneParamRoute = false;
  objectId: number;
  onboardingTabs: any;
  mainOnboardingContent: any;
  headerText: any;
  activeSection:string;
  formSections: string[]=['1. Data feed','2. Sample File', '3. Configuration', '4. Scheduling'];
 
  // dataFeedDetails: DataFeedDetails = new DataFeedDetails();
  dataFeedDetails: DataFeedDetails = {
    obj_id: null, data_provider_name: null, data_set_name: null, master_obj_id: null, file_type_name: null, file_description: null,
    file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null, standard_file_name: null,
    file_suffix_date_Format: null, file_encoding: null, file_extension: null, file_column_delemiter: null, file_text_qualifier: null,
    file_has_header: null, sample_file_name: null, obj_type: null, obj_uri_id: null, src_data_layer_id: null
  };

  @ViewChild('dataFeedDetails', { static: false }) objDataFeedDetailsComponent: DataFeedDetailsComponent;
  @ViewChild('feedDetails', { static: false }) objFeedDetailsComponent: FeedDetailsComponent;
  @ViewChild('fileDetails', { static: false }) objFileDetailsComponent: FileDetailsComponent;
  @ViewChild('dataLayoutDetails', { static: false }) objLayoutDetailsComponent: DataLayoutDetailsComponent;
  //@ViewChild('dataConnectionDetails', { static: false }) objDataConnectionDetailsComponent: DataConnectionDetailsComponent;
  //@ViewChild('dataProviderDetails', { static: false }) objDataProviderDetailsComponent: DataProviderDetailsComponent;
  @ViewChild('dataStorageDetails', { static: false }) objDataConnectionDetailsComponent: DataStorageDetailsComponent;
  @ViewChild('dataPocDetails', { static: false }) objDataProviderDetailsComponent: PocDetailsComponent;
  savedFeedDetails: any;
  objectDetails: any;
  tableObjectDetails: any;

  // headerText: object = [{ text: 'Data Feed'}, { text: 'File Layout'},
  // { text: 'Source Connection' }, { text: 'Data Agreement' }];

  constructor(private toastPopupService: ToastPopupService, cs: ContentService, private route: ActivatedRoute, private router: Router, private cdRef : ChangeDetectorRef) {
    this.mainOnboardingContent = cs.getContent(environment.modules.dataOnboarding).mainOnboard;
    this.headerText = this.mainOnboardingContent.subMenu.headerText;
  }

  ngOnInit() {
    this.activeSection=this.formSections[0];
    if(this.route.snapshot.paramMap.get('id') && this.route.snapshot.paramMap.get('id').includes("edit")) {
      this.objectId = parseInt(this.route.snapshot.paramMap.get('id').split('=')[1]);
      this.editParamRoute = true;
    } else if(this.route.snapshot.paramMap.get('id') && this.route.snapshot.paramMap.get('id').includes("clone")) {
      this.objectId = parseInt(this.route.snapshot.paramMap.get('id').split('=')[1]);
      this.cloneParamRoute = true;
    } else {
      this.objectId = null;
    }
  }
  ngAfterViewChecked() {
      this.cdRef.detectChanges();
  }

  ngAfterViewInit() {
    // setTimeout(() => {
    //   this.dataFeedDetails = this.objDataFeedDetailsComponent.dataFeedDetails;
    // });
  }
  
  OnNextClick() {
    let index = this.formSections.findIndex((item =>
      item == this.activeSection
    ));
    this.activeSection=this.formSections[index + 1];
  }
  OnPreviousBtnClick() {
    let index = this.formSections.findIndex((item =>
      item == this.activeSection
    ));
    this.activeSection=this.formSections[index - 1];
  }

  saveOnboardingDetails() {
    let formsValid = true;

    if (!this.objFeedDetailsComponent.dataFeedForm.valid && !this.objFileDetailsComponent.dataFeedForm.valid) {
      formsValid = false;
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.mainOnboardingContent.dataFeedError));
    }

    // if (this.objectId === 0 &&
    //   (typeof (this.objDataConnectionDetailsComponent) === 'undefined' || !this.objDataConnectionDetailsComponent.connDetailsForm.valid)) {
    //   formsValid = false;
    //   this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.onboardingContent.connectionError));
    // }

    // if (this.objectId === 0 &&
    //   (typeof (this.objDataProviderDetailsComponent) === 'undefined' || !this.objDataProviderDetailsComponent.dataProviderForm.valid)) {
    //   formsValid = false;
    //   this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.onboardingContent.providerError));
    // }
    if (this.objectId === null && 
      (typeof (this.objLayoutDetailsComponent) === 'undefined' || !this.objLayoutDetailsComponent.isFormValid ||
      this.objLayoutDetailsComponent.dataLayoutDetails === null || this.objLayoutDetailsComponent.isLayoutInfoInvalid())) {
      formsValid = false;
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.mainOnboardingContent.layoutError));
    }
    if (this.objectId === null && (typeof (this.objLayoutDetailsComponent) === 'undefined' || !this.objFeedDetailsComponent.fileTypeNameValid || 
      this.objLayoutDetailsComponent.isObjectNameAlreadyExist || this.objLayoutDetailsComponent.isObjectNameRefAlreadyExist)) {
      formsValid = false;
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.mainOnboardingContent.InvalidInfoError));
    }

    if (formsValid) {
      const thisObj = this;
      this.objFileDetailsComponent.getObjectInstance(this.objectId).subscribe(data => {
      this.objectDetails=data && data.Contents && data.Contents.length>0 ? data.Contents[0] : "";
      if(this.objectDetails) {
        if (this.objectId !== null) {
          const updateMethods = [];
          let old_obj_uri_id = this.objFileDetailsComponent.dataFeedDetails.obj_uri_id,
            schedulingDetails = {
            fileNamingPtrn: this.objDataConnectionDetailsComponent? this.objDataConnectionDetailsComponent.fileNamingPattern : null,
            fileFreq: this.objDataConnectionDetailsComponent? this.objDataConnectionDetailsComponent.dataStorageDetails.file_frequency : null,
            fileStDay: this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.dataStorageDetails.file_arrival_start_day : null,
            fileEndDay: this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.dataStorageDetails.file_arrival_end_day : null,
            fileArvlTime: this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.dataStorageDetails.file_arrival_time : null,
            landingLayerID: this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.landingDetails.data_layer_id : null,
            controlFileId : this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.controlFileId : null
          },
          feedDetails = {
            data_set_name: this.objFeedDetailsComponent.dataFeedDetails.data_set_name,
            description: this.objFeedDetailsComponent.dataFeedDetails.file_description
          };
          if (typeof (this.objFileDetailsComponent) !== 'undefined') {
            updateMethods.push(this.objFileDetailsComponent.updateDataFeedDetails('update_object_meta', this.objectDetails.version, this.objectDetails.obj_uri_id, schedulingDetails, feedDetails));
          }

          // if (typeof (this.objDataProviderDetailsComponent) !== 'undefined') {
          //   updateMethods.push(thisObj.objDataProviderDetailsComponent.saveDataProviderDetails('update_poc_meta', thisObj.objectId));
          // }
          if (typeof (this.objDataConnectionDetailsComponent) !== 'undefined') {
            this.objDataConnectionDetailsComponent.getSchedulingStatus(this.objectId).subscribe(data => {
              let storageInfo = data && data.Contents && data.Contents.length>0 ? data.Contents[0] : null;
              if(storageInfo && storageInfo.status) {
                updateMethods.push(this.objDataConnectionDetailsComponent.saveStorageDetails(this.objectId, this.objectDetails.obj_uri_id));
              } else {
                updateMethods.push(thisObj.objDataConnectionDetailsComponent.updateStorageDetails(old_obj_uri_id, this.objectDetails.obj_uri_id));
              }
            });
          }
          setTimeout(() => {
            forkJoin(updateMethods).subscribe(() => {
              this.objFileDetailsComponent.postgresScan(this.objectId).subscribe(() => {});
              this.toastPopupService._exception.emit(thisObj.toastPopupService.getToastSuccessMessage(this.mainOnboardingContent.detailsUpdated));
              this.router.navigate(['onboardedData'], { relativeTo: this.route.parent });
            });
          },2000);
        } else {
          const InsertMethods = [];
          let schedulingDetails = {
             fileNamingPtrn: this.objDataConnectionDetailsComponent? this.objDataConnectionDetailsComponent.fileNamingPattern : null,
             fileFreq: this.objDataConnectionDetailsComponent? this.objDataConnectionDetailsComponent.dataStorageDetails.file_frequency : null,
             fileStDay: this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.dataStorageDetails.file_arrival_start_day : null,
             fileEndDay: this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.dataStorageDetails.file_arrival_end_day : null,
             fileArvlTime: this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.dataStorageDetails.file_arrival_time : null,
             landingLayerID: this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.landingDetails.data_layer_id : null,
             controlFileId : this.objDataConnectionDetailsComponent ? this.objDataConnectionDetailsComponent.controlFileId : null
          },
          dataLayerId = this.objLayoutDetailsComponent.activeLayerId,
          objectName = this.objLayoutDetailsComponent.tblObjectName;
             
          this.objFileDetailsComponent.saveDataFeedDetails('add_object_meta', this.objectDetails.obj_id, this.objectDetails.version, this.objectDetails.obj_uri_id, schedulingDetails)
            .subscribe(data => {
              this.savedFeedDetails=data && data.Contents && data.Contents.length>0 ? data.Contents[0] : {};
              this.objFileDetailsComponent.getObjectInstance(this.objectId).subscribe(data => {
                this.tableObjectDetails=data && data.Contents && data.Contents.length>0 ? data.Contents[0] : "";
                if(this.tableObjectDetails) {
                  InsertMethods.push(this.objFileDetailsComponent.saveLdgFeedDetails('add_object_meta', this.savedFeedDetails.obj_id,this.tableObjectDetails.obj_id, this.tableObjectDetails.obj_uri_id, this.tableObjectDetails.version, dataLayerId, objectName));
                }
                InsertMethods.push(this.objLayoutDetailsComponent.saveDataLayoutDetails(this.objectDetails.obj_id, this.objectDetails.obj_uri_id))
                if (typeof (this.objDataConnectionDetailsComponent) !== 'undefined') {
                  InsertMethods.push(this.objDataConnectionDetailsComponent.saveStorageDetails(this.objectDetails.obj_id, this.objectDetails.obj_uri_id));
                }               

                forkJoin(InsertMethods).subscribe((results) => {
                  thisObj.objLayoutDetailsComponent.saveDataLayoutLandingDetails(results && results.length>0 ?results[0].Contents[0].obj_id: "", this.tableObjectDetails.obj_uri_id)
                  .subscribe(() => {
                    this.objFileDetailsComponent.postgresScan(this.objectDetails.obj_id).subscribe(() => {});
                    this.toastPopupService._exception.emit(thisObj.toastPopupService.getToastSuccessMessage(this.mainOnboardingContent.DetailsSaved));
                    this.router.navigate(['onboardedData'], { relativeTo: this.route.parent });
                  });
                });
              });
            });      
        }
      }
    });
  }
}
  
pullDataLayoutDetails(args: any[]) {
    if (typeof (this.objLayoutDetailsComponent) !== 'undefined') {
      this.objLayoutDetailsComponent.fetchDataLayoutDetails(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11]);
    }
  }

  pullBusinessLayerName(args: any) {
    if (typeof (this.objLayoutDetailsComponent) !== 'undefined') {
      this.objLayoutDetailsComponent.getBusinessName(args);
    }
  }
}