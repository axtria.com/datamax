import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { DataLayoutDetails } from 'src/app/components/container-section/main-canvas-section/data-onboarding/DataLayoutDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { SortService, ResizeService, PageService, EditService, ExcelExportService, PdfExportService, ContextMenuService, ContextMenuItem, EditSettingsModel, SaveEventArgs, DialogEditEventArgs, SelectionService, RowDDService } from '@syncfusion/ej2-angular-grids';
import { NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { GridComponent} from '@syncfusion/ej2-angular-grids';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-data-layout-details',
  templateUrl: './data-layout-details.component.html',
  styleUrls: ['./data-layout-details.component.css'],
  providers: [SortService, ResizeService, PageService, EditService, ExcelExportService, PdfExportService, ContextMenuService, RowDDService,
    SelectionService]
})
export class DataLayoutDetailsComponent implements OnInit {

  dataLayoutContent: any;

  layoutDisplayOptions: string[] = ['None', 'MasterObject', 'OnboardedObject', 'File'];

  @Input() onboardMode: string;
  @Input() objId: number;
  @Input() masterObjId: number;
  @Input() sampleFileName: string;
  @Input() fileColumnDelemiter: string;
  @Input() fileTextQualifier: string;
  @Input() s3ConnectorId: number;
  @Input() folderPrefix: string;
  @Input() editparam: boolean;
  @Input() isFixedWidthFile: boolean;
  @Input() fileColumnsName: string;
  @Input() fileDelimeter: string;
  @Input() fileHasHeader:string;
  @Input() fileBusinessName:string;
  @Input() existingLayoutHeader: any[];

  public contextMenuItems: ContextMenuItem[];
  public editing: EditSettingsModel;
  // gridToolbar: ToolbarItems[] | Object;

  dataLayoutDetails: DataLayoutDetails[];
  objectList: [];
  objectFields: object = { text: 'table_name', value: 'table_name' };
  dataLayoutDetail: any;
  dataTypeList: any[];
  dateFormatList: any[];
  requiredList: any[];
  defaultValueList: any[];
  fileHeaderList: any[];
  actualFileHeaderList: any[];
  tempFileHeaderList: any[];
  isFormValid: boolean = false;
  tblObjectName:string;
  activeGranularity: string;
  storageTypes: any[];
  granularityCheckOptions: any[];
  layer: any;
  selectedLayerId: any;
  activeLayerId: any;
  sourceStorageFields: object = { text: 'name', value: 'layer_id' };
  isExistingLayoutSelected: boolean = false;
  isObjectNameAlreadyExist: any;
  isObjectNameRefAlreadyExist: any;
  isLayoutDetailsInvalid: boolean = false;
  columnNameEmpty: boolean = false;
  gridPageSettings: Object;
  currentPage = 1;  
  @ViewChild('dataLayoutForm', { static: false }) dataLayoutForm: NgForm;
  @ViewChild('grid', { static: false }) grid: GridComponent;
  @ViewChild('at_nm',{static: false}) dropDownListObject: DropDownListComponent;

  //public initialPage: Object;

  constructor(private dataService: RestService, private authenticateService: AuthenticateService, private toastPopupService: ToastPopupService, cs: ContentService) {
    this.dataLayoutContent = cs.getContent(environment.modules.dataOnboarding).dataLayout;
    this.defaultValueList = this.dataLayoutContent.defaultValueList;
    this.contextMenuItems = this.dataLayoutContent.contextMenuItems;
    this.requiredList = this.dataLayoutContent.requiredList;
    this.dateFormatList = this.dataLayoutContent.dateFormats;
    this.granularityCheckOptions = this.dataLayoutContent.granularityCheckOptions;
    this.activeGranularity = this.granularityCheckOptions[1];
  }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: false, pageSize:5, currentPage: this.currentPage, template: ''};    

    //this.initialPage = { pageSizes: true, pageCount: 5 };

    this.tblObjectName = this.fileBusinessName !== null ? ('stg_' + this.fileBusinessName.replace(/ /g, '_').toLowerCase()) : null;

    // this.contextMenuItems = ['Copy', 'Edit', 'Delete', 'Save', 'Cancel',
    //   'FirstPage', 'PrevPage', 'LastPage', 'NextPage'];
    this.editing = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top'};
    // this.gridToolbar = [{ text: 'ColumnChooser', template: '#columnChooserTemplateDL', align: 'Right' }];

    this.dataService.getData(environment.onboarding.layout.getDataLayers, {name: 'get_obj_meta_data_layers'})
      .subscribe(data => {
        this.storageTypes = data && data.Contents ? data.Contents : [];
        this.activeLayerId = this.storageTypes[0].layer_id;
      });

    this.dataService.getData(environment.commonUtilities.getTypes, { name: 'get_dataTypes' })
      .subscribe(data => {
        this.dataTypeList = data.Contents;
        // if (this.objId == 0) {
        if (this.masterObjId !== null || this.sampleFileName !== null) {
          this.fetchDataLayoutDetails(this.onboardMode, this.objId, this.masterObjId, this.sampleFileName, this.fileColumnDelemiter, this.fileTextQualifier, 
            this.s3ConnectorId, this.folderPrefix, this.isFixedWidthFile , this.fileColumnsName, this.fileDelimeter, this.fileHasHeader);
        } 
        // else {
        //   this.toastPopupService._exception.emit(
        //     this.toastPopupService.getToastWarningMessage(this.dataLayoutContent.dataFeedError));
        // }

        // } else {
        //   this.dataService.callServerOnboardingParams('/exchange', { name: 'get_object_attribute_details', object_id: this.objId })
        //     .subscribe(data => { this.dataLayoutDetails = data.Contents;
        //                          this.editing = { allowEditing: false, allowAdding: false, allowDeleting: false, newRowPosition: 'Top' };
        //     });
        // }
      });
  }

  getBusinessName(args: any) {
    if(this.activeGranularity == this.granularityCheckOptions[1])
    this.tblObjectName = args !== null ? ('stg_' + args.replace(/ /g, '_').toLowerCase()) : null;
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
  }

  changePageNumber(currentPage: any) {
    this.currentPage = currentPage;
    this.gridPageSettings = { currentPage: this.currentPage };
  }

  dataLayerChange(e: any) {
    if (e.itemData !== null) {
      this.activeLayerId = e.itemData.layer_id;
      this.selectedLayerId = e.itemData.layer_id;
      this.populateObject(this.activeLayerId);
      if(!this.isExistingLayoutSelected) {
        this.checkDuplicateTableName();
      }
    }
  }

  populateObject(layerId: string) {
    this.dataService.getData(environment.onboarding.layout.getTableObjects,
      {layer_id: layerId })
      .subscribe(data => {
        this.objectList = data && data.Contents ? data.Contents : [];
    });
  }

  checkDuplicateTableName() {
    this.dataService.getData(environment.onboarding.layout.getTableObjectsMatch,
      {layer_id: this.activeLayerId, match: this.tblObjectName})
      .subscribe(data => {
        this.isObjectNameAlreadyExist = data.Contents.some(element => {
          return element.table_name == this.tblObjectName;
        });
    });
    
    this.dataService.getData(environment.commonUtilities.checkFileStatus, {
      name: 'get_file_name_status', 
      obj_nm: this.tblObjectName,
      obj_type: "Target Table",
      data_layer_id: this.activeLayerId
     }).subscribe(data => {
        this.isObjectNameRefAlreadyExist = (data && data.Contents && data.Contents.length > 0 && (data.Contents[0].message === "Duplicate Name"))
     });
  }
  
  objectChange(e: any) {
    this.grid.refresh();
    if (e.itemData !== null) {
      this.tblObjectName = e.itemData.table_name;
      this.dataService.getData(environment.commonUtilities.checkFileStatus, {
        name: 'get_file_name_status', 
        obj_nm: this.tblObjectName,
        obj_type: "Target Table",
        data_layer_id: this.activeLayerId
       }).subscribe(data => {
          this.isObjectNameRefAlreadyExist = (data && data.Contents && data.Contents.length > 0 && (data.Contents[0].message === "Duplicate Name"));
          if(!this.isObjectNameRefAlreadyExist) {
            this.dataService.getData(environment.onboarding.layout.getTableObjectsList, 
              { layer_id: this.selectedLayerId, table_name: this.tblObjectName })
              .subscribe(data => {
                if(this.existingLayoutHeader && (data.Contents.length > this.existingLayoutHeader.length)) {
                  this.toastPopupService._exception.emit(
                    this.toastPopupService.getToastWarningMessage(this.dataLayoutContent.tableMapError));
                } else {
                  this.dataLayoutDetails = data.Contents;
                  this.actualFileHeaderList = [...this.existingLayoutHeader];
                  this.fileHeaderList = [...this.existingLayoutHeader];
                  this.tempFileHeaderList = this.fileHeaderList;
                  this.editing = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top'};
                  this.dataLayoutDetails.forEach(layoutElement => {
                    layoutElement.at_nm = '';
                    layoutElement.is_mandatory = 'N';
                });
                }
            });
          }
       });
    }
  }

  isLayoutInfoInvalid() {
    if(this.dataLayoutDetails) {
      this.isLayoutDetailsInvalid = this.dataLayoutDetails.some(element => {
        return element.at_nm == '';
      });
    }
    return this.isLayoutDetailsInvalid;
  }

  fetchDataLayoutDetails(onboardMode: string, objId: number, masterObjId: number, sampleFileName: string, fileColumnDelemiter: string, fileTextQualifier: string,
     s3ConnectorId: number, folderPrefix: string, isFixedWidthFile: boolean, fileColumnsName: string, fileDelimeter: string, fileHasHeader: string) {

    if (onboardMode.toUpperCase() === this.layoutDisplayOptions[0].toUpperCase()) {
      this.dataLayoutDetails = null;
    }

    if (onboardMode.toUpperCase() === this.layoutDisplayOptions[1].toUpperCase()) {
      this.dataService.getData(environment.onboarding.layout.getMasterObjectDetails,
        { name: 'get_master_attribute_details', master_object_id: masterObjId })
        .subscribe(data => {
          this.dataLayoutDetails = data.Contents;
          this.editing = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top'};
        });
    }

    if (onboardMode.toUpperCase() === this.layoutDisplayOptions[2].toUpperCase()) {
      this.dataService.getData(environment.commonUtilities.getObjectDetails, { name: 'get_object_attribute_details', obj_id: objId })
        .subscribe(data => {
          this.dataLayoutDetails = data.Contents;
          this.editing = { allowEditing: false};
        });
    }

    if (onboardMode.toUpperCase() === this.layoutDisplayOptions[3].toUpperCase() && this.activeGranularity !== this.granularityCheckOptions[0]) {
      this.dataService.getData(environment.onboarding.layout.getAttributeInfo, {
        layer_id: s3ConnectorId,
        delimiter: isFixedWidthFile ? fileDelimeter : fileColumnDelemiter,
        column_names: (isFixedWidthFile || fileHasHeader == 'N') ? fileColumnsName : null,
        folder_prefix:folderPrefix+'/',
        file_name: sampleFileName,
        text_qualifier: isFixedWidthFile ? null : fileTextQualifier,
        file_type: isFixedWidthFile ? 'F' : 'D',
        data_header: fileHasHeader        
        })
        .subscribe(data => { 
          this.dataLayoutDetails = data.Contents;
          this.dataLayoutDetails.forEach(layoutElement => {
            layoutElement.is_mandatory = 'N';
          });
          this.editing = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top'};  
        }) 
      }

    // if (masterObjId !== null) {
    //   this.dataService.callServerOnboardingParams('/exchange',
    //     { name: 'get_master_attribute_details', master_object_id: masterObjId })
    //     .subscribe(data => { this.dataLayoutDetails = data.Contents; });
    // }

    // if (sampleFileName !== null) {
    //   this.dataService.callServerOnboardingParams('/operations/attribute_info', {
    //     s3_connector_id: s3ConnectorId,
    //     file_name: sampleFileName, delimiter: fileColumnDelemiter
    //   })
    //     .subscribe(data => { this.dataLayoutDetails = data.Contents; });
    // }
  }

  onColumnHeaderSelection(e: any) {
    if(e.itemData) {
      let index = this.tempFileHeaderList.indexOf(e.itemData.value);
      if (index !== -1) {
        this.tempFileHeaderList.splice(index, 1);
      }
      this.fileHeaderList = this.tempFileHeaderList;
    }
  }

  onColumnHeaderDropdownOpen(val: string) {
    if (this.actualFileHeaderList.includes(val) && !this.tempFileHeaderList.includes(val)) {
      this.dropDownListObject.addItem(val,0);
      this.tempFileHeaderList.unshift(val);
    } else {
      this.fileHeaderList = this.tempFileHeaderList;
    }
  }

  onColumnHeaderDropdownClose(val: string) {
    if(this.tempFileHeaderList.includes(val)) {
      let index = this.tempFileHeaderList.indexOf(val);
      this.tempFileHeaderList.splice(index,1);
      this.fileHeaderList = this.tempFileHeaderList;
    }
      this.columnNameEmpty = this.dataLayoutDetail.at_nm == '';
  }

  granularityChange(granularityCheckOption: string) {
    this.grid.refresh();
    this.activeGranularity = granularityCheckOption;
    this.dataLayoutDetails = null;
    if ( this.activeGranularity !== this.granularityCheckOptions[0]) {
      this.isExistingLayoutSelected = true;
      this.dataService.getData(environment.onboarding.layout.getAttributeInfo, {
        layer_id: this.s3ConnectorId,
        delimiter: this.isFixedWidthFile ? this.fileDelimeter : this.fileColumnDelemiter,
        column_names: (this.isFixedWidthFile || this.fileHasHeader == 'N') ? this.fileColumnsName : null,
        folder_prefix:this.folderPrefix+'/',
        file_name: this.sampleFileName,
        text_qualifier: this.isFixedWidthFile ? null : this.fileTextQualifier,
        file_type: this.isFixedWidthFile ? 'F' : 'D',
        data_header: this.fileHasHeader        
        })
        .subscribe(data => { 
          this.dataLayoutDetails = data.Contents;
          this.dataLayoutDetails.forEach(layoutElement => {
            layoutElement.is_mandatory = 'N'
          });
          this.editing = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top'};  
        }) 
      } else {
        this.isExistingLayoutSelected = false;
      }
  }

  actionBegin(args: SaveEventArgs): void {
    if (args.requestType === 'beginEdit' || args.requestType === 'add') {
      this.dataLayoutDetail = Object.assign({}, args.rowData);
    }
    if (args.requestType === 'save') {
      if (this.dataLayoutForm.valid) {
        args.data = this.dataLayoutDetail;
        let index = this.dataLayoutDetails.findIndex((item =>
          item.etl_nm == this.dataLayoutDetail.etl_nm
        ));
        this.dataLayoutDetails[index] = this.dataLayoutDetail;
      } else {
        args.cancel = true;
      }
    }
  }

  actionComplete(args: DialogEditEventArgs): void {
    if (args.requestType === 'beginEdit' || args.requestType === 'add') {
      // const dialog = args.dialog;
      // dialog.header = args.requestType === 'beginEdit' ? 'Record of ' + args.rowData[0] : 'New Entry';
      // Set initail Focus
      // if (args.requestType === 'beginEdit') {
      //   (args.form.elements.namedItem('etl_nm') as HTMLInputElement).focus();
      // } else
      // if (args.requestType === 'add') {
      //   (args.form.elements.namedItem('attr_nm') as HTMLInputElement).focus();
      // }

    }
    this.isFormValid = this.dataLayoutForm.valid;
  }

  headerCellInfo(args) {
    args.node.getElementsByClassName('e-checkbox-wrapper')[0] && args.node.getElementsByClassName('e-checkbox-wrapper')[0].remove();
  }

  focusIn(target: HTMLElement): void {
    target.parentElement.classList.add('e-input-focus');
  }

  focusOut(target: HTMLElement): void {
    target.parentElement.classList.remove('e-input-focus');
  }

  setIntialFormatValue(target: any): string {
    let _targetDataType = target.value.toUpperCase();

    return this.formatDisplayText(_targetDataType);
  }

  formatDisplayText(dataType: string): string {
    let _targetDataType = dataType.toUpperCase();

    let frmt = _targetDataType === 'DATE' ? 'MM-dd-yyyy' :  _targetDataType === 'TIMESTAMP' || _targetDataType === 'TIMESTAMP_TZ' || _targetDataType === 'TIMESTAMP_NTZ' ? 'MM-dd-yyyy hh:mm:ss' : 'NA';

    return frmt;
  }

  enableFormatControl(data_type: any): boolean {
    return !(['DATE', 'TIMESTAMP','TIMESTAMP_TZ', 'TIMESTAMP_NTZ'].includes(data_type))
  }

  setIntialPrecisionValue(target: any, prec: number): number {

    let _targetDataType = target.value.toUpperCase();
    if (target.previousItemData === null && ['NUMBER', 'FLOAT', 'CHAR', 'REAL', 'NUMERIC', 'DECIMAL', 'DOUBLE PRECISION', 'STRING'].includes(_targetDataType)) {
      return prec;
    } else {
      return _targetDataType === 'NUMBER' ? 38 : _targetDataType === 'FLOAT' ? 5 : _targetDataType === 'CHAR' ? 1 : _targetDataType === 'Decimal' ? 10 :
       _targetDataType === 'NUMERIC' ? 30 :_targetDataType === 'REAL' ? 15 :_targetDataType === 'DOUBLE PRECISION' ? 15 :_targetDataType === 'STRING' ? 15 : 255;
    }
  }

  scaleStatus(dataType: string): boolean {
    const dataTypeItem = this.dataTypeList.find(item => item.datatype === dataType);

    let dataTypeCategory: string;

    if (dataTypeItem) {
      dataTypeCategory = dataTypeItem.datacategory.toUpperCase();
    }

    return dataTypeCategory === 'WITHSCALENPREC';
  }

  scaleDisplayText(dataType: string, scaleVal: number): string {
    return !this.scaleStatus(dataType) ? 'NA' : scaleVal.toString();
  }

  setIntialScaleValue(target: any, scale: number): number {
    const dataTypeItem = this.dataTypeList.find(item => item.datatype === target.itemData.datatype);
    let dataTypeCategory: string;
    if (dataTypeItem) {
      dataTypeCategory = dataTypeItem.datacategory.toUpperCase();
    }
    if (dataTypeCategory === 'WITHSCALENPREC') {
      if (target.previousItemData === null) {
        return scale;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }

  precMaxLength(dataType: string): number {
    return dataType.toUpperCase() === 'VARCHAR' ? 8000 : dataType.toUpperCase() === 'FLOAT' ? 5 : dataType.toUpperCase() === 'NUMBER' ? 38 : dataType.toUpperCase() === 'DECIMAL' ? 10 :dataType.toUpperCase() === 'REAL' ? 15
     : dataType.toUpperCase() === 'DOUBLE PRECISION' ? 15 : dataType.toUpperCase() === 'NUMERIC' ? 30 : dataType.toUpperCase() === 'STRING' ? 15 : 20;
  }

  scaleMaxLength(dataType: string, precision: number): number {
    if (dataType.toUpperCase() === 'NUMBER' || dataType.toUpperCase() === 'FLOAT' || dataType.toUpperCase() === 'DECIMAL' || dataType.toUpperCase() === 'NUMERIC') {
      if (precision !== 0) {
        return precision - 1;
      } else {
        return 0;
      }
    }
  }

  precisionStatus(dataType: string): boolean {
    const dataTypeItem = this.dataTypeList.find(item => item.datatype === dataType);

    let dataTypeCategory: string;

    if (dataTypeItem) {
      dataTypeCategory = dataTypeItem.datacategory.toUpperCase();
    }

    return (dataTypeCategory === 'WITHSCALENPREC' || dataTypeCategory === 'ONLYPREC')
  }

  precisionDisplayText(dataType: string, precisionVal: number): string {
    return this.precisionStatus(dataType)?precisionVal.toString():'NA';
  }

  saveDataLayoutDetails(obj_id: number, obj_uri_id: number) {
    let attrDetailsParam = '';
    let attrSequence = 0;
    let dataTypePrefix = this.isFixedWidthFile ? 'CHAR' : null;
    this.dataLayoutDetails.forEach(element => {
      let prec = this.isFixedWidthFile ? element.column_width : element.prec;
      let scale = this.isFixedWidthFile ? 0 : element.scale;
      attrDetailsParam += (attrSequence !== 0 ? '^' : '') +
        '(' + obj_id + '|' + 'attr_seq' + '|\'' + obj_uri_id + '\'|\'' + element.at_nm + '\'|[\'' + dataTypePrefix + '\',' + prec + ',' +
        scale + ']|' + (element.att_des == 'null' ? null : ('\'' + element.att_des + '\'')) + '|' + ++attrSequence + '|' + element.m_att_id + '|'
        + element.m_obj_id + '|\'' + element.dt_frmt + '\'|' + (element.b_nm == 'null' ? null : ('\'' + element.b_nm + '\'')) + '|'
        + (element.is_mandatory == 'N' ? ('\'' + element.etl_nm + '\'') : null) + '|' + (element.is_mandatory == 'Y' ? 'False' : 'true') 
        + '|'+ 'current_date' + '|\'' + '9999-12-31' +  '\'|' + 'true'+ '|\'' + 'UI00100' + '\'|' + 'current_timestamp' + '|\'' + this.authenticateService.getUserInfo().user_id + '\'' +')';
    });

    const reqFormObj: any = {attr_lists: attrDetailsParam};
    const params = new HttpParams({
      fromObject: reqFormObj
    });

    return this.dataService.postData(environment.onboarding.layout.saveLayoutDetails, { name: 'add_attributes_meta'}, params);
    //return this.dataService.getData(environment.onboarding.layout.saveLayoutDetails, { name: 'add_attributes_meta', attr_lists: attrDetailsParam });
  }

  saveDataLayoutLandingDetails(objId: number, obj_uri_id: number) {
    let attrDetailsParam = '';
    let attrSequence = 0;
    this.dataLayoutDetails.forEach(element => {
      if (element.is_mandatory == 'N') {
        attrDetailsParam += (attrSequence !== 0 ? '^' : '') +
        '(' + objId + '|' + 'attr_seq' + '|\'' + obj_uri_id + '\'|\'' + element.etl_nm + '\'|[\'' + element.data_type + '\',' + element.prec + ',' +
        element.scale + ']|' + (element.att_des == 'null' ? null : ('\'' + element.att_des + '\'')) + '|' + ++attrSequence + '|' + element.m_att_id + '|'
        + element.m_obj_id + '|\'' + element.dt_frmt + '\'|' + (element.b_nm == 'null' ? null : ('\'' + element.b_nm + '\'')) + '|'
        + (element.is_mandatory == 'N' ? ('\'' + element.etl_nm + '\'') : null ) + '|' + (element.is_mandatory == 'N' ? 'True' : 'False') 
        + '|'+ 'current_date' + '|\'' + '9999-12-31' +  '\'|' + 'true'+ '|\'' + 'UI00100' + '\'|' + 'current_timestamp' + '|\'' + this.authenticateService.getUserInfo().user_id + '\'' +')';
      }
    });
    
    const reqFormObj: any = {attr_lists: attrDetailsParam};
    const params = new HttpParams({
      fromObject: reqFormObj
    });

    return this.dataService.postData(environment.onboarding.layout.saveLayoutDetails, { name: 'add_attributes_meta'}, params);
  }
}