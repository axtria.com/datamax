export class DataLayoutDetails {
    obj_id: number;
    m_obj_id: number;
    m_att_id: number;
    att_seq: number;
    at_nm: string;
    etl_nm: string;
    b_nm: string;
    att_des: string;
    is_mandatory: string;
    data_type: string;
    dt_frmt: string;
    scale: number;
    prec: number;
    column_width: number;
    t_obj_id: number;
    at_id: number;
    t_at_id: number;
    dt_typ: string;
    col_loc: number;
    t_col_loc: number;
}