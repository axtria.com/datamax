import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-poc-details',
  templateUrl: './poc-details.component.html',
  styleUrls: ['./poc-details.component.css']
})
export class PocDetailsComponent implements OnInit {

  dataProviderContent: any;
  index = 0;
  providerDetailsForm: FormGroup;
  items: FormArray;
  communicationGroupDropdown: string;
  data: any;

  @Input() objId: number;
  @Input() editparam: boolean;

  @ViewChild('addNewRuleForm', { static: false }) form: any;

  constructor(private dataService: RestService, private authenticateService: AuthenticateService, cs: ContentService, private fb: FormBuilder) {
    this.dataProviderContent = cs.getContent(environment.modules.dataOnboarding);
    this.communicationGroupDropdown = this.dataProviderContent.pocDetails.communicationGroupDropdown;
  }

  ngOnInit() {
    this.providerDetailsForm = this.fb.group({
      items: this.fb.array([this.createItem()])
    });

    if (this.editparam) {
      // delete the already created empty formarray
      this.deleteItem(0);

      //call the dataservice to fetch data
      this.dataService.getData(environment.onboarding.provider.getAgreementDetails, { name: 'get_data_agreement_details', object_id: this.objId })
        .subscribe(data => {

          this.data = data.Contents;

          // index set to one value less than lenght of datasets fetched
          this.index = this.data.length - 1;

          // for each dataset create items formarray and push the corresponding data values
          for (var n = 0; n < this.data.length; n++) {
            this.items = this.providerDetailsForm.get('items') as FormArray;
            this.items.push(this.pushItem(this.data[n].poc_group, this.data[n].poc_nm, this.data[n].poc_email, this.data[n].poc_ph));
          }

        });
    }

  }

  createItem(): FormGroup {
    return this.fb.group({
      poc_group: '',
      poc_nm: '',
      poc_email: '',
      poc_ph: ''
    });
  }

  getControls() {
    return (<FormArray>this.providerDetailsForm.get('items')).controls;
  }

  addItem(): void {
    this.index += 1;
    this.items = this.providerDetailsForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }


  deleteItem(i: number) {
    this.items = this.providerDetailsForm.get('items') as FormArray;
    this.items.removeAt(i);
  }

  pushItem(grp, nm, email, ph) {
    return this.fb.group({
      poc_group: grp,
      poc_nm: nm,
      poc_email: email,
      poc_ph: ph
    });
  }

  saveDataProviderDetails(name: string, obj_id: number, obj_uri_id: number) {
    let attrDetailsParam = '';
    let itemCount = 0;
    const is_active = 'Y';

    this.providerDetailsForm.value.items.forEach(element => {
      attrDetailsParam += (itemCount !== 0 ? '^' : '') + '(' + obj_id + '|' + obj_uri_id + '|\'' + element.poc_nm + '\'|\'' 
      + element.poc_email + '\'|\'' + element.poc_ph + '\'|\'' + this.authenticateService.getUserInfo().user_id + '\'|' 
      + 'current_date' +  '|\'' + element.poc_group + '\'|' +  'current_date' + '|\'' + is_active + '\'' + ')';

      ++itemCount;
    });

    return this.dataService.getData(environment.onboarding.layout.savePOCDetails, { name: name, conn_poc_lists: attrDetailsParam });
  }

  updateDataProviderDetails(name: string) {
    let pocDetailsParam = '';
    let itemCount = 0;
    let id_list = '';

    this.providerDetailsForm.value.items.forEach(element => {
      pocDetailsParam += (itemCount !== 0 ? '^' : '') + '(' + '\'' + element.poc_nm + '\'|\'' + element.poc_email 
      + '\'|\'' + element.poc_ph + '\'|\'' + this.authenticateService.getUserInfo().user_id + '\'|' + 'current_timestamp' 
      + '|\'' + element.poc_group + '\'' + ')';


      id_list += (itemCount !== 0 ? '|' : '') + this.data[itemCount].id;
      ++itemCount;
    });

    return this.dataService.getData(environment.onboarding.layout.updatePOCDetails, { name: name, conn_poc_list: pocDetailsParam, id_list: id_list });
  }

}
