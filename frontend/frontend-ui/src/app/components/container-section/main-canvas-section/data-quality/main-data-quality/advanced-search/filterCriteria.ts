export class FilterCriteria {
    data_layer_id: any;
    data_layer_name: any;
    obj_id: any;
    obj_nm: any;
    attr_id_lst: any;
    attr_nm_lst: any;
    bk_attr_id_lst: any;
    bk_attr_nm_lst: any;
    mstr_rule_id: any;
    visual_rule_desc: any;
    rule_desc: string;
    is_rule_actv: boolean;
    rule_svrt: any;
}