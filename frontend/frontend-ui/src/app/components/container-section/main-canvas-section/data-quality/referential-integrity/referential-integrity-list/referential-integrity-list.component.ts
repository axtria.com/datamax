import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DialogComponent, ButtonPropsModel, PositionDataModel } from '@syncfusion/ej2-angular-popups';
import { DataQualityDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataQualityDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { RiDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/RiDetails';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { CommandModel, IRow, Column, GridComponent, ToolbarService, ColumnChooserService, ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { closest, EmitType } from '@syncfusion/ej2-base';
import { ReferentialIntegrityFormComponent } from '../referential-integrity-form/referential-integrity-form.component';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';

@Component({
  selector: 'app-referential-integrity-list',
  templateUrl: './referential-integrity-list.component.html',
  styleUrls: ['./referential-integrity-list.component.css'],
  providers: [ToolbarService, ColumnChooserService]
})
export class ReferentialIntegrityListComponent implements OnInit {

  @ViewChild('addNewRIRuleDialog', { static: false }) addNewRIRuleDialog: DialogComponent;
  @ViewChild('RIListGrid', { static: false }) RIListGrid: GridComponent;
  @ViewChild('referentialIntegrityFormComponent', { static: false }) referentialIntegrityFormComponent: ReferentialIntegrityFormComponent;
  @Input() dataQualityDetails: DataQualityDetails;
  @Input() active: boolean;
  @Input() gridResultsCountRI: number;
  // @Output() updateRulesForSearch = new EventEmitter<any[]>()
  //@Output() totalRIRulesCount = new EventEmitter<number>();

  //search filter
  @Input('searchtxt') searchtxt: string; 
  @Input('layerToFilter') layerToFilter: number[]; 
  @Input('objToFilter') objToFilter: number[]; 
  @Input('attrToFilter') attrToFilter: number[];
  @Input('ruleToFilter') ruleToFilter: number[];
  @Input('severityToFilter') severityToFilter: string[];
  @Input('statusToFilter') statusToFilter: string[];
  @Output() totalRIRulesCount = new EventEmitter<number>();
  

  gridPageSettings: Object;
  gridToolbar: ToolbarItems[] | Object;
  public position: PositionDataModel = { X: 'right', Y: 'top' };
  dlgAnimationSettings: Object = { effect: 'SlideRight', duration: 600, delay: 0 };
  public gridCommands: CommandModel[];
  editRIDetails: RiDetails;
  RIRules: RiDetails[];
  filterMetadata = { count: 0 };
  // filteredRIRules: RiDetails[];
  isRuleEditMode: boolean = false;
  latest_rule_id = 0;
  riFormContent: any;
  dlgButtons: ButtonPropsModel[] = [];
  pageCounts = [1,2,3,4];
  currentPage = 1;  
  dataQualityContent: any;
  numberOfRIRules = 5;

  constructor(private dataService: RestService, cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.riFormContent = this.dataQualityContent.subMenu.referentialIntegrity;
    this.dlgButtons = [
      {
        click: this.dlgCancelBtnClick.bind(this), buttonModel: {
          content: this.riFormContent.buttons.cancel,
          cssClass: 'btn btn-secondary actionButton'
        }
      },
      {
        click: this.dlgSaveBtnClick.bind(this), buttonModel: {
          content: this.riFormContent.buttons.save,
          cssClass: 'btn btn-primary actionButton', disabled: false
        }
      }
    ];
  }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: true, pageSize:5, currentPage: this.currentPage, template: ''};    
    this.gridCommands = [{
        type: 'Edit', buttonOption: {
        iconCss: ' e-icons e-edit', cssClass: 'e-flat',
        click: this.editRIRule.bind(this)
      }
    }];

    this.setToolbar();
    // Polulate RI Rules
    this.populateRIRules();
    // this.filterRIRules(this.totalRIRules);
  }

  onRIListGridDataBound()
  {
    this.totalRIRulesCount.emit(this.filterMetadata.count);
  }

  setToolbar(){
    this.gridToolbar = [{ type: 'label', template: '#showRIResultsCount', align: 'Right' },
    { type: 'Input', template: '#addRIRuleTemplate', align: 'Right', id: 'addRule', click: this.addRuleHandler.bind(this) },
    { type: 'Input', template: '#exportRITemplate', align: 'Right' },
    { text: 'ColumnChooser', template: '#columnChooserTemplateRI', align: 'Right' }];
  }

  addRuleHandler(args: ClickEventArgs): void {
    if (args.item.id === "addRule") {
      this.addRIRule();
    }
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.numberOfRIRules = arg.itemData.value;
  }

  changePageNumber(currentPage: any) {
    this.currentPage = currentPage;
    this.gridPageSettings = { currentPage: this.currentPage };
  }

  populateRIRules() {
    this.dataService.getData(environment.dataQuality.getRIRules,
      { name: 'get_ri_rules', src_data_layer_id: 0, 
      obj_id: 0, attr_id: 0, rule_id: 0, 
      latest_rule_id: this.latest_rule_id })
      .subscribe(data => { 
        this.RIRules = data && data.Contents ? data.Contents : [];
        this.gridResultsCountRI = this.RIRules.length;
      });
  }

  openRIRuleForm(isRuleEditMode: boolean) {
    // Open the Referential Integrity Form Dialog with slide effect
    this.addNewRIRuleDialog.show();
    // Call the addNewRIRule on the Form
    if (!isRuleEditMode)
      this.referentialIntegrityFormComponent.addNewRIRule(this.dataQualityDetails);
  }

  editRIRule(args: Event): void {

    this.isRuleEditMode = true;
    this.openRIRuleForm(true);

    const rowObj: IRow<Column> = this.RIListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').getAttribute('data-uid'));

    const riDetails = rowObj.data as RiDetails;

    this.referentialIntegrityFormComponent.editRIRule(riDetails);
  }

  
  addRIRule() {
    this.isRuleEditMode = false;
    this.openRIRuleForm(false);
  }

  dlgCancelBtnClick() {
    // Reset the fields in form to defaults
    this.referentialIntegrityFormComponent.resetVariablesToDefaults();
    // Hide the RI Form
    this.addNewRIRuleDialog.hide();
  }

  public modalDlgClose: EmitType<Event> = () => {
    this.dlgCancelBtnClick();
  }

   dlgSaveBtnClick() {
     // Calling save function from Form Component
     this.referentialIntegrityFormComponent.saveRIRule();

     // Hide the Data Validation Form & Repopulate the Rules
    if (!this.referentialIntegrityFormComponent.addNewRIForm.invalid) {
      this.addNewRIRuleDialog.hide();
      //Fetch the latest rule id after rule save/update
    this.latest_rule_id = this.referentialIntegrityFormComponent.latest_rule_id;
    }
   }

   updateLatestRuleId(val:any){
    this.latest_rule_id = val;
    this.populateRIRules();
  }
}

