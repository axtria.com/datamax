import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DialogComponent, ButtonPropsModel, PositionDataModel } from '@syncfusion/ej2-angular-popups';
import { CommandModel, GridComponent, IRow, Column, ResizeService, ReorderService, ToolbarService, ColumnChooserService } from '@syncfusion/ej2-angular-grids';
import { NgForm } from '@angular/forms';
import { DataQualityDetails } from '../DataQualityDetails';
import { ThresholdDetails } from '../ThresholdDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { closest } from '@syncfusion/ej2-base';
import { ContentService } from 'src/app/shared/services/content.service';
import { CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns';

@Component({
  selector: 'app-threshold-validatons',
  templateUrl: './threshold-validatons.component.html',
  styleUrls: ['./threshold-validatons.component.css'],
  providers: [CheckBoxSelectionService, ResizeService, ReorderService, ToolbarService, ColumnChooserService]
})
export class ThresholdValidatonsComponent implements OnInit {

  @ViewChild('addNewThresholdRuleForm', { static: false }) addNewThresholdRuleForm: NgForm;
  @ViewChild('addNewThresholdRuleDialog', { static: false }) addNewThresholdRuleDialog: DialogComponent;
  @ViewChild('thresholdListGrid', { static: false }) thresholdListGrid: GridComponent;

  @Input() dataQualityDetails: DataQualityDetails;

  public commands: CommandModel[];
  toolbar: string[];
  position: PositionDataModel = { X: 'right', Y: 'top' };
  editThresholdDetails: ThresholdDetails;
  thresholdRules: ThresholdDetails[];

  status: boolean;
  isRuleEditMode: boolean = false;
  reportColList: any;

  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };

  objectList: [];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };

  attributeList: any[];
  timePeriodAttrList: any[];
  isTimePeriodAttr: boolean;
  attributeFields: object = { text: 'at_nm', value: 'at_id' };

  checkList: any[];
  checkFields: object = { text: 'visual_desc', value: 'mstr_rule_id' };
  activeCheckType = '';

  aggFuncList: string[];

  threshold: any;

  severityOptions: string[];

  thDetails: ThresholdDetails = {
    rule_id: null, processing_stage: 'Post-Ingestion', data_layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
    fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null, data_layer_nm: null,
    obj_nm: null, attr_nm: null, bk_attr_nm: null, trending_attr_id: null, dimension_attr_nm: null, trending_attr_nm: null,
    dimension_attr_id: null, visual_rule_desc: null
  };

  dlgButtons: ButtonPropsModel[];
  formInvalidAfterSaveClick: boolean = false;
  // disable : boolean = true;

  constructor(private dataService: RestService, private toastPopupService: ToastPopupService, cs: ContentService) {
    this.threshold = cs.getContent(environment.modules.dataQuality).subMenu.threshold;
    this.severityOptions = this.threshold.severityOptions;
    this.aggFuncList = this.threshold.aggFuncList;
    this.dlgButtons = [
      {
        click: this.dlgCancelBtnClick.bind(this), buttonModel: {
          content: this.threshold.buttons.cancel, isPrimary: true,
          cssClass: 'btn btn-outline-warning actionButton'
        }
      },
      {
        click: this.dlgSaveBtnClick.bind(this), buttonModel: {
          content: this.threshold.buttons.save, isPrimary: true,
          cssClass: 'btn btn-outline-warning actionButton', disabled: false
        }
      }
    ];
  }



  ngOnInit() {
    this.toolbar = ['ColumnChooser'];

    this.populateThresholdRules();
    this.commands = [{ buttonOption: { content: 'Edit', cssClass: 'e-flat', click: this.editThresholdRule.bind(this) } }];

    // Populate all the Data Threshold Rules
    this.populateDataLayer();

    // Populate Data Layer Dropdown
    this.populateCheckType();
  }

  populateThresholdRules() {
    this.dataService.getData(environment.dataQuality.getThresholdRules,
      { name: 'get_th_rules', obj_layer_nm: this.dataQualityDetails.layer_id, obj_id_value: this.dataQualityDetails.obj_id, attr_id_value: 0, rule_id_value: 0 })
      .subscribe(data => { this.thresholdRules = data && data.Contents ? data.Contents : []; });
  }

  populateDataLayer() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_business_layer' })
      .subscribe(data => { this.businessLayerList = data && data.Contents ? data.Contents : []; });
  }

  populateCheckType() {
    this.dataService.getData(environment.dataQuality.getCheckType,
      { name: 'get_check_type', datatype_value: 'Not Applicable', rule_type: 'Threshold' })
      .subscribe(data => { this.checkList = data && data.Contents ? data.Contents : []; });
  }

  dataLayerChange(e: any) {
    this.thDetails.obj_id = null;
    this.objectList = [];
    if (e.itemData !== null) {
      this.populateObject(e.itemData.layer_id);
    }
  }

  populateObject(dataLayer: string) {
    this.dataService.getData(environment.dataQuality.getObjects,
      { name: 'get_data_layer_objects', src_data_layer_id: dataLayer })
      .subscribe(data => {
        this.objectList = data && data.Contents ? data.Contents : [];

        // If the form is in EDIT mode then pre-set the Object Dropdown - It will fire objectChange() method
        this.thDetails.obj_id = this.isRuleEditMode ? this.editThresholdDetails.obj_id : this.dataQualityDetails.obj_id
      });
  }

  objectChange(e: any) {
    this.thDetails.attr_id = null;
    this.attributeList = [];
    if (e.itemData !== null) {
      this.populateAttribute(e.itemData.obj_id);
    }
  }

  checkTypeChange(e: any) {
    this.thDetails.attr_id = null;
    this.thDetails.bk_attr_id = null;
    this.thDetails.agg_fn = null;
    if (e.itemData !== null) {
      this.activeCheckType = e.itemData.visual_desc;
    }

  }

  populateAttribute(objectId: number) {
    this.dataService.getData(environment.dataQuality.getAttributes,
      { name: 'get_object_attribute_details', object_id: objectId })
      .subscribe(data => {
        this.attributeList = data && data.Contents ? data.Contents : [];
        this.timePeriodAttrList = this.attributeList.filter(function (item: any) {
            if (['DATE', 'TIMESTAMP', 'TIMESTAMPZ'].indexOf(item.dt_typ) !== -1) {
              return item;
            }
        });

        // If the form is in EDIT mode then pre-set the Attribute Dropdown - It will fire attributeChange() method
        if (this.isRuleEditMode) {
          this.thDetails.attr_id = this.editThresholdDetails.attr_id;
        }
      });
  }

  attributeChange(e: any) {
    this.thDetails.bk_attr_id = null;
    this.reportColList = [];

    if (e.itemData) {
      this.reportColList = this.attributeList.filter(function (item: any) {
        if (item.at_id !== e.itemData.at_id) {
          return item;
        }
      });
    }

    if (this.isRuleEditMode) {
      this.thDetails = this.editThresholdDetails;
      this.status = this.editThresholdDetails.is_rule_actv;
    }
  }

  multiAttributeSelected(e: any) {
    if (e.value != null) {
      this.reportColList = [];

      // Create Grouping Conditions List by excluding the Selected Attributes for Conditioanl Uniqueness Rule
      this.reportColList = this.attributeList.filter((item: any) => e.value.indexOf(item.at_id) < 0);
    }
  }

  reportColsChange(e: any) {
    this.isTimePeriodAttr = this.timePeriodAttrList.filter((item: any) => e.value.indexOf(item.at_id) < 0).length === this.timePeriodAttrList.length;
    if (e.value === null || e.value !== null && this.isTimePeriodAttr) {
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.threshold.popupMsg.timePeriodAttr));
    }
  }

  addNewThresholdRule() {
    //Reset the previously filled form if so
    this.addNewThresholdRuleForm.reset();

    // This is to set the data layer same as the one selected at the top
    this.thDetails.data_layer_id = this.dataQualityDetails.layer_id;

    //Set default severity and status
    this.thDetails.rule_svrt = this.severityOptions.length > 0 ? this.severityOptions[0] : '';
    this.thDetails.is_rule_actv = true;

    this.formInvalidAfterSaveClick = false;

    this.editThresholdDetails = null;
    // Open the Data Validation Form addNewThresholdRuleDialog with slide effect
    this.addNewThresholdRuleDialog.animationSettings = { effect: 'SlideRight', duration: 400 };
    this.addNewThresholdRuleDialog.show();
  }

  editThresholdRule(args: Event): void {
    let rowObj: IRow<Column> = this.thresholdListGrid.getRowObjectFromUID(closest(<Element>args.target, '.e-row').getAttribute('data-uid'));
    let record = <ThresholdDetails>rowObj.data;

    this.isRuleEditMode = true;

    this.thDetails = {
      rule_id: null, processing_stage: 'Post-Ingestion', data_layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
      fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null, data_layer_nm: null,
      obj_nm: null, attr_nm: null, bk_attr_nm: null, trending_attr_id: null, dimension_attr_nm: null, trending_attr_nm: null,
      dimension_attr_id: null, visual_rule_desc: null
    };

    let additionalRptColns = null;
    if (record.bk_attr_id != null) {
      additionalRptColns = record.bk_attr_id.toString().replace(record.attr_id.toString(), '')
        .replace(/\|/g, ' ').trim().split(' ').map(Number);
    }

    this.editThresholdDetails = {
      obj_id: record.obj_id,
      attr_id: record.attr_id,
      mstr_rule_id: record.mstr_rule_id,
      bk_attr_id: additionalRptColns.length === 1 && additionalRptColns[0] === 0 ?
        null : additionalRptColns,
      rule_id: record.rule_id,
      rule_desc: record.rule_desc,
      processing_stage: record.processing_stage,
      data_layer_id: record.data_layer_id,
      grnl_lvl: record.grnl_lvl,
      rule_cat: 'A',
      rule_svrt: record.rule_svrt,
      is_rule_actv: record.is_rule_actv,
      agg_fn: record.agg_fn,
      th_val: record.th_val,
      fltr_con: record.fltr_con,
      actv_disp: record.actv_disp,
      data_layer_nm: record.data_layer_nm,
      obj_nm: record.obj_nm, 
      attr_nm: record.attr_nm, 
      bk_attr_nm: record.bk_attr_nm,
      trending_attr_id: '',
      dimension_attr_nm: '', 
      trending_attr_nm: '',
      dimension_attr_id: '',
      visual_rule_desc: ''
    };

    // If the form is in EDIT mode then pre-set the DataLayer Dropdown - It will fire dataLayerChange() method
    this.thDetails.data_layer_id = this.editThresholdDetails.data_layer_id;

    // Open the Data Validation Form addNewThresholdRuleDialog with slide effect
    this.addNewThresholdRuleDialog.animationSettings = { effect: 'SlideRight', duration: 400 };
    this.addNewThresholdRuleDialog.show();

  }

  dlgCancelBtnClick() {
    this.thDetails = {
      rule_id: null, processing_stage: 'Post-Ingestion', data_layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
      fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null, data_layer_nm: null,
      obj_nm: null, attr_nm: null, bk_attr_nm: null, trending_attr_id: null, dimension_attr_nm: null, trending_attr_nm: null,
    dimension_attr_id: null, visual_rule_desc: null
    };

    // Reset the Form
    this.addNewThresholdRuleForm.reset();
    this.formInvalidAfterSaveClick = false;
    //this.severityClicked = false;

    this.thDetails.is_rule_actv = false;
    this.thDetails.rule_svrt = null;
    this.isRuleEditMode = false;
    this.addNewThresholdRuleDialog.hide();
  }

  dlgSaveBtnClick() {
    if (this.addNewThresholdRuleForm.invalid) {
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.threshold.popupMsg.invalid));
      this.formInvalidAfterSaveClick = this.addNewThresholdRuleForm.invalid;
    }
    // else {
    //   if (!this.isRuleEditMode) {
    //     this.dataService.getData(environment.dataQuality.addThresholdRule,
    //       {
    //         name: 'add_th_rule',
    //         obj_id: this.thDetails.obj_id,
    //         mstr_rule_id: this.thDetails.mstr_rule_id,
    //         attr_id: this.thDetails.attr_id,
    //         bk_attr_id: this.thDetails.bk_attr_id && this.thDetails.bk_attr_id.toString() !== '' ?
    //           this.thDetails.attr_id.toString().concat('|', this.thDetails.bk_attr_id.toString().replace(/,/g, '|')) :
    //           this.thDetails.attr_id,
    //         processing_stage: this.thDetails.processing_stage,
    //         layer_id: this.thDetails.layer_id,
    //         grnl_lvl: this.thDetails.grnl_lvl,
    //         agg_fn: this.thDetails.agg_fn,
    //         th_val: this.thDetails.th_val,
    //         fltr_con: this.thDetails.fltr_con,
    //         rule_desc: this.thDetails.rule_desc,
    //         rule_svrt: this.thDetails.rule_svrt.slice(0, 1),
    //         rule_cat: this.thDetails.rule_cat,
    //         is_rule_actv: this.thDetails.is_rule_actv ? 'Y' : 'N'
    //       })
    //       .subscribe(data => {
    //         if (data.Contents.length > 0) {
    //           if (data.Contents[0].status.toUpperCase() != 'FAIL') {
    //             this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.threshold.popupMsg.saved));
    //             this.populateThresholdRules();

    //             this.thDetails = {
    //               rule_id: null, processing_stage: 'Post-Ingestion', layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
    //               fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null
    //             };
    //             this.status = false;
    //             this.isRuleEditMode = false;
    //             this.addNewThresholdRuleDialog.hide();
    //           } else {
    //             this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(data.Contents[0].message));
    //           }
    //         }
    //       });
    //   }
    //   if (this.isRuleEditMode) {
    //     this.dataService.getData(environment.dataQuality.updateThresholdRules,
    //       {
    //         name: 'update_th_rule',
    //         bk_attr_id_value: this.thDetails.bk_attr_id && this.thDetails.bk_attr_id.toString() !== '' ?
    //           this.thDetails.attr_id.toString().concat('|', this.thDetails.bk_attr_id.toString().replace(/,/g, '|')) :
    //           this.thDetails.attr_id,
    //         agg_fn_value: this.thDetails.agg_fn,
    //         th_val_value: this.thDetails.th_val,
    //         fltr_con: this.thDetails.fltr_con,
    //         rule_desc_value: this.thDetails.rule_desc,
    //         rule_svrt: this.thDetails.rule_svrt.slice(0, 1),
    //         rule_cat: this.thDetails.rule_cat,
    //         is_rule_actv: this.thDetails.is_rule_actv ? 'Y' : 'N',
    //         rule_id: this.thDetails.rule_id
    //       })
    //       .subscribe(() => {
    //         this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.threshold.popupMsg.updated));
    //         this.populateThresholdRules();
    //       });
    //     this.thDetails = {
    //       rule_id: null, processing_stage: 'Post-Ingestion', layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
    //       fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null
    //     };
    //     this.status = false;
    //     this.isRuleEditMode = false;
    //     this.addNewThresholdRuleDialog.hide();
    //   }
    // }

  }
}
