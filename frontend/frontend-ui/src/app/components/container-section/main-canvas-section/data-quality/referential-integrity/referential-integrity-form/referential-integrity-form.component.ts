import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { DataQualityDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataQualityDetails';
import { RiDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/RiDetails';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { RestService } from 'src/app/shared/services/rest.service';

@Component({
  selector: 'app-referential-integrity-form',
  templateUrl: './referential-integrity-form.component.html',
  styleUrls: ['./referential-integrity-form.component.css']
})

export class ReferentialIntegrityFormComponent implements OnInit {

  @ViewChild('addNewRIForm', { static: false }) addNewRIForm: any;
  @Output() latestRuleIdEmitter = new EventEmitter<number>();

  dataQualityDetails: DataQualityDetails;
  editRIDetails: RiDetails;
  RIRules: RiDetails[];
  status: boolean;
  isRuleEditMode = false;
  ruleAlreadyExist = false;
  ruleAlreadyExistMsg = '';
  latest_rule_id = 0;
  businessLayerList: [];
  referenceLayerList: any[];
  filteredReferenceObjectList: any[];
  layerFields: object = { text: 'name', value: 'layer_id' };

  objectList: [];
  referenceObjectList: [];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };

  attributeList: [];
  referenceAttributeList: [];
  attributeFields: object = { text: 'etl_nm', value: 't_at_id' };

  reportColList: any[];

  checkTypeList: any = [];
  checkTypeFields: object = { text: 'visual_rule_desc', value: 'mstr_rule_id' };

  riFormContent: any;
  severityOptions: string[];
  activeSeverityOption: string;
  commonContent: any;
  applyDropdownList: any[];
  activeApplyDropdownList: string;
  formInvalidAfterSaveClick = false;
  severityClicked = false;
  public filterPlaceholder: any = 'Search';

  riDetails: RiDetails = {
    rule_id: null, data_layer_id: null, mstr_rule_id: null, obj_id: null, attr_id: null, bk_attr_id: null,
    grnl_lvl: "ATTRIBUTE", fltr_con: "", ri_data_layer_id: null, ri_obj_id: null,
    ri_attr_id: null, ri_fltr_con: "", rule_desc: null, rule_svrt: null, is_rule_actv: null, data_layer_nm: null,
    ri_data_layer_nm: null, obj_nm: null, ri_obj_nm: null, attr_nm: null, bk_attr_nm: null,
    ri_attr_nm: null, mstr_rule_desc: null,
  };

  constructor(private dataService: RestService, cs: ContentService, private toastPopupService: ToastPopupService, private authenticateService: AuthenticateService) {
    this.riFormContent = cs.getContent(environment.modules.dataQuality).subMenu.referentialIntegrity;
    this.severityOptions = this.riFormContent.severityOptions;
    this.commonContent = cs.getContent(environment.modules.dataQuality).commonContent;
    this.applyDropdownList = this.commonContent.applyDropdownList;
    this.activeApplyDropdownList = this.applyDropdownList[0];
  }

  ngOnInit() {
    this.populateDataLayer();
    this.populateCheckType();
  }

  populateRIRules() {
    this.dataService.getData(environment.dataQuality.getRIRules,
      { name: 'get_ri_rules', src_data_layer_id: this.dataQualityDetails.layer_id, obj_id: this.dataQualityDetails.obj_id, attr_id: 0, rule_id: 0, latest_rule_id: this.latest_rule_id })
      .subscribe(data => { this.RIRules = data && data.Contents ? data.Contents : []; });
  }

  populateDataLayer() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_obj_meta_data_layers' })
      .subscribe(data => { if (data && data.Contents) { this.referenceLayerList = data.Contents; this.businessLayerList = data.Contents; } });
  }

  populateCheckType() {
    this.dataService.getData(environment.dataQuality.getCheckType,
      { name: 'get_dq_rule_checks', rule_type: 'Referential Integrity' })
      .subscribe(data => {
        this.checkTypeList = data && data.Contents ? data.Contents : [];
      });

  }

  layerChange(e: any, n: number) {
    if (n === 0) {
      this.riDetails.obj_id = null;
      this.objectList = [];
      
      if (e.itemData !== null)
        this.riDetails.ri_data_layer_id = e.itemData.layer_id;
    }
    //if (n === 1) {
    this.riDetails.ri_obj_id = null;
    this.referenceObjectList = [];
    //}
    if (e.itemData !== null) {
      this.populateObject(e.itemData.layer_id, n);
    }

  }

  populateObject(dataLayerName: string, n: number) {
    this.dataService.getData(environment.dataQuality.getObjects,
      { name: 'get_data_layer_objects', src_data_layer_id: dataLayerName })
      .subscribe(data => {
        if (n === 0) {
          this.objectList = data && data.Contents ? data.Contents : [];

          // If the form is in EDIT mode then pre-set the Object Dropdown - It will fire objectChange() method
          if (this.isRuleEditMode) {
            this.riDetails.obj_id = this.editRIDetails.obj_id;
          } else {
            this.riDetails.obj_id = this.dataQualityDetails.obj_id;
          }
        }
        if (n === 1) {
          this.referenceObjectList = data && data.Contents ? data.Contents : [];
          // If the form is in EDIT mode then pre-set the Ref Object Dropdown - It will fire refObjectChange() method
          if (this.isRuleEditMode) {
            this.riDetails.obj_id = this.editRIDetails.obj_id;
            this.riDetails.ri_obj_id = this.editRIDetails.ri_obj_id;
          }
          // Filter refernce object list
          this.filterRefObjList(this.riDetails.obj_id);
        }
      });
  }

  filterRefObjList(objId: number) {
    if ((objId !== 0 || objId !== null) && this.referenceObjectList !== undefined) {
      this.filteredReferenceObjectList = [];
      this.filteredReferenceObjectList = this.referenceObjectList.filter(function (item: any) {
        if (item.obj_id !== objId) {
          return item;
        }
      });
    }
  }

  objectChange(e: any, n: number) {
    if (n === 0) {
      this.riDetails.attr_id = null;
      this.attributeList = [];
      if (e.itemData !== null) {
        this.filterRefObjList(e.itemData.obj_id);
      }
    }
    if (n === 1) {
      this.riDetails.ri_attr_id = null;
      this.referenceAttributeList = [];
    }

    if (e.itemData !== null) {
      this.populateAttribute(e.itemData.obj_id, n);
    }
  }

  populateAttribute(objectId: number, n: number) {
    this.dataService.getData(environment.dataQuality.getAttributes,
      { name: 'get_object_attribute_details', obj_id: objectId })
      .subscribe(data => {
        if (n === 0) {
          this.attributeList = data && data.Contents ? data.Contents : [];

          // If the form is in EDIT mode then pre-set the Attribute Dropdown - It will fire objectChange() method
          if (this.isRuleEditMode) {
            this.riDetails.attr_id = this.editRIDetails.attr_id;
          }
        }
        if (n === 1) {
          this.referenceAttributeList = data && data.Contents ? data.Contents : [];
          if (this.isRuleEditMode) {
            this.riDetails.ri_attr_id = this.editRIDetails.ri_attr_id;
          }
        }
      });
  }

  attributeChange(e: any) {
    this.riDetails.bk_attr_id = [];
    this.reportColList = [];

    if (e.itemData) {
      this.populateRptColList(e.itemData.t_at_id);
      this.dataService.getData(environment.dataQuality.checkDuplicityRIRule,
        {
          name: 'ri_dup_check',
          obj_id: this.riDetails.obj_id,
          attr_id: e.itemData.t_at_id,
          mstr_rule_id: this.checkTypeList[0].mstr_rule_id
        })
        .subscribe(data => {
          this.ruleAlreadyExistMsg = data.Contents[0].message;
          if (data.Contents[0].status === "False") {
            this.ruleAlreadyExist = true;
            this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.ruleAlreadyExistMsg));
          } else {
            this.ruleAlreadyExist = false;
          }
        });
    }
  }

  populateRptColList(attr_id: number) {
    this.riDetails.bk_attr_id = [];
    this.reportColList = [];

    if (attr_id !== null) {
      this.reportColList = this.attributeList.filter(function (item: any) {
        if (item.t_at_id !== attr_id) {
          return item;
        }
      });
    }
  }

  refAttributeChange(e: any) {
    if (e.itemData) {
      if (this.isRuleEditMode) {
        this.riDetails = this.editRIDetails;
      }
    }
  }

  addNewRIRule(dataQualityDetails: DataQualityDetails) {
    //Reset the previously filled form
    this.resetVariablesToDefaults();

    // This is to set the Main Data Quality Model to the local variable
    this.dataQualityDetails = dataQualityDetails;

    // This is to reset the default controls on the Form
    this.riDetails.data_layer_id = this.dataQualityDetails.layer_id === 0 ? null : this.dataQualityDetails.layer_id;

    this.severityClicked = false;

    //Set default severity and status
    this.riDetails.mstr_rule_id = this.checkTypeList.length > 0 ? this.checkTypeList[0].mstr_rule_id : null;
    this.riDetails.rule_desc = this.checkTypeList.length > 0 ? this.checkTypeList[0].mstr_rule_desc : null;
    this.riDetails.is_rule_actv = true;
    this.riDetails.rule_svrt = this.severityOptions.length > 0 ? this.severityOptions[0] : '';
    this.editRIDetails = null;
  }

  resetVariablesToDefaults() {
    this.riDetails = {
      rule_id: null, data_layer_id: null, mstr_rule_id: null, obj_id: null, attr_id: null, bk_attr_id: null,
      grnl_lvl: "ATTRIBUTE", fltr_con: "", ri_data_layer_id: null, ri_obj_id: null,
      ri_attr_id: null, ri_fltr_con: "", rule_desc: null, rule_svrt: null, is_rule_actv: null, data_layer_nm: null,
      ri_data_layer_nm: null, obj_nm: null, ri_obj_nm: null, attr_nm: null, bk_attr_nm: null,
      ri_attr_nm: null, mstr_rule_desc: null
    };
    this.objectList = [];
    this.attributeList = [];
    this.reportColList = [];

    // Reset the form
    this.addNewRIForm.reset();

    // To highlight the mandory or default input error message
    this.formInvalidAfterSaveClick = false;

    this.isRuleEditMode = false;
    this.activeSeverityOption = this.severityOptions.length > 0 ? this.severityOptions[0] : '';
  }

  saveRIRule() {
    if (this.addNewRIForm.invalid) {
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.riFormContent.popupMsg.invalid));
      this.formInvalidAfterSaveClick = this.addNewRIForm.invalid;
    }
    else {
      if (!this.isRuleEditMode) {
        if (this.ruleAlreadyExist) {
          this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.ruleAlreadyExistMsg));
        } else {
          this.dataService.getData(environment.dataQuality.addRIRule,
            {
              name: 'add_dq_ri_rule',
              obj_id: this.riDetails.obj_id,
              mstr_rule_id: this.riDetails.mstr_rule_id,
              attr_id: this.riDetails.attr_id,
              bk_attr_id: this.riDetails.bk_attr_id && this.riDetails.bk_attr_id.toString() !== '' ?
                this.riDetails.attr_id.toString().concat('|', this.riDetails.bk_attr_id.toString()
                  .replace(/,/g, '|')) : this.riDetails.attr_id,
              data_layer_id: this.riDetails.data_layer_id,
              grnl_lvl: this.riDetails.grnl_lvl,
              fltr_con: this.riDetails.fltr_con == null || this.riDetails.fltr_con === '' ? '1=1' : this.riDetails.fltr_con,
              ri_obj_id: this.riDetails.ri_obj_id,
              ri_attr_id: this.riDetails.ri_attr_id,
              ri_fltr_con: this.riDetails.ri_fltr_con == "" || this.riDetails.ri_fltr_con === null ? '1=1' : this.riDetails.ri_fltr_con,
              rule_desc: this.riDetails.rule_desc,
              rule_svrt: this.riDetails.rule_svrt !== null ? this.riDetails.rule_svrt.slice(0, 1) : null,
              is_rule_actv: this.riDetails.is_rule_actv,
              audit_insrt_id: this.authenticateService.getUserInfo().user_id,
              job_desc: ''
            })
            .subscribe(data => {
              if (data.Contents.length > 0) {
                if (data.Contents[0].status.toUpperCase() != 'FAIL') {
                  this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.riFormContent.popupMsg.saved));
                  this.latest_rule_id = data.Contents[0].latest_rule_id;
                  this.latestRuleIdEmitter.emit(this.latest_rule_id);

                  this.riDetails = {
                    rule_id: null, data_layer_id: null, mstr_rule_id: null, obj_id: null, attr_id: null, bk_attr_id: null,
                    grnl_lvl: "ATTRIBUTE", fltr_con: "", ri_data_layer_id: null, ri_obj_id: null,
                    ri_attr_id: null, ri_fltr_con: "", rule_desc: null, rule_svrt: null, is_rule_actv: null,
                    data_layer_nm: null, ri_data_layer_nm: null, obj_nm: null, ri_obj_nm: null, attr_nm: null, bk_attr_nm: null,
                    ri_attr_nm: null, mstr_rule_desc: null,
                  };
                  this.status = false;
                  this.isRuleEditMode = false;
                }
                else {
                  this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(data.Contents[0].message));
                }
              }
            });
        }
      }

      if (this.isRuleEditMode) {
        this.dataService.getData(environment.dataQuality.updateRIRules,
          {
            name: 'update_dq_ri_rule',
            bk_attr_id: this.riDetails.bk_attr_id && this.riDetails.bk_attr_id.toString() !== '' ?
              this.riDetails.attr_id.toString().concat('|', this.riDetails.bk_attr_id.toString().replace(/,/g, '|')) :
              this.riDetails.attr_id,
            fltr_con: this.riDetails.fltr_con == null || this.riDetails.fltr_con === '' ? '1=1' : this.riDetails.fltr_con,
            ri_fltr_con: this.riDetails.ri_fltr_con == null || this.riDetails.ri_fltr_con === '' ? '1=1' : this.riDetails.ri_fltr_con,
            rule_desc: this.riDetails.rule_desc,
            rule_id: this.riDetails.rule_id,
            rule_svrt: this.riDetails.rule_svrt !== null ? this.riDetails.rule_svrt.slice(0, 1) : null,
            is_rule_actv: this.riDetails.is_rule_actv,
            audit_updt_id: this.authenticateService.getUserInfo().user_id,
            ri_obj_id: this.riDetails.ri_obj_id,
            ri_attr_id: this.riDetails.ri_attr_id
          })
          .subscribe(data => {
            this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.riFormContent.popupMsg.updated));
            this.latest_rule_id = data.Contents[0].latest_rule_id;
            this.latestRuleIdEmitter.emit(this.latest_rule_id);
          });
        this.riDetails = {
          rule_id: null, data_layer_id: null, mstr_rule_id: null, obj_id: null, attr_id: null, bk_attr_id: null,
          grnl_lvl: "ATTRIBUTE", fltr_con: "", ri_data_layer_id: null, ri_obj_id: null,
          ri_attr_id: null, ri_fltr_con: "", rule_desc: null, rule_svrt: null, is_rule_actv: null,
          data_layer_nm: null, ri_data_layer_nm: null, obj_nm: null, ri_obj_nm: null, attr_nm: null, bk_attr_nm: null,
          ri_attr_nm: null, mstr_rule_desc: null,
        };
        this.status = false;
        this.isRuleEditMode = false;
      }

    }
  }

  editRIRule(record: RiDetails): void {

    this.resetVariablesToDefaults();
    this.isRuleEditMode = true;

    const additionalRptColns = record.bk_attr_id !== null ? record.bk_attr_id.toString().replace(record.attr_id.toString(), '')
      .replace(/\|/g, ' ').trim().split(' ').map(Number) : [];

    this.editRIDetails = {
      rule_id: record.rule_id,
      obj_id: record.obj_id,
      attr_id: record.attr_id,
      mstr_rule_id: record.mstr_rule_id,
      bk_attr_id: additionalRptColns.length === 1 && additionalRptColns[0] === 0 ? null : additionalRptColns,
      ri_data_layer_id: record.ri_data_layer_id,
      ri_obj_id: record.ri_obj_id,
      ri_attr_id: record.ri_attr_id,
      ri_fltr_con: record.ri_fltr_con == "1=1" ? null : record.ri_fltr_con,
      rule_desc: record.rule_desc,
      data_layer_id: record.data_layer_id,
      grnl_lvl: record.grnl_lvl,
      rule_svrt: record.rule_svrt,
      is_rule_actv: record.is_rule_actv,
      fltr_con: record.fltr_con == "1=1" ? null : record.fltr_con,
      data_layer_nm: record.data_layer_nm,
      ri_data_layer_nm: record.ri_data_layer_nm,
      obj_nm: record.obj_nm,
      ri_obj_nm: record.ri_obj_nm,
      attr_nm: record.attr_nm,
      bk_attr_nm: record.bk_attr_nm,
      ri_attr_nm: record.ri_attr_nm,
      mstr_rule_desc: record.mstr_rule_desc,
    };

    // If the form is in EDIT mode then pre-set the DataLayer Dropdown - It will fire dataLayerChange() method

    this.riDetails.data_layer_id = this.editRIDetails.data_layer_id;
    this.dataService.getData(environment.dataQuality.getAttributes,
      { name: 'get_object_attribute_details', obj_id: this.editRIDetails.obj_id })
      .subscribe(data => {
        this.attributeList = data && data.Contents ? data.Contents : [];
        this.populateRptColList(this.editRIDetails.attr_id);
      });
    this.riDetails.ri_data_layer_id = this.editRIDetails.ri_data_layer_id;
  }

}
