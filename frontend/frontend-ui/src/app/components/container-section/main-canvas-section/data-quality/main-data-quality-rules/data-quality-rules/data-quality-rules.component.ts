import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { TabAnimationSettingsModel } from '@syncfusion/ej2-angular-navigations';
import { UtilService } from 'src/app/shared/services/util.service';
import { DataQualityDetails } from '../../DataQualityDetails';
import { DataValidationListComponent } from '../../data-validations/data-validation-list/data-validation-list.component';
import { DataValidationFormComponent } from '../../data-validations/data-validation-form/data-validation-form.component';
import { ReferentialIntegrityListComponent } from '../../referential-integrity/referential-integrity-list/referential-integrity-list.component';
import { ReferentialIntegrityFormComponent } from '../../referential-integrity/referential-integrity-form/referential-integrity-form.component';
import { ThresholdValidatonListComponent } from '../../threshold-validatons/threshold-validaton-list/threshold-validaton-list.component';
import { KpiValidationFormComponent } from '../../kpi-validation/kpi-validation-form/kpi-validation-form.component';
import { ThresholdValidatonFormComponent } from '../../threshold-validatons/threshold-validaton-form/threshold-validaton-form.component';
import { KpiValidationListComponent } from '../../kpi-validation/kpi-validation-list/kpi-validation-list.component';
import { DataControlFormComponent } from '../../data-cont/data-control-form/data-control-form.component';
import { DataControlListComponent } from '../../data-cont/data-control-list/data-control-list.component';


@Component({
  selector: 'app-data-quality-rules',
  templateUrl: './data-quality-rules.component.html',
  styleUrls: ['./data-quality-rules.component.css']
})
export class DataQualityRulesComponent implements OnInit {
  
    utilService = new UtilService;
    // filter-search start
    searchtxt: string;
    layerToFilter: number[];
    objToFilter: number[];
    attrToFilter: number[];
    ruleToFilter: string[];
    statusToFilter: string[];
    severityToFilter: string[];
    attributesForFilter = [];
    allFilterCriteria = [];
    activeOptions: string[];
    severityOptions: string[];
    selectedAttributes = [];
   // filter-search end
    dataQualityContent: any;
    headerTextMainNav: any;
    headerText: any;
    businessLayerList: [];
    businessLayerFields: object = { value: 'layer_id', text: 'name' };
    objectList: [];
    refObjectList: [];
    objectFields: object = { value: 'obj_id', text: 'obj_nm' };
    attributeOptions: any[];
    refAttributeOptions: any[];
    attributeFields: object = { text: 'attr_nm', value: 'attr_id' };
    ruleTypeOptions: [];
    ruleTypeFields: object = { text: 'visual_rule_desc', value: 'mstr_rule_id' };
    dataQualityDetails: DataQualityDetails = {
      obj_id: 0, attr_id: 0, rule_id: 0, obj_name: null, layer_id: 0
    };
    rulesCountList: any[];
  
    activeTab: any;
    tabAnimationSettings: TabAnimationSettingsModel = { previous: { effect: "None" }, next: { effect: "None" } };
  
    @ViewChild('dataValidationList', { static: false }) objdataValidationListComponent: DataValidationListComponent;
    @ViewChild('dataValidationForm', { static: false }) objdataValidationFormComponent: DataValidationFormComponent;
    @ViewChild('referentialIntegrityList', { static: false }) objRIListComponent: ReferentialIntegrityListComponent;
    @ViewChild('referentialIntegrityForm', { static: false }) objRIFormComponent: ReferentialIntegrityFormComponent;
    @ViewChild('thresholdValidationList', { static: false }) objThresholdValidationList: ThresholdValidatonListComponent;
    @ViewChild('thresholdValidationForm', { static: false }) objThresholdValidationForm: ThresholdValidatonFormComponent;
    @ViewChild('kpiValidationForm', { static: false }) objKpiValidationForm: KpiValidationFormComponent;
    @ViewChild('kpiValidationList', { static: false }) objkpiValidationList: KpiValidationListComponent;
    @ViewChild('dataControlList', { static: false }) objDataControlList: DataControlListComponent;
    @ViewChild('dataControlForm', { static: false }) objDataControlForm: DataControlFormComponent;
    @Input() objId: number;
    resultsCountRI: number;
    resultsCountDV: number;
    resultsCountTH: number;
    resultsCountKPI: number;
    resultsCountDC: number;
  
    constructor(private dataService: RestService, cs: ContentService) {
      this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
      this.headerTextMainNav = this.dataQualityContent.menu.headerText;
      this.headerText = this.dataQualityContent.subMenu.headerText;
      this.activeOptions = this.dataQualityContent.commonContent.activeOptions;
      this.severityOptions = this.dataQualityContent.commonContent.severityOptions;
    }
  
    ngOnInit() {
      this.dataService.getData(environment.dataQuality.getBusinessLayer,
        { name: 'get_obj_meta_data_layers' })
        .subscribe(data => { this.businessLayerList = data && data.Contents ? data.Contents : []; });
      this.getRulesCount();
      this.activeTab = this.headerText[0].text;
  
      this.dataService.getData(environment.dataQuality.getCheckType,
        { name: 'get_dq_rule_checks', rule_type: 'Data Validation' })
        .subscribe(data => {
          this.ruleTypeOptions = data && data.Contents ? data.Contents : [];
        });
  
      this.allFilterCriteria = [{ name: "Attribute", value: "attr_id", dataSource: this.attributeOptions, fields: this.attributeFields },
      { name: "Rule Type", value: "mstr_rule_id", dataSource: this.ruleTypeOptions, fields: this.ruleTypeFields },
      { name: "Active", value: "is_rule_actv", dataSource: this.activeOptions, fields: "" },
      { name: "Severity", value: "rule_svrt", dataSource: this.severityOptions, fields: "" }];
      this.attributesForFilter = this.allFilterCriteria;
    }
  
    ////////// search filter  starts /////////////////
    businessLayerChange(e: any) {
      if (e.value !== null) {
        this.dataQualityDetails.layer_id = e.value.toString().replace(/\,/g, '|');
        this.layerToFilter = e.value;
        this.objToFilter = [];
        this.attrToFilter = [];
      }
      this.dataQualityDetails.obj_id = null;
  
      if (e.value !== undefined) {
        this.dataService.getData(environment.dataQuality.getObjects,
          { name: 'get_data_layer_objects', src_data_layer_id: this.dataQualityDetails.layer_id === null ? 0 : this.dataQualityDetails.layer_id })
          .subscribe(data => { this.objectList = data && data.Contents ? data.Contents : [] });
      }
    }
  
    objectChange(e: any) {
      if (e.value !== null) {
        this.dataQualityDetails.obj_id = e.value.toString().replace(/\,/g, '|');
        this.objToFilter = e.value;
        this.attrToFilter = [];
      }
      this.dataQualityDetails.attr_id = null;
      if (e.value !== undefined) {
        this.dataService.getData(environment.dataQuality.getAllAttributes,
          { name: 'object_filter', obj_id: this.dataQualityDetails.obj_id === null ? 0 : this.dataQualityDetails.obj_id })
          .subscribe(data => { this.attributeOptions = data && data.Contents ? data.Contents : [] });
      }
    }
  
    colsSelected(val:string, e: any) {
      if (e.value !== null) {
        if(val==='attr_id'){
          this.attrToFilter = e.value;
        }
        if(val==='mstr_rule_id'){
          this.ruleToFilter = e.value;
        }
        if(val==='is_rule_actv'){
          const status = e.value.map(item => {
            return item==="Yes";
        });
        this.statusToFilter = status;
        }
        if(val==='rule_svrt'){
          this.severityToFilter = e.value;
        }
      }
    }
    ruleTypeChange(e: any) {
      if (e.value !== null) {
        this.ruleToFilter = e.value;
      }
    }
  
    attributeSelected(attrVal: any) { 
      this.attributesForFilter = this.utilService.removeArrayPropItem(this.attributesForFilter, 'value', attrVal);
      this.selectedAttributes.push(attrVal);
    }
  
    deleted(toDelete: any) {
      this.attributesForFilter.push(toDelete);
      this.selectedAttributes = this.utilService.removeArrayItem(this.selectedAttributes, toDelete);
      if (toDelete.value === "attr_id") {
        this.attrToFilter = [];
      }
      if (toDelete.value === "mstr_rule_id") {
        this.ruleToFilter = [];
      }
      if (toDelete.value === "is_rule_actv") {
        this.statusToFilter = [];
      }
      if (toDelete.value === "rule_svrt") {
        this.severityToFilter = [];
      }
    }
  
    clearFilter(){
      this.attributesForFilter = this.allFilterCriteria;
      this.searchtxt = '';
      this.selectedAttributes = []
      this.layerToFilter = [];
      this.objToFilter = [];
      this.attrToFilter = [];
      this.ruleToFilter = [];
      this.statusToFilter = [];
      this.severityToFilter = [];
      this.dataQualityDetails.layer_id = null;
      this.dataQualityDetails.obj_id = null;
      this.dataQualityDetails.attr_id = null;
    }
  
    ////////// search filter ends  /////////////////
  
    select(e: any) {
      if (e.selectedItem.textContent.startsWith(this.headerText[0].text)) {
        this.ruleToFilter = [];
        this.dataQualityDetails.rule_id = null;
        this.activeTab = this.headerText[0].text;
      }
      if (e.selectedItem.textContent.startsWith(this.headerText[1].text)) {
        this.ruleToFilter = [];
        this.dataQualityDetails.rule_id = null;
        this.activeTab = this.headerText[1].text;
      }
      if (e.selectedItem.textContent.startsWith(this.headerText[2].text)){
        this.ruleToFilter = [];
        this.dataQualityDetails.rule_id = null;
        this.activeTab = this.headerText[2].text;
      }
      if (e.selectedItem.textContent.startsWith(this.headerText[3].text)) {
        this.ruleToFilter = [];
        this.dataQualityDetails.rule_id = null;
        this.activeTab = this.headerText[3].text;
      }
      if (e.selectedItem.textContent.startsWith(this.headerText[4].text)) {
        this.ruleToFilter = [];
        this.dataQualityDetails.rule_id = null;
        this.activeTab = this.headerText[4].text;
      }
      this.dataService.getData(environment.dataQuality.getCheckType,
        { name: 'get_dq_rule_checks', rule_type: this.activeTab })
        .subscribe(data => {
          this.ruleTypeOptions = data && data.Contents ? data.Contents : [];
        });
      this.setToolbar();
    }
  
    setToolbar() {
      if ((typeof (this.objdataValidationListComponent) !== 'undefined') && (this.activeTab === this.headerText[0].text)) {
        this.objdataValidationListComponent.setToolbar();
      }
      if ((typeof (this.objRIListComponent) !== 'undefined') && (this.activeTab === this.headerText[1].text)) {
        this.objRIListComponent.setToolbar();
      }
      if ((typeof (this.objThresholdValidationList) !== 'undefined') && (this.activeTab === this.headerText[2].text)) {
        this.objThresholdValidationList.setToolbar();
      }
      if ((typeof (this.objkpiValidationList) !== 'undefined') && (this.activeTab === this.headerText[4].text)) {
        this.objkpiValidationList.setToolbar();
      }
      if ((typeof (this.objDataControlList) !== 'undefined') && (this.activeTab === this.headerText[3].text)) {
        this.objDataControlList.setToolbar();
      }
    }
  
    populateDataQualityRules() {
      setTimeout(() => {
        if (typeof (this.objdataValidationListComponent) !== 'undefined') {
          this.objdataValidationListComponent.populateDataValidationRules();
        }
        if (typeof (this.objThresholdValidationList) !== 'undefined') {
          this.objThresholdValidationList.populateThresholdRules();
        }
        if (typeof (this.objRIListComponent) !== 'undefined') {
          this.objRIListComponent.populateRIRules();
        }
        if (typeof (this.objkpiValidationList) !== 'undefined') {
          this.objkpiValidationList.populateKPIRules();
        }
        if (typeof (this.objDataControlList) !== 'undefined') {
          this.objDataControlList.populateDataControlRules();
        }
      });
    }
  
    getRulesCount() {
      this.dataService.getData(environment.dataQuality.getDQRulesCount,
        { name: 'dq_rules_cat_count' })
        .subscribe(data => {
          this.rulesCountList = data && data.Contents ? data.Contents : [];
          if (this.rulesCountList.length > 0) {
            this.resultsCountDV = this.rulesCountList[0].count;
            this.resultsCountRI = this.rulesCountList[1].count;
            this.resultsCountTH = this.rulesCountList[2].count;
            this.resultsCountKPI = this.rulesCountList[3].count;
            this.resultsCountDC = this.rulesCountList[4].count;
          }
        });
    }
  
    updateDVRulesCount(cnt: number)
    {
      this.resultsCountDV=cnt;
    }
    updateRIRulesCount(cnt: number)
    {
      this.resultsCountRI=cnt;
    }
    updateTHRulesCount(cnt: number)
    {
      this.resultsCountTH=cnt;
    }
    updateKPIRulesCount(cnt: number)
    {
      this.resultsCountKPI=cnt;
    }
    updateDCRulesCount(cnt: number)
    {
      this.resultsCountDC=cnt;
    }
  }
  
