import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { DataValidationRuleDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataValidationRuleDetails';
import { DialogComponent, ButtonPropsModel, PositionDataModel } from '@syncfusion/ej2-angular-popups';
import { RestService } from 'src/app/shared/services/rest.service';
import { DataQualityDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataQualityDetails';
import { CommandModel, GridComponent, IRow, Column, ResizeService, ReorderService, ToolbarService, ColumnChooserService, ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { closest, EmitType } from '@syncfusion/ej2-base';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns';
import { DataValidationFormComponent } from '../data-validation-form/data-validation-form.component';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';

@Component({
  selector: 'app-data-validation-list',
  templateUrl: './data-validation-list.component.html',
  styleUrls: ['./data-validation-list.component.css'],
  providers: [CheckBoxSelectionService, ResizeService, ReorderService, ToolbarService, ColumnChooserService]
})
export class DataValidationListComponent implements OnInit {

  gridPageSettings: Object;
  gridToolbar: ToolbarItems[] | Object;
  gridCommands: CommandModel[];
  dlgPosition: PositionDataModel = { X: 'right', Y: 'top' };
  dlgAnimationSettings: Object = { effect: 'SlideRight', duration: 600, delay: 0 };
  dlgButtons: ButtonPropsModel[];

  @ViewChild('dataValidationListGrid', { static: false }) dataValidationListGrid: GridComponent;
  @ViewChild('addNewDataValidationRuleDialog', { static: false }) addNewDataValidationRuleDialog: DialogComponent;
  @ViewChild('dataValidationFormComponent', { static: false }) dataValidationFormComponent: DataValidationFormComponent;
  @Input() dataQualityDetails: DataQualityDetails;
  @Input() active: boolean;
  @Input() gridResultsCountDV: number;
  @Output() totalDVRulesCount = new EventEmitter<number>();
  // @Output() updateRulesForSearch = new EventEmitter<any[]>();

  //search filter
  @Input('searchtxt') searchtxt: string; 
  @Input('layerToFilter') layerToFilter: number[]; 
  @Input('objToFilter') objToFilter: number[];
  @Input('attrToFilter') attrToFilter: number[];
  @Input('ruleToFilter') ruleToFilter: number[];
  @Input('severityToFilter') severityToFilter: string[]; 
  @Input('statusToFilter') statusToFilter: string[];


  isRuleEditMode = false;
  latestRuleId = 0;
  dataQualityContent: any;
  dataValidationFormContent: any;
  dataValidationRules: DataValidationRuleDetails[];
  filterMetadata = { count: 0 };
  // filteredDataValidationRules: DataValidationRuleDetails[];
  initialGridLoad = true;
  currentPage = 1;
  numberOfDVRules = 5;
  totalRulesCount : number;

  constructor(private dataService: RestService, contentsService: ContentService) {
    // Initialize all the HTML Text Contents from the Content Resource File
    this.dataQualityContent = contentsService.getContent(environment.modules.dataQuality);
    this.dataValidationFormContent = this.dataQualityContent.subMenu.dataValidation;

    // Setup the AddNewValidation Form Dialog Action Buttons
    this.dlgButtons = [
      {
        click: this.dlgCancelBtnClick.bind(this), buttonModel: {
          content: this.dataValidationFormContent.buttons.cancel,
          cssClass: 'btn btn-secondary actionButton'
        }
      },
      {
        click: this.dlgSaveBtnClick.bind(this), buttonModel: {
          content: this.dataValidationFormContent.buttons.save,
          cssClass: 'btn btn-primary actionButton', disabled: false
        }
      }
    ];
  }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: true, pageSize:5, currentPage: this.currentPage, template: '', pageCount:5};    
    this.gridCommands = [{
      type: 'Edit', buttonOption: {
        iconCss: 'e-icons e-edit', cssClass: 'e-flat',
        click: this.editDataValidationRule.bind(this)
      }
    }];

    this.setToolbar();
    // Populate all the Data Validation Rules
    this.populateDataValidationRules();
  }

  onDVListGridDataBound()
  {
    this.totalDVRulesCount.emit(this.filterMetadata.count);
    //alert(this.dataValidationListGrid.pageSettings.currentPage);
  }

  setToolbar(){
    this.gridToolbar = [{ type: 'label', template: '#showDVResultsCount', align: 'Right' },
    { type: 'Input', template: '#addDVRuleTemplate', align: 'Right', id: 'addRule', click: this.addRuleHandler.bind(this) },
    { type: 'Input', template: '#exportDVTemplate', align: 'Right' },
    { text: 'ColumnChooser', template: '#columnChooserTemplateDV', align: 'Right' }];
  }

  addRuleHandler(args: ClickEventArgs): void {
    if (args.item.id === "addRule") {
      this.addDataValidationRule();
    }
  }
  
  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.numberOfDVRules = arg.itemData.value;
  }

  changePageNumber(currentPage: any) {
    this.currentPage = currentPage;
    this.gridPageSettings = { currentPage: this.currentPage };
  }

  // filterDataQualityRules(filteredData){
  //   this.filteredDataValidationRules = filteredData;
  //   this.totalRulesCount = filteredData.length;
  //   this.totalDVRulesCount.emit(filteredData.length);
    
  // }
  populateDataValidationRules() {
    this.dataService.getData(environment.dataQuality.getDataValidationRules,
      {
        name: 'get_data_validation_rules', src_data_layer_id: 0, obj_id: 0,
        attr_id_lst: 0, rule_id: 0, latest_rule_id: this.latestRuleId
      })
      .subscribe(data => {
      this.dataValidationRules = data && data.Contents ? data.Contents : [];
      // emit updated rules to advanced-search component
      // this.updateRulesForSearch.emit(this.dataValidationRules);

      // this.filteredDataValidationRules.valueChanges
      // .subscribe(queryField => this.search(queryField));

      // this.filteredDataValidationRules = this.dataValidationRules;
        this.gridResultsCountDV = this.dataValidationRules.length;
        // this.totalDVRulesCount.emit(this.gridResultsCountDV);
      });
  }

  openDataValidationRuleForm(isRuleEditMode: boolean) {
    // Open the Data Validation Form Dialog with slide effect
    this.addNewDataValidationRuleDialog.show();

    // Call the addNewDataValidationRule on the Form
    if (!isRuleEditMode)
      this.dataValidationFormComponent.addNewDataValidationRule(this.dataQualityDetails);
  }

  editDataValidationRule(args: Event): void {
    this.isRuleEditMode = true;
    this.openDataValidationRuleForm(true);

    const rowObj: IRow<Column> = this.dataValidationListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').
      getAttribute('data-uid'));

    const dataValidationRule = rowObj.data as DataValidationRuleDetails;

    this.dataValidationFormComponent.editDataValidationRule(dataValidationRule);
  }

  addDataValidationRule() {
    this.isRuleEditMode = false;
    this.openDataValidationRuleForm(false);
  }

  dlgCancelBtnClick() {
    // Call the resetVariblestoDefaults on the Form
    this.dataValidationFormComponent.resetVariblestoDefaults();

    // Hide the Data Validation Form
    this.addNewDataValidationRuleDialog.hide();
  }

  public modalDlgClose: EmitType<Event> = () => {
    this.dlgCancelBtnClick();
  }

  dlgSaveBtnClick() {
    // Call the saveNewDataValidationRule on the Form
    this.dataValidationFormComponent.saveNewDataValidationRule();

    // Hide the Data Validation Form & Repopulate the Rules
    if (!this.dataValidationFormComponent.addNewDataValidationRuleForm.invalid) {
      this.addNewDataValidationRuleDialog.hide();
      //Fetch the latest rule id after rule save/update
      this.latestRuleId = this.dataValidationFormComponent.latest_rule_id;
    }
  }

  updateLatestRuleId(val: any) {
    this.latestRuleId = val;
    this.populateDataValidationRules();
  }
}
