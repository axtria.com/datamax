import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { ToolbarItems } from '@syncfusion/ej2-grids';
import { Router, ActivatedRoute } from '@angular/router';
import { ObjectRulesInformationComponent } from '../../main-data-quality/object-rules-information/object-rules-information/object-rules-information.component';


@Component({
  selector: 'app-data-quality-objects',
  templateUrl: './data-quality-objects.component.html',
  styleUrls: ['./data-quality-objects.component.css']
})
export class DataQualityObjectsComponent implements OnInit {
  @ViewChild('objectRulesInfoComponent', { static: false }) objObjectRulesInfoComponent: ObjectRulesInformationComponent;
  @Output() totalObjectCount = new EventEmitter<number>();
  dataQualityContent: any;
  dqObjects: any[];
  gridToolbar: ToolbarItems[] | Object;
  gridPageSettings: Object;
  currentPage: 1;
  filterMetadata = { count: 0 };
  dqObjContent: any;
  totalObjects: number;


  constructor(private dataService: RestService, cs: ContentService, private router: Router, private route: ActivatedRoute) { 
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.dqObjContent = this.dataQualityContent.dqObject;

    
  }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: true, pageSize:5, currentPage: this.currentPage, template: ''};    
    this.dataService.getData(environment.dataQuality.getDqObjects,
      { name: 'get_dq_objects_info' })
      .subscribe(data => { 
        this.dqObjects = data && data.Contents ? data.Contents : []; 
        this.totalObjects = this.dqObjects.length});
    this.setToolbar();
  }

  setToolbar(){
    this.gridToolbar = [{ type: 'label', template: '#showObjectCount', align: 'Right' },
    { text: 'ColumnChooser', template: '#columnChooserTemplateObj', align: 'Right' }];
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value }; 
  }

  changePageNumber(currentPage: any) {
    this.currentPage = currentPage;
    this.gridPageSettings = { currentPage: this.currentPage };
  }

  onObjectDataBound(){
    this.totalObjectCount.emit(this.filterMetadata.count);
  }

  viewObjectInfoHandler(objId): void {
    this.router.navigate(['objectRulesInfo',objId], { relativeTo: this.route.parent });
  }
}
