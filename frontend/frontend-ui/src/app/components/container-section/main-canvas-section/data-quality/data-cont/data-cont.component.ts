import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { ButtonPropsModel, DialogComponent, PositionDataModel} from '@syncfusion/ej2-angular-popups';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { SourceData } from './sourceData';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-data-cont',
  templateUrl: './data-cont.component.html',
  styleUrls: ['./data-cont.component.css']
})
export class DataContComponent implements OnInit {

  @ViewChild('Dialog', { static: false }) Dialog: DialogComponent;
  @ViewChild('addNewRuleForm', { static: false }) form: any;
  position: PositionDataModel = { X: 'right', Y: 'top' };
  isRuleEditMode = false;
  index = 0;
  status = true;
  waterMark = 'Select';
  targetMetricList: [];
  targetAggLevelList: any[]; 
  metricFields: object = { text: 'at_nm', value: 'at_id' };
  layerList: [];
  layerFields: object = { text: 'name', value: 'layer_id' };
  sourceObjectList: [];
  targetObjectList: [];
  objectFields: object = { value: 'obj_id', text: 'obj_nm' };
  sourceData: SourceData = {
    objectList: [], metricList: [], aggregationLevelList: [], aggregationLevelList2: [],
  };

  dlgButtons: ButtonPropsModel[] = [];
  dataControlContent: any;
  severityOptions: any;
  activeSeverityOption = 'Informational';
  dataControlOptions: string[];
  activeDataControlOption: string;

  constructor(private dataService: RestService, private fb: FormBuilder, cs: ContentService) {
    this.dataControlContent = cs.getContent(environment.modules.dataQuality).subMenu.dataControl;
    this.severityOptions = this.dataControlContent.severityOptions;
    this.dataControlOptions = this.dataControlContent.controlType.options;
    this.activeDataControlOption = this.dataControlOptions.length > 0 ? this.dataControlOptions[0] : '';

    this.dlgButtons = [
      {
        click: this.dlgCancelBtnClick.bind(this), buttonModel: {
          content: this.dataControlContent.buttons.cancel, isPrimary: true,
          cssClass: 'btn btn-outline-warning actionButton'
        }
      },
      {
        click: this.dlgSaveBtnClick.bind(this), buttonModel: {
          content: this.dataControlContent.buttons.save, isPrimary: true,
          cssClass: 'btn btn-outline-warning actionButton', disabled: false
        }
      }
    ];
   }

  dataControlForm: FormGroup;
  items: FormArray;
  deleteItems: FormArray;

  ngOnInit() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_obj_meta_data_layers' }).subscribe(data => { this.layerList = data.Contents; });

    this.dataControlForm = this.fb.group({
      controlType: 'Control Sum',
      items: this.fb.array([this.createItem()]),
      targetDetails: this.fb.group({
        layer: '',
        object: '',
        metric: '',
        aggLevel: '',
        condition: '',
        status: true,
        severity: 'Informational',
        rule_desc: ''
      })
    });
  }

  createItem(): FormGroup {
    return this.fb.group({
      layer: '',
      object: '',
      metric: '',
      aggLevel: '',
      condition: ''
    });
  }
  addItem(): void {
    this.index += 1;
    this.items = this.dataControlForm.get('items') as FormArray;
    this.items.push(this.createItem());
    this.sourceObjectList = [];
  }
  deleteItem(n: number){
    this.items = this.dataControlForm.get('items') as FormArray;
    this.items.removeAt(n);
  }
  getControls() {
    return (<FormArray>this.dataControlForm.get('items')).controls;
  }
  onSubmit() {
  }

  controlTypeChange(e: any) {
    this.activeDataControlOption = e.itemData.value;
  }



  addNewRule() {
    //to open the add new rule dialogue box with slide effect
    this.Dialog.animationSettings = { effect: 'SlideRight', duration: 400 };
    this.Dialog.show();
  }

  sourceLayerChange(n: number, e: any) {
      this.dataService.getData(environment.dataQuality.getObjects,
        { name: 'get_data_layer_objects', src_data_layer_id: e.itemData.obj_layer_nm }).subscribe(data => { this.sourceData.objectList[n] = data.Contents; });
  }
  targetLayerChange(e:any){
      this.dataService.getData(environment.dataQuality.getObjects,
        { name: 'get_data_layer_objects', src_data_layer_id: e.itemData.obj_layer_nm }).subscribe(data => { this.targetObjectList = data.Contents; });
  }

  sourceObjectChange(n:number, e:any) {
        this.dataService.getData(environment.dataQuality.getAttributes,
          { name: 'get_object_attribute_details', object_id: e.itemData.obj_id }).subscribe(data => { this.sourceData.metricList[n] = data.Contents; });
  }
  targetObjectChange(e:any){
        this.dataService.getData(environment.dataQuality.getAttributes,
          { name: 'get_object_attribute_details', object_id: e.itemData.obj_id }).subscribe(data => { this.targetMetricList = data.Contents; });
  }

  sourceMetricChange(e: any, n: number) {
    this.sourceData.aggregationLevelList[n] = [];
    if(this.dataControlOptions.length > 0){
      if (this.activeDataControlOption === this.dataControlOptions[0]) {
        let metrics = this.sourceData.metricList[n].filter((item: any) => {
          return e.value.some((f: any) => {
            return f === item.at_id;
          });
        });
        this.sourceData.aggregationLevelList[n] = this.sourceData.metricList[n].filter((x: any) => !metrics.includes(x));
    } else if (this.activeDataControlOption === this.dataControlOptions[1]) {
        this.sourceData.aggregationLevelList[n] = this.sourceData.metricList[n].filter((item: any) => {
          if (item.at_id !== (e.value)) {
            return item;
          }
        });
    }
    }
  }
  targetMetricChange(e: any){
    if (e.value) {
        this.targetAggLevelList = this.targetMetricList.filter((element: any) => {
          if (element.at_id !== (e.value)) {
            return element;
          }
        });
    }
  }

  severityChange(severity: string){
    this.activeSeverityOption = severity;
  }

  dlgCancelBtnClick() {
    this.Dialog.hide();
    this.activeDataControlOption = '';
  }

  dlgSaveBtnClick() {
    console.log(this.dataControlForm.value);
    this.Dialog.hide();
    this.activeDataControlOption = '';
  }

}
