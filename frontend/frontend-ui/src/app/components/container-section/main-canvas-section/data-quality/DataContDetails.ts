export class DataControlRuleDetails {
    rule_id: string;
    // tgt_lyr_id: number;
    data_layer_id: number;
    tgt_obj_id: number;
    tgt_attr_id: any;
    tgt_bk_attr_id: any;
    tgt_fltr_con: string;
    src_lyr_id: any;
    src_obj_id: any;
    src_attr_id: any;
    src_bk_attr_id: any;
    src_fltr_con: any;

    mstr_rule_id: string;
    grnl_lvl: any;
    is_rule_actv: boolean;
    rule_svrt: string;
    rule_desc: string;

    obj_count: boolean;
    src_attr_count: boolean;
    tgt_attr_count: boolean;
    src_count: string;
}

// audit_insrt_id
// job_desc