import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';

@Injectable({
  providedIn: 'root'
})
export class DataValidationService {

  dateTypes = ['DATE', 'TIMESTAMP', 'TIMESTAMP_NTZ', 'TIMESTAMP_TZ']
  tooltipContent: any;

  constructor(contentsService: ContentService) {
    this.tooltipContent = contentsService.getContent(environment.modules.dataQuality).subMenu.dataValidation.tooltipContent;
  }

  getDataPattern(activeCheckName?: string, selectedAttrDataType?: string) {
    let dataPattern: string;
    let customMsg: string;
    if (selectedAttrDataType.includes('CHAR')) {
      dataPattern = '[a-zA-Z0-9]*';
      customMsg = this.tooltipContent.var;
    } else if (selectedAttrDataType === 'NUMBER') {
      dataPattern = '[0-9]*';
      customMsg = this.tooltipContent.num;
    } else if (selectedAttrDataType === 'DECIMAL' || selectedAttrDataType === 'REAL' || selectedAttrDataType === 'DOUBLE PRECISION') {
      dataPattern = '[0-9.]*';
      customMsg = this.tooltipContent.decInt;
    } else if (this.dateTypes.indexOf(selectedAttrDataType.toUpperCase()) > -1) {
      dataPattern = '';
      customMsg = this.tooltipContent.dateTime;
    } else if (selectedAttrDataType === 'FLOAT') {
      dataPattern = '[0-9.]*';
      customMsg = this.tooltipContent.float;
    }

    if (activeCheckName != undefined) {
      if (activeCheckName.startsWith('LEN') || activeCheckName.startsWith('FIXED')) {
        dataPattern = '[0-9]{0,255}';
        customMsg = this.tooltipContent.int;
      }
      if (activeCheckName.startsWith('DOMAIN')) {
        if (selectedAttrDataType.includes('NUMBER')) {
          dataPattern = '[0-9,]*';
          customMsg = this.tooltipContent.pipeInt;
        }
        if (selectedAttrDataType === 'BOOLEAN') {
          dataPattern = '';
          customMsg = this.tooltipContent.pipeBool;
        }
        if (selectedAttrDataType === 'DECIMAL' || selectedAttrDataType === 'REAL' || selectedAttrDataType === 'DOUBLE PRECISION') {
          dataPattern = '[0-9.,]*';
          customMsg = this.tooltipContent.pipeDecInt;
        }
        if (selectedAttrDataType === 'FLOAT') {
          dataPattern = '[0-9.,]*';
          customMsg = this.tooltipContent.pipeFloat;
        }
        if (selectedAttrDataType.includes('CHAR')) {
          dataPattern = '[a-zA-Z0-9,]*';
          customMsg = this.tooltipContent.pipeVar;
        }
        if (this.dateTypes.indexOf(selectedAttrDataType.toUpperCase()) > -1) {
          dataPattern = '';
          customMsg = this.tooltipContent.pipedateTime;
        }
      }
      if (activeCheckName === "PATTERN CHECK") {
        if (selectedAttrDataType.includes('NUMBER')) {
          dataPattern = '[0-9,]*';
          customMsg = this.tooltipContent.patNum;
        }
        if (selectedAttrDataType === 'BOOLEAN') {
          dataPattern = '';
          customMsg = this.tooltipContent.patBool;
        }
        if (selectedAttrDataType === 'FLOAT') {
          dataPattern = '[0-9.,]*';
          customMsg = this.tooltipContent.patFloat;
        }
        if (selectedAttrDataType.includes('CHAR')) {
          dataPattern = '[a-zA-Z0-9,]*';
          customMsg = this.tooltipContent.patChar;
        }
        if (this.dateTypes.indexOf(selectedAttrDataType.toUpperCase()) > -1) {
          dataPattern = '';
          customMsg = this.tooltipContent.patDateTime;
        }
      }
      if (activeCheckName === 'DATA AVAILABILITY CHECK') {
        dataPattern = '[0-9]{0,255}';
        customMsg = this.tooltipContent.int;
      }
    }

    return [dataPattern, customMsg];
  }
}
