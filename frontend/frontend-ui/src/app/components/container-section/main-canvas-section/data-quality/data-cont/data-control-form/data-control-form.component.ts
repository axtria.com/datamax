import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { DataControlRuleDetails } from '../../DataContDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { SourceData } from '../sourceData';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { DataControlDetails } from '../../DataControlDetails';

@Component({
  selector: 'app-data-control-form',
  templateUrl: './data-control-form.component.html',
  styleUrls: ['./data-control-form.component.css']
})
export class DataControlFormComponent implements OnInit {
  @Output() hideDialog = new EventEmitter<boolean>();
  utilService = new UtilService;
  sourceDataForEdit = [];
  tgtDataForEdit: any;
  dataControlContent: any;
  commonContent: any;
  applyDropdownList: any[];
  activeApplyDropdownList: string;

  isRuleEditMode = false;
  formInvalidAfterSaveClick = false;
  status = true;
  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };
  objectList: any[];
  srcObjectList: any[];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };
  attributeList: any[];
  grpByAttrList: any[];
  grpByAttrList2: any[];
  srcAttributeList: any[];
  // trgtGrpByLength: number;
  trgtAttrLength: number;
  attributeFields: object = { text: 'etl_nm', value: 't_at_id' };
  aggFuncList: any[];
  aggFuncFields: object = { text: 'visual_rule_desc', value: 'mstr_rule_id' };
  severityOptions: [];
  objectCountOptions = [];
  activeobjectCountType: string;
  sourceData: SourceData = {
    objectList: [], metricList: [], aggregationLevelList: [], aggregationLevelList2: [],
  };

  dataControlRuleDetails: DataControlRuleDetails = {
    rule_id: null,
    data_layer_id: null,
    tgt_obj_id: null,
    tgt_attr_id: null,
    tgt_bk_attr_id: null,
    tgt_fltr_con: null,
    src_lyr_id: null,
    src_obj_id: null,
    src_attr_id: null,
    src_bk_attr_id: null,
    src_fltr_con: null,

    mstr_rule_id: null,
    grnl_lvl: null,
    is_rule_actv: null,
    rule_svrt: null,
    rule_desc: null,

    obj_count: null,
    src_attr_count: null,
    tgt_attr_count: null,
    src_count: 'Single'
  };

  constructor(private dataService: RestService, private authenticateService: AuthenticateService, private toastPopupService: ToastPopupService, contentsService: ContentService, private fb: FormBuilder) {
    this.dataControlContent = contentsService.getContent(environment.modules.dataQuality).subMenu.dataControl;
    this.commonContent = contentsService.getContent(environment.modules.dataQuality).commonContent;
    this.applyDropdownList = this.commonContent.applyDropdownList;
    this.activeApplyDropdownList = this.applyDropdownList[0];
    this.objectCountOptions = this.dataControlContent.objectCountOptions;
    this.activeobjectCountType = this.objectCountOptions[0];
    this.severityOptions = this.dataControlContent.severityOptions;
  }

  dataControlForm: FormGroup;
  items: FormArray;
  tgDetails: FormGroup;
  deleteItems: FormArray;
  index = 0;

  ngOnInit() {
    this.populateDataLayer();
    this.populateCheckType();
    this.resetEntireForm();
    // this.dataControlForm = this.fb.group({
    //   targetDetails: this.fb.group({
    //     layer: [null, [Validators.required]],
    //     object: [null, [Validators.required]],
    //     controlType: ['MRDQ1001', [Validators.required]],
    //     metricAttr: [null, this.metricAttrValidator],
    //     grpByAttr1: [''],
    //     grpByAttr2: [''],
    //     fltrConn: [''],
    //     status: [true, [Validators.required]],
    //     severity: ['Informational', [Validators.required]],
    //     rule_desc: [''],
    //     objCount: ['Single', [Validators.required]]
    //   }),
    //   items: this.fb.array([this.createItem()]),
    // });
    // this.tgDetails = <FormGroup>this.dataControlForm.controls.targetDetails;
    this.radioChanges();
  }

  resetEntireForm() {
    this.dataControlForm = this.fb.group({
      targetDetails: this.fb.group({
        layer: [null, [Validators.required]],
        object: [null, [Validators.required]],
        controlType: [null, [Validators.required]],
        metricAttr: [null, this.metricAttrValidator],
        grpByAttr1: [''],
        grpByAttr2: [''],
        fltrConn: [''],
        status: [true, [Validators.required]],
        severity: ['Informational', [Validators.required]],
        rule_desc: [''],
        objCount: ['Single', [Validators.required]]
      }),
      items: this.fb.array([this.createItem()]),
    });
    this.tgDetails = <FormGroup>this.dataControlForm.controls.targetDetails;
    this.dataControlForm.get('targetDetails').get('layer').setValue(null, { onlySelf: true });
    this.dataControlForm.get('targetDetails').get('object').setValue(null, { onlySelf: true });
    this.formInvalidAfterSaveClick = false;
    this.isRuleEditMode = false;
    this.dataControlRuleDetails.data_layer_id = null;
    this.dataControlRuleDetails.tgt_obj_id = null;
    this.dataControlRuleDetails.tgt_attr_id = null;
    this.dataControlRuleDetails.tgt_bk_attr_id = null;
    this.dataControlRuleDetails.mstr_rule_id = 'MRDQ1001';
    this.trgtAttrLength = 1;

    // this.addNewDataControlRuleForm.reset();
    // debugger;
    // console.log(this.trgtLayer);
    // this.trgtLayer.itemData=null;
  }

  get lyr() {
    return this.tgDetails.controls.layer;
  }
  get obj() {
    return this.tgDetails.controls.object;
  }
  get ctrlTyp() {
    return this.tgDetails.controls.controlType;
  }
  get metric() {
    return this.tgDetails.controls.metricAttr;
  }
  get isActv() {
    return this.tgDetails.controls.status;
  }
  get svrt() {
    return this.tgDetails.controls.severity;
  }
  get objCnt() {
    return this.tgDetails.controls.objCount;
  }

  createItem(): FormGroup {
    return this.fb.group({
      layer: [null, [Validators.required]],
      object: [null, [Validators.required]],
      metricAttr: [null, this.metricAttrValidator],
      grpByAttr1: '',
      grpByAttr2: '',
      fltrConn: ''
    });
  }

  radioChanges() {
    this.dataControlForm.get('targetDetails').valueChanges.subscribe(val => {
      this.dataControlRuleDetails.src_count = val.obj_count;
    });
  }

  populateCheckType() {
    this.dataService.getData(environment.dataQuality.getCheckType,
      { name: 'get_dq_rule_checks', rule_type: 'Data Control' })
      .subscribe(data => { this.aggFuncList = data && data.Contents ? data.Contents : []; });
  }
  populateDataLayer() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_obj_meta_data_layers' })
      .subscribe(data => { this.businessLayerList = data && data.Contents ? data.Contents : []; });
  }
  dataLayerChange(e: any) {
    if (e.itemData !== null) {
      this.dataControlRuleDetails.data_layer_id = e.itemData.layer_id;
      this.dataControlRuleDetails.tgt_obj_id = null;
      this.dataControlRuleDetails.tgt_attr_id = null;
      this.dataControlRuleDetails.tgt_bk_attr_id = null;
      this.populateObject(e.itemData.layer_id);
    }
  }
  srcLayerChange(e: any, n: number) {
    if (e.itemData !== null) {
      this.dataControlRuleDetails.src_lyr_id = this.dataControlRuleDetails.src_lyr_id !== null ? this.dataControlRuleDetails.src_lyr_id.toString().concat('|') : '';
      this.dataControlRuleDetails.src_lyr_id = this.dataControlRuleDetails.src_lyr_id.toString().concat(e.itemData.layer_id);
      this.dataControlRuleDetails.src_obj_id = null;
      this.dataControlRuleDetails.src_attr_id = null;
      this.dataControlRuleDetails.src_bk_attr_id = null;
      this.populateSrcObject(e.itemData.layer_id, n);
    }
  }
  populateObject(dataLayerName: string) {
    this.objectList = [];
    this.dataControlForm.get('targetDetails').get('object').setValue(null);
    this.dataControlForm.get('targetDetails').get('metricAttr').setValue(null);
    this.dataService.getData(environment.dataQuality.getObjects,
      { name: 'get_data_layer_objects', src_data_layer_id: dataLayerName })
      .subscribe(data => {
        this.objectList = data && data.Contents ? data.Contents : [];
        this.grpByAttrList = data && data.Contents ? data.Contents : [];
      });
  }
  populateSrcObject(dataLayerName: string, n: number) {
    this.sourceData.objectList[n] = [];
    this.getSrcControls[n].get('object').setValue(null);
    this.getSrcControls[n].get('metricAttr').setValue(null);
    this.sourceData.aggregationLevelList[n] = [];
    this.sourceData.aggregationLevelList2[n] = [];
    this.dataService.getData(environment.dataQuality.getObjects,
      { name: 'get_data_layer_objects', src_data_layer_id: dataLayerName })
      .subscribe(data => {
        this.sourceData.objectList[n] = data && data.Contents ? data.Contents : [];
        this.sourceData.aggregationLevelList[n] = data && data.Contents ? data.Contents : [];
      });
  }
  objectChange(e: any) {
    this.attributeList = [];
    if (e.itemData !== null) {
      this.dataControlRuleDetails.tgt_attr_id = null;
      this.dataControlRuleDetails.tgt_bk_attr_id = null;
      this.dataControlRuleDetails.tgt_obj_id = e.itemData.obj_id;
      this.populateAttribute(this.dataControlRuleDetails.tgt_obj_id);
    }
  }
  srcObjectChange(e: any, n: number) {
    this.srcAttributeList = [];
    if (e.itemData !== null) {
      this.dataControlRuleDetails.src_attr_id = null;
      this.dataControlRuleDetails.src_bk_attr_id = null;
      this.dataControlRuleDetails.src_obj_id = this.dataControlRuleDetails.src_obj_id !== null ? this.dataControlRuleDetails.src_obj_id.toString().concat('|') : '';
      this.dataControlRuleDetails.src_obj_id = this.dataControlRuleDetails.src_obj_id.toString().concat(e.itemData.obj_id);
      this.populateSrcAttribute(e.itemData.obj_id, n);
    }
  }
  populateAttribute(objectId: number) {
    this.attributeList = [];
    this.dataControlForm.get('targetDetails').get('metricAttr').setValue(null);
    this.grpByAttrList = [];
    this.grpByAttrList2 = [];
    if (objectId !== null) {
      this.dataService.getData(environment.dataQuality.getAttributes,
        { name: 'get_object_attribute_details', obj_id: objectId })
        .subscribe(data => {
          this.attributeList = data && data.Contents ? data.Contents : [];
          this.grpByAttrList = this.attributeList;
          // If the form is in EDIT mode then pre-set the Attribute Dropdown - It will fire attributeChange() method
          // if (this.isRuleEditMode) {
          //   this.dataValidationLayoutDetails.attr_id_lst = this.editDataValidationLayoutDetails.attr_id_lst;
          // }
        });
    }
  }
  populateSrcAttribute(objectId: number, n: number) {
    this.sourceData.metricList[n] = [];
    // this.dataControlForm.get('targetDetails').get('metricAttr').setValue(null);
    this.getSrcControls[n].get('metricAttr').setValue(null);
    this.sourceData.aggregationLevelList[n] = [];
    this.sourceData.aggregationLevelList2[n] = [];
    if (objectId !== null) {
      this.dataService.getData(environment.dataQuality.getAttributes,
        { name: 'get_object_attribute_details', obj_id: objectId })
        .subscribe(data => {
          this.sourceData.metricList[n] = data && data.Contents ? data.Contents : [];
          if (this.dataControlRuleDetails.mstr_rule_id === 'MRDQ1002') {
            this.sourceData.aggregationLevelList[n] = this.sourceData.metricList[n];
          }
        });
    }
  }
  aggFnChange(e: any) {
    if (e.itemData !== null) {
      this.dataControlForm.get('targetDetails').get('controlType').setValue(e.itemData.mstr_rule_id, { onlySelf: true });
      this.dataControlRuleDetails.mstr_rule_id = e.itemData.mstr_rule_id;

      if (e.itemData.mstr_rule_id === 'MRDQ1001') {
        this.dataControlRuleDetails.src_count = this.objectCountOptions[0];
        this.grpByAttrList = [];
        this.grpByAttrList2 = [];
        this.dataControlForm.value.targetDetails.grpByAttr1 = '';
        this.dataControlForm.value.targetDetails.grpByAttr2 = '';
      } else {
        this.grpByAttrList = this.attributeList;
        this.dataControlForm.get('targetDetails').get('grpByAttr1').setValue(null, { onlySelf: true });
        this.dataControlForm.get('targetDetails').get('grpByAttr2').setValue(null, { onlySelf: true });
      }
    }
    this.dataControlRuleDetails.tgt_attr_id = null;
    this.dataControlForm.get('targetDetails').get('objCount').enable({ emitEvent: true });
    this.dataControlForm.get('targetDetails').get('metricAttr').setValue(null, { onlySelf: true });


    this.metric.updateValueAndValidity();
    for (let item of this.getSrcControls) {
      item.get('metricAttr').updateValueAndValidity()
    }
    // console.log(e.itemData.mstr_rule_id);
    // console.log(this.dataControlForm.value.targetDetails.controlType)
  }
  metricAttrValidator(control: AbstractControl) {
    if (control) {
      const group = <FormGroup>control.root.get('targetDetails');
      if (group) {
        const aggFn = group.controls.controlType;
        if (aggFn) {
          if (aggFn.value === 'MRDQ1001') {
            if (control.value === null || control.value === undefined || control.value === '') {
              return { 'attr_error': 'Metric Attribute is required.' }
            }
          }
        }
      }
    }
    return null;
  }
  metricAttrChange(e: any) {
    this.dataControlRuleDetails.tgt_attr_id = e.value;
    if (e.value !== null) {
      this.trgtAttrLength = e.value.length;
      if (this.dataControlRuleDetails.mstr_rule_id === 'MRDQ1001') {
        if (e.value.length > 1) {
          this.dataControlForm.get('targetDetails').get('objCount').setValue('Single');
          this.dataControlForm.get('targetDetails').get('objCount').disable({ emitEvent: true });
          this.deleteExtraSrcControls();
        } else {
          this.dataControlForm.get('targetDetails').get('objCount').enable({ emitEvent: true });
        }
        // this.dataControlForm.get('targetDetails').get('objCount').disable({ emitEvent: false });
        if (this.attributeList !== undefined && this.attributeList !== null) {
          if (!this.isRuleEditMode) {
            this.grpByAttrList = this.utilService.removeArrayPropArrayItem(this.attributeList, 't_at_id', e.value);
          }
        }
      } else {
        this.dataControlForm.get('targetDetails').get('objCount').enable({ emitEvent: true });
        this.grpByAttrList = this.attributeList;
      }
    }
  }
  srcMetricAttrChange(e: any, n: number) {
    if (this.dataControlRuleDetails.mstr_rule_id === 'MRDQ1001') {
      let metrics = this.sourceData.metricList[n].filter((item: any) => {
        return e.value.some((f: any) => {
          return f === item.t_at_id;
        });
      });
      this.sourceData.aggregationLevelList[n] = this.sourceData.metricList[n].filter((x: any) => !metrics.includes(x));
    } else {
      this.sourceData.aggregationLevelList[n] = this.sourceData.metricList[n];
    }
    // this.sourceData.aggregationLevelList[n] = this.sourceData.metricList[n].filter(function (item: any) {
    //   if (item.t_at_id !== e.value) {
    //     return item;
    //   }
    // });
    if (this.dataControlRuleDetails.tgt_attr_id !== null && this.dataControlRuleDetails.tgt_attr_id.length > 1) {
      if (e.value.length !== this.dataControlRuleDetails.tgt_attr_id.length) {
        this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage("Select same number of source metric attribute as the target one"));
      }
    }
  }

  singleSrcMetricAttrChange(e: any, n: number) {
    if (this.dataControlRuleDetails.mstr_rule_id === 'MRDQ1001') {
      let metrics = this.sourceData.metricList[n].filter((item: any) => {
        return e.value === item.t_at_id;
      });
      this.sourceData.aggregationLevelList[n] = this.sourceData.metricList[n].filter((x: any) => !metrics.includes(x));
    } else {
      this.sourceData.aggregationLevelList[n] = this.sourceData.metricList[n];
    }
    if (this.dataControlRuleDetails.tgt_attr_id !== null && this.dataControlRuleDetails.tgt_attr_id.length > 1) {
      if (e.value.length !== this.dataControlRuleDetails.tgt_attr_id.length) {
        this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage("Select same number of source metric attribute as the target one"));
      }
    }
  }

  grpByAttrChange(e: any) {
    this.grpByAttrList2 = [];
    this.dataControlForm.get('targetDetails').get('grpByAttr2').setValue(null, { onlySelf: true });
    if (this.grpByAttrList !== undefined && this.grpByAttrList !== null) {
      if (!this.isRuleEditMode) {
        this.grpByAttrList2 = this.grpByAttrList.filter(value => value['t_at_id'] !== e.value);
      }
    } else {
      this.grpByAttrList2 = this.grpByAttrList;
    }
    // this.trgtGrpByLength = e.value.length;
    // if (e.value !== null && e.value.length > 2) {
    //   this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage("Maximum of two Group By Attribute is allowed"));
    // }
  }

  srcGrpByAttrChange(e:any, n: number){
    this.sourceData.aggregationLevelList2[n] = [];
    this.getSrcControls[n].get('grpByAttr2').setValue(null, { onlySelf: true }); 
    // etValue({grpByAttr2 : null});
    // this.getSrcControls[n].get('grpByAttr2').setValue(null, { onlySelf: true });
    // var items = this.getSrcControls;
    // items.con [n].get('grpByAttr2').setValue(null, { onlySelf: true });
    // this.dataControlForm.get('items').get('grpByAttr2').setValue(null, { onlySelf: true });
    if (this.sourceData.aggregationLevelList[n] !== undefined && this.sourceData.aggregationLevelList[n] !== null) {
      if (!this.isRuleEditMode) {
        this.sourceData.aggregationLevelList2[n] = this.sourceData.aggregationLevelList[n].filter(value => value['t_at_id'] !== e.value);
      }
    } else {
      this.sourceData.aggregationLevelList2[n] = this.sourceData.aggregationLevelList[n];
    }
  }

  objectCountChange(objCount: string) {
    this.dataControlRuleDetails.src_count = objCount;
    this.dataControlForm.get('targetDetails').get('objCount').setValue(objCount, { onlySelf: true });
    if (objCount === this.objectCountOptions[0]) {
      this.deleteExtraSrcControls();
    }
  }

  deleteExtraSrcControls() {
    while (this.getSrcControls.length > 1) {
      this.deleteSource(1);
    }
  }
  // srcGrpByAttrChange(e:any, n: number){
  //   if(e.value !== null && e.value.length > 2){
  //     this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage("Maximum of two Group By Attribute is allowed"));
  //   }
  // }
  addSource(): void {
    this.index += 1;
    this.items = this.dataControlForm.get('items') as FormArray;
    this.items.push(this.createItem());
    //update value and validity of metricAttr controls of each items for custom validation
    for (let item of this.getSrcControls) {
      item.get('metricAttr').updateValueAndValidity()
    }
  }
  deleteSource(n: number) {
    this.index -= 1;
    this.items = this.dataControlForm.get('items') as FormArray;
    this.items.removeAt(n);
  }
  // pushItem(lyr, obj, mtr, grp, flt) {
  //   return this.fb.group({
  //     layer: lyr,
  //     object: obj,
  //     metricAttr: mtr,
  //     grpByAttr: grp,
  //     fltrConn: flt
  //   });
  // }

  addNewDataControlRule() {
    this.isRuleEditMode = false;
    this.dataControlForm.get('targetDetails').get('layer').setValue('', { onlySelf: true });
    this.dataControlForm.get('targetDetails').get('object').setValue('', { onlySelf: true });
  }
  get getSrcControls() {
    return (<FormArray>this.dataControlForm.get('items')).controls;
  }
  // resetDataControlForm() {
  //   this.items = this.dataControlForm.get('items') as FormArray;
  //   // while (this.items.length !== 0) {
  //   //   this.items.removeAt(0);
  //   // }
  //   this.items = this.fb.array([]);
  //   this.items.push(this.createItem());
  // }
  deleteAllSrcItems() {
    // this.items = this.dataControlForm.get('items') as FormArray;
    while (this.getSrcControls.length > 1) {
      this.deleteSource(0);
    }
    // for( let item of this.getSrcControls()){
    //   this.deleteSource(0);
    // }
  }
  saveDataControlRule() {
    if (this.dataControlForm.invalid) {
      this.toastPopupService._exception.
        emit(this.toastPopupService.getToastWarningMessage(this.dataControlContent.popupMsg.invalid));
      this.formInvalidAfterSaveClick = this.dataControlForm.invalid;
    } else {
      if (!this.isRuleEditMode) {    // for (let item of this.dataControlForm.value.items.grpByAttr) {
        //   if(item.length !== this.dataControlForm.value.targetDetails.grpByAttr.length){
        //     this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage("For each source item select same number of source grp by attribute as the target one"));
        //   }
        // }
        // if (this.dataControlForm.value.items['grpByAttr'].length === this.dataControlForm.value.targetDetails.grpByAttr.length) {
        //   this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage("For each source item select same number of source grp by attribute as the target one"));
        // }

        let p_tgt_attr_count = false;
        let p_src_attr_count = false;
        let p_obj_count = false;
        // console.log(this.dataControlForm.value);
        // console.log(this.dataControlRuleDetails);
        if (this.dataControlRuleDetails.mstr_rule_id === 'MRDQ1002' || this.dataControlRuleDetails.src_count === this.objectCountOptions[1]) {
          p_obj_count = true;
        } else if (this.dataControlForm.value.targetDetails.metricAttr.length === 1 && this.dataControlForm.value.items[0].metricAttr.length > 1) {
          p_src_attr_count = true;
        } else {
          p_tgt_attr_count = true;
        }
        let p_src_fltr_con = '';
        let p_tgt_bk_attr_id = '';
        let p_src_bk_attr_id = '';
        let p_src_attr_id = '';
        let p_src_obj_id = '';
        let p_tgt_attr_id = this.dataControlForm.value.targetDetails.metricAttr;
        if (this.dataControlForm.value.targetDetails.metricAttr !== null) {
          p_tgt_attr_id = this.dataControlForm.value.targetDetails.metricAttr.length > 1 ? this.dataControlForm.value.targetDetails.metricAttr.toString().replace(/\,/g, '|') : this.dataControlForm.value.targetDetails.metricAttr;
        }


        if (this.dataControlForm.value.targetDetails.grpByAttr1 !== '' && this.dataControlForm.value.targetDetails.grpByAttr2 !== '') {
          p_tgt_bk_attr_id = p_tgt_bk_attr_id.concat(this.dataControlForm.value.targetDetails.grpByAttr1).concat('|').concat(this.dataControlForm.value.targetDetails.grpByAttr2);
        } else if (this.dataControlForm.value.targetDetails.grpByAttr1) {
          p_tgt_bk_attr_id = this.dataControlForm.value.targetDetails.grpByAttr1;
        } else if (this.dataControlForm.value.targetDetails.grpByAttr2) {
          p_tgt_bk_attr_id = this.dataControlForm.value.targetDetails.grpByAttr2;
        } else {
          p_tgt_bk_attr_id = null;
        }

        for (let item of this.dataControlForm.value.items) {
          let fltr = item.fltrConn.toString() === '' ? '1=1' : item.fltrConn.toString();
          p_src_fltr_con = p_src_fltr_con !== '' ? p_src_fltr_con.concat('|').concat(fltr) : p_src_fltr_con.concat(fltr);
          if (item.grpByAttr1 && item.grpByAttr2) {
            p_src_bk_attr_id = p_src_bk_attr_id !== '' ? p_src_bk_attr_id.concat('^').concat(item.grpByAttr1.toString()).concat('|').concat(item.grpByAttr2.toString()) : p_src_bk_attr_id.concat(item.grpByAttr1.toString().concat('|').concat(item.grpByAttr2.toString()));
          } else if (item.grpByAttr1) {
            p_src_bk_attr_id = p_src_bk_attr_id !== '' ? p_src_bk_attr_id.concat('^').concat(item.grpByAttr1.toString()) : p_src_bk_attr_id.concat(item.grpByAttr1.toString());
          } else if (item.grpByAttr2) {
            p_src_bk_attr_id = p_src_bk_attr_id !== '' ? p_src_bk_attr_id.concat('^').concat(item.grpByAttr2.toString()) : p_src_bk_attr_id.concat(item.grpByAttr2.toString());
          } else {
            p_src_bk_attr_id = '';
          }

          p_src_obj_id = p_src_obj_id !== '' ? p_src_obj_id.concat('|').concat(item.object.toString()) : p_src_obj_id.concat(item.object.toString());
          if (item.metricAttr !== null) {
            p_src_attr_id = p_src_attr_id !== '' && item.metricAttr !== null ? p_src_attr_id.concat('|').concat(item.metricAttr.toString().replace(/\,/g, '|')) : p_src_attr_id.concat(item.metricAttr.toString().replace(/\,/g, '|'));
          }
          // if (this.dataControlForm.value.targetDetails.metricAttr.length > 1) {
          //   p_src_attr_id = p_src_attr_id !== '' ? p_src_attr_id.concat('|').concat(item.metricAttr.toString().replace(/\,/g, '|')) : p_src_attr_id.concat(item.metricAttr.toString().replace(/\,/g, '|'));
          // } else {
          //   p_src_attr_id = p_src_attr_id !== '' ? p_src_attr_id.concat('|').concat(item.metricAttr.toString()) : p_src_attr_id.concat(item.metricAttr.toString());
          // }
        }

        this.dataService.getData(environment.dataQuality.addDataControlRule,
          {
            name: 'add_data_control_rule',
            obj_count: p_obj_count,
            src_attr_count: p_src_attr_count,
            job_desc: '',
            tgt_attr_id: p_tgt_attr_id === '' ? null : p_tgt_attr_id, //null in case of Total Check
            grnl_lvl: this.dataControlRuleDetails.grnl_lvl,
            src_fltr_con: p_src_fltr_con === '' ? "1=1" : p_src_fltr_con,
            rule_desc: this.dataControlForm.value.targetDetails.rule_desc,
            tgt_obj_id: this.dataControlForm.value.targetDetails.object,
            src_bk_attr_id: p_src_bk_attr_id === '' ? null : p_src_bk_attr_id,
            src_attr_id: p_src_attr_id === '' ? null : p_src_attr_id,  //null in case of Total Check
            // tgt_bk_attr_id: this.dataControlForm.value.targetDetails.grpByAttr1 && this.dataControlForm.value.targetDetails.grpByAttr2 ? this.dataControlForm.value.targetDetails.grpByAttr1.toString().concat('|').concat(this.dataControlForm.value.targetDetails.grpByAttr2) : this.dataControlForm.value.targetDetails.grpByAttr1 | this.dataControlForm.value.targetDetails.grpByAttr2,
            tgt_bk_attr_id: p_tgt_bk_attr_id,
            tgt_fltr_con: this.dataControlForm.value.targetDetails.fltrConn === '' ? "1=1" : this.dataControlForm.value.targetDetails.fltrConn,
            data_layer_id: this.dataControlForm.value.targetDetails.layer,
            src_obj_id: p_src_obj_id,
            tgt_attr_count: p_tgt_attr_count,
            is_rule_actv: this.dataControlForm.value.targetDetails.status,
            audit_insrt_id: this.authenticateService.getUserInfo().user_id,
            rule_svrt: this.dataControlForm.value.targetDetails.severity.slice(0, 1),
            mstr_rule_id: this.dataControlForm.value.targetDetails.controlType

          })
          .subscribe(data => {
            if (data.Contents[0].status.toUpperCase() === 'PASS') {
              this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(data.Contents[0].message));
              this.resetEntireForm();
              this.hideDialog.emit(true);
            } else {
              this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage("Rule Not Saved!"));
            }
          });
      }

      if (this.isRuleEditMode) {
        // console.log(this.dataControlForm.value);
        this.dataService.getData(environment.dataQuality.updateDataControlRule,
          {
            name: 'update_data_control_rule',
            // src_attr_id: this.sourceDataForEdit.forEach(element => element['src_attr_id'].join('|'))  .split('.').join('x'),
            src_obj_id: this.sourceDataForEdit[0].src_obj_id !== null ? Array.prototype.map.call(this.sourceDataForEdit, s => s.src_obj_id).toString().split(',').join('|') : null,
            src_attr_id: this.sourceDataForEdit[0].src_attr_id !== null ? Array.prototype.map.call(this.sourceDataForEdit, s => s.src_attr_id).toString().split(',').join('|') : null,
            obj_count: this.sourceDataForEdit.length > 1,
            audit_updt_id: this.authenticateService.getUserInfo().user_id,
            rule_id: this.tgtDataForEdit.rule_id,
            rule_desc: this.dataControlForm.value.targetDetails.rule_desc,
            is_rule_actv: this.dataControlForm.value.targetDetails.status,
            rule_svrt: this.dataControlForm.value.targetDetails.severity.slice(0, 1),
            tgt_fltr_con: this.dataControlForm.value.targetDetails.fltrConn === '' ? "1=1" : this.dataControlForm.value.targetDetails.fltrConn,
            src_fltr_con: this.dataControlForm.value.items[0].fltrConn !== null ? Array.prototype.map.call(this.dataControlForm.value.items, s => s.fltrConn).toString().split(',').join('|') : null,
            // src_fltr_con: Array.prototype.map.call(this.dataControlForm.value.items, s => s.fltrConn).toString().substring(1).split(',').join('|'),
          })
          .subscribe(data => {
            if (data.Contents[0].status.toUpperCase() === 'PASS') {
              this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(data.Contents[0].message));
              this.resetEntireForm();
              this.hideDialog.emit(true);
            } else {
              this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage("Rule Not Updated!"));
            }
          });
      }
    }
  }
  editDataControlRule(record: DataControlDetails): void {
    // debugger;
    // console.log(record);
    this.tgtDataForEdit = record;
    this.dataControlForm.get('targetDetails').patchValue({
      layer: record.tgt_layer_id,
      object: record.tgt_obj_id,
      controlType: record.mstr_rule_id,
      metricAttr: record.tgt_attr_id !== null ? record.tgt_attr_id.split('|') : '',
      grpByAttr: record.tgt_bk_attr_id !== null ? record.tgt_bk_attr_id.split('|') : '',
      fltrConn: record.tgt_fltr_con,
      status: record.is_rule_actv,
      severity: record.rule_svrt,
      rule_desc: record.rule_desc,
      objCount: 'Single'
    });
    this.dataService.getData(environment.dataQuality.getSrcDataControlDetails,
      {
        name: 'get_src_data_control_details',
        rule_id: record.rule_id
      })
      .subscribe(data => {
        // console.log(data.Contents);
        this.sourceDataForEdit = data && data.Contents ? data.Contents : [];
        // console.log(this.dataControlForm.value);
        // this.deleteAllSrcItems();
        // console.log(this.dataControlForm.value);
        this.deleteSource(0);
        // console.log(this.dataControlForm.value);
        this.isRuleEditMode = true;
        for (var n = 0; n < this.sourceDataForEdit.length; n++) {
          // debugger;
          this.items = this.dataControlForm.get('items') as FormArray;
          this.items.push(this.fb.group({
            layer: [this.sourceDataForEdit[n].src_layer_id],
            object: [this.sourceDataForEdit[n].src_obj_id],
            metricAttr: [this.sourceDataForEdit[n].src_attr_id !== null ? this.sourceDataForEdit[n].src_attr_id.split(',') : ''],
            grpByAttr1: [this.sourceDataForEdit[n].bk_attr_id.length > 0 ? this.sourceDataForEdit[n].bk_attr_id[0] : ''],
            grpByAttr2: [this.sourceDataForEdit[n].bk_attr_id.length > 1 ? this.sourceDataForEdit[n].bk_attr_id[1] : ''],
            fltrConn: [this.sourceDataForEdit[n].src_fltr_con]
          }));
          // console.log(this.sourceDataForEdit[n].src_fltr_con);
          // const srcItems = this.dataControlForm.get('items') as FormArray;
          // srcItems.push(this.fb.group({
          //   layer: this.sourceDataForEdit[n].src_layer_id, 
          //   object: this.sourceDataForEdit[n].src_obj_id, 
          //   metricAttr: this.sourceDataForEdit[n].src_attr_id !== null ? this.sourceDataForEdit[n].src_attr_id.split(',') : '',
          //   grpByAttr: this.sourceDataForEdit[n].bk_attr_id !== null ? this.sourceDataForEdit[n].bk_attr_id.split(',') : '', 
          //   fltrConn: this.sourceDataForEdit[n].src_fltr_con
          // }));
        }
        // console.log(this.dataControlForm.value);
      });
  }
}
