import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DialogComponent, ButtonPropsModel, PositionDataModel } from '@syncfusion/ej2-angular-popups';
import { CommandModel, GridComponent, ResizeService, ReorderService, ToolbarService, ColumnChooserService, ToolbarItems, IRow, Column } from '@syncfusion/ej2-angular-grids';
//import { NgForm } from '@angular/forms';
import { DataQualityDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataQualityDetails';
import { KPIDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/KPIDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
//import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { closest, EmitType } from '@syncfusion/ej2-base';
import { ContentService } from 'src/app/shared/services/content.service';
import { CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';
import { KpiValidationFormComponent } from '../kpi-validation-form/kpi-validation-form.component';


@Component({
  selector: 'app-kpi-validation-list',
  templateUrl: './kpi-validation-list.component.html',
  styleUrls: ['./kpi-validation-list.component.css'],
  providers: [CheckBoxSelectionService, ResizeService, ReorderService, ToolbarService, ColumnChooserService]
})

export class KpiValidationListComponent implements OnInit {

  //@ViewChild('addNewThresholdRuleForm', { static: false }) addNewThresholdRuleForm: NgForm;
  @ViewChild('addNewKPIRuleDialog', { static: false }) addNewKPIRuleDialog: DialogComponent;
  @ViewChild('kpiListGrid', { static: false }) kpiListGrid: GridComponent;
  @ViewChild('kpiValidationFormComponent', { static: false }) kpiValidationFormComponent: KpiValidationFormComponent;
  @Input() dataQualityDetails: DataQualityDetails;
  @Input() active: boolean;
  @Input() gridResultsCountKPI: number;

    //search filter
    @Input('searchtxt') searchtxt: string; 
    @Input('layerToFilter') layerToFilter: number[]; 
    @Input('objToFilter') objToFilter: number[];
    @Input('severityToFilter') severityToFilter: string[]; 
    @Input('statusToFilter') statusToFilter: string[];
    @Output() totalKPIRulesCount = new EventEmitter<number>();

    // count of filtered data after search
    filterMetadata = { count: 0 };

  public gridCommands: CommandModel[];
  gridToolbar: ToolbarItems[] | Object;
  public position: PositionDataModel = { X: 'right', Y: 'top' };
  dlgAnimationSettings: Object = { effect: 'SlideRight', duration: 600, delay: 0 };
  editKpiDetails: KPIDetails;
  KPIRules: KPIDetails[];

  status: boolean;
  isRuleEditMode: boolean = false;
  reportColList: any;

  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };
  objectList: [];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };
  attributeList: any[];
  timePeriodAttrList: any[];
  isTimePeriodAttr: boolean;
  attributeFields: object = { text: 'at_nm', value: 'at_id' };
  checkList: any[];
  checkFields: object = { text: 'visual_desc', value: 'master_rule_id' };
  activeCheckType = '';
  kpi: any;
  severityOptions: string[];
  dataQualityContent: any;
  gridPageSettings: Object;
  pageCounts = [1,2,3,4];
  currentPage = 1;  
  latest_rule_id = 0;

  dlgButtons: ButtonPropsModel[];
  formInvalidAfterSaveClick: boolean = false;
  // disable : boolean = true;

  constructor(private dataService: RestService, cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.kpi = this.dataQualityContent.subMenu.kpi;
    this.severityOptions = this.kpi.severityOptions;

    this.dlgButtons = [
      {
        click: this.dlgCancelBtnClick.bind(this), buttonModel: {
          content: this.kpi.buttons.cancel,
          cssClass: 'btn btn-secondary actionButton'
        }
      },
      {
        click: this.dlgSaveBtnClick.bind(this), buttonModel: {
          content: this.kpi.buttons.save,
          cssClass: 'btn btn-primary actionButton', disabled: false
        }
      }
    ];
  }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: true, pageSize:5, currentPage: this.currentPage, template: ''};    
    this.gridCommands = [{
        type: 'Edit', buttonOption: {
        iconCss: ' e-icons e-edit', cssClass: 'e-flat',
        click: this.editKPIRule.bind(this)
      }
    }];

    this.setToolbar();
    this.populateKPIRules();
  }

  onKPIListGridDataBound()
  {
    this.totalKPIRulesCount.emit(this.filterMetadata.count);
  }

  setToolbar(){
    this.gridToolbar = [{ type: 'label', template: '#showKPIResultsCount', align: 'Right' },
    { type: 'Input', template: '#addKPIRuleTemplate', align: 'Right', id: 'addRule', click: this.addRuleHandler.bind(this) },
    { type: 'Input', template: '#exportKPITemplate', align: 'Right' },
    { text: 'ColumnChooser', template: '#columnChooserTemplateKPI', align: 'Right' }];
  }

  addRuleHandler(args: ClickEventArgs): void {
    if (args.item.id === "addRule") {
      this.addKPIRule();
    }
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    //this. = arg.itemData.value;
  }

  changePageNumber(currentPage: any) {
    this.currentPage = currentPage;
    this.gridPageSettings = { currentPage: this.currentPage };
  }

  populateKPIRules() {
    this.dataService.getData(environment.dataQuality.getKPIRules,
      { name: 'get_kpi_rules', src_data_layer_id: 0, 
      obj_id: 0, rule_id: 0 })
      .subscribe(data => { 
        this.KPIRules = data && data.Contents ? data.Contents : [];
        this.gridResultsCountKPI = this.KPIRules.length;
      });
  }

  openKPIRuleForm(isRuleEditMode: boolean) {
    // Open the Threshold Form Dialog with slide effect
    this.addNewKPIRuleDialog.show();
    // Call the addNewThRule on the Form
    if (!isRuleEditMode)
      this.kpiValidationFormComponent.addNewKPIRule(this.dataQualityDetails);
  }

  editKPIRule(args: Event): void {
    this.isRuleEditMode = true;
    this.openKPIRuleForm(true);

    const rowObj: IRow<Column> = this.kpiListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').
      getAttribute('data-uid'));

    const kpiDetails = rowObj.data as KPIDetails;

   this.kpiValidationFormComponent.editKPIRule(kpiDetails);
  }

  addKPIRule() {
    this.isRuleEditMode = false;
    this.openKPIRuleForm(false);
  }

  dlgCancelBtnClick() {
    // Reset the fields in form to defaults
    this.kpiValidationFormComponent.resetVariablesToDefaults();
    // Hide the RI Form
    this.addNewKPIRuleDialog.hide();
  }

  public modalDlgClose: EmitType<Event> = () => {
    this.dlgCancelBtnClick();
  }

  dlgSaveBtnClick() {
    // Call the saveNewKPIRule() on the Form
    this.kpiValidationFormComponent.saveNewKPIRule();

    // Hide the KPI Form & Repopulate the Rules
   if (!this.kpiValidationFormComponent.addNewKPIRuleForm.invalid) {
     this.addNewKPIRuleDialog.hide();
     //Fetch the latest rule id after rule save/update
   this.latest_rule_id = this.kpiValidationFormComponent.latest_rule_id;
   }
  }

  updateLatestRuleId(val:any){
    this.latest_rule_id = val;
    this.populateKPIRules();
  }

}

