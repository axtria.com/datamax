export class ThresholdDetails {
    rule_id: number;
    processing_stage: string;
    data_layer_id: number;
    grnl_lvl: string;
    obj_id: number;    
    attr_id: any;    
    bk_attr_id: any;
    agg_fn: string;
    th_val: number;
    fltr_con: string;
    rule_desc: string;
    mstr_rule_id: any;
    rule_svrt: string;
    rule_cat: string;
    is_rule_actv: boolean;
    actv_disp: string;
    data_layer_nm: string;
    obj_nm: string;
    attr_nm: string;
    bk_attr_nm: string;
    trending_attr_id: any;
    dimension_attr_nm: string;
    trending_attr_nm: string;
    dimension_attr_id: any;
    visual_rule_desc: string;
}
   
 