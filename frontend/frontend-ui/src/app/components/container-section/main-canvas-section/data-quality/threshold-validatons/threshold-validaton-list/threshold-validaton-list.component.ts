import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DialogComponent, ButtonPropsModel, PositionDataModel } from '@syncfusion/ej2-angular-popups';
import { CommandModel, GridComponent, IRow, Column, ResizeService, ReorderService, ToolbarService, ColumnChooserService, ToolbarItems } from '@syncfusion/ej2-angular-grids';
//import { NgForm } from '@angular/forms';
import { DataQualityDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataQualityDetails';
import { ThresholdDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/ThresholdDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
//import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { closest, EmitType } from '@syncfusion/ej2-base';
import { ContentService } from 'src/app/shared/services/content.service';
import { CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';
import { ThresholdValidatonFormComponent } from '../threshold-validaton-form/threshold-validaton-form.component';


@Component({
  selector: 'app-threshold-validaton-list',
  templateUrl: './threshold-validaton-list.component.html',
  styleUrls: ['./threshold-validaton-list.component.css'],
  providers: [CheckBoxSelectionService, ResizeService, ReorderService, ToolbarService, ColumnChooserService]
})

export class ThresholdValidatonListComponent implements OnInit {

  //@ViewChild('addNewThresholdRuleForm', { static: false }) addNewThresholdRuleForm: NgForm;
  @ViewChild('addNewThresholdRuleDialog', { static: false }) addNewThresholdRuleDialog: DialogComponent;
  @ViewChild('thresholdListGrid', { static: false }) thresholdListGrid: GridComponent;
  @ViewChild('thresholdFormComponent', { static: false }) thresholdFormComponent: ThresholdValidatonFormComponent;
  @Input() dataQualityDetails: DataQualityDetails;
  @Input() active: boolean;

    //search filter
    @Input('searchtxt') searchtxt: string; 
    @Input('layerToFilter') layerToFilter: number[]; 
    @Input('objToFilter') objToFilter: number[];
    @Input('trendAttrToFilter') trendAttrToFilter: string[];
    @Input('ruleToFilter') ruleToFilter: number[];
    @Input('aggFnToFilter') aggFnToFilter: string[];
    @Input('severityToFilter') severityToFilter: string[]; 
    @Input('statusToFilter') statusToFilter: string[];
    @Output() totalTHRulesCount = new EventEmitter<number>();
    @Input() gridResultsCountTH: number;

    // count of filtered data after search
    filterMetadata = { count: 0 };

  public gridCommands: CommandModel[];
  gridToolbar: ToolbarItems[] | Object;
  public position: PositionDataModel = { X: 'right', Y: 'top' };
  dlgAnimationSettings: Object = { effect: 'SlideRight', duration: 600, delay: 0 };
  editThresholdDetails: ThresholdDetails;
  thresholdRules: ThresholdDetails[];

  status: boolean;
  isRuleEditMode: boolean = false;
  reportColList: any;

  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };
  objectList: [];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };
  attributeList: any[];
  timePeriodAttrList: any[];
  isTimePeriodAttr: boolean;
  attributeFields: object = { text: 'at_nm', value: 'at_id' };
  checkList: any[];
  checkFields: object = { text: 'visual_desc', value: 'master_rule_id' };
  activeCheckType = '';
  aggFuncList: string[];
  threshold: any;
  severityOptions: string[];
  dataQualityContent: any;
  gridPageSettings: Object;
  pageCounts = [1,2,3,4];
  currentPage = 1;  
  //latest_rule_id = 0;

  dlgButtons: ButtonPropsModel[];
  formInvalidAfterSaveClick: boolean = false;
  saveAfterDupCheck: boolean = false;
  // disable : boolean = true;

  constructor(private dataService: RestService, cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.threshold = this.dataQualityContent.subMenu.threshold;
    this.severityOptions = this.threshold.severityOptions;
    this.aggFuncList = this.threshold.aggFuncList;

    this.dlgButtons = [
      {
        click: this.dlgCancelBtnClick.bind(this), buttonModel: {
          content: this.threshold.buttons.cancel,
          cssClass: 'btn btn-secondary actionButton'
        }
      },
      {
        click: this.dlgSaveBtnClick.bind(this), buttonModel: {
          content: this.threshold.buttons.save,
          cssClass: 'btn btn-primary actionButton', disabled: false
        }
      }
    ];
  }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: true, pageSize:5, currentPage: this.currentPage, template: ''};    
    this.gridCommands = [{
        type: 'Edit', buttonOption: {
        iconCss: ' e-icons e-edit', cssClass: 'e-flat',
        click: this.editThresholdRule.bind(this)
      }
    }];

    this.setToolbar();
    this.populateThresholdRules();
  }

  onTHListGridDataBound()
  {
    this.totalTHRulesCount.emit(this.filterMetadata.count);
  }

  setToolbar(){
    this.gridToolbar = [{ type: 'label', template: '#showThResultsCount', align: 'Right' },
    { type: 'Input', template: '#addThRuleTemplate', align: 'Right', id: 'addRule', click: this.addRuleHandler.bind(this) },
    { type: 'Input', template: '#exportThTemplate', align: 'Right' },
    { text: 'ColumnChooser', template: '#columnChooserTemplateTh', align: 'Right' }];
  }

  addRuleHandler(args: ClickEventArgs): void {
    if (args.item.id === "addRule") {
      this.addThresholdRule();
    }
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    //this. = arg.itemData.value;
  }

  changePageNumber(currentPage: any) {
    this.currentPage = currentPage;
    this.gridPageSettings = { currentPage: this.currentPage };
  }

  populateThresholdRules() {
    this.dataService.getData(environment.dataQuality.getThresholdRules,
      { name: 'get_th_rules', 
      src_data_layer_id: 0,
      obj_id: 0, attr_id: 0, rule_id: 0 })
      .subscribe(data => { 
        this.thresholdRules = data && data.Contents ? data.Contents : [];
        this.gridResultsCountTH = this.thresholdRules.length;
       });
  }

  openThresholdRuleForm(isRuleEditMode: boolean) {
    // Open the Threshold Form Dialog with slide effect
    this.addNewThresholdRuleDialog.show();
    // Call the addNewThRule on the Form
    if (!isRuleEditMode)
      this.thresholdFormComponent.addNewThresholdRule(this.dataQualityDetails);
  }

  editThresholdRule(args: Event): void {

    this.isRuleEditMode = true;
    this.openThresholdRuleForm(true);

    const rowObj: IRow<Column> = this.thresholdListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').getAttribute('data-uid'));

    const thDetails = rowObj.data as ThresholdDetails;

    this.thresholdFormComponent.editThresholdRule(thDetails);
  }

  addThresholdRule() {
    this.isRuleEditMode = false;
    this.openThresholdRuleForm(false);
  }

  
  dlgCancelBtnClick() {
    // Reset the fields in form to defaults
    this.thresholdFormComponent.resetVariablesToDefaults();
    // Hide the RI Form
    this.addNewThresholdRuleDialog.hide();
  }

  public modalDlgClose: EmitType<Event> = () => {
    this.dlgCancelBtnClick();
  }

  dlgSaveBtnClick() {
    // Calling save function from Form Component
    this.thresholdFormComponent.saveThresholdRule();
    this.saveAfterDupCheck = true;
  }

 hideDialog(val){
   if (val){
     this.saveAfterDupCheck = false;
     this.addNewThresholdRuleDialog.hide();
     this.populateThresholdRules();
   }
 }

}
