export class DataValidationRuleDetails {
    rule_id: string;
    data_layer_id: number;
    data_layer_name: string;
    obj_id: number;
    obj_nm: string;
    attr_id_lst: any;
    attr_nm_lst: string;
    bk_attr_id_lst: any;
    bk_attr_nm_lst: string;    
    mstr_rule_id: string;
    visual_rule_desc: string;
    arg_1: any;
    arg_2: any;
    arg_3: any;
    arg_4: any;
    rule_desc: string;
    is_rule_actv: boolean;
    rule_svrt: string;
    fltr_con: string;    
    grnl_lvl: string;
    rule_cat: string;
}