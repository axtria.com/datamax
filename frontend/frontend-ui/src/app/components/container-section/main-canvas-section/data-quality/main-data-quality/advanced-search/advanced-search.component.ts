import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { DataQualityDetails } from '../../DataQualityDetails';
import { FilterCriteria } from './filterCriteria';
import { DataValidationListComponent } from '../../data-validations/data-validation-list/data-validation-list.component';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.css']
})
export class AdvancedSearchComponent implements OnInit {
  @Output() populateRules = new EventEmitter<any[]>();
  @Output() eventClicked = new EventEmitter<any[]>();

  dataValidationListComponent: DataValidationListComponent;

  dataQualityContent: any;
  filterInput = '';

  allAttributes = [];
  attributesForFilter = [];
  selectedAttributes = [];

  queryResults: any[] = [];
  queryField: FormControl = new FormControl();
  prevQuery = null;
  dataToSearch = [];

  data = [];
  filteredData = [];
  selectedFilterCriteria = { data_layer_id: null, obj_id: null, attr_id_lst: null, mstr_rule_id: null, bk_attr_id_lst: null, rule_desc: null, is_rule_actv: null, rule_svrt: null };

  severityOptions: string[];
  activeOptions: string[];
  DataLayerOptions: [];  //{layer_id: 3, name: "public"}
  RuleTypeOptions: []; //{mstr_rule_id: "MRDQ1003", mstr_rule_desc: "Length Check", visual_rule_desc: "LENGTH CHECK", multi_attr_select: false, multi_attr_rule: false}

  DataLayerFields: object = { value: 'layer_id', text: 'name' };
  objectList = [];
  objectFields: object = { value: 'obj_id', text: 'obj_nm' };
  RuleTypeFields: object = { value: "mstr_rule_id", text: "visual_rule_desc" }

  dataQualityDetails: DataQualityDetails = {
    obj_id: null, attr_id: null, rule_id: null, obj_name: null, layer_id: null
  };
  filterCriteria: FilterCriteria = {
    data_layer_id: null,
    data_layer_name: null,
    obj_id: null,
    obj_nm: null,
    attr_id_lst: null,
    attr_nm_lst: null,
    bk_attr_id_lst: null,
    bk_attr_nm_lst: null,
    mstr_rule_id: null,
    visual_rule_desc: null,
    rule_desc: null,
    is_rule_actv: null,
    rule_svrt: null
  };

  constructor(private dataService: RestService, cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.severityOptions = this.dataQualityContent.commonContent.severityOptions;
    this.activeOptions = ["Yes", "No"]
  }

  ngOnInit() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_obj_meta_data_layers' })
      .subscribe(data => {
        this.DataLayerOptions = data && data.Contents ? data.Contents : [];
        // this.allAttributes[0].dataSource = this.DataLayerOptions;
      });

    this.dataService.getData(environment.dataQuality.getDQDataValidationCheckType,
      { name: 'get_data_validation_checks' })
      .subscribe(data => {
        this.RuleTypeOptions = data && data.Contents ? data.Contents : [];
        this.allAttributes[0].dataSource = this.RuleTypeOptions;
      });

    // this.dataService.getData(environment.dataQuality.getDataValidationRules,
    //   {
    //     name: 'get_data_validation_rules', src_data_layer_id: this.dataQualityDetails.layer_id,
    //     obj_id: this.dataQualityDetails.obj_id, attr_id_lst: 0, rule_id: 0, latest_rule_id: 0
    //   })
    //   .subscribe(data => {
    //     this.data = data && data.Contents ? data.Contents : [];
    //     this.filteredData = this.data;
    //   });

    // this.queryField.valueChanges
    //   .subscribe(result => console.log(result));

    this.queryField.valueChanges
      .subscribe(queryField => this.search(queryField));
    // .subscribe(response => this.queryResults = this.response.json().artists.items));

    // this.allAttributes = [{ name: "Data Layer", value: "data_layer_id", dataSource: "", fields: this.DataLayerFields }, { name: "Object Name", value: "obj_id", dataSource: this.DataLayerOptions, fields: this.objectFields },
    // { name: "Attribute Name", value: "attr_id_lst", dataSource: this.DataLayerOptions, fields: "" }, { name: "Rule Type", value: "mstr_rule_id", dataSource: this.RuleTypeOptions, fields: this.RuleTypeFields },
    // { name: "Additional Reporting", value: "bk_attr_id_lst", dataSource: this.DataLayerOptions, fields: "" }, { name: "Rule Description", value: "rule_desc", dataSource: this.DataLayerOptions, fields: "" },
    // { name: "Active", value: "is_rule_actv", dataSource: this.DataLayerOptions, fields: "" }, { name: "Severity", value: "rule_svrt", dataSource: this.severityOptions, fields: "" }];

    this.allAttributes = [{ name: "Rule Type", value: "mstr_rule_id", dataSource: this.RuleTypeOptions, fields: this.RuleTypeFields },
    { name: "Attribute Name", value: "attr_id_lst", dataSource: null, fields: "" }, { name: "Additional Reporting", value: "bk_attr_id_lst", dataSource: null, fields: "" },
    { name: "Active", value: "is_rule_actv", dataSource: this.activeOptions, fields: "" }, { name: "Severity", value: "rule_svrt", dataSource: this.severityOptions, fields: "" }];

    this.attributesForFilter = this.allAttributes;
    this.selectedAttributes = [];
  }

  dataLayerChange(e: any) {
    if(e.value !== null){
      this.dataQualityDetails.layer_id = e.value.toString().replace(/\,/g, '|');
    }
    this.selectedFilterCriteria.obj_id = null;
    this.dataQualityDetails.obj_id = null;
    this.objectList = [];

    if (e.value !== undefined) {

      this.selectedFilterCriteria.data_layer_id = e.value;
      this.filterData();

        this.dataService.getData(environment.dataQuality.getObjects,
          { name: 'get_data_layer_objects', src_data_layer_id: this.dataQualityDetails.layer_id === null ? 0 : this.dataQualityDetails.layer_id })
          .subscribe(data => { this.objectList = data && data.Contents ? data.Contents : [] });


      // for (let lyr_id in e.value) {
      //   let temp_obj = [];
      //   this.dataService.getData(environment.dataQuality.getObjects,
      //     { name: 'get_data_layer_objects', src_data_layer_id: lyr_id })
      //     .subscribe(data => { temp_obj = data && data.Contents ? data.Contents : []; console.log(data.Contents); this.objectList.push(temp_obj)});
      // }
    }


  }

  objectChange(e: any) {
    if (e.itemData !== null) {
      this.dataQualityDetails.obj_id = e.value;
      this.selectedFilterCriteria.obj_id = e.value;
      this.filterData();
    }
  }

  attributeChange(e: any) {
    this.selectedFilterCriteria.attr_id_lst = e.value;
    this.filterData();
  }

  bkAttributeChange(e: any) {
    this.selectedFilterCriteria.bk_attr_id_lst = e.value;
    this.filterData();
  }


  attributeSelected(attrVal: any) {
    this.attributesForFilter = this.attributesForFilter.filter(function (item: any) {
      return item.value !== attrVal.value
    });
    this.selectedAttributes.push(attrVal);
    if (attrVal.value === "is_rule_actv") {
      this.selectedFilterCriteria.is_rule_actv = true;
      this.filterData();
    }
  }
  deleted(toDelete: any) {
    this.attributesForFilter.push(toDelete);
    this.selectedAttributes = this.selectedAttributes.filter(item => item !== toDelete);
    this.selectedFilterCriteria[toDelete.value] = null;
    this.filterData();
  }
  ruleTypeChange(e: any) {
    this.selectedFilterCriteria.mstr_rule_id = e.value;
    this.filterData();
  }
  severityTypeChange(svrt: string) {
    this.selectedFilterCriteria.rule_svrt = svrt;
    this.filterData();
  }
  statusChange(e: any) {
    this.selectedFilterCriteria.is_rule_actv = e.checked;
    this.filterData();
  }

  clearSearches(){
    this.attributesForFilter = this.allAttributes;
    this.selectedAttributes = [];
    this.selectedFilterCriteria = { data_layer_id: null, obj_id: null, attr_id_lst: null, mstr_rule_id: null, bk_attr_id_lst: null, rule_desc: null, is_rule_actv: null, rule_svrt: null };
    this.dataQualityDetails = { obj_id: null, attr_id: null, rule_id: null, obj_name: null, layer_id: null };
  }

  emtiUpdatedRulesToSearch(updatedRules) {
    this.data = updatedRules;
    this.filterData();
    this.selectedAttributes = [];
    this.attributesForFilter = this.allAttributes;
    this.dataQualityDetails.layer_id = null;
    this.dataQualityDetails.obj_id = null;
  }

  filterData() {
    this.filteredData = this.data;
    //layer type
    if (this.selectedFilterCriteria.data_layer_id !== null && this.selectedFilterCriteria.data_layer_id.length > 0) {
      this.filteredData = this.filteredData.filter(item => {
        return this.selectedFilterCriteria.data_layer_id.includes(item.data_layer_id);
        // return item.data_layer_id === this.selectedFilterCriteria.data_layer_id;
      });
    }

    //Object type
    if (this.selectedFilterCriteria.obj_id !== null && this.selectedFilterCriteria.obj_id.length > 0) {
      this.filteredData = this.filteredData.filter(item => {
        return this.selectedFilterCriteria.obj_id.includes(item.obj_id);
        // return item.obj_id === this.selectedFilterCriteria.obj_id;
      });
    }

    //rule type
    if (this.selectedFilterCriteria.mstr_rule_id !== null) {
      this.filteredData = this.filteredData.filter(item => {
        return this.selectedFilterCriteria.mstr_rule_id.includes(item.mstr_rule_id);
      });
    }

    //attribute type
    if (this.selectedFilterCriteria.attr_id_lst !== null) {
      this.filteredData = this.filteredData.filter(item => {
        return this.selectedFilterCriteria.attr_id_lst.includes(item.attr_id_lst);
      });
    }

    //bk_attribute type
    if (this.selectedFilterCriteria.bk_attr_id_lst !== null) {
      this.filteredData = this.filteredData.filter(item => {
        return this.selectedFilterCriteria.bk_attr_id_lst.includes(item.bk_attr_id_lst);
      });
    }

    //severity type
    if (this.selectedFilterCriteria.rule_svrt !== null) {
      this.filteredData = this.filteredData.filter(item => {
        return item.rule_svrt.toUpperCase() === this.selectedFilterCriteria.rule_svrt.toUpperCase();
      });
    }

    //active type
    if (this.selectedFilterCriteria.is_rule_actv !== null) {
      this.filteredData = this.filteredData.filter(item => {
        return this.selectedFilterCriteria.is_rule_actv ? item.is_rule_actv === true : item.is_rule_actv === false;
      });
    }

    this.populateRules.emit(this.filteredData);
  }

  search(queryField: string) {
    // let filtCount = 0;
    let queryResult = [];

    if (this.prevQuery === null || this.prevQuery > queryField) {
      this.dataToSearch = this.filteredData;
      this.dataToSearch.forEach(obj => {
        for (let content in obj) {
          if (obj[content] !== null && obj[content].toString().toUpperCase().indexOf(queryField.toUpperCase()) > -1) {
            queryResult.push(obj);
            // filtCount++;
            break;
          }
        }
        return;
      })
      this.prevQuery = queryField;
      this.dataToSearch = queryResult;
    }

    if (this.prevQuery < queryField) {
      this.dataToSearch.forEach(obj => {
        for (let content in obj) {
          if (obj[content] !== null && obj[content].toString().toUpperCase().indexOf(queryField.toUpperCase()) > -1) {
            queryResult.push(obj);
            // filtCount++;
            break;
          }
        }
        return;
      })
      this.prevQuery = queryField;
      this.dataToSearch = queryResult;
    }

    // this.filteredData.forEach(obj => {
    //   for (let content in obj) {
    //       if(obj[content] !== null && obj[content].toString().indexOf(queryField) > -1 ){
    //         queryResult.push(obj);
    //         filtCount++;
    //       } 
    //   }
    //   return queryResult;
    // })
    this.populateRules.emit(queryResult);
  }

}
