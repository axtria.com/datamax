import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { DataQualityDetails } from '../../DataQualityDetails';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { GridComponent, ToolbarItems, CommandModel } from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';

@Component({
  selector: 'app-object-rules-list',
  templateUrl: './object-rules-list.component.html',
  styleUrls: ['./object-rules-list.component.css']
})
export class ObjectRulesListComponent implements OnInit {
  @ViewChild('objectRulesListGrid', { static: false }) objectRulesListGrid: GridComponent;
  @Input() dataQualityDetails: DataQualityDetails;
  @Input() objId: number;
  @Output() objectName = new EventEmitter<number>();
  @Input() active: boolean;
  objectRules: any[];
  dataQualityContent: any;
  currentPage: 1;
  gridPageSettings: Object;
  gridToolbar: ToolbarItems[] | Object;
  gridCommands: CommandModel[];
  numberOfObjRules: any;
  objRuleListFormContent: any;
  filterMetadata = { count: 0 };


  constructor(private dataService: RestService, cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.objRuleListFormContent = this.dataQualityContent.objSubMenu.objectRules;
  }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: true, pageSize:5, currentPage: this.currentPage, template: '', pageCount:5};    
    this.gridCommands = [{
      type: 'Edit', buttonOption: {
        iconCss: 'e-icons e-edit', cssClass: 'e-flat',
        //click: this.editDataValidationRule.bind(this)
      }
    }];
    this.setToolbar();
    this.populateObjectRules();
    
  }

  populateObjectRules(){
    this.dataService.getData(environment.dataQuality.getObjectRules,
      {
        name: 'get_dq_object_rules_info',
        obj_id: this.objId
      })
      .subscribe(data => { this.objectRules = data && data.Contents ? data.Contents : []; 
        this.objectName.emit(this.objectRules[0].obj_nm);
      });
  }

  setToolbar(){
    this.gridToolbar = [{ type: 'label', template: '#showObjectResultsCount', align: 'Right' },
    { type: 'Input', template: '#addObjectRuleTemplate', align: 'Right', id: 'addRule', click: this.addRuleHandler.bind(this) },
    { text: 'ColumnChooser', template: '#columnChooserTemplateObject', align: 'Right' }];
  }

  addRuleHandler(args: ClickEventArgs): void {
    if (args.item.id === "addRule") {
      //this.addDataValidationRule();
    }
  }
  
  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.numberOfObjRules = arg.itemData.value;
  }

  changePageNumber(currentPage: any) {
    this.currentPage = currentPage;
    this.gridPageSettings = { currentPage: this.currentPage };
  }

}
