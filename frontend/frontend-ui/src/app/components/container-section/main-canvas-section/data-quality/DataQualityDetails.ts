export class DataQualityDetails {
    obj_id: number;
    attr_id: number;
    rule_id: number;
    layer_id: number;
    obj_name: string;
}