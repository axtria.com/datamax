import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataQualityDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataQualityDetails';
import { ThresholdDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/ThresholdDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { UtilService } from 'src/app/shared/services/util.service';

@Component({
  selector: 'app-threshold-validaton-form',
  templateUrl: './threshold-validaton-form.component.html',
  styleUrls: ['./threshold-validaton-form.component.css'],
  providers: [CheckBoxSelectionService]
})
export class ThresholdValidatonFormComponent implements OnInit {

  @ViewChild('addNewThresholdRuleForm', { static: false }) addNewThresholdRuleForm: NgForm;
  @Input() dataQualityDetails: DataQualityDetails;
  @Input() saveAfterDupCheck: boolean;
  @Output() hideDialog = new EventEmitter<boolean>();
  utilService = new UtilService;
  editThresholdDetails: ThresholdDetails;
  thresholdRules: ThresholdDetails[];
  status: boolean;
  isRuleEditMode: boolean = false;
  dimAttrList: any[];
  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };
  objectList: [];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };
  trendingAttrList: any[];
  timePeriodAttrList: any[];
  isTimePeriodAttr: boolean;
  attributeFields: object = { text: 'at_nm', value: 'at_id' };
  checkTypeList: any[];
  checkTypeFields: object = { text: 'visual_rule_desc', value: 'mstr_rule_id' };
  aggFuncList: string[];
  threshold: any;
  severityOptions: string[];
  formInvalidAfterSaveClick: boolean = false;
  activeSeverityOption: string;
  commonContent: any;
  applyDropdownList: any[];
  activeApplyDropdownList: string;
  aggFuncListTrend: string[];
  aggFuncListVariation: string[];
  aggrFuncAttrList: any[];
  aggrFuncFields: object = { text: 'text', value: 'value' };
  ruleAlreadyExist = false;
  ruleAlreadyExistMsg = '';
  public filterPlaceholder: any = 'Search';
  thDetails: ThresholdDetails = {
    rule_id: null, processing_stage: 'Post-Ingestion', data_layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
    fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null, data_layer_nm: null,
    obj_nm: null, attr_nm: null, bk_attr_nm: null, trending_attr_id: null, dimension_attr_nm: null, trending_attr_nm: null,
    dimension_attr_id: null, visual_rule_desc: null
  };
  

  constructor(private dataService: RestService, private toastPopupService: ToastPopupService, cs: ContentService, private authenticateService: AuthenticateService) {
    this.threshold = cs.getContent(environment.modules.dataQuality).subMenu.threshold;
    this.severityOptions = this.threshold.severityOptions;
    this.aggFuncListTrend = this.threshold.aggFuncListTrend;
    this.aggFuncListVariation = this.threshold.aggFuncListVariation;
    this.commonContent = cs.getContent(environment.modules.dataQuality).commonContent;
    this.applyDropdownList = this.commonContent.applyDropdownList;
    this.activeApplyDropdownList = this.applyDropdownList[0];
  }

  ngOnInit() {
    // Populate all the Data Threshold Rules
    this.populateDataLayer();
    // Populate Data Layer Dropdown
    this.populateCheckType();
  }

  populateDataLayer() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_obj_meta_data_layers' })
      .subscribe(data => { this.businessLayerList = data && data.Contents ? data.Contents : []; });
  }

  populateCheckType() {
    this.dataService.getData(environment.dataQuality.getCheckType,
      { name: 'get_dq_rule_checks', rule_type: 'Threshold' })
      .subscribe(data => { this.checkTypeList = data && data.Contents ? data.Contents : []; });
  }

  dataLayerChange(e: any) {
    this.thDetails.obj_id = null;
    this.objectList = [];
    if (e.itemData !== null) {
      this.populateObject(e.itemData.layer_id);
    }
  }

  populateObject(dataLayer: string) {
    this.dataService.getData(environment.dataQuality.getObjects,
      { name: 'get_data_layer_objects', src_data_layer_id: dataLayer })
      .subscribe(data => {
        this.objectList = data && data.Contents ? data.Contents : [];

        // If the form is in EDIT mode then pre-set the Object Dropdown - It will fire objectChange() method
        this.thDetails.obj_id = this.isRuleEditMode ? this.editThresholdDetails.obj_id : this.dataQualityDetails.obj_id
      });
  }

  objectChange(e: any) {
    this.thDetails.trending_attr_id = null;
    this.trendingAttrList = [];
    this.aggrFuncAttrList = [];
    this.thDetails.bk_attr_id = null;
    this.dimAttrList = [];
    if (e.itemData !== null) {
      this.populateAttribute(e.itemData.obj_id);
    }
  }

  checkTypeChange(e: any) {
    this.aggrFuncAttrList = [];
    this.dimAttrList = [];
    this.thDetails.trending_attr_id = null;
    this.thDetails.bk_attr_id = null;
    this.thDetails.attr_id = null;
    if (e.itemData !== null && this.checkTypeList.length > 0) {
      this.thDetails.rule_desc = e.itemData.mstr_rule_desc;
      this.aggFuncList = e.itemData.mstr_rule_id === 'MRDQ1017' ? this.aggFuncListVariation : this.aggFuncListTrend;
      if (this.trendingAttrList.length > 0 && e.itemData.mstr_rule_id === 'MRDQ1017') {
        this.aggrFuncAttrList = this.trendingAttrList;
      }
      else {
        this.aggrFuncAttrList = [];
      }
    }
  }

  populateAttribute(objectId: number) {
    this.dataService.getData(environment.dataQuality.getAttributes,
      { name: 'get_object_attribute_details', obj_id: objectId })
      .subscribe(data => {
        this.trendingAttrList = data && data.Contents ? data.Contents : [];
        if (this.thDetails.mstr_rule_id && this.thDetails.mstr_rule_id === 'MRDQ1017') {
          this.aggrFuncAttrList = this.trendingAttrList;
          if (this.thDetails.agg_fn && this.thDetails.agg_fn === "Count") {
            this.dimAttrList = this.trendingAttrList;
          }
        }
        if (this.isRuleEditMode) {
          this.thDetails.attr_id = this.editThresholdDetails.attr_id;
        }
      });
  }

  attributeChange(e: any) {
    this.thDetails.attr_id = null;
    this.aggrFuncAttrList = [];
    this.dimAttrList = [];
    this.thDetails.bk_attr_id = null;
    if (e.itemData) {
      this.aggrFuncAttrList = this.utilService.removeArrayPropItem(this.trendingAttrList, 'at_id', e.itemData);
    }
    if (this.isRuleEditMode) {
      this.thDetails = this.editThresholdDetails;
      this.status = this.editThresholdDetails.is_rule_actv;
    }
  }

  aggrFuncAttrChange(e: any) {
    if (e.value != null) {
      this.dimAttrList = [];
      this.thDetails.bk_attr_id = null;
      // Create Grouping Conditions List by excluding the Selected Attributes
      if (e.itemData) {
        this.dimAttrList = this.utilService.removeArrayPropItem(this.aggrFuncAttrList, 'at_id', e.itemData);
      }
    }
  }

  aggrFuncChange(e: any) {
    if (this.thDetails.mstr_rule_id && this.thDetails.mstr_rule_id === 'MRDQ1017') {
      this.dimAttrList = [];
      this.thDetails.bk_attr_id = null;
      if (e.value && e.value === "Count") {
        this.aggrFuncAttrList = [];
        this.thDetails.attr_id = null;
        this.dimAttrList = this.trendingAttrList;
      } else {
        this.aggrFuncAttrList = this.trendingAttrList;
      }
    }
  }
  addNewThresholdRule(dataQualityDetails: DataQualityDetails) {
    //Reset the previously filled form
    this.resetVariablesToDefaults();

    // This is to set the Main Data Quality Model to the local variable
    this.dataQualityDetails = dataQualityDetails;

    // This is to reset the default controls on the Form
    this.thDetails.data_layer_id = this.dataQualityDetails.layer_id === 0 ? null : this.dataQualityDetails.layer_id;

    //Set default severity and status
    //this.thDetails.mstr_rule_id = this.checkTypeList.length > 0 ? this.checkTypeList[0].mstr_rule_id : null;
    this.thDetails.is_rule_actv = true;
    this.thDetails.rule_svrt = this.severityOptions.length > 0 ? this.severityOptions[0] : '';

    this.editThresholdDetails = null;
  }

  resetVariablesToDefaults() {
    this.thDetails = {
      rule_id: null, processing_stage: 'Post-Ingestion', data_layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
      fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null, data_layer_nm: null,
      obj_nm: null, attr_nm: null, bk_attr_nm: null, trending_attr_id: null, dimension_attr_nm: null, trending_attr_nm: null,
      dimension_attr_id: null, visual_rule_desc: null
    };
    this.objectList = [];
    this.trendingAttrList = [];
    this.aggrFuncAttrList = [];
    this.dimAttrList = [];

    //Reset the form
    this.addNewThresholdRuleForm.reset();
    // To highlight the mandory or default input error message
    this.formInvalidAfterSaveClick = false;
    this.isRuleEditMode = false;
    this.saveAfterDupCheck = false;
    this.activeSeverityOption = this.severityOptions.length > 0 ? this.severityOptions[0] : '';
  }

  editThresholdRule(record: ThresholdDetails): void {

    this.resetVariablesToDefaults();
    this.isRuleEditMode = true;
    const dimensionAttribute = record.dimension_attr_id !== null ? record.dimension_attr_id.toString().replace(/\|/g, ' ').trim().split(' ').map(Number) : [];

    this.editThresholdDetails = {
      obj_id: record.obj_id,
      attr_id: record.attr_id,
      mstr_rule_id: record.mstr_rule_id,
      bk_attr_id: dimensionAttribute,
      rule_id: record.rule_id,
      rule_desc: record.rule_desc,
      processing_stage: record.processing_stage,
      data_layer_id: record.data_layer_id,
      grnl_lvl: record.grnl_lvl,
      rule_cat: 'A',
      rule_svrt: record.rule_svrt,
      is_rule_actv: record.is_rule_actv,
      agg_fn: record.agg_fn,
      th_val: record.th_val/100,
      fltr_con: record.fltr_con == "1=1" ? null : record.fltr_con,
      actv_disp: record.actv_disp,
      data_layer_nm: record.data_layer_nm,
      obj_nm: record.obj_nm,
      attr_nm: record.attr_nm,
      bk_attr_nm: record.bk_attr_nm,
      trending_attr_id: record.trending_attr_id,
      dimension_attr_nm: record.dimension_attr_nm && record.dimension_attr_nm !== '' ? record.dimension_attr_nm : "Null",
      trending_attr_nm: record.trending_attr_nm,
      dimension_attr_id: record.dimension_attr_id,
      visual_rule_desc: record.visual_rule_desc
    };

    // If the form is in EDIT mode then pre-set the DataLayer Dropdown - It will fire dataLayerChange() method
    this.thDetails.data_layer_id = this.editThresholdDetails.data_layer_id;
    this.thDetails = this.editThresholdDetails;
    this.dataService.getData(environment.dataQuality.getAttributes,
      { name: 'get_object_attribute_details', obj_id: this.editThresholdDetails.obj_id })
      .subscribe(data => {
        this.trendingAttrList = data && data.Contents ? data.Contents : [];
        this.populateEditDimList(this.editThresholdDetails.trending_attr_id, this.editThresholdDetails.attr_id);
      });
  }

  populateEditDimList(trending_attr_id: number, attr_id: number) {
    this.thDetails.bk_attr_id = [];
    this.thDetails.dimension_attr_id = [];
    this.dimAttrList = [];

    if (trending_attr_id !== null) {
      this.dimAttrList = this.trendingAttrList.filter(function (item: any) {
        if ((Number)(item.at_id) !== (Number)(trending_attr_id)) {
          return item;
        }
      });
    }
    if (attr_id !== null) {
      this.dimAttrList = this.dimAttrList.filter(function (itemAttr: any) {
        if ((Number)(itemAttr.at_id) !== (Number)(attr_id)) {
          return itemAttr;
        }
      });
    }
  }
  checkDuplicity(e: any) {
    const value = e === null ? '' : e.value;
    if (this.thDetails.obj_id !== null) {
      this.dataService.getData(environment.dataQuality.checkDuplicityThRule,
        {
          name: 'th_dup_check',
          obj_id: this.thDetails.obj_id,
          attr_id: this.thDetails.attr_id ? this.thDetails.attr_id : "NONE",
          agg_fn: this.thDetails.agg_fn,
          mstr_rule_id: this.thDetails.mstr_rule_id,
          bk_attr_id: this.thDetails.trending_attr_id && this.thDetails.trending_attr_id.toString() !== '' ?
            (value && value.toString() !== '' ? this.thDetails.trending_attr_id.toString().concat('|', value.toString().replace(/,/g, '|')) : this.thDetails.trending_attr_id.toString()) :
            (value && value.toString() !== '' ? value.toString().replace(/,/g, '|') : "NONE"),
        })
        .subscribe(data => {
          this.ruleAlreadyExistMsg = data.Contents[0].message;
          if (data.Contents[0].status === "False") {
            this.ruleAlreadyExist = true;
            this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.ruleAlreadyExistMsg));
          } else {
            this.ruleAlreadyExist = false;
          }
          if (((this.thDetails.bk_attr_id && this.thDetails.bk_attr_id.length === 0) || this.thDetails.bk_attr_id === null) && this.saveAfterDupCheck) {
            if (this.ruleAlreadyExist) {
              return;
            }
            this.saveThRule();
          }
        });
    }
  }

  saveThresholdRule() {
    if ((this.thDetails.bk_attr_id && this.thDetails.bk_attr_id.length === 0) || this.thDetails.bk_attr_id === null && !this.isRuleEditMode) {
      this.checkDuplicity(this.thDetails.bk_attr_id);
    }
    else {
      this.saveThRule();
    }

  }

  saveThRule() {
    if (this.addNewThresholdRuleForm.invalid) {
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.threshold.popupMsg.invalid));
      this.formInvalidAfterSaveClick = this.addNewThresholdRuleForm.invalid;
    }
    else {
      if (!this.isRuleEditMode) {
        if (this.ruleAlreadyExist) {
          this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.ruleAlreadyExistMsg));
        } else {
          this.dataService.getData(environment.dataQuality.addThresholdRule,
            {
              name: 'add_dq_th_rule',
              obj_id: this.thDetails.obj_id,
              mstr_rule_id: this.thDetails.mstr_rule_id,
              attr_id: this.thDetails.attr_id ? this.thDetails.attr_id : "NONE",
              bk_attr_id: this.thDetails.trending_attr_id && this.thDetails.trending_attr_id.toString() !== '' ?
                (this.thDetails.bk_attr_id && this.thDetails.bk_attr_id.toString() !== '' ? this.thDetails.trending_attr_id.toString().concat('|', this.thDetails.bk_attr_id.toString().replace(/,/g, '|')) : this.thDetails.trending_attr_id.toString()) :
                (this.thDetails.bk_attr_id && this.thDetails.bk_attr_id.toString() !== '' ? this.thDetails.bk_attr_id.toString().replace(/,/g, '|') : "NONE"),
              processing_stage: this.thDetails.processing_stage,
              data_layer_id: this.thDetails.data_layer_id,
              grnl_lvl: this.thDetails.grnl_lvl,
              agg_fn: this.thDetails.agg_fn,
              th_val: this.thDetails.th_val * 100,
              fltr_con: this.thDetails.fltr_con == null || this.thDetails.fltr_con === '' ? '1=1' : this.thDetails.fltr_con,
              rule_desc: this.thDetails.rule_desc,
              rule_svrt: this.thDetails.rule_svrt !== null ? this.thDetails.rule_svrt.slice(0, 1) : null,
              rule_cat: this.thDetails.rule_cat,
              is_rule_actv: this.thDetails.is_rule_actv ? 'Y' : 'N',
              audit_insrt_id: this.authenticateService.getUserInfo().user_id,
              job_desc: ''
            })
            .subscribe(data => {
              if (data.Contents.length > 0) {
                if (data.Contents[0].status.toUpperCase() != 'FAIL') {
                  this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.threshold.popupMsg.saved));
                  this.hideDialog.emit(true);
                  this.thDetails = {
                    rule_id: null, processing_stage: 'Post-Ingestion', data_layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
                    fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null, data_layer_nm: null,
                    obj_nm: null, attr_nm: null, bk_attr_nm: null, trending_attr_id: null, dimension_attr_nm: null, trending_attr_nm: null, dimension_attr_id: null, visual_rule_desc: null
                  };
                  this.status = false;
                  this.isRuleEditMode = false;
                } else {
                  this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(data.Contents[0].message));
                }
              }
            });
        }
      }
      if (this.isRuleEditMode) {
        this.dataService.getData(environment.dataQuality.updateThresholdRules,
          {
            name: 'update_dq_th_rule',
            th_val: this.thDetails.th_val * 100,
            fltr_con: this.thDetails.fltr_con,
            rule_desc: this.thDetails.rule_desc,
            rule_svrt: this.thDetails.rule_svrt !== null ? this.thDetails.rule_svrt.slice(0, 1) : null,
            is_rule_actv: this.thDetails.is_rule_actv ? 'Y' : 'N',
            rule_id: this.thDetails.rule_id,
            audit_updt_id: this.authenticateService.getUserInfo().user_id,
          })
          .subscribe(data => {
            if (data.Contents.length > 0) {
              this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.threshold.popupMsg.updated));
              this.hideDialog.emit(true);
              this.thDetails = {
                rule_id: null, processing_stage: 'Post-Ingestion', data_layer_id: null, grnl_lvl: 'ATTRIBUTE', obj_id: null, attr_id: null, bk_attr_id: null, agg_fn: null, th_val: null,
                fltr_con: null, rule_desc: null, mstr_rule_id: null, rule_svrt: null, rule_cat: 'A', is_rule_actv: null, actv_disp: null, data_layer_nm: null,
                obj_nm: null, attr_nm: null, bk_attr_nm: null, trending_attr_id: null, dimension_attr_nm: null, trending_attr_nm: null, dimension_attr_id: null, visual_rule_desc: null
              };
              this.status = false;
              this.isRuleEditMode = false;
            }
          });
      }
    }

  }
}




