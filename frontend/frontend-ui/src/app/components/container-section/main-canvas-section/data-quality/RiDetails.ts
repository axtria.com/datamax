export class RiDetails {
    rule_id: number;
    data_layer_id: number;
    mstr_rule_id: string;
    obj_id: number;
    attr_id: number;
    bk_attr_id: any;
    grnl_lvl: string;
    fltr_con: string;
    ri_data_layer_id: number;
    ri_obj_id: number;
    ri_attr_id: number;
    ri_fltr_con: string;
    rule_desc: string;
    rule_svrt: string;
    is_rule_actv: boolean;
    data_layer_nm: string;
    ri_data_layer_nm: string;
    obj_nm: string;
    ri_obj_nm: string;
    attr_nm: string;
    bk_attr_nm: string;
    ri_attr_nm: string;
    mstr_rule_desc: string;
}