import { Component, OnInit, ViewChild } from '@angular/core';
import { DataQualityDetails } from '../DataQualityDetails';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { TabAnimationSettingsModel } from '@syncfusion/ej2-angular-navigations';
import { DataQualityRulesComponent } from '../main-data-quality-rules/data-quality-rules/data-quality-rules.component';
import { DataQualityObjectsComponent } from '../main-data-quality-objects/data-quality-objects/data-quality-objects.component';

@Component({
  selector: 'app-main-data-quality',
  templateUrl: './main-data-quality.component.html',
  styleUrls: ['./main-data-quality.component.css']
})
export class MainDataQualityComponent implements OnInit {
  dataQualityContent: any;
  headerTextMainNav: any;
  headerText: any;
  dataQualityDetails: DataQualityDetails = {
    obj_id: 0, attr_id: 0, rule_id: 0, obj_name: null, layer_id: 0
  };
  tabAnimationSettings: TabAnimationSettingsModel = { previous: { effect: "None" }, next: { effect: "None" } };
  @ViewChild('dataQualityRules', { static: false }) objDataQualityRules: DataQualityRulesComponent;
  @ViewChild('dataQualityObjects', { static: false }) objDataQualityObjects: DataQualityObjectsComponent;

  constructor(cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.headerTextMainNav = this.dataQualityContent.menu.headerText;
    this.headerText = this.dataQualityContent.subMenu.headerText;
  }

  ngOnInit() {
    
  }

 }
