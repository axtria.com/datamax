import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { DataValidationRuleDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataValidationRuleDetails';
import { DataValidationService } from '../data-validation-form/data-validation.service';
//import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { RestService } from 'src/app/shared/services/rest.service'; import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { DataQualityDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataQualityDetails';
//import { CommandModel, GridComponent, IRow, Column, ResizeService, ReorderService, ToolbarService, ColumnChooserService } from '@syncfusion/ej2-angular-grids';
//import { closest } from '@syncfusion/ej2-base';
//import { NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
//import { CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-data-validation-form',
  templateUrl: './data-validation-form.component.html',
  styleUrls: ['./data-validation-form.component.css']
})
export class DataValidationFormComponent implements OnInit {
  @ViewChild('addNewDataValidationRuleForm', { static: false }) addNewDataValidationRuleForm: any;
  @Output() latestRuleIdEmitter = new EventEmitter<number>();

  dataQualityDetails: DataQualityDetails;

  ObjNotSelected = true;
  isRuleEditMode = false;
  ruleAlreadyExist = false;
  ruleAlreadyExistMsg = '';
  dataPattern = '';
  customMsg = '';
  latest_rule_id = 0;
  isTextboxEmpty: boolean = false;
  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };

  objectList: any[];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };

  attributeList: any[];
  reportColList: any[];
  //grpConditionsList: any[];
  attributeFields: object = { text: 'etl_nm', value: 't_at_id' };


  rangeOptionFields: object = { text: 'text', value: 'value' };

  checkList: any[];
  checkFields: object = { text: 'visual_rule_desc', value: 'mstr_rule_id' };
  activeCheckName: string;
  multiAttrSelect: boolean;
  bulkSave: boolean;
  multiAttrRule: boolean;
  // selectedCkeck: string;
  activeCheckType: string;
  checkIndex: number;

  dateTypes = ['DATE', 'TIMESTAMP', 'TIMESTAMP_NTZ', 'TIMESTAMP_TZ'];

  checkTypeOptions: string[];
  granularityCheckOptions: string[];
  FilteredMultiAttributecheckList: string[];
  ObjectLevelcheckList = [];
  //activeGranularity: string;
  noParamCheckTypeOptions: any[];
  singleParamCheckTypeOptions: any[];
  doubleParamCheckTypeOptions: any[];
  tripleParamCheckTypeOptions: any[];
  activeOptions: string[];
  tooltipContent: any;

  selectedAttrDataType: string;

  severityOptions: string[];
  lengthOptions: string[];
  activeLengthOption: string;
  rangeOptions: any[];
  dataAvailabilityOptions: string[];
  activeDataAvailabilityType: string;
  applyDropdownList: any[];
  activeApplyDropdownList: string;

  dateFields: string[];
  activeDateFormat: string;

  status = false;

  dataValidationFormContent: any;
  commonContent: any;
  formInvalidAfterSaveClick = false;
  // dataValidationRules: DataValidationRuleDetails[];
  public filterPlaceholder: any = 'Search';
  dataValidationLayoutDetails: DataValidationRuleDetails = {
    rule_id: null, data_layer_id: null, data_layer_name: null, obj_id: null, obj_nm: null, attr_id_lst: null, attr_nm_lst: null,
    bk_attr_id_lst: null, bk_attr_nm_lst: null, mstr_rule_id: null, visual_rule_desc: null, arg_1: null, arg_2: null, arg_3: null, arg_4: null,
    rule_desc: null, is_rule_actv: null, rule_svrt: null, fltr_con: null, grnl_lvl: 'ATTRIBUTE', rule_cat: 'A'
  };

  editDataValidationLayoutDetails: DataValidationRuleDetails = {
    rule_id: null, data_layer_id: null, data_layer_name: null, obj_id: null, obj_nm: null, attr_id_lst: null, attr_nm_lst: null,
    bk_attr_id_lst: null, bk_attr_nm_lst: null, mstr_rule_id: null, visual_rule_desc: null, arg_1: null, arg_2: null, arg_3: null, arg_4: null,
    rule_desc: null, is_rule_actv: null, rule_svrt: null, fltr_con: null, grnl_lvl: 'ATTRIBUTE', rule_cat: 'A'
  };

  constructor(private dataService: RestService, private toastPopupService: ToastPopupService, contentsService: ContentService,
    private dataValidationService: DataValidationService, private authenticateService: AuthenticateService, private datePipe: DatePipe) {
    // Initialize all the HTML Text Contents & SET all the required Array Options from the Content Resource File
    this.dataValidationFormContent = contentsService.getContent(environment.modules.dataQuality).subMenu.dataValidation;
    this.commonContent = contentsService.getContent(environment.modules.dataQuality).commonContent;
    this.granularityCheckOptions = this.dataValidationFormContent.granularityCheckOptions;
    this.checkTypeOptions = this.dataValidationFormContent.checkTypeOptions;
    this.noParamCheckTypeOptions = this.dataValidationFormContent.noParamCheckTypeOptions;
    this.singleParamCheckTypeOptions = this.dataValidationFormContent.singleParamCheckTypeOptions;
    this.doubleParamCheckTypeOptions = this.dataValidationFormContent.doubleParamCheckTypeOptions;
    this.tripleParamCheckTypeOptions = this.dataValidationFormContent.tripleParamCheckTypeOptions;
    this.severityOptions = this.dataValidationFormContent.severityOptions;
    this.lengthOptions = this.dataValidationFormContent.lengthOptions;
    this.rangeOptions = this.dataValidationFormContent.rangeOptions;
    this.applyDropdownList = this.commonContent.applyDropdownList;
    this.activeApplyDropdownList = this.applyDropdownList[0];
    this.dataAvailabilityOptions = this.dataValidationFormContent.dataAvailabilityOptions;
    this.dateFields = this.dataValidationFormContent.dateFields;
    this.activeOptions = this.dataValidationFormContent.checkActiveOptions;
    this.tooltipContent = this.dataValidationFormContent.tooltipContent;

    this.ObjectLevelcheckList = this.dataValidationFormContent.ObjectLevelcheckList;
    //this.MultiAttributeLevelcheckList = this.dataValidationFormContent.MultiAttributeLevelcheckList;
  }

  ngOnInit() {
    // Populate Data Layer and Checks Dropdown
    this.populateDataLayer();
    this.populateChecks();
  }

  populateDataLayer() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_obj_meta_data_layers' })
      .subscribe(data => { this.businessLayerList = data && data.Contents ? data.Contents : []; });
  }

  dataLayerChange(e: any) {
    if (e.itemData !== null) {
      this.populateObject(e.itemData.layer_id);
    }
  }

  populateObject(dataLayerName: string) {
    this.dataService.getData(environment.dataQuality.getObjects,
      { name: 'get_data_layer_objects', src_data_layer_id: dataLayerName })
      .subscribe(data => {
        this.objectList = data && data.Contents ? data.Contents : [];

        // If the form is in EDIT mode then pre-set the Object Dropdown - It will fire objectChange() method
        if (this.isRuleEditMode) {
          this.dataValidationLayoutDetails.obj_id = this.editDataValidationLayoutDetails.obj_id;
        } else {
          this.dataValidationLayoutDetails.obj_id = this.dataQualityDetails.obj_id === 0 ? null : this.dataQualityDetails.obj_id;
        }
      });
  }

  objectChange(e: any) {
    this.attributeList = [];
    if (e.itemData !== null) {
      // this.populateAttribute(e.itemData.obj_id);
      this.dataValidationLayoutDetails.obj_id = e.itemData.obj_id;
      this.populateAttribute(this.dataValidationLayoutDetails.obj_id, this.dataValidationLayoutDetails.mstr_rule_id);
      this.ObjNotSelected = false;
    }
  }

  populateChecks() {
    this.dataService.getData(environment.dataQuality.getDQDataValidationCheckType,
      { name: 'get_data_validation_checks' })
      .subscribe(data => {
        this.checkList = data && data.Contents ? data.Contents : [];
        // If the form is in EDIT mode then pre-set the Check Type Dropdown - It will fire checkTypeChange() method
        if (this.isRuleEditMode) {
          this.status = this.editDataValidationLayoutDetails.is_rule_actv;
          this.dataValidationLayoutDetails.mstr_rule_id = this.editDataValidationLayoutDetails.mstr_rule_id;
          let ruleIndex = this.checkList.findIndex(item => item.mstr_rule_id === this.dataValidationLayoutDetails.mstr_rule_id);
          this.bulkSave = this.checkList[ruleIndex].multi_attr_select;
          this.multiAttrSelect = this.checkList[ruleIndex].multi_attr_select;
          this.multiAttrRule = this.checkList[ruleIndex].multi_attr_rule;

        }
      });
  }

  checkTypeChange(e: any) {
    this.dataValidationLayoutDetails.attr_id_lst = null;
    // this.ruleAlreadyExist = false;
    this.dataValidationLayoutDetails.bk_attr_id_lst = null;
    this.reportColList = [];
    this.selectedAttrDataType = '';
    this.dataValidationLayoutDetails.arg_1 = null;
    this.dataValidationLayoutDetails.arg_2 = null;
    this.dataValidationLayoutDetails.arg_3 = null;
    // Important for range option pre-select in case of adding new Length/Range Check
    // this.dataValidationLayoutDetails.arg_4 = null;
    this.dataValidationLayoutDetails.fltr_con = null;
    this.dataValidationLayoutDetails.rule_desc = null;

    if (e.itemData) {
      this.dataValidationLayoutDetails.mstr_rule_id = e.itemData.mstr_rule_id;
      this.dataValidationLayoutDetails.rule_desc = e.itemData.mstr_rule_desc;
      this.activeCheckName = e.itemData.visual_rule_desc.toUpperCase();
      this.bulkSave = e.itemData.multi_attr_select;
      this.multiAttrSelect = e.itemData.multi_attr_select;
      this.multiAttrRule = e.itemData.multi_attr_rule;
      this.activeDateFormat = 'yyyy-MM-dd';

      this.populateAttribute(this.dataValidationLayoutDetails.obj_id, this.dataValidationLayoutDetails.mstr_rule_id);


      // Set the Check Type & Check Index to find if "noParamCheck" or "singleParamCheck" or "doubleParamCheck" and set the form ccordingly
      this.activeCheckType = this.checkTypeOptions.length > 0 ? this.checkTypeOptions[0] : '';

      let checkIndex = this.noParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === this.activeCheckName);
      if (checkIndex === -1) {
        checkIndex = this.singleParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === this.activeCheckName);
        this.activeCheckType = this.checkTypeOptions[1];
      }
      if (checkIndex === -1) {
        checkIndex = this.doubleParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === this.activeCheckName);
        this.activeCheckType = this.checkTypeOptions[2];
      }
      if (checkIndex === -1) {
        checkIndex = this.tripleParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === this.activeCheckName);
        this.activeCheckType = this.checkTypeOptions[3];
      }

      this.checkIndex = checkIndex;

      // If the form is in EDIT mode then set all the modal values to prefill the form
      if (this.isRuleEditMode) {

        // this.dataValidationLayoutDetails = this.editDataValidationLayoutDetails;

        if (this.dateTypes.indexOf(this.selectedAttrDataType.toUpperCase()) > -1) {
          if (this.checkTypeOptions.length > 0) {
            if (this.activeCheckType === this.checkTypeOptions[0]) {
              this.activeDateFormat = this.editDataValidationLayoutDetails.arg_1;
            }
            if (this.activeCheckType === this.checkTypeOptions[2]) {
              this.activeDateFormat = this.editDataValidationLayoutDetails.arg_3;
              this.dataValidationLayoutDetails.arg_1 = this.dateTransform(this.dataValidationLayoutDetails.arg_1, this.activeDateFormat);
              this.dataValidationLayoutDetails.arg_2 = this.dateTransform(this.dataValidationLayoutDetails.arg_2, this.activeDateFormat);
            }
          }
        }
      }

      this.dataPattern = this.dataValidationService.getDataPattern(this.activeCheckName, this.selectedAttrDataType)[0];
      this.customMsg = this.dataValidationService.getDataPattern(this.activeCheckName, this.selectedAttrDataType)[1];
    }
  }







  // granularityChange(granularityCheckOption: string) {
  //   this.dataValidationLayoutDetails.granularity = this.activeGranularity;
  //   this.dataValidationLayoutDetails.mstr_rule_id = null;
  //   this.checkList = null;
  //   this.dataValidationLayoutDetails.bk_attr_id_lst = null;
  //   this.reportColList = [];
  //   this.activeCheckType = this.checkTypeOptions.length > 0 ? this.checkTypeOptions[0] : '';
  //   this.selectedAttrDataType = "";
  //   this.dataValidationLayoutDetails.arg_1 = null;
  //   this.dataValidationLayoutDetails.arg_2 = null;
  //   this.dataValidationLayoutDetails.arg_3 = null;
  //   this.dataValidationLayoutDetails.arg_4 = null;
  //   this.dataValidationLayoutDetails.fltr_con = null;
  //   this.dataValidationLayoutDetails.rule_desc = null;
  //   this.checkList = null;
  //   this.FilteredMultiAttributecheckList = null;

  //   this.activeGranularity = granularityCheckOption;
  // }

  populateAttribute(objectId: number, mstr_rule_id: string) {
    if (objectId !== null) {
      this.dataService.getData(environment.dataQuality.getDQAttributes,
        { name: 'get_dq_object_attribute_details', obj_id: objectId, mstr_rule_id: mstr_rule_id })
        .subscribe(data => {
          this.attributeList = data && data.Contents ? data.Contents : [];

          // If the form is in EDIT mode then pre-set the Attribute Dropdown - It will fire attributeChange() method
          if (this.isRuleEditMode) {
            this.dataValidationLayoutDetails.attr_id_lst = this.editDataValidationLayoutDetails.attr_id_lst;
          }
        });
    }
  }

  attributeChange(e: any) {
    this.dataValidationLayoutDetails.bk_attr_id_lst = null;
    this.reportColList = [];
    // this.activeCheckType = this.checkTypeOptions.length > 0 ? this.checkTypeOptions[0] : '';
    this.dataValidationLayoutDetails.arg_1 = null;
    this.dataValidationLayoutDetails.arg_2 = null;
    this.dataValidationLayoutDetails.arg_3 = null;

    // Important for range option pre-select in case of adding new Length/Range Check
    // this.dataValidationLayoutDetails.arg_4 = null;
    // this.FilteredMultiAttributecheckList = null;

    if (e.itemData !== null) {
      this.selectedAttrDataType = e.itemData.dt_typ;

      // if (!this.isRuleEditMode) {
      //   this.dataValidationLayoutDetails.fltr_con = null;
      //   this.dataValidationLayoutDetails.rule_desc = null;


      //     .subscribe(data => {
      //       this.ruleAlreadyExistMsg = data.Contents[0].message;
      //       if (data.Contents[0].status.toUpperCase() === "FALSE") {
      //         this.ruleAlreadyExist = true;
      //         this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.ruleAlreadyExistMsg));
      //       }
      //     });
      // }

      // if (this.isRuleEditMode) {
      //   this.dataValidationLayoutDetails = this.editDataValidationLayoutDetails;
      // }

      // Create Reporting Attribute List by excluding the Selected Attribute for applying Data Validation Rule
      this.populateRptAttribute(this.dataValidationLayoutDetails.obj_id, e.itemData.t_at_id, 0);

      // this.reportColList = this.attributeList.filter(function (item: any) {
      //   if (item.t_at_id !== e.itemData.t_at_id) {
      //     return item;
      //   }
      // });

      // Set Argument Input Validation based on DataType
      this.dataPattern = this.dataValidationService.getDataPattern(undefined, this.selectedAttrDataType)[0];
      this.customMsg = this.dataValidationService.getDataPattern(undefined, this.selectedAttrDataType)[1];

    } else {
      this.reportColList = [];
    }

  }

  multiAttrChange(e: any) {
    this.dataValidationLayoutDetails.bk_attr_id_lst = null;
    this.reportColList = [];
    this.ruleAlreadyExist = false;
    // this.activeCheckType = this.checkTypeOptions.length > 0 ? this.checkTypeOptions[0] : '';
    this.selectedAttrDataType = '';
    this.dataValidationLayoutDetails.arg_1 = null;
    this.dataValidationLayoutDetails.arg_2 = null;
    this.dataValidationLayoutDetails.arg_3 = null;
    // this.dataValidationLayoutDetails.arg_4 = null;
    // this.FilteredMultiAttributecheckList = null;
    if (e.value !== null) {
      this.populateRptAttribute(this.dataValidationLayoutDetails.obj_id, e.value, 1);
      this.dataPattern = this.dataValidationService.getDataPattern(undefined, this.selectedAttrDataType)[0];
      this.customMsg = this.dataValidationService.getDataPattern(undefined, this.selectedAttrDataType)[1];
      //Check for Duplicate record
      if (!this.isRuleEditMode) {
        this.dataService.getData(environment.dataQuality.checkDuplicityDataValidationRule,
          {
            name: 'data_validation_dup_check',
            obj_id: this.dataValidationLayoutDetails.obj_id,
            attr_id: e.value.toString().replace(/\,/g, '|'),
            mstr_rule_id: this.dataValidationLayoutDetails.mstr_rule_id,
            arg_1: this.dataValidationLayoutDetails.arg_1
          })
          .subscribe(data => {
            if (data.Contents[0].status.toUpperCase() === 'FALSE') {
              this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(data.Contents[0].message));
              this.ruleAlreadyExist = true;
              this.ruleAlreadyExistMsg = data.Contents[0].message;
            }
          });
      }
    } else {
      this.reportColList = [];
    }
    // If the form is in EDIT mode then pre-set the Check Type Dropdown - It will fire checkTypeChange() method
    // if (this.isRuleEditMode) {
    //   this.status = this.editDataValidationLayoutDetails.is_rule_actv;
    //   // this.dataValidationLayoutDetails.mstr_rule_id = this.editDataValidationLayoutDetails.mstr_rule_id;
    // }
    // if (e.value !== null) {
    //   // Create Grouping Conditions List by excluding the Selected Attributes for Conditioanl Uniqueness Rule
    //   this.grpConditionsList = [];
    //   this.grpConditionsList = this.attributeList.filter((item: any) => e.value.indexOf(item.t_at_id) < 0);
    // }
  }

  dupCheck(e: any) {
    if (!this.isRuleEditMode && this.dataValidationLayoutDetails.attr_id_lst !== null) {
      this.dataService.getData(environment.dataQuality.checkDuplicityDataValidationRule,
        {
          name: 'data_validation_dup_check',
          obj_id: this.dataValidationLayoutDetails.obj_id,
          attr_id: this.dataValidationLayoutDetails.attr_id_lst.toString().replace(/\,/g, '|'),
          mstr_rule_id: this.dataValidationLayoutDetails.mstr_rule_id,
          arg_1: e.value.toString().replace(/\,/g, '|')
        })
        .subscribe(data => {
          if (data.Contents[0].status.toUpperCase() === 'FALSE') {
            this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(data.Contents[0].message));
            this.ruleAlreadyExist = true;
            this.ruleAlreadyExistMsg = data.Contents[0].message;
          }
        });
    } else return;
  }

  populateRptAttribute(objectId: number, t_attr_id: any, n: number) {
    this.dataService.getData(environment.dataQuality.getAttributes,
      { name: 'get_object_attribute_details', obj_id: objectId })
      .subscribe(data => {
        const reportColList_temp = data && data.Contents ? data.Contents : [];

        if (n === 0) {
          this.selectedAttrDataType = reportColList_temp[reportColList_temp.findIndex(item => (Number)(item.t_at_id) === (Number)(t_attr_id))].dt_typ;

          this.reportColList = reportColList_temp.filter(function (item: any) {
            if ((Number)(item.t_at_id) !== (Number)(t_attr_id)) {
              return item;
            }
          });
        }
        if (n === 1) {

          if (t_attr_id !== null) {
            this.reportColList = reportColList_temp.filter((item: any) => t_attr_id.indexOf(item.t_at_id) < 0);
          }
        }
      });
  }

  dateTransform(date: Date, format: string) {
    if (this.dateTypes.indexOf(this.selectedAttrDataType.toUpperCase()) > -1) {
      return this.datePipe.transform(date, format);
    }
  }

  setDatePickerFormat(e: any) {
    if (e.itemData != null) {
      this.activeDateFormat = e.itemData.value;
    }
  }

  updateTextboxHighlight() {
    this.isTextboxEmpty = false;
  }

  dataAvailabilityOptionChange(dataAvailabilityOption: string) {
    if (dataAvailabilityOption === this.dataAvailabilityOptions[0]) {
      this.activeCheckType = this.checkTypeOptions[1];
      this.checkIndex = 5;
    } else {
      this.activeCheckType = this.checkTypeOptions[2];
      this.checkIndex = 2;
    }
  }

  resetVariblestoDefaults() {
    //
    this.dataValidationLayoutDetails = {
      rule_id: null, data_layer_id: null, data_layer_name: null, obj_id: null, obj_nm: null, attr_id_lst: null, attr_nm_lst: null,
      bk_attr_id_lst: null, bk_attr_nm_lst: null, mstr_rule_id: null, visual_rule_desc: null, arg_1: null, arg_2: null, arg_3: null, arg_4: null,
      rule_desc: null, is_rule_actv: null, rule_svrt: null, fltr_con: null, grnl_lvl: 'ATTRIBUTE', rule_cat: 'A'
    };

    this.editDataValidationLayoutDetails = {
      rule_id: null, data_layer_id: null, data_layer_name: null, obj_id: null, obj_nm: null, attr_id_lst: null, attr_nm_lst: null,
      bk_attr_id_lst: null, bk_attr_nm_lst: null, mstr_rule_id: null, visual_rule_desc: null, arg_1: null, arg_2: null, arg_3: null, arg_4: null,
      rule_desc: null, is_rule_actv: null, rule_svrt: null, fltr_con: null, grnl_lvl: 'ATTRIBUTE', rule_cat: 'A'
    };

    this.objectList = [];
    this.attributeList = [];
    this.reportColList = [];
    //this.grpConditionsList = [];

    // Set activeCheckName to NULL to show a default form
    this.activeCheckName = '';
    this.activeCheckType = this.checkTypeOptions.length > 0 ? this.checkTypeOptions[0] : '';

    // Reset the Form
    this.addNewDataValidationRuleForm.reset();
    // To highlight the mandory or default input error message
    this.formInvalidAfterSaveClick = false;

    this.bulkSave = false;
    this.isRuleEditMode = false;
    this.ObjNotSelected = true;
    this.selectedAttrDataType = '';
  }

  addNewDataValidationRule(dataQualityDetails: DataQualityDetails) {
    this.resetVariblestoDefaults();

    // This is to set the Main Data Quality Model to the local variable
    this.dataQualityDetails = dataQualityDetails;


    // This is to reset the default controls on the Form
    this.dataValidationLayoutDetails.data_layer_id = this.dataQualityDetails.layer_id !== 0 ? this.dataQualityDetails.layer_id : null;

    //Set to defaults
    // this.activeGranularity = this.granularityCheckOptions.length > 0 ? this.granularityCheckOptions[0] : '';
    // this.dataValidationLayoutDetails.granularity = this.activeGranularity;

    this.selectedAttrDataType = '';

    this.dataValidationLayoutDetails.rule_svrt = this.severityOptions.length > 0 ? this.severityOptions[0] : '';
    this.activeLengthOption = this.lengthOptions.length > 0 ? this.lengthOptions[0] : '';
    this.activeDataAvailabilityType = this.dataAvailabilityOptions.length > 0 ? this.dataAvailabilityOptions[0] : '';
    this.dataValidationLayoutDetails.arg_4 = this.rangeOptions.length > 0 ? this.rangeOptions[0].value : '';
    this.dataValidationLayoutDetails.is_rule_actv = true;

    this.editDataValidationLayoutDetails = null;
  }

  saveNewDataValidationRule() {
    if (this.addNewDataValidationRuleForm.invalid) {
      this.toastPopupService._exception.
        emit(this.toastPopupService.getToastWarningMessage(this.dataValidationFormContent.popupMsg.invalid));
      this.formInvalidAfterSaveClick = this.addNewDataValidationRuleForm.invalid;
    } else {
      // Set bk_attr and arg_1 for single and CONDITIONAL UNIQUENESS CHECK
      let temp_bk_attr_id = null;
      let temp_arg_1 = null;
      let temp_arg_4 = null;

      // Replace , with | in for temp_bk_attr_id
      temp_bk_attr_id = this.dataValidationLayoutDetails.bk_attr_id_lst && this.dataValidationLayoutDetails.bk_attr_id_lst.toString() !== '' ?
        this.dataValidationLayoutDetails.bk_attr_id_lst.toString().replace(/,/g, '|') : null;

      //     .replace(/,/g, '|')) : this.dataValidationLayoutDetails.attr_id_lst
      // this.dataValidationLayoutDetails.bk_attr_id_lst && this.dataValidationLayoutDetails.bk_attr_id_lst.toString() !== '' ?
      //   this.dataValidationLayoutDetails.attr_id_lst.toString().concat('|', this.dataValidationLayoutDetails.bk_attr_id_lst.toString()
      //     .replace(/,/g, '|')) : this.dataValidationLayoutDetails.attr_id_lst;

      // Replace , with | in for arg_1
      if (this.activeCheckName === 'DOMAIN CHECK' || this.activeCheckName === 'CONDITIONAL UNIQUENESS CHECK') {
        temp_arg_1 = this.dataValidationLayoutDetails.arg_1 !== null ?
          this.dataValidationLayoutDetails.arg_1.toString().replace(/,/g, '|') : null;
      } else {
        temp_arg_1 = this.dataValidationLayoutDetails.arg_1
      }

      // Validate RANGE & LENGTH CHECK Params
      if (this.activeCheckName === 'RANGE CHECK' || this.activeCheckName === 'LENGTH CHECK') {
        // if (!(this.dataValidationLayoutDetails.arg_1 || this.dataValidationLayoutDetails.arg_2)) {
        //   this.isTextboxEmpty = true;
        //   this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.doubleParamCheckTypeOptions[0].warningMsg));
        //   return;
        // }

        // In case of Length Options Switch save/updates the required null values already bound to the arg_1 and arg_2 models 
        if (this.activeLengthOption === this.lengthOptions[0]) {
          this.dataValidationLayoutDetails.arg_2 = null;
        } else if(this.activeLengthOption === this.lengthOptions[1]){
          temp_arg_1 = null;
        }
        // else {
          temp_arg_4 = this.dataValidationLayoutDetails.arg_4 !== null ? this.dataValidationLayoutDetails.arg_4.slice(0, 1) : null;
          if (this.dateTypes.indexOf(this.selectedAttrDataType.toUpperCase()) > -1) {
            if (this.checkTypeOptions.length > 0) {
              if (this.activeCheckType === this.checkTypeOptions[0]) {
                this.activeDateFormat = this.dataValidationLayoutDetails.arg_1;
              }
              if (this.activeCheckType === this.checkTypeOptions[2]) {
                this.activeDateFormat = this.dataValidationLayoutDetails.arg_3;
                temp_arg_1 = this.dateTransform(this.dataValidationLayoutDetails.arg_1, this.activeDateFormat);
                this.dataValidationLayoutDetails.arg_2 = this.dateTransform(this.dataValidationLayoutDetails.arg_2, this.activeDateFormat);
              }
            }
          }
        // }
      }
      // if (this.activeCheckName === 'LENGTH CHECK' && !(temp_arg_1 || this.dataValidationLayoutDetails.arg_2 || this.dataValidationLayoutDetails.arg_3)) {
      //   this.isTextboxEmpty = true;
      //   this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.tripleParamCheckTypeOptions[0].warningMsg));
      //   return;
      // }

      if (!this.isRuleEditMode) {
        if (this.ruleAlreadyExist) {
          this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.ruleAlreadyExistMsg));
          return;
        }

        this.dataService.getData(environment.dataQuality.addDataValidationRuleV2,
          {
            name: 'add_dq_data_validation_rule',
            obj_id: this.dataValidationLayoutDetails.obj_id,
            attr_id_lst: this.dataValidationLayoutDetails.attr_id_lst.toString().replace(/,/g, '|'),
            mstr_rule_id: this.dataValidationLayoutDetails.mstr_rule_id,
            bk_attr_id: temp_bk_attr_id,
            layer_id: this.dataValidationLayoutDetails.data_layer_id,
            grnl_lvl: this.dataValidationLayoutDetails.grnl_lvl,
            arg_1: temp_arg_1,
            arg_2: this.dataValidationLayoutDetails.arg_2,
            arg_3: this.dataValidationLayoutDetails.arg_3,
            arg_4: temp_arg_4,
            rule_desc: this.dataValidationLayoutDetails.rule_desc,
            job_desc: '',
            is_rule_actv: this.dataValidationLayoutDetails.is_rule_actv,
            rule_svrt: this.dataValidationLayoutDetails.rule_svrt !== null ? this.dataValidationLayoutDetails.rule_svrt.slice(0, 1) : null,
            fltr_con: '1=1',
            audit_insrt_id: this.authenticateService.getUserInfo().user_id,
            multi_attr_select: this.multiAttrSelect,
            multi_attr_rule: this.multiAttrRule

          })
          .subscribe(data => {
            if (data.Contents.length) {
              if (data.Contents[0].status.toUpperCase() !== 'FAIL') {
                this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.dataValidationFormContent.popupMsg.saved));
                this.latest_rule_id = data.Contents[0].latest_rule_id;
                this.latestRuleIdEmitter.emit(this.latest_rule_id);
                // this.populateDataValidationRules();

                // Reset the DataValidation Form Modal
                this.dataValidationLayoutDetails = {
                  rule_id: null, data_layer_id: null, data_layer_name: null, obj_id: null, obj_nm: null, attr_id_lst: null, attr_nm_lst: null,
                  bk_attr_id_lst: null, bk_attr_nm_lst: null, mstr_rule_id: null, visual_rule_desc: null, arg_1: null, arg_2: null, arg_3: null, arg_4: null,
                  rule_desc: null, is_rule_actv: null, rule_svrt: null, fltr_con: null, grnl_lvl: 'ATTRIBUTE', rule_cat: 'A'
                };

                this.activeCheckType = this.checkTypeOptions.length > 0 ? this.checkTypeOptions[0] : '';
                //this.addNewDataValidationRuleDialog.hide();
                this.isRuleEditMode = false;
                this.activeLengthOption = '';
              } else {
                this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(data.Contents[0].message));
              }
            }
          });
      }

      if (this.isRuleEditMode) {
        this.dataService.getData(environment.dataQuality.updateDataValidationRule,
          {
            name: 'update_dq_data_validation_rule',
            rule_id: this.dataValidationLayoutDetails.rule_id,
            bk_attr_id: temp_bk_attr_id,
            arg_1: temp_arg_1,
            arg_2: this.dataValidationLayoutDetails.arg_2,
            arg_3: this.dataValidationLayoutDetails.arg_3,
            arg_4: temp_arg_4,
            rule_desc: this.dataValidationLayoutDetails.rule_desc,
            is_rule_actv: this.dataValidationLayoutDetails.is_rule_actv,
            rule_svrt: this.dataValidationLayoutDetails.rule_svrt !== null ? this.dataValidationLayoutDetails.rule_svrt.slice(0, 1) : null,
            fltr_con: '1=1',
            audit_updt_id: this.authenticateService.getUserInfo().user_id,
            multi_attr_select: this.multiAttrSelect,
            multi_attr_rule: this.multiAttrRule
          })
          .subscribe(data => {
            this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.dataValidationFormContent.popupMsg.updated));
            this.latest_rule_id = data.Contents[0].latest_rule_id;
            this.latestRuleIdEmitter.emit(this.latest_rule_id);
            //this.populateDataValidationRules();

            // Reset the DataValidation Form Modal
            this.dataValidationLayoutDetails = {
              rule_id: null, data_layer_id: null, data_layer_name: null, obj_id: null, obj_nm: null, attr_id_lst: null, attr_nm_lst: null,
              bk_attr_id_lst: null, bk_attr_nm_lst: null, mstr_rule_id: null, visual_rule_desc: null, arg_1: null, arg_2: null, arg_3: null, arg_4: null,
              rule_desc: null, is_rule_actv: null, rule_svrt: null, fltr_con: null, grnl_lvl: 'ATTRIBUTE', rule_cat: 'A'
            };
            this.activeCheckType = this.checkTypeOptions.length > 0 ? this.checkTypeOptions[0] : '';
            //this.addNewDataValidationRuleDialog.hide();
            this.isRuleEditMode = false;
            this.activeLengthOption = '';
          });
      }
    }
  }

  editDataValidationRule(record: DataValidationRuleDetails): void {
    this.resetVariblestoDefaults();
    this.isRuleEditMode = true;


    const additionalRptColns = record.bk_attr_id_lst !== null ? record.bk_attr_id_lst.toString().replace(record.attr_id_lst.toString(), '')
      .replace(/\|/g, ' ').trim().split(' ').map(Number) : [];
    this.editDataValidationLayoutDetails = {
      data_layer_id: record.data_layer_id, data_layer_name: record.data_layer_name, obj_id: record.obj_id, obj_nm: record.obj_nm, attr_nm_lst: record.attr_nm_lst, attr_id_lst: record.attr_id_lst,
      mstr_rule_id: record.mstr_rule_id, visual_rule_desc: record.visual_rule_desc,
      bk_attr_id_lst: additionalRptColns.length === 1 && additionalRptColns[0] === 0 ? null : additionalRptColns,
      bk_attr_nm_lst: record.bk_attr_nm_lst,
      rule_id: record.rule_id, arg_4: record.arg_4, arg_3: record.arg_3, arg_2: record.arg_2, arg_1: record.arg_1,
      fltr_con: record.fltr_con, rule_desc: record.rule_desc, grnl_lvl: 'ATTRIBUTE', rule_cat: 'A',
      rule_svrt: record.rule_svrt, is_rule_actv: record.is_rule_actv
    };

    ///////////////////////////
    //Set the Check Type & Check Index to find if "noParamCheck" or "singleParamCheck" or "doubleParamCheck" and set the form ccordingly

    const editCheckName = this.editDataValidationLayoutDetails.visual_rule_desc.toUpperCase();
    this.populateChecks();
    let editCheckTypeOption = null;

    let editCheckIndex = this.noParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === editCheckName);
    if (editCheckIndex !== -1) {
      editCheckIndex = this.noParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === editCheckName);
      editCheckTypeOption = this.checkTypeOptions[0];
    } else {
      editCheckIndex = this.singleParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === editCheckName);
      if (editCheckIndex !== -1) {
        editCheckIndex = this.singleParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === editCheckName);
        editCheckTypeOption = this.checkTypeOptions[1];
      } else {
        editCheckIndex = this.doubleParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === editCheckName);
        if (editCheckIndex !== -1) {
          editCheckIndex = this.doubleParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === editCheckName);
          editCheckTypeOption = this.checkTypeOptions[2];
        } else {
          editCheckIndex = this.tripleParamCheckTypeOptions.findIndex(item => item.checkName.toUpperCase() === editCheckName);
          editCheckTypeOption = this.checkTypeOptions[3];
        }
      }
    }

    this.activeCheckName = editCheckName;
    this.activeCheckType = editCheckTypeOption;
    this.checkIndex = editCheckIndex;

    // set the 'DATA AVAILABILITY CHECK' to the doubleParamCheckType when arg_2 !== null
    if (this.activeCheckName === this.singleParamCheckTypeOptions[5].checkName && this.editDataValidationLayoutDetails.arg_2 !== null) {
      this.activeCheckType = this.checkTypeOptions[2];
      this.checkIndex = 2;
    }
    // set the length options in case of 'LENGTH CHECK'
    if(this.activeCheckName===this.doubleParamCheckTypeOptions[0].checkName || this.activeCheckName===this.doubleParamCheckTypeOptions[1].checkName){
      if(this.editDataValidationLayoutDetails.arg_1 !== null && this.editDataValidationLayoutDetails.arg_2 !== null){
        this.activeLengthOption = this.lengthOptions[2];
      } else {
        this.activeLengthOption = this.editDataValidationLayoutDetails.arg_1 !== null ? this.lengthOptions[0] : this.lengthOptions[1];
      }
    }

    // let multi_attr_select: boolean = this.checkList[this.checkList.findIndex((item: any) => item.mstr_rule_desc.toUpperCase() === editCheckName)].multi_attr_select;
    // let multi_attr_rule: boolean = this.checkList[this.checkList.findIndex((item: any) => item.mstr_rule_desc.toUpperCase() === editCheckName)].multi_attr_rule;

    // Create Reporting Attribute List by excluding the Selected Attribute for applying Data Validation Rule
    // if (multi_attr_select && multi_attr_rule) {
    if (this.activeCheckName !== this.noParamCheckTypeOptions[4].checkName) {
      if (this.activeCheckName === this.singleParamCheckTypeOptions[4].checkName) {
        this.populateRptAttribute(this.editDataValidationLayoutDetails.obj_id, this.editDataValidationLayoutDetails.attr_id_lst, 1);
      } else {
        this.populateRptAttribute(this.editDataValidationLayoutDetails.obj_id, this.editDataValidationLayoutDetails.attr_id_lst, 0);
      }
    }

    //}

    //NOT TO DELETE
    //this.editDataValidationLayoutDetails.attr_id_lst = (Number)(record.attr_id_lst);
    this.editDataValidationLayoutDetails.attr_id_lst = record.attr_id_lst !== null ?
      record.attr_id_lst.toString().replace(/\|/g, ' ').trim().split(' ').map(Number) : [];


    // For Domain Check ???
    // if (this.editDataValidationLayoutDetails.visual_rule_desc === this.singleParamCheckTypeOptions[3].checkName) {
    //   this.editDataValidationLayoutDetails.arg_1 = (Number)(record.arg_1);
    //   this.editDataValidationLayoutDetails.arg_1 = record.arg_1;
    // }



    // CONDITIONAL UNIQUENESS CHECK - Following code is to convert the PIPE seperated Attribute IDs to Number Array of Attribute IDs
    if (this.editDataValidationLayoutDetails.visual_rule_desc === this.singleParamCheckTypeOptions[4].checkName) {
      this.editDataValidationLayoutDetails.arg_1 = (Number)(record.arg_1);
      this.editDataValidationLayoutDetails.arg_1 = record.arg_1 !== null ?
        record.arg_1.toString().replace(/\|/g, ',').trim().split(',').map(Number) : [];
    }

    // if (this.activeGranularity === this.granularityCheckOptions[0]) {
    //   if (this.editDataValidationLayoutDetails.visual_rule_desc === this.singleParamCheckTypeOptions[3].checkName) {
    //     this.editDataValidationLayoutDetails.arg_1 = (Number)(record.arg_1);
    //     this.editDataValidationLayoutDetails.arg_1 = record.arg_1;
    //     //  !== null ?
    //     //   record.arg_1.toString().replace(/\|/g, ',') : null
    //   }

    // }

    // if (this.activeGranularity === this.granularityCheckOptions[1]) {

    //   this.editDataValidationLayoutDetails.attr_id_lst = record.attr_id_lst !== null ?
    //     record.attr_id_lst.toString().replace(/\|/g, ' ').trim().split(' ').map(Number) : [];

    //   if (this.editDataValidationLayoutDetails.visual_rule_desc === this.singleParamCheckTypeOptions[4].checkName) {
    //     this.editDataValidationLayoutDetails.arg_1 = (Number)(record.arg_1);
    //     this.editDataValidationLayoutDetails.arg_1 = record.arg_1 !== null ?
    //       record.arg_1.toString().replace(/\|/g, ',').trim().split(',').map(Number) : [];
    //   }
    // }

    //this.editDataValidationLayoutDetails.granularity = this.activeGranularity;


    ////////////////////////////
    //If the form is in EDIT mode then pre-set the DataLayer Dropdown - It will fire dataLayerChange() method
    this.dataValidationLayoutDetails.data_layer_id = this.editDataValidationLayoutDetails.data_layer_id;
    this.dataValidationLayoutDetails = this.editDataValidationLayoutDetails;
  }
}