import { Component, OnInit, ViewChild } from '@angular/core';
import { ObjectRulesListComponent } from '../../../object-rules-list/object-rules-list/object-rules-list.component';
import { TabAnimationSettingsModel } from '@syncfusion/ej2-navigations';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-object-rules-information',
  templateUrl: './object-rules-information.component.html',
  styleUrls: ['./object-rules-information.component.css']
})
export class ObjectRulesInformationComponent implements OnInit {
  @ViewChild('objectRulesList', { static: false }) objObjectRulesListComponent: ObjectRulesListComponent;

  tabAnimationSettings: TabAnimationSettingsModel = { previous: { effect: "None" }, next: { effect: "None" } };
  objId: number;
  dataQualityContent: any;
  headerText: any;
  objName: any;
  activeTab: any;
  constructor(private route: ActivatedRoute, cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.headerText = this.dataQualityContent.objSubMenu.headerText;
   }

  ngOnInit() {
    this.objId = parseInt(this.route.snapshot.paramMap.get('id'));
    this.activeTab = this.headerText[0].text;
  }

  objectName(obj_nm){
    this.objName = obj_nm.toUpperCase();
  }

  select(e: any) {
    if (e.selectedItem.textContent.startsWith(this.headerText[0].text)) {
      this.activeTab = this.headerText[0].text;
    }
    if (e.selectedItem.textContent.startsWith(this.headerText[1].text)) {
      this.activeTab = this.headerText[1].text;
    }
  }

}
