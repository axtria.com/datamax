import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
//import { DialogComponent, ButtonPropsModel, PositionDataModel } from '@syncfusion/ej2-angular-popups';
//import { CommandModel, GridComponent, IRow, Column, ResizeService, ReorderService, ToolbarService, ColumnChooserService } from '@syncfusion/ej2-angular-grids';
import { NgForm } from '@angular/forms';
import { DataQualityDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/DataQualityDetails';
import { KPIDetails } from 'src/app/components/container-section/main-canvas-section/data-quality/KPIDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
//import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
//import { closest } from '@syncfusion/ej2-base';
import { ContentService } from 'src/app/shared/services/content.service';
import { CheckBoxSelectionService } from '@syncfusion/ej2-angular-dropdowns';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-kpi-validation-form',
  templateUrl: './kpi-validation-form.component.html',
  styleUrls: ['./kpi-validation-form.component.css'],
  providers: [CheckBoxSelectionService]
})
export class KpiValidationFormComponent implements OnInit {

  @ViewChild('addNewKPIRuleForm', { static: false }) addNewKPIRuleForm: NgForm;
  @Input() dataQualityDetails: DataQualityDetails;
  @Output() latestRuleIdEmitter = new EventEmitter<number>();

  KPIRules: KPIDetails[];
  checkList: any[];
  editKPIDetails: KPIDetails;
  status: boolean;
  isRuleEditMode: boolean = false;
  // ruleAlreadyExist = false;
  // ruleAlreadyExistMsg: string;
  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };
  objectList: [];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };
  kpi: any;
  severityOptions: string[];

  kpiDetails: KPIDetails = {
    rule_id: null, mstr_rule_id: null, data_layer_id: null, data_layer_nm: null, obj_id: null, obj_nm: null, cstm_qry: null, grnl_lvl: null, rule_desc: null, rule_svrt: null, is_rule_actv: null
  };

  formInvalidAfterSaveClick: boolean = false;
  activeSeverityOption: string;
  latest_rule_id = 0;
  commonContent: any;
  applyDropdownList: any[];
  activeApplyDropdownList: string;
  public filterPlaceholder: any = 'Search';
  // disable : boolean = true;

  constructor(private dataService: RestService, cs: ContentService, private toastPopupService: ToastPopupService, private authenticateService: AuthenticateService) {
    this.kpi = cs.getContent(environment.modules.dataQuality).subMenu.kpi;
    this.severityOptions = this.kpi.severityOptions;
    this.commonContent = cs.getContent(environment.modules.dataQuality).commonContent;
    this.applyDropdownList = this.commonContent.applyDropdownList;
    this.activeApplyDropdownList = this.applyDropdownList[0];
  }

  ngOnInit() {
    // Populate all the Data Threshold Rules
    this.populateDataLayer();
  }

  // populateKPIRules() {
  //   this.dataService.getData(environment.dataQuality.getKPIRules,
  //     { name: 'get_kpi_rules', src_data_layer_id: this.dataQualityDetails.layer_id, obj_id: this.dataQualityDetails.obj_id, rule_id: 0 })
  //     .subscribe(data => { this.KPIRules = data && data.Contents ? data.Contents : []; });
  // }

  populateDataLayer() {
    this.dataService.getData(environment.dataQuality.getBusinessLayer,
      { name: 'get_obj_meta_data_layers' })
      .subscribe(data => { this.businessLayerList = data && data.Contents ? data.Contents : []; });
  }
  populateChecks(){
    this.dataService.getData(environment.dataQuality.getCheckType,
      { name: 'get_dq_rule_checks', rule_type: 'KPI' })
      .subscribe(data => {
        this.checkList = data && data.Contents ? data.Contents : [];
        console.log(this.checkList);
        if(this.checkList){
          this.kpiDetails.rule_desc = this.checkList[0].mstr_rule_desc;
          this.kpiDetails.mstr_rule_id = this.checkList[0].mstr_rule_id;
          console.log(this.kpiDetails.rule_desc, this.kpiDetails.mstr_rule_id);
        }
      });
  }

  dataLayerChange(e: any) {
    this.kpiDetails.obj_id = null;
    // this.ruleAlreadyExist = false;
    this.objectList = [];
    if (e.itemData !== null) {
      this.populateObject(e.itemData.layer_id);
    }
  }

  populateObject(dataLayer: string) {
    this.dataService.getData(environment.dataQuality.getObjects,
      { name: 'get_data_layer_objects', src_data_layer_id: dataLayer })
      .subscribe(data => {
        this.objectList = data && data.Contents ? data.Contents : [];
        // If the form is in EDIT mode then pre-set the Object Dropdown - It will fire objectChange() method
        this.kpiDetails.obj_id = this.isRuleEditMode ? this.editKPIDetails.obj_id : null;
      });
  }

  // objectChange(e: any) {
  //   if (e.itemData !== null){
  //     this.ruleAlreadyExist = false;
  //     this.kpiDetails.obj_id = e.value;
  //   }
  //   if (!this.isRuleEditMode && e.value !== null) {
  //     this.dataService.getData(environment.dataQuality.checkDuplicityKPI,
  //       {
  //         name: 'kpi_dup_check',
  //         obj_id: this.kpiDetails.obj_id,
  //         mstr_rule_id: this.kpiDetails.mstr_rule_id
  //       })
  //       .subscribe(data => {
  //         if (data.Contents[0].status.toUpperCase() === 'FALSE') {
  //           this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(data.Contents[0].message));
  //           this.ruleAlreadyExist = true;
  //           this.ruleAlreadyExistMsg = data.Contents[0].message;
  //         }
  //       });
  //   }
  // }

  addNewKPIRule(dataQualityDetails: DataQualityDetails) {
    //Reset the previously filled form
    this.resetVariablesToDefaults();

    // This is to set the Main Data Quality Model to the local variable
    this.dataQualityDetails = dataQualityDetails;

    // This is to reset the default controls on the Form
    this.kpiDetails.data_layer_id = this.dataQualityDetails.layer_id === 0 ? null : this.dataQualityDetails.layer_id;

    //Set default severity and status
    this.kpiDetails.is_rule_actv = true;
    this.kpiDetails.rule_svrt = this.severityOptions.length > 0 ? this.severityOptions[0] : '';
    this.populateChecks();
    this.editKPIDetails = null;
  }

  resetVariablesToDefaults() {
    this.kpiDetails = {
      rule_id: null, mstr_rule_id: null, data_layer_id: null, data_layer_nm: null, obj_id: null, obj_nm: null, cstm_qry: null, grnl_lvl: null, rule_desc: null, rule_svrt: null, is_rule_actv: null
    };
    this.editKPIDetails = {
      rule_id: null, mstr_rule_id: null, data_layer_id: null, data_layer_nm: null, obj_id: null, obj_nm: null, cstm_qry: null, grnl_lvl: null, rule_desc: null, rule_svrt: null, is_rule_actv: null
    };
    this.objectList = [];

    //Reset the form
    this.addNewKPIRuleForm.reset();
    // To highlight the mandory or default input error message
    this.formInvalidAfterSaveClick = false;
    this.isRuleEditMode = false;
    this.activeSeverityOption = this.severityOptions.length > 0 ? this.severityOptions[0] : '';
  }

  saveNewKPIRule() {
    if (this.addNewKPIRuleForm.invalid) {
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.kpi.popupMsg.invalid));
      this.formInvalidAfterSaveClick = this.addNewKPIRuleForm.invalid;
    }
    else {
      if (!this.isRuleEditMode) {
        // if (this.ruleAlreadyExist) {
        //   this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.ruleAlreadyExistMsg));
        //   return;
        // }
        this.dataService.getData(environment.dataQuality.addKPIRule,
          {
            name: 'add_dq_kpi_rule',
            obj_id: this.kpiDetails.obj_id,
            mstr_rule_id: this.kpiDetails.mstr_rule_id,
            data_layer_id: this.kpiDetails.data_layer_id,
            grnl_lvl: this.kpiDetails.grnl_lvl,
            rule_desc: this.kpiDetails.rule_desc,
            cstm_qry: this.kpiDetails.cstm_qry,
            rule_svrt: this.kpiDetails.rule_svrt !== null ? this.kpiDetails.rule_svrt.slice(0, 1) : null,
            is_rule_actv: this.kpiDetails.is_rule_actv,
            audit_insrt_id: this.authenticateService.getUserInfo().user_id,
            job_desc: ''
          })
          .subscribe(data => {
            if (data.Contents.length > 0) {
              if (data.Contents[0].status.toUpperCase() != 'FAIL') {
                this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.kpi.popupMsg.saved));
                this.latest_rule_id = data.Contents[0].latest_rule_id;
                this.latestRuleIdEmitter.emit(this.latest_rule_id);

                this.kpiDetails = {
                  rule_id: null, mstr_rule_id: null, data_layer_id: null, data_layer_nm: null, obj_id: null, obj_nm: null, cstm_qry: null, grnl_lvl: null, rule_desc: null, rule_svrt: null, is_rule_actv: null
                };
                this.status = false;
                this.isRuleEditMode = false;
              }
              else {
                this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(data.Contents[0].message));
              }
            }
          });
      }
    }

    if (this.isRuleEditMode) {
      this.dataService.getData(environment.dataQuality.updateKPIRules,
        {
          name: 'update_dq_kpi_rule',
          rule_id: this.kpiDetails.rule_id,
          rule_desc: this.kpiDetails.rule_desc,
          cstm_qry: this.kpiDetails.cstm_qry,
          rule_svrt: this.kpiDetails.rule_svrt !== null ? this.kpiDetails.rule_svrt.slice(0, 1) : null,
          is_rule_actv: this.kpiDetails.is_rule_actv,
          audit_updt_id: this.authenticateService.getUserInfo().user_id
        })
        .subscribe(data => {
          this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.kpi.popupMsg.updated));
          this.latest_rule_id = data.Contents[0].latest_rule_id;
          this.latestRuleIdEmitter.emit(this.latest_rule_id);
        });
      this.kpiDetails = {
        rule_id: null, mstr_rule_id: null, data_layer_id: null, data_layer_nm: null, obj_id: null, obj_nm: null, cstm_qry: null, grnl_lvl: null, rule_desc: null, rule_svrt: null, is_rule_actv: null
      };
      this.status = false;
      this.isRuleEditMode = false;
    }

  }

  editKPIRule(record: KPIDetails) {
    this.resetVariablesToDefaults();
    this.isRuleEditMode = true;

    this.editKPIDetails = {
      rule_id: record.rule_id,
      obj_id: record.obj_id,
      obj_nm: record.obj_nm,
      mstr_rule_id: record.mstr_rule_id,
      data_layer_id: record.data_layer_id,
      data_layer_nm: record.data_layer_nm,
      grnl_lvl: record.grnl_lvl,
      rule_desc: record.rule_desc,
      cstm_qry: record.cstm_qry,
      rule_svrt: record.rule_svrt,
      is_rule_actv: record.is_rule_actv
    };

    // If the form is in EDIT mode then pre-set the DataLayer Dropdown - It will fire dataLayerChange() method
    this.kpiDetails.data_layer_id = this.editKPIDetails.data_layer_id;
    this.kpiDetails = this.editKPIDetails;
  }

}

