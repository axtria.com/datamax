import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { DialogComponent, PositionDataModel, ButtonPropsModel } from '@syncfusion/ej2-angular-popups';
import { GridComponent, CommandModel, ToolbarItems, IRow, Column } from '@syncfusion/ej2-angular-grids';
import { DataControlFormComponent } from '../data-control-form/data-control-form.component';
import { DataQualityDetails } from '../../DataQualityDetails';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';
import { DataControlDetails } from '../../DataControlDetails';
import { closest } from '@syncfusion/ej2-base';


@Component({
  selector: 'app-data-control-list',
  templateUrl: './data-control-list.component.html',
  styleUrls: ['./data-control-list.component.css']
})
export class DataControlListComponent implements OnInit {

  @ViewChild('addNewDataControlDialog', { static: false }) addNewDataControlDialog: DialogComponent;
  @ViewChild('dataControlListGrid', { static: false }) dataControlListGrid: GridComponent;
  @ViewChild('dataControlFormComponent', { static: false }) dataControlFormComponent: DataControlFormComponent;
  @Input() dataQualityDetails: DataQualityDetails;
  @Input() active: boolean;

  //search filter
  @Input('searchtxt') searchtxt: string; 
  @Input('layerToFilter') layerToFilter: number[]; 
  @Input('objToFilter') objToFilter: number[];
  @Input('attrToFilter') attrToFilter: string[];
  @Input('ruleToFilter') ruleToFilter: string[];
  @Input('severityToFilter') severityToFilter: string[]; 
  @Input('statusToFilter') statusToFilter: string[];
  @Output() totalDCRulesCount = new EventEmitter<number>();
  //@Input() gridResultsCountDC: number;

  // count of filtered data after search
  filterMetadata = { count: 0 };

  dataQualityContent: any;
  dataControl: any;
  currentPage: 1;
  gridPageSettings: object;
  public gridCommands: CommandModel[];
  gridToolbar: ToolbarItems[] | Object;
  public position: PositionDataModel = { X: 'right', Y: 'top' };
  dlgAnimationSettings: Object = { effect: 'SlideRight', duration: 600, delay: 0 };
  dlgButtons: ButtonPropsModel[];
  status: boolean;
  isRuleEditMode: boolean = false;
  businessLayerList: [];
  businessLayerFields: object = { value: 'layer_id', text: 'name' };
  objectList: [];
  objectFields: object = { text: 'obj_nm', value: 'obj_id' };
  
  dataControlRules: DataControlDetails[];

  constructor(private dataService: RestService, cs: ContentService) {
    this.dataQualityContent = cs.getContent(environment.modules.dataQuality);
    this.dataControl = this.dataQualityContent.subMenu.dataControl;
    this.dlgButtons = [
      {
        click: this.dlgCancelBtnClick.bind(this), buttonModel: {
          content: this.dataControl.buttons.cancel,
          cssClass: 'btn btn-secondary actionButton'
        }
      },
      {
        click: this.dlgSaveBtnClick.bind(this), buttonModel: {
          content: this.dataControl.buttons.save,
          cssClass: 'btn btn-primary actionButton', disabled: false
        }
      }
    ];

   }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: true, pageSize:5, currentPage: this.currentPage, template: ''};    
    this.gridCommands = [{
        type: 'Edit', buttonOption: {
        iconCss: ' e-icons e-edit', cssClass: 'e-flat',
        click: this.editDataControlRule.bind(this)
      }
    }];
    this.setToolbar();
    this.populateDataControlRules();
    
  }

  onDCListGridDataBound()
  {
    this.totalDCRulesCount.emit(this.filterMetadata.count);
  }

  setToolbar(){
    this.gridToolbar = [{ type: 'label', template: '#showDCResultsCount', align: 'Right' },
    { type: 'Input', template: '#addDCRuleTemplate', align: 'Right', id: 'addRule', click: this.addRuleHandler.bind(this)},
    { type: 'Input', template: '#exportDCTemplate', align: 'Right' },
    { text: 'ColumnChooser', template: '#columnChooserTemplateDC', align: 'Right' }];
  }
  
  addRuleHandler(args: ClickEventArgs): void {
    if (args.item.id === "addRule") {
      this.addDataControlRule();
    }
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    //this. = arg.itemData.value;
  }

  changePageNumber(currentPage: any) {
    this.currentPage = currentPage;
    this.gridPageSettings = { currentPage: this.currentPage };
  }

  addDataControlRule() {
    this.isRuleEditMode = false;
    this.openDataControlRuleForm(false);
  }

  openDataControlRuleForm(isRuleEditMode: boolean) {
    // Open the Data Control Form Dialog with slide effect
    this.dataControlFormComponent.resetEntireForm();
    this.addNewDataControlDialog.show();
    // Call the addNewDataControlRule on the Form
    if (!isRuleEditMode)
      this.dataControlFormComponent.addNewDataControlRule();
  }
  
  populateDataControlRules() {
    this.dataService.getData(environment.dataQuality.getDataControlRules,
      { name: 'get_data_control_rules'})
      .subscribe(data => { 
        this.dataControlRules = data && data.Contents ? data.Contents : []; });
        //this.gridResultsCountDC = this.dataControlRules.length;
  }

  dlgCancelBtnClick() {
    // Reset the fields in form to defaults
    //this.thresholdFormComponent.resetVariablesToDefaults();
    // Hide the RI Form
    this.dataControlFormComponent.resetEntireForm();
    this.addNewDataControlDialog.hide();
  }

  dlgSaveBtnClick() {
    // Calling save function from Form Component
    this.dataControlFormComponent.saveDataControlRule();
  }

  editDataControlRule(args: Event): void {

    this.isRuleEditMode = true;
    this.openDataControlRuleForm(true);

    const rowObj: IRow<Column> = this.dataControlListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').getAttribute('data-uid'));

    const dcDetails = rowObj.data as DataControlDetails;

    this.dataControlFormComponent.editDataControlRule(dcDetails);
  }

  hideDialog(val){
    if (val){
    this.addNewDataControlDialog.hide();
    this.populateDataControlRules();
    }
  }

}
