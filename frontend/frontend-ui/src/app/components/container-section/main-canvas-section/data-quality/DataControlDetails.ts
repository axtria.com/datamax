export class DataControlDetails {
    rule_id: number;
    mstr_rule_id: string;
    src_obj_id: number;
    src_attr_id: number;
    src_bk_attr_id: any;
    src_fltr_con: string;
    tgt_layer_id: number;
    tgt_obj_id: number;
    tgt_attr_id: any;
    tgt_bk_attr_id: any;
    tgt_fltr_con: string;
    tgt_layer_nm: string;
    rule_desc: string;
    agg_fn: string;
    rule_svrt: string;
    is_rule_actv: boolean;
}