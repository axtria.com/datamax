export class KPIDetails {
    rule_id: string;
    mstr_rule_id: string;
    data_layer_id: number;
    data_layer_nm: string;
    obj_id: number;  
    obj_nm: string; 
    cstm_qry: string;
    grnl_lvl: string;
    rule_desc: string;
    rule_svrt: string;
    is_rule_actv: boolean;
}
   
 