import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';

import { FilterService, EditService, CommandColumnService , ToolbarService, ColumnChooserService} from '@syncfusion/ej2-angular-grids';
import { TextBoxModule, NumericTextBoxModule, UploaderModule, MaskedTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { TooltipModule } from '@syncfusion/ej2-angular-popups';
import { ChipListModule } from '@syncfusion/ej2-angular-buttons';
import { DatePickerModule, TimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { RadioButtonModule } from '@syncfusion/ej2-angular-buttons';
//import { ListViewModule } from '@syncfusion/ej2-angular-lists';
// import { NgxTributeModule } from 'ngx-tribute';
import { HighlightDirective } from '../main-canvas-section/brms/rule_section/highlight.directive'
import { MainCanvasSectionRoutingModule } from './main-canvas-section-routing.module';
import { DataCatalogueComponent } from './data-catalogue/data-catalogue.component';
// import { SyncFusionDataTableCatalogueComponent } from './data-catalogue/sync-fusion-data-table-catalogue/sync-fusion-data-table-catalogue.component';
import { BrmsComponent } from './brms/brms.component';
import { InputComponent } from './brms/input_section/input.component';
import { SummaryComponent } from './brms/summary/summary.component';
import { RuleComponent } from './brms/rule_section/rule.component';
import { MainOnboardComponent } from './data-onboarding/main-onboard/main-onboard.component';
// import { NestedSyncTableComponent } from './data-catalogue/sync-fusion-data-table-catalogue/nested-sync-table/nested-sync-table.component';
import { DynamicSyncGridRepeaterComponent } from './data-catalogue/sync-fusion-data-table-catalogue/nested-sync-table/dynamic-sync-grid-repeater/dynamic-sync-grid-repeater.component';
import { WorkspaceComponent } from './brms/workspace/workspace.component';
import { WorkflowComponent } from './brms/workflow/workflow.component';
import { WorkflowDetailComponent } from './brms/workflow-detail/workflow-detail.component';
import { DataFeedDetailsComponent } from './data-onboarding/data-feed-details/data-feed-details.component';
import { FeedDetailsComponent } from './data-onboarding/feed-details/feed-details.component';
import { FileDetailsComponent } from './data-onboarding/file-details/file-details.component';

import { DataLayoutDetailsComponent } from './data-onboarding/data-layout-details/data-layout-details.component';
import { PocDetailsComponent } from './data-onboarding/poc-details/poc-details.component'
import { MainOnboardedDataComponent } from './onboarded-data/main-onboarded-data/main-onboarded-data.component';
import {OnboardedDataCatalogComponent} from './onboarded-data/onboarded-data-catalog/onboarded-data-catalog.component';
import { OnboardedDataDetailsComponent } from './onboarded-data/onboarded-data-details/onboarded-data-details.component';
import { MainDataIngestionComponent } from './data-ingestion/main-data-ingestion/main-data-ingestion.component';
import { DataIngestionDetailsComponent } from './data-ingestion/data-ingestion-details/data-ingestion-details.component';
import { MainDataQualityComponent } from './data-quality/main-data-quality/main-data-quality.component';
import { DetailedCatalogueComponent } from './data-catalogue/detailed-catalogue/detailed-catalogue.component';
import { PhysicalViewCatalogComponent } from './data-catalogue/detailed-catalogue/physical-view-catalog/physical-view-catalog.component';
import { ParametersComponent } from './brms/rule_section/parameters/parameters.component';
import { OverviewComponent } from './brms/overview/overview.component';
import { WorkflowViewComponent } from './brms/overview/workflow-view/workflow-view.component';
import { RuleSetComponent } from './brms/overview/rule-set/rule-set.component';
// tslint:disable-next-line: max-line-length
import { DetailedCatalogViewComponent } from './data-catalogue/detailed-catalogue/detailed-catalog-view/detailed-catalog-view.component';
import { ScenarioComponent } from './brms/scenario/scenario.component';
import { ThresholdValidatonsComponent } from './data-quality/threshold-validatons/threshold-validatons.component';
import { DataService } from './brms/brms_data.service';
import { TreeGridAllModule } from '@syncfusion/ej2-angular-treegrid';
import { AutoCompleteModule } from '@syncfusion/ej2-angular-dropdowns';
import { ReactiveFormsModule } from '@angular/forms';
//import { AngularSvgIconModule } from 'angular-svg-icon';
import { SplitPipe } from 'src/app/shared/pipes/split.pipe';
import { ScenarioListComponent } from './brms/scenario-list/scenario-list.component';
import { enableRipple } from '@syncfusion/ej2-base';
import { DataContComponent } from './data-quality/data-cont/data-cont.component';
import { LineageComponent } from './data-catalogue/detailed-catalogue/lineage/lineage.component';
import { SyncfusionModule } from 'src/app/shared/modules/syncfusion.module';
import { StatisticsComponent } from './data-catalogue/detailed-catalogue/statistics/statistics.component';
//import { from } from 'rxjs';
import { RatingsAndCommentsComponent } from './data-catalogue/detailed-catalogue/ratings-and-comments/ratings-and-comments.component';
import { StarRatingsComponent } from './data-catalogue/detailed-catalogue/ratings-and-comments/star-ratings/star-ratings.component';
import { RatingProgressBarComponent } from './data-catalogue/detailed-catalogue/ratings-and-comments/rating-progress-bar/rating-progress-bar.component';
import { DataStorageDetailsComponent } from './data-onboarding/data-storage-details/data-storage-details.component';
import { NotificationsViewAllComponent } from './notifications-view-all/notifications-view-all.component';
import { MultiSelectAllModule, ListBoxAllModule, DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { CheckBoxModule, ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { DataValidationFormComponent } from './data-quality/data-validations/data-validation-form/data-validation-form.component';
import { DataValidationListComponent } from './data-quality/data-validations/data-validation-list/data-validation-list.component';
import { ReferentialIntegrityFormComponent } from './data-quality/referential-integrity/referential-integrity-form/referential-integrity-form.component';
import { ReferentialIntegrityListComponent } from './data-quality/referential-integrity/referential-integrity-list/referential-integrity-list.component';
import { DataAssetComponent } from './data-catalogue/detailed-catalogue/data-asset/data-asset.component';
import { GlossaryComponent } from './data-catalogue/detailed-catalogue/glossary/glossary.component';
import { BasketInfoComponent } from './data-catalogue/detailed-catalogue/basket-info/basket-info.component';
import { ObjectFileViewComponent } from './data-catalogue/detailed-catalogue/object-file-view/object-file-view.component';
import { ReferentialIntegrityComponent } from './data-quality/referential-integrity/referential-integrity.component';
import { AdvancedSearchComponent } from './data-quality/main-data-quality/advanced-search/advanced-search.component';
import { ThresholdValidatonFormComponent } from './data-quality/threshold-validatons/threshold-validaton-form/threshold-validaton-form.component';
import { ThresholdValidatonListComponent } from './data-quality/threshold-validatons/threshold-validaton-list/threshold-validaton-list.component';
import { DataDictionaryComponent } from './data-catalogue/detailed-catalogue/data-dictionary/data-dictionary.component';
import { KpiValidationListComponent } from './data-quality/kpi-validation/kpi-validation-list/kpi-validation-list.component';
import { KpiValidationFormComponent } from './data-quality/kpi-validation/kpi-validation-form/kpi-validation-form.component';
import { QualityMetricsComponent } from './data-catalogue/detailed-catalogue/quality-metrics/quality-metrics.component';
import { DataControlListComponent } from './data-quality/data-cont/data-control-list/data-control-list.component';
import { DataControlFormComponent } from './data-quality/data-cont/data-control-form/data-control-form.component';
import { DataAssetDetailedViewComponent } from './data-catalogue/detailed-catalogue/data-asset/data-asset-detailed-view/data-asset-detailed-view.component';
import { DataQualityRulesComponent } from './data-quality/main-data-quality-rules/data-quality-rules/data-quality-rules.component';
import { DataQualityObjectsComponent } from './data-quality/main-data-quality-objects/data-quality-objects/data-quality-objects.component';
import { ObjectRulesListComponent } from './data-quality/object-rules-list/object-rules-list/object-rules-list.component';
import { ObjectRulesInformationComponent } from './data-quality/main-data-quality/object-rules-information/object-rules-information/object-rules-information.component';

enableRipple(true);

@NgModule({
  declarations: [
    DataCatalogueComponent,
    // SyncFusionDataTableCatalogueComponent,
    BrmsComponent,
    InputComponent,
    SummaryComponent,
    RuleComponent,
    MainOnboardComponent,
    // NestedSyncTableComponent,
    DynamicSyncGridRepeaterComponent,
    WorkspaceComponent,
    WorkflowComponent,
    WorkflowDetailComponent,
    DataFeedDetailsComponent,
    FeedDetailsComponent,
    FileDetailsComponent,
    DataLayoutDetailsComponent,
    PocDetailsComponent,
    MainOnboardedDataComponent,
    OnboardedDataCatalogComponent,
    OnboardedDataDetailsComponent,
    MainDataIngestionComponent,
    DataIngestionDetailsComponent,
    MainDataQualityComponent,
    AdvancedSearchComponent,
    DataValidationListComponent,
    DataValidationFormComponent,
    ReferentialIntegrityComponent,
    ReferentialIntegrityFormComponent,
    ReferentialIntegrityListComponent,
    DetailedCatalogueComponent,
    PhysicalViewCatalogComponent,
    ParametersComponent,
    OverviewComponent,
    WorkflowViewComponent,
    RuleSetComponent,
    DetailedCatalogViewComponent,
    ScenarioComponent,
    DataContComponent,
    DataControlListComponent,
    DataControlFormComponent,
    ThresholdValidatonsComponent,
    ThresholdValidatonListComponent,
    ThresholdValidatonFormComponent,
    SplitPipe,
    ScenarioListComponent,
    HighlightDirective,
    LineageComponent,
    StatisticsComponent,
    RatingsAndCommentsComponent,
    StarRatingsComponent,
    RatingProgressBarComponent,
    DataStorageDetailsComponent,
    NotificationsViewAllComponent,
    DataAssetComponent,
    GlossaryComponent,
    BasketInfoComponent,
    ObjectFileViewComponent,
    DataDictionaryComponent,
    KpiValidationListComponent,
    KpiValidationFormComponent,
    QualityMetricsComponent,
    DataAssetDetailedViewComponent,
    DataQualityRulesComponent,
    DataQualityObjectsComponent,
    ObjectRulesListComponent,
    ObjectRulesInformationComponent
  ],
  imports: [
    MainCanvasSectionRoutingModule,
    TextBoxModule,
    RadioButtonModule,
    UploaderModule,
    DatePickerModule,
    //ListViewModule,
    //DialogModule,
    TooltipModule,
    // NgxTributeModule,
    ChipListModule,
    TimePickerModule,
    NumericTextBoxModule,
    MaskedTextBoxModule,
    TreeGridAllModule,
    ReactiveFormsModule,
    //AngularSvgIconModule,
    SyncfusionModule,
    MultiSelectAllModule,
    CheckBoxModule,
    ButtonModule,
    ListBoxAllModule,
    DropDownListModule,
    AutoCompleteModule 
    // ProgressBarModule
  ],
  providers: [DatePipe, FilterService, EditService, DataService, CommandColumnService, ToolbarService, ColumnChooserService]
})
export class MainCanvasSectionModule { }
