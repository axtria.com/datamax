import { Component, OnInit } from '@angular/core';
import { TabAnimationSettingsModel } from '@syncfusion/ej2-angular-navigations';
import { GlobalConstants } from 'src/app/shared/common/global-constants';
import { CatalogService } from 'src/app/shared/services/data-max-app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { parentsUntil } from '@syncfusion/ej2-angular-grids';


@Component({
  selector: 'app-data-asset-detailed-view',
  templateUrl: './data-asset-detailed-view.component.html',
  styleUrls: ['./data-asset-detailed-view.component.css']
})
export class DataAssetDetailedViewComponent implements OnInit {
  tabAnimationSettings: TabAnimationSettingsModel = { previous: { effect: "None" }, next: { effect: "None" } };
  public title: any;
  public dataAssetRowData: any;
  public tabkey: any;
  public displayTabName: any;
  public key: string;
  toolbar1: any;
  dataAssetDetailedSort: any;
  searchdataAssetdetails: string; 
  public textWrapSettings: Object = { wrapMode: "both" };
  // pageSettings = { pageSize: 8 };
  // gridPageSettings: Object;
  // currentPage = 1;
  // selectPaginator: any;
  // public PaginationList: any[] = ['5', '10', '20', '30', '50', '100'];

  constructor(private catalogService:CatalogService, private router: Router, private route: ActivatedRoute,) { }

  ngOnInit() {
    this.title = GlobalConstants.siteTitle;
    this.tabkey = this.catalogService.tabKeyname;
    if (this.tabkey == 'dataAsset') {
      this.displayTabName = 'Data Asset';
    }
    this.dataAssetRowData = this.catalogService.dataAssetRowData;
    if (!this.dataAssetRowData) {
      this.router.navigate(['dataCatalogue'], { relativeTo: this.route.parent });
    }
    // this.selectPaginator = this.PaginationList[0];
    // this.gridPageSettings = { pageSizes: false, pageSize: 5, currentPage: this.currentPage, template: ''};
    this.dataAssetDetailedSort = {
      columns: [{ field: 'uri', direction: 'Ascending'},{ field: 'ob_name', direction: 'Ascending'},
      {field: 'col_name', direction: 'Ascending'},{field: 'logi_name', direction: 'Ascending'}, 
      {field: 'datatype', direction: 'Ascending'}, {field: 'buss_desc', direction: 'Ascending'}]};
  }

  setBreadcrumbKey () {
      this.catalogService.setTabKeyname('objects');
      this.router.navigate(['dataCatalogue'], { relativeTo: this.route.parent });
  }

  clickaction(e) {
    const rowEle = parentsUntil(e.target, 'e-row');
    if(rowEle) {
      if(rowEle.classList.contains('border-all')) {
        rowEle.classList.remove('border-all');
      } else {
        rowEle.classList.add('border-all');
      }
    }
  }
}
