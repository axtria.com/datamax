import { Component, AfterViewInit, ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { Network, DataSet } from 'vis';

@Component({
  selector: 'app-lineage',
  templateUrl: './lineage.component.html',
  styleUrls: ['./lineage.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LineageComponent implements AfterViewInit {
  @ViewChild('network1', { static: false }) netwrokX: ElementRef;
  public networkInstance: any;
  public nodes = [
    {
      "id": "customer_file",
      "label": "customer_file",
      "type": "dataset"
    },
    {
      "id": "call_goal_file",
      "label": "call_goal_file",
      "type": "dataset"
    },
    {
      "id": "o1",
      "label": "o1",
      "type": "dataset"
    },
    {
      "id": "o2",
      "label": "o2",
      "type": "dataset"
    },
    {
      "id": "o3",
      "label": "o3",
      "type": "dataset"
    },
    {
      "id": "o4",
      "label": "o4",
      "type": "dataset"
    },
    {
      "id": "4bb79ce4dd6c4375af7fcf5c11f3f431",
      "label": "customer_1",
      "type": "filter"
    },
    {
      "id": "703aa7d89d3f424bb37ba8fbf5478e31",
      "label": "customer_2",
      "type": "filter"
    },
    {
      "id": "7735d6a1e8d4434dbfe9a8cbee8d9254",
      "label": "union",
      "type": "union"
    },
    {
      "id": "e7ec3d34bc814100956f61222be6126c",
      "label": "union all",
      "type": "union"
    }];
  public edges = [
    {
      "from": "customer_file",
      "to": "4bb79ce4dd6c4375af7fcf5c11f3f431"
    },
    {
      "from": "4bb79ce4dd6c4375af7fcf5c11f3f431",
      "to": "o1"
    },
    {
      "from": "customer_file",
      "to": "703aa7d89d3f424bb37ba8fbf5478e31"
    },
    {
      "from": "703aa7d89d3f424bb37ba8fbf5478e31",
      "to": "o2"
    },
    {
      "from": "o1",
      "to": "7735d6a1e8d4434dbfe9a8cbee8d9254"
    },
    {
      "from": "o2",
      "to": "7735d6a1e8d4434dbfe9a8cbee8d9254"
    },
    {
      "from": "7735d6a1e8d4434dbfe9a8cbee8d9254",
      "to": "o3"
    },
    {
      "from": "o1",
      "to": "e7ec3d34bc814100956f61222be6126c"
    },
    {
      "from": "o2",
      "to": "e7ec3d34bc814100956f61222be6126c"
    },
    {
      "from": "e7ec3d34bc814100956f61222be6126c",
      "to": "o4"
    }
  ];
  network_builder() {
    const container = this.netwrokX.nativeElement;
    const nodes = new DataSet<any>(this.nodes);
    const edges = new DataSet<any>(this.edges);
    const options = {
      edges: {
        smooth: {
          type: 'cubicBezier',
          roundness: 0.4
        }
      },
      layout: {
        hierarchical: {
          direction: 'LR',
          sortMethod: 'directed',
        }
      },
      physics:
        { stabilization: { fit: true } }
    };
    const network_compiler = { nodes, edges, options };
    this.networkInstance = new Network(container, network_compiler, {});
    this.networkInstance.stabilize(100);
  }

  // request_visualize(){
      //     this.apiService.getData(environment[this.screenId].visualize_workflow, 
      //   { 'id': this.workflow_data['workflow_id']})
      //   .subscribe(_data => {
      //     this.nodes=this.visual_service.node_mapper(_data['nodes']);
      //     this.edges=this.visual_service.edge_mapper(_data['edges']);
      //     this.network_builder();
      //   });
      // this.network_builder();  
  // }
  constructor() { }
  // ngOnInit() {
  //   // this.network_builder();
  // }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
     this.network_builder();
  }
}
