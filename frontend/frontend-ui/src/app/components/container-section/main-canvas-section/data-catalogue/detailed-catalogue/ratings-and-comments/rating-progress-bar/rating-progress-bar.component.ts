import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-rating-progress-bar',
  templateUrl: './rating-progress-bar.component.html',
  styleUrls: ['./rating-progress-bar.component.css']
})
export class RatingProgressBarComponent implements OnInit {
  public ratingandCommentsLabels: any;
  @ViewChild('rateDiv', { static: false }) public rateDiv;
  @ViewChild('countDiv', { static: false }) public countDiv;
  @Input() index: any;
  @Input() count: any;
  @Input() calcTotalRatingCount: any;
  constructor(cs: ContentService) {
    this.ratingandCommentsLabels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
  }

  ngOnInit() {
  }
  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    const averagePrecisionValue = Math.round( this.count * 10 ) / 10;
    this.rateDiv.nativeElement.style.width = this.count + '%' ;
    this.countDiv.nativeElement.innerText = averagePrecisionValue + ' %';
  }
}
