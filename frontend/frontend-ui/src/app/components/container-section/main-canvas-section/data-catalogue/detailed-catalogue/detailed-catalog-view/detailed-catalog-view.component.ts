import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/shared/services/data-max-app.service';
import { environment } from 'src/environments/environment';
import { EditSettingsModel } from '@syncfusion/ej2-angular-grids';
import { ContentService } from 'src/app/shared/services/content.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { FieldSettingsModel, ToolbarSettingsModel  } from '@syncfusion/ej2-dropdowns';
import { ListBoxComponent, DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { GlobalConstants } from 'src/app/shared/common/global-constants';
import { TabAnimationSettingsModel } from '@syncfusion/ej2-angular-navigations';

@Component({
  selector: 'app-detailed-catalog-view',
  templateUrl: './detailed-catalog-view.component.html',
  styleUrls: ['./detailed-catalog-view.component.css']
})

export class DetailedCatalogViewComponent implements OnInit {
  searchTextTag: any = '';
  public title: any;
  public profileDetails: any;
  public columns: any;
  public editSettings: EditSettingsModel;
  public tableMetaData: any = {};
  public columnMetaData: any = {};
  public completeDataFeed: any = {};
  public statistics: any = {};
  public sample: any = {};
  public structureData: any = [];
  public tagStringFromUIArr: Array<string> = [];
  public bkpTagStringFromUIArr : any[] = [];
  public addTagInput = '';
  public displayTagInputSection = false;
  public editDescriptionFlag = false;
  public editDescriptionInput = '';
  public editCommentsFlag = false;
  public editCommentsInput = '';
  public editBusinessDescriptionFlag = false;
  public editBusinessDescriptionInput = '';
  public editLogicalDescriptionFlag = false;
  public editLogicalDescriptionInput = '';
  public editTechnicalDescriptionFlag = false;
  public editTechnicalDescriptionInput = '';
  public editLogicalNameFlag = false;
  public editLogicalNameInput = '';
  public saveInProgress = false;
  public LoadingProgress = true;
  public editableDataAsset = false;
  public dataAssetFields: FieldSettingsModel = { text: 'name', value: '__id' };
  public dataAssetPlaceholder = 'Choose from below options...';
  public objectToPassDataBack = {
    layer_name: '',
    layer_type: '',
    object_name: '',
    description: '',
    column_name: ''
  };
  public tagSuggestions: any[];   // = ['abc', 'xyz', 'KB'];
  URI: any;
  rowCount: any;
  fileSize: any;
  i18Labels: any;
  URI_ID: any;
  @ViewChild('listbox2', {static: false}) public listObj2: ListBoxComponent;
  @ViewChild('dataAssetDropDown', {static: false}) public dataAssetDropDown: DropDownListComponent;
  public fields: FieldSettingsModel = { text: 'name'};
  public toolbarSettings: ToolbarSettingsModel = { items: ['moveTo', 'moveFrom', 'moveAllTo', 'moveAllFrom']};
  public availableTagList: any;
  public selectedTagList: any;
  selectedObjects: any = [];
  bkpTagSuggestions: any[];
  tabAnimationSettings: TabAnimationSettingsModel = { previous: { effect: "None" }, next: { effect: "None" } };
  listOfDataAssets: any[] = [];
  selectedDataAsset: any;
  dataAssetDisplay: any = {};

  // showDeleteTagOption: boolean;
  public editDescription() {
    this.completeDataFeed.Description = this.editDescriptionInput;
    this.editDescriptionFlag = false;
    this.requestToEdit('Description', this.editDescriptionInput, '');
  }
  public editComments() {
    this.completeDataFeed.Comments = this.editCommentsInput;
    this.editCommentsFlag = false;
    this.requestToEdit('Comments', this.editCommentsInput, '');
  }
  public editBusinessDescription() {
    this.completeDataFeed['Business Description'] = this.editBusinessDescriptionInput;
    this.editBusinessDescriptionFlag = false;
    this.requestToEdit('Business Description', this.editBusinessDescriptionInput, '');
  }

  public editLogicalDescription() {
    this.completeDataFeed['Logical Description'] = this.editLogicalDescriptionInput;
    this.editLogicalDescriptionFlag = false;
    this.requestToEdit('Logical Description', this.editLogicalDescriptionInput, '');
  }

  public editTechnicalDescription() {
    this.completeDataFeed['Technical Description'] = this.editTechnicalDescriptionInput;
    this.editTechnicalDescriptionFlag = false;
    this.requestToEdit('Technical Description', this.editTechnicalDescriptionInput, '');
  }

  public editLogicalName() {
    this.completeDataFeed['Logical Name'] = this.editLogicalNameInput;
    this.editLogicalNameFlag = false;
    this.requestToEdit('Logical Name', this.editLogicalNameInput, '');
  }

  public passTagDetails(tags) {
    tags.forEach(element => {
      element.isDeletable = true;
    });
    let localArr = [];
    this.tagStringFromUIArr = tags;
    this.bkpTagStringFromUIArr = [...tags];

    this.tagStringFromUIArr.forEach(ele => {localArr.push(ele['name']);});

    this.tagSuggestions = this.tagSuggestions.filter(item => {
        return !(localArr.includes(item.name));
    });
  }

  public cancelTagUpdate() {
    this.tagStringFromUIArr = this.bkpTagStringFromUIArr;
    this.tagSuggestions = this.bkpTagSuggestions;
    this.passTagDetails(this.tagStringFromUIArr);
    this.displayTagInputSection = false;
  }

  public deleteTag(tag, check) {
    if(check == 1) {
      const found = this.tagStringFromUIArr.indexOf(tag);
      this.tagStringFromUIArr.splice(found, 1);
      this.bkpTagStringFromUIArr = this.tagStringFromUIArr;
      this.tagSuggestions.push(tag);
    }
    /* this.tagStringFromUIArr = this.tagStringFromUIArr.filter((value) => {
      if (value.trim() !== tag.trim()) {
        return value.trim();
      }
    }); */
    this.requestToEdit('DeleteTag', tag.name, '');
  }

  filterTagList() {
    let localArr = [];
    this.tagSuggestions = this.bkpTagSuggestions;
    this.tagStringFromUIArr.forEach(ele => {localArr.push(ele['name']);});
    this.tagSuggestions = this.tagSuggestions.filter(item => {
        return (!(localArr.includes(item.name))) && (item.name.toLowerCase().includes(this.searchTextTag.toLowerCase()));
    });
  }

  add_tags() {
    this.bkpTagStringFromUIArr = [];
    this.tagStringFromUIArr.forEach((tag: any) => {
      tag.isDeletable = true;
      this.bkpTagStringFromUIArr.push(tag);
      this.addTagInput = tag.name;
      this.addTagToList();
    });
  }
  public addTagToList() {
    this.requestToEdit('Tag', this.addTagInput, '');
    // this.addTagInput = '';
    this.displayTagInputSection = false;
  }

  public addDataAsset() {
    if(this.dataAssetDropDown.value !== null) {
      this.dataService.getData(environment.catalog.link_data_asset, {
        node_id : this.catalogService.completeDataFeed.id,
        data_node_id : this.dataAssetDropDown.value
      }).subscribe(() => {
        this.editableDataAsset = !this.editableDataAsset;
        this.dataAssetDisplay = {};
        this.dataAssetDisplay.name = this.dataAssetDropDown.text;
      });
    }
  }

  cellSave(args) {
    if ((args.columnName === 'Description' ||
        args.columnName === 'Business Description' ||
        args.columnName === 'Logical Name') && args.value.length > 4000) {
      alert(`${this.i18Labels.Detailedcatalogview.ExceedingCharacters} ${args.columnName}!`);
      args.value = args.previousValue;
    } else if (confirm( this.i18Labels.Detailedcatalogview.Message)) {
      const data = {
        attribute_key: (args.columnName === 'Description' ?
                                            'remarks' :
                                            (args.columnName === 'Business Description' ?
                                                                'buss_desc' :
                                                                (args.columnName === 'Logical Name' ?
                                                                'logi_name' : ''))),
        value: args.value,
        // layer_name: this.catalogService.Schema_Name,
        // layer_type: this.catalogService.layerType,
        // object_name: this.catalogService.Table_Name,
        // object_name: args.rowData.parentItem.Name,
        // column_name: args.rowData.Name
        node_id: args.rowData.id
      };
      this.dataService.getData(environment.catalog.edit_attribute, data)
        .subscribe(() => {
        });
    } else {
      args.value = args.previousValue;
    }
  }

  public requestToEdit(attr, attrValue, level) {
    this.saveInProgress = true;
    const objWithParams: any = {};
    objWithParams.user =  this.authService.getUserInfo().username;
    objWithParams.node_id = this.catalogService.completeDataFeed.id;
    // objWithParams.layer_name = this.catalogService.Schema_Name;
    // objWithParams.layer_type = this.catalogService.layerType;
    // objWithParams.object_name = this.catalogService.Table_Name;
    if (level === '') {
      objWithParams.column_name = '';
    }
    if (attr === 'Tag') {
      objWithParams.tag = attrValue;
      this.dataService.getData(environment.catalog.add_tag, objWithParams).subscribe(() => {
        const found = this.tagSuggestions.find(data => {
          return data.name === this.addTagInput;
        });
        if (found) {
          const date = new Date();
          found.created_on = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.toLocaleTimeString();
        }
        found && this.tagStringFromUIArr.push(found);
        this.saveInProgress = false;
        this.addTagInput = '';
      });
    } else if (attr === 'DeleteTag') {
      objWithParams.tag = attrValue;
      this.dataService.getData(environment.catalog.delete_tag, objWithParams).subscribe(() => { this.saveInProgress = false; });
    } else {
      objWithParams.value = attrValue;
      if (attr === 'Business Description') {
        objWithParams.attribute_key = 'buss_desc';
      } else if (attr === 'Description') {
        objWithParams.attribute_key = 'remarks';
      } else if (attr === 'Comments') {
        objWithParams.attribute_key = 'comments';
      } else if (attr === 'Logical Description') {
        objWithParams.attribute_key = 'logi_desc';
      } else if (attr === 'Technical Description') {
        objWithParams.attribute_key = 'tech_desc';
      } else if (attr === 'Logical Name') {
        objWithParams.attribute_key = 'logi_name';
      }
      delete objWithParams.user;
      this.dataService.getData(environment.catalog.edit_attribute, objWithParams).subscribe(() => { this.saveInProgress = false; });
    }
  }

  constructor(public router: Router,
              private route: ActivatedRoute,
              public catalogService: CatalogService,
              private dataService: RestService,
              private authService: AuthenticateService,
              cs: ContentService) {
                this.i18Labels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
              }

  ngOnInit() {
    // this.showDeleteTagOption = false;
    this.loadData();
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: false, mode: 'Batch' };
    this.completeDataFeed = this.catalogService.completeDataFeed;
    if (this.catalogService.completeDataFeed) {
      this.fileSize = this.catalogService.completeDataFeed.Size && (this.catalogService.completeDataFeed.Size / 1024).toFixed(2);
      this.URI = this.catalogService.completeDataFeed.URI;
      this.URI_ID = this.catalogService.completeDataFeed.URI_ID;
      this.URI = this.URI.replace('//', '/').split('/');
      this.URI.shift();
      this.tagSuggestions = this.catalogService.listOfTags;
      this.bkpTagSuggestions = this.catalogService.listOfTags;
      this.listOfDataAssets = this.catalogService.listOfDataAssets;
      this.dataAssetDisplay = this.completeDataFeed.DataAsset[0];
      this.structureData = this.catalogService.ColumnInfo;
      this.passTagDetails(this.completeDataFeed.Tags);
      this.LoadingProgress = false;
    }
    this.title = GlobalConstants.siteTitle;
  }
  heading(col) {
    return col.replace('_', ' ').toUpperCase();
  }
  loadData() {
    if (this.catalogService.Schema_Name &&
      this.catalogService.Table_Name) {
      if (this.catalogService.layerType === this.i18Labels.Detailedcatalogview['Redshift'] ) {
        this.dataService.getData(environment.catalog.profile,
          {
            node_id: this.catalogService.completeDataFeed.id
            // schema: this.catalogService.Schema_Name,
            // table_name: this.catalogService.Table_Name
          }
        )
          .subscribe(data => {
            if (data.response.length > 0 ) {
              this.profileDetails = data;
              [this.tableMetaData, this.columnMetaData, this.sample, this.statistics] = this.profileDetails.response;
              this.rowCount = this.tableMetaData.content[0].row_count;
            }
          });
      } else if (this.catalogService.layerType === this.i18Labels.Detailedcatalogview['S3']) {
        this.dataService.getData(environment.catalog.s3_profiler,
          {
            node_id: this.catalogService.completeDataFeed.id
          }
        )
          .subscribe(data => {
            this.sample = data.response[0];
          });
      } else if (this.catalogService.layerType === this.i18Labels.Detailedcatalogview['Snowflake']) {
        this.dataService.getData(environment.catalog.snowflake_profiler,
          {
            node_id: this.catalogService.completeDataFeed.id
          }
        )
          .subscribe(data => {
            if (data.response.length > 0 ) {
              this.profileDetails = data;
              [this.tableMetaData, this.columnMetaData, this.sample] = this.profileDetails.response;
              this.rowCount = this.tableMetaData.content[0].row_count;
            }
          });
      }
    } else {
      // this.router.navigateByUrl('home/features/dataCatalogue');
      this.router.navigate(['dataCatalogue'], { relativeTo: this.route.parent });
    }
  }

  attachTagToSelected(tag) {
    this.tagSuggestions.forEach((item, pos) => {
      if(item.name == tag.name) {
        this.tagSuggestions.splice(pos, 1);
        this.tagStringFromUIArr.push(tag);
      }
    });
  }

  detachTag(tag) {
    this.tagStringFromUIArr = this.tagStringFromUIArr.filter((item: any) => {
      return !(item.name == tag.name)
    });
  	this.tagSuggestions.push(tag);
    if(tag.isDeletable) {
      this.deleteTag(tag, 2);
    }
  }

  getDataAsset(dataAsset) {
    if(dataAsset && dataAsset[0]) {
      this.dataAssetDropDown.value = dataAsset[0].__id;
      this.dataAssetDropDown.text = dataAsset[0].name;
      return dataAsset[0].name;
    }
  }

  editDataAsset() {
    this.editableDataAsset = !this.editableDataAsset;
    (this.dataAssetDisplay && this.dataAssetDisplay.name && this.dataAssetDisplay.name.length > 0) && setTimeout(() => {
        this.dataAssetDropDown.text = this.dataAssetDisplay.name;
      }, 10);
  }

  // onCreation() {
  //   setTimeout(() => {
  //     if(this.completeDataFeed.DataAsset[0] && this.completeDataFeed.DataAsset[0].name) {
  //       this.dataAssetDropDown.text = this.completeDataFeed.DataAsset[0].name;
  //     }
  //   }, 10);
  // }
}
