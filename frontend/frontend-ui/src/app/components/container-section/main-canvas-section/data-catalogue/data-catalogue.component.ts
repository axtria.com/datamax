import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-data-catalogue',
  templateUrl: './data-catalogue.component.html',
  styleUrls: ['./data-catalogue.component.css']
})
export class DataCatalogueComponent implements OnInit {
  constructor() { }

  public countToDisplay: number;

  public dataResponse: any;
  ngOnInit() {
  }
}
