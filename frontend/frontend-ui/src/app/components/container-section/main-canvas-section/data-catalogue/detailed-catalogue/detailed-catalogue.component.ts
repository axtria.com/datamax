import { Component, OnInit, ViewChild } from '@angular/core';
import { CatalogService } from 'src/app/shared/services/data-max-app.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { DropDownListComponent, MultiSelectComponent } from '@syncfusion/ej2-angular-dropdowns';
import { DatePipe } from '@angular/common';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { CheckBoxComponent } from '@syncfusion/ej2-angular-buttons/src/check-box/checkbox.component';
import { HttpParams } from '@angular/common/http';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-detailed-catalogue',
  templateUrl: './detailed-catalogue.component.html',
  styleUrls: ['./detailed-catalogue.component.css']
})
export class DetailedCatalogueComponent implements OnInit {
  backupConetnt: any[];
  object_uri_id: string;
  basketCount: any;
  fileNameToView: any;
  displayRecords: boolean = false;
  successRecords: any;
  totalRecords: any;
  failedRecords: any;
  report_id: any;
  // searchsuggestion: string = ''; 
  searchdataAsset: string;
  attrArray: any[] = ['tag', 'layer', 'logical_name', 'technical_name'];
  suggestion_list: any[];
  constructor(private catalogService: CatalogService,
              private datePipe: DatePipe,
              private dataService: RestService,
              private authService: AuthenticateService,
              private ts: ToastPopupService,
              private storageService: StorageService,
              cs: ContentService) {
              this.i18Labels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
              this.EndDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  }
  @ViewChild('savedModalDialog', { static: false }) public savedModalDialog: DialogComponent;
  @ViewChild('checkbox', { static: false }) public mulObj: MultiSelectComponent;
  @ViewChild('selectall', { static: false }) public checkboxObj: CheckBoxComponent;
  i18Labels: any;
  SearchIn: string = 'All';
  Layers: string = 'All';
  Catalog_Name: string = '';
  EndDate: any;
  files_list: any = ['Raw files', 'validated files'];
  showAdvSearch: boolean = false;
  showSaveingInput: boolean = false;
  isSearchSaved: boolean = false;

  list1: any = [];
  public receivedData = [];
  public SearchResults: any;
  public key: string;

  menuSelected: any = 'Logical View';
  @ViewChild('fileFrequency', { static: false }) fileFrequency: DropDownListComponent;
  list2: any[];
  public list3: any[];
  public showSearchScreen: boolean;
  advanceSearchList: string[];
  listOfSavedSearches: any;
  nameToSaveSearch: any;

  // Code for new search form
  public searchText: any = '';
  public mode: any = 'CheckBox';
  public filterPlaceholder: any = 'Start typing for filtering...';
  public checkWaterMark: string = 'Choose...';
  public listOfAttributesForSearch: any[] = [];
  public show_input_lists = [];
  public filterInput = '';
  public check_for_column_level: any = true;
  @ViewChild('fileUpload', { static: false }) FileUpload: any;

  fileToUpload: File = null;
  dataFeedContent: any;
  selectedFiles: FileList;
  isUploadSuccessFull = false;
  isUploading = false;
  isUploadConfirmed = false;
  selectedFileName: string;
  fileUploadStatus: any;
  progressval: any;
  progresstext: any;
  progressheight: any;
  progresswidth: any;
  isValidating = false;

  selectSampleFile(event) {
    this.selectedFiles = event.target.files;
  }

  uploadSampleFile() {
    this.isValidating = true;
    this.fileNameToView = this.FileUpload.nativeElement.files[0].name;
    this.isUploading = true;
    this.FileUpload.nativeElement.disabled = true;
    this.progresswidth = 0;
    this.selectedFileName = this.selectedFiles.item(0).name;

    const formData = new FormData();
    const api = environment.catalog.validateBulkData;
    formData.append('file', this.FileUpload.nativeElement.files.item(0));
    this.dataService.postDataWithProgress(api, {
      user_id : this.authService.getUserInfo().user_id,
      // user_name: this.authService.getUserInfo().user_name
      user_name: this.authService.getUserInfo().username
    }, formData)
      .subscribe((data: any) => {
        if (data.percentageComplete) {
          this.progresswidth = data.percentageComplete;
        } else if (data.complete && data.body) {
          this.displayRecords = true;
          this.successRecords = data.body.success_records;
          this.totalRecords = data.body.total_records;
          this.failedRecords = data.body.failure_records;
          this.report_id = data.body.report_id;
          // this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18Labels.uploadFile['Bulk Validate']));
          this.isValidating = false;
          // this.clearUploadContent();
        }
        // this.clearUploadContent();
        this.isValidating = false;
      });
  }

  downloadReport() {
    const filename = new Date().getTime() + '.csv';
    this.dataService.downloadReport(environment.catalog.bulk_import_report, {
      report_id: this.report_id,
      time_stamp: new Date().getTime()
    }, filename);
  }

  clearUploadContent() {
    this.isUploading = false;
    this.isUploadConfirmed = false;
    this.FileUpload.nativeElement.value = null;
    this.FileUpload.nativeElement.disabled = false;
    this.selectedFiles = null;
    this.isValidating = false;
    this.progresswidth = 0;
    this.fileNameToView = '';
    this.displayRecords = false;
  }

  public selectdrop(e, attr) {
    if (attr === 'layer' && e.value.length < 1) {
      e.element.ej2_instances[0].value = e.oldValue;
      alert('Select atleast one layer!');
    } else {
      let str = '';
      this.show_input_lists.forEach((item, pos) => {
        if (item.attribute_name === attr) {
          if (e.value.length > 0) {
            if (attr === 'id' || attr === 'description') {
              const valTosend = e.value.split(',');
              str = JSON.stringify({ [attr]: e.value });
              this.show_input_lists[pos].callBack = JSON.stringify({ [attr]: valTosend });    // '{' + str + '}';
            } else {
              str = str + [attr] + ':[' + e.value + ']';
              this.show_input_lists[pos].callBack = JSON.stringify({ [attr]: e.value });
            }
          } else {
            this.show_input_lists[pos].callBack = '';
          }
        }
      });
      this.search_catalog();
    }
  }

  public attributeSelected(attr) {
    this.dataService.getData(environment.catalog.get_attributesDetails, { 'attr': attr.attribute_name })
      .subscribe(data => {
        if (attr.attribute_name === 'id') {
          attr.type = 'uritext';
          attr.viewText = '';
        } else if (attr.attribute_name === 'description') {
          attr.type = 'desctext';
          attr.specific_list = data.Content[0].unique_values;
          attr.viewText = '';
        } else {
          attr.type = 'checkbox';
          attr.specific_list = data.Content[0].unique_values;
        }
        this.show_input_lists.push(attr);
        this.show_input_lists.sort(this.compare);
        if (attr.attribute_name === 'layer') {
          const set = this.authService.getLayerNames();
          attr.specific_list = attr.specific_list.filter((item) => {
            return set.includes(item);
          });
          attr.value = [attr.specific_list[0]];
          const obj = {};
          obj["value"] = [attr.specific_list[0]];
          this.selectdrop(obj, 'layer');
        }
        // this.listOfAttributesForSearch.forEach((item, pos) => {
        //   if (item.attribute_name === attr.attribute_name) {
        //     this.listOfAttributesForSearch.splice(pos, 1);
        //   }
        // });
        this.listOfAttributesForSearch = this.listOfAttributesForSearch.filter((item) => {
          return (item.attribute_name !== attr.attribute_name);
        });
        // this.listOfAttributesForSearch.splice(i, 1);
      });
  }

  public compare( a, b ) {
    if ( a.order < b.order ) {
      return -1;
    }
    if ( a.order > b.order ) {
      return 1;
    }
    return 0;
  }

  public clear_attribute_for_search(item, index) {
    item.callBack = '';
    this.show_input_lists.splice(index, 1);
    this.listOfAttributesForSearch.push(item);
    this.search_catalog();
  }

  public search_suggestion() {
    this.dataService.getDataWithoutLoader(environment.catalog.get_search_suggestions, {
      searchText : this.searchText
    }).subscribe(data => {
      this.suggestion_list = data['contents'];
    });
  }

  public search_catalog() {
    let attribute_name = '';
    // const formData = new FormData();
    this.show_input_lists.forEach(item => {
      if (item.callBack && item.callBack.length > 0) {
        const str = item.callBack;
        // str.substring(2,str.length-1);
        attribute_name = attribute_name + str.substring(1, str.length - 1) + ',';
      }
    });
    attribute_name = '{' + unescape(attribute_name.substring(0, attribute_name.length - 1)) + '}';
    const params = new HttpParams({
      fromObject: { attribute_name, search_text: this.searchText }
    });
    this.dataService.getData(environment.catalog.list_data_asset, {}).subscribe(data => {
      this.catalogService.setListOfDataAssets(data['Data Asset']);
    });
    this.dataService.postData(environment.catalog.catalog_object_search, {}, params).subscribe((data: any) => {
      this.SearchResults = data.contents;
      // this.checkForAlreadyBasketMember();
      const arr = this.getBasketList();
      this.SearchResults.forEach(element => {
        if (arr.includes(element.URI_ID)) {
          element.alreadyAddedToBasket = true;
        }
      });
      this.catalogService.setSearchResults(data.contents);
      this.catalogService.setShowSearchScreen(true);
      this.showSearchScreen = true;
    });
  }

  getBasketList() {
    let basketList = [];
    if (this.storageService.getLocalItem('basketContent') === null) {
      basketList = []; // empty array of objects
    } else {
      basketList = JSON.parse(this.storageService.getLocalItem('basketContent'));
    }
    return basketList;
  }

  public filterOptions() {
    if (this.filterInput.length < 1) {
      this.listOfAttributesForSearch = this.backupConetnt;
    } else {
      this.listOfAttributesForSearch = this.listOfAttributesForSearch.filter(item => {
        return item.display_attribute_name.toLowerCase().includes(this.filterInput.toLowerCase());
      });
    }
  }
  ngOnInit() {
    this.show_input_lists = [];
    if (this.storageService.getLocalItem('basketContent') && this.storageService.getLocalItem('basketContent').length > 0) {
      const bulkArr = JSON.parse(this.storageService.getLocalItem('basketContent'));
      this.basketCount = bulkArr.filter(function(item, pos) {
        return bulkArr.indexOf(item) === pos;
      }).length;
    } else {
      this.basketCount = 0;
    }
    if(this.catalogService.tabKeyname) {
      this.key = this.catalogService.tabKeyname;
    } else {
        this.key = 'objects';
    }
    // this.displayAttributeList = [];
    this.progressval = 35;
    this.progressheight = 20;
    this.progresswidth = '50%';
    this.progresstext = "loading";
    this.dataService.getData(environment.catalog.get_attributes, {})
      .subscribe(data => {
        this.listOfAttributesForSearch = data.Content;
        this.backupConetnt = this.listOfAttributesForSearch;
        this.listOfAttributesForSearch.forEach((attr) => {
          // this.displayAttributeList.push(attr.display_attribute_name);
          if (this.attrArray.includes(attr.attribute_name)) {
            if(attr.attribute_name === 'layer') {
              attr.order = 1;
            } else if(attr.attribute_name === 'logical_name') {
              attr.order = 2;
            } else if(attr.attribute_name === 'technical_name') {
              attr.order = 4;
            } else if(attr.attribute_name === 'tag') {
              attr.order = 3;
            }
            this.attributeSelected(attr);
          }
        });
      });
  }
  
  setKey() {
    if(this.key == 'dataAsset') {
      this.catalogService.setTabKeyname(this.key);
    } 
  }

  onEnter() {
    if (this.Catalog_Name.length > 0) {
      this.GetSearchResults1();
    }
  }

  GetSearchResults1() {
    this.catalogService.setSearchString(this.Catalog_Name);
    this.dataService.getData(environment.catalog.catalog_search, { search_string: this.Catalog_Name })
      .subscribe(data => {
        this.SearchResults = data.contents;
        this.catalogService.setSearchResults(data.contents);
        this.catalogService.setShowSearchScreen(true);
        this.showSearchScreen = true;
      });
  }

  openSavedModal() {
    this.listOfSavedSearches = [];
    this.dataService.getData(environment.catalog.get_catalog_search_query, { user_id: this.authService.getUserInfo().user_id })
      .subscribe(data => {
        if (data.contents) {
          this.listOfSavedSearches = data.contents;
          this.savedModalDialog.show();
        }
      });
  }

  hideSavedModal() {
    this.savedModalDialog.hide();
  }

  onNotify(e) {
    if (e === 'updateScreen') {
      this.showSearchScreen = false;
    } else if (e === 'updateRecords') {
      this.search_catalog();
    } else if(e === 'bulkRefresh') {
      this.clearUploadContent();
    }
  }

  recieveData(e) {
    this.receivedData = e;
    const set = new Set();
    e.map(item => { set.add(item.Type); });
    // const arr = Array.from(set);
    // arr.forEach(item => {
    //   this.Search_In_List.push({ Searchin_val: item });
    // });
  }

  sendOutBasketCount(e) {
    this.basketCount = e;
  }

  GetSearchResults() {
    const objDetailed: any = {};
    objDetailed.Menu = this.menuSelected;
    objDetailed.Layers = this.Layers;
    objDetailed.Catalog_Name = this.Catalog_Name;
    objDetailed.EndDate = this.EndDate;
    this.advanceSearchList = Object.keys(objDetailed).map(item => { const str = item + ':' + objDetailed[item]; return str; });
    this.catalogService.setDetailed_search_params(objDetailed);
    this.dataService.getData(environment.catalog.catalog_adv_search,
      {
        search_string: this.Catalog_Name,
        layer_type: this.SearchIn === 'All' ? '' : this.SearchIn,
        layer_name: this.Layers === 'All' ? '' : this.Layers
      })
      .subscribe(data => {
        this.SearchResults = data.contents;
        this.catalogService.setSearchResults(data.contents);
        this.catalogService.setShowSearchScreen(true);
        this.showSearchScreen = true;
      });
  }

  AdvanceDataCatalog_Click() {
    this.GetSearchResults();
  }

  clearSearchClicked() {
    this.advanceSearchList = [];
  }

  fileFrequencyChange() {
  }

  menuSelectionChange(selectedMenu) {
    this.menuSelected = selectedMenu;
  }

  callSaveSearch() {
    this.dataService.getData(environment.catalog.save_catalog_search_query, {
      search_text: this.Catalog_Name,
      user_id: this.authService.getUserInfo().user_id,
      connector_type: this.SearchIn,
      layer_name: this.Layers,
      name: this.nameToSaveSearch
    }).subscribe(() => {
      // if (data.status === 'success') {
        this.isSearchSaved = true;
      // }
    });
  }
  basketClicked() {
    this.object_uri_id = this.storageService.getLocalItem('basketContent');
  }
  searchRowClicked(item) {
    this.Catalog_Name = item.search_text;
    this.SearchIn = item.connector_type;
    this.Layers = item.layer_name;
    this.nameToSaveSearch = item.name;
    this.savedModalDialog.hide();
    this.AdvanceDataCatalog_Click();
  }

  public bulkDownloadClicked() {
    const set = this.authService.getLayers();
    let str: any = '';
    const listOfLayersForDownload = [];
    set.forEach(item => {
      if (!(listOfLayersForDownload.includes(item.layer_id))) {
        listOfLayersForDownload.push(item.layer_id);
      }
    });
    listOfLayersForDownload.forEach((item: any) => { str = str + item + ','; });
    const filename = new Date().getTime() + '.csv';
    let api = '';
    this.check_for_column_level === true ? api = environment.catalog.export_catalog_column_level :
                                           api = environment.catalog.download_request;
    this.dataService.downloadReport(api, {
      layers: str.slice(0, str.length - 1),
      time_stamp: new Date().getTime()
    }, filename);
  }
  finalUpload() {
    this.fileNameToView = this.FileUpload.nativeElement.files[0].name;
    this.isUploading = true;
    this.FileUpload.nativeElement.disabled = true;
    this.progresswidth = 0;
    this.selectedFileName = this.selectedFiles.item(0).name;

    let api = '';
    this.check_for_column_level === true ? api = environment.catalog.uploadBulkDataColumn : api = environment.catalog.uploadBulkData;
    this.dataService.getData(api, {
      report_id : this.report_id,
      user_id : this.authService.getUserInfo().user_id,
      user_name: this.authService.getUserInfo().username
    }).subscribe((data: any) => {
      // if (data.status === 'success') {
        this.displayRecords = true;
        this.successRecords = data.success_records;
        this.totalRecords = data.total_records;
        this.failedRecords = data.failure_records;
        this.report_id = data.report_id;
        this.ts._exception.emit(this.ts.getToastSuccessMessage(this.i18Labels.uploadFile['Bulk Upload']));
        this.clearUploadContent();
        document.getElementById('closeImportBtn').click();
      // }
      // this.clearUploadContent();
    });
  }

  resetCatalog() {
    this.ngOnInit();
  }

}
