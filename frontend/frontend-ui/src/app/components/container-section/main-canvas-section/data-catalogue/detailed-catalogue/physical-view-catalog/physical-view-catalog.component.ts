import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/shared/services/data-max-app.service';
import { GridComponent, QueryCellInfoEventArgs, parentsUntil } from '@syncfusion/ej2-angular-grids';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { FilterSettingsModel } from '@syncfusion/ej2-angular-grids';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { FieldSettingsModel, ToolbarSettingsModel  } from '@syncfusion/ej2-dropdowns';
import { ListBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { StorageService } from 'src/app/shared/services/storage.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';

@Component({
  selector: 'app-physical-view-catalog',
  templateUrl: './physical-view-catalog.component.html',
  styleUrls: ['./physical-view-catalog.component.css']
})

export class PhysicalViewCatalogComponent implements OnInit {
  @ViewChild('closeAddExpenseModal', { static: false }) closeAddExpenseModal: ElementRef;
  @Output() sendOutBasketCount = new EventEmitter();
  @Output() notify = new EventEmitter();
  @Input() searchResults: any;
  @Input() searchText: any;
  searchTextTag: any = '';
  @Output() recieveData = new EventEmitter();
  public filterOptions: FilterSettingsModel;
  currentPage = 1;
  public initialSort: Object;
  public gridPageSettings: Object;
  public toolbar: any[];
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;
  @ViewChild('listbox1', {static: false}) public listObj1: ListBoxComponent;
  @ViewChild('listbox2', {static: false}) public listObj2: ListBoxComponent;
  public handCursor: Object = { class: 'clickcss' };
  i18Labels: any;
  initialGridLoad: boolean = true;
  public fields: FieldSettingsModel = { text: 'name'};
  public toolbarSettings: ToolbarSettingsModel = { items: ['moveTo', 'moveFrom', 'moveAllTo', 'moveAllFrom']};
  public availableTagList: any = [];
  public selectedTagList: any = [];
  selectedObjects: any = [];
  backUpAvailableTagList: any = [];
  // basketDisplayCount: number;
  listOfPagination : any[] = ['5', '10', '20', '30', '50', '100'];
  selectionPaginator: any;
  countGen: number = 1;

  constructor(private router: Router,
              private dataService: RestService,
              private catalogService: CatalogService,
              private authService: AuthenticateService,
              cs: ContentService,
              private storageService: StorageService,
              private ts: ToastPopupService,
              private route: ActivatedRoute) {
    this.i18Labels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
  }
  @Output() valueChange = new EventEmitter();
  Set_data_Path(URI) {
    this.valueChange.emit(URI);
  }
  ngOnInit() {
    this.selectionPaginator = this.listOfPagination[2];
    this.filterOptions = {
      type: 'Menu'
    };
    this.toolbar = [{ type: 'label', template: '#showDVResultsCount', align: 'Center' },
    { type: 'Input', template: '#actionDropdown', align: 'Left' },
    { type: 'Input', template: '#customPagerTemplate1', align: 'Right'},
    { type: 'Input', template: '#rightButtons', align: 'Right' },
    { text: 'ColumnChooser', align: 'Right', template: '#columnsSection' } ];
    if (this.searchResults === undefined &&
      this.catalogService.searchResults &&
      this.catalogService.searchResults.length) {
      this.searchResults = this.catalogService.searchResults;
      this.checkForAlreadyBasketMember();
    }
    this.initialSort = {
      columns: [{ field: 'Name', direction: 'Ascending' },
                {field: 'Logical Name', direction: 'Descending'},
                {field: 'URI_ID', direction: 'Descending'},
                {field: 'Tags', direction: 'Descending'},
                {field: 'Type', direction: 'Descending'},
                {field: 'dataAsset', direction: 'Descending'},
                {field: 'Layer_name', direction: 'Descending'}]
    };
    if (this.catalogService.resultsFromRequestedView && this.catalogService.resultsFromRequestedView.contents) {
      this.recieveData.emit(this.catalogService.resultsFromRequestedView.contents);
    }
    this.catalogService.setShowSearchScreen(true);
    // this.initialPage = { pageSizes: true, pageCount: 5 };
    this.gridPageSettings = { pageSizes: true, pageSize: this.selectionPaginator, currentPage: this.currentPage};
    // if (this.catalogService.listOfTags === undefined) {
    this.dataService.getData(environment.catalog.list_tags, {}).subscribe(data => {
        this.availableTagList = data.tags;
        this.backUpAvailableTagList = data.tags;
        this.catalogService.setListOfTags(data.tags);
    });
    // this.dataService.getData(environment.catalog.list_data_asset, {}).subscribe(data => {
    //   this.catalogService.setListOfDataAssets(data['Data Asset']);
    // });
  }

  ngOnChanges() {
    this.dataService.getData(environment.catalog.list_tags, {}).subscribe(data => {
      this.availableTagList = data.tags;
      this.backUpAvailableTagList = data.tags;
      this.catalogService.setListOfTags(data.tags);
    });
  }

  checkForAlreadyBasketMember () {
    let arr = this.getBasketList();
    this.searchResults.forEach(element => {
      if(arr.includes(element.URI_ID)) {
        element.alreadyAddedToBasket = true;
      }
    });
  }

  clearBulkContent() {
    this.notify.emit('bulkRefresh');
  }

  check(e) {
    if(e.target.classList.contains('fade')) {
      this.cancelTag();
    }
  }

  imgClick() {
    this.catalogService.setShowSearchScreen(false);
    // Output() notify: EventEmitter<Boolean> = new EventEmitter<Boolean>();
    this.notify.emit('updateScreen');
    // this.router.navigate(['/features/dataCatalogue']);
  }
  customiseCell(args: QueryCellInfoEventArgs) {
    if (this.searchText.length > 0) {
      if (args.column.field === 'Description' || args.column.field === 'Name' || args.column.field === 'id') {
        const str: string = typeof (args.data[args.column.field]) === "string" ?
          args.data[args.column.field].toLowerCase() : args.data[args.column.field];
        if (typeof (args.data[args.column.field]) === "string" && str.includes(this.searchText.toLowerCase())) {
          args.cell.classList.add('background-color', 'HighLightText');
        } else {
          if (str === this.searchText) {
            args.cell.classList.add('background-color', 'HighLightText');
          }
        }
      } else if (args.column.field === 'Tags') {
        args.data[args.column.field].forEach(item => {
          if (item.name.toLowerCase().includes(this.searchText.toLowerCase())) {
            args.cell.classList.add('background-color', 'HighLightText');
          }
        });
      }
    }
  }
  clickaction(e) {
    const rowEle = parentsUntil(e.target, 'e-row');
    if(rowEle) {
      if(rowEle.classList.contains('border-all')) {
        rowEle.classList.remove('border-all');
      } else {
        rowEle.classList.add('border-all');
      }
    }
    // console.log(rowEle);
    if(e.target.classList.contains('e-currentitem')) {
      this.countGen = parseInt(e.target.name.substring(e.target.name.length - 1, e.target.name.length));
    }
    const gridEle = parentsUntil(e.target, 'e-grid');
    const gobj = gridEle['ej2_instances'][0];
    if (gobj.getRowInfo(e.target) && gobj.getRowInfo(e.target).rowData) {
      if (//(e.target.classList.contains('e-rowcell') && e.target['cellIndex'] === 1) ||
        e.target.classList.contains('next')
      ) {
        const SelectedRowData = this.grid.getRowInfo(e.target).rowData;
        const objectID = SelectedRowData['id'];
        let ColumnInfo: any;
        this.dataService.getData(environment.catalog.requested_view, { node_id: objectID })
          .subscribe(data => {
            ColumnInfo = data.contents;
            const TableName = SelectedRowData['Name'];
            const path = SelectedRowData['URI'];
            const SchemaName = path.split('/')[(path.split('/').length - 2)];
            const objPar: any = {};
            objPar.Schema_Name = SchemaName;
            objPar.Layer_Type = SelectedRowData['Layer Type'];
            objPar.Table_Name = TableName;
            objPar.completeDataFeed = SelectedRowData;
            objPar.ColumnInfo = ColumnInfo;
            this.catalogService.setDetails_Params(objPar);
            this.Set_data_Path(path);
            this.catalogService.setShowSearchScreen(true);
            this.router.navigate(['detailedCatalogView'], { relativeTo: this.route.parent });
          });
      }
      if (e.target.classList.contains('add')) {
        const SelectedRowData = this.grid.getRowInfo(e.target).rowData;
        const objectID = SelectedRowData['URI_ID'];
        const arr = this.getBasketList();
        if (!arr.includes(objectID)) {
          arr.push(objectID);
        }
        this.storageService.setLocalItem('basketContent', JSON.stringify(arr));
        this.sendOutBasketCount.emit(arr.length);
      }
    }
  }

  getStringOfTags(tagsArr) {
    // return tagsArr;
    if (tagsArr.length > 0) {
      let str = '';
      tagsArr.forEach(item => {
        str = str + item.name + ', ';
      });
      return str.substr(0, str.length - 2);
    }
  }

  getDataAsset(dataAsset) {
    if(dataAsset && dataAsset[0]) {
      return dataAsset[0].name;
    }
  }

  dataBound() {
    if (this.initialGridLoad) {
      this.initialGridLoad = false;
      const pager = document.getElementsByClassName('e-gridpager');
      let topElement;
      if (this.grid.allowGrouping || this.grid.toolbar) {
        topElement = this.grid.allowGrouping ? document.getElementsByClassName('e-groupdroparea') :
          document.getElementsByClassName('e-toolbar');
      } else {
        topElement = document.getElementsByClassName('e-gridheader');
      }
      this.grid.element.insertBefore(pager[0], topElement[0]);
    }
  }
  add_tags() {
    this.selectedObjects = document.getElementById('physical-View')['ej2_instances'][0].getSelectedRecords();
    this.selectedObjects.forEach((item) => {
      this.selectedTagList.forEach((tag) => {
        const objWithParams: any = {};
        objWithParams.user = this.authService.getUserInfo().username;
        objWithParams.node_id = item.id;
        objWithParams.tag = tag.name;
        this.dataService.getData(environment.catalog.add_tag, objWithParams).subscribe(() => {
          this.ts._exception.emit(this.ts.getToastSuccessMessage('Tag Added!!'));
          this.closeAddExpenseModal.nativeElement.click();
          this.notify.emit('updateRecords');
        });
      });
    });
  }

  addToBasket() {
    const arr = this.getBasketList();
    this.grid.getSelectedRecords().forEach(item => {
      if (!arr.includes(item)) {
        arr.push(item['URI_ID']);
      }
    });
    // this.basketDisplayCount = this.grid.getSelectedRecords().length;
    this.storageService.setLocalItem('basketContent', JSON.stringify(arr));

    this.sendOutBasketCount.emit(arr.filter((item, pos) => {
      return arr.indexOf(item) === pos;
    }).length);
    this.notify.emit('updateRecords');
  }
  getBasketList() {
    let basketList = [];
    if (this.storageService.getLocalItem('basketContent') === null) {
      basketList = [];  //empty array of objects
    } else {
      basketList = JSON.parse(this.storageService.getLocalItem('basketContent'));
    }
    return basketList;
  }
  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.selectionPaginator = arg.itemData.value;
  }

  attachTagToSelected(tag) {
    this.availableTagList.forEach((item, pos) => {
      if(item.name == tag.name) {
        this.availableTagList.splice(pos, 1);
        this.selectedTagList.push(tag);
      }
    });
  }

  detachTag(tag) {
    this.selectedTagList.forEach((item, pos) => {
      if(item.name == tag.name) {
        this.selectedTagList.splice(pos, 1);
        this.availableTagList.push(tag);
      }
    });
  }

  filterTagList() {
    this.availableTagList = this.backUpAvailableTagList;
    this.availableTagList = this.availableTagList.filter(element => {
      return (element.name.toLowerCase().includes(this.searchTextTag.toLowerCase()));
    });
  }

  cancelTag() {
    if(this.selectedTagList.length > 0) {
      this.selectedTagList.forEach(element => {
        this.availableTagList.push(element);
      });
    }
    this.selectedTagList = [];
  }
}
