import { Component, OnInit, Input } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-ratings-and-comments',
  templateUrl: './ratings-and-comments.component.html',
  styleUrls: ['./ratings-and-comments.component.css']
})
export class RatingsAndCommentsComponent implements OnInit {
  @Input() nodeId: any;
  public ratingandCommentsLabels: any;
  public reviewComments: any[];
  public averageRating;
  public totalReviewCount;
  public ratingFrequency;
  public percentileCount;
  public calcTotalRatingCount;
  public submitNewReview = false;
  public editReview = false;
  public currentDate = Date.now();
  public loggedInUsername = this.authService.getUserInfo().firstname + ' ' + this.authService.getUserInfo().lastname;
  public newReviewComment: any;
  rateCount = [];
  progressBarRatePercentile = [];
  score = 5;

  submitReview() {
    let flag = false;
    this.reviewComments.forEach(item => {
      if (item.user === this.loggedInUsername) {
        flag = true;
        this.rateCount[item.rating] = this.rateCount[item.rating] - 1;
        this.rateCount[this.score] = this.rateCount[this.score] + 1;
        this.averageRating = ((this.averageRating * this.totalReviewCount) - item.rating + this.score ) / this.totalReviewCount;
        item.comment = this.newReviewComment;
        item.timestamp = this.currentDate;
        item.rating = this.score;
        this.dataService.getData(environment.catalog.add_review_comments,
          {node_id: this.nodeId,
            rating: this.score,
            user: this.loggedInUsername,
            comment: this.newReviewComment
          }).subscribe();
        this.submitNewReview = false;
      }
    });
    if (!flag) {
      this.dataService.getData(environment.catalog.add_review_comments,
        {
          node_id: this.nodeId,
          rating: this.score,
          user: this.loggedInUsername,
          comment: this.newReviewComment
        }).subscribe();
      this.reviewComments.unshift({ comment: this.newReviewComment, rating: this.score, user: this.loggedInUsername });
      this.reviewComments.forEach(item => {
        if (item.user === this.loggedInUsername) {
          this.totalReviewCount = parseInt(this.totalReviewCount) + 1;
          this.rateCount[this.score] = this.rateCount[this.score] + 1;
          if (this.totalReviewCount < 1) {
            this.averageRating = 0;
            this.averageRating = ((this.averageRating * this.totalReviewCount) + this.score) / this.totalReviewCount;
          } else {
            this.averageRating = ((this.averageRating * (this.totalReviewCount - 1)) + this.score ) / this.totalReviewCount;
          }
        }
      });
    }
    this.submitNewReview = false;
    this.editReview = true;
  }

  newReview() {
    this.submitNewReview = true;
  }

  dismissReviewSubmit() {
    this.submitNewReview = false;
  }

  constructor( cs: ContentService , private dataService: RestService,
               private authService: AuthenticateService) {
    this.ratingandCommentsLabels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
  }

  ngOnInit() {
    this.dataService.getData(environment.catalog.reviews, {node_id : this.nodeId })
    .subscribe((reviewData) => {
        this.averageRating = reviewData.contents.average_rating ? reviewData.contents.average_rating : '0';
        this.reviewComments = reviewData.contents.comments;
        this.ratingFrequency = reviewData.contents.rating_frequency;
        const rateProgress = Object.keys(this.ratingFrequency);
        rateProgress.forEach(frequency => {
          this.rateCount.push(this.ratingFrequency[frequency]);
        });
        function totalReviewCount(obj) {
          return Object.keys(obj).reduce((sum, key) => sum + parseFloat(obj[key] || 0), 0);
        }
        this.totalReviewCount = `${totalReviewCount(reviewData.contents.rating_frequency)}`;
        this.calcTotalRatingCount = this.rateCount.length;
        this.reviewComments.forEach(comment => {
          if (this.loggedInUsername === comment.user) {
            this.editReview = true;
            this.newReviewComment = comment.comment;
            this.score = comment.rating;
          }
        });
    });
  }

  onRateChange = (score) => {
    this.score = score;
  }

}
