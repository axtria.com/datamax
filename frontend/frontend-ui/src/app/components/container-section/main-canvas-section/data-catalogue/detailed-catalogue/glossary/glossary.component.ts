import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { CommandModel, Column, IRow, IEditCell } from '@syncfusion/ej2-grids';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { MultiSelectComponent } from '@syncfusion/ej2-angular-dropdowns';
// import { Query } from '@syncfusion/ej2-data';
import { closest } from '@syncfusion/ej2-base';
import { HttpParams } from '@angular/common/http';
// import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-glossary',
  templateUrl: './glossary.component.html',
  styleUrls: ['./glossary.component.css']
})
export class GlossaryComponent implements OnInit {
  csLabels: any;
  editSettings: any;
  newSubCategoryList: any[];
  fileNameToView: any;
  progresswidth: number;
  report_id: any;
  flagForSatus: boolean = false;
  fileId: any;
  constructor(private dataService: RestService, cs: ContentService, private ts: ToastPopupService, 
    private authenticateService: AuthenticateService) {
    this.csLabels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
  }
  public GlosListOfPagination : any[] = ['5', '10', '20', '30', '50', '100'];
  selectionPaginator: any;
  rowData: any;
  public data: any[];
  public searchText = '';
  initialSort: any;
  toolbar1: any;
  public gridPageSettings: object;
  currentPage = 1;
  newStatus = false;
  newDescription = '';
  newValue = '';
  newSubCategory = '';
  newCategory = '';
  selectedFiles: FileList;
  isUploadSuccessFull = false;
  isUploading = false;
  isUploadConfirmed = false;
  selectedFileName: string;
  fileUploadStatus: any;
  public commands: CommandModel[];
  public valueDataSource: any[] = [];
  public searchDescription: any = '';
  public boolParams: IEditCell = { params: {checked: false } };

  displayRecords = false;
  successRecords: any;
  totalRecords: any;
  failedRecords: any;

  @ViewChild('gridelement', { static: false }) public grid: GridComponent;
  @ViewChild('fileUpload', { static: false }) FileUpload: any;

  public categoryData: any[] = [];
  /*
  { [key: string]: Object }[] = [
    { categoryName: 'Tag', categoryId: '1' },
    { categoryName: 'KPI', categoryId: '2' },
    { categoryName: 'Abbreviations', categoryId: '3'},
    { categoryName: 'DataAsset', categoryId: '4'}
  ]; */

  public subCategoryData: any[] = [];
  /*
  { [key: string]: Object }[] = [
      { subCategoryName: 'Business Tag', categoryId: '1', subCategoryId: '101' },
      { subCategoryName: 'Logical Tag ', categoryId: '1', subCategoryId: '102' },
      { subCategoryName: 'Other tag ', categoryId: '1', subCategoryId: '103' },
      { subCategoryName: 'Quality', categoryId: '2', subCategoryId: '201' },
      { subCategoryName: 'Others ', categoryId: '2', subCategoryId: '202' },
      { subCategoryName: 'Others ', categoryId: '3', subCategoryId: '301' },
      { subCategoryName: 'DataAsset ', categoryId: '4', subCategoryId: '401' }
  ]; */

  public dropDownData: any = []; //{status: "success",content: [{category: "DataAsset",sub_category: ["DataAsset"]},{category: "KPI",sub_category: ["Logical KPI","Technical KPI"]},{category: "Tag",sub_category: ["Logical Tag","Busniess Tag"]}]};

  public allCategories: any[] = [];
  public allSubCategories: any[] = [];

  public subCategoryDataDource: { [key: string]: Object }[] = this.subCategoryData;
  public categoryFields: Object = { text: 'categoryName', value: 'categoryId' };
  public subCategoryFields: Object = { text: 'subCategoryName', value: 'subCategoryId' };
  public categoryWatermark = 'Category';
  public subCategoryWatermark = 'Sub-category';
  public valueWatermark = 'Value';
  public mode: any = 'CheckBox';
  @ViewChild('category' , { static: false } ) public categoryObj: MultiSelectComponent;
  @ViewChild('subCategory' , { static: false }) public subCategoryObj: MultiSelectComponent;
  @ViewChild('valueMulti' , { static: false }) public valueMultiObj: MultiSelectComponent;
  
  public triggerSearch() {
    let attribute_name = '';
    if (this.categoryObj.value && this.categoryObj.value.length) {
      let str = '';
      str = str + JSON.stringify({ ['category']: this.categoryObj.text.split(',') });
      str = str.substring(1, str.length - 1);
      attribute_name = attribute_name + str;
    }
    if (this.subCategoryObj.value && this.subCategoryObj.value.length) {
      let str = '';
      str = str + JSON.stringify({ ['subcategory']: this.subCategoryObj.text.split(',') });
      str = ',' + str.substring(1, str.length - 1);
      attribute_name = attribute_name + str;
    }
    if (this.valueMultiObj.value && this.valueMultiObj.value.length) {
      let str = '';
      str = str + JSON.stringify({ ['name']: this.valueMultiObj.text.split(',') });
      str = ',' + str.substring(1, str.length - 1);
      attribute_name = attribute_name + str;
    }
    if (this.searchDescription && this.searchDescription.length > 0) {
      let str = '';
      str = str + JSON.stringify({ ['description']: [this.searchDescription] });
      str = ',' + str.substring(1, str.length - 1);
      attribute_name = attribute_name + str;
    }
    if ( attribute_name.length > 0 || this.searchText.length > 0 ) {
      attribute_name = '{' + unescape(attribute_name) + '}';
      const params = new HttpParams({
        fromObject: { attribute_name, search_text: this.searchText }
      });
      this.dataService.postData(environment.catalog.search_glossary, {}, params).subscribe((data: any) => {
        this.data = data.contents;
        this.data && this.data.forEach(item => {
          item.status === 'true' ? item.status = true : item.status = false;
        });
      });
    } else {
      this.loadContentForGlossary();
    }
  }

  public categoryChange(): void {
    this.allSubCategories = [];
    this.valueDataSource = [];
    this.subCategoryObj.value = [];
    this.valueMultiObj.value = [];
    if (this.categoryObj.value.length > 0) {
      this.categoryObj.value.forEach(item => {
        this.dropDownData.content.forEach(ele => {
          if (ele.category === item) {
            this.allSubCategories.push(...ele.sub_category);
          }
        });
      });
      this.data && this.data.forEach(item => {
        if (this.categoryObj.text.includes(item.category)) {
          this.valueDataSource.push(item.name);
        }
      });
      if (this.valueDataSource.length > 0) {
        this.valueMultiObj.enabled = true;
      }
    }
    this.triggerSearch();
  }
  subCategoryChange() {
    this.valueDataSource = [];
    this.valueMultiObj.value = [];
    if (this.subCategoryObj.value.length > 0) {
      this.valueMultiObj.enabled = true;
      this.data && this.data.forEach(item => {
        if (this.categoryObj.text.includes(item.category) && this.subCategoryObj.text.includes(item.subcategory)) {
          this.valueDataSource.push(item.name);
        }
      });
    }
    this.triggerSearch();
  }

  public valueChange() {
    this.triggerSearch();
  }

  ngOnInit() {
    this.commands = [{ type: 'Edit', buttonOption: { iconCss: '', cssClass: 'editBtn' } },
    { type: 'Save', buttonOption: { content: 'Save', cssClass: 'btn btn-sm btn-primary actionButton custom-save', click: this.saveClicked.bind(this) } },
    { type: 'Cancel', buttonOption: { content: 'Cancel', cssClass: 'btn actionButton custom-cancel' } }];

    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Normal', allowEditOnDblClick: false };
    this.loadContentForGlossary();

    this.selectionPaginator = this.GlosListOfPagination[0];
    this.toolbar1 = [
      { type: 'label', template: '#showDVResultsCount2', align: 'Center'},
      { type: 'Input', template: '#customGlosPagerTemplate1', align: 'Right'},
      { type: 'Input', template: '#glossaryTools', align: 'Right' }
    ];

    this.initialSort = {
      columns: [{ field: 'category', direction: 'Ascending' }]
    };

    this.gridPageSettings = { pageSizes: true, pageSize: 5, currentPage: this.currentPage};
  }

  // ngAfterViewInit() {
    // console.log('Here');
    // debugger
    // this.categoryObj.selectAll(true);
  // }

  public saveClicked(args: Event): void  {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(closest(args.target as Element, '.e-row').getAttribute('data-uid'));
    setTimeout(() => {
      this.editRow(rowObj);
   }, 10);
  }

  public loadContentForGlossary() {
    this.dataService.getData(environment.catalog.get_glossary_meta, {}).subscribe(dropDownData => {
      this.allCategories = [];
      // this.allSubCategories = [];
      this.dropDownData = dropDownData;
      this.allCategories.push(...this.dropDownData.content.map(item => item.category));
      // this.dropDownData.content.forEach(item => this.allCategories.push(item.category));
      this.allSubCategories.push(...this.dropDownData.content.map(item => item.sub_category));
      // this.dropDownData.content.forEach(item => this.allSubCategories.push(...item.sub_category));
    });
    this.dataService.getData(environment.catalog.list_business_glossary, {}).subscribe(data => {
      this.data = data.business_glossary;
      // this.categoryObj.selectAll(true);
      this.data && this.data.forEach(item => {
        item.status === 'true' ? item.status = true : item.status = false;
      });
    });
  }

  public onEditStatus() {
    this.flagForSatus = !this.flagForSatus;
  }

  addToList() {
    // dmx_dev/api/catalog/add_business_glossary?status=true&name=tesing&category=Tag&subcategory=KPI2&description=iuwehf

    this.dataService.getData(environment.catalog.add_business_glossary, {
      status: this.newStatus,
      name: this.newValue,
      category: this.newCategory,
      subcategory: this.newSubCategory,
      description: this.newDescription
    }).subscribe(() => {
      this.loadContentForGlossary();
      this.newCategory = '';
      this.newSubCategory = '';
      this.newValue = '';
      this.newDescription = '';
      // this.ts._exception.emit(this.ts.getToastSuccessMessage('Message To Display!'));
      document.getElementById('cancelBtn').click();
    });
  }

  downloadTemplate() {
    const filename = 'GlossaryTemplate' + '.csv';
    this.dataService.downloadReport(environment.catalog.export_glossary, {}, filename);
  }
  selectSampleFile(event) {
    this.selectedFiles = event.target.files;
  }

  // onCreate(): void {
  //   this.data && this.categoryObj.selectAll(true);
  // }

  uploadBusinessGlossary() {
    this.isUploading = true;
    this.fileNameToView = this.FileUpload.nativeElement.files[0].name;
    this.isUploading = true;
    this.FileUpload.nativeElement.disabled = true;
    this.progresswidth = 0;
    this.selectedFileName = this.selectedFiles.item(0).name;

    const formData = new FormData();
    // let api = '';
    // this.check_for_column_level == true ? api = environment.catalog.uploadBulkDataColumn : api = environment.catalog.uploadBulkData;
    formData.append('file', this.FileUpload.nativeElement.files.item(0));
    this.dataService.postDataWithProgress(environment.catalog.validate_glossary, {          // import_glossary
      // user_id : this.authService.getUserInfo().user_id,
      // user_name: this.authService.getUserInfo().user_name
    }, formData)
      .subscribe((data: any) => {
        if (data.percentageComplete) {
          this.progresswidth = data.percentageComplete;
        } else if (data.complete && data.body) {
          this.displayRecords = true;
          this.successRecords = data.body.success_records;
          this.totalRecords = data.body.total_records;
          this.failedRecords = data.body.failed_records;
          this.fileId = data.body.file_id;
          this.report_id = data.body.report_id;
        }
      });
  }

  finalUpload() {
    this.dataService.getData(environment.catalog.import_glossary, {uuid: this.fileId}).subscribe(() => {
      this.ts._exception.emit(this.ts.getToastSuccessMessage(this.csLabels.uploadFile['Bulk upload glossary']));
      this.clearUploadContent();
      document.getElementById('closeModalBtn').click();
    });
  }

  downloadReport() {
    const filename = new Date().getTime() + '.csv';
    this.dataService.downloadReport(environment.catalog.bulk_import_report, {
      report_id: this.report_id,
      time_stamp: new Date().getTime()
    }, filename);
  }

  clearUploadContent() {
    this.isUploading = false;
    this.isUploadConfirmed = false;
    this.FileUpload.nativeElement.value = null;
    this.FileUpload.nativeElement.disabled = false;
    this.selectedFiles = null;
    this.progresswidth = 0;
    this.fileNameToView = '';
    this.displayRecords = false;
  }

  editRow(rd1) {
    const rd = rd1.data;
    var obj = {
      node_id : rd.id,
      value : rd.name,
      category : rd.category,
      subcategory : rd.subcategory,
      description : rd.description
    };
    this.flagForSatus ? obj['status'] = !rd.status : obj['status'] = rd.status;
    this.dataService.getData(environment.catalog.update_business_glossary, obj).subscribe(
      () => {this.flagForSatus = false; this.triggerSearch();}
    );
  }

  addedNewCategory(newCategory) {
    // let localId: any = '';
    this.newSubCategoryList = [];
    // this.allCategories.forEach(element => {
    //   if (element === newCategory) {
    //     localId = element;
    //   }
    // });
    this.dropDownData.content.forEach(ele => {
      if (ele.category === newCategory) {
        this.newSubCategoryList.push(...ele.sub_category);
      }
    });
    this.newSubCategoryList.length > 0 ? this.newSubCategory = this.newSubCategoryList[0] : this.newSubCategory = '';
    // this.allSubCategories.forEach(ele => {
    //   if (ele === localId) {
    //     this.newSubCategoryList.push(ele);
    //   }
    // });
  }
  changeGlosPageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.selectionPaginator = arg.itemData.value;
  }

  checkPermission() {
    return this.authenticateService.isModuleVisible('CATALOG.ASSIGN_TAG');
  }
}
