import { Component, OnInit, Input, EventEmitter, Output  } from '@angular/core';

@Component({
  selector: 'app-star-ratings',
  templateUrl: './star-ratings.component.html',
  styleUrls: ['./star-ratings.component.css']
})
export class StarRatingsComponent implements OnInit {

@Input() score;
@Input() maxRateCount = 5;
@Input() showStarRating = false;
@Input() isEdit = false;
@Output() rateChanged = new EventEmitter();
marked = -1;

constructor() { }

ngOnInit() {
}

  public markStar = (index) => {
    this.marked = this.marked === index ? index - 1 : index;
    this.score = this.marked + 1;
    this.rateChanged.next(this.score);
    return true;
  }

  public isStarMarked = (index) => {
    if (!this.showStarRating) {
      if (index <= this.marked) {
        return 'fa to-rate stars fa fa-star fa-lg checked';
      } else {
        return 'fa to-rate stars fa fa-star-o fa-lg unchecked';
      }
    } else {
      if (this.score >= index + 1) {
        return 'to-display fa stars fa fa-star fa-lg checked';
      } else if (this.score > index && this.score < index + 1) {
        return 'to-display fa stars fa fa-star-half-o fa-lg checked';
      } else {
        return 'to-display fa stars fa fa-star-o fa-lg unchecked';
      }
    }
  }

}
