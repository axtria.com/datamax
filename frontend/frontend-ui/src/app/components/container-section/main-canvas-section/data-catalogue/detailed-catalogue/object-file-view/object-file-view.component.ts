import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RestService } from 'src/app/shared/services/rest.service';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-object-file-view',
  templateUrl: './object-file-view.component.html',
  styleUrls: ['./object-file-view.component.css']
})
export class ObjectFileViewComponent implements OnInit {
  sortSettings: any;
  toolbarSettings: any;
  public gridPageSettings: object;
  currentPage = 1;
  selectPaginator: any;
  public PaginationList: any[] = ['5', '10', '20', '30', '50', '100'];
  @Input() objUriId: any;
  FileLabels: any;
  public FileInfo: any;
  constructor(cs: ContentService, private dataService: RestService) {
    this.FileLabels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
  }
  ngOnInit() {
    this.loadData();
  }

  changePage(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.selectPaginator = arg.itemData.value;
  }

  loadData() {
    this.dataService.getData(environment.catalog.object_file_profiler, {
      obj_uri_id: this.objUriId
    })
      .subscribe(data => {
        if (data.content.length > 0) {
          this.FileInfo = data.content;
          this.sortSettings = {
            columns: [{ field: 'file_name', direction: 'Ascending' }]
          };
          this.selectPaginator = this.PaginationList[0];
          this.toolbarSettings = [
            { type: 'label', template: '#showResultsCount1', align: 'Right'},
            // { type: 'Input', template: '#fileTemplate1', align: 'Right'},
            // { text: 'ColumnChooser', align: 'Right', template: '#filecolSection'}
          ];
          this.gridPageSettings = { pageSizes: true, pageSize: 5, currentPage: this.currentPage};
        }
      });
  }

}
