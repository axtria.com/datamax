import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dynamic-sync-grid-repeater',
  templateUrl: './dynamic-sync-grid-repeater.component.html',
  styleUrls: ['./dynamic-sync-grid-repeater.component.css']
})
export class DynamicSyncGridRepeaterComponent implements OnInit {

  @Input() public profile: any;
  public columns: any;
  gridToolBar: any;
  public gridPageSettings: object;
  currentPage = 1;
  selectPaginator: any;
  public PaginationList: any[] = ['5', '10', '20', '30', '50', '100'];

  constructor() { }

  ngOnInit() { }

  ngOnChanges(): void {
    this.profile ? this.columns = this.profile.columns : this.columns = [];
    this.gridToolBar = [
      // { type: 'label', template: '#showResultsCount', align: 'Center'},
      // { type: 'Input', template: '#dictionaryTemplate', align: 'Right'},
      // { text: 'ColumnChooser', align: 'Right', template: '#colSection' },
      { type: 'label', template: '#showResultsCount', align: 'Right'} ];
      this.selectPaginator = this.PaginationList[0];
      this.gridPageSettings = { pageSizes: true, pageSize: 5, currentPage: this.currentPage};
  }
  changePage(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.selectPaginator = arg.itemData.value;
  }

  heading(col) {
    return col.replace('_', ' ').toUpperCase();
  }
}
