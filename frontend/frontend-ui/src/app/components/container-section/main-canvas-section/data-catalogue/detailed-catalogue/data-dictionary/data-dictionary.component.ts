import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { EditSettingsModel } from '@syncfusion/ej2-angular-grids';

@Component({
  selector: 'app-data-dictionary',
  templateUrl: './data-dictionary.component.html',
  styleUrls: ['./data-dictionary.component.css']
})
export class DataDictionaryComponent implements OnInit {
  @Input() public dictionary: any;
  i18Labels: any;
  dictionarySort: any;
  dictionaryToolBar: any;
  public gridPageSettings: object;
  currentPage = 1;
  selectPaginator: any;
  public PaginationList: any[] = ['5', '10', '20', '30', '50', '100'];
  public editSettings: EditSettingsModel;
  constructor(private dataService: RestService,
              cs: ContentService) {
    this.i18Labels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
  }

  ngOnInit() {
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: false, mode: 'Batch' };
    this.dictionarySort = {
      columns: [{ field: 'Name', direction: 'Ascending' }]
    };
    this.selectPaginator = this.PaginationList[0];
    this.dictionaryToolBar = [
      { type: 'label', template: '#showResultsCount', align: 'Right'},
      { type: 'Input', template: '#dictionaryTemplate', align: 'Right'}
    ];
    this.gridPageSettings = { pageSizes: true, pageSize: 5, currentPage: this.currentPage};
  }
  changePage(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.selectPaginator = arg.itemData.value;
  }
  cellSave(args) {
    if ((args.columnName === 'Description' ||
      args.columnName === 'Business Description' ||
      args.columnName === 'Logical Name') && args.value.length > 4000) {
      alert(`${this.i18Labels.Detailedcatalogview.ExceedingCharacters} ${args.columnName}!`);
      args.value = args.previousValue;
    } else if (confirm(this.i18Labels.Detailedcatalogview.Message)) {
      const data = {
        attribute_key: (args.columnName === 'Description' ?
          'remarks' :
          (args.columnName === 'Business Description' ?
            'buss_desc' :
            (args.columnName === 'Logical Name' ?
              'logi_name' : ''))),
        value: args.value,
        node_id: args.rowData.id
      };
      this.dataService.getData(environment.catalog.edit_attribute, data)
        .subscribe(() => {
        });
    } else {
      args.value = args.previousValue;
    }
  }
}
