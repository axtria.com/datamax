import { Component, OnInit, Input } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/shared/services/data-max-app.service';
import { parentsUntil } from '@syncfusion/ej2-angular-grids';

@Component({
  selector: 'app-data-asset',
  templateUrl: './data-asset.component.html',
  styleUrls: ['./data-asset.component.css']
})
export class DataAssetComponent implements OnInit {
  constructor(private dataService: RestService, private router: Router, private route: ActivatedRoute,
    public catalogService: CatalogService) { }
  // public searchText: string = '';
  @Input('searchdataAsset') searchdataAsset: string; 
  initialSort: any;
  toolbar1: any;
  dataAssetSort: any;
  public textWrapSettings: Object = { wrapMode: "both" };
  pageSettings = { pageSize: 8 };
  gridPageSettings: Object;
  currentPage = 1;
  selectPaginator: any;
  rowData: any;
  // initialPage: any;
  public handCursor: Object = { class: 'view-cursor' };
  public PaginationList: any[] = ['5', '10', '20', '30', '50', '100'];
  public dataAsset: any;
  // public dataAsset: any[] = [
  //   {
  //     name: "Customer",
  //     description: "customer data",
  //     layer: 'Landing'
  //   },
  //   {
  //     name: "Customer",
  //     description: "customer data",
  //     layer: 'staging'
  //   }];
    
  ngOnInit() {
    // this.loadDataAsset();
    this.dataAssetList();
    this.selectPaginator = this.PaginationList[0];
    this.gridPageSettings = { pageSizes: true, pageSize: 5, currentPage: this.currentPage, template: ''};
    this.dataAssetSort = {
      columns: [{ field: 'dataasset', direction: 'Ascending'}, { field: 'description', direction: 'Ascending'},
      { field: 'layer', direction: 'Ascending'}]};
  }

  public dataAssetList() {
    this.dataService.getData(environment.catalog.get_data_asset, {}).subscribe((data) => {
      this.dataAsset = data.content;
    });
  }

  public loadDataAsset() {
    this.dataService.getData(environment.catalog.requested_view, { node_id: 26183 })
      .subscribe(data => {
        this.dataAsset = data.contents;
      });
  }

  clickaction(e) {
    if(e.target.classList.contains('next')) {
      this.router.navigate(['detaileddataassetView'], { relativeTo: this.route.parent });
    }
    const rowEle = parentsUntil(e.target, 'e-row');
    if(rowEle) {
      if(rowEle.classList.contains('border-all')) {
        rowEle.classList.remove('border-all');
      } else {
        rowEle.classList.add('border-all');
      }
    }
  }

  openViewPanel(data) {
    this.catalogService.setDataAssetRowData(data);
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
    this.selectPaginator = arg.itemData.value;
  }

  // triggerSearch() {

  // }
}
