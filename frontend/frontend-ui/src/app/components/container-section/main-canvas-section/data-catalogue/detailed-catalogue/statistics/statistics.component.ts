import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { RestService } from 'src/app/shared/services/rest.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  @Input() nodeId: any;
  public SummaryResults: any;
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;
  SummaryLabels: any;
  constructor(cs: ContentService, private dataService: RestService) {
    this.SummaryLabels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
  }

  ngOnInit() {
  }
  ViewSummaryClicked() {
    this.dataService.getData(environment.catalog.view_summary, {
      node_id : this.nodeId,
      time_stamp: new Date().getDate()
    })
    .subscribe(data => {
      if (data.response.length > 0) {
        this.SummaryResults = data.response[3];
      }
    });
  }
}
