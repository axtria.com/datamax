import { Component, ViewChild, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/shared/services/data-max-app.service';
import { parentsUntil, GridComponent } from '@syncfusion/ej2-angular-grids';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { FilterSettingsModel } from '@syncfusion/ej2-angular-grids';
import { HttpParams } from '@angular/common/http';
import { StorageService } from 'src/app/shared/services/storage.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';

@Component({
  selector: 'app-basket-info',
  templateUrl: './basket-info.component.html',
  styleUrls: ['./basket-info.component.css']
})
export class BasketInfoComponent implements OnChanges {
  @Output() sendOutBasketCount = new EventEmitter();
  @Input() public Obj_URI_IDs: any;
  @Output() notify = new EventEmitter();
  i18Labels: any;
  currentPage = 1;
  searchText: any;
  public searchResults: any[];
  public initialSort: object;
  public initialPage: object;
  public baskettoolbar: any[];
  public filterOptions: FilterSettingsModel;
  @ViewChild('basketgrid', { static: false }) public grid: GridComponent;
  public basketAvlTags: any;
  public basketAddedTags: any = [];
  selectedObjects: any = [];
  searchTextTag: any = '';
  public checkForColumnLevel: boolean = true;

  constructor(private router: Router,
              private dataService: RestService,
              public catalogService: CatalogService,
              private authService: AuthenticateService,
              cs: ContentService,
              private ts: ToastPopupService,
              private storageService: StorageService,
              private route: ActivatedRoute,
              private utilService: UtilService) {
              this.i18Labels = cs.getContent(environment.modules.catalog).DATACATALOGUE;
              this.CustomSettingForBasket();
  }

  ngOnChanges() {
    if (this.Obj_URI_IDs != null) {
      this.loadData();
      this.basketAvlTags = this.catalogService.listOfTags;
      //this.backUpAvailableTagList = this.catalogService.listOfTags;
      // if (this.catalogService.listOfTags === undefined) {
      //   this.dataService.getData(environment.catalog.list_tags, {}).subscribe(data => {
      //     this.basketAvlTags = data.tags;
      //   });
      // }
    }
  }
  private CustomSettingForBasket() {
    this.initialPage = { pageSizes: false, pageSize: 5, currentPage: this.currentPage };
    this.baskettoolbar = [
      { type: 'Input', template: '#actionDropdown1', align: 'Left' },
      { type: 'Input', template: '#showBasketCount', align: 'Right'},
      { text: 'ColumnChooser', template: '#basketColumnSection', align: 'Right' },
    ];
  }

  loadData() {
    let localArr = JSON.parse(this.Obj_URI_IDs);
    let attribute_name = '';
    if (localArr.length > 0) {
      attribute_name = JSON.stringify({ ['unique_id']: localArr });
      const params = new HttpParams({
        fromObject: { attribute_name, search_text: '' }
      });
      this.dataService.postData(environment.catalog.catalog_object_search, {}, params).subscribe((data: any) => {
        this.searchResults = data.contents;
        this.sendOutBasketCount.emit(data.contents.length);
        // this.CustomSettingForBasket();
      });
    } else {
      this.searchResults = null;
      this.sendOutBasketCount.emit(0);
    }
   }
  getStringOfTags(tagsArr) {
    if (tagsArr.length > 0) {
      return tagsArr.map(item => item.name).join(', ');
    }
  }
  clickaction(e) {
    const gridEle = parentsUntil(e.target, 'e-grid');
    const gobj = gridEle['ej2_instances'][0];
    if (gobj.getRowInfo(e.target) && gobj.getRowInfo(e.target).rowData) {
      if (e.target.classList.contains('next')) {
        const SelectedRowData = this.grid.getRowInfo(e.target).rowData;
        const objectID = SelectedRowData['id'];
        let ColumnInfo: any;
        this.dataService.getData(environment.catalog.requested_view, { node_id: objectID })
          .subscribe(data => {
            ColumnInfo = data.contents;
            const TableName = SelectedRowData['Name'];
            const path = SelectedRowData['URI'];
            const SchemaName = path.split('/')[(path.split('/').length - 2)];
            const objPar: any = {};
            objPar.Schema_Name = SchemaName;
            objPar.Layer_Type = SelectedRowData['Layer Type'];
            objPar.Table_Name = TableName;
            objPar.completeDataFeed = SelectedRowData;
            objPar.ColumnInfo = ColumnInfo;
            this.catalogService.setDetails_Params(objPar);
            // this.Set_data_Path(path);
            // this.catalogService.setShowSearchScreen(true);
            this.router.navigate(['detailedCatalogView'], { relativeTo: this.route.parent });
          });
      }
    }
  }
  includeTag(tag) {
    this.basketAvlTags = this.utilService.removeArrayPropItem(this.basketAvlTags, 'name', tag);
    this.basketAddedTags.push(tag);
  }

  detachTag(tag) {
    this.basketAddedTags = this.utilService.removeArrayPropItem(this.basketAddedTags, 'name', tag );
    this.basketAvlTags.push(tag);
  }

  cancelTag() {
    if(this.basketAddedTags.length > 0) {
      this.basketAvlTags = this.basketAvlTags.concat(this.basketAddedTags);
    }
    this.basketAddedTags = [];
  }

  check(e) {
    if(e.target.classList.contains('fade')) {
      this.cancelTag();
    }
  }

  add_basket_tags() {
    this.selectedObjects = document.getElementById('basket-View')['ej2_instances'][0].getSelectedRecords();
    this.selectedObjects.forEach((item) => {
      this.basketAddedTags.forEach((tag) => {
        const objWithParams: any = {};
        objWithParams.user = this.authService.getUserInfo().username;
        objWithParams.node_id = item.id;
        objWithParams.tag = tag.name;
        this.dataService.getData(environment.catalog.add_tag, objWithParams).subscribe(() => {
          this.notify.emit('updateRecords');
          this.ts._exception.emit(this.ts.getToastSuccessMessage('Tag Added!!'));
          document.getElementById('cancelTagBtn').click();
          this.loadData();
        });
      });
    });
  }
  removeRecord(uri_id) {
    var localArr = JSON.parse(this.Obj_URI_IDs);
    localArr = this.utilService.removeArrayItem(localArr, uri_id);
    this.Obj_URI_IDs = JSON.stringify(localArr);
    this.storageService.setLocalItem('basketContent', this.Obj_URI_IDs);
    this.loadData();
  }

  basketDownload() {
    const filename = new Date().getTime() + '.csv';
    this.dataService.downloadReport(environment.catalog.export_nodes, {
      uriList: this.Obj_URI_IDs,
      columnFlag: this.checkForColumnLevel
    }, filename);
  }
}
