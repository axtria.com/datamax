import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataAssetDetailedViewComponent } from './data-asset-detailed-view.component';

describe('DataAssetDetailedViewComponent', () => {
  let component: DataAssetDetailedViewComponent;
  let fixture: ComponentFixture<DataAssetDetailedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataAssetDetailedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataAssetDetailedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
