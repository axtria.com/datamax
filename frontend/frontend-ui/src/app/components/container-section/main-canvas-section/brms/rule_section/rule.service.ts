import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RuleService {

  constructor() { }

  make_tree_from_data_set(received_datasets) {
    let counter=1
    var tree_array=[];
    for(let i in received_datasets){
      let parent_value = counter++;
      tree_array.push({id:parent_value, name:i, hasChild: true, tooltip: i})
        for(let j=0;j<received_datasets[i].length;j++){
          tree_array.push({
            id: counter,
            pid: parent_value,
            name: received_datasets[i][j]['column_name']+" ("+received_datasets[i][j]['data_type'] + ")",
            tooltip: i + '.' + received_datasets[i][j]['column_name']
        }) 
          counter++;
      }
    }
    return { dataSource: tree_array, id: 'id', parentID: 'pid', text: 'name', hasChildren: 'hasChild', tooltip: 'tooltip' };
  }

  getRulesData(received_rules) {
    var itemsData = [];
    for(let i: number = 0; i < received_rules.length; i++) {
      itemsData.push({ header: "Title : <strong> "+ received_rules[i]['rule_name'] + '</strong><button ejs-button type="button" class="btn fa fa-pencil" style="float: right; " [isPrimary]="true" id='+ received_rules[i]['rule_id'] +'></button> </strong><button ejs-button type="button" class="btn fa fa-trash" style="float: right; " [isPrimary]="true" id='+ received_rules[i]['rule_id'] +'></button>', content: "<strong>Output table name:</strong> " + received_rules[i]['output']+" <br><br>"+ "<strong>Rule:</strong>" + received_rules[i]['user_input'] });
    }
    return itemsData;
  }

}
