import { Component, OnInit, ViewChild, Input,ElementRef,ViewEncapsulation } from '@angular/core';
import { Network, DataSet } from 'vis';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { WorkflowViewService } from './workflow-view.service';

@Component({
  selector: 'app-workflow-view',
  templateUrl: './workflow-view.component.html',
  styleUrls: ['./workflow-view.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WorkflowViewComponent implements OnInit {
  public screenId;
  @ViewChild('network', { static: false }) netwrokX: ElementRef;
  @Input() public workflow_data : any;
  public networkInstance: any;
  public nodes = null;
  public edges = null;

  network_builder() {
    const container = this.netwrokX.nativeElement;
    const nodes = new DataSet<any>(this.nodes);
    const edges = new DataSet<any>(this.edges);
    const options = {
      edges: {
        smooth: {
          type: "cubicBezier",
          roundness: 0.4
        }
      },
      layout: {
        hierarchical: {
          direction: "LR",
          sortMethod: "directed",
        }
      },
      physics:
        { stabilization: { fit: true } }
    };
    const network_compiler = { nodes, edges, options };
    this.networkInstance = new Network(container, network_compiler, {});
    this.networkInstance.stabilize(100);
  }

  request_visualize(){
      this.apiService.getData(environment[this.screenId].visualize_workflow, 
        { 'id': this.workflow_data['workflow_id']})
        .subscribe(_data => {
          this.nodes=this.visual_service.node_mapper(_data['nodes']);
          this.edges=this.visual_service.edge_mapper(_data['edges']);
          this.network_builder();
        });
        
  }

  constructor(private apiService: RestService,private route: ActivatedRoute,private visual_service:WorkflowViewService) { 
    this.screenId = this.route.snapshot.paramMap.get('id');    
  }

  ngOnChanges(){
    if(this.workflow_data.workflow_id!=null)
    this.request_visualize()
  }


  ngOnInit() {
  }

}

