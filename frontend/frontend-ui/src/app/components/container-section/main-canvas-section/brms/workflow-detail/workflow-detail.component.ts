import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { DataService } from '../brms_data.service';
import { TabComponent } from '@syncfusion/ej2-angular-navigations/src/tab/tab.component';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogComponent } from '@syncfusion/ej2-angular-popups/src/dialog/dialog.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-workflow-detail',
  templateUrl: './workflow-detail.component.html',
  styleUrls: ['./workflow-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WorkflowDetailComponent implements OnInit {
  @ViewChild('tab',{static:false}) tab: TabComponent;
  @ViewChild('confirmDialog', { static: false }) public confirmDialog: DialogComponent;
  headerText: Object;
  workflow_name: string;
  wfContent: any;
  enableSave: boolean = false;
  public confirmWidth: string = '500px';
  public animationSettings: Object = { effect: 'Zoom' };
  screenId: string;

  switchtab(){ // this will work on save/create and next button
    this.tab.enableTab(0, false);
    this.tab.enableTab(1, true);
    this.tab.enableTab(0, true);
    this.enableSave = true;
    this.ds.ruleSet.emit("");
  }

  callrulesave(){
    this.ts._exception.emit(this.ts.getToastSuccessMessage(this.wfContent.toastwfSaveSuccess));
    this.cancelRuleSet();
  }

  openConfirmDialog() {
    this.confirmDialog.show();
  }

  cancelRuleSet() {
      this.router.navigate(['workflow', this.screenId], {relativeTo: this.route.parent});
    
  }

  dont_delete_file() {
    this.confirmDialog.hide();
  }

  tabtoggle(e){ // this will work on if tab is switced from tab itself.
    if(e.selectedIndex==0)
      this.ds.inputSet.emit('');
    if(e.selectedIndex==1)
      this.ds.ruleSet.emit('');
  }

  constructor(private ds: DataService, private ts: ToastPopupService, private cs : ContentService,
    private router: Router, public route: ActivatedRoute) {
    this.screenId = this.route.snapshot.paramMap.get('id');
    this.workflow_name = sessionStorage.getItem('workspace_name');
    this.wfContent = this.cs.getContent(environment.modules.brms).WORKFLOW;
    this.headerText= [{ text: this.wfContent.input }, { text: this.wfContent.ruleSet }];
  }

  ngOnInit() {
  }
}
