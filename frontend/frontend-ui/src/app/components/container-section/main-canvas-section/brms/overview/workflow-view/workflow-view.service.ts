import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WorkflowViewService {

  node_mapper(nodes) {
    nodes.map(function (node) {
      switch(node.type){
        case "dataset":     { node['image'] = "assets/images/component_icons/dataset.svg";    node['shape'] = "image"; node['widthConstraint']=80 ; return (node); }
        case "aggregate" :  { node['image'] = "assets/images/component_icons/aggregate.svg";  node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        case "analytical" : { node['image'] = "assets/images/component_icons/analytical.svg"; node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        case "custom" :     { node['image'] = "assets/images/component_icons/custom.svg";     node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        case "dedupe" :     { node['image'] = "assets/images/component_icons/dedupe.svg";     node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        case "derived" :    { node['image'] = "assets/images/component_icons/derived.svg";    node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        case "filter" :     { node['image'] = "assets/images/component_icons/filter.svg";     node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        case "join" :       { node['image'] = "assets/images/component_icons/join.svg";       node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        case "partition" :  { node['image'] = "assets/images/component_icons/partition.svg";  node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        case "union" :      { node['image'] = "assets/images/component_icons/union.svg";      node['shape'] = "image"; node['widthConstraint']=80; node["size"]=35; return (node); }
        default : return (node)
      }
    })
    return(nodes)
  }
    edge_mapper(edges){
        edges.map(function (edge) {
        edge['arrows'] = "to";
        return (edge);
        })
        return(edges)
    }

  constructor() { }
}




