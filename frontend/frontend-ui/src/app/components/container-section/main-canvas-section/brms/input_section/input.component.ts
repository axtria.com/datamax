import { Component, OnInit, ViewChild, ViewEncapsulation, Input } from '@angular/core';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';
import { ToolbarItems, RowDeselectEventArgs, GridComponent } from '@syncfusion/ej2-angular-grids';
import { RowSelectEventArgs } from '@syncfusion/ej2-angular-grids'
import { DataUtil } from '@syncfusion/ej2-data';
import { Output, EventEmitter } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { ChipList } from '@syncfusion/ej2-buttons';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { DataService } from '../brms_data.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { InputService } from './input.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InputComponent implements OnInit {
  // __________________________________Defined Variables________________________________________________________
  @ViewChild('modalDialog', { static: false }) public modalDialog: DialogComponent;
  @ViewChild('chip', { static: false }) public chip: ChipList;
  @ViewChild('chip1', { static: false }) public chip1: ChipList;
  @ViewChild('grid', { static: false }) public grid: GridComponent;
  @Input() public screenId : any;
  @Output() executetabswitch = new EventEmitter<string>();
  screenUrl: any;
  public target: string = "#modalTarget";
  public content: string = "";
  public isModal: Boolean = true;
  public animationSettings: Object = { effect: 'Zoom' };
  public hide: any;
  public toolbarOptions: ToolbarItems[] ;
  Store_Tables = [];
  removable = true;
  public dropdata: string[];
  public adapterlist: string[] = [];
  public adapterlistold: string[] = [];
  public adapterlistByRowNo: number[] = [];
  public filename;
  public description;
  public connector_id;
  inputContent: any;
  isEditable: boolean = false;
  selectOptions={ persistSelection: true };
  gridDataBoundGrid1: any;
  public actualadapterlist: any[]= [];
  public actualadapterlistold: any[] = [];
  subsVar: Subscription;wfId;
  // __________________________________Defined Variables End________________________________________________________

  rowSelected(args: RowSelectEventArgs) {

    if(args.target && args.data && args.data['name']) {

      let _temparray = this.adapterlistold.concat(this.adapterlist);
      if ("validation" === this.screenId && _temparray.length == 1 && args.data){
        this.adapterlist.pop();
        this.actualadapterlist.pop();
        this.adapterlistByRowNo.pop();
      }
      if((("validation" === this.screenId && _temparray.length !=1) || "validation" !== this.screenId) 
          && args.data &&  _temparray.indexOf(args.data['name']) === -1) {
        this.adapterlist.push( args.data['name']);
        this.actualadapterlist.push({ "name":args['data']['name'],"adaptor_meta": args['data']['adaptor_meta'],"schema": args['data']['schema'] })
        this.adapterlistByRowNo.push(args.rowIndex);
        this.chip.refresh();
        this.chip1.refresh();
      }
    }

  }

  rowDeselecting(args: RowDeselectEventArgs){
    if (this.adapterlistold.indexOf(args.data[0]['name']) != -1) {
      args.cancel = true;
      return;
    }
  }

  rowDeselected(args: RowDeselectEventArgs){
      this.adapterlist.splice(this.adapterlist.indexOf(args.data[0]['name']), 1); 
      this.actualadapterlist.splice(this.actualadapterlist.findIndex(function(item){return item.name === args.data[0]['name']}), 1); 
      this.adapterlistByRowNo.splice(this.adapterlistByRowNo.indexOf(args.rowIndex[0]), 1);
      this.chip.refresh();
      this.chip1.refresh();
    
  }
  
  onDelete(args) {
    this.adapterlist.splice(this.adapterlist.indexOf(args.data), 1); 
    this.actualadapterlist.splice(this.actualadapterlist.findIndex(function(item){return item.name === args.data}), 1); 
    this.adapterlistByRowNo.splice(this.adapterlistByRowNo.indexOf(this.grid.getRowIndexByPrimaryKey(args.data)), 1);
    this.grid.selectionModule.selectRows(this.adapterlistByRowNo);
    this.chip.refresh();
    this.chip1.refresh();
    args.cancel = true; 
    
  } 
 
  // pressing create and next OR save and next (depend on click from workspace) button
  submitdata() {
    this.inputService.createOrSaveWf(this.wfId, this.screenId, this.filename, this.description, 
      JSON.stringify(this.actualadapterlistold.concat(this.actualadapterlist)))
    .subscribe(() => this.executetabswitch.emit());
    
  }

  getdata_request() {
    this.dropdata=null;
    this.adapterlist=[];
    this.adapterlistold=[];
    this.actualadapterlistold=[];
    this.adapterlistByRowNo=[];
    this.apiService.getData(environment[this.screenId].GET_WORKFLOW, 
    { 'id': this.wfId})
      .subscribe(data => {
        this.filename=data.META.name;
        this.description=data.META.description;   
        this.adapterlistold=Object.keys(data.SELECTED_DATASETS);
        this.actualadapterlistold = this.inputService.getFormattedAdapterList(data.SELECTED_DATASETS);
    })
  }


  // __________________________________ Modal Functions ________________________________________________________
  ngAfterViewInit(): void {
    document.getElementById('modalbtn').focus();
  }
  // On Button click, modal Dialog will be shown
  public modalBtnClick: EmitType<object> = () => {
    this.modalDialog.show();
    
  }
  // On Dialog close, 'Open' Button will be shown
  public modalDlgClose: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = '';
    this.chip.refresh();
    
  }
  // On Dialog open, 'Open' Button will be hidden
  public modalDlgOpen: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = 'none';
    if(this.adapterlistByRowNo.length == 0) {
      this.adapterlistold.forEach((data)=>{
        this.adapterlistByRowNo.push(this.grid.getRowIndexByPrimaryKey(data));
      });
    }
    this.grid.selectionModule.selectRows(this.adapterlistByRowNo);
    this.chip1.refresh();
  }
  // Close the Dialog, while clicking "OK" Button of Dialog
  public dlgButtonClick: EmitType<object> = () => {
    this.modalDialog.hide();
  }
  // Initialize Button to open the modal Dialog
  public buttons: Object[] = [{ click: this.dlgButtonClick.bind(this), buttonModel: { content: 'Done', cssClass: 'e-primary' } }];

// __________________________________ Modal Functions End ________________________________________________________
  ngOnInit() {
    this.initialise();
  }

  initialise(){
    this.wfId = sessionStorage.getItem('workflow_id');
    // this will fetch list of tables and schemas
    this.apiService.getData(environment[this.screenId].LIST_FILE_ADAPTER, {'connector_id':sessionStorage.getItem("connectorid")}).subscribe(data => {
      this.Store_Tables = data.contents;
      this.dropdata = DataUtil.distinct(this.Store_Tables, 'table_schema') as string[];
      this.screenUrl = environment[this.screenId];
    });
    if(this.wfId){
      this.getdata_request();
    }
  }
  ngOnDestroy() {
    if (this.subsVar) {
      this.subsVar.unsubscribe()
    }
  }  
  constructor(private apiService: RestService, private cs: ContentService, private ds: DataService, 
    private inputService: InputService) { 
    this.inputContent = this.cs.getContent(environment.modules.brms).INPUT;
    this.toolbarOptions = [this.inputContent.Search];
    this.subsVar=this.ds.inputSet.subscribe(() => {
      this.initialise();
      });
  }
}




