import {Injectable, EventEmitter} from'@angular/core';

@Injectable()
export class DataService{
    //auxillary emitter
    ruleSet = new EventEmitter<any>();
    inputSet = new EventEmitter<any>();
    isWorkflowEditable : boolean;
    constructor() { }

}