import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { RuleService } from '../rule_section/rule.service';
import { TreeViewComponent, NodeKeyPressEventArgs, NodeClickEventArgs } from '@syncfusion/ej2-angular-navigations';
import { ChipList } from '@syncfusion/ej2-buttons';

@Component({
  selector: 'app-scenario',
  templateUrl: './scenario.component.html',
  styleUrls: ['./scenario.component.css']
})
export class ScenarioComponent implements OnInit {

  scContent: any;
  screenId: string;scenarioId;
  screenUrl: any;
  received_workflow_id:any;
  workflow_data: any;
  parameters: any;
  public field: Object = {}; datasets: Object = {};
  public filename;
  @ViewChild('overviewDialog',{static: false}) public overviewDialog: DialogComponent;
  @ViewChild('outputDialog',{static: false}) public outputDialog: DialogComponent;
  ParameterContent: any;
  parameter_type_list: any;
  wfdesc: any;wfName: any;
  public customNameAttributes: Object = {class: 'paramNameColorBlue'};
  parameterValues: any;
  @ViewChild ('outputtree',{static: false}) outputtree: TreeViewComponent;
  @ViewChild('chipds', { static: false }) public chipds: ChipList;
  checkedDataSet : any[] = [];
  scenarioParameterValues: void;
  public checkedNodes: string[];

  constructor(private apiService: RestService,private cs : ContentService,
    private route: ActivatedRoute, private router: Router, private ts: ToastPopupService,
    private ruleService: RuleService) { 
    this.scContent = this.cs.getContent(environment.modules.brms).SCENARIO;
    this.screenId = this.route.snapshot.paramMap.get('id');
    this.scenarioId = sessionStorage.getItem('scenarioId');
    this.screenUrl = environment[this.screenId];
    this.wfName = sessionStorage.getItem("workspace_name");
    this.wfdesc = sessionStorage.getItem("workflow_description");
    this.ParameterContent = this.cs.getContent(environment.modules.brms).PARAMETERS;
    this.parameter_type_list = this.ParameterContent.ParameterTypeList;
  }

  ngOnInit() {

    this.received_workflow_id = sessionStorage.getItem('workflow_id');
     let _apiList = [
      this.apiService.getData(this.screenUrl.GET_WORKFLOW, {'id': this.received_workflow_id}),
      this.apiService.getData(this.screenUrl.scenario.get_parameter_values, {'id': this.received_workflow_id})
     ];
     if(this.scenarioId)
     _apiList.push(this.apiService.getData(this.screenUrl.scenario.view_scenario, { 'id': this.scenarioId }));
      
      forkJoin(_apiList).subscribe(
        data=>{
          let _workflow = data[0];
          this.workflow_data={"rules" : _workflow.RULES,"workflow_id":sessionStorage.getItem('workflow_id')}
          this.parameters=Object.values(_workflow.PARAMETERS);
          this.datasets = this.ruleService.make_tree_from_data_set(_workflow.DATASETS);
          if(data[1]) {
            this.parameterValues = data[1];
          }
          if(data[2]){
            let _data = data[2].Contents[0];
            this.checkedDataSet = _data.scenario_meta.output_adaptors;
            this.scenarioParameterValues = _data.scenario_meta.parameters.reduce(function(r, e) {
              r[e.name] = e.value;
              return r;
            }, {});
            this.chipds.refresh();
            this.filename = _data.name;
          }
        });

    }
    
    setParamValue(val){
      this.field[val.name] = val.value.toString();
    }

    saveScenario(){
      let mapped = Object.keys(this.field).map(key => ({name: key, value: this.field[key]}));
      this.apiService.getData(this.screenUrl.scenario.add_scenario, 
        {
          'id': this.received_workflow_id,
          'name': this.filename,
          'meta': JSON.stringify({"parameters":  mapped, "output_adaptors": this.checkedDataSet})
        }).subscribe(
        ()=>{
          this.ts._exception.emit(this.ts.getToastSuccessMessage(this.scContent.toastscSaveSuccess));
          this.cancelScenario();
        });
        
    }

    saveScenarioExecute() {

    }

    cancelScenario(){
      this.router.navigate(['scenario_list', this.screenId], {relativeTo: this.route.parent});
    }

    show: boolean = false; 
    outputModalShow: boolean = false;

    public modalDlgClose: EmitType<object> = () => {
      this.show = false; 
    }
  
    public modalDlgOpen: EmitType<object> = () => {
      this.overviewDialog.show();
      this.show = true; 
    }

    public outModalDlgClose: EmitType<object> = () => {
      this.outputDialog.hide();
      this.outputModalShow = false; 
    }
  
    public outModalDlgOpen: EmitType<object> = () => {
      this.outputDialog.show();
      this.outputModalShow = true; 
    }

    public buttons: Object[] = [{ click: this.outModalDlgClose.bind(this), buttonModel: { content: 'Done', isPrimary: true } }];

    public nodeCheck(args: NodeKeyPressEventArgs | NodeClickEventArgs): void {
      
      let getNodeDetails: any = this.outputtree.getNode(args.node);
      this.checkedNodes = this.outputtree.getAllCheckedNodes();
      if (getNodeDetails.isChecked == 'true' && this.checkedDataSet.indexOf(args.node.title) == -1) {
        this.checkedDataSet.push(args.node.title);
      } else if (getNodeDetails.isChecked == 'false') {
        this.checkedDataSet.splice(this.checkedDataSet.indexOf(args.node.title), 1);
      }
      this.chipds.refresh();
    }

    goBack() {
      this.router.navigate(['scenario_list/', this.screenId], {relativeTo: this.route.parent});
    }
}
