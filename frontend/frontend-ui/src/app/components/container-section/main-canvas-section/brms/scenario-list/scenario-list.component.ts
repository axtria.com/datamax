import { Component, OnInit, ViewChild } from '@angular/core';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { DataUtil } from '@syncfusion/ej2-data';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from 'src/app/shared/services/rest.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';
import { EmitType } from '@syncfusion/ej2-base';
import { LoaderService } from 'src/app/components/loader/loader.service';

@Component({
  selector: 'app-scenario-list',
  templateUrl: './scenario-list.component.html',
  styleUrls: ['./scenario-list.component.css']
})
export class ScenarioListComponent implements OnInit {

  wf_id;workflow_name;workflow_desc;
  public store_workflow_data = [];

  @ViewChild('grid', { static: false })
  public grid: GridComponent;
  slContent: any;
  show: boolean = false;
  screenUrl: any;
  screenId: string;
  public pageSettings: Object;
  public formatOptions: Object = { type: 'dateTime', format:'dd/MM/yyyy'  } ;
  isActive: boolean = false;

  getScenarioList() {
    this.connection_call_Service.getData(this.screenUrl.scenario.list_scenario,
      { 'id': this.wf_id }).subscribe(data => {
        this.isActive = false;
        this.store_workflow_data = DataUtil.parse.parseJson(data.Contents);
        this.ls.disableLoader = false;
      });
  }

  constructor(private connection_call_Service: RestService,
    private cs: ContentService, private route: ActivatedRoute, private router: Router,
    private ls: LoaderService
  ) {
    this.wf_id = sessionStorage.getItem('workflow_id') || '';
    this.slContent = this.cs.getContent(environment.modules.brms).SCENARIO_LIST;
    this.workflow_name = sessionStorage.getItem('workflow_name') || '';
    this.workflow_desc = sessionStorage.getItem('workflow_description') || '';
  }

  ngOnInit() {
    this.pageSettings = { pageSize: 8 };
    this.route.params.subscribe(routeParams => {
      this.screenId = routeParams.id;
      this.screenUrl = environment[this.screenId];
      this.getScenarioList();
    });
    
  }

  public modalDlgClose: EmitType<object> = () => {
    this.show = false; 
  }

  public modalDlgOpen: EmitType<object> = () => {
    this.show = true; 
  }

  /* getSwitchVal(status) {
    return status== 'Active';
  } */

  deactivate(id) {
    this.connection_call_Service.getData(this.screenUrl.scenario.deactivate_scenario,
      { 'id': id }).subscribe(() => {
        this.getScenarioList();
      });
  }

  openWf() {
    this.router.navigate(['workflow/', this.screenId], {relativeTo: this.route.parent});
  }

  viewScenario(id?) {
    id ? sessionStorage.setItem('scenarioId', id) : sessionStorage.removeItem('scenarioId');
    this.router.navigate(['scenario', this.screenId], {relativeTo: this.route.parent});
  }

  runScenario(id) {
    this.connection_call_Service.getData(this.screenUrl.scenario.run_scenario,
      { 'scenario_id': id , workflow_id: this.wf_id}).subscribe(() => {
        this.getScenarioList();
      });
  }

  onRefresh(){
    this.isActive = true;
    this.ls.disableLoader = true;
    this.getScenarioList();
  }

}
