import { Component, OnInit, Input } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  headerText: Object;
  @Input() public workflow_data : any;

  constructor(private cs : ContentService) { 
    this.headerText= [{ text: this.cs.getContent(environment.modules.brms).OVERVIEW.rules }, { text: this.cs.getContent(environment.modules.brms).OVERVIEW.visualize }];
  }

  ngOnInit() {
  }

}
