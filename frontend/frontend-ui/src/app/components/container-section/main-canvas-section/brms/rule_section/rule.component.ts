import { AccordionComponent, TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { Component, OnInit, ViewChild, Input, ViewEncapsulation  } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { DataService } from '../brms_data.service';
import { EditSettingsModel, ToolbarItems  } from '@syncfusion/ej2-angular-grids';
import { GridComponent } from '@syncfusion/ej2-angular-grids'
import { ToastPopupComponent } from 'src/app/components/toast-popup/toast-popup.component';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { RuleService } from './rule.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { EmitType } from '@syncfusion/ej2-base/src/base';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class RuleComponent implements OnInit {

  @ViewChild ('element',{static:false}) accordionObj: AccordionComponent;
  @ViewChild(ToastPopupComponent,{static:false}) toastObj:ToastPopupComponent;
  @ViewChild('modalDialog',{static: false}) public modalDialog: DialogComponent;
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;
  @ViewChild('parameterDialog',{static: false}) public parameterDialog: DialogComponent;
  @ViewChild('overviewDialog',{static: false}) public overviewDialog: DialogComponent;
  @ViewChild('deleterulewarning',{static:false}) public deleterulewarning:DialogComponent;
  @ViewChild('saverulewarning',{static:false}) public saverulewarning:DialogComponent;
  @ViewChild('tree',{static: false}) public tree: TreeViewComponent;
  @ViewChild('ruleInput',{static:false}) public userinput;
  @ViewChild('rulevar',{static:false}) public userinput1;
  caretPos: number = 0;
  public error_variable = {"error_switch":false,"error_message":null};
  public current_div_id=null;
  public itemsData: any = [];
  public value: string = '';
  public received_workflow_id;
  public received_parameter=[];
  public editSettings: EditSettingsModel;
  public toolbar: ToolbarItems[];
  public parameters= [];
  public rules_user_input='';
  public rule_name='';
  public config_type='';
  public config_value='';
  public output='';
  public help_text='';
  public Description='';
  public parameters_name=[];
  public data: object[];
  public field: Object = {};
  public rule_complete_data;
  public delete_rule_id='';
  public editruledetails: Object={};
  public only_single_rule_edit=false;
  // public right_kword=['song', 'name', 'poor']; // dont delete will be use in future
  subsVar: Subscription;
  tagList: any;
  pills = [];
  phrases: any;
  public selected: any;
  public isExpanded = true;
  public rule_paramter_data = [];
  @Input() public screenId : any;
  public isButtonElement: boolean;

  ruleContent: any;
  config_type_list: string[];
  type: string | number | boolean;
  name: any;
  public show: boolean = false;

  public modalDlgClose: EmitType<object> = () => {
    this.show = false; 
  }

  public modalDlgOpen: EmitType<object> = () => {
    this.show = true; 
  }
  workflow_data: any;
  screenUrl: any;
  pill_name: any;

  make_data_request() {
    this.show = false; 
    this.parameters=[];
    this.itemsData = [];
    this.apiService.getData(this.screenUrl.GET_WORKFLOW, {'id': this.received_workflow_id}).subscribe
      (data => {
        this.map_variable(data);
      })
  }

  reset_parameter(){
    this.current_div_id=null;
    this.error_variable["error_switch"]=false;
    this.error_variable["error_message"]=null;
    this.only_single_rule_edit=false;
  }

  map_variable(data){
    this.show = false; 
    this.parameters=[];
    this.itemsData = [];
    this.show = true; 
    this.itemsData = [];
    this.received_parameter = [];
    this.rules_user_input = '';
    this.rule_name = '';
    this.output = '';
    this.name='';
    this.help_text='';
    this.Description='';
    this.config_type='';
    this.config_value='';
    this.received_parameter=data.PARAMETERS;
    this.parameters=Object.values(this.received_parameter);
    for (let value in this.received_parameter) {
      this.parameters_name.push(value);
    }
    this.grid && this.grid.refresh();
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true };
    this.itemsData = this.ruleService.getRulesData(data.RULES);
    this.workflow_data={"rules" : data.RULES,"workflow_id":sessionStorage.getItem('workflow_id')};
    this.field = this.ruleService.make_tree_from_data_set(data.DATASETS);
    this.rule_complete_data=data.RULES;
    if(this.itemsData.length > 0) {
      this.isExpanded = false;
      this.selected = -1;
      this.phrases = [];
    }
  }
  
   setEditflag(flag) {
     // to diable the other edit button whtile editing rule.
    this.only_single_rule_edit=flag;
 }
  public paramshow: boolean = false; 

  public paraModalDlgClose: EmitType<object> = () => {
    this.paramshow = false; 
  }

  public paraModalDlgOpen: EmitType<object> = () => {
    this.paramshow = true; 
  }
  
  pmodalDlgOpen() {
    this.parameterDialog.show();
  }

  omodalDlgOpen() {
    this.overviewDialog.show();
  }

  // options = {
  //   values: (cb) => {
  //     cb(this.tagList);
  //   },
  //   autocompleteMode: true,
  //   lookup: 'value',
  //   fillAttr: 'value'
  // };
  
  addrules() {
    this.reset_parameter()
    this.apiService.getDataWithoutAlert(this.screenUrl.ADD_RULE, 
    {'rules': this.userinput.nativeElement.value, 'output': this.output, 'id': sessionStorage.getItem('workflow_id'),
     'rule_name': this.rule_name})
      .subscribe(data => {
        this.map_variable(data); //again making api call to refresh the data from database
        this.ts._exception.emit(this.ts.getToastSuccessMessage(this.ruleContent.toastruleAddSuccess));
        this.isExpanded = false;
        this.selected = -1;
        this.phrases = [];
      },
      error =>{
        this.error_variable["error_switch"]=true;
        this.error_variable["error_message"]=error;
      }
      )
  }

  getPills() {
    this.apiService.getData(this.screenUrl.GET_PILLS, {})
      .subscribe(data => {
        this.pills = data && data['Contents'] || [];
      });

  }

  selectPill(pillVal) {
      this.pill_name = pillVal.component_name;
      this.userinput.nativeElement.value = pillVal.phrases[0];
      this.phrases = pillVal.phrases;
  }

  selectPhrase(phraseVal) {
    this.userinput.nativeElement.value = phraseVal;
  }

  getCaretPos(oField) {
       if (oField.selectionStart || oField.selectionStart == '0') {
        this.caretPos = oField.selectionStart;
      }
    }
  
  fieldSelected(e) {
    if(e.event.srcElement.classList.contains('e-fullrow')){
      let _start = this.userinput.nativeElement.value.substring(0, this.caretPos);
      let _end = this.userinput.nativeElement.value.substring(this.caretPos);
      if(this.screenId == 'workflow' && this.pill_name != this.ruleContent['join'])
        this.userinput.nativeElement.value = _start + (e.node.title.split('.').length >1 ? e.node.title.split('.')[1] : e.node.title) + _end;
      else
        this.userinput.nativeElement.value = _start + e.node.title + _end;
    }
  };

   ruleDeletewarning(rule_id) {  
    sessionStorage.setItem("delete_rule_id",rule_id);
    this.deleterulewarning.show()
   }
   ruleDelete(){
    this.reset_parameter()
    this.deleterulewarning.hide()
    this.delete_rule_id=sessionStorage.getItem("delete_rule_id");
      this.apiService.getData(this.screenUrl.delete_rule, 
        {'rule_id': this.delete_rule_id,'workflow_id': sessionStorage.getItem('workflow_id')})
          .subscribe(data => {
            this.ts._exception.emit(this.ts.getToastSuccessMessage(this.ruleContent.toastruleDeleteSuccess));
            this.map_variable(data); 
          })//again making api call to refresh the data from database 
    this.deleterulewarning.hide()
    sessionStorage.removeItem("delete_rule_id");
  }
  dontdelterule(){
    this.deleterulewarning.hide()
    sessionStorage.removeItem("delete_rule_id");
  }

  SaveEditRule(id,name,output) {
    this.reset_parameter()
    this.editruledetails=JSON.parse(sessionStorage.getItem('edit_rule_details'));
    sessionStorage.removeItem("edit_rule_details");
    this.apiService.getDataWithoutAlert(this.screenUrl.edit_rule, 
          {'rule_id': id,'workflow_id': sessionStorage.getItem('workflow_id'),'rule_output':output,'rule_name':name,'user_input':this.userinput.nativeElement.value})
            .subscribe(data => {
              this.ts._exception.emit(this.ts.getToastSuccessMessage(this.ruleContent.toastruleModifiedSuccess));
              this.map_variable(data); 
            },
            error =>{
              this.only_single_rule_edit=true;
              this.error_variable["error_switch"]=true;
              this.error_variable["error_message"]=error;
            })  

  }
  dontSaveEditRule(){
    this.reset_parameter()
    sessionStorage.removeItem("edit_rule_details");
    this.make_data_request(); 
  }
  constructor(private apiService: RestService, private ds: DataService,
    private ruleService: RuleService, private ts: ToastPopupService, private cs: ContentService,
    private route: ActivatedRoute) {
      this.ruleContent = this.cs.getContent(environment.modules.brms).Rules;
    this.subsVar = this.ds.ruleSet.subscribe(() => {
        this.initialise();
      });
  }
  
  initialise() {
    this.screenId = this.route.snapshot.paramMap.get('id');
    this.screenUrl = environment[this.screenId];
    this.getPills();
    this.received_workflow_id = sessionStorage.getItem('workflow_id') || '';
    this.make_data_request();
    // this.getTags();
  }
  ngOnInit() {
   
   }
   
   ngOnDestroy() {
    if (this.subsVar) {
      this.subsVar.unsubscribe()
    }
  }

}