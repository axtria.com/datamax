import { Component, OnInit, ViewChild,ViewEncapsulation } from '@angular/core';
import { EmitType } from '@syncfusion/ej2-base';
import { RestService } from 'src/app/shared/services/rest.service';
import { GridComponent, RowSelectEventArgs } from '@syncfusion/ej2-angular-grids'
import { Router, ActivatedRoute } from '@angular/router';
import { ContentService } from 'src/app/shared/services/content.service';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { DialogComponent } from '@syncfusion/ej2-angular-popups/src/dialog/dialog.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WorkspaceComponent implements OnInit {

  public target: string = "#modalTarget";
  public workspace_dropdown_name: string = null;
  public content: string = "";
  public isModal: Boolean = true;
  public animationSettings: Object = { effect: 'Zoom' };
  public hide: any;
  public workspaceid;
  public workspace_name;
  public description;
  public connector_id;
  public length_of_workspace;
  public store_workspace_data = '';
  public editSettings;
  public toolbarItems;
  public delete_row_id;
  public pageSettings: Object;
  public update_flag: boolean = false;
  public customNameAttributes: Object = { class: 'clickcss' };
  public descp_length={'max_length':280,'remaining_text_length':''};
  // Connector drop properties in new workspace page START
  public workspace_dropdown: Object[] = [];
  public fields: Object = { text: 'name', value: 'id' };
  public workspace_dropdown_placeholder: string = 'Select a Connector * ';
  // Connector drop properties in new workspace page END

  @ViewChild('modalDialog', { static: false }) public modalDialog: DialogComponent;
  @ViewChild('confirmDialog', { static: false }) public confirmDialog: DialogComponent;
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;

  // On Button click, modal Dialog will be shown
  public modalBtnClick: EmitType<object> = () => {
    this.modalDialog.show();
  }
  // On Dialog close, 'New workspace' Button will be shown
  public modalDlgClose: EmitType<object> = () => {
    this.update_flag = false;
    document.getElementById('modalbtn').style.display = '';
    this.workspace_name = null;
    this.description = null
    sessionStorage.removeItem('connectorid');
    this.workspace_dropdown_name = null;
    this.descp_length['remaining_text_length']=null;
    (<HTMLFormElement>document.getElementById("template_driven")).reset();
  }
  // On Dialog open, 'New workspace' Button will be hidden
  public modalDlgOpen: EmitType<object> = () => {
    document.getElementById('modalbtn').style.display = 'none';
  }
  workspaceContent: any;
  Store_workflowid: any;
  received_workspace_id: any;
  screenUrl: any;
  workflow_name: string;

  public request_list_workspace() {
    this.connection_call_Service.getData(environment.workspace.LIST_WORKSPACE, {}).subscribe(listworkspace => {
      this.length_of_workspace = listworkspace && listworkspace.Contents && listworkspace.Contents.length || 0;
      this.store_workspace_data = listworkspace && listworkspace.Contents || []
      this.grid && this.grid.refresh();
    });
    this.connection_call_Service.getData(environment.workspace.LIST_CONNECTOR, {}).subscribe(connector_list => this.workspace_dropdown = connector_list['Contents'])
  }

  // Close the Dialog, while clicking "OK" Button of Dialog
  public Submit(): void {
    this.connection_call_Service.getData(environment.workspace.add_workspace,
      { 'workspace_name': this.workspace_name, 'description': this.description, 'connector_id': this.connector_id })
      .subscribe(() => {
        this.ts._exception.emit(this.ts.getToastSuccessMessage(this.workspaceContent.toastwsCreationSuccess));
        this.modalDialog.hide();
        this.request_list_workspace()
      });
  }

  edit_workspace(workspaceid, workspace_name, description, connector) {
    this.update_flag = true;
    this.modalDialog.show();
    this.workspace_name = workspace_name;
    this.description = description;
    this.workspace_dropdown_name = connector;
    this.workspaceid = workspaceid;
  }
  update_workspace() {
    this.connection_call_Service.getData(environment.workspace.edit_workspace, { 'workspace_id': this.workspaceid, 'workspace_name': this.workspace_name, 'description': this.description })
      .subscribe(() => {
        this.ts._exception.emit(this.ts.getToastSuccessMessage(this.workspaceContent.toastwsUpdateSuccess));
        this.modalDialog.hide();
        this.update_flag = false;
        this.request_list_workspace()
      })
  }
  
  clickaction(e) {
    let rowDataId = this.grid.getRowInfo(e.target).rowData['id'];
    if (e.target.title == this.workspaceContent.DELETE) {
      this.confirmDialog.show();
      this.delete_row_id = rowDataId;
    }

  }

  // ___________________ Feature depreciated ___________________
  // createworkflow(id, name) {
  //   sessionStorage.setItem('workspaceid', id);
  //   sessionStorage.setItem('workspace_name', name);
  //   sessionStorage.removeItem('workflow_id');
  //   this.router.navigate(['/features/', 'workflow_detail', 'workflow']);
  // }
  // ___________________ Feature depreciated ___________________ 
  
  // deleteing files
  delete_file() {
    this.connection_call_Service
      .getData(environment.workspace.deactivate_workspace, { 'workspace_id': this.delete_row_id })
      .subscribe(() => {
        this.ts._exception.emit(this.ts.getToastSuccessMessage(this.workspaceContent.toastwsDeleteSuccess));
        this.request_list_workspace();
        this.confirmDialog.hide();
        this.delete_row_id = null;
      });
  }
  dont_delete_file() {
    this.confirmDialog.hide();
  }

  rowSelected(args: RowSelectEventArgs) {
    if (args.target['cellIndex'] == 0) {
      let selectedrecords: Object[] = this.grid.getSelectedRecords();  // Get the selected records.
      sessionStorage.setItem('workspaceid', selectedrecords[0]['id']);
      sessionStorage.setItem('workspace_name', selectedrecords[0]['name']);
      sessionStorage.setItem('connectorid', selectedrecords[0]['connector_id']);
      this.router.navigate(['workflow/workflow'], {relativeTo: this.route.parent});
    }

  }

  public selected_dropdown(args: any): void {
    if (args.isInteracted) {
      this.connector_id = args.itemData['id']
    }
  }

  public desc_len_check(){
    this.descp_length['remaining_text_length']=this.workspaceContent.Remaining_Characters + String(this.descp_length['max_length']-this.description.length);
  }
  constructor(private connection_call_Service: RestService, 
    private router: Router, cs: ContentService,
    private ts: ToastPopupService, private route: ActivatedRoute){
    this.workspaceContent = cs.getContent(environment.modules.brms).WORKSPACE;
  }

  ngOnInit() {
    this.pageSettings = { pageSize: 8 };
    this.request_list_workspace();
  }

  ngAfterViewInit(): void {
    document.getElementById('modalbtn').focus();
  }
}