import { Component, OnInit, Input } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-rule-set',
  templateUrl: './rule-set.component.html',
  styleUrls: ['./rule-set.component.css']
})
export class RuleSetComponent implements OnInit {
  @Input() public workflow_data : any;
  rule_content:any;
  constructor(private cs : ContentService) { 
    this.rule_content = this.cs.getContent(environment.modules.brms).OVERVIEW;
  }

  ngOnInit() {
  }

}
