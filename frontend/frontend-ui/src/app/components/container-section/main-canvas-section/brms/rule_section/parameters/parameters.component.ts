import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { SaveEventArgs } from '@syncfusion/ej2-angular-grids';
import { GridComponent } from '@syncfusion/ej2-angular-grids'
import { ToastPopupComponent } from 'src/app/components/toast-popup/toast-popup.component';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.css']
})
export class ParametersComponent implements OnInit {
  @Output() childEvent = new EventEmitter();
  ParameterContent: any;
  make_data_request() {
    this.childEvent.emit('');
  }
  @Input() public parameters: any;
  @Input() public screenId: string;
  @ViewChild('gridelement', { static: false }) public grid: GridComponent;
  @ViewChild(ToastPopupComponent, { static: false }) toastObj: ToastPopupComponent;

  public paraData: any;
  public parameter_type_list: any;
  ruleContent: any;
  public selected_schema;
  public whole_data = [];
  public table_name = [];
  public schemalist = [];
  public editSettings: { allowEditing: boolean; allowAdding: boolean; allowDeleting: boolean; };
  public toolbar: string[];
  public filtered_column_to_display: any[];
  public filtered_table_to_display: any[];

  constructor(private apiService: RestService, private cs: ContentService) {
    this.ParameterContent = this.cs.getContent(environment.modules.brms).PARAMETERS;
    this.parameter_type_list = this.ParameterContent.ParameterTypeList;
  }

  empty() {

    this.paraData.config_value = '';
  }

  addParameterDef() {
    if (this.paraData.config_type == this.parameter_type_list[3]) {
      this.paraData['config_value'] = this.paraData['schema'] + ',' + this.paraData['table'] + ',' + this.paraData['column'];
    }
    this.apiService.getData(environment[this.screenId].add_parameter_definition,
      {
        'parameter_name': this.paraData.name, 'help_text': this.paraData.help_text, 'description': this.paraData.description, 'config_type': this.paraData.config_type, 'config_value': this.paraData.config_value,
        'workflow_id': sessionStorage.getItem('workflow_id')
      })
      .subscribe(() => {
        this.make_data_request(); //again making api call to refresh the data from database

      })
  }




  ngOnInit() {

    this.apiService.getData(environment[this.screenId].LIST_FILE_ADAPTER, { 'connector_id': sessionStorage.getItem("connectorid") }).subscribe(data => {
      this.whole_data = data.contents;
      this.whole_data.forEach((content) => this.schemalist.push(content.schema));
    });
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true };
    this.toolbar = this.ParameterContent.Toolbar;
  }

  selecting_columns(tablename) {
    console.log(tablename);
    console.log(this.selected_schema);
    var selectedSchemaHT = this.selected_schema;
    var filtered_columns = [];
    this.filtered_column_to_display = [];
    this.whole_data.forEach(function (item) {
      if (item.name === tablename && item.schema === selectedSchemaHT) {
        filtered_columns.push(item.adaptor_meta)
      }
    })
    filtered_columns.forEach(columnArray => {
      columnArray.forEach(x => {
        this.filtered_column_to_display.push(x.column_name);
      });
    });
  }

  selecting_tables(schema) {
    this.selected_schema = schema;
    //var filtered_tables = [];
    this.filtered_table_to_display = [];
    this.whole_data.forEach(item => {
      if (item.schema == schema) {
        this.filtered_table_to_display.push(item.name)
      }
    })
  }



  actionBegin(args: SaveEventArgs) {
    if (args.requestType === 'beginEdit' || args.requestType === 'add') {
      this.paraData = Object.assign({}, args.rowData);
      this.paraData.isEditable = true;
      console.log('args...', args);
      console.log(this.paraData);
      if (this.paraData['config_type'] == this.parameter_type_list[3] && this.paraData['config_value'] != '') {
        var _values = args.rowData['config_value'].split(' , ');
        this.paraData['schema'] = _values[0]
        this.paraData['table'] = _values[1];
        this.paraData['column'] = _values[2];

      }

    }
    if (args.requestType === 'save') {
      // cast string to integer value.
      delete this.paraData.isEditable;
      args.data['config_type'] = this.paraData['config_type'];
      args.data['help_text'] = this.paraData['help_text'];
      args.data['description'] = this.paraData['description'];
      if (this.paraData.config_type == this.parameter_type_list[3])
        this.paraData['config_value'] = this.paraData['schema'] + ' , ' + this.paraData['table'] + ' , ' + this.paraData['column'];
      args.data['config_value'] = this.paraData['config_value'];


    }
  }


}
