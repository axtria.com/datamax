import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RestService} from 'src/app/shared/services/rest.service';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { EmitType } from '@syncfusion/ej2-base/src/base';
import { DialogComponent } from '@syncfusion/ej2-angular-popups/src/dialog/dialog.component';
import { environment } from 'src/environments/environment';
import { DataUtil } from '@syncfusion/ej2-data';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.css']
})
export class WorkflowComponent implements OnInit {
  received_workspace_id; workflow_name;
  public store_workflow_data = [];
  public confirmWidth: string = '400px';
  @ViewChild('grid', { static: false }) public grid: GridComponent;
  wfContent: any;
  show: boolean = false;
  workflow_data : any ={"rules":null,"workflow_id":null};
  @ViewChild('overviewDialog', { static: false }) public overviewDialog: DialogComponent;
  @ViewChild('deletewarning', { static: false }) public deletewarning: DialogComponent;
  screenUrl: any;
  data: {};
  screenId: string;
  public delete_row_id = null;
  public pageSettings: Object;
  public formatOptions: Object = { type: 'dateTime', format:'dd/MM/yyyy hh:mm:ss a'  } ;
  public animationSettings: Object = { effect: 'Zoom' };

  clickaction(e) {
    if (e.target.title == this.wfContent.overview) {
      this.workflow_data = {"rules":this.grid.getRowInfo(e.target).rowData[this.screenId + '_meta']['RULES'],"workflow_id":this.grid.getRowInfo(e.target).rowData['id']};
      this.overviewDialog.show();
    } 
    else if (e.target.title == this.wfContent.Publish) {
      let rowDataId = this.grid.getRowInfo(e.target).rowData['id'];
      this.connection_call_Service
        .getData(this.screenUrl.WORKFLOW_PUBLISH, { id: rowDataId})
        .subscribe(() => {
          this.ts._exception.emit(this.ts.getToastSuccessMessage(this.wfContent.toastwfPublishedSuccess));
          this.get_workflow();
        });
    }
    else if (e.target.title == this.wfContent.Edit) {
      let rowDataId = this.grid.getRowInfo(e.target).rowData['id'];
      this.editworkflow(rowDataId);
    }
    else if (e.target.title == this.wfContent.Duplicate) {
      let rowDataId = this.grid.getRowInfo(e.target).rowData['id'];
      this.connection_call_Service
        .getData(this.screenUrl.WORKFLOW_DUPLICATE, { id: rowDataId})
        .subscribe(() => {
          this.ts._exception.emit(this.ts.getToastSuccessMessage(this.wfContent.toastwfDuplicatedSuccess));
          this.get_workflow();
        });
    }
    else if (e.target.title == this.wfContent.Delete) {
      this.deletewarning.show()
      this.delete_row_id = this.grid.getRowInfo(e.target).rowData['id'];
    }
    else if (e.target.title == this.wfContent.Create_Scenario) {
      let rowDataId = this.grid.getRowInfo(e.target).rowData['id'];
      sessionStorage.setItem('workflow_id', rowDataId);
      sessionStorage.setItem('workflow_name', this.grid.getRowInfo(e.target).rowData['name']);
      sessionStorage.setItem('workflow_description', this.grid.getRowInfo(e.target).rowData['description']);
      sessionStorage.removeItem('scenarioId');
      this.router.navigate(['scenario/', this.screenId], {relativeTo: this.route.parent});
    }
    else if (e.target.title == this.wfContent.Scenario_List) {
      let rowDataId = this.grid.getRowInfo(e.target).rowData['id'];
      sessionStorage.setItem('workflow_id', rowDataId);
      sessionStorage.setItem('workflow_name', this.grid.getRowInfo(e.target).rowData['name']);
      sessionStorage.setItem('workflow_description', this.grid.getRowInfo(e.target).rowData['description']);
      this.router.navigate(['scenario_list/', this.screenId], {relativeTo: this.route.parent});
    }

  }

  openWs() {
    this.router.navigate(['brms'], {relativeTo: this.route.parent});
  }

  createworkflow() {
    sessionStorage.removeItem('workflow_id');
    this.router.navigate(['workflow_detail', this.screenId], {relativeTo: this.route.parent});
  }

  editworkflow(workflowid) {
    this.router.navigate(['workflow_detail', this.screenId], {relativeTo: this.route.parent});
    sessionStorage.setItem('workflow_id', workflowid);
  }

  rowSelected(id) {
    this.editworkflow(id);
  }

  get_workflow() {
    this.connection_call_Service.getData(this.screenUrl.WORKFLOW_LIST,
      { 'workspaceId': this.received_workspace_id }).subscribe(fetchallconnection => {
        this.store_workflow_data = DataUtil.parse.parseJson(fetchallconnection.Contents)
      });
      this.grid && this.grid.refresh();
  }

  // deleteing worflows
  delete_file() {
    this.connection_call_Service
      .getData(this.screenUrl.WORKFLOW_DELETE, { id: this.delete_row_id })
      .subscribe(() => {
        this.ts._exception.emit(this.ts.getToastSuccessMessage(this.wfContent.toastwfDeleteSuccess));
        this.get_workflow();
      });
      this.deletewarning.hide();
      this.delete_row_id = null;
  }
  dont_delete_file() {
    this.deletewarning.hide();
  }

  constructor(private router: Router,
    private connection_call_Service: RestService,
    private ts: ToastPopupService, private cs: ContentService, private route: ActivatedRoute
  ) {
    this.received_workspace_id = sessionStorage.getItem('workspaceid') || '';
    this.wfContent = this.cs.getContent(environment.modules.brms).WORKFLOW;
    this.workflow_name = sessionStorage.getItem('workspace_name') || '';
  }

  ngOnInit() {
    this.pageSettings = { pageSize: 8 };
    this.route.params.subscribe(routeParams => {
      this.screenId = routeParams.id;
      this.screenUrl = environment[this.screenId];
      this.get_workflow();
    });

  }

  public modalDlgClose: EmitType<object> = () => {
    this.show = false;
  }

  public modalDlgOpen: EmitType<object> = () => {
    this.show = true;
  }

}
