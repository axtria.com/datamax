import { Injectable } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { tap, mergeMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InputService {

  constructor(private apiService: RestService) { }

  createOrSaveWf(id, screenId, filename, description, adapterlist){
    if(!id)  // creating new workflow and adding meta data
    {

      let received_workspace_id = sessionStorage.getItem('workspaceid') || '';
      return this.apiService.getData(environment[screenId].WORKFLOW_CREATE,{ 'workspaceId': received_workspace_id })
      .pipe(
        tap(workflow_id => {
          sessionStorage.setItem('workflow_id', workflow_id['workflow_id']);
        }),
        mergeMap(() => this.apiService.getData(
          environment[screenId].METADATA_ADD, 
          { 'id': (sessionStorage.getItem('workflow_id') || ''), 'name': filename ,
            'description': description, 'selected_datasets': adapterlist
          })
        
      ));

    }
    else // only adding meta data to existing workflow
    {
      return this.apiService.getData(environment[screenId].METADATA_ADD, 
      { 'id': (id || ''), 'name': filename ,
        'description': description, 'selected_datasets': adapterlist});
    }
  }

  getFormattedAdapterList(selected_datasets) {
    let actualadapterlistold = [];
    Object.keys(selected_datasets).forEach((key) => {
        actualadapterlistold.push({"name":key, "adaptor_meta":selected_datasets[key]['meta'], "schema":selected_datasets[key]['schema']})
    });
    return actualadapterlistold;
  }

}
