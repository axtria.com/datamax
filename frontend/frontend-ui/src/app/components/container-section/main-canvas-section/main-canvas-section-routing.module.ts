import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataCatalogueComponent } from './data-catalogue/data-catalogue.component';
import { MainOnboardComponent } from './data-onboarding/main-onboard/main-onboard.component';
import { OnboardedDataDetailsComponent } from './onboarded-data/onboarded-data-details/onboarded-data-details.component';
import { BrmsComponent } from './brms/brms.component';
import { WorkflowComponent } from './brms/workflow/workflow.component';
import { WorkflowDetailComponent } from './brms/workflow-detail/workflow-detail.component';
import { MainOnboardedDataComponent } from './onboarded-data/main-onboarded-data/main-onboarded-data.component';
import { MainDataIngestionComponent } from './data-ingestion/main-data-ingestion/main-data-ingestion.component';
import { MainDataQualityComponent } from './data-quality/main-data-quality/main-data-quality.component';
import { DetailedCatalogViewComponent } from './data-catalogue/detailed-catalogue/detailed-catalog-view/detailed-catalog-view.component';
import { DataAssetDetailedViewComponent } from './data-catalogue/detailed-catalogue/data-asset/data-asset-detailed-view/data-asset-detailed-view.component';
import { ScenarioComponent } from './brms/scenario/scenario.component';
import { ScenarioListComponent } from './brms/scenario-list/scenario-list.component';
import { environment } from 'src/environments/environment';
import {DataResolverService} from 'src/app/shared/services/content-data-resolver.service';
import { RoleGuard } from 'src/app/shared/services/role.guard';
import { NotificationsViewAllComponent } from './notifications-view-all/notifications-view-all.component';
import { AuthGuard } from 'src/app/shared/services/auth.guard';
import { ObjectRulesInformationComponent } from './data-quality/main-data-quality/object-rules-information/object-rules-information/object-rules-information.component';
//import { UserDetailsComponent } from '../../admin/usermanagement/users/user-details/user-details.component';
//import { GroupDetailComponent } from '../../admin/usermanagement/groups/group-detail/group-detail.component';

const routes: Routes = [ 
  { path: 'notifications', component: NotificationsViewAllComponent, data: {module: environment.modules.notifications} },
  { path: 'dataCatalogue', component: DataCatalogueComponent, canActivate: [RoleGuard], data: {module: environment.modules.catalog} },
  { path: 'dataOnboarding', component: MainOnboardComponent, data: {module: environment.modules.dataOnboarding} },
  { path: 'dataOnboarding/:id', component: MainOnboardComponent, data: {module: environment.modules.dataOnboarding} },
  { path: 'onboardedDataDetails/:id', component: OnboardedDataDetailsComponent, data: {module: environment.modules.dataOnboarding} },
  { path: 'brms', component: BrmsComponent, data: {module: environment.modules.brms} },
  //{ path: 'myprofile', component:  UserDetailsComponent, data: {module: environment.modules.admin} },
  //{ path : 'groupdetails', component: GroupDetailComponent , data: {module: environment.modules.admin} },
  { path: 'workflow/:id', component: WorkflowComponent, data: {module: environment.modules.brms} },
  { path: 'workflow_detail/:id', component: WorkflowDetailComponent, data: {module: environment.modules.brms} },
  { path: 'onboardedData', component: MainOnboardedDataComponent, data: {module: environment.modules.dataOnboarding} },
  { path: 'dataIngestion', component: MainDataIngestionComponent, data: {module: environment.modules.dataOnboarding}},
  { path: 'dataQuality', component: MainDataQualityComponent, data: {module: environment.modules.dataQuality} },
  { path: 'detailedCatalogView', component: DetailedCatalogViewComponent, canActivate: [RoleGuard], data: {module: environment.modules.catalog} },
  { path: 'detaileddataassetView', component: DataAssetDetailedViewComponent, canActivate: [RoleGuard], data: {module: environment.modules.catalog} },
  { path : 'scenario_list/:id', component: ScenarioListComponent, resolve: [DataResolverService ], data: {module: environment.modules.brms}},
  { path : 'scenario/:id', component: ScenarioComponent, resolve: [DataResolverService ], data: {module: environment.modules.brms}},
  //{ path : 'userdetails/:type', component: UserDetailsComponent, data: {module: environment.modules.admin}},
  { path: 'admin', canActivate: [AuthGuard], canLoad: [RoleGuard],
      loadChildren: () => import('../../../components/admin/admin.module').then(m => m.AdminModule), 
      data: { preload: true, module: environment.modules.admin } , resolve:[DataResolverService]
    },
  { path: 'objectRulesInfo/:id', component: ObjectRulesInformationComponent, data: {module: environment.modules.dataQuality} }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainCanvasSectionRoutingModule { }
