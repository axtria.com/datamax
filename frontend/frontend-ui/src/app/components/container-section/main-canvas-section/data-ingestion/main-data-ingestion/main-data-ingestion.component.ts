import { Component, OnInit, ViewChild } from '@angular/core';
import { DataLayoutDetailsComponent } from '../../data-onboarding/data-layout-details/data-layout-details.component';
import { DataIngestionDetailsComponent } from '../data-ingestion-details/data-ingestion-details.component';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { forkJoin } from 'rxjs';
import { ContentService } from 'src/app/shared/services/content.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-main-data-ingestion',
  templateUrl: './main-data-ingestion.component.html',
  styleUrls: ['./main-data-ingestion.component.css']
})
export class MainDataIngestionComponent implements OnInit {
  mainIngestionContent: any;

  public headerText: object;

  @ViewChild('dataIngestionDetails', { static: false }) objDataIngestionDetails: DataIngestionDetailsComponent;
  @ViewChild('dataLayoutDetails', { static: false }) objDataLayoutDetailsComponent: DataLayoutDetailsComponent;

  constructor(private toastPopupService: ToastPopupService, cs:ContentService) {
    this.mainIngestionContent = cs.getContent(environment.modules.dataOnboarding).mainIngestion;
    this.headerText=this.mainIngestionContent.headerText;
   }

  ngOnInit() {
  }

  pullDataLayoutDetails(args: any[]) {
    if (typeof (this.objDataLayoutDetailsComponent) !== 'undefined') {
      this.objDataLayoutDetailsComponent.fetchDataLayoutDetails(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10],args[11]);
    }
  }

  saveDataIngestionDetails() {
    let formsValid = true;

    if (!this.objDataIngestionDetails.dataIngestionDetailsForm.valid) {
      formsValid = false;
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.mainIngestionContent.ingestionError));
    }

    if (typeof (this.objDataLayoutDetailsComponent) === 'undefined' || !this.objDataLayoutDetailsComponent.dataLayoutForm.valid) {
      formsValid = false;
      this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.mainIngestionContent.layoutError));
    }

    if (formsValid) {
      if (this.objDataIngestionDetails.activeLayoutOption === this.objDataIngestionDetails.layoutOptions[1]) {
        this.objDataIngestionDetails.saveDataIngestionDetails()
        .subscribe(data => {
          this.objDataIngestionDetails.dataIngestionDetails.obj_id = data && data.Contents ? data.Contents[0].obj_id : "";           
          forkJoin([this.objDataIngestionDetails.saveDataIngestionExternalTableDetails(this.objDataIngestionDetails.dataIngestionDetails.obj_id),  
          this.objDataIngestionDetails.saveConnectionDetails('add_conn_meta', this.objDataIngestionDetails.dataIngestionDetails.obj_id),
          this.objDataIngestionDetails.registerDataFile(this.objDataIngestionDetails.dataIngestionDetails.obj_id),
          this.objDataLayoutDetailsComponent.saveDataLayoutDetails(this.objDataIngestionDetails.dataIngestionDetails.obj_id, this.objDataIngestionDetails.dataIngestionDetails.obj_uri_id)])
          .subscribe(results=>{
            this.objDataIngestionDetails.fileInstanceId = results && results.length>0 ? results[2].Contents[0].file_inst_id : "";
            this.objDataLayoutDetailsComponent.saveDataLayoutDetails(results[0].Contents[0].obj_id, results[0].Contents[0].obj_uri_id)
            .subscribe(()=>{
              this.objDataIngestionDetails.createExternalTable(this.objDataIngestionDetails.dataIngestionDetails.obj_id)
              .subscribe(()=>{
                this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.mainIngestionContent.ingestionSaved));
              })      
            })
          })
        });
      } else {
        this.objDataIngestionDetails.saveConnectionDetails('update_conn_meta', this.objDataIngestionDetails.dataIngestionDetails.obj_id);
      }
    }
  }
}
