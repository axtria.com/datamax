import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { DataIngestionDetails } from '../DataIngestionDetails';
// import { S3FileOperationsService } from 'src/app/shared/services/S3FileOperations.service';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { RestService } from 'src/app/shared/services/rest.service';
import { StorageDetails } from 'src/app/components/container-section/main-canvas-section/data-onboarding/StorageDetails';
import { DialogComponent, ButtonPropsModel } from '@syncfusion/ej2-angular-popups';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { CheckBox } from '@syncfusion/ej2-buttons/src/check-box/check-box';
// import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';
import { NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-data-ingestion-details',
  templateUrl: './data-ingestion-details.component.html',
  styleUrls: ['./data-ingestion-details.component.css']
})
export class DataIngestionDetailsComponent implements OnInit {
  ingestionDetailsContent: any;

  tgtSndBxOptions: any[];
  activeTgtSndBxOption: string;
  // checkedSandbox = true;
  layoutOptions: string[] = ['Existing Layout', 'New Layout'];
  activeLayoutOption: string;
  // sameLayout = true;
  dataSourceOptions: any[];
  activeDataSourceOption: string;
  // dataSource = true;
  layoutDisplayOptions: string[] = ['None', 'MasterObject', 'OnboardedObject', 'File'];
  activeLayoutDisplayOption: string;

  @ViewChild('sourceConnection', { static: false }) sourceConnection: DropDownListComponent;
  @ViewChild('fileType', { static: false }) fileType: DropDownListComponent;
  @ViewChild('fileExtension', { static: false }) fileExtension: DropDownListComponent;
  @ViewChild('fileColumnDelimiter', { static: false }) fileColumnDelimiter: DropDownListComponent;
  @ViewChild('Dialog', { static: false }) Dialog: DialogComponent;
  @ViewChild('grid', { static: false }) grid: GridComponent;
  @ViewChild('s3UploadDialog', { static: false }) s3UploadDialog: DialogComponent;
  @ViewChild('fileUpload', { static: false }) FileUpload: ElementRef;
  @ViewChild('s3Chosen', { static: false }) s3Chosen: CheckBox;
  @ViewChild('localChosen', { static: false }) localChosen: CheckBox;
  @ViewChild('dataIngestionDetailsForm', { static: false }) dataIngestionDetailsForm: NgForm;

  fileTypeList: [];
  fileTypeFields: object = { text: 'obj_nm', value: 'ref_obj_id' };
  connectionTypes: any[];
  sourceConnectionFields: object = { text: 'conn_name', value: 'conn_id' };
  connDetails: any[];
  sourceConnectionFilefields: object = { value: 'Path' };
  fileExtensionList: any[] = [{ ext_val: 'csv' }, { ext_val: 'psv' }, { ext_val: 'txt' }, { ext_val: 'xls' }, { ext_val: 'xlsx' }];
  fileExtensionFields: object = { value: 'ext_val' };
  fileColumnDelimiterList: any[];
  fileColumnDelimiterFields: object = { text: 'del_txt', value: 'del_val' };
  fileTextQualifierList: any[];
  fileTextQualifierFields: object = { text: 'qual_txt', value: 'qual_val' };
  fileEncodingList: any[];
  fileEncodingFields: object = { value: 'encoding_type' };
  allowedExtensions = '.csv,.psv,.txt,.xls,.xlsx';
  fileTypeNameValid = true;
  animationSettings: object = { effect: 'Zoom' };

  dataIngestionDetails: DataIngestionDetails = {
    obj_id: null, obj_uri_id: null, file_type_name: null, business_purpose: null, sample_file_name: null,
    file_extension: null, file_column_delemiter: null, file_text_qualifier: null, file_encoding: null, last_modified_time: null
  };

  sourceConnectionDetails: StorageDetails = { obj_data_layer_id: null, data_layer_id: null, obj_id: null, abslt_path: null, user_id: null, user_type: null, layer_flg: null, eff_srt_dt: null, eff_end_dt: null, is_active: null,
    file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null};

  fileInstanceId: number;
  selectedFiles: FileList;
  selectedFileName: string;
  public sampleData: any[];
  public sampleDataColumns: any[];
  fileColumnDelemiter: string;
  samples_file_upload_S3_connector_id = 1;
  folderPrefix = "OnboardingSampleFiles";
  isUploadSuccessFull = false;
  isUploading = false;
  isUploadConfirmed = false;
  modalHidden = false;
  s3UploadmodalHidden = false;
  dlgButtons: ButtonPropsModel[];
  dlgButtonss3Upload: ButtonPropsModel[];


  @Output() dataIngestionDetailsSaved = new EventEmitter<number[]>();
  @Output() dataFileSelected = new EventEmitter<any[]>();
  dataFeedDetails: any;

  constructor(private authenticateService: AuthenticateService, private dataService: RestService, cs: ContentService) {
    this.ingestionDetailsContent = cs.getContent(environment.modules.dataOnboarding).ingestionDetails;
    this.tgtSndBxOptions = this.ingestionDetailsContent.tgtSndBxOptions;
    this.dataSourceOptions = this.ingestionDetailsContent.dataSourceOptions;
    this.fileTextQualifierList = this.ingestionDetailsContent.fileTextQualifierList;
    this.fileEncodingList = this.ingestionDetailsContent.fileEncodingList;
    this.dlgButtons = [
      { click: this.dlgRejectBtnClick.bind(this, 'preview'), buttonModel: { content: this.ingestionDetailsContent.dataFile.buttons.reject, isPrimary: true, cssClass: 'btn btn-outline-warning' } },
      { click: this.dlgConfirmBtnClick.bind(this), buttonModel: { content: this.ingestionDetailsContent.dataFile.buttons.confirm, isPrimary: true, cssClass: 'btn btn-outline-warning' } }
    ];

    this.dlgButtonss3Upload = [
      { click: this.dlgRejectBtnClick.bind(this, 's3Upload'), buttonModel: { content: this.ingestionDetailsContent.dataFile.buttons.reject, isPrimary: true, cssClass: 'btn btn-outline-warning' } },
      { click: this.uploadSampleFile.bind(this, 'True'), buttonModel: { content: this.ingestionDetailsContent.dataFile.buttons.confirm, isPrimary: true, cssClass: 'btn btn-outline-warning' } }
    ];
  }

  ngOnInit() {
    this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[0] : "";
    this.activeTgtSndBxOption = this.tgtSndBxOptions.length > 0 ? this.tgtSndBxOptions[0].text : "";
    this.activeLayoutOption = this.layoutOptions.length > 0 ? this.layoutOptions[0] : "";
    this.activeDataSourceOption = this.dataSourceOptions.length > 0 ? this.dataSourceOptions[0].text : "";

    this.dataService.getData(environment.ingestion.getFileTypeList, {
      name: 'get_objects', obj_layer_nm: 'External'
    })
      .subscribe(data => {
        this.fileTypeList = data && data.Contents ? data.Contents : [];
        // this.fileTypeList = data.Contents.filter(function (item: any) {
        //   if (item.provider !== 'IQVIA') {
        //     return item;
        //   }
        // });
      });

    this.dataService.getData(environment.commonUtilities.getTypes,
      { name: 'get_connection_type' }).subscribe(data => { this.connectionTypes = data && data.Contents ? data.Contents : []; });
  }

  toggleLayoutOption() {

    this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[0] : "";
    this.dataFileSelected.emit([this.activeLayoutDisplayOption, null, null, null, null, null, null, null]);

    this.dataIngestionDetails = {
      obj_id: null, obj_uri_id: null, file_type_name: null, business_purpose: null, sample_file_name: null,
      file_extension: null, file_column_delemiter: null, file_text_qualifier: null, file_encoding: null, last_modified_time: null
    };

    this.sourceConnectionDetails = { obj_data_layer_id: null, data_layer_id: null, obj_id: null, abslt_path: null, user_id: null, user_type: null, layer_flg: null, eff_srt_dt: null, eff_end_dt: null, is_active: null,
      file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null};

    this.setupNewFileUpload();
  }

  validateFileTypeName(e: any) {
    if (e.value.trim() !== '') {
      this.dataService.getData(environment.commonUtilities.checkFileStatus, { name: 'get_file_name_status', obj_nm: e.value })
        .subscribe(data => {
          this.fileTypeNameValid = data && data.Contents && data.Contents.length > 0 ? (data.Contents[0].message !== this.ingestionDetailsContent.fileUniqueError) : false;
        });
    }
  }

  fileTypeChange(e: any) {
    this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[2] : "";
    this.dataIngestionDetails.file_type_name = e.itemData.obj_nm;
    if (this.fileType) {
      setTimeout(() => {
        this.dataService.getData(environment.commonUtilities.getDataFeedDetails, { name: 'get_data_feed_details', obj_id: this.fileType.value })
          .subscribe(data => {
            this.dataFeedDetails = data && data.Contents && data.Contents[0] ? data.Contents[0] : {};
            this.fileColumnDelemiter = this.dataFeedDetails.obj_delimiter;
            this.dataIngestionDetails.file_extension = this.dataFeedDetails.obj_extension;
            this.dataIngestionDetails.file_text_qualifier = this.dataFeedDetails.obj_text_qualifier ? data.Contents[0].obj_text_qualifier : '--';
            this.dataIngestionDetails.file_encoding = this.dataFeedDetails.obj_encoding;
            this.dataIngestionDetails.business_purpose = this.dataFeedDetails.obj_desc;
            this.allowedExtensions = '.' + this.dataFeedDetails.obj_extension;
            this.dataFileSelected.emit([this.activeLayoutDisplayOption, this.fileType.value, null, null, null, null, null, null]);
          });
      });
    }
  }

  selectSampleFile(e: any) {
    this.selectedFiles = e.target.files;

    // To be reviewed
    this.sourceConnectionDetails.data_layer_id = this.samples_file_upload_S3_connector_id;
    this.sourceConnectionDetails.abslt_path = (this.connectionTypes.find(item => item.data_layer_id === this.sourceConnectionDetails.data_layer_id)).folder_prefix;
  }

  uploadSampleFile(override) {
    if (override) {
      this.s3UploadDialog.hide();
    }
    this.isUploading = true;
    this.FileUpload.nativeElement.disabled = true;

    this.selectedFileName = this.selectedFiles.item(0).name;

    let formData = new FormData();
    formData.append('file', this.FileUpload.nativeElement.files.item(0));
    this.dataService.postData(environment.commonUtilities.uploads3File, {
      s3_connector_id: this.samples_file_upload_S3_connector_id,
      override: override
    }, formData)
      .subscribe(data => {
        if (data['Status'] == 'Success') {
          this.dataService.getData(environment.commonUtilities.getFileProperties, {
            s3_connector_id: this.samples_file_upload_S3_connector_id,
            file_name: this.selectedFileName
          })
            .subscribe(element => {
              if (this.activeLayoutOption === this.layoutOptions[0]) {
                // Validate if file properties complies with chosen existing layout
                // this.dataIngestionDetails.file_encoding.toUpperCase() === data.encoding.toUpperCase();
                // this.dataIngestionDetails.file_column_delemiter === data.delimiter;
                // this.dataIngestionDetails.file_text_qualifier.toUpperCase() === data.text_qualifier.toUpperCase();
                // this.dataIngestionDetails.file_extension.toUpperCase() === data.file_extension.toUpperCase();
              } else {
                this.dataIngestionDetails.file_encoding = element ? element.encoding.toUpperCase() : "";
                this.fileColumnDelemiter = element ? element.delimiter : "";
                this.dataIngestionDetails.file_text_qualifier = element ? element.text_qualifier.toUpperCase() === 'NONE' ?
                  '--' : element.text_qualifier : "";
                this.dataIngestionDetails.file_extension = element ? element.file_extension.toLowerCase() : "";
                this.dataIngestionDetails.last_modified_time = element ? element.last_modified_time.toString().split('+')[0] : "";
              }
            });
          this.isUploadSuccessFull = true;
          this.isUploading = false;
        }
        else {
          this.s3UploadDialog.show();
        }
      });
  }

  connectionSelected(e: any) {
    if (e.itemData) {
      // setTimeout(() => {
      this.dataService.getData(environment.commonUtilities.getFilesForConnection,
        { s3_connector_id: this.sourceConnection.value, category: 'Files', extension: this.allowedExtensions })
        .subscribe(data => { this.connDetails = data.Contents; });
      // });
    }
  }

  s3FileSelected(args: any) {
    if (args.value === null) {
      this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[0] : "";
      this.fileColumnDelemiter = null;
      this.dataIngestionDetails.file_extension = null;
      this.dataIngestionDetails.file_column_delemiter = null;
      this.dataIngestionDetails.file_text_qualifier = null;
      this.dataIngestionDetails.file_encoding = null;
      this.dataFileSelected.emit([this.activeLayoutDisplayOption, null, null, null, null, null, null, null]);
    } else {
      this.sourceConnectionDetails.data_layer_id = this.sourceConnection.value as number;
      const ConnFolderPrefix = (this.connectionTypes.find(item => item.data_layer_id === this.sourceConnectionDetails.data_layer_id)).folder_prefix;
      const fullFileName = args.value;
      // let fileName;
      let filePath;
      // let connectionPath;
      if (fullFileName != null) {
        filePath = fullFileName.toString().replace(ConnFolderPrefix, '').replace(/\//g, ' ').trim().replace(/ /g, '/');
        // fileName = filePath.toString().split('/').pop();
        // connectionPath = fullFileName.replace(fileName, '').replace(/\//g, ' ').trim().replace(/ /g, '/');
      }
      // this.sourceConnectionDetails.connection_path = connectionPath;
      this.selectedFileName = filePath;

      this.dataService.getData(environment.commonUtilities.getFileProperties, {
        s3_connector_id: this.sourceConnectionDetails.data_layer_id,
        file_name: filePath
      })
        .subscribe(data => {
          if (this.activeLayoutOption === this.layoutOptions[0]) {
            // Validate if file properties complies with chosen existing layout
            // thisObj.dataIngestionDetails.file_encoding.toUpperCase() === data.encoding.toUpperCase();
            // thisObj.dataIngestionDetails.file_column_delemiter === data.delimiter;
            // thisObj.dataIngestionDetails.file_text_qualifier.toUpperCase() === data.text_qualifier.toUpperCase();
            // thisObj.dataIngestionDetails.file_extension.toUpperCase() === data.file_extension.toUpperCase();
          } else {
            this.dataIngestionDetails.file_encoding = data ? data.encoding.toUpperCase() : "";
            this.fileColumnDelemiter = data ? data.delimiter : "";
            this.dataIngestionDetails.file_text_qualifier = data ? data.text_qualifier.toUpperCase() === 'NONE' ?
              '--' : data.text_qualifier : "";
            this.dataIngestionDetails.file_extension = data ? data.file_extension.toLowerCase() : "";
            this.dataIngestionDetails.last_modified_time = data ? data.last_modified_time.toString().split['+'][0] : "";
          }

          this.isUploadSuccessFull = true;
          this.isUploading = false;
        });
    }
  }

  onSampleFilePreview() {
    this.dataService.getData(environment.commonUtilities.getPreviewData, {
      s3_connector_id: this.sourceConnectionDetails.data_layer_id,
      file_name: this.selectedFileName,
      delimiter: this.dataIngestionDetails.file_column_delemiter === '--' ? null : this.dataIngestionDetails.file_column_delemiter,
      text_qualifier: this.dataIngestionDetails.file_text_qualifier === '--' ? 'None' : this.dataIngestionDetails.file_text_qualifier
    }).subscribe(data => {
      this.sampleData = data.content;
      this.sampleDataColumns = data.columns;
      this.grid.columns = [];
      this.Dialog.show();
    });
  }

  dataBound() {
    this.grid.autoFitColumns(this.sampleDataColumns);
  }

  replaceSelectedFile() {
    if (this.activeLayoutOption === this.layoutOptions[1]) {
      this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[0] : "";
      this.dataFileSelected.emit([this.activeLayoutDisplayOption, null, null, null, null, null, null, null]);
    }

    this.sourceConnectionDetails = { obj_data_layer_id: null, data_layer_id: null, obj_id: null, abslt_path: null, user_id: null, user_type: null, layer_flg: null, eff_srt_dt: null, eff_end_dt: null, is_active: null,
      file_frequency: null, file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null};

    this.setupNewFileUpload();
  }

  setupNewFileUpload() {
    if (this.FileUpload) {
      if (this.isUploadSuccessFull) {
        this.dataService.getData(environment.commonUtilities.deletes3File, {
          layer_id: this.samples_file_upload_S3_connector_id,
          file_name: this.selectedFiles.item(0).name,
          folder_prefix: this.folderPrefix
        })
          .subscribe(data => {
            if (data.Status == 'Success') {
              this.FileUpload.nativeElement.value = null;
              if (this.dataIngestionDetails.file_type_name) {
                this.FileUpload.nativeElement.disabled = false;
              }
            }
          });
      }
    }

    this.sampleData = null;
    this.sampleDataColumns = null;

    this.selectedFiles = null;
    this.selectedFileName = null;

    this.isUploadSuccessFull = false;
    this.isUploading = false;
    this.isUploadConfirmed = false;

    this.dataIngestionDetails.sample_file_name = null;
    if (this.activeLayoutOption === this.layoutOptions[0]) {
      if (this.fileType && this.fileType.value) {
        this.dataService.getData(environment.commonUtilities.getDataFeedDetails, { name: 'get_data_feed_details', obj_id: this.fileType.value })
          .subscribe(data => {
            this.fileColumnDelemiter = data && data.Contents ? data.Contents[0].obj_delimiter : "";
            this.dataIngestionDetails.file_extension = data && data.Contents ? data.Contents[0].obj_extension : "";
            this.dataIngestionDetails.file_text_qualifier = data && data.Contents ? (data.Contents[0].obj_text_qualifier ? data.Contents[0].obj_text_qualifier : '--') : "";
            this.dataIngestionDetails.file_encoding = data && data.Contents ? data.Contents[0].obj_encoding : "";
            this.allowedExtensions = data && data.Contents ? '.' + data.Contents[0].obj_extension : "";
          });
      }
    } else {
      this.fileColumnDelemiter = null;
      this.dataIngestionDetails.file_extension = null;
      this.dataIngestionDetails.file_text_qualifier = null;
      this.dataIngestionDetails.file_encoding = null;
      this.allowedExtensions = '.csv,.psv,.txt,.xls,.xlsx';
    }
  }

  dlgConfirmBtnClick() {
    this.activeLayoutDisplayOption = this.layoutDisplayOptions.length > 0 ? this.layoutDisplayOptions[3] : "";
    this.isUploadConfirmed = true;
    this.Dialog.hide();

    const ConnFolderPrefix = (this.connectionTypes.find(item => item.conn_id === this.sourceConnectionDetails.data_layer_id)).folder_prefix;
    let fileName = '';
    if (this.activeDataSourceOption === this.dataSourceOptions[1].text) {
      const filePath = this.sourceConnectionDetails.abslt_path.replace(ConnFolderPrefix, '').replace(/\//g, ' ').trim().replace(/ /g, '/');
      fileName = filePath.toString().split('/').pop();
    } else {
      fileName = this.selectedFileName;
    }

    this.dataIngestionDetails.sample_file_name = fileName;

    if (this.activeLayoutOption === this.layoutOptions[1]) {
      this.dataFileSelected.emit([this.activeLayoutDisplayOption, null, null,
      this.dataIngestionDetails.sample_file_name, this.dataIngestionDetails.file_column_delemiter,
      this.dataIngestionDetails.file_text_qualifier === '--' ? 'None' : this.dataIngestionDetails.file_text_qualifier,
      this.sourceConnectionDetails.data_layer_id, this.sourceConnectionDetails.abslt_path]);
    } else {
      // Call for file validation
    }

  }

  dlgRejectBtnClick(modalType) {
    if (modalType === 'preview') {
      this.setupNewFileUpload();
      this.Dialog.hide();
    }
    else {
      this.FileUpload.nativeElement.disabled = false;
      this.FileUpload.nativeElement.value = null;
      this.setupNewFileUpload();
      this.s3UploadDialog.hide();
    }
  }

  selectIngestFile(event) {
    this.selectedFiles = event.target.files;
  }

  fileExtensionChange() {
    if (this.fileExtension.value === 'csv') {
      this.fileColumnDelimiterList = [{ del_txt: 'Comma(,)', del_val: ',' }];
      this.fileColumnDelemiter = ',';
    } else if (this.fileExtension.value === 'psv') {
      this.fileColumnDelimiterList = [{ del_txt: 'Pipe(|)', del_val: '|' }];
      this.fileColumnDelemiter = '|';
    } else if (this.fileExtension.value === 'txt') {
      this.fileColumnDelimiterList = [{ del_txt: 'Tab(/t)', del_val: '/t' }, { del_txt: 'Comma(,)', del_val: ',' },
      { del_txt: 'Pipe(|)', del_val: '|' }];
    } else if (this.fileExtension.value === 'xlsx' || this.fileExtension.value === 'xls') {
      this.fileColumnDelimiterList = [{ del_txt: 'NA', del_val: '--' }];
      this.fileColumnDelemiter = '--';
    }

    this.dataIngestionDetails.file_column_delemiter = this.fileColumnDelemiter;
  }

  saveDataIngestionDetails() {
    return this.dataService.getData(environment.commonUtilities.saveObjectDetails,
      {
        name: 'add_object_meta', obj_schema_value: 'datamax_raw', obj_nm_value: this.dataIngestionDetails.file_type_name,
        obj_type_value: 'file', obj_desc_value: this.dataIngestionDetails.business_purpose,
        obj_src_value: null, obj_layer_value: 'External', obj_delimiter_value: this.dataIngestionDetails.file_column_delemiter,
        obj_text_qualifier_value: this.dataIngestionDetails.file_text_qualifier === '--' ? null : this.dataIngestionDetails.file_text_qualifier,
        obj_extension_value: this.dataIngestionDetails.file_extension, is_header_value: 'Y', frequency_value: null,
        mstr_obj_id_value: null, file_name: null, obj_encoding: this.dataIngestionDetails.file_encoding, obj_pattern_fmt: null,
        obj_provider_nm: null, sample_file_name: null,
        file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null, created_by_value: 'DS101',
        data_persist_value: null, ref_obj_id_value: null, obj_id_value: null, last_updated_by: null
      });
  }

  saveDataIngestionExternalTableDetails(ref_object_Id: number) {
    const tgtSchema = (this.tgtSndBxOptions.find(item => item.text === this.activeTgtSndBxOption)).value;

    return this.dataService.getData(environment.commonUtilities.saveObjectDetails,
      {
        name: 'add_object_meta', obj_schema_value: tgtSchema, obj_nm_value: 'ext_' + this.dataIngestionDetails.file_type_name.toLowerCase().replace(/ /g, '_'),
        obj_type_value: 'table', obj_desc_value: null,
        obj_src_value: null, obj_layer_value: 'External', obj_delimiter_value: null,
        obj_text_qualifier_value: null, obj_extension_value: null,
        is_header_value: null, frequency_value: null,
        mstr_obj_id_value: null, file_name: null,
        obj_encoding: this.dataIngestionDetails.file_encoding, obj_pattern_fmt: null,
        obj_provider_nm: null, sample_file_name: null,
        file_arrival_start_day: null, file_arrival_end_day: null, file_arrival_time: null, created_by_value: 'DS101',
        data_persist_value: null, ref_obj_id_value: ref_object_Id, obj_id_value: null, last_updated_by: null
      })
  }

  saveConnectionDetails(apiName: string, obj_id: number) {
    const ConnFolderPrefix = (this.connectionTypes.find(item => item.conn_id === this.sourceConnectionDetails.data_layer_id)).folder_prefix;
    let connectionPath = '';
    if (this.activeDataSourceOption === this.dataSourceOptions[1].text) {
      const filePath = this.sourceConnectionDetails.abslt_path.replace(ConnFolderPrefix, '').replace(/\//g, ' ').trim().replace(/ /g, '/');
      const fileName = filePath.toString().split('/').pop();
      connectionPath = this.sourceConnectionDetails.abslt_path.replace(fileName, '').replace(/\//g, ' ').trim().replace(/ /g, '/');
    } else {
      connectionPath = this.sourceConnectionDetails.abslt_path;
    }

    return this.dataService.getData(environment.commonUtilities.saveConnectionDetails,
      {
        name: apiName, obj_id_value: obj_id, conn_id_value: this.sourceConnectionDetails.data_layer_id,
        user_id_value: this.authenticateService.getUserInfo().user_id, user_type_value: null, folder_prefix: connectionPath,
        created_by_value: this.authenticateService.getUserInfo().user_id, last_updated_by_value: this.authenticateService.getUserInfo().user_id
      })
  }

  registerDataFile(obj_id: number) {
    const fileSource = (this.dataSourceOptions.find(item => item.text === this.activeDataSourceOption)).value;

    return this.dataService.getData(environment.ingestion.registerDataFile,
      {
        name: 'add_file_register', obj_id_value: obj_id, file_nm_value: this.dataIngestionDetails.sample_file_name,
        ingestion_flag_value: null,
        sla_flag_value: null, file_notice_out_value: null,
        delay_notice_in_value: null, user_id_value: 'DS101',
        file_status_code_value: null, file_source_value: fileSource,
        actual_arrival_time_value: this.dataIngestionDetails.last_modified_time
      })
  }

  createExternalTable(obj_id_val: number) {
    const tgtSchema = (this.tgtSndBxOptions.find(item => item.text === this.activeTgtSndBxOption)).value;

    return this.dataService.getData(environment.ingestion.createExternalTable, {
      rs_connector_id: 2, obj_id: obj_id_val,
      create_flg: 'Y', // this.activeLayoutOption === this.layoutOptions[1] ? 'Y' : 'N',
      schema_name: tgtSchema
    })
  }
}
