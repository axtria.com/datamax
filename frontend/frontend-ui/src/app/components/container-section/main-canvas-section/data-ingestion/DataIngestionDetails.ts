export class DataIngestionDetails {
    obj_id: number;
    obj_uri_id: number;
    file_type_name: string;
    business_purpose: string;
    sample_file_name: string;    
    file_extension: string;
    file_column_delemiter: string;
    file_text_qualifier: string;
    file_encoding: string;
    last_modified_time: any;
}