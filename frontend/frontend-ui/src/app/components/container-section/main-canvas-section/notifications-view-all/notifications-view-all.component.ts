import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-notifications-view-all',
  templateUrl: './notifications-view-all.component.html',
  styleUrls: ['./notifications-view-all.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class NotificationsViewAllComponent implements OnInit {

  i18Labels: any;
  notifications: any = [];
  displayNotifications: any = [];
  totalNotificationCount: any;
  key: string;

  constructor(cs: ContentService, private rs: RestService,
    public storageService: StorageService, private utilService: UtilService) {
    this.i18Labels = cs.getContent(environment.modules.notifications);
  }

  ngOnInit() {
    let property = 'ui_event_name';
    this.rs.getDataWithoutAlert(environment.landing.get_notifications, {}).subscribe(data => {
      this.notifications = data.Contents;
      this.displayNotifications = this.utilService.getArrayGroupByKey(this.notifications, property);
      this.totalNotificationCount = this.notifications.length;
      this.key = Object.keys(this.displayNotifications).sort()[0];
    });
  }

}
