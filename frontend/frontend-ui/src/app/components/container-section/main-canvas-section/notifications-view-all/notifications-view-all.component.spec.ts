import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsViewAllComponent } from './notifications-view-all.component';

describe('NotificationsViewAllComponent', () => {
  let component: NotificationsViewAllComponent;
  let fixture: ComponentFixture<NotificationsViewAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsViewAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsViewAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
