import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { OnboardedDataDetailsComponent } from '../onboarded-data-details/onboarded-data-details.component';
import { RestService } from 'src/app/shared/services/rest.service';

@Component({
  selector: 'app-main-onboarded-data',
  templateUrl: './main-onboarded-data.component.html',
  styleUrls: ['./main-onboarded-data.component.css'],
})
export class MainOnboardedDataComponent implements OnInit {
  objId: number;
  mainOnboardedContent: any;
  onboardedDataSetCatalog = [];
  providerList = [];
  dataSetList = [];
  businessNameList = [];
  frequencyList = [];
  searchtxt: string;
  providerToFilter: string[];
  dataSetToFilter: string[];
  businessNameToFilter: string[];
  frequencyToFilter: string[];
  allFilterCriteria = [];
  attributesForFilter = [];
  selectedAttributes = [];
  

  @ViewChild('onboardedDataDetails', { static: false }) objOnboardedDataDetailsComponent: OnboardedDataDetailsComponent;
  @ViewChild('providerName', { static: false }) providerName: any;
  @ViewChild('datasetName', { static: false }) datasetName: any;
  @ViewChild('businessName', { static: false }) businessName: any;
  @ViewChild('frequency', { static: false }) frequency: any;

  constructor(cs: ContentService, private dataService: RestService) {
    this.mainOnboardedContent = cs.getContent(environment.modules.dataOnboarding).mainOnboarded;
  }

  ngOnInit() {
    this.dataService.getData(environment.sourceConnectors.getSourceConnectors,
      { name: 'get_source_data_catalogue' }).subscribe(data => {
        this.onboardedDataSetCatalog = data.Contents;
        this.providerList = [...new Set(this.onboardedDataSetCatalog.map(item => item.provider))];
        this.dataSetList = [...new Set(this.onboardedDataSetCatalog.map(item => item.dataset_nm))];
        this.businessNameList = [...new Set(this.onboardedDataSetCatalog.map(item => item.business_nm))];
        this.frequencyList = [...new Set(this.onboardedDataSetCatalog.map(item => item.frequency))];
      });
      
    this.allFilterCriteria = [{ name: "Business Name", value: "business_nm", dataSource: this.businessNameList},
    { name: "Frequency", value: "frequency", dataSource: this.frequencyList }]

    this.attributesForFilter = this.allFilterCriteria;
  }

  providerListChange(e: any) {
    this.providerToFilter = e.value;
  }
  dataSetListChange(e: any) {
    this.dataSetToFilter = e.value;
  }

  attributeSelected(attrVal: any) {
    this.attributesForFilter = this.attributesForFilter.filter(function (item: any) {
      return item.value !== attrVal.value
    });
    this.selectedAttributes.push(attrVal);
  }
  deleted(toDelete: any) {
    this.attributesForFilter.push(toDelete);
    this.selectedAttributes = this.selectedAttributes.filter(item => item !== toDelete);
    if (toDelete.value === "business_nm") {
      this.businessNameToFilter = [];
    }
    if (toDelete.value === "frequency") {
      this.frequencyToFilter = [];
    }
  }
  businessNameChange(e:any){
    if (e.value !== null) {
      this.businessNameToFilter = e.value;
    }
  }
  frequencyChange(e:any){
    if (e.value !== null) {
      this.frequencyToFilter = e.value;
    }
  }

  clearFilter(){
    this.attributesForFilter = this.allFilterCriteria;
    this.selectedAttributes = []
    this.businessNameToFilter = [];
    this.frequencyToFilter = [];
    this.providerToFilter = [];
    this.dataSetToFilter = [];
    this.searchtxt = null;
    if(this.providerName)  this.providerName.value = [];
    if(this.datasetName) this.datasetName.value = [];
    if(this.businessName) this.businessName.value = [];
    if(this.frequency) this.frequency.value = [];
  }
}
