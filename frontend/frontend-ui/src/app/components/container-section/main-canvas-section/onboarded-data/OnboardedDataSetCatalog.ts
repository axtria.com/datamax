export interface OnboardedDataSetCatalog {
    obj_id: number;
    provider: string;
    dataset_nm: string;    
    business_nm: string;
    frequency:string;
    description: string;
    sample_file_nm: string;
    data_layer_id: number;
}
