export interface OnboardedPocDetails {
    poc_group: string
    poc_nm: string;
    poc_email: string;
    poc_ph: string;
}