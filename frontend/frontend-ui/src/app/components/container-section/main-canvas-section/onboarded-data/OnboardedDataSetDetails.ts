export interface OnboardedDataSetDetails {
    business_nm: string;
    dataset_nm: string;
    provider: string;
    data_feed_sla: string;
    obj_desc: string;
    sample_file_nm: string;
    obj_typ: string;
    header_info: string;
    obj_extn: string;
    obj_delimiter: string;
    obj_text_qualifier: string;
    obj_encoding: string;
    obj_ptrn_fmt: string;
    table_data_lyr_nm: string;
    table_nm: string;
}