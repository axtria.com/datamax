export interface OnboardedSourceConnectionDetails {
    name: string;
    lyr_type: string;
    s3_path: string;
    abslt_path: string;
    schema_name: string;
    description: string;
    display_data_lyr_name: string;
    data_lyr_name: string;
}