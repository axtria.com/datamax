import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { RestService } from 'src/app/shared/services/rest.service';
import { OnboardedDataSetCatalog } from 'src/app/components/container-section/main-canvas-section/onboarded-data/OnboardedDataSetCatalog';
import { Output, EventEmitter } from '@angular/core';
import { RowSelectEventArgs, ToolbarItems } from '@syncfusion/ej2-grids';
import { CommandModel, GridComponent, IRow, Column } from '@syncfusion/ej2-angular-grids';
import { environment } from 'src/environments/environment';
import { closest } from '@syncfusion/ej2-base';
import { ContentService } from 'src/app/shared/services/content.service';
import { ResizeService, ReorderService } from '@syncfusion/ej2-angular-grids';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogComponent, PositionDataModel } from '@syncfusion/ej2-angular-popups';
import { OnboardedDataDetailsComponent } from '../onboarded-data-details/onboarded-data-details.component';
import { ToastPopupService } from 'src/app/components/toast-popup/toast-popup.service';

@Component({
  selector: 'app-onboarded-data-catalog',
  templateUrl: './onboarded-data-catalog.component.html',
  styleUrls: ['./onboarded-data-catalog.component.css'],
  providers: [ResizeService, ReorderService]
})
export class OnboardedDataCatalogComponent implements OnInit {
  @Input('searchtxt') searchtxt: string;
  @Input('providerToFilter') providerToFilter: string[];
  @Input('dataSetToFilter') dataSetToFilter: string[]; 
  @Input('businessNameToFilter') businessNameToFilter: string[];
  @Input('frequencyToFilter') frequencyToFilter: string[]; 
  // count of filtered data after search
  filterMetadata = { count: 0 };

  onboardedDataCatalogContent: any;
  objId: number;

  @Output() dataSetSelected = new EventEmitter();
  public onboardedDataSetCatalog: OnboardedDataSetCatalog[];
  gridPageSettings: Object;
  gridToolbar: ToolbarItems[] | Object;
  currentPage = 1;
  // gridResultsCountSrc: number;
  gridCommands: CommandModel[];
  gridCloneCommands: CommandModel[];
  gridViewCommands: CommandModel[];
  gridFileDownloadCommands: CommandModel[];
  gridDeleteCommands: CommandModel[];
  dlgPosition: PositionDataModel = { X: 'right', Y: 'top' };
  dlgAnimationSettings: Object = { effect: 'SlideRight', duration: 600, delay: 0 };
  samples_file_download_S3_layer_id = 85;
  folderPrefix = "OnboardingSampleFiles/";

  @ViewChild('onboardedSourcesListGrid', { static: false }) onboardedSourcesListGrid: GridComponent;
  @ViewChild('onboardedDataDialog', { static: false }) onboardedDataDialog: DialogComponent;
  @ViewChild('onboardedDataDetails', { static: false }) objOnboardedDataDetailsComponent: OnboardedDataDetailsComponent;

  constructor(private dataService: RestService,private toastPopupService: ToastPopupService, cs: ContentService, private route: ActivatedRoute, private router: Router) {
    this.onboardedDataCatalogContent = cs.getContent(environment.modules.dataOnboarding).onboardedCatalog;
  }

  ngOnInit() {
    this.gridPageSettings = { pageSizes: true, pageSize: 5, currentPage: this.currentPage, template: '', pageCount: 5 };
    this.dataService.getData(environment.sourceConnectors.getSourceConnectors,
      { name: 'get_source_data_catalogue' }).subscribe(data => {
        this.onboardedDataSetCatalog = data.Contents;
        // this.gridResultsCountSrc = this.onboardedDataSetCatalog.length;
      });
    this.gridCommands = [{
      type: 'Edit', buttonOption: {
        iconCss: 'e-icons e-edit', cssClass: 'e-flat',
        click: this.editSourceHandler.bind(this)
      }
    }];
    this.gridCloneCommands = [{
      title: 'Clone', buttonOption: {
        iconCss: 'fa fa-clone', cssClass: 'e-flat',
        click: this.cloneSourceHandler.bind(this)
      }
    }];
    // this.gridViewCommands = [{
    //   title: 'View', buttonOption: {
    //     iconCss: 'fa fa-angle-right', cssClass: 'e-flat',
    //     click: this.viewSourceHandler.bind(this)
    //   }
    // }];
    // this.gridFileDownloadCommands = [{
    //   title: 'Download File', buttonOption: {
    //     iconCss: 'fa fa-download', cssClass: 'e-flat',
    //     click: this.downloadFileSourceHandler.bind(this)
    //   }
    // }];
    this.gridDeleteCommands = [{
      title: 'Delete', buttonOption: {
        iconCss: 'fa fa-trash-o', cssClass: 'e-flat'
      }
    }];

    this.setToolbar();
  }

  setToolbar() {
    this.gridToolbar = [{ type: 'label', template: '#showSrcResultsCount', align: 'Right' },
    { type: 'Input', template: '#addNewSrc', align: 'Right', id: 'addNewSrc', click: this.addSourceHandler.bind(this) },
    { text: 'ColumnChooser', template: '#columnChooserTemplateSrc', align: 'Right' }];
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
  }

  addSourceHandler(args: ClickEventArgs): void {
    if (args.item.id === "addNewSrc") {
      this.router.navigate(['dataOnboarding'], { relativeTo: this.route.parent });
    }
  }

  editSourceHandler(args: Event): void {
    const rowObj: IRow<Column> = this.onboardedSourcesListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').getAttribute('data-uid'));
    const record = rowObj.data as OnboardedDataSetCatalog;
    this.router.navigate(['dataOnboarding', 'edit=' + record.obj_id], { relativeTo: this.route.parent });
  }

  cloneSourceHandler(args: Event): void {
    const rowObj: IRow<Column> = this.onboardedSourcesListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').getAttribute('data-uid'));
    const record = rowObj.data as OnboardedDataSetCatalog;
    this.router.navigate(['dataOnboarding', 'clone=' + record.obj_id], { relativeTo: this.route.parent });
  }
  viewSourceHandler(objId): void {
    // const rowObj: IRow<Column> = this.onboardedSourcesListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').getAttribute('data-uid'));
    // const record = rowObj.data as OnboardedDataSetCatalog;
    this.router.navigate(['onboardedDataDetails', objId], { relativeTo: this.route.parent });
    // this.objOnboardedDataDetailsComponent.populateDataSetDetails(record.obj_id);
  }

  // downloadFileSourceHandler(args: Event): void {
  //   const rowObj: IRow<Column> = this.onboardedSourcesListGrid.getRowObjectFromUID(closest(args.target as Element, '.e-row').getAttribute('data-uid'));
  //   const record = rowObj.data as OnboardedDataSetCatalog;
  //  // this.downloadFile(record.sample_file_nm);
  // }

  downloadFile(fileName) {
    this.dataService.getData(environment.commonUtilities.downloads3File, {
      layer_id: environment.onboarding.onboardingSampleFileLayerId,
      file_name: fileName,
      folder_prefix: this.folderPrefix
    })
      .subscribe((data) => {
        if(data.Status === "Success") {
          this.toastPopupService._exception.emit(this.toastPopupService.getToastSuccessMessage(this.onboardedDataCatalogContent.downloadSuccess));
        } else {
          this.toastPopupService._exception.emit(this.toastPopupService.getToastWarningMessage(this.onboardedDataCatalogContent.downloadFail));
        }
      });
  }

  onRowSelected(args: RowSelectEventArgs): void {
    const record = args.data as OnboardedDataSetCatalog;
    this.objId = record.obj_id;
    this.dataSetSelected.emit(record.obj_id);
  }
}
