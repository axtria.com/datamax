import { Component, OnInit, ViewChild } from '@angular/core';
import { OnboardedDataSetDetails } from 'src/app/components/container-section/main-canvas-section/onboarded-data/OnboardedDataSetDetails';
import { OnboardedPocDetails } from 'src/app/components/container-section/main-canvas-section/onboarded-data/OnboardedPocDetails';
import { OnboardedSourceConnectionDetails } from 'src/app/components/container-section/main-canvas-section/onboarded-data/OnboardedSourceConnectionDetails';
import { RestService } from 'src/app/shared/services/rest.service';
import { environment } from 'src/environments/environment';
import { ContentService } from 'src/app/shared/services/content.service';
import { ResizeService, ReorderService, ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { DataLayoutDetails } from 'src/app/components/container-section/main-canvas-section/data-onboarding/DataLayoutDetails';
import { TabAnimationSettingsModel } from '@syncfusion/ej2-angular-navigations';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-onboarded-data-details',
  templateUrl: './onboarded-data-details.component.html',
  styleUrls: ['./onboarded-data-details.component.css'],
  providers: [ResizeService, ReorderService]
})

export class OnboardedDataDetailsComponent implements OnInit {
  onboardedDetailsContent: any;

  dataDetailsOptions: string[];
  activeDetailsOptions: string;

  onboardedDataSetDetails: OnboardedDataSetDetails;
  dataLayoutDetails: DataLayoutDetails[];
  dataLayoutDetail: any;
  inboundDetails: OnboardedSourceConnectionDetails;
  landingDetails: OnboardedSourceConnectionDetails;
  archivalDetails: OnboardedSourceConnectionDetails;
  OnboardedPocDetails: OnboardedPocDetails;
  gridPageSettings: object;
  currentPage = 1;
  gridToolbar: ToolbarItems[] | Object;
  gridResultsCountColumns : number;
  objId: number;

  tabAnimationSettings: TabAnimationSettingsModel = { previous: { effect: "None" }, next: { effect: "None" } };
  @ViewChild('btnDetail', { static: false }) btnDetail: any;
  @ViewChild('btnAttributes', { static: false }) btnAttributes: any;
  constructor(private dataService: RestService, cs: ContentService, private route: ActivatedRoute) {
    this.onboardedDetailsContent = cs.getContent(environment.modules.dataOnboarding);
    this.dataDetailsOptions = this.onboardedDetailsContent.onboardedDetails.dataDetailsOptions;
  }

  ngOnInit() {
    this.objId = parseInt(this.route.snapshot.paramMap.get('id'));
    this.dataService.getData(environment.sourceConnectors.getSourceConnectorDetails,
      { name: 'get_provider_data_catalogue', obj_id: this.objId })
      .subscribe(data => { this.onboardedDataSetDetails = data.Contents[0]; });

    this.dataService.getData(environment.onboarding.connection.getSourceConnectionDetails,
      { name: 'get_source_connection_details', object_id: this.objId })
      .subscribe(data => {
        this.inboundDetails = data.Contents[0];
        this.landingDetails = data.Contents[1];
        this.archivalDetails = data.Contents[2];
      });

    this.dataService.getData(environment.onboarding.provider.getAgreementDetails,
      { name: 'get_data_agreement_details', object_id: this.objId })
      .subscribe(data => {
        this.OnboardedPocDetails = data.Contents;
      });

    this.dataService.getData(environment.sourceConnectors.getSourceConnectorAttributes,
      { name: 'get_object_attribute_details', obj_id: this.objId })
      .subscribe(data => { this.dataLayoutDetails = data.Contents;
        this.gridResultsCountColumns = this.dataLayoutDetails.length;
      });
    this.gridPageSettings = { pageSizes: false, pageSize:5, currentPage: this.currentPage, template: ''};
    this.activeDetailsOptions = this.dataDetailsOptions[0];

    this.setToolbar();
  }

  setToolbar(){
    this.gridToolbar = [{ type: 'label', template: '#showResultsCountDetails', align: 'Right' },    
    { text: 'ColumnChooser', template: '#columnChooserTemplateCols', align: 'Right' }];
  }

  changePageSize(arg: any) {
    this.gridPageSettings = { pageSize: arg.itemData.value };
  }
  // toggleOnboardOption(selectedDetailsOption: string) {
  //   this.activeDetailsOptions = selectedDetailsOption;
  //   if (this.activeDetailsOptions === this.dataDetailsOptions[0]) {
  //     if (this.btnDetail.nativeElement.classList.contains('btn-outline-warning')) {
  //       this.btnDetail.nativeElement.classList.replace('btn-outline-warning', 'btn-warning');
  //       this.btnAttributes.nativeElement.classList.replace('btn-warning', 'btn-outline-warning');
  //     }
  //   }
  //   if (this.activeDetailsOptions === this.dataDetailsOptions[1]) {
  //     if (this.btnAttributes.nativeElement.classList.contains('btn-outline-warning')) {
  //       this.btnAttributes.nativeElement.classList.replace('btn-outline-warning', 'btn-warning');
  //       this.btnDetail.nativeElement.classList.replace('btn-warning', 'btn-outline-warning');
  //     }
  //   }
  // }

  formatDisplayText(dataType: string): string {
    let _targetDataType = dataType.toUpperCase();

    let frmt = _targetDataType === 'DATE' ? 'MM-dd-yyyy' :  _targetDataType === 'TIMESTAMP' || _targetDataType === 'TIMESTAMP_TZ' || _targetDataType === 'TIMESTAMP_NTZ' ? 'MM-dd-yyyy hh:mm:ss' : 'NA';

    return frmt;
  }
}
