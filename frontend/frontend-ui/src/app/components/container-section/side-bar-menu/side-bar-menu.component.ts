import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ContentService } from 'src/app/shared/services/content.service';
import { config } from 'src/environments/config';
import { StorageService } from 'src/app/shared/services/storage.service';
declare var $: any;

// Hide submenus
//$('#body-row .collapse').collapse('hide');

// Collapse/Expand icon
//$('#collapse-icon').addClass('fa-angle-double-left');

@Component({
  selector: 'app-side-bar-menu',
  templateUrl: './side-bar-menu.component.html',
  styleUrls: ['./side-bar-menu.component.css']
})
export class SideBarMenuComponent implements OnInit {
  menuContent: any;
  showAllMenu = config.showAllMenu;

  constructor(public router: Router, cs: ContentService, public route: ActivatedRoute, public storageService: StorageService) {
    this.menuContent = cs.getContent("menu");
  }

  ngOnInit() {
  // this.SidebarCollapse();
  if (this.storageService.getSessionItem('highlightOption')) {
    this.HighlightMainMenu(this.storageService.getSessionItem('highlightOption'));
  }
  }

  HighlightMainMenu(id: string) {
    this.storageService.setSessionItem('highlightOption', id);
    $('.selected').removeClass('selected');
    $('.submenu-selected').removeClass('submenu-selected');
    $(id).addClass('selected');
  }

  HighlightSubMenu(id: string) {
    $('.submenu-selected').removeClass('submenu-selected');
    $(id).addClass('submenu-selected');
  }

  SidebarCollapse() {
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-rotate-180');
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');

    // Treating d-flex/d-none on separators with title
    const SeparatorTitle = $('.sidebar-separator-title');
    if (SeparatorTitle.hasClass('d-flex')) {
      SeparatorTitle.removeClass('d-flex');
    } else {
      SeparatorTitle.addClass('d-flex');
    }
  }
}
