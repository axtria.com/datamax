import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../../shared/services/authenticate.service';
import { ContentService } from 'src/app/shared/services/content.service';
import { config } from 'src/environments/config';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {
  model: any = {};
  loginContent: any;
  imagePath = config.imagesPath;

  constructor(private authenticateService: AuthenticateService, cs: ContentService) {
    this.loginContent = cs.getContent("login");
   }

  ngOnInit() {
  }

  login() {
    this.authenticateService.login(this.model);
  }
}
