import { Component, OnInit } from '@angular/core';
import { GlobalConstants } from 'src/app/shared/common/global-constants';
import { Router } from '@angular/router';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';

@Component({
  selector: 'app-sso',
  templateUrl: './sso.component.html',
  styleUrls: ['./sso.component.css']
})
export class SsoComponent implements OnInit {

  constructor(private router: Router, private authenticateService: AuthenticateService) { 
    
  }

  ngOnInit() {
    GlobalConstants.sso = true;
    if(!GlobalConstants.sso)
        this.router.navigate(['authentication']);
      else
        this.authenticateService.ssologin();
  }

}
