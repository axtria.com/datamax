import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/shared/services/content.service';

@Component({
  selector: 'app-footer-view',
  templateUrl: './footer-view.component.html',
  styleUrls: ['./footer-view.component.css']
})
export class FooterViewComponent implements OnInit {
  public footerContent: any;

  constructor(cs: ContentService) {
    this.footerContent = cs.getContent("Landing");
   }

  ngOnInit() {}

}
