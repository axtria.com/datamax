import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { AuthenticateService } from 'src/app/shared/services/authenticate.service';
import { DOCUMENT } from '@angular/common';
import { config } from 'src/environments/config';
import { StorageService } from './shared/services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  imagePath = config.imagesPath;
  isNewView= false;

  constructor(
    private authenticateService: AuthenticateService , 
    @Inject(DOCUMENT) private _document: HTMLDocument,
    private storageService: StorageService
  ) {  
    this._document.getElementById('appFavicon').setAttribute('href', 'assets/images/' + this.imagePath + '/fav.png');
    
  }

  ngOnInit() {
    //const token = this.storageService.getLocalItem('token');
    this.storageService.removeSessionItem('view');
  }
  loggedIn()  {
    this.isNewView = (this.storageService.getSessionItem('view'));
    return this.authenticateService.getUserInfo();
  }
  
}
