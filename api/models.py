#   datamax/models.py
#   Copyright (C) 2019 Axtria.
#
#   Author(s):
#   Varun Singhal <varun.singhal@axtria.com>;
#   Hemalatta Seksaria <Hemalatta.Seksaria@axtria.com>;
#   Neha Verma <Neha.Verma@axtria.com>
import datetime

from sqlalchemy import MetaData, Column, Integer, String, ForeignKey, Date, Boolean, JSON, Sequence, DateTime, func, \
    text, Time
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

metadata = MetaData()
Base = declarative_base(metadata=metadata)

config_instance_id_seq = Sequence('config_instance_id_seq', metadata=metadata)


class Project(Base):
    __tablename__ = 'project'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    created_by = Column(String)
    creation_date = Column(Date)
    modified_by = Column(String)
    modification_date = Column(Date)
    is_active = Column(Boolean)

    def __repr__(self):
        return '<Project %r - %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Connector(Base):
    __tablename__ = 'connection_meta'
    __table_args__ = {'schema': 'system_config'}

    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    description = Column(String(256))
    project_id = Column(Integer, ForeignKey('project.id'))
    connection_type = Column(String)
    connection = Column(JSON)
    created_by = Column(String)
    creation_date = Column(DateTime(timezone=True), server_default=func.now())
    modified_by = Column(String)
    modification_date = Column(
        DateTime(timezone=True), server_default=func.now(), onupdate=func.now())
    is_active = Column(Boolean)
    status = Column(String)

    # virtual column
    project = relationship('Project')

    def __repr__(self):
        return '<Connector [%r] - %r - %r>' % (self.id, self.connection_type, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Files(Base):
    __tablename__ = 'files'

    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    description = Column(String(256))
    connector_id = Column(Integer, ForeignKey(
        'system_config.connection_meta.id'))
    shape = Column(JSON)
    properties = Column(JSON)
    created_by = Column(String)
    creation_date = Column(DateTime(timezone=True), server_default=func.now())
    modified_by = Column(String)
    modification_date = Column(Date)
    is_active = Column(Boolean)
    source_type = Column(String)

    # virtual column
    connector = relationship("Connector")

    def __repr__(self):
        return '<Files [%r] %r>' % (self.id, self.name)


class Rules(Base):
    __tablename__ = 'rules'

    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    description = Column(String)
    redshiftquery = Column(String)
    pyfunctionname = Column(String)
    createdby = Column(String)
    creationdate = Column(Date)
    modifiedby = Column(String)
    modificatindate = Column(Date)
    parameters = Column(JSON)
    level = Column(String)
    is_active = Column(Boolean)
    datatype = Column(String)
    ui_description = Column(String)

    def __repr__(self):
        return '<Rule [%r] %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Config(Base):
    __tablename__ = 'config'

    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    params = Column(JSON)
    std_object = Column(String)
    file_id = Column(Integer)
    project_id = Column(Integer, ForeignKey('project.id'))
    version = Column(Integer, default=1)
    is_active = Column(Boolean)
    created_by = Column(String)
    creation_date = Column(DateTime, default=datetime.datetime.utcnow)
    modified_by = Column(String)
    modification_date = Column(Date)

    def __repr__(self):
        return '<Config [%r] %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class ConfigInstance(Base):
    __tablename__ = 'config_instance'

    id = Column(Integer, config_instance_id_seq,
                server_default=config_instance_id_seq.next_value(), primary_key=True)
    config_id = Column(Integer, ForeignKey('config.id'))
    rule_type = Column(String)
    target_table = Column(String)
    target_schema = Column(String)
    detail_table_name = Column(String)
    detail_schema_name = Column(String)
    success_table_name = Column(String)
    success_schema_name = Column(String)
    failure_table_name = Column(String)
    failure_schema_name = Column(String)
    created_by = Column(String)
    start_date = Column(DateTime, default=datetime.datetime.utcnow)
    end_date = Column(DateTime)
    success_count = Column(Integer)
    fail_count = Column(Integer)
    status = Column(String)
    stage = Column(Integer, primary_key=True)
    success_s3_path = Column(String)
    failure_s3_path = Column(String)
    is_archieved = Column(Boolean, default=False)

    # virtual column
    config = relationship("Config", backref='config_instance')

    def __repr__(self):
        return '<ConfigInstance [%r] with config ID - %r>' % (self.id, self.config_id)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class RuleInstance(Base):
    __tablename__ = 'rule_instance'

    id = Column(Integer, primary_key=True)
    config_instance_id = Column(Integer)
    rule_id = Column(Integer)
    query = Column(String)
    start_date = Column(DateTime, default=datetime.datetime.utcnow())
    end_date = Column(DateTime)
    fail_count = Column(Integer)
    status = Column(String)
    stage = Column(Integer)

    def __repr__(self):
        return '<RuleInstance [%r] with rule ID - %r>' % (self.id, self.rule_id)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


api_id_seq = Sequence(
    'system_config.api_exchange_meta_id_seq', metadata=metadata)


class API(Base):
    __tablename__ = 'api_exchange_meta'
    __table_args__ = {'schema': 'system_config'}
    id = Column(Integer, api_id_seq,
                server_default=api_id_seq.next_value(), unique=True)
    name = Column(String, primary_key=True)
    version = Column(Integer, primary_key=True, server_default="1")
    description = Column(String)
    status = Column(String)
    category = Column(String)
    created_on = Column(DateTime(timezone=True), server_default=func.now())

    # server_default=text(
    #    "nextval('admn.event_meta_event_id_seq'::regclass)")


class APIAuth(Base):
    __tablename__ = 'api_exchange_auth'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    password = Column(String)
    is_active = Column(Boolean, default=True)

    def __repr__(self):
        return '<API Auth [%r] with name - %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class APIMeta(Base):
    __tablename__ = 'api_exchange_instnc'
    __table_args__ = {'schema': 'system_config'}
    id = Column(Integer, primary_key=True)
    api_id = Column(Integer, ForeignKey('system_config.api_exchange_meta.id'))
    query = Column(String)
    connector_id = Column(Integer, ForeignKey(
        'system_config.connection_meta.id'))
    sequence = Column(Integer)

    def __repr__(self):
        return '<API Meta [%r] with name - %r>' % (self.id, self.query)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class ConnectionMetaExt(Base):
    __tablename__ = 'connection_meta_ext'
    __table_args__ = {"schema": "system_config"}
    conn_id = Column(Integer, primary_key=True)
    id = Column(Integer, ForeignKey('system_config.connection_meta.id'))
    bucket = Column(String(50))
    folder_prefix = Column(String(200))
    # virtual column
    connector = relationship('Connector')

    def __repr__(self):
        return '<Test_con [%r] - %r - %r>' % (self.conn_id, self.bucket, self.prefix)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


workspace_id_seq = Sequence('workspace_id_seq', metadata=metadata)


class Workspace(Base):
    __tablename__ = 'workspace'
    __table_args__ = {'schema': 'brms'}
    id = Column(Integer, workspace_id_seq,
                server_default=workspace_id_seq.next_value(), unique=True)
    name = Column(String, primary_key=True, unique=True)
    description = Column(String)
    connector_id = Column(Integer, ForeignKey(
        'system_config.connection_meta.id'))
    is_active = Column(Boolean, server_default='true')
    created_on = Column(DateTime(timezone=True), server_default=func.now())
    modified_on = Column(DateTime(timezone=True),
                         server_default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<Workspace [%r] with name - %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Workflow(Base):
    __tablename__ = 'workflow'
    __table_args__ = {'schema': 'brms'}
    id = Column(String, primary_key=True)
    name = Column(String)
    description = Column(String)
    workflow_meta = Column(JSON)
    version = Column(Integer, server_default='1')
    status = Column(String, server_default='DRAFT')
    health = Column(Boolean, server_default='true')
    is_active = Column(Boolean, server_default='true')
    workspace_id = Column(Integer, ForeignKey('brms.workspace.id'))
    created_on = Column(DateTime(timezone=True), server_default=func.now())
    modified_on = Column(DateTime(timezone=True),
                         server_default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<Workflow [%r] with name - %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class ValidationWorkflow(Base):
    __tablename__ = 'validation_workflow'
    __table_args__ = {'schema': 'brms'}
    id = Column(String, primary_key=True)
    name = Column(String)
    description = Column(String)
    validation_meta = Column(JSON)
    version = Column(Integer, server_default='1')
    status = Column(String, server_default='DRAFT')
    health = Column(Boolean, server_default='true')
    is_active = Column(Boolean, server_default='true')
    created_on = Column(DateTime(timezone=True), server_default=func.now())
    modified_on = Column(DateTime(timezone=True),
                         server_default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<Validation workflow [%r] with name - %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


scenario_id_seq = Sequence('scenario_id_seq', metadata=metadata)


class Scenario(Base):
    __tablename__ = 'scenario'
    __table_args__ = {'schema': 'brms'}
    id = Column(Integer, scenario_id_seq,
                server_default=scenario_id_seq.next_value(), unique=True)
    name = Column(String, primary_key=True)
    workflow_id = Column(String, ForeignKey(
        'brms.workflow.id'), primary_key=True)
    scenario_meta = Column(JSON)
    is_active = Column(Boolean, server_default='true')
    last_run = Column(String)
    created_on = Column(DateTime(timezone=True), server_default=func.now())
    modified_on = Column(DateTime(timezone=True),
                         server_default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<ETLScenario [%r] with name - %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


scenario_log_id_seq = Sequence('scenario_log_id_seq', metadata=metadata)


class ScenarioLog(Base):
    __tablename__ = 'scenario_log'
    __table_args__ = {'schema': 'brms'}
    id = Column(String, scenario_log_id_seq,
                server_default=scenario_log_id_seq.next_value(), primary_key=True)
    scenario_id = Column(Integer, ForeignKey('brms.scenario.id'))
    scenario_log_meta = Column(JSON)
    created_on = Column(DateTime(timezone=True), server_default=func.now())
    modified_on = Column(DateTime(timezone=True),
                         server_default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<ETLScenarioLog [%r] with scenario id - %r>' % (self.id, self.scenario_id)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


validation_scenario_id_seq = Sequence(
    'validation_scenario_id_seq', metadata=metadata)


class ValidationScenario(Base):
    __tablename__ = 'validation_scenario'
    __table_args__ = {'schema': 'brms'}
    id = Column(Integer, validation_scenario_id_seq, server_default=validation_scenario_id_seq.next_value(),
                unique=True)
    name = Column(String, primary_key=True)
    workflow_id = Column(String, ForeignKey(
        'brms.validation_workflow.id'), primary_key=True)
    scenario_meta = Column(JSON)
    is_active = Column(Boolean, server_default='true', default=True)
    last_run = Column(String)
    created_on = Column(DateTime(
        timezone=True), server_default=func.now(), default=datetime.datetime.utcnow)
    modified_on = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(),
                         default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<ValidationScenario [%r] with name - %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


validation_scenario_log_id_seq = Sequence(
    'validation_scenario_log_id_seq', metadata=metadata)


class ValidationScenarioLog(Base):
    __tablename__ = 'validation_scenario_log'
    __table_args__ = {'schema': 'brms'}
    id = Column(String, validation_scenario_log_id_seq,
                server_default=validation_scenario_log_id_seq.next_value(), primary_key=True)
    scenario_id = Column(Integer, ForeignKey('brms.validation_scenario.id'))
    scenario_log_meta = Column(JSON)
    created_on = Column(DateTime(timezone=True), server_default=func.now())
    modified_on = Column(DateTime(timezone=True),
                         server_default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<ValidationScenarioLog [%r] with scenario id - %r>' % (self.id, self.scenario_id)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Phrases(Base):
    __tablename__ = 'phrases'
    __table_args__ = {'schema': 'brms'}
    id = Column(Integer, primary_key=True)
    component_name = Column(String)
    phrases = Column(JSON)
    category = Column(String)
    is_active = Column(Boolean, server_default='true')
    created_on = Column(DateTime(timezone=True), server_default=func.now())
    modified_on = Column(DateTime(timezone=True),
                         server_default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<Phrase [%r] with name - %r>' % (self.id, self.component_name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class FileAdaptor(Base):
    __tablename__ = 'adaptor'
    __table_args__ = {'schema': 'files'}
    id = Column(Integer, primary_key=True)
    name = Column(String)
    adaptor_meta = Column(JSON)
    is_active = Column(Boolean, server_default='true')
    created_on = Column(DateTime(timezone=True), server_default=func.now())
    modified_on = Column(DateTime(timezone=True),
                         server_default=func.now(), onupdate=func.now())

    def __repr__(self):
        return '<File Adaptor [%r] with name - %r>' % (self.id, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


user_id_seq = Sequence('system_config.user_meta_user_id', metadata=metadata)


class User(Base):
    __tablename__ = 'user_meta'
    __table_args__ = {'schema': 'system_config'}

    firstname = Column('first_name', String(50))
    lastname = Column('last_name', String(50))
    is_active = Column('is_usr_actv', Boolean,default=True)
    is_rec_active = Column('is_actv', Boolean,default=True)
    created_date = Column('audit_insrt_dt', DateTime, nullable=False)
    modified_date = Column('audit_updt_dt', DateTime)
    is_deleted = Column(Boolean, default=True)
    user_id = Column('user_id',Sequence('user_meta_user_id', schema='system_config',metadata=metadata),primary_key=True, server_default = text("nextval('system_config.user_meta_user_id'::regclass)"))
    # user_id = Column(Integer, primary_key=True, server_default = user_id_seq.next_value())
    username = Column('user_name', String(50), nullable=False, unique=True)
    is_ldap_user = Column(Boolean,default=False)
    is_local_user = Column(Boolean,default=True)
    created_by = Column('audit_insrt_id', Integer)
    modified_by = Column('audit_updt_id', Integer)
    is_admin = Column(Boolean,default=False)
    password = Column(String(250))
    email = Column(String(250))

    #parent = relationship('User', remote_side=[user_id], primaryjoin='User.created_by == User.user_id')
    #parent1 = relationship('User', remote_side=[user_id], primaryjoin='User.modified_by == User.user_id')

    def __repr__(self):
        return '<User [%r] with name - %r>' % (self.id, self.username)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Layer(Base):
    __tablename__ = 'data_layer_meta'
    __table_args__ = {'schema': 'system_config'}

    layer_id = Column('data_lyr_id',Integer,primary_key=True,
                      server_default=text("nextval('system_config.data_layer_meta_data_lyr_id'::regclass)"))
    name = Column('data_lyr_name', String(50))
    layer_type = Column('lyr_type', String(10))
    description = Column(String(250))
    s3_path = Column(String(400))
    schema_name = Column(String(50))
    connector_id = Column('conn_id',Integer)
    is_active = Column('is_actv', Boolean)
    created_by = Column('audit_insrt_id', Integer, nullable=False)
    # created_date = Column(DateTime, nullable=False)
    # modified_date = Column(DateTime)
    modified_by = Column('audit_updt_id', Integer)
    #connector = relationship('Connector')
    #user = relationship('User', primaryjoin='Layer.created_by == User.user_id')
    #user1 = relationship('User', primaryjoin='Layer.modified_by == User.user_id')
    tenant_id = Column(Integer)
    lyr_logical_type = Column(String(10))
    # eff_srt_dt = Column(DateTime)
    # eff_end_dt = Column(DateTime)
    # audit_batch_id = Column(String(8))
    # audit_insrt_dt = Column(DateTime)
    # audit_insrt_id = Column(DateTime)
    # audit_updt_dt = Column(DateTime)
    # audit_updt_id = Column(DateTime)

    def __repr__(self):
        return '<Layer [%r] with name - %r>' % (self.layer_type, self.name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

# admin


class Module(Base):
    __tablename__ = 'module_meta'
    __table_args__ = {'schema': 'system_config'}

    module_id = Column(Integer, primary_key=True,server_default=text(
        "nextval('system_config.module_meta_module_id'::regclass)"))
    name = Column('module_name', String(50))
    description = Column(String(250))
    created_by = Column('audit_insrt_id', nullable=False)
    created_date = Column('audit_insrt_dt', nullable=False)
    modified_date = Column('audit_updt_dt', DateTime)
    modified_by = Column('audit_updt_id')
    is_active = Column('is_actv', Boolean)

    #user = relationship('User', primaryjoin='Module.created_by == User.user_id')
    #user1 = relationship('User', primaryjoin='Module.modified_by == User.user_id')


class UserGroup(Base):
    __tablename__ = 'usergrp'
    __table_args__ = {'schema': 'system_config'}

    name = Column('usergrp_name', String(50), unique=True)
    description = Column(String(400))
    is_usergroup_actv = Column('is_ug_actv', Boolean)
    is_active = Column('is_actv', Boolean)
    created_date = Column('audit_insrt_dt', DateTime, nullable=False)
    modified_date = Column('audit_updt_dt', DateTime)
    is_deleted = Column(Boolean)
    group_type = Column('grp_typ', String(1))
    usergroup_id = Column('usergrp_id',Integer, primary_key=True,
                   server_default=text("nextval('system_config.usergrp_usergrp_id'::regclass)"))
    # usergroup_id = Column('usergrp_id',primary_key=True, Integer,
    #     server_default=text("nextval('system_config.usergrp_usergrp_id'::regclass)"))
    created_by = Column('audit_insrt_id')
    modified_by = Column('audit_updt_id')

    #user = relationship('User', primaryjoin='UserGroup.created_by == User.user_id')
    #user1 = relationship('User', primaryjoin='UserGroup.modified_by == User.user_id')


class FunctionalityMaster(Base):
    __tablename__ = 'module_fnctnlty'
    __table_args__ = {'schema': 'system_config'}

    functionality_id = Column('module_fnctnlty_id', Integer, primary_key=True, server_default=text(
        "nextval('system_config.module_fnctnlty_module_fnctnlty_id'::regclass)"))
    name = Column('functionality_name', String(250), unique=True)
    description = Column(String(400))
    module_id = Column(Integer)
    is_active = Column('is_actv', Boolean)

    #module = relationship('Module')


class ModuleLayerMapping(Base):
    __tablename__ = 'module_data_lyr'
    __table_args__ = {'schema': 'system_config'}

    module_layer_mapping_id = Column('module_data_lyr_id', Integer, primary_key=True, server_default=text(
        "nextval('system_config.module_data_lyr_module_data_lyr_id'::regclass)"))
    module_id = Column(Integer)
    layer_id = Column('lyr_id', Integer)
    created_by = Column('audit_insrt_id', nullable=False)
    created_date = Column('audit_insrt_dt', DateTime, nullable=False)
    modified_date = Column('audit_updt_dt', DateTime)
    modified_by = Column('audit_updt_id')
    is_active = Column('is_actv', Boolean)

    #user = relationship('User', primaryjoin='ModuleLayerMapping.created_by == User.user_id')
    #layer = relationship('Layer')
    #user1 = relationship('User', primaryjoin='ModuleLayerMapping.modified_by == User.user_id')
    #module = relationship('Module')


class UserGroupFunctionalityMapping(Base):
    __tablename__ = 'usergrp_mdl_fnctnlty'
    __table_args__ = {'schema': 'system_config'}

    usergroup_functionality_mapping_id = Column('usergrp_mdl_fnclty_id', Integer, primary_key=True,server_default=text(
        "nextval('system_config.usergrp_mdl_fnctnlty_usergrp_mdl_fnclty_id'::regclass)"))
    functionality_id = Column('module_fnctnlty_id', Integer, nullable=False)
    usergroup_id = Column('usergrp_id', Integer)
    created_by = Column('audit_insrt_id', Integer, nullable=False)
    created_date = Column('audit_insrt_dt', DateTime, nullable=False)
    modified_date = Column('audit_updt_dt', DateTime)
    modified_by = Column('audit_updt_id', Integer)
    is_active = Column('is_actv', Boolean)

    #user = relationship('User', primaryjoin='UserGroupFunctionalityMapping.created_by == User.user_id')
    #functionality = relationship('FunctionalityMaster')
    #user1 = relationship('User', primaryjoin='UserGroupFunctionalityMapping.modified_by == User.user_id')
    #usergroup = relationship('UserGroup')

usergroupuserseq = Sequence('usergrp_user_usergrp_user_id', schema='system_config',metadata=metadata)
class UserGroupUserMapping(Base):
    __tablename__ = 'usergrp_user'
    __table_args__ = {'schema': 'system_config'}

    usergroup_id = Column('usergrp_id', Integer)
    user_id = Column('user_id', Integer)
    created_by = Column('audit_insrt_id', Integer, nullable=False)
    created_date = Column('audit_insrt_dt', DateTime, nullable=False)
    modified_date = Column('audit_updt_dt', DateTime)
    modified_by = Column('audit_updt_id', Integer)
    is_active = Column('is_actv', Boolean,default=True)
    # usergroup_user_mapping_id = Column('usergrp_user_id', Integer,primary_key=True, server_default=text(
    #   "nextval('system_config.usergrp_user_usergrp_user_id'::regclass)"))
    # user = relationship('User', primaryjoin='UserGroupUserMapping.created_by == User.user_id')
    # user1 = relationship('User', primaryjoin='UserGroupUserMapping.modified_by == User.user_id')
    # user2 = relationship('User', primaryjoin='UserGroupUserMapping.user_id == User.user_id')
    # usergroup = relationship('UserGroup')
    usergroup_user_mapping_id = Column('usergrp_user_id',usergroupuserseq,primary_key=True, server_default = usergroupuserseq.next_value())

class UserGroupLayerMapping(Base):
    __tablename__ = 'usergrp_data_lyr'
    __table_args__ = {'schema': 'system_config'}

    usergroup_layer_mapping_id = Column('usergrp_data_lyr_id', Integer, primary_key=True, server_default=text(
        "nextval('system_config.usergrp_data_lyr_usergrp_data_lyr_id'::regclass)"))
    layer_id = Column('data_lyr_id', Integer)
    usergroup_id = Column('usergroup_id', Integer)
    created_by = Column('audit_insrt_id', Integer, nullable=False)
    created_date = Column('audit_insrt_dt', DateTime, nullable=False)
    modified_date = Column('audit_updt_dt', DateTime)
    modified_by = Column('audit_updt_id', Integer)
    is_active = Column('is_actv', Boolean)

    # user = relationship('User', primaryjoin='UserGroupLayerMapping.created_by == User.user_id')
    # layer = relationship('Layer')
    # user1 = relationship('User', primaryjoin='UserGroupLayerMapping.modified_by == User.user_id')
    # usergroup = relationship('UserGroup')


# On-boarding/Ingestion

class ObjectMeta(Base):
    __tablename__ = 'object_meta'
    __table_args__ = {'schema': 'system_config'}

    obj_id = Column(Integer, primary_key=True,
                    server_default=text("nextval('system_config.object_meta_obj_id_seq'::regclass)"))
    obj_schema = Column(String(45))
    obj_nm = Column(String(100))
    obj_typ = Column(String(45))
    obj_desc = Column(String(256))
    obj_src_nm = Column(String(256))
    obj_layer_nm = Column(String(45))
    obj_delimiter = Column(String(45))
    obj_text_qualifier = Column(String(45))
    obj_extension = Column(String(45))
    is_header = Column(String(1))
    is_encoding = Column(String(1))
    frequency = Column(String(45))
    mstr_obj_id = Column(Integer)
    country_code = Column(String(2))
    business_unit_code = Column(String(50))
    language_code = Column(String(50))
    std_file_name = Column(String(500))
    obj_encoding = Column(String(45))
    obj_pattern_fmt = Column(String(45))
    obj_provider_nm = Column(String(100))
    sample_file_name = Column(String(50))
    file_arrival_start_day = Column(Integer)
    file_arrival_end_day = Column(Integer)
    # file_arrival_time = Column(Time)
    data_persist = Column(String(5))
    created_by = Column(String(50))
    # created_date = Column(DateTime)
    last_updated_by = Column(String(50))
    # last_updated_date = Column(DateTime)
    ref_obj_id = Column(Integer)
    # effctv_strt_dt = Column(Date)
    # effctv_end_dt = Column(Date)
    is_active = Column(String(1))
    # obj_uri_id = Column(Integer)
    # version = Column(Integer)

    def __repr__(self):
        return '<ObjectMeta [%r] with name - %r>' % (self.obj_id, self.obj_nm)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class ObjectMetaExt(Base):
    __tablename__ = 'object_meta_ext'
    __table_args__ = {'schema': 'system_config'}

    obj_id = Column(Integer, primary_key=True)
    file_typ = Column(String(45))
    ext_file_name = Column(String(100))

    def __repr__(self):
        return '<ObjectMetaExt [%r] with name - %r>' % (self.obj_id, self.ext_file_name)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class ObjectPOCMeta(Base):
    __tablename__ = 'object_poc_meta'
    __table_args__ = {'schema': 'system_config'}

    obj_id = Column(Integer, nullable=False, primary_key=True)
    poc_nm = Column(String(45))
    poc_email = Column(String(100))
    poc_ph = Column(String(45))
    created_by = Column(String(50))
    # created_date = Column(DateTime)
    last_updated_by = Column(String(50))
    # last_updated_date = Column(DateTime)
    # effctv_strt_dt = Column(Date)
    # effctv_end_dt = Column(Date)
    is_active = Column(String(1))
    id = Column(Integer, nullable=False,
                server_default=text("nextval('system_config.object_poc_meta_id_seq'::regclass)"))
    poc_group = Column(String(25))
    # obj_uri_id = Column(Integer)

    def __repr__(self):
        return '<ObjectPOCMeta [%r] with name - %r>' % (self.obj_id, self.poc_nm)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class ObjectConnectionMeta(Base):
    __tablename__ = 'object_connection_meta'
    __table_args__ = {'schema': 'system_config'}

    id = Column(Integer, nullable=False,
                server_default=text(
                    "nextval('system_config.object_connection_meta_id_seq'::regclass)"),
                primary_key=True)
    obj_id = Column(Integer, nullable=False)
    conn_id = Column(Integer)
    user_id = Column(String(20))
    user_type = Column(String(20))
    folder_path = Column(String(200))
    created_by = Column(String(50))
    # created_date = Column(DateTime)
    last_updated_by = Column(String(50))
    # last_updated_date = Column(DateTime)
    # effctv_strt_dt = Column(Date)
    # effctv_end_dt = Column(Date)
    is_active = Column(String(1))
    conn_flg = Column(String(20))
    # obj_uri_id = Column(Integer)

    def __repr__(self):
        return '<ObjectConnectionMeta [%r] with conn - %r>' % (self.obj_id, self.conn_id)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class AttributeMeta(Base):
    __tablename__ = 'attribute_meta'
    __table_args__ = {'schema': 'system_config'}

    attr_id = Column(Integer, primary_key=True,
                     server_default=text("nextval('system_config.attribute_meta_attr_id_seq'::regclass)"))
    obj_id = Column(Integer)
    attr_nm = Column(String(256))
    attr_data_typ = Column(String(45))
    attr_desc = Column(String(256))
    col_loc = Column(Integer)
    mstr_attr_id = Column(Integer)
    mstr_obj_id = Column(Integer)
    business_attr_nm = Column(String(50))
    etl_attr_nm = Column(String(50))
    created_by = Column(String(50))
    # created_date = Column(DateTime)
    last_updated_by = Column(String(50))
    # last_updated_date = Column(DateTime)
    is_mandatory = Column(String(1))
    # effctv_strt_dt = Column(Date)
    # effctv_end_dt = Column(Date)
    is_active = Column(String(1))
    attr_frmt = Column(String(4000))
    # obj_uri_id = Column(Integer)

    def __repr__(self):
        return '<AttributeMeta [%r] with name - %r>' % (self.obj_id, self.attr_nm)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class FileRegister(Base):
    __tablename__ = 'file_register'
    __table_args__ = {'schema': 'system_config'}

    file_inst_id = Column(Integer, nullable=False,
                          server_default=text(
                              "nextval('system_config.file_register_file_inst_id_seq'::regclass)"),
                          primary_key=True)
    obj_id = Column(Integer)
    file_nm = Column(String(1000))
    # actual_arrival_time = Column(DateTime)
    ingestion_flag = Column(String(5))
    sla_flag = Column(String(5))
    file_notice_out = Column(String(5))
    delay_notice_in = Column(String(5))
    user_id = Column(String(100))
    # created_date = Column(DateTime)
    file_status_code = Column(String(5))
    file_source = Column(String(20))

    def __repr__(self):
        return '<FileRegister [%r] with name - %r>' % (self.obj_id, self.file_nm)

    @property
    def serialize(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


# Event Notification

# class EventMeta(Base):
#     __tablename__ = 'event_meta'
#     __table_args__ = {'schema': 'admn'}

#     event_id = Column(Integer, primary_key=True, server_default=text("nextval('admn.event_meta_event_id_seq'::regclass)"))
#     module_id =  Column(Integer) #Column(ForeignKey('admn.module.module_id'))
#     event_name = Column(String(50)) # Column(String(50), unique=True)
#     event_api_name = Column(String(50))
#     event_desc = Column(String(400))
#     event_type = Column(String(50))
#     recipient_usr_flag = Column(Boolean)
#     recipient_grp_flag = Column(Boolean)
#     recipient_grp_all_flag = Column(Boolean)
#     is_active = Column(Boolean)
#     sender_email = Column(String(100))
#     api_before_event = Column(String(200))
#     api_after_event = Column(String(200))
#     created_date = Column(DateTime, server_default=func.now())
#     modified_date = Column(DateTime,server_default=func.now(), onupdate=func.now())
#     created_by = Column(Integer) #Column(ForeignKey('admn.user.user_id'))
#     modified_by =  Column(Integer) #Column(ForeignKey('admn.user.user_id'))
#     recipient_to_usr = Column(String(200)) # Column(String(50), unique=True)
#     recipient_to_grp = Column(String(200))
#     recipient_cc_usr = Column(String(200))
#     recipient_cc_grp = Column(String(200))

#     '''def __repr__(self):
#         return '<Event Meta [%r] with name - %r>' % (self.event_id, self.event_name)

#     @property
#     def serialize(self):
#         return {c.name: getattr(self, c.name) for c in self.__table__.columns}'''


# class Event_Instance(Base):
#     __tablename__ = 'event_instance'
#     __table_args__ = {'schema': 'admn'}

#     event_instance_id = Column(Integer, primary_key=True, server_default=text(
#         "nextval('admn.event_instance_event_instance_id_seq'::regclass)"))
#     event_id =  Column(Integer) #Column(ForeignKey('admn.module.module_id'))
#     recipient_to_usr = Column(String) # Column(String(50), unique=True)
#     recipient_to_grp = Column(String)
#     recipient_to_ext = Column(String)
#     recipient_cc_usr = Column(String(200))
#     recipient_cc_grp = Column(String(200))
#     recipient_cc_ext = Column(String(200))
#     recipient_grp_all_flag = Column(Boolean)
#     usrgrp_email_mapping = Column(String)
#     content = Column(String)
#     title = Column(String)
#     server_response = Column(String)
#     sent_status = Column(String)
#     is_active = Column(Boolean)
#     created_date = Column(DateTime, server_default=func.now())
#     modified_date = Column(DateTime,server_default=func.now(), onupdate=func.now())
#     created_by = Column(Integer) #Column(ForeignKey('admn.user.user_id'))
#     modified_by =  Column(Integer) #Column(ForeignKey('admn.user.user_id'))
#     sent_on = Column(DateTime)

# class Template_Meta(Base):
#     __tablename__ = 'template_meta'
#     __table_args__ = {'schema': 'admn'}

#     template_id = Column(Integer, primary_key=True, server_default=text(
#         "nextval('admn.template_meta_template_id_seq'::regclass)"))
#     event_id = Column(Integer) # Column(String(50), unique=True)
#     template_body = Column(String(200))
#     template_title = Column(String(100))
#     is_active = Column(Boolean)
#     created_date = Column(DateTime, server_default=func.now())
#     modified_date = Column(DateTime,server_default=func.now(), onupdate=func.now())
#     created_by = Column(Integer)
#     modified_by =  Column(Integer)


# class Notification_Queue(Base):
#     __tablename__ = 'notification_queue'
#     __table_args__ = {'schema': 'admn'}

#     notification_id = Column(Integer, primary_key=True, server_default=text(
#         "nextval('admn.notification_queue_notification_id_seq'::regclass)"))
#     mail_to = Column(String(100))
#     mail_cc = Column(String(100))
#     mail_subject = Column(String(100))
#     mail_body =  Column(String(1000))
#     mail_sender = Column(String(100))
#     mail_format = Column(String(10))
#     status = Column(String(40))
#     server_response = Column(String(100))
#     created_date = Column(DateTime, server_default=func.now())
#     modified_date = Column(DateTime,server_default=func.now(), onupdate=func.now())
#     created_by = Column(Integer)
#     modified_by =  Column(Integer)


# class DataLayer(Base):
#     __tablename__ = 'data_layer_meta'
#     __table_args__ = {'schema': 'system_config'}

#     data_lyr_id = Column(Integer, primary_key=True,server_default=text("nextval('system_config.object_data_layer_meta_obj_data_layer_id'::regclass)"))
#     conn_id = Column(ForeignKey('system_config.connection_meta.id'))
#     tenant_id = Column(Integer)
#     data_lyr_name = Column(String(50))
#     lyr_logical_type = Column(String(10))
#     description = Column(String(250))
#     s3_path = Column(String(200))
#     schema_name = Column(String(50))
#     is_actv = Column(Boolean)
#     eff_srt_dt = Column(DateTime)
#     eff_end_dt = Column(DateTime)
#     audit_batch_id = Column(String(8))
#     audit_insrt_dt = Column(DateTime)
#     audit_insrt_id = Column(DateTime)
#     audit_updt_dt = Column(DateTime)
#     audit_updt_id = Column(DateTime)

#     def __repr__(self):
#         return '<Layer [%r] with name - %r>' % (self.lyr_logical_type, self.name)

#     @property
#     def serialize(self):
#         return {c.name: getattr(self, c.name) for c in self.__table__.columns}
