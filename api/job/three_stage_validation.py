#   datamax/api/job/three_stage_validation.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from sqlalchemy.orm import Session

from services.data_movement.redshift_to_s3 import RedshiftToS3
from services.data_movement.s3_to_redshift import S3ToRedshift
from services.data_validator.redshift_validation_engine import RedshiftValidationEngine
from services.data_validator.s3_validation_engine import S3ValidationEngine
from services.data_validator.validation_response import ConstructSuccessRecords, ConstructFailedRecords
from vendor.appdb import AppDb


class ThreeStageValidation:
    def __init__(self, **kwargs):
        self.session = Session(AppDb.engine)
        self.kwargs = kwargs
        self.kwargs.update(session=self.session)

    def execute(self):
        service_3 = S3ValidationEngine(**{**self.kwargs, "stage": 1,
                                          "target_bucket": self.kwargs["source_bucket"],
                                          "target_file_path": self.kwargs.get("header_object")})
        service_3.execute()

        service_4 = S3ToRedshift(**self.kwargs, destination_schema="temp")
        service_4.execute()

        service_5 = RedshiftValidationEngine(**{**self.kwargs,
                                                "stage": 2, "rule_type": "system_defined",
                                                "config_instance_id": service_3.response.get("config_instance")[0].get(
                                                    "id"),
                                                "target_schema": "temp",
                                                "target_table": service_4.response.get("table_name")})
        service_5.execute()

        service_6 = ConstructSuccessRecords(**{**self.kwargs, "success_schema": "validation", "stage": 2,
                                               "config_instance_id": service_3.response.get("config_instance")[0].get(
                                                   "id"),
                                               "success_table": service_5.response.get('config_instance')[1].get(
                                                   'target_table')})
        service_6.execute()

        service_7 = RedshiftValidationEngine(**{**self.kwargs, "rule_type": "user_defined",
                                                "stage": 3,
                                                "config_instance_id": service_3.response.get("config_instance")[0].get(
                                                    "id"),
                                                "target_schema": service_6.response.get("config_instance")[1].get(
                                                    "success_schema_name"),
                                                "target_table": service_6.response.get("config_instance")[1].get(
                                                    "success_table_name")})
        service_7.execute()

        service_8 = ConstructSuccessRecords(**{**self.kwargs, "success_schema": "validation", "stage": 3, "shape": None,
                                               "config_instance_id": service_3.response.get("config_instance")[0].get(
                                                   "id")})
        service_8.execute()

        service_9 = ConstructFailedRecords(**{**self.kwargs, "failed_schema": "validation", "stage": 3,
                                              "config_instance_id": service_3.response.get("config_instance")[0].get(
                                                  "id")})
        service_9.execute()

        service_10 = RedshiftToS3(**{**self.kwargs, "source_schema": service_9.response.get("config_instance")[2].get(
            "success_schema_name"), "source_table": service_9.response.get("config_instance")[2].get(
            "success_table_name")})
        service_10.execute()

        service_11 = RedshiftToS3(**{**self.kwargs, "source_schema": service_9.response.get("config_instance")[2].get(
            "failure_schema_name"),
            "source_table": service_9.response.get("config_instance")[2].get("failure_table_name")
        })
        service_11.execute()
        self.session.close()
