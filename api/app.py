#   datamax/api/app.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import logging
from datetime import date
from logging.handlers import TimedRotatingFileHandler

from flask import Flask, request, jsonify
from flask.json import JSONEncoder
from flask.logging import default_handler
from flask_sqlalchemy import SQLAlchemy
from werkzeug.local import LocalProxy

from models import metadata


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


app = Flask(__name__)
app.json_encoder = CustomJSONEncoder

app.config.from_pyfile('config.py')
app.url_map.strict_slashes = False
prefix = app.config.get('APPLICATION_URL')
secret = app.config['SECRET_KEY']

smtphost = app.config['SMTP_HOST']
glasalluseremail = app.config['GLAS_IP_ALL_EMAIL']

db = SQLAlchemy(metadata=metadata)
db.init_app(app)


class RequestFormatter(logging.Formatter):
    def format(self, record):
        record.url = request.url
        record.remote_addr = request.remote_addr
        return super().format(record)


formatter = RequestFormatter('[%(asctime)s]  - %(levelname)s  - [%(remote_addr)s] -  %(name)s - %(message)s')
default_handler.setFormatter(formatter)

logger = LocalProxy(lambda: app.logger)


@app.route(prefix + "/")
def app_index():
    app.logger.info("enter")
    return "DataMax"


@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return jsonify(message="API does not exist.", traceback="404"), 404


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type, Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response


@app.errorhandler(Exception)
def all_exception_handler(error):
    logging.exception(error)
    return jsonify({"message": "run-time exception", "traceback": str(error)}), 500


@app.before_request
def before_request():
    logging.info('Request ::: %r' % request.url)


from controller.config import config as t_config
from controller.config_instance import config_instance
from controller.connector import connector
from controller.files import files
from controller.project import project
from controller.rules import rules
from controller.validation import validation
from controller.data_movement import data_movement
from controller.operations import operations
from controller.docs import docs
from controller.exchange import api_exchange
from controller.broker import broker
from controller.data_quality import data_quality
from controller.catalog import catalog
from controller.brms import brms
from controller.brms_validation import brms_validation
from controller.data_ingestion import data_ingestion
from controller.notification import notification

from views.exchange import views_exchange
from views.validation import views_validation

# APIs
app.register_blueprint(project, url_prefix=prefix + '/api/project')
app.register_blueprint(connector, url_prefix=prefix + '/api/connector')
app.register_blueprint(files, url_prefix=prefix + '/api/files')
app.register_blueprint(rules, url_prefix=prefix + '/api/rules')
app.register_blueprint(t_config, url_prefix=prefix + '/api/config')
app.register_blueprint(config_instance, url_prefix=prefix + '/api/config_instance')
app.register_blueprint(validation, url_prefix=prefix + '/api/validation')
app.register_blueprint(data_movement, url_prefix=prefix + '/api/data_movement')
app.register_blueprint(operations, url_prefix=prefix + '/api/operations')
app.register_blueprint(docs, url_prefix=prefix + '/api/docs')
app.register_blueprint(broker, url_prefix=prefix + '/api/broker')
app.register_blueprint(data_quality, url_prefix=prefix + '/api/data_quality')
app.register_blueprint(catalog, url_prefix=prefix + '/api/catalog')
app.register_blueprint(api_exchange, url_prefix=prefix + '/api/exchange')
app.register_blueprint(brms, url_prefix=prefix + '/api/brms')
app.register_blueprint(brms_validation, url_prefix=prefix + '/api/brms_validation')
app.register_blueprint(data_ingestion, url_prefix=prefix + '/api/data_ingestion')
app.register_blueprint(notification, url_prefix=prefix + '/api/notification')

# Views
app.register_blueprint(views_exchange, url_prefix=prefix + '/views/exchange')
app.register_blueprint(views_validation, url_prefix=prefix + '/views/validation')

logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("logs/system.log", when="midnight", interval=1),
                              logging.StreamHandler()],
                    format='%(asctime)s  - %(levelname)s  - %(name)s - %(message)s')

logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('s3transfer').setLevel(logging.CRITICAL)
logging.getLogger('urllib3').setLevel(logging.CRITICAL)
logging.getLogger('s3fs').setLevel(logging.CRITICAL)
logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
logging.getLogger('sqlalchemy.engine').setLevel(logging.CRITICAL)
logging.getLogger('neobolt').setLevel(logging.CRITICAL)
logging.getLogger('snowflake').setLevel(logging.CRITICAL)

# Snowflake Incremental Scnaner BEGINS
from flask_apscheduler import APScheduler
from config import SF_INC_SCAN_INTERVAL
from services.catalog.snowflake_incremental_scan import snowflake_incremental_scan_scheduled

scheduler = APScheduler()
scheduler.init_app(app)
scheduler.start()

@app.before_first_request
def scanner_scheduler():
    scheduler.add_job(func=snowflake_incremental_scan_scheduled,
                         trigger="interval", seconds=SF_INC_SCAN_INTERVAL, id='SF_INC_SCAN', max_instances=1)
# Snowflake Incremental Scnaner ENDS

if __name__ == '__main__':
    app.run(port=app.config.get('PORT'), debug=True)

