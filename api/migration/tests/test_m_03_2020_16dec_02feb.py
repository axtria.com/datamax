from migration.m_03_2020_26dec_02feb import adaptor


def test_pre_workflow():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': []}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT',
                         'health': True},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': []}
    assert actual == expected


def test_post_workflow():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT',
                         'health': False},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': []}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT',
                         'health': False},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': []}
    assert actual == expected
