from migration.m_01_2019_27nov_15dec import adaptor


def test_empty_workflow():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': []}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': []}
    assert actual == expected


def test_with_selected_datasets_workflow():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {
                    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
                            {'column_name': 'c2', 'data_type': 'varchar'},
                            {'column_name': 'c3', 'data_type': 'numeric'},
                            {'column_name': 'c4', 'data_type': 'varchar'}],

                    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
                            {'column_name': 'c2', 'data_type': 'varchar'},
                            {'column_name': 'c3', 'data_type': 'numeric'},
                            {'column_name': 'c4', 'data_type': 'varchar'},
                            {'column_name': 'c5', 'data_type': 'varchar'}]

                },
                'PARAMETERS': {},
                'RULES': []}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {
                    'ds1': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'}],
                            "schema": ""},
                    'ds2': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'c5', 'data_type': 'varchar'}],
                            "schema": ""}
                },
                'PARAMETERS': {},
                'RULES': []}
    assert actual == expected


def test_with_already_migrated_datasets_workflow():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {
                    'ds1': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'}],
                            "schema": ""},
                    'ds2': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'c5', 'data_type': 'varchar'}],
                            "schema": ""}
                },
                'PARAMETERS': {},
                'RULES': []}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {
                    'ds1': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'}],
                            "schema": ""},
                    'ds2': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'c5', 'data_type': 'varchar'}],
                            "schema": ""}
                },
                'PARAMETERS': {},
                'RULES': []}
    assert actual == expected
