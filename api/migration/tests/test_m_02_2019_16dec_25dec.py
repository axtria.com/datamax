from migration.m_02_2019_16dec_25dec import adaptor


def test_empty_workflow():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': []}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': []}
    assert actual == expected


def test_aggregate_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'aggregate': {'group_by': ['c1', 'c2'],
                                   'agg_expr': [{'formula': 'sum(nrx)',
                                                 'column_alias': 'sum_nrx',
                                                 'data_type': 'double'}]},
                     'input': 'ds1',
                     'query': 'select c1, c2, cast(sum(nrx) as double) as sum_nrx from '
                              '{schema_ds1}.{ds1} group by c1, c2',
                     'parameters': [],
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'sum_nrx', 'data_type': 'double', 'derived': True}]
                     }
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'aggregate': {'group_by': ['c1', 'c2'],
                                   'agg_expr': [{'formula': 'sum(nrx)',
                                                 'column_alias': 'sum_nrx',
                                                 'data_type': 'double'}]},
                     'input': 'ds1',
                     'query': 'select c1, c2, cast(sum(nrx) as double) as sum_nrx from '
                              '{schema_ds1}.{ds1} group by c1, c2',
                     'parameters': [],
                     'rule_type': 'aggregate',
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'sum_nrx', 'data_type': 'double', 'derived': True}]
                     }
                ]}
    assert actual == expected


def test_analytical_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'input': 'ds2',
                     'analytical': {'function_name': 'cumulative_sum', 'column': 'c2',
                                    'group_by': ['c3', 'c4'], 'order_column': 'C5', 'order_type': 'asc',
                                    'dtype': 'numeric',
                                    'alias': 'new_column'},
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'c3', 'data_type': 'numeric'},
                                          {'column_name': 'c4', 'data_type': 'varchar'},
                                          {'column_name': 'C5', 'data_type': 'varchar'},
                                          {'column_name': 'new_column', 'data_type': 'numeric', 'derived': True}],
                     'query': 'select *, cast(cumulative_sum(c2) over(partition by c3,c4 order by c5 asc rows '
                              'unbounded preceding) as numeric) '
                              'as new_column from {schema_ds2}.{ds2}',
                     'parameters': []}
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'input': 'ds2',
                     'analytical': {'function_name': 'cumulative_sum', 'column': 'c2',
                                    'group_by': ['c3', 'c4'], 'order_column': 'C5', 'order_type': 'asc',
                                    'dtype': 'numeric',
                                    'alias': 'new_column'},
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'c3', 'data_type': 'numeric'},
                                          {'column_name': 'c4', 'data_type': 'varchar'},
                                          {'column_name': 'C5', 'data_type': 'varchar'},
                                          {'column_name': 'new_column', 'data_type': 'numeric', 'derived': True}],
                     'query': 'select *, cast(cumulative_sum(c2) over(partition by c3,c4 order by c5 asc rows '
                              'unbounded preceding) as numeric) '
                              'as new_column from {schema_ds2}.{ds2}',
                     'rule_type': 'analytical',
                     'parameters': []}
                ]}
    assert actual == expected


def test_custom_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'custom': {'name': 'public.fn_custom_function', 'parameters': ['p1'],
                                'columns': [{'column_name': 'c3', 'data_type': 'float', 'derived': True}]},
                     '_output_metadata': [{'column_name': 'c3', 'data_type': 'float', 'derived': True},
                                          {'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'}],
                     'input': ['ds1'],
                     'query': 'select * from "public.fn_custom_function" (p1) ',
                     'parameters': []}
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'custom': {'name': 'public.fn_custom_function', 'parameters': ['p1'],
                                'columns': [{'column_name': 'c3', 'data_type': 'float', 'derived': True}]},
                     '_output_metadata': [{'column_name': 'c3', 'data_type': 'float', 'derived': True},
                                          {'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'}],
                     'input': ['ds1'],
                     'query': 'select * from "public.fn_custom_function" (p1) ',
                     'rule_type': 'custom',
                     'parameters': []}
                ]}
    assert actual == expected


def test_dedupe_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'input': 'ds1', 'dedupe': ['c1', 'c2'],
                     'query': 'select A.* from {schema_ds1}.{ds1} A inner join (select c1, c2, count(*) from '
                              '{schema_ds1}.{ds1} group by '
                              'c1, c2 having count(*) = 1) B on A.c1 = B.c1 and A.c2 = B.c2',
                     'parameters': [],
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'}]
                     }
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'input': 'ds1', 'dedupe': ['c1', 'c2'],
                     'query': 'select A.* from {schema_ds1}.{ds1} A inner join (select c1, c2, count(*) from '
                              '{schema_ds1}.{ds1} group by '
                              'c1, c2 having count(*) = 1) B on A.c1 = B.c1 and A.c2 = B.c2',
                     'parameters': [],
                     'rule_type': 'dedupe',
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'}]
                     }
                ]}
    assert actual == expected


def test_derived_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'input': 'ds1',
                     'derived': [{'formula': 'sales/goals',
                                  'column_alias': 'attainment',
                                  'data_type': 'double'}],
                     'query': 'select *, cast(sales/goals as double) as attainment from {schema_ds1}.{ds1}',
                     'parameters': [],
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'attainment', 'data_type': 'double', 'derived': True}]
                     }
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'input': 'ds1',
                     'derived': [{'formula': 'sales/goals',
                                  'column_alias': 'attainment',
                                  'data_type': 'double'}],
                     'query': 'select *, cast(sales/goals as double) as attainment from {schema_ds1}.{ds1}',
                     'parameters': [],
                     'rule_type': 'derived',
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'attainment', 'data_type': 'double', 'derived': True}]
                     }
                ]}
    assert actual == expected


def test_filter_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'filter': {'rules': [{'COLUMN': 'c1', 'OPERATOR': 'like', 'VALUE': "'%segment%'"}]},
                     'input': 'ds1',
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'}],
                     'query': "select * from {schema_ds1}.{ds1} where c1 like '%segment%'",
                     'parameters': []}
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'filter': {'rules': [{'COLUMN': 'c1', 'OPERATOR': 'like', 'VALUE': "'%segment%'"}]},
                     'input': 'ds1',
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'}],
                     'query': "select * from {schema_ds1}.{ds1} where c1 like '%segment%'",
                     'rule_type': 'filter', 'parameters': []}
                ]}
    assert actual == expected


def test_join_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'input': ['ds1', 'ds2'],
                     'join': {
                         'rules': [
                             {'join_input': 'ds2', 'join_type': 'left', 'join_expr': [('ds1.c2', '=', 'ds2.c3')]}],
                         'columns': [('ds1.c1', 'c1'), ('ds1.c2', 'c2'), ('ds2.C5', 'C5')]},
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'C5', 'data_type': 'varchar'}],
                     'query': 'select {schema_ds1}.{ds1}.c1 as c1, {schema_ds1}.{ds1}.c2 as c2, '
                              '{schema_ds2}.{ds2}.C5 as C5 from {schema_ds1}.{ds1} left join {schema_ds2}.{ds2} '
                              'on {schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3',
                     'parameters': []}
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'input': ['ds1', 'ds2'],
                     'join': {
                         'rules': [
                             {'join_input': 'ds2', 'join_type': 'left', 'join_expr': [('ds1.c2', '=', 'ds2.c3')]}],
                         'columns': [('ds1.c1', 'c1'), ('ds1.c2', 'c2'), ('ds2.C5', 'C5')]},
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'C5', 'data_type': 'varchar'}],
                     'query': 'select {schema_ds1}.{ds1}.c1 as c1, {schema_ds1}.{ds1}.c2 as c2, '
                              '{schema_ds2}.{ds2}.C5 as C5 from {schema_ds1}.{ds1} left join {schema_ds2}.{ds2} '
                              'on {schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3',
                     'rule_type': 'join',
                     'parameters': []}
                ]}
    assert actual == expected


def test_partition_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {
                        'partition': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx',
                                       'partition_by': {'columns': ['c1', 'c2']}}],
                        'input': 'ds1', '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                                             {'column_name': 'c2', 'data_type': 'varchar'},
                                                             {'column_name': 'sum_nrx', 'data_type': 'numeric',
                                                              'derived': True}],
                        'query': 'select *, sum(nrx) over (partition by c1, c2) as sum_nrx from {schema_ds1}.{ds1}',
                        'parameters': []}
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {
                        'partition': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx',
                                       'partition_by': {'columns': ['c1', 'c2']}}],
                        'input': 'ds1', '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                                             {'column_name': 'c2', 'data_type': 'varchar'},
                                                             {'column_name': 'sum_nrx', 'data_type': 'numeric',
                                                              'derived': True}],
                        'query': 'select *, sum(nrx) over (partition by c1, c2) as sum_nrx from {schema_ds1}.{ds1}',
                        'rule_type': 'partition',
                        'parameters': []}
                ]}
    assert actual == expected


def test_union_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'union': {'rs1': ['columns', 'c1', 'c2'], 'rs2': ['columns', 'c4', 'C5'],
                               'columns': [('rs1.c1', 'c1'), ('rs1.c2', 'c2')]},
                     'input': ['rs1', 'rs2'],
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}],
                     'query': 'select {schema_rs1}.{rs1}.c1 as c1, {schema_rs1}.{rs1}.c2 as c2 from {schema_rs1}.{rs1} '
                              'union select columns, c4, C5 from {schema_rs2}.{rs2}',
                     'parameters': []}
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'union': {'rs1': ['columns', 'c1', 'c2'], 'rs2': ['columns', 'c4', 'C5'],
                               'columns': [('rs1.c1', 'c1'), ('rs1.c2', 'c2')]},
                     'input': ['rs1', 'rs2'],
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}],
                     'query': 'select {schema_rs1}.{rs1}.c1 as c1, {schema_rs1}.{rs1}.c2 as c2 from {schema_rs1}.{rs1} '
                              'union select columns, c4, C5 from {schema_rs2}.{rs2}',
                     'rule_type': 'union',
                     'parameters': []}
                ]}
    assert actual == expected


def test_already_migrated_rule():
    workflow = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'aggregate': {'group_by': ['c1', 'c2'],
                                   'agg_expr': [{'formula': 'sum(nrx)',
                                                 'column_alias': 'sum_nrx',
                                                 'data_type': 'double'}]},
                     'input': 'ds1',
                     'query': 'select c1, c2, cast(sum(nrx) as double) as sum_nrx from '
                              '{schema_ds1}.{ds1} group by c1, c2',
                     'parameters': [],
                     'rule_type': 'aggregate',
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'sum_nrx', 'data_type': 'double', 'derived': True}]
                     }
                ]}
    actual = adaptor(workflow)
    expected = {'META': {'workflow_id': '2ddbb980967d4c47a9191bf362b1d378',
                         'name': '', 'description': '',
                         'workspace_id': '1', 'status': 'DRAFT'},
                'DATASETS': {},
                'SELECTED_DATASETS': {},
                'PARAMETERS': {},
                'RULES': [
                    {'aggregate': {'group_by': ['c1', 'c2'],
                                   'agg_expr': [{'formula': 'sum(nrx)',
                                                 'column_alias': 'sum_nrx',
                                                 'data_type': 'double'}]},
                     'input': 'ds1',
                     'query': 'select c1, c2, cast(sum(nrx) as double) as sum_nrx from '
                              '{schema_ds1}.{ds1} group by c1, c2',
                     'parameters': [],
                     'rule_type': 'aggregate',
                     '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                          {'column_name': 'c2', 'data_type': 'varchar'},
                                          {'column_name': 'sum_nrx', 'data_type': 'double', 'derived': True}]
                     }
                ]}
    assert actual == expected
