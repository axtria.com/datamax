import logging

from sqlalchemy.orm import Session

from models import Workflow as tWorkflow, ValidationWorkflow as tValidationWorkflow
from vendor.appdb import AppDb

log = logging.getLogger(__file__)

session = Session(AppDb.engine)


def invoke(session: Session):
    # workflow treatment
    for workflow in session.query(tWorkflow).all():
        log.debug("reading file -- %r" % workflow)
        old_workflow_meta = workflow.workflow_meta
        workflow.workflow_meta = adaptor(old_workflow_meta)
        session.commit()
    # workflow treatment
    for workflow in session.query(tValidationWorkflow).all():
        log.debug("reading file -- %r" % workflow)
        old_workflow_meta = workflow.validation_meta
        workflow.validation_meta = adaptor(old_workflow_meta)
        session.commit()


def adaptor(workflow: dict):
    for dataset_name, metadata in workflow['SELECTED_DATASETS'].items():
        if isinstance(workflow['SELECTED_DATASETS'][dataset_name], list):
            workflow['SELECTED_DATASETS'][dataset_name] = {"meta": metadata, "schema": ""}
    return workflow

