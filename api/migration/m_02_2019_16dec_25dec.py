import logging

from sqlalchemy.orm import Session

from models import Workflow as tWorkflow

log = logging.getLogger(__file__)


def invoke(session: Session):
    # workflow treatment
    for workflow in session.query(tWorkflow).all():
        log.debug("reading file -- %r" % workflow)
        old_workflow_meta = workflow.workflow_meta
        workflow.workflow_meta = adaptor(old_workflow_meta)
        session.query(tWorkflow).filter(tWorkflow.id == workflow.id).update({"workflow_meta": workflow.workflow_meta})
        session.commit()


def adaptor(workflow: dict):
    for rule in workflow['RULES']:
        if 'rule_type' not in rule:
            if 'aggregate' in rule:
                rule['rule_type'] = 'aggregate'
            elif 'analytical' in rule:
                rule['rule_type'] = 'analytical'
            elif 'custom' in rule:
                rule['rule_type'] = 'custom'
            elif 'dedupe' in rule:
                rule['rule_type'] = 'dedupe'
            elif 'derived' in rule:
                rule['rule_type'] = 'derived'
            elif 'filter' in rule:
                rule['rule_type'] = 'filter'
            elif 'join' in rule:
                rule['rule_type'] = 'join'
            elif 'union' in rule:
                rule['rule_type'] = 'union'
            elif 'partition' in rule:
                rule['rule_type'] = 'partition'
    return workflow

