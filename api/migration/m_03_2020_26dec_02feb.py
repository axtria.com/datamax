import logging

from sqlalchemy.orm import Session

from models import Workflow as tWorkflow

log = logging.getLogger(__file__)


def invoke(session: Session):
    # workflow treatment
    for workflow in session.query(tWorkflow).all():
        log.debug("reading file -- %r" % workflow)
        old_workflow_meta = workflow.workflow_meta
        workflow.workflow_meta = adaptor(old_workflow_meta)
        session.query(tWorkflow).filter(tWorkflow.id == workflow.id).update({"workflow_meta": workflow.workflow_meta})
        session.commit()


def adaptor(workflow: dict):
    if workflow['META'].get('health') is None:
        workflow['META']['health'] = True
    return workflow

