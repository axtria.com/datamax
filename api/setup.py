#   datamax/api/setup.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from app import app, db

with app.app_context():
    db.create_all()
