from datetime import datetime
from vendor.appdb import AppDb
from models import API, APIMeta, Connector
from services import Service
from utils.exceptions import ApiExchangeIntegrityFailed
import pandas as pd
from sqlalchemy import exc
from sqlalchemy.exc import ResourceClosedError
import logging

class ExportDQ(Service):
    """Bulk Dump catalog"""
    def __init__(self, session, name: str, layers=None, limit_records=1000000, **kwargs):
        super().__init__()
        self.layers = layers
        self.limit_records = int(limit_records)
        self.name = name
        self.postgres = session
        self.params = kwargs

    def execute(self):

        for api, api_meta, connector in self.postgres.query(API, APIMeta, Connector).filter(
                API.name == self.name, API.status == 'published'). \
                join(APIMeta, APIMeta.api_id == API.id).join(Connector, Connector.id == APIMeta.connector_id).all():
            self.log.debug(api_meta.query)
            self.log.debug(self.params)
        engine = AppDb.engine
        try:
            contents = pd.read_sql(api_meta.query, engine, params=self.params).to_dict(orient='records')
        except exc.IntegrityError as e:
            self.log.exception(e)
            raise ApiExchangeIntegrityFailed
        except ResourceClosedError:
            contents = {}
        logging.debug("Query Output  %s" % contents)
        df = pd.DataFrame(contents)
        logging.debug("Contents DataFrame  %s" % df)
        df = df[sorted(df.columns.tolist())]
        ts = datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')
        fname = f'dumps/basket_dump_{ts}.csv'
        df.to_csv(fname, index=None)
        self.add_response('file_path', fname)
        self.add_response('file_name', f'export_data_{ts}.csv')

