#   datamax/api/services/data_operations/read.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import io
import re
import logging
import pandas as pd
from sqlalchemy.orm import Session
from models import Layer
from services import Service
from utils.file_operation import get_file_extension, get_file_encoding, get_text_qualifier
from utils.row_operation import inspect_delimiter
from vendor.redshift import Redshift
from vendor.s3 import S3
from sqlalchemy import and_

class Header(Service):
    def __init__(self, session: Session, s3_connector_id: int, source_bucket: str, header_object: str,
                 delimiter=None, **kwargs):
        super().__init__()
        self.s3 = S3.initialize(s3_connector_id, session)
        self.source_bucket = source_bucket
        self.header_object = header_object
        self.delimiter = delimiter

    def execute(self):
        header_row = self.s3.read(self.source_bucket + '/' + self.header_object, 1)[0]
        if self.delimiter == '' or self.delimiter is None:
            self.delimiter = inspect_delimiter(header_row)

        self.add_response('header', header_row.split(self.delimiter))
        self.add_response('delimiter', self.delimiter)


class Rows(Service):
    def __init__(self, session: Session, s3_connector_id: int, source_bucket: str, source_object: str,
                 data_header=True, delimiter=None, count_rows=5, **kwargs):
        super().__init__()
        self.s3 = S3.initialize(s3_connector_id, session)
        self.source_bucket = source_bucket
        self.source_object = source_object
        self.data_header = data_header
        self.delimiter = delimiter
        self.count_rows = count_rows

    def execute(self):
        if self.source_object.endswith('/'):
            file = self.s3.explorer(self.source_bucket, self.source_object, 1)[0]
        else:
            file = self.source_object

        sample_records = self.s3.read(self.source_bucket + '/' + file, self.count_rows)

        if self.data_header:
            sample_records = sample_records[1:]

        self.add_response('sample_records', list(map(lambda x: x.split(self.delimiter), sample_records)))
        self.add_response('delimiter', self.delimiter)


class Shape(Service):
    def __init__(self, session: Session, rs_connector_id: int, schema: str, table_name: str,
                 **kwargs):
        super().__init__()
        self.redshift = Redshift.initialize(rs_connector_id, session)
        self.schema = schema
        self.table_name = table_name

    def execute(self):
        column_properties = self.redshift.execute("""
                        select column_name as name, 
                        case 
                        when data_type like 'character%%' then 'varchar'
                        when data_type like '%%int%%' or data_type like 'numeric%%' or data_type like 'double%%' then 'numeric'
                        when data_type like 'timestamp%%' then 'date'
                        else data_type
                        end as dtype
                        from svv_columns
                        where table_name = '{}' and table_schema='{}' """.format(self.table_name, self.schema))
        self.add_response('shape', column_properties.to_dict(orient='records'))


class SampleFile(Service):
    def __init__(self, session, layer_id: int, file_name: str,file_type: str,
                 data_header= str , delimiter=None, text_qualifier=None, act_filename=None, prefix='', count_rows=6,
                 column_names=None, **kwargs):
        super().__init__()
        self.session = session
        self.file_name = file_name
        self.data_header = data_header
        self.delimiter = delimiter
        self.count_rows = count_rows
        self.layer_id = layer_id
        self.text_qualifier = text_qualifier
        self.act_filename = act_filename
        self.prefix = prefix
        self.column_names = column_names
        self.file_type = file_type

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        s3 = S3.initialize(connector.connector_id, self.session)
        temp_bucket = connector.s3_path
        logging.debug("connection established bucket name  %s" % temp_bucket)
        bucket = temp_bucket.split('//')[1]
        if self.file_type == 'D' :
            if self.file_name.endswith('/'):
                file = s3.explorer(bucket, self.file_name, 1)[0]
            else:
                file = self.file_name
            if self.text_qualifier == 'None':
                self.text_qualifier = ' '
            if self.file_name.endswith('.xlsx') or self.file_name.endswith('.xls'):
                self.text_qualifier = self.text_qualifier.strip("\\")
                if self.delimiter == 'null':
                    self.delimiter = ','
                sample_records = s3.read_excel(bucket, self.prefix + file, self.count_rows)
                if self.data_header == 'Y':
                    column_name = sample_records[0]
                    sample_records = sample_records[1:6]
                    col = [''.join(str(x)) for x in column_name]
                    contents = pd.DataFrame(data=sample_records, columns=col).fillna('')
                    contents = contents.to_dict(orient='records')
                else:
                    col = list(map(lambda x: x, self.column_names.split(',')))
                    sample_records = sample_records[0:5]
                    contents = pd.DataFrame(data=sample_records, columns=col).fillna('')
                    contents = contents.to_dict(orient='records')


                '''
                columns = []
                for i in sample_records:
                    co = list(map(lambda x: (str(x).replace('[', ' ').replace(']', ' ')).split(self.delimiter), i))
                    columns.append(co)
                col = []
                for i in str(column_name).replace('[', ' ').replace(']', ' ').split(self.delimiter):
                    j = i
                    col.append(j.strip().strip(" ' ")
                '''
                # contents = pd.DataFrame(columns, columns=col).to_dict(orient='records')

            elif self.file_name.endswith('.gz'):
                self.text_qualifier = self.text_qualifier.strip("\\")
                sample_records = s3.read(bucket + '/' + self.prefix + self.file_name, 5)
                '''
                header_list= sample_records[0][sample_records[0].rfind('\u00bf')+1:-1]
    
                header = pd.read_csv(io.StringIO(header_list),header=None,sep= self.delimiter)
    
                '''
                data = (''.join(str(e) for e in sample_records))
                if self.delimiter == '\\t':
                    column_data = pd.read_csv(io.StringIO(data), dtype=object, sep='\t').fillna('').astype(str)
                else:
                    column_data = pd.read_csv(io.StringIO(data), sep=self.delimiter, dtype=object).fillna('').astype(str)
                column_data = column_data.iloc[:, :-1]
                col = column_data.columns
                contents = column_data.to_dict(orient='records')

            elif self.file_name.endswith('.zip'):
                file_zip = s3.read_zip(bucket, self.prefix, self.file_name, self.act_filename, self.count_rows)

                if file_zip.endswith('.xlsx') or file_zip.endswith('.xls'):
                    sample_records = s3.read_excel(bucket, file_zip, self.count_rows)
                    text_qualifier = get_text_qualifier(sample_records)
                    text_qualifier = text_qualifier.strip("\\")
                    file_delimiter = inspect_delimiter(sample_records[0])

                    if file_delimiter == 'null':
                        file_delimiter = ','
                    # sample_records = s3.read_excel(connector.bucket, connector.folder_prefix + '/' + file)
                    column_name = sample_records[0]
                    sample_records = sample_records[1:6]
                    col = [''.join(x) for x in column_name]
                    contents = pd.DataFrame(data=sample_records, columns=col).to_dict(orient='records').fillna('')
                    s3.remove(bucket, self.prefix, file_zip)
                else:
                    sample_records = s3.read(bucket + '/' + file_zip, self.count_rows)
                    text_qualifier = get_text_qualifier(sample_records)
                    text_qualifier = text_qualifier.strip("\\")
                    if text_qualifier == 'None':
                        text_qualifier = ' '
                    file_delimiter = inspect_delimiter(sample_records[0])
                    sample_records = sample_records[0:6]
                    data = (''.join(str(e) for e in sample_records))
                    if file_delimiter == '\\t':
                        column_data = pd.read_csv(io.StringIO(data), quotechar=text_qualifier, sep='\t',
                                                  dtype=object).fillna('').astype(str)
                    else:
                        column_data = pd.read_csv(io.StringIO(data), sep=file_delimiter, quotechar=text_qualifier,
                                                  dtype=object).fillna('').astype(str)
                    col = column_data.columns
                    contents = column_data.to_dict(orient='records')
                    s3.remove(connector.bucket, connector.folder_prefix, file_zip)
            else:
                self.text_qualifier = self.text_qualifier.strip("\\")
                sample_records = s3.read(bucket + '/' + self.prefix + file, self.count_rows)
                if self.data_header == 'Y' :
                    sample_records = sample_records[0:6]

                    col = []
                    '''
                    column_name = sample_records[0].strip()
        
        
                    for i in column_name.split(self.delimiter):
                        j = i.lstrip('\\').rstrip('\\n').rstrip('\n').strip(self.text_qualifier)
                        col.append(j)
                    '''

                    data = (''.join(str(e) for e in sample_records))
                    if self.delimiter == '\\t':
                        column_data = pd.read_csv(io.StringIO(data), quotechar=self.text_qualifier, sep='\t',
                                                  dtype=object).fillna('')
                    else:
                        column_data = pd.read_csv(io.StringIO(data), sep=self.delimiter, quotechar=self.text_qualifier,
                                                  dtype=object).fillna('')

                    col = column_data.columns
                    contents = column_data.to_dict(orient='records')
                else:
                    col = list(map(lambda x: x, self.column_names.split(',')))
                    data = (''.join(str(e) for e in sample_records[0:5]))
                    if self.delimiter == '\\t':
                        column_data = pd.read_csv(io.StringIO(data),header=None,names= col, quotechar=self.text_qualifier, sep='\t',
                                                  dtype=object).fillna('')
                    else:
                        column_data = pd.read_csv(io.StringIO(data),header=None,names=col , sep=self.delimiter, quotechar=self.text_qualifier,
                                                  dtype=object).fillna('')
                    contents = column_data.to_dict(orient='records')
                '''
                else:
                    columns = list(map(lambda x: re.split(self.delimiter, x), sample_records))
                    col = []
                    for i in re.split(self.delimiter, column_name):
                        j = i.rstrip('\\n').lstrip('\\').strip(self.text_qualifier)
                        col.append(j)
                    contents = pd.DataFrame(columns, columns=col).to_dict(orient='records')
                '''
            logging.debug("file data  %s" % contents)
            self.add_response('Contents', contents)
            self.add_response('Columns', col)
        else:
            resp = s3.read_fwf(bucket, self.prefix, self.delimiter, self.column_names, self.file_name, self.count_rows)
            if self.data_header == 'N':
                col = resp.columns
                col_len = list(map(lambda x: int(x), self.delimiter.split(',')))
            else:
                resp = resp[1:]
                col = resp.columns
                col_len = list(map(lambda x: int(x), self.delimiter.split(',')))
            logging.debug("file data  %s" % resp)
            self.add_response('Contents', resp.to_dict(orient='records'))
            self.add_response('Columns', col)
            self.add_response('column_width', col_len)


class FileProperties(Service):
    def __init__(self, session,layer_id: int, file_type: str, file_name: str,data_header= str,
                 column_names= None, act_filename=None, prefix='', delimiter= None , count_rows=10, **kwargs):
        super().__init__()
        self.session = session
        self.count_rows = count_rows
        self.file_name = file_name
        self.layer_id = layer_id
        self.act_filename = act_filename
        self.prefix = prefix
        self.data_header = data_header
        self.column_names = column_names
        self.file_type = file_type
        self.delimiter = delimiter

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        s3 = S3.initialize(connector.connector_id, self.session)
        temp_bucket = connector.s3_path
        bucket = temp_bucket.split('//')[1]
        logging.debug("connection established bucket name  %s" % bucket)
        if self.file_type == 'D':
            if self.file_name.endswith('.xlsx') or self.file_name.endswith('.xls'):
                sample_records = s3.read_excel(bucket, self.prefix + self.file_name, self.count_rows)
                file_delimiter = inspect_delimiter(sample_records[1])
                # if file_delimiter == " ":
                # file_delimiter = '\t'
                last_modified_time = str(
                    s3.last_modified_time(bucket, self.prefix + self.file_name))
                text_qualifier = get_text_qualifier(sample_records)

                file_encoding = get_file_encoding(sample_records)
                file_ext = get_file_extension(self.file_name)

            elif self.file_name.endswith('.gz'):
                sample_records = s3.read(bucket + '/' + self.prefix + self.file_name, 5)
                '''
                header_list= sample_records[0][sample_records[0].rfind('\u00bf')+1:-1]
    
                header = pd.read_csv(io.StringIO(header_list),header=None,sep= self.delimiter)
    
                '''
                file_delimiter = inspect_delimiter(sample_records[1])
                if file_delimiter == " ":
                    file_delimiter = '\t'
                file_encoding = get_file_encoding(sample_records)
                file_ext = get_file_extension(self.file_name)
                text_qualifier = get_text_qualifier(sample_records)
                last_modified_time = str(s3.last_modified_time(bucket, self.prefix + '/' +
                                                               self.file_name))
            elif self.file_name.endswith('.zip'):
                file_zip = s3.read_zip(bucket, self.prefix, self.file_name, self.act_filename,
                                       self.count_rows)
                if file_zip.endswith('.xlsx') or file_zip.endswith('.xls'):
                    sample_records = s3.read_excel(bucket, file_zip, self.count_rows)
                    text_qualifier = get_text_qualifier(sample_records)
                    file_delimiter = inspect_delimiter(sample_records[1])
                    file_encoding = get_file_encoding(sample_records)
                    file_ext = get_file_extension(file_zip)
                    if file_delimiter == " ":
                        file_delimiter = '\t'
                    last_modified_time = str(s3.last_modified_time(bucket, self.prefix + self.file_name))
                    s3.remove(bucket, self.prefix, file_zip)
                else:
                    sample_records = s3.read(bucket + '/' + file_zip, self.count_rows)
                    text_qualifier = get_text_qualifier(sample_records)
                    text_qualifier = text_qualifier.strip("\\")
                    file_delimiter = inspect_delimiter(sample_records[1])
                    file_encoding = get_file_encoding(sample_records)
                    file_ext = get_file_extension(file_zip)
                    if file_delimiter == " ":
                        file_delimiter = '\t'
                    last_modified_time = str(
                        s3.last_modified_time(bucket, self.prefix + self.file_name))
                    s3.remove(connector.bucket, connector.folder_prefix, file_zip)

            else:
                sample_records = s3.read(bucket + '/' + self.prefix + self.file_name,
                                         self.count_rows)
                logging.debug("file data  %s" % sample_records)
                file_delimiter = inspect_delimiter(sample_records[1])

                if file_delimiter == " ":
                    file_delimiter = '\t'
                file_encoding = get_file_encoding(sample_records)
                file_ext = get_file_extension(self.file_name)
                text_qualifier = get_text_qualifier(sample_records)

                last_modified_time = str(s3.last_modified_time(bucket, self.prefix +
                                                               self.file_name))
        else :
            sample_records = s3.read_fwf(bucket, self.prefix, self.delimiter, self.column_names, self.file_name,
                                         self.count_rows)
            if self.data_header == 'N':
                sample_records = sample_records

            else:
                sample_records = sample_records[1:]
            logging.debug("file data  %s" % sample_records)
            file_encoding = get_file_encoding(sample_records,self.file_type)
            file_ext = get_file_extension(self.file_name)
            text_qualifier = 'None'
            file_delimiter = 'null'
            last_modified_time = str(s3.last_modified_time(bucket, self.prefix +
                                                           self.file_name))

        self.add_response('last_modified_time', last_modified_time.split('+')[0])
        self.add_response('file_extension', file_ext)
        self.add_response('encoding', file_encoding.upper())
        self.add_response('delimiter', file_delimiter)
        self.add_response('text_qualifier', str(text_qualifier).strip("\\"))


class AttributeInfo(Service):
    def __init__(self, session, layer_id: int, file_name: str, file_type: str,act_filename=None,
                 data_header=True, delimiter=None, count_rows=100, prefix='', text_qualifier=None,
                 column_names=str, **kwargs):
        super().__init__()
        self.session = session
        self.file_name = file_name
        self.data_header = data_header
        self.delimiter = delimiter
        self.count_rows = count_rows
        self.layer_id = layer_id
        self.text_qualifier = text_qualifier
        self.act_filename = act_filename
        self.prefix = prefix
        self.column_names = column_names
        self.file_type = file_type

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        s3 = S3.initialize(connector.connector_id, self.session)
        temp_bucket = connector.s3_path
        bucket = temp_bucket.split('//')[1]
        logging.debug("connection established bucket name  %s" % bucket)
        if self.file_type == 'D' :
            file = self.file_name
            self.text_qualifier = self.text_qualifier.strip("\\")
            if self.text_qualifier == 'None':
                self.text_qualifier = ' '
            if self.file_name.endswith('.xlsx') or self.file_name.endswith('.xls'):
                if self.delimiter == 'null':
                    self.delimiter = ','
                if self.data_header == 'Y':
                    sample_records = s3.read_excel(bucket, self.prefix + file, self.count_rows)
                    header_list = sample_records[0]
                    data_info = sample_records[1:]
                    data_df = pd.DataFrame(data=data_info)
                else:
                    sample_records = s3.read_excel(bucket, self.prefix + file, self.count_rows)
                    header_list = list(map(lambda x: x, self.column_names.split(',')))
                    data_info = sample_records[1:]
                    data_df = pd.DataFrame(data=data_info)

            elif self.file_name.endswith('.gz'):
                sample_records = s3.read(bucket + '/' + self.prefix + self.file_name, 5)
                header = sample_records[0].strip()
                if self.delimiter == '|':
                    header_list = header.split(self.delimiter)
                else:
                    header_list = re.split(self.delimiter, header)
                header_list = list(map(lambda x: x.strip(self.text_qualifier), header_list))
                data_info = sample_records[1:]
                data = (''.join(str(e) for e in data_info))
                if self.delimiter == '\\t':
                    data_df = pd.read_csv(io.StringIO(data), header=None, sep='\t')
                else:
                    data_df = pd.read_csv(io.StringIO(data), header=None, sep=self.delimiter)

            elif self.file_name.endswith('.zip'):
                file_zip = s3.read_zip(bucket, self.prefix, self.file_name, self.act_filename, self.count_rows)
                if file_zip.endswith('.xlsx') or file_zip.endswith('.xls'):
                    if self.delimiter == 'null':
                        self.delimiter = ','
                    sample_records = s3.read_excel(bucket, file_zip, self.count_rows)
                    header_list = sample_records[0]
                    data_info = sample_records[1:]
                    data_df = pd.DataFrame(data=data_info)
                    s3.remove(bucket, self.prefix, file_zip)
                else:
                    sample_records = s3.read(bucket + '/' + file_zip, self.count_rows)

                    header = sample_records[0].strip()
                    if self.delimiter == '|':
                        header_list = header.split(self.delimiter)
                    else:
                        header_list = re.split(self.delimiter, header)
                    header_list = list(map(lambda x: x.strip(self.text_qualifier), header_list))

                    data_info = sample_records[1:]
                    data = (''.join(str(e) for e in data_info))
                    if self.delimiter == '\\t':
                        data_df = pd.read_csv(io.StringIO(data), header=None, sep='\t')
                    else:
                        data_df = pd.read_csv(io.StringIO(data), header=None, sep=self.delimiter,
                                              quotechar=self.text_qualifier)
                        object_count = []
                        temp = 0
                        for typ in data_df.dtypes:
                            if typ == 'object':
                                object_count.append(temp)
                            temp = temp + 1
                        data_df = pd.read_csv(io.StringIO(data), header=None, sep=self.delimiter,
                                              quotechar=self.text_qualifier,
                                              parse_dates=object_count)
                    s3.remove(bucket, self.prefix, file_zip)

            else:
                sample_records = s3.read_flat_files(bucket, self.prefix + file, self.count_rows)
                header = sample_records[0].strip()
                if self.data_header == 'Y' :
                    if self.delimiter == '|':
                        header_list = header.split(self.delimiter)
                    else:
                        header_list = re.split(self.delimiter, header)
                    header_list = list(map(lambda x: x.strip(self.text_qualifier), header_list))
                else:
                    header_list = list(map(lambda x: x, self.column_names.split(',')))
                data_info = sample_records[1:]
                data = (''.join(str(e) for e in data_info))
                if self.delimiter == '\\t':
                    data_df = pd.read_csv(io.StringIO(data), header=None, sep='\t')
                else:
                    data_df = pd.read_csv(io.StringIO(data), header=None, sep=self.delimiter, quotechar=self.text_qualifier)
                    object_count = []
                    temp = 0
                    'to handle dates correctly. and pass all the column index with object datatype as parse_dates parameter value'
                    for typ in data_df.dtypes:
                        if typ == 'object':
                            object_count.append(temp)
                        temp = temp + 1
                    data_df = pd.read_csv(io.StringIO(data), header=None, sep=self.delimiter, quotechar=self.text_qualifier,
                                          parse_dates=object_count, infer_datetime_format=True)
        else:
            data_df = s3.read_fwf(bucket, self.prefix, self.delimiter, self.column_names, self.file_name, self.count_rows)
            if self.data_header == 'N':
                header_list = list(data_df.columns)

            else:
                data_df = data_df[1:]
                header_list = list(data_df.columns)
        i = 0
        datatype_list = []
        logging.debug("same data  %s" % data_df)
        col_len = []
        data_df.fillna('')
        if self.file_type == 'D':
            while i < len(data_df.columns):
                if data_df[i].isnull().values.all():
                    datatype_list.append('object')
                    col_len.append('0')
                else:
                    dat_typ = str(data_df[i].dtype)
                    col_l = max(data_df[i].map(str).apply(len))
                    datatype_list.append(dat_typ)
                    col_len.append(col_l)
                i = i + 1
        else:
            for i in data_df.columns:
                if data_df[i].isnull().values.all():
                    datatype_list.append('object')

                else:
                    dat_typ = str(data_df[i].dtype)
                    datatype_list.append(dat_typ)
                col_len = list(map(lambda x: int(x), self.delimiter.split(',')))

        d_type = {'object': ['VARCHAR', '255', '0', 'NA','VARCHAR'], 'int64': ['BIGINT', '0', '0', 'NA','NUMBER'], 'float64':
            ['DOUBLE PRECISION', '15', '14', 'NA','NUMBER'], 'datetime64[ns]': ['TIMESTAMP_TZ', '0', '0', 'MM-dd-yyyy hh:mm:ss','TIMESTAMP_TZ'],
                  'date': ['date', '0', '0', 'MM-dd-yyyy','TIMESTAMP_TZ'],
                  'float32': ['REAL', '6', '5', 'NA','NUMBER'], 'int32': ['INTEGER', '0', '0', 'NA','NUMBER'],
                  "<class 'pandas._libs.tslibs.nattype.NaTType'>": ['Not Defined', '0', '0', 'NA','Not Defined'], 'bool':
                      ['BOOLEAN', '0', '0', 'NA','Boolean']}

        contents = pd.DataFrame(header_list, columns=['at_nm'])
        contents['att_des'] = 'null'
        contents['b_nm'] = 'null'
        contents['col_loc'] = 'null'
        for i in datatype_list:
            try:
                str(d_type[i])
            except:
                d_type.setdefault(str(i), ['VARCHAR', '255', '0', 'NA','VARCHAR'])
        contents['data_type'] = list(map(lambda x: d_type[x][0], datatype_list))
        contents['dt_typ'] = list(map(lambda x: d_type[x][4], datatype_list))
        contents['prec'] = list(map(lambda x: d_type[x][1], datatype_list))
        contents['scale'] = list(map(lambda x: d_type[x][2], datatype_list))
        contents['dt_frmt'] = list(map(lambda x: d_type[x][3], datatype_list))
        contents['old_etl_nm'] = contents['at_nm']
        a = []
        for x in contents['old_etl_nm']:
            if re.match('^\d+', str(x)):
                # ab= re.match('^\d+', x).span()[1]
                # a.append(str(x)[0:ab] + 'dta_' + str(x)[ab:])
                a.append('dta_' + str(x))
            else:
                m = x
                a.append(m)
        contents['old_etl_nm'] = a
        contents['old_etl_nm'] = contents['old_etl_nm'].str.strip().replace(to_replace=' ', value='_', regex=True). \
            replace(to_replace='#+$', value='_num', regex=True). \
            replace(to_replace='^%+', value='per_', regex=True).replace(to_replace='%', value='_per_', regex=True). \
            replace(to_replace='[!@#$%^&*(),.?":{}|<> ]', value='_', regex=True).replace(to_replace='_+', value='_',regex=True)
        #replace(to_replace=[aeiou],value=,regex = True)
        temp_list = ['ZIP','PIN','ID','REPS','LAB','BIO','OF','POS','MCO','FEE','DAW','DEA','AGE','INFO','EDI','TO',
                     'NPI','VEEVA','SHA','REP','NUNIT','UNIT']
        col_f= []
        for i in contents['old_etl_nm']:
            temp_final = ''
            for j in i.split('_'):
                temp = j[0]
                if j.upper() in temp_list:
                    temp_r = str(j[1:])
                else:
                    temp_r = re.sub('[aeiouAEIOU]','',str(j[1:]))
                temp_f = temp + temp_r
                temp_final += '_'+temp_f
            col_f.append(temp_final.strip('_'))
        contents['etl_nm'] = col_f
        contents['m_att_id'] = 'null'
        contents['m_obj_id'] = 'null'
        contents['column_width'] = col_len
        contents['is_mandatory'] = 'Y'
        json_contents = contents.to_dict(orient='records')
        self.add_response('Contents', json_contents)


class GetZipContents(Service):

    def __init__(self, session, layer_id: int, file_name: str, prefix=' ', **kwargs):
        super().__init__()
        self.session = session
        self.file_name = file_name
        self.layer_id = layer_id
        self.prefix = prefix

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        s3 = S3.initialize(connector.connector_id, self.session)
        temp_bucket = connector.s3_path
        bucket = temp_bucket.split('//')[1]
        self.add_response('Contents',
                          s3.get_zip_files(bucket, self.prefix + self.file_name))


