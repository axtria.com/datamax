from sqlalchemy.orm import Session
from services import Service
from vendor.sftp import Sftp


class GetFolders(Service):
    def __init__(self,  session: Session, sftp_connector_id: int, prefix, **kwargs):
        super().__init__()
        self.sftp = Sftp.initialize(sftp_connector_id,session)
        self.sftp_connector_id = sftp_connector_id
        self.prefix = prefix

    def execute(self):
        res = self.sftp.get_folders()
        self.add_response('Contents', res)


