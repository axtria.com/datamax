#   datamax/api/services/s3_buckets.py
#   Copyright (C) 2019 Axtria.
from models import ConnectionMetaExt, Layer
from services import Service
from vendor.s3 import S3
import pandas as pd
import logging
from sqlalchemy import and_


class GetKeys(Service):
    def __init__(self, session, s3_connector_id: int, bucket_name, **kwargs):
        super().__init__()
        self.bucket_name = bucket_name
        self.session = session
        self.s3 = S3.initialize(s3_connector_id, self.session)

    def execute(self):
        self.add_response('keys', self.s3.obj_bucket(self.bucket_name))


class Upload(Service):
    def __init__(self, session, layer_id: int, file_obj, override=False, prefix='', **kwargs):
        super().__init__()
        self.session = session
        self.layer_id = layer_id
        self.file_obj = file_obj
        self.override = override
        self.prefix = prefix

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        s3 = S3.initialize(connector.connector_id, self.session)
        temp_bucket = connector.s3_path
        bucket = temp_bucket.split('//')[1]
        result = s3.s3_file_upload(bucket, self.prefix, self.file_obj, self.override)
        logging.debug("file upload result  %s" % result)
        res_status = {'Success': 'Success', 'Fail': 'Fail'}
        res_message = {'Success': 'File Uploaded Successfully',
                       'Fail': 'File with similar name, already present. Do you want to'
                               ' override? (True/False)'}
        self.add_response('Status', res_status[result])
        self.add_response('Message', res_message[result])


class DeleteKey(Service):
    def __init__(self, session, layer_id: int, file_name, prefix='', **kwargs):
        super().__init__()
        self.session = session
        self.layer_id = layer_id
        self.file_name = file_name
        self.prefix = prefix

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        s3 = S3.initialize(connector.connector_id, self.session)
        temp_bucket = connector.s3_path
        bucket = temp_bucket.split('//')[1]
        logging.debug("bucket  %s" % bucket)
        result = s3.remove(bucket, self.prefix, self.file_name)
        logging.debug("Delete file  %s" % result)
        res_status = {'Success': 'Success', 'Fail': 'Fail'}
        res_message = {'Success': 'File Deleted Successfully', 'Fail': 'File not found'}
        self.add_response('Status', res_status[result])
        self.add_response('Message', res_message[result])


class DownloadKey(Service):
    def __init__(self, session, layer_id: int, file_name, prefix ='',  **kwargs):
        super().__init__()
        self.session = session
        self.layer_id = layer_id
        self.file_name = file_name
        self.prefix = prefix

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        s3 = S3.initialize(connector.connector_id, self.session)
        temp_bucket = connector.s3_path
        bucket = temp_bucket.split('//')[1]

        result = s3.download(bucket, self.prefix , self.file_name)
        logging.debug("Download file %s" % result)
        res_status = {'Success': 'Success', 'Fail': 'Fail'}
        res_message = {'Success': 'File downloaded', 'Fail': 'File not found'}
        self.add_response('Status', res_status[result])
        self.add_response('Message', res_message[result])


class StorageLoc(Service):
    def __init__(self, session, layer_id: int, category, extension=None, **kwargs):
        super().__init__()
        self.session = session
        self.layer_id = layer_id
        self.category = category
        self.extension = extension

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        s3 = S3.initialize(connector.connector_id, self.session)

        temp_bucket = connector.s3_path
        bucket = temp_bucket.split('//')[1]
        logging.debug("Bucket %s" % bucket)
        prefix = ''
        for i in temp_bucket.split('//')[2:]:
            prefix += str(i) + '/'
        prefix = prefix.strip('/')
        logging.debug("prefix %s" % prefix)
        if self.category == 'Files':
            all_keys = s3.get_keys(bucket, prefix + '/')
            ext_file = []
            self.log.debug(all_keys)
            for i in all_keys:
                if i[len(i) - 1] == '/':
                    pass
                else:
                    if self.extension is not None:
                        for e in self.extension.split(','):
                            t = i.split('/')
                            if t[-1].endswith(e):
                                if t[-1].startswith('manifest_'):
                                    pass
                                else:
                                    ext_file.append(i)
                    else:
                        t = i.split('/')
                        if t[-1].startswith('manifest_'):
                            pass
                        else:
                            ext_file.append(i)

            contents = pd.DataFrame(ext_file, columns=['Path']).to_dict(orient='records')
            logging.debug("Output %s" % contents)
            self.add_response('Contents', contents)
        else:
            file_k = []
            file_keys = []
            final_keys = []
            all_keys = s3.get_keys(bucket, prefix + '/')

            for i in all_keys:
                if i.rfind('/') == -1:
                    pass
                else:
                    file_k.append(i[:i.rfind('/')])
            for x in file_k:
                if x not in file_keys:
                    file_keys.append(x)

            for i in file_keys:

                if str(i) == '':
                    pass
                elif i[len(i) - 1] == '/':
                    pass
                else:
                    final_keys.append(i)

            contents = pd.DataFrame(final_keys, columns=['Path']).to_dict(orient='records')
            logging.debug("Output %s" % contents)
            self.add_response('Contents', contents)


class GetBuckets(Service):
    """To retrieve buckets from s3 """

    def __init__(self, session, s3_connector_id: int, bucket_name, **kwargs):
        super().__init__()
        self.session = session
        self.s3 = S3.initialize(s3_connector_id, self.session)

    def execute(self):
        """Fetch the buckets in s3.
        Parameters:
        Returns:
            List: 
        To list the buckets in s3 connection.
        """
        self.add_response('buckets', self.s3.list_buckets())
        