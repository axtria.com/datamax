import pandas as pd
from vendor.appdb import AppDb
from services import Service
from vendor.s3 import S3
from vendor.redshift import Redshift


class RedshiftTableCreation(Service):

    def __init__(self, session, rs_connector_id: int, obj_id: int, create_flg: str, schema_name: str, **kwargs):
        super().__init__()
        self.session = session
        self.rs_connector_id = rs_connector_id
        self.obj_id = obj_id
        self.create_flg = create_flg
        self.schema_name = schema_name
        self.redshift = Redshift.initialize(self.rs_connector_id, self.session)
        self.engine = AppDb.engine

    def execute(self):
        read_obj_nm = pd.read_sql(
            "Select om.obj_nm,is_header,obj_delimiter,obj_extension,case when obj_text_qualifier is null or trim(obj_text_qualifier) = '' then 'NO TEXT QUALIFIER CONFIGURED' else obj_text_qualifier end as obj_text_qualifier,obj_encoding,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(obj_pattern_fmt,'DD','%%d'),'dd','%%-d'),'MMM','%%%%'),'MM','%%m'),'mm','%%-m'),'YYYY','%%Y'),'YY','%%y'),'HH','%%H'),'hh','%%-H'),'MI','%%M'),'mi','%%-M'),'SS','%%S'),'ss','%%-S'),'F','%%f') as obj_pattern_fmt,ome.file_typ as child_extension,ome.ext_file_name as child_obj_nm,om.is_encoding,(case when length(ome.file_typ)=0 then obj_extension else coalesce(ome.file_typ,obj_extension) end) as extension,std_file_name as s3_file_name from system_config.object_meta om left join system_config.object_meta_ext ome on om.obj_id=ome.obj_id  where om.obj_id = " "'" + str(
                self.obj_id) + "'", self.engine)
        read_tbl_obj_id = pd.read_sql(
            "Select obj_id, obj_nm as tbl_obj_nm from system_config.object_meta where ref_obj_id=" "'" + str(
                self.obj_id) + "'",
            self.engine)
        tbl_obj_id = read_tbl_obj_id.obj_id[0]
        tbl_obj_nm = read_tbl_obj_id.tbl_obj_nm[0]
        read_conn = pd.read_sql(
            "select conn.connection from system_config.connection_meta conn join (select conn_id,id,bucket from system_config.connection_meta_ext) conn_ext on conn.id = conn_ext.id join (select obj_id, conn_id,folder_path from system_config.object_connection_meta) obj on conn_ext.conn_id=obj.conn_id where obj.obj_id = " "'" + str(
                self.obj_id) + "'", self.engine)
        read_conn_out = pd.DataFrame(read_conn.connection.apply(pd.Series))
        read_conn_folder = pd.read_sql(
            "select conn_ext.bucket,obj.folder_path from system_config.connection_meta conn join (select conn_id,id,bucket from system_config.connection_meta_ext) conn_ext on conn.id = conn_ext.id join (select obj_id, conn_id,folder_path from system_config.object_connection_meta) obj on conn_ext.conn_id=obj.conn_id where obj.obj_id = " "'" + str(
                self.obj_id) + "'", self.engine)
        access_key = read_conn_out.iloc[0, 0]
        secret_access_key = read_conn_out.iloc[0, 1]
        bucket = read_conn_folder.iloc[0, 0]
        folder = read_conn_folder.iloc[0, 1]
        file_ext = read_obj_nm.obj_extension[0]
        is_header = read_obj_nm.is_header[0]
        file_delim = read_obj_nm.obj_delimiter[0]
        #    child_extension=read_obj_nm.child_extension[0]
        extension = read_obj_nm.extension[0]
        obj_text_qualifier = read_obj_nm.obj_text_qualifier[0]
        #    s3_file_name=read_obj_nm.s3_file_name[0].split(',')[-1]
        read_s3_filename = pd.read_sql(
            "Select file_nm from system_config.file_register where file_inst_id=(select max(file_inst_id) from"
            " system_config.file_register where obj_id=" "'" + str(
                self.obj_id) + "')", self.engine)
        s3_filename = read_s3_filename.file_nm[0]
        s3_file_list = folder + '/' + s3_filename
        df = pd.DataFrame(s3_file_list.split(','))
        var1 = '{"url": "path","meta": { "content_length": length}}'
        var2 = var1
        result = []
        i = 0
        s3 = S3.initialize_with_access_key(access_key, secret_access_key)
        prefix = df[0][i]
        s3_bucket = s3.get_buckets(bucket)
        s3_objects = s3.get_objects(bucket, prefix)
        for files in df:
            for obj in s3_objects:
                while i < len(df):
                    '''        
                    if (file_ext.upper() == 'XLS' or file_ext.upper() == 'XLSX'):
                        records = _read_excel(access_key,secret_access_key,bucket,df[0][i],5000000)
                        if (is_header.upper()=='Y'):                        
                            data_info = records[1:]
                            xls_data= ('\n'.join('\t'.join(e) for e in data_info))
                        else:        
                            data_info = records[:]
                            xls_data= ('\n'.join('\t'.join(e) for e in data_info))
        #                    xls_data= (''.join(str(e) for e in records))

                        s3_resource=boto3.resource('s3',aws_access_key_id = access_key, aws_secret_access_key = secret_access_key)
                        key=df[0][i].replace('.xlsx','.csv').replace('.xls','.csv')
                        s3_resource.Object(bucket, key).put(Body=xls_data)
                    '''
                    if extension.upper() == 'XLS' or extension.upper() == 'XLSX':
                        actual_path = "s3://" + bucket + '/' + df[0][i]
                        actual_path = actual_path.replace('.xlsx', '.csv').replace('.xls', '.csv')
                        df[0][i] = df[0][i].replace('.xlsx', '.csv').replace('.xls', '.csv')
                        actual_length = str(s3_bucket.Object(df[0][i]).content_length)

                    else:
                        actual_path = "s3://" + bucket + '/' + df[0][i]
                        actual_length = str(s3_bucket.Object(df[0][i]).content_length)

                    result.append(var2.replace('path', actual_path).replace(' length', actual_length))
                    var2 = var1
                    i = i + 1
        out = ","
        resultout = '{  "entries": [' + out.join(result) + ']}'
        print(resultout)
        key = folder+'m_'+tbl_obj_nm+".txt"
        s3.put_object(bucket, key, resultout)
        if self.create_flg.upper() == 'Y':
            include_header = ''
            if (is_header.upper() == 'Y'):
                #       if file_delim=='\\t':
                #           df.columns=','.join(header_info.split('\t')).replace(' ','_').split(',')
                #       else: df.columns=header_info.replace(' ','_').split(file_delim)
                if file_ext == 'gz':
                    include_header = '\ntable properties (\'compressionType\'=\'' + file_ext + '\', \'skip.header.line.count\'=\'1\')'
                else:
                    include_header = '\ntable properties (\'skip.header.line.count\'=\'1\')'
            else:
                #        df.columns=df_header
                if file_ext == 'gz':
                    include_header = '\ntable properties (\'compressionType\'=\'' + file_ext + '\')'

            external_schema = self.schema_name
            # Create_DDL=pd.io.sql.get_schema(df, external_schema+'.'+obj_nm).replace('"','').replace(' TABLE ',' EXTERNAL TABLE ')
            read_attr_list = pd.read_sql(
                "select attr_nm||' '||attr_data_typ as meta_col from system_config.attribute_meta where obj_id = " + str(
                    tbl_obj_id) + " order by col_loc", self.engine)
            meta_col_list = list(read_attr_list.meta_col)

            Create_DDL = 'Create External Table ' + external_schema + '.' + tbl_obj_nm + ' ' + str(
                meta_col_list).replace('[', '(').replace(']', ')').replace('\'', '')

            #    i=0

            #    for m in re.finditer(' TEXT', Create_DDL):
            #        Create_DDL=Create_DDL.replace(' TEXT',' VARCHAR('+df_dtypes_new[i]+')',1)
            #        i=i+1

            s3path = 's3://' + bucket + '/' + folder + '/m_' + tbl_obj_nm + '.txt'

            #    additional_path='\nrow format delimited \nfields terminated by \'' + file_delim +'\' \nstored as textfile \nlocation \''+s3path+'\''+include_header+' ;'
            additional_path = '\nrow format serde \'org.apache.hadoop.hive.serde2.OpenCSVSerde\' \nwith serdeproperties (\'separatorChar\' = \'' + file_delim + '\',\'quoteChar\' = \'' + obj_text_qualifier + '\') \nstored as textfile \nlocation \'' + s3path + '\'' + include_header + ' ;'
            table_drop = 'DROP TABLE IF EXISTS ' + external_schema + '.' + tbl_obj_nm + ';\n'
            Create_DDL = table_drop + Create_DDL + additional_path

            if self.redshift.engine.execute(Create_DDL):
                self.add_response('Contents', 'Success')
            else:
                self.add_response('Contents', 'Fail')
