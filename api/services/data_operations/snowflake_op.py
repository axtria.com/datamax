from services import Service
from vendor.snowflake import Snowflake
from vendor.appdb import AppDb
from models import Layer
import pandas as pd
import json
import re
import logging
from sqlalchemy import and_

app_db = AppDb()


class SfExistingTableCheck(Service):

    def __init__(self, session, layer_id : int, match= '' , **kwargs):
        super().__init__()
        self.layer_id = layer_id
        self.session = session
        self.match = match

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        schema_name = connector.schema_name
        sf = Snowflake.initialize(connector.connector_id, self.session)
        sf_query = "SHOW  TABLES LIKE '" + self.match + "%' IN  SCHEMA " + " " + schema_name
        resp = sf.execute(sf_query)
        contents = pd.DataFrame()
        contents['table_name'] = resp['name']
        contents['schema_name'] = resp['schema_name']
        contents['database_name'] = resp['database_name']
        self.add_response('Contents',contents.to_dict(orient='records'))

'''
class SfTableList(Service):

    def __init__(self, session, layer_id : int, **kwargs):
        super().__init__()
        self.layer_id = layer_id
        self.session = session

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        schema_name = connector.schema_name
        sf = Snowflake.initialize(connector.connector_id, self.session)
        sf_query = "SHOW  TABLES  IN  SCHEMA " + " " + schema_name
        resp = sf.execute(sf_query)
        self.add_response('Contents', resp.to_dict(orient='records'))

'''


class SfTableMeta(Service):

    def __init__(self, session, layer_id : int,table_name: str ,  **kwargs):
        super().__init__()
        self.layer_id = layer_id
        self.session = session
        self.table_name = table_name
        self.sf_columns = pd.DataFrame()

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        schema_name = connector.schema_name
        sf = Snowflake.initialize(connector.connector_id, self.session)
        sf_query = "SHOW COLUMNS IN  " + schema_name + "." + self.table_name
        self.sf_columns= sf.execute(sf_query)
        contents= pd.DataFrame()
        flat_data = json.dumps(list(self.sf_columns['data_type'].apply(json.loads)))
        self.sf_columns= self.sf_columns.join(pd.read_json(flat_data))
        contents['etl_nm'] = self.sf_columns['column_name']
        contents['dt_typ'] = self.sf_columns['type']
        d_type= {'TEXT' : ['255','0','NA','VARCHAR'],'FIXED' : ['38','0','NA','NUMBER'],'REAL' : ['15','14','NA','FLOAT'],
                 'TIMESTAMP_NTZ' : ['0','0','MM-dd-yyyy hh:mm:ss','TIMESTAMP_NTZ'],'TIMESTAMP_LTZ' : ['0','0','MM-dd-yyyy hh:mm:ss','TIMESTAMP_LTZ'],
                 'DATE' : ['0','0','MM-dd-yyyy','DATE'],'BINARY' : ['4','0','NA','BINARY'],'BOOLEAN' : ['0','0','NA','BOOLEAN'],
                 'TIME': ['0', '9', 'NA', 'TIME'] }
        for i in contents['dt_typ']:
            try:
                str(d_type[i])
            except:
                d_type.setdefault(str(i), ['VARCHAR', '255', '0', 'NA','VARCHAR'])
        contents['att_des'] = 'null'
        contents['b_nm'] = 'null'
        contents['col_loc'] = 'null'
        contents['data_type']= list(map(lambda x: d_type[x][3], contents['dt_typ']))
        contents['prec'] = list(map(lambda x: d_type[x][0], contents['dt_typ']))
        contents['scale'] = list(map(lambda x: d_type[x][1], contents['dt_typ']))
        contents['dt_frmt'] = list(map(lambda x: d_type[x][2], contents['dt_typ']))
        contents['old_etl_nm'] = contents['etl_nm']
        contents['at_nm'] = 'null'
        contents['m_att_id'] = 'null'
        contents['m_obj_id'] = 'null'
        contents['column_width'] = 'null'
        contents['is_mandatory'] = 'Y'
        self.add_response('Contents', contents.to_dict(orient='records'))


class SfTableMetaData(Service):

    def __init__(self, session, layer_id : int,table_name: str ,  **kwargs):
        super().__init__()
        self.layer_id = layer_id
        self.session = session
        self.table_name = table_name

    def execute(self):
        connector = self.session.query(Layer).filter(and_(
            Layer.layer_id == self.layer_id , Layer.is_active == True)).first()
        schema_name = connector.schema_name
        sf = Snowflake.initialize(connector.connector_id, self.session)
        #select get_ddl('table', 'STAGE.STG_CUST_ACCNT')
        sf_query = "select get_ddl('table','" + schema_name + "." + self.table_name + "')"
        resp = sf.execute(sf_query)
        logging.debug("Query result  %s" % resp)
        resp1 = str(resp.iloc[0][0].strip(''))
        col_l = list(map(lambda x: x.strip(',').strip(''), resp1[resp1.find('(')+1:-2].split('\n')))
        tab_l = []
        dat_l = []
        data_t = []
        for c in col_l[1:-1]:
            data_t.append(c.split(' ')[1])
            c=c.strip('	')
            if '(' in c:
                c= c.strip(re.findall('\(.*?\)', c)[0])
            tab_l.append(c.split(' ')[0].rstrip(''))
            dat_l.append(c.split(' ')[1])
        contents= pd.DataFrame()
        logging.debug("Column Names %s" % tab_l)
        logging.debug("Data Types Names  %s" % dat_l)
        contents['data_type'] = dat_l
        contents['etl_nm'] = tab_l
        prec_l= []
        scal_l = []
        for val in data_t:
            if '(' in val:
                prec_l.append(re.findall('\(.*?\)',val)[0].strip(' ').strip(')').strip('(').split(',')[0])
                if len(re.findall('\(.*?\)', val)[0].strip(' ').strip(')').strip('(').split(',')) == 1:
                    scal_l.append("0")
                else:
                    scal_l.append(re.findall('\(.*?\)', val)[0].strip(' ').strip(')').strip('(').split(',')[1])
            else:
                prec_l.append("0")
                scal_l.append("0")
        d_type = {'DATE': ['MM-dd-yyyy'],'TIMESTAMP_LTZ': ['MM-dd-yyyy hh:mm:ss'],'TIMESTAMP_TZ' : ['MM-dd-yyyy hh:mm:ss'],
                  'TIMESTAMP_NTZ' : ['MM-dd-yyyy hh:mm:ss']}
        for i in contents['data_type']:
            try:
                str(d_type[i])
            except:
                d_type.setdefault(str(i), ['NA'])

        contents['dt_typ'] = contents['data_type']
        contents['prec'] = prec_l
        contents['scale'] = scal_l
        contents['att_des'] = 'null'
        contents['b_nm'] = 'null'
        contents['col_loc'] = 'null'
        contents['dt_frmt'] = list(map(lambda x: d_type[x][0], contents['data_type']))
        contents['old_etl_nm'] = contents['etl_nm']
        contents['at_nm'] = 'null'
        contents['m_att_id'] = 'null'
        contents['m_obj_id'] = 'null'
        contents['column_width'] = 'null'
        contents['is_mandatory'] = 'Y'
        logging.debug("Contents  %s" % contents)
        self.add_response('Contents', contents.to_dict(orient='records'))





