# datamax/api/services/data_movement/s3_to_redshift.py

from uuid import uuid4

from services import Service
from services.data_movement.redshift_to_redshift import RedshiftToRedshift
from utils.querybuilder import create_external_query, EXTERNAL_SCHEMA
from utils.row_operation import inspect_columns, inspect_delimiter
from vendor.redshift import Redshift
from vendor.s3 import S3


class S3ToRedshift(Service):
    def __init__(self, session, s3_connector_id, rs_connector_id, source_bucket, source_object, destination_schema,
                 header_object, destination_table=None, delimiter=None, file_format='TEXTFILE', data_header=False,
                 **kwargs):
        super().__init__()
        self.s3 = S3.initialize(s3_connector_id, session)
        self.redshift = Redshift.initialize(rs_connector_id, session)
        self.session = session
        self.rs_connector_id = rs_connector_id
        self.source_bucket = source_bucket
        self.source_object = source_object
        self.destination_schema = destination_schema
        self.destination_table = destination_table
        self.header_object = header_object
        self.delimiter = delimiter
        self.file_format = file_format
        self.data_header = data_header
        self.tbl_properties = []

    def execute(self):
        """
        """
        header = self.s3.read(self.source_bucket + '/' + self.header_object, 1)[0]
        self.log.debug(header)

        if not self.delimiter:
            self.delimiter = inspect_delimiter(header)

        if self.data_header:
            self.tbl_properties.append("'skip.header.line.count' = '1'")

        table = self._create_tablename()
        columns = inspect_columns(header, self.delimiter)
        create_query = create_external_query(table, columns, self.delimiter,
                                             's3://' + self.source_bucket + '/' + self.source_object, self.file_format,
                                             self.tbl_properties)

        self.add_response('create_query', create_query)
        self.redshift.execute(create_query)

        # assign the dynamic name created to the table name
        self.destination_table = table if self.destination_table is None else self.destination_table

        service = RedshiftToRedshift(self.session, self.rs_connector_id, EXTERNAL_SCHEMA, table,
                                     self.destination_schema, self.destination_table, columns)
        service.execute()
        self.add_response('child', service.response)

    def _create_tablename(self):
        table_name = 's3_' + str(uuid4().hex) if self.destination_table is None else self.destination_table
        self.add_response('table_name', table_name)
        return table_name
