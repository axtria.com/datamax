# datamax/api/services/data_movement/s3_to_s3.py

from services import Service
from vendor.s3 import S3


class S3ToS3(Service):
    def __init__(self, session, s3_connector_id: int, source_bucket: str, source_object: str, 
                 destination_bucket: str = None, destination_object: str = None, **kwargs):
        super().__init__()
        self.s3 = S3.initialize(s3_connector_id, session)
        self.source_bucket = source_bucket
        self.source_object = source_object
        self.destination_bucket = destination_bucket
        self.destination_object = destination_object

    def execute(self):
        """
        Assumptions:

        - `destination_bucket` is missing then, it will be same as source bucket.
        - `destination_object` is missing then, it will be same as source object.

        """
        self.destination_bucket = self.source_bucket if self.destination_bucket is None else self.destination_bucket
        self.destination_object = self.source_object if self.destination_object is None else self.destination_object

        self.s3.copy(
            {'bucket': self.source_bucket, 'key': self.source_object},
            {'bucket': self.destination_bucket, 'key': self.destination_object}
        )
