# datamax/src/services/data_movement/redshift_to_redshift.py

from services import Service
from utils.querybuilder import copy_query
from vendor.redshift import Redshift


class RedshiftToRedshift(Service):
    def __init__(self, session, rs_connector_id: int, source_schema: str, source_table: str,
                 destination_schema: str, destination_table: str = None, columns: list = None):
        super().__init__()
        self.rs = Redshift.initialize(rs_connector_id, session)
        self.source_schema = source_schema
        self.source_table = source_table
        self.destination_schema = destination_schema
        self.destination_table = source_table if destination_table is None else destination_table
        self.columns = columns

    def execute(self):
        id_column = 'DMX_ID bigint identity(1,1), '
        query = copy_query(new_schema=self.destination_schema, new_table_name=self.destination_table,
                           old_schema=self.source_schema, old_table_name=self.source_table, extra=id_column,
                           columns=self.columns)
        self.add_response('copy_query', query)
        self.rs.execute(query)
