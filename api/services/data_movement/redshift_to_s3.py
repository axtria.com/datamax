# datamax/src/services/data_movement/redshift_to_s3.py

from services import Service
from utils.querybuilder import unload_query,unload_export_query
from vendor.redshift import Redshift
from vendor.s3 import S3

''' TODO :execute method can be refractored'''
class RedshiftToS3(Service):
    def __init__(self, session, rs_connector_id, s3_connector_id, source_schema: str, source_table: str, destination_bucket,
                 destination_object, parallel=False, header='', **kwargs):
        super().__init__()
        self.rs = Redshift.initialize(rs_connector_id, session)
        self.s3 = S3.initialize(s3_connector_id, session)
        self.source_schema = source_schema
        self.source_table = source_table
        self.destination_bucket = destination_bucket
        self.destination_object = destination_object + source_table
        self.parallel = 'ON' if parallel else 'OFF'
        self.header = header

    def execute(self):
        unload = unload_query(self.source_schema, self.source_table,
                              's3://' + self.destination_bucket + '/' + self.destination_object, self.parallel,
                              self.header, self.s3.access_key, self.s3.secret_key)
        self.rs.execute(unload)
        self.add_response('filename', self.destination_object)
        self.add_response('bucket', self.destination_bucket)
        try:
            self.add_response('data', self.s3.analyse(self.destination_bucket, self.destination_object+'000'))
        except Exception as e:
            self.log.exception(e)

class RedshiftQueryExportToS3(Service):
    def __init__(self, session, rs_connector_id, s3_connector_id, query:str, destination_bucket,
                 destination_object,  fileprefix : str, parallel=False,header='',  **kwargs):
        super().__init__()
        self.rs = Redshift.initialize(rs_connector_id, session)
        self.s3 = S3.initialize(s3_connector_id, session)
        self.query = query
        self.destination_bucket = destination_bucket
        self.destination_object = destination_object + fileprefix + '_'
        self.parallel = 'ON' if parallel else 'OFF'
        self.header = header

    def execute(self):
        unload = unload_export_query(self.query,'s3://' + self.destination_bucket + '/' + self.destination_object, self.parallel,
                              self.header, self.s3.access_key, self.s3.secret_key)
        self.rs.execute(unload)
        self.add_response('filename', self.destination_object)
        self.add_response('bucket', self.destination_bucket)
        try:
            self.add_response('data', self.s3.analyse(self.destination_bucket, self.destination_object+'000'))
        except Exception as e:
            self.log.exception(e)


