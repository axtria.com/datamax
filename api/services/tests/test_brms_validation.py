#   datamax/api/services/tests/test_brms.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import json
from unittest.mock import Mock, patch

import pytest

from entity.scenario import ValidationScenario
from entity.workflow import Workflow
from services.brms_validation import CreateWorkflow, AddWorkflowMeta, AddRule, UpdateRule, SaveWorkflow, \
    GetWorkflow, EditWorkflow, CopyWorkflow, AddParameterDefinition, DeleteRule, ExecuteScenario, PublishWorkflow
from utils.exceptions import BRMSPublishedWorkflowModified, BRMSDraftWorkflowExecuted, BRMSDuplicateWorkflowName, \
    BRMSEmptyWorkflowPublished

empty_workflow_db = {
    "META": {"workflow_id": "d4c063a9ab734b24b1eb03c90a8597fb", "name": "hello", "description": "some desc",
             "workspace_id": 1, "status": "DRAFT", "health": True},
    "DATASETS": {
        "ds1": [{"column_name": "c1", "data_type": "varchar"}, {"column_name": "c2", "data_type": "varchar"},
                {"column_name": "c3", "data_type": "numeric"}, {"column_name": "c4", "data_type": "varchar"}]
    },

    "SELECTED_DATASETS": {
        "ds1": {"meta": [{"column_name": "c1", "data_type": "varchar"}, {"column_name": "c2", "data_type": "varchar"},
                         {"column_name": "c3", "data_type": "numeric"}, {"column_name": "c4", "data_type": "varchar"}],
                "schema": "brms_instance"}},
    "PARAMETERS": {},
    "RULES": [
    ]
}
PUBLISHED = 'PUBLISHED'
session = Mock()


def test_create_workflow():
    service = CreateWorkflow(session)
    service.execute()
    assert 'workflow_id' in service.response


@patch("services.brms_validation.WorkflowGateway.restore")
def test_add_workflow_meta(restore):
    workflow_instance = Workflow()
    workflow_instance.workflow_id = empty_workflow_db['META']['workflow_id']
    restore.return_value = workflow_instance
    session.query.return_value.filter.return_value.all.return_value = []
    selected_datasets = json.dumps([{"name": "ds1",
                                     "adaptor_meta": empty_workflow_db['SELECTED_DATASETS']['ds1']['meta'],
                                     'schema': empty_workflow_db['SELECTED_DATASETS']['ds1']['schema']}])

    service = AddWorkflowMeta(session, workflow_instance.workflow_id, empty_workflow_db['META']['name'],
                              empty_workflow_db['META']['description'], selected_datasets=selected_datasets)
    service.execute()
    assert service.response['META'] == empty_workflow_db['META']


@patch("services.brms_validation.WorkflowGateway.restore")
def test_add_rule(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance

    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c1 is integer')
    service.execute()
    assert len(workflow_instance.rules.serialize) == 1


@patch("services.brms_validation.WorkflowGateway.restore")
def test_add_rule_in_published_workflow_should_raise_error(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance

    workflow_instance.status = PUBLISHED
    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c1 is integer')
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert len(workflow_instance.rules.serialize) == 0


@patch("services.brms_validation.WorkflowGateway.restore")
def test_update_rule_should_not_add_new_rule(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c1 is integer')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']

    service = UpdateRule(session, validation_id=workflow_instance.workflow_id, rule_name='dummy rule v2',
                         user_input='data type of ds1.c2 is integer', rule_id=rule_id)
    service.execute()
    assert len(service.response['RULES']) == 1


@patch("services.brms_validation.WorkflowGateway.restore")
def test_update_rule_incorrect_grammar(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c1 is integer')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']
    with pytest.raises(Exception):
        service = UpdateRule(session, validation_id=workflow_instance.workflow_id, rule_name='dummy rule v2',
                             user_input='data type of ds1.c2', rule_id=rule_id)
        service.execute()
        assert len(service.response['RULES']) == 1
        assert 'dummy rule' == service.response['RULES'][0]['rule_name']
        assert 'ds1' in service.response['DATASETS']


@patch("services.brms_validation.WorkflowGateway.restore")
def test_update_rule_in_published_workflow_should_not_allow_change(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance

    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c1 is integer')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']
    workflow_instance.status = PUBLISHED

    service = UpdateRule(session, validation_id=workflow_instance.workflow_id, rule_name='dummy rule v2',
                         user_input='data type of ds1.c2 is integer', rule_id=rule_id)
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert len(workflow_instance.rules.serialize) == 1


@patch("services.brms_validation.WorkflowGateway.restore")
def test_delete_rule_should_not_add_new_rule(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c1 is integer')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']

    service = DeleteRule(session, validation_id=workflow_instance.workflow_id, rule_id=rule_id)
    service.execute()
    assert len(service.response['RULES']) == 0


@patch("services.brms_validation.WorkflowGateway.restore")
def test_delete_rule_in_published_workflow_should_not_allow_change(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance

    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c3 is integer')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']
    workflow_instance.status = PUBLISHED

    service = DeleteRule(session, validation_id=workflow_instance.workflow_id, rule_id=rule_id)
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert len(workflow_instance.rules.serialize) == 1


@patch("services.brms_validation.WorkflowGateway.restore")
def test_save_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = SaveWorkflow(session, validation_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response is not None


@patch("services.brms_validation.WorkflowGateway.restore")
def test_save_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    workflow_instance.status = PUBLISHED

    service = SaveWorkflow(session, validation_id=workflow_instance.workflow_id)

    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert service.response is not None


@patch("services.brms_validation.WorkflowGateway.restore")
def test_publish_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c3 is integer')
    service.execute()
    service = PublishWorkflow(session, validation_id=workflow_instance.workflow_id)
    service.execute()

    assert service.response is not None


@patch("services.brms_validation.WorkflowGateway.restore")
def test_publish_empty_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = PublishWorkflow(session, validation_id=workflow_instance.workflow_id)
    with pytest.raises(BRMSEmptyWorkflowPublished):
        service.execute()


@patch("services.brms_validation.WorkflowGateway.restore")
def test_get_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = GetWorkflow(session, validation_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response == empty_workflow_db


@patch("services.brms_validation.WorkflowGateway.restore")
def test_edit_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = EditWorkflow(session, validation_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response == empty_workflow_db


@patch("services.brms_validation.WorkflowGateway.restore")
def test_edit_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow_instance.status = PUBLISHED
    restore.return_value = workflow_instance

    service = EditWorkflow(session, validation_id=workflow_instance.workflow_id)
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()


@patch("services.brms_validation.WorkflowGateway.restore")
def test_copy_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    session.query.return_value.filter.return_value.all.return_value = []
    service = CopyWorkflow(session, validation_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response['META']['workflow_id'] != empty_workflow_db['META']['workflow_id']
    assert service.response['META']['name'] == 'Copy of ' + empty_workflow_db['META']['name']
    assert service.response['META']['status'] == 'DRAFT'


@patch("services.brms_validation.WorkflowGateway.restore")
def test_copy_duplicate_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    session.query.return_value.filter.return_value.all.return_value = [1]
    service = CopyWorkflow(session, validation_id=workflow_instance.workflow_id)
    with pytest.raises(BRMSDuplicateWorkflowName):
        service.execute()


@patch("services.brms_validation.WorkflowGateway.restore")
def test_copy_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow_instance.status = PUBLISHED
    restore.return_value = workflow_instance
    session.query.return_value.filter.return_value.all.return_value = []
    service = CopyWorkflow(session, validation_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response['META']['workflow_id'] != empty_workflow_db['META']['workflow_id']
    assert service.response['META']['name'] == 'Copy of ' + empty_workflow_db['META']['name']
    assert service.response['META']['status'] == 'DRAFT'


@patch("services.brms_validation.WorkflowGateway.restore")
def test_add_parameter_definition_in_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='range of numeric in ds1.c1 is between $threshold and 20')
    service.execute()
    service = AddParameterDefinition(session, validation_id=workflow_instance.workflow_id, parameter_name='$threshold',
                                     help_text='', description='dataset name', config_type='numeric',
                                     config_value='10')
    service.execute()
    assert len(service.response['PARAMETERS']) == 1
    assert service.response['PARAMETERS']['$threshold']['config_value'] == '10'


@patch("services.brms_validation.WorkflowGateway.restore")
def test_add_parameter_definition_in_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='range of numeric in ds1.c1 is between $threshold and 20')
    service.execute()
    workflow_instance.status = PUBLISHED

    service = AddParameterDefinition(session, validation_id=workflow_instance.workflow_id, parameter_name='$threshold',
                                     help_text='', description='dataset name', config_type='text',
                                     config_value='10')
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert len(service.response['PARAMETERS']) == 1
        assert service.response['PARAMETERS']['$threshold']['config_value'] == ''


@patch("vendor.redshift.Redshift.initialize")
@patch("services.brms_validation.WorkflowGateway.restore")
@patch("services.brms_validation.ScenarioGateway.restore")
def test_empty_execute_scenario(scenario, workflow, redshift):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow.return_value = workflow_instance
    scenario_meta = {"parameters": []}
    scenario_instance = ValidationScenario.initialize_with_dataset(1, 's1', scenario_meta, True,
                                                                   workflow_instance,
                                                                   execution_schema='brms_instance',
                                                                   output_schema='brms_output')
    scenario.return_value = scenario_instance

    workflow_instance.status = PUBLISHED
    service = ExecuteScenario(session, 1, workflow_instance.workflow_id,
                              execution_schema='brms_instance', output_schema='brms_instance')
    service.execute()

    assert service.response['META']['workflow_id'] == workflow_instance.workflow_id
    assert service.response['META']['scenario_id'] is not None
    assert service.response['META']['scenario_id'] != service.response['META']['workflow_id']
    assert 'QUERY' in service.response
    assert 'OUTPUT' in service.response
    assert len(service.response['QUERY']) == 6  # 2 queries for each success and error table
    assert len(service.response['OUTPUT']) == 3  # 3 records per dataset


@patch("vendor.redshift.Redshift.initialize")
@patch("services.brms_validation.WorkflowGateway.restore")
@patch("services.brms_validation.ScenarioGateway.restore")
def test_one_rule_execute_scenario(scenario, workflow, redshift):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow.return_value = workflow_instance
    service = AddRule(session, validation_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='data type of ds1.c1 is integer')
    service.execute()

    scenario_meta = {"parameters": [{"name": "$threshold", "value": "4"}]}
    scenario_instance = ValidationScenario.initialize_with_dataset(1, 's1', scenario_meta, True,
                                                                   workflow_instance,
                                                                   execution_schema='brms_instance',
                                                                   output_schema='brms_output')
    scenario.return_value = scenario_instance
    workflow_instance.status = PUBLISHED
    service = ExecuteScenario(session, 1, workflow_instance.workflow_id,
                              execution_schema='brms_instance', output_schema='brms_instance')
    service.execute()
    assert service.response['META']['workflow_id'] == workflow_instance.workflow_id
    assert service.response['META']['scenario_id'] is not None
    assert service.response['META']['scenario_id'] != service.response['META']['workflow_id']
    assert 'QUERY' in service.response
    assert len(service.response['QUERY']) == 7
    assert 'OUTPUT' in service.response
    assert len(service.response['OUTPUT']) == 3


@patch("vendor.redshift.Redshift.initialize")
@patch("services.brms_validation.WorkflowGateway.restore")
@patch("services.brms_validation.ScenarioGateway.restore")
def test_execute_draft_workflow(scenario, workflow, redshift):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow.return_value = workflow_instance
    scenario_meta = {"parameters": []}
    scenario_instance = ValidationScenario.initialize_with_dataset(1, 's1', scenario_meta, True,
                                                                   workflow_instance,
                                                                   execution_schema='brms_instance',
                                                                   output_schema='brms_output')
    scenario.return_value = scenario_instance

    with pytest.raises(BRMSDraftWorkflowExecuted):
        service = ExecuteScenario(session, 1, workflow_instance.workflow_id,
                                  execution_schema='brms_instance', output_schema='brms_instance')
        service.execute()
