#   datamax/api/services/tests/test_brms.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import json
from unittest.mock import Mock, patch

import pytest

from entity.scenario import ETLScenario
from entity.workflow import Workflow
from services.brms import CreateWorkflow, AddWorkflowMeta, AddRule, UpdateRule, SaveWorkflow, \
    GetWorkflow, EditWorkflow, CopyWorkflow, AddParameterDefinition, DeleteRule, GetParameterValues, \
    ExecuteScenario, VisualizeWorkflow, PublishWorkflow
from utils.exceptions import BRMSPublishedWorkflowModified, BRMSDraftWorkflowExecuted, BRMSDuplicateWorkflowName, \
    BRMSEmptyWorkflowPublished

empty_workflow_db = {
    "META": {"workflow_id": "d4c063a9ab734b24b1eb03c90a8597fb", "name": "hello", "description": "some desc",
             "workspace_id": 1, "status": "DRAFT", "health": True},
    "DATASETS": {
        "ds2": [{"column_name": "c1", "data_type": "varchar"}, {"column_name": "c2", "data_type": "varchar"},
                {"column_name": "c3", "data_type": "numeric"}, {"column_name": "c4", "data_type": "varchar"},
                {"column_name": "c5", "data_type": "varchar"}],
        "ds1": [{"column_name": "c1", "data_type": "varchar"}, {"column_name": "c2", "data_type": "varchar"},
                {"column_name": "c3", "data_type": "numeric"},
                {"column_name": "c4", "data_type": "varchar"}]
    },

    "SELECTED_DATASETS": {
        "ds2": {"meta": [{"column_name": "c1", "data_type": "varchar"}, {"column_name": "c2", "data_type": "varchar"},
                         {"column_name": "c3", "data_type": "numeric"}, {"column_name": "c4", "data_type": "varchar"},
                         {"column_name": "c5", "data_type": "varchar"}],
                "schema": "brms_instance"},
        "ds1": {"meta": [{"column_name": "c1", "data_type": "varchar"}, {"column_name": "c2", "data_type": "varchar"},
                         {"column_name": "c3", "data_type": "numeric"},
                         {"column_name": "c4", "data_type": "varchar"}],
                "schema": "brms_instance"}},
    "PARAMETERS": {},
    "RULES": [
    ]
}
PUBLISHED = 'PUBLISHED'
session = Mock()


def test_create_workflow():
    service = CreateWorkflow(session, workspace_id=1)
    service.execute()
    assert 'workflow_id' in service.response


@patch("services.brms.WorkflowGateway.restore")
def test_add_workflow_meta(restore):
    workflow_instance = Workflow()
    workflow_instance.workflow_id = empty_workflow_db['META']['workflow_id']
    restore.return_value = workflow_instance
    session.query.return_value.filter.return_value.all.return_value = []
    selected_datasets = json.dumps([{"name": "ds1",
                                     "adaptor_meta": empty_workflow_db['SELECTED_DATASETS']['ds1']['meta'],
                                     'schema': empty_workflow_db['SELECTED_DATASETS']['ds1']['schema']},
                                    {"name": "ds2",
                                     "adaptor_meta": empty_workflow_db['SELECTED_DATASETS']['ds2']['meta'],
                                     'schema': empty_workflow_db['SELECTED_DATASETS']['ds2']['schema']}])
    service = AddWorkflowMeta(session, workflow_instance.workflow_id, empty_workflow_db['META']['name'],
                              empty_workflow_db['META']['description'], selected_datasets=selected_datasets)
    service.execute()
    assert service.response['META'] == empty_workflow_db['META']


@patch("services.brms.WorkflowGateway.restore")
def test_add_rule(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance

    service = AddRule(session, workflow_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', output='union_rs',
                      user_input='union ds1 using c1, c2 and ds2 using c1,c2')
    service.execute()
    assert len(workflow_instance.rules.serialize) == 1


@patch("services.brms.WorkflowGateway.restore")
def test_add_rule_in_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance

    workflow_instance.status = PUBLISHED
    service = AddRule(session, workflow_id=workflow_instance.workflow_id, output='union_rs',
                      rule_name='dummy rule', user_input='union ds1 using c1, c2 and ds2 using c1, c2')
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert len(workflow_instance.rules.serialize) == 0


@patch("services.brms.WorkflowGateway.restore")
def test_update_rule(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id, output='union_rs',
                      rule_name='dummy rule', user_input='union ds1 using c1, c2 and ds2 using c1, c2')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']

    service = UpdateRule(session, workflow_id=workflow_instance.workflow_id, rule_name='dummy rule v2',
                         user_input='filter ds1 where (c2 > 0)', output='rs2', rule_id=rule_id)
    service.execute()
    assert len(service.response['RULES']) == 1
    assert 'filter' in service.response['RULES'][0]
    assert 'rs2' in service.response['DATASETS']
    assert 'union_rs' not in service.response['DATASETS']


@patch("services.brms.WorkflowGateway.restore")
def test_update_rule_incorrect_grammar(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id, output='union_rs',
                      rule_name='dummy rule', user_input='union ds1 using c1, c2 and ds2 using c1, c2')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']
    with pytest.raises(Exception):
        service = UpdateRule(session, workflow_id=workflow_instance.workflow_id, rule_name='dummy rule v2',
                             user_input='filter ds1', output='rs2', rule_id=rule_id)
        service.execute()
        assert len(service.response['RULES']) == 1
        assert 'union' in service.response['RULES'][0]
        assert 'union_rs' in service.response['DATASETS']
        assert 'rs2' not in service.response['DATASETS']


@patch("services.brms.WorkflowGateway.restore")
def test_update_rule_in_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance

    service = AddRule(session, workflow_id=workflow_instance.workflow_id, output='union_rs',
                      rule_name='dummy rule', user_input='union ds1 using c1, c2 and ds2 using c1, c2')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']
    workflow_instance.status = PUBLISHED
    service = UpdateRule(session, workflow_id=workflow_instance.workflow_id, rule_name='dummy rule v2',
                         user_input='filter ds1 where (c2 > 0)', output='rs2', rule_id=rule_id)
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert len(workflow_instance.rules.serialize) == 1


@patch("services.brms.WorkflowGateway.restore")
def test_delete_rule(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id, output='union_rs',
                      rule_name='dummy rule', user_input='union ds1 using c1, c2 and ds2 using c1, c2')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']

    service = DeleteRule(session, workflow_id=workflow_instance.workflow_id, rule_id=rule_id)
    service.execute()
    assert len(service.response['RULES']) == 0
    assert 'ds1' in service.response['DATASETS']
    assert 'union_rs' not in service.response['DATASETS']


@patch("services.brms.WorkflowGateway.restore")
def test_delete_rule_in_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance

    service = AddRule(session, workflow_id=workflow_instance.workflow_id, output='union_rs',
                      rule_name='dummy rule', user_input='union ds1 using c1, c2 and ds2 using c1, c2')
    service.execute()
    rule_id = service.response['RULES'][0]['rule_id']
    workflow_instance.status = PUBLISHED

    service = DeleteRule(session, workflow_id=workflow_instance.workflow_id, rule_id=rule_id)
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert len(workflow_instance.rules.serialize) == 1


@patch("services.brms.WorkflowGateway.restore")
def test_save_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = SaveWorkflow(session, workflow_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response is not None


@patch("services.brms.WorkflowGateway.restore")
def test_save_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    workflow_instance.status = PUBLISHED

    service = SaveWorkflow(session, workflow_id=workflow_instance.workflow_id)

    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert service.response is not None


@patch("services.brms.WorkflowGateway.restore")
def test_publish_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id, output='union_rs',
                      rule_name='dummy rule', user_input='union ds1 using c1, c2 and ds2 using c1, c2')
    service.execute()
    service = PublishWorkflow(session, workflow_id=workflow_instance.workflow_id)
    service.execute()

    assert service.response is not None


@patch("services.brms.WorkflowGateway.restore")
def test_publish_empty_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = PublishWorkflow(session, workflow_id=workflow_instance.workflow_id)
    with pytest.raises(BRMSEmptyWorkflowPublished):
        service.execute()


@patch("services.brms.WorkflowGateway.restore")
def test_get_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = GetWorkflow(session, workflow_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response == empty_workflow_db


@patch("services.brms.WorkflowGateway.restore")
def test_visualize_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = VisualizeWorkflow(session, workflow_id=workflow_instance.workflow_id)
    service.execute()
    assert 'nodes' in service.response
    assert 'edges' in service.response


@patch("services.brms.WorkflowGateway.restore")
def test_edit_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = EditWorkflow(session, workflow_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response == empty_workflow_db


@patch("services.brms.WorkflowGateway.restore")
def test_edit_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow_instance.status = PUBLISHED
    restore.return_value = workflow_instance

    service = EditWorkflow(session, workflow_id=workflow_instance.workflow_id)
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()


@patch("services.brms.WorkflowGateway.restore")
def test_copy_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    session.query.return_value.filter.return_value.all.return_value = []
    service = CopyWorkflow(session, workflow_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response['META']['workflow_id'] != empty_workflow_db['META']['workflow_id']
    assert service.response['META']['name'] == 'Copy of ' + empty_workflow_db['META']['name']
    assert service.response['META']['status'] == 'DRAFT'


@patch("services.brms.WorkflowGateway.restore")
def test_copy_duplicate_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    session.query.return_value.filter.return_value.all.return_value = [1]
    service = CopyWorkflow(session, workflow_id=workflow_instance.workflow_id)
    with pytest.raises(BRMSDuplicateWorkflowName):
        service.execute()


@patch("services.brms.WorkflowGateway.restore")
def test_copy_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow_instance.status = PUBLISHED
    restore.return_value = workflow_instance
    session.query.return_value.filter.return_value.all.return_value = []
    service = CopyWorkflow(session, workflow_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response['META']['workflow_id'] != empty_workflow_db['META']['workflow_id']
    assert service.response['META']['name'] == 'Copy of ' + empty_workflow_db['META']['name']
    assert service.response['META']['status'] == 'DRAFT'


@patch("services.brms.WorkflowGateway.restore")
def test_add_parameter_definition_in_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='filter ds1 where (c2 > $threshold)', output='filter_rs')
    service.execute()

    service = AddParameterDefinition(session, workflow_id=workflow_instance.workflow_id, parameter_name='$threshold',
                                     help_text='', description='threshold value', config_type='picklist',
                                     config_value='p1,p2')
    service.execute()
    assert len(service.response['PARAMETERS']) == 1
    assert service.response['PARAMETERS']['$threshold']['config_value'] == 'p1,p2'


@patch("services.brms.WorkflowGateway.restore")
def test_add_parameter_definition_in_published_workflow(restore):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='filter ds1 where (c2 > $threshold)', output='filter_rs')
    service.execute()
    workflow_instance.status = PUBLISHED

    service = AddParameterDefinition(session, workflow_id=workflow_instance.workflow_id, parameter_name='$threshold',
                                     help_text='', description='threshold value', config_type='picklist',
                                     config_value='p1,p2')
    with pytest.raises(BRMSPublishedWorkflowModified):
        service.execute()
        assert len(service.response['PARAMETERS']) == 1
        assert service.response['PARAMETERS']['$threshold']['config_value'] == ''


@patch("vendor.redshift.Redshift.initialize")
@patch("services.brms.WorkflowGateway.restore")
def test_get_parameter_values_for_table_column(restore, redshift):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='filter ds1 where (c2 > $threshold)', output='filter_rs')
    service.execute()

    service = AddParameterDefinition(session, workflow_id=workflow_instance.workflow_id, parameter_name='$threshold',
                                     help_text='', description='threshold value', config_type='Table column',
                                     config_value='s1,t1,c1')
    service.execute()
    service = GetParameterValues(session, workflow_id=workflow_instance.workflow_id)
    service.execute()
    assert '$threshold' in service.response


@patch("vendor.redshift.Redshift.initialize")
@patch("services.brms.WorkflowGateway.restore")
def test_get_parameter_values_with_no_table_column(restore, redshift):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    restore.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='filter ds1 where (c2 > $threshold)', output='filter_rs')
    service.execute()

    service = AddParameterDefinition(session, workflow_id=workflow_instance.workflow_id, parameter_name='$threshold',
                                     help_text='', description='threshold value', config_type='text',
                                     config_value='')
    service.execute()
    service = GetParameterValues(session, workflow_id=workflow_instance.workflow_id)
    service.execute()
    assert service.response == {}


@patch("vendor.redshift.Redshift.initialize")
@patch("services.brms.WorkflowGateway.restore")
@patch("services.brms.ScenarioGateway.restore")
def test_empty_execute_scenario(scenario, workflow, redshift):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow.return_value = workflow_instance
    scenario_meta = {"parameters": [], "output_adaptors": []}
    scenario_instance = ETLScenario.initialize_with_dataset(1, 's1', scenario_meta, True,
                                                            workflow_instance,
                                                            execution_schema='brms_instance',
                                                            output_schema='brms_output')
    scenario.return_value = scenario_instance

    workflow_instance.status = PUBLISHED
    service = ExecuteScenario(session, 1, workflow_instance.workflow_id,
                              execution_schema='brms_instance', output_schema='brms_instance')
    service.execute()

    assert service.response['META']['workflow_id'] == workflow_instance.workflow_id
    assert service.response['META']['scenario_id'] is not None
    assert service.response['META']['scenario_id'] != service.response['META']['workflow_id']
    assert 'QUERY' in service.response
    assert 'OUTPUT' in service.response
    assert len(service.response['QUERY']) == 0
    assert len(service.response['OUTPUT']) == 2


@patch("vendor.redshift.Redshift.initialize")
@patch("services.brms.WorkflowGateway.restore")
@patch("services.brms.ScenarioGateway.restore")
def test_one_rule_execute_scenario(scenario, workflow, redshift):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow.return_value = workflow_instance
    service = AddRule(session, workflow_id=workflow_instance.workflow_id,
                      rule_name='dummy rule', user_input='filter ds1 where (c2 > $threshold)', output='filter_rs')
    service.execute()

    scenario_meta = {"parameters": [{"name": "$threshold", "value": "4"}], "output_adaptors": ["filter_rs"]}
    scenario_instance = ETLScenario.initialize_with_dataset(1, 's1', scenario_meta, True,
                                                            workflow_instance,
                                                            execution_schema='brms_instance',
                                                            output_schema='brms_output')
    scenario.return_value = scenario_instance
    workflow_instance.status = PUBLISHED
    service = ExecuteScenario(session, 1, workflow_instance.workflow_id,
                              execution_schema='brms_instance', output_schema='brms_instance')
    service.execute()
    assert service.response['META']['workflow_id'] == workflow_instance.workflow_id
    assert service.response['META']['scenario_id'] is not None
    assert service.response['META']['scenario_id'] != service.response['META']['workflow_id']
    assert 'QUERY' in service.response
    assert len(service.response['QUERY']) == 2
    assert 'OUTPUT' in service.response
    assert len(service.response['OUTPUT']) == 3


@patch("vendor.redshift.Redshift.initialize")
@patch("services.brms.WorkflowGateway.restore")
@patch("services.brms.ScenarioGateway.restore")
def test_execute_draft_workflow(scenario, workflow, redshift):
    workflow_instance = Workflow.initialize(empty_workflow_db)
    workflow.return_value = workflow_instance
    scenario_meta = {"parameters": [], "output_adaptors": []}
    scenario_instance = ETLScenario.initialize_with_dataset(1, 's1', scenario_meta, True,
                                                            workflow_instance,
                                                            execution_schema='brms_instance',
                                                            output_schema='brms_output')
    scenario.return_value = scenario_instance

    with pytest.raises(BRMSDraftWorkflowExecuted):
        service = ExecuteScenario(session, 1, workflow_instance.workflow_id,
                                  execution_schema='brms_instance', output_schema='brms_instance')
        service.execute()
