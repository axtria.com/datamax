import datetime
import re
import logging
import pandas as pd
import json
from sqlalchemy import exc,or_,func
from sqlalchemy.exc import ResourceClosedError
from sqlalchemy.sql.functions import count
from functools import reduce

from collections.abc import Iterable #not in use at present


from models import API, APIMeta, Connector,UserGroup,UserGroupUserMapping,User
from services import Service, ServiceFactory
from utils.exceptions import ApiExchangeIntegrityFailed
from vendor.appdb import AppDb
from vendor.redshift import Redshift

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from app import smtphost,glasalluseremail



def SendEmail(to: str,cc:str,content:str,subject:str, sender:str, params :dict ):
    host = smtphost
    attachments = params.get('attachements',None)
    emailtype = 'plain' if params.get('type',None) is None else params.get('type')
    
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] =  to
    #msg['Cc'] = cc
    msg['Subject'] = subject
    msg.attach(MIMEText(content,emailtype))
    log = logging.getLogger(__name__)
    log.debug(f'message sent. subject : {subject}, content : {content},to : {to} , cc : {cc}')
    try:
        smtpclient = smtplib.SMTP(host)
        smtpclient.starttls()
        smtpclient.set_debuglevel(1)
        smtpclient.send_message(msg)
        log.debug(f'message sent successfully..!')
        smtpclient.quit()
    except Exception as e:        
        log.exception(f'error while sending the email. {type(e)} - {str(e)}')
        raise e

class SendNotification(Service):
    def __init__(self, session,**kwargs):
        super().__init__()
        self.params = kwargs
        self.name = str.lower(self.params.get('name',''))
        self.postgres = session
        self.body = kwargs.get("details",'')
        self.usergroupids =  '' if (len(kwargs.get("usergroup_ids",''))  == 0 or kwargs.get("usergroup_ids",'') == 'null')  else \
            list(map(lambda ugpid: int(ugpid), kwargs.get("usergroup_ids",'').split(',')))
        self.usergroupuser = {}
        self.title = kwargs.get("title",'')
        self.alluser = str.lower(kwargs.get("all_users",''))  == 'true'   
        # retrieve recipient_to_usr from params
        # populate these using api_exchagne
        self.status = False


    def execute(self):
        self.log.debug(self.params)
        usergroups = ''
        users = ''
        serverresponse = ''
        sender = ''
        emailsentstatus = False

        #retrieve usergroups email ids if self.recipient_grps not None set it to self.recipient_to list , usrgrp1, usrgrp2
        try:
            for eventmeta in self.postgres.query(EventMeta).filter(
                or_(func.lower(EventMeta.event_api_name)== self.name, func.lower(EventMeta.event_name) == self.name), EventMeta.is_active == True).all():
                self.alluser = eventmeta.recipient_grp_all_flag & self.alluser # event_meta in db will override group sent flag value in case set to false in db
                sender = eventmeta.sender_email
                if eventmeta.api_after_event is not None: 
                    usergroups = '' if eventmeta.recipient_to_grp is None else eventmeta.recipient_to_grp  

                    engine = AppDb.engine
                    for api, api_meta, connector in self.postgres.query(API, APIMeta, Connector).filter(
                        API.name == eventmeta.api_after_event, API.status == 'published'). \
                        join(APIMeta, APIMeta.api_id == API.id).join(Connector, Connector.id == APIMeta.connector_id).all():
                        self.log.debug(api_meta.query)
                        self.log.debug(self.params)
                        try:                        
                            contents = pd.read_sql(api_meta.query, engine, params=self.params).to_dict(orient='records')
                        except exc.IntegrityError as e:
                            self.log.exception(e)
                            raise ApiExchangeIntegrityFailed
                        except ResourceClosedError:
                            contents = {}
                        if( len(contents) > 0):
                            self.params.update(**contents[0])
                            usergroups = f'{usergroups},{self.params.get("recipient_to_grp","")}'
                            usergroups = usergroups.strip(',')
                            users = self.params.get("recipient_to_usr",'')
                            users = users.strip(',')
                            

                for template in self.postgres.query(Template_Meta).filter(
                    Template_Meta.event_id ==
                    eventmeta.event_id,Template_Meta.is_active == True).all():  
            
                    self.body = template.template_body.format(**self.params) if len(self.body) == 0 else self.body 
                    self.title = template.template_title if len(self.title) == 0 else self.title
                        
                
                # retrieve recepient_to from usergroup list from input parameter and database
                
                records = self.postgres.query(UserGroup,User).filter(and_(or_(
                    func.lower(UserGroup.name).in_(str.lower(usergroups).split(',')) ,\
                    UserGroup.usergroup_id.in_(self.usergroupids)), UserGroup.is_active == True, User.is_active==True)).join( \
                    UserGroupUserMapping, and_(UserGroupUserMapping.usergroup_id == UserGroup.usergroup_id, UserGroupUserMapping.is_active == True)). \
                    join(User,User.user_id == UserGroupUserMapping.user_id).all()
                
                self.usergroupuser = self.usergrouptousers(records)
                

                
                # entry into event_instance
                recipientusrs = '' # shall be populated from the dbquery api_exchagne or passed into the input parameter
                recipientusrs = reduce((lambda email1,email2:f'{email1},{email2}'),list(self.usergroupuser.values())) if len(self.usergroupuser) >0 else ''
                recipientusrs = f'{recipientusrs},{users},{ glasalluseremail if self.alluser else ""}'
                recipientusrs =  recipientusrs.replace(',,',',').strip(',')

                recipientusers_cc = ''

                recipientusrsgrp = '' # shall be populated from the dbquery api_exchagne or passed into the input parameter
                recipientusrsgrp = reduce((lambda usergp1,usergp2:f'{usergp1},{usergp2}'),list(self.usergroupuser.keys())) if len(self.usergroupuser) >0 else ''
                recipientusrsgrp = recipientusrsgrp.strip(',')

                usergrp_email_mapping = reduce(lambda item1,item2 : f'{item1[0]}:{item1[1]};{item2[0]}:{item2[1]}', 
                list(zip(self.usergroupuser.keys(), self.usergroupuser.values()))) if len(self.usergroupuser) >0 else ''
                if (len(recipientusrs.replace(',','').strip()) > 0):
                    instance = Event_Instance( event_id = eventmeta.event_id, recipient_to_usr =  
                    recipientusrs, 
                    recipient_to_grp = recipientusrsgrp,
                    usrgrp_email_mapping =  usergrp_email_mapping,
                    recipient_grp_all_flag =  self.alluser,
                    is_active = True,
                    content = self.body, title = self.title,
                    created_date  =  func.now(),
                    modified_date  =  func.now(),
                    created_by = self.params["loggedin_userid"],
                    modified_by = self.params["loggedin_userid"]
                    )
                    self.postgres.add(instance)
                    self.postgres.commit()
                    self.status = not sender == '' 
                    try:
                        if(self.status):
                            SendEmail(recipientusrs,recipientusers_cc,self.body, self.title, sender,self.params)
                            instance.sent_status = "success"
                            instance.sent_on = func.now()
                            emailsentstatus = True
                            
                    except Exception as e:
                        self.log.exception(e)                        
                        instance.sent_status = "fail"
                        instance.server_response = str(e) 
                    self.postgres.commit()
        except Exception as e:
            self.log.exception(e)
             
        content =   "Notification sent successfully" if emailsentstatus else "Notification saved but could not be sent" if self.status else "Notification error"
        self.add_response('Contents', content)
        self.add_response('ResponseMetadata', {'HTTPStatusCode': 200, 'HTTPHeaders':
            {'date': datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), 'content-type': 'application/json',
                'transfer-encoding': 'none', 'server': 'Notification'}})

                
    def usergrouptousers(self,usergroups :list)->dict:
        ret = {}
        for ugp ,u in usergroups:
            if(ret.get(ugp.name) is None):
               ret[ugp.name] = u.email 
            else:
                ret[ugp.name] = f'{ret[ugp.name]},{u.email}'
        return ret

            


        



