import datetime

import pandas as pd
from services import Service
from vendor.appdb import AppDb
from vendor.redshift import Redshift
from services.api_exchange import ApiExchange
from vendor.snowflake import Snowflake
from sqlalchemy.exc import ResourceClosedError

class RedshiftBroker(Service):
    def __init__(self, session,  rs_connector_id: int, **kwargs):
        super().__init__()
        self.params = kwargs
        self.postgres = session
        self.rs_connector_id = rs_connector_id
        self.redshift = Redshift.initialize(self.rs_connector_id, session)

    def execute(self):
        command = self.params['command']
        self.log.debug('commnd to execute in postgres - %r' % command)
        command = 'select {0}'.format( command)  
        try:
            query = pd.read_sql(command, AppDb.engine).iloc[0, 0]
            self.log.debug('redshift query to execute %r'% query)    
            response = self.redshift.execute(query).to_dict(orient='records')            
        except:
            self.log.debug('redshift query execution error')
            raise
        self.add_response('response', response)