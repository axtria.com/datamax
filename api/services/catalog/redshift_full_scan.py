#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
'''Datamax Catalog - Redshift full scan'''

import pandas as pd 
from services import Service
from vendor.neo4j import Neo4j 
from vendor.redshift import Redshift
from models import Connector
from datetime import datetime
from numpy import random
from vendor.appdb import AppDb
from utils.helpers import thread

neo4j = Neo4j()
app_db = AppDb()

class RedshiftFullScan(Service):
	'''Register metadata of Redshift to neo4j db'''
	def __init__(self, session, rs_connector_id, **kwargs):
		super().__init__()

		self.session = session
		self.rs_connector_id = int(rs_connector_id)
		self.rs = Redshift.initialize(rs_connector_id, session)
		self.over_the_network = True
		self.add_response('message','Request Acknowledged')		 

	@thread		
	def execute(self):

		filter_config = app_db.execute("select name as layer_name, layer_id, description, schema_name, connector_id from admn.layer where layer_type='rs' and is_active")

		schemas = filter_config.loc[filter_config['connector_id'] == self.rs_connector_id,'schema_name'].tolist()

		schemas_tuple = "('" + "','".join(schemas) + "')" 

		rs_query =  'select '\
					't.table_schema as table_schema, '\
					't.table_name as table_name, '\
					't.table_type as table_type, '\
					't.remarks as table_remarks, '\
					'c.column_name as column_name, '\
					'c.data_type as column_datatype, '\
					'c.ordinal_position as column_ordinal_position, '\
					'c.remarks as column_remarks '\
					'from '\
					'svv_columns as c join svv_tables as t '\
					'on '\
					'(t.table_name = c.table_name and t.table_schema = c.table_schema) '\
					f'where t.table_schema in {schemas_tuple};'


		metadata = self.rs.execute(rs_query)
		metadata.fillna('No description available', inplace=True)
		metadata = pd.merge(metadata, filter_config, how='left', left_on='table_schema', right_on='schema_name')

		rs_connector = self.session.query(Connector).filter(Connector.id == self.rs_connector_id).first()

		database = rs_connector.connection['DBNAME']
		host = rs_connector.connection['HOST']
		port = rs_connector.connection['PORT']
		cluster = host.split('.')[0]

		if self.over_the_network:

			for _,row in metadata.iterrows():

				neo4j_query =	f'merge(rc:RedshiftCluster{{name:"{cluster}", host:"{host}", port:{port}}}) set rc +=  {{deleted:false}} '\
								f'merge(r:Redshift{{database:"{database}", host:"{host}", port:{port}}}) set r +=  {{deleted:false}} '\
								f'merge(s:RedshiftSchema{{name:"{row.table_schema}", database:"{database}", host:"{host}", port:{port}}}) set s +=  {{layer_id:"{row.layer_id}",connector_id:"{self.rs_connector_id}",deleted:false, layer:"{row.layer_name}", remarks:"{row.description}"}} '\
								f'merge(t:RedshiftTable{{name:"{row.table_name}", schema:"{row.table_schema}", type:"{row.table_type}", database:"{database}", host:"{host}", port:{port}}}) set t +=  {{layer:"{row.layer_name}", layer_id:"{row.layer_id}", deleted:false, remarks:"{row.table_remarks}"}} '\
								f'merge(c:RedshiftColumn{{name:"{row.column_name}", table:"{row.table_name}", schema:"{row.table_schema}", database:"{database}", host:"{host}", port:{port}}}) set c +=  {{layer:"{row.layer_name}", layer_id:"{row.layer_id}",deleted:false, remarks:"{row.column_remarks}", ordinal_position:{row.column_ordinal_position}, datatype:"{row.column_datatype}"}} '\
								'merge((rc)-[:HAS]->(r)) '\
								'merge((r)-[:HAS]->(s)) '\
								'merge((s)-[:HAS]->(t)) '\
								'merge((t)-[:HAS]->(c)) ' 

				_ = neo4j.run_query(neo4j_query)
			
		else:

			fname = f'tmp_{self.uid()}.csv'
			fpath = f'{neo4j.config.import_dir()}{fname}'

			metadata.to_csv(fpath, index=None)

			neo4j_query = 	f'load csv with headers from "file:///{fname}" as row '\
							f'merge(rc:RedshiftCluster{{name:"{cluster}", host:"{host}", port:{port}}}) set rc +=  {{deleted:false}} '\
							f'merge(r:Redshift{{database:"{database}", host:"{host}", port:{port}}}) set r +=  {{deleted:false}} '\
							f'merge(s:RedshiftSchema{{name:row.table_schema, database:"{database}", host:"{host}", port:{port}}}) set s += {{deleted:false, layer:row.layer_name, remarks:row.description}} '\
							f'merge(t:RedshiftTable{{name:row.table_name, schema:row.table_schema, type:row.table_type, database:"{database}", host:"{host}", port:{port}}}) set t +=  {{deleted:false, remarks:row.table_remarks}} '\
							f'merge(c:RedshiftColumn{{name:row.column_name, table:row.table_name, schema:row.table_schema, database:"{database}", host:"{host}", port:{port}}}) set c +=  {{deleted:false, remarks:row.column_remarks, ordinal_position:row.column_ordinal_position, datatype:row.column_datatype}} '\
							'merge((rc)-[:HAS]->(r)) '\
							'merge((r)-[:HAS]->(s)) '\
							'merge((s)-[:HAS]->(t)) '\
							'merge((t)-[:HAS]->(c)) ' 

			_ = neo4j.run_query(neo4j_query)

		self.add_response('status','success')


	def uid(self):
		return hex(random.randint(10**6)) + datetime.now().strftime('___%d_%b_%Y__%H_%M_%S_%f')