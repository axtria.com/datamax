#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
'''Datamax Catalog - Display list view'''

from services import Service
from vendor.neo4j import Neo4j

class DisplayListView(Service):
	'''Gets a list view of catalog'''
	neo4j = Neo4j()

	def __init__(self, session, **kwargs):
		super().__init__()

		self.session = session

		
	def execute(self):
		counter = 1

		layers = []

		for _,s in DisplayListView.neo4j.get_nodes('RedshiftSchema', deleted=False).iterrows():

			layer = {

				'Name':s['name'],
				'Type':'Redshift Schema',
				'Description': 'No description available' if 'remarks' not in s.keys() else s['remarks'] if isinstance(s['remarks'],str) else 'No description available',
				'Last Updated':'NA',
				'Tags':'' if 'tags' not in s.keys() else s['tags'].replace(',',', ') if isinstance(s['tags'],str) else '',
				'URI': 'rs://'+s['name'],
				'Business Description': 'No description available' if 'buss_desc' not in s.keys() else s['buss_desc'] if isinstance(s['buss_desc'],str) else 'No description available'
			}

			tables = []

			for _,t in DisplayListView.neo4j.get_nodes('RedshiftTable', schema = s['name'], deleted=False).iterrows():

				table = {
					'Name': t['name'],
					'Type': t['type'],
					'Description': 'No description available' if 'remarks' not in t.keys() else t['remarks'] if isinstance(t['remarks'],str) else 'No description available',
					'Last Updated':'NA',
					'Tags':'' if 'tags' not in t.keys() else t['tags'].replace(',',', ') if isinstance(t['tags'],str) else '',
					'URI': 'rs://'+s['name']+'/'+t['name'],
					'Business Description': 'No description available' if 'buss_desc' not in t.keys() else t['buss_desc'] if isinstance(t['buss_desc'],str) else 'No description available'
				}

				columns =[]

				for _,c in DisplayListView.neo4j.get_nodes('RedshiftColumn', schema = s['name'], table = t['name'], deleted=False).iterrows():

					column = {
						'Name': c['name'],
						'Type': c['datatype'],
						'Position': c['ordinal_position'],
						'Description': 'No description available' if 'remarks' not in c.keys() else c['remarks'] if isinstance(c['remarks'],str) else 'No description available',
						'Tags':'' if 'tags' not in c.keys() else c['tags'].replace(',',', ') if isinstance(c['tags'],str) else '',
						'URI': 'rs://'+s['name']+'/'+t['name']+'::'+c['name'],
						'Business Description': 'No description available' if 'buss_desc' not in c.keys() else c['buss_desc'] if isinstance(c['buss_desc'],str) else 'No description available'
					}
					columns.append(column)

				table['children'] = columns
				table['Count'] = len(columns)

				tables.append(table)

			layer['children'] = tables
			layer['Count'] = len(tables)

			layers.append(layer)



		key_identifier = '__datamax_internal__'
		key = lambda x: key_identifier+x
		dekey = lambda x: x.replace(key_identifier,'')

		for _,b in DisplayListView.neo4j.get_nodes('S3Bucket', deleted=False).iterrows():

			layer = {

				'Name':b['bucketName'],
				'Type':'S3 Bucket',
				'Description':'No description available' if 'remarks' not in b.keys() else b['remarks'] if isinstance(b['remarks'],str) else 'No description available',
				'Last Updated':b['lastUpdated'],
				'Tags':'' if 'tags' not in b.keys() else b['tags'].replace(',',', ') if isinstance(b['tags'],str) else '',
				'URI':'s3://'+b['bucketName'],
				'Business Description': 'No description available' if 'buss_desc' not in b.keys() else b['buss_desc'] if isinstance(b['buss_desc'],str) else 'No description available'
			}

			layer_contents = NestedDict()

			folder_details = DisplayListView.neo4j.get_nodes('S3Folder', bucketName = b['bucketName'], deleted=False)
			folder_details['folder_level'] = folder_details['folderPath'].str.count('/')
			folder_details = folder_details.sort_values('folder_level').reset_index(drop=True)

			for _,f in folder_details.iterrows():

				folder = {
					key('Name'): f['name'],
					key('Type'): 'Folder',
					key('Description'): 'No description available' if 'remarks' not in f.keys() else f['remarks'] if isinstance(f['remarks'],str) else 'No description available',
					key('Last Updated'):f['lastUpdated'],
					key('Tags'):'' if 'tags' not in f.keys() else f['tags'].replace(',',', ') if isinstance(f['tags'],str) else '',
					key('URI'):'s3://'+f['bucketName']+'/'+f['folderPath'][:-1],
					key('Business Description'): 'No description available' if 'buss_desc' not in f.keys() else f['buss_desc'] if isinstance(f['buss_desc'],str) else 'No description available'
				}


				json_level = f['folderPath'].split('/')[:-1]
				layer_contents.update_level(json_level, folder, self)


			files = []

			for _,f in DisplayListView.neo4j.get_nodes('S3File', bucketName = b['bucketName'], deleted=False).iterrows():

				file = {
					key('Name'): f['name'],
					key('Type'): 'Delimited Data File' if f['detectedColumns'] else 'Other',
					key('Description'): 'No description available' if 'remarks' not in f.keys() else f['remarks'] if isinstance(f['remarks'],str) else 'No description available',
					key('Last Updated'):f['lastUpdated'],
					key('Tags'):'' if 'tags' not in f.keys() else f['tags'].replace(',',', ') if isinstance(f['tags'],str) else '',
					key('URI'):'s3://'+f['bucketName']+'/'+f['objectKey'],
					key('Size'):f['size'],
					key('Business Description'): 'No description available' if 'buss_desc' not in f.keys() else f['buss_desc'] if isinstance(f['buss_desc'],str) else 'No description available'
				}

				columns =[]

				for _,c in DisplayListView.neo4j.get_nodes('S3FileDetectedColumn', bucketName = b['bucketName'], objectKey = f['objectKey'], deleted=False).iterrows():

					column = {
						'Name': c['name'],
						'Type': c['dataType'],
						'Position': c['ordinalPosition'],
						'Description': 'No description available' if 'remarks' not in c.keys() else c['remarks'] if isinstance(c['remarks'],str) else 'No description available',
						'Tags':'' if 'tags' not in c.keys() else c['tags'].replace(',',', ') if isinstance(c['tags'],str) else '',
						'URI':'s3://'+f['bucketName']+'/'+f['objectKey']+'::'+c['name'],
						'Business Description': 'No description available' if 'buss_desc' not in c.keys() else c['buss_desc'] if isinstance(c['buss_desc'],str) else 'No description available'
					}
					columns.append(column)

				file['children'] = columns
				file[key('Count')] = len(columns)

				files.append(file)
				json_level = f['objectKey'].split('/')
				layer_contents.update_level(json_level, file, self)


			system_attributes = [key(i) for i in ['Name','Type','Count','Last Updated','Tags','Description','Position','URI','Business Description','Size']]

			def make_children(content_dict):

				if 'children' not in content_dict.keys():

					children = []

					for k in set(content_dict.keys())-set(system_attributes):
						children.append(content_dict[k])
						del content_dict[k]

					content_dict['children'] = children
					content_dict['Count'] = 0

					for child in children:
						content_dict['Count'] += make_children(child)

					return content_dict['Count']
				return 1

			def dekey_system_attributes(content_dict):

				for k in content_dict.keys():

					if k.startswith(key_identifier):
						content_dict[dekey(k)] = content_dict[k]
						del content_dict[k]

					if 'children' in content_dict.keys():
						for child in content_dict['children']:
							dekey_system_attributes(child)

			count = make_children(layer_contents)
			dekey_system_attributes(layer_contents)

			layer['children'] = layer_contents['children']
			layer['Count'] = count

			layers.append(layer)

		self.add_response('contents', layers)
		self.add_response('status','success')


class NestedDict(dict):
	
	def __init__(self, dictionary = {}):
		super().__init__(dictionary)

	def __missing__(self, key):
		
		self[key] = NestedDict()
		return self[key]
	
	def update_level(self, levels: list, value, ob):

		temp = self
		nlevels = len(levels)
		
		for n, level in enumerate(levels):
			
			if n+1 != nlevels:
				temp = temp[level]

		temp[level] = value
