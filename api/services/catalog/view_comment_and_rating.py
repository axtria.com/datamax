#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - View Comment and rating"""

from services import Service
from vendor.neo4j import Neo4j 
from datetime import datetime

class ViewCommentAndRating(Service):
	"""view comment and rating in neo4j"""
	def __init__(self, session, node_id, **kwargs):
		super().__init__()

		self.neo4j = Neo4j()

		self.node_id = node_id

	def execute(self):

		node = self.neo4j.get_node_by_id(self.node_id)

		response = self._get(node)

		self.add_response('status','success')
		self.add_response('contents', response)


	def _get(self, node):

		user_comments = node.get('user_comments').split(';') if 'user_comments' in node.keys() else []

		user_ratings = node.get('user_ratings').split(';') if 'user_ratings' in node.keys() else []
		user_ratings = [int(r) for r in user_ratings]

		rating_users = node.get('rating_users').split(';') if 'rating_users' in node.keys() else []

		rating_timestamps = node.get('rating_timestamps').split(';') if 'rating_timestamps' in node.keys() else []

		comments = []

		for i, comment in enumerate(user_comments):
			if comment:
				comments.append({'comment':comment, 'rating':user_ratings[i], 'user':rating_users[i], 'timestamp':rating_timestamps[i]})

		average_rating = round(sum(user_ratings)/len(user_ratings),2) if user_ratings else None

		rating_frequency = {i:user_ratings.count(i) for i in range(6)}

		return {'comments':comments, 'average_rating':average_rating, 'rating_frequency':rating_frequency}








