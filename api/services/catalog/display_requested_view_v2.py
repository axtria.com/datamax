#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author(s):
#   Akshay Saini <akshay.saini@axtria.com>
#   Nitin Sharma <nitin.sharma@axtria.com>
#   Varun Singhal <varun.singhal@axtria.com>
"""Datamax Catalog - Display requested view"""

from services import Service
from vendor.appdb import AppDb
from vendor.neo4j import Neo4j


class DisplayRequestedView(Service):

    def __init__(self, session, node_id=None, **kwargs):
        super().__init__()
        self.session = session
        self.params = kwargs
        self.neo4j = Neo4j()
        self.id = int(node_id) if node_id else node_id
    def execute(self):
        if not self.id:
            layers = self.get_layers()
            self.add_response('contents', layers)
            self.add_response('status', 'success')
        else:
            parent_node = self.neo4j.get_node_by_id(self.id)
            if str(parent_node.labels)[1:] in ['S3Bucket']:
                # children = self.neo4j.get_nodes('Object')
                children = self.neo4j.run_query(f"match(n:Object) return n as child", True)
                children = [self.prepare_objects(c['child']) for c in children]
                orphan_children = self.neo4j.run_query(
                    f"match((n)-[:HAS]->(m{{deleted:False}})) where id(n) = {self.id} return m as child", True)
                children += [self.prepare_objects(c['child']) for c in orphan_children]

            else:
                children = self.neo4j.run_query(
                    f"match((n)-[:HAS]->(m{{deleted:False}})) where id(n) = {self.id} return m as child", True)
                children = [self.prepare_objects(c['child']) for c in children]

            self.add_response('contents', children)
            self.add_response('status', 'success')

    def get_layers(self):
        layers = []
        for _, s in self.neo4j.get_nodes('RedshiftSchema', deleted=False).iterrows():
            layer = {
                'Connector_id': s['connector_id'],
                'Name': s['name'],
                'Layer Name': s['__label'],
                'Layer id': int(s['layer_id']),
                'Type': 'Redshift Schema',
                'Description': s['description'] if 'description else' in s.keys() else 'No description available',
                'Comments': '' if 'comments' not in s.keys() else s['comments'] if isinstance(s['comments'],
                                                                                              str) else '',
                'Last Updated': 'NA',
                'Tags': self.get_tags(s['__id']),
                'URI': 'rs://' + s['name'] + '/',
                'Business Description': 'No description available' if 'buss_desc' not in s.keys() else s[
                    'buss_desc'] if isinstance(s['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in s.keys() else s[
                    'tech_desc'] if isinstance(s['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in s.keys() else s[
                    'logi_desc'] if isinstance(s['logi_desc'], str) else 'No description available',
                'Count': self._children_count(s['__id']),
                'id': s['__id'],
                'ParentItem': self.id,
                'isParent': True
            }

            layers.append(layer)

        for _, s in self.neo4j.get_nodes('SnowflakeSchema', deleted=False).iterrows():
            layer = {
                'Name': s['name'],
                'Layer id': int(s['layer_id']),
                'Layer Name': '' if 'layer' not in s.keys() else s['layer'] if isinstance(s['layer'], str) else '',
                'Type': 'Snowflake Schema',
                'Description': 'No description available' if 'remarks' not in s.keys() else s['remarks'] if isinstance(
                    s['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in s.keys() else s['comments'] if isinstance(s['comments'],
                                                                                              str) else '',
                'Last Updated': 'NA',
                'Tags': self.get_tags(s['__id']),
                'URI': 'sf://' + s['layer'] + '/',
                "URI_ID" : s.get("uri",""),
                'Business Description': 'No description available' if 'buss_desc' not in s.keys() else s[
                    'buss_desc'] if isinstance(s['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in s.keys() else s[
                    'tech_desc'] if isinstance(s['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in s.keys() else s[
                    'logi_desc'] if isinstance(s['logi_desc'], str) else 'No description available',
                'Count': self._children_count(s['__id']),
                'id': s['__id'],
                'ParentItem': self.id,
                'isParent': True
            }

            layers.append(layer)

        for _, b in self.neo4j.get_nodes('S3Bucket', deleted=False).iterrows():
            layer = {
                'Name': b['bucketName'],
                'Layer id': int(b['layer_id']),
                'Layer Name': '' if 'layer' not in b.keys() else b['layer'] if isinstance(b['layer'], str) else '',
                'Type': 'S3 Bucket',
                'Description': 'No description available' if 'remarks' not in b.keys() else b['remarks'] if isinstance(
                    b['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in b.keys() else b['comments'] if isinstance(b['comments'],
                                                                                              str) else '',
                'Last Updated': b['lastUpdated'],
                'Tags': self.get_tags(b['__id']),
                'URI': 's3://' + b['layer'] + '/',
                "URI_ID": b.get("uri", ""),
                'Business Description': 'No description available' if 'buss_desc' not in b.keys() else b[
                    'buss_desc'] if isinstance(b['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in b.keys() else b[
                    'tech_desc'] if isinstance(b['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in b.keys() else b[
                    'logi_desc'] if isinstance(b['logi_desc'], str) else 'No description available',
                'Count': self._recursive_object_count_s3(b['__id']),
                'id': b['__id'],
                'ParentItem': self.id,
                'isParent': True
            }

            layers.append(layer)

        return layers

    def prepare_objects(self, obj):

        node_type = str(obj.labels)[1:]

        json = None

        if node_type == 'SnowflakeTable':
            json = {
                'Name': obj['name'],
                'Type': obj['type'],
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Last Updated': 'NA',
                'Tags': self.get_tags(obj.identity),
                'URI': 'sf://' + self.neo4j.get_node_by_id(self.id)['layer'] + '/' + obj['name'],
                "URI_ID": obj.get("uri", ""),
                'Logical Name': 'No Name available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No Name available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'Count': self._children_count(obj.identity),
                'id': obj.identity,
                'ParentItem': self.id,
                'isParent': True,
                'Average Rating': 0 if 'average_rating' not in obj.keys() else obj['average_rating'] if obj[
                    'average_rating'] else 0
            }

        if node_type == 'SnowflakeColumn':
            layer_name = self.neo4j.run_query(
                f'match((L)-[:HAS]->(t)) where id(t) = {self.id} return L.layer as layer', True).pop()['layer']
            json = {
                'Name': obj['name'],
                "attr_id" : obj['attr_id'],
                'Type': obj['datatype'],
                'Position': obj['ordinal_position'],
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Tags': self.get_tags(obj.identity),
                'URI': 'sf://' + layer_name + '/' + obj['table'] + '::' + obj['name'],
                "URI_ID": obj.get("uri", ""),
                'Logical Name': 'No description available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No description available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'id': obj.identity,
                'ParentItem': self.id,
                'isParent': False
            }

        if node_type == 'RedshiftTable':
            json = {
                'Name': obj['name'],
                'Type': obj['type'],
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Last Updated': 'NA',
                'Tags': self.get_tags(obj.identity),
                'URI': 'rs://' + obj['schema'] + '/' + obj['name'],
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'Count': self._children_count(obj.identity),
                'id': obj.identity,
                'ParentItem': self.id,
                'isParent': True,
                'Average Rating': 0 if 'average_rating' not in obj.keys() else obj['average_rating'] if obj[
                    'average_rating'] else 0
            }

        if node_type == 'RedshiftColumn':
            json = {
                'Name': obj['name'],
                'Type': obj['datatype'],
                'Position': obj['ordinal_position'],
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Tags': self.get_tags(obj.identity),
                'URI': 'rs://' + obj['schema'] + '/' + obj['table'] + '::' + obj['name'],
                'Logical Name': 'No description available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No description available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'id': obj.identity,
                'ParentItem': self.id,
                'isParent': False
            }

        if node_type == 'S3Folder':
            layer = self.neo4j.run_query(
                f'match((L:S3Bucket)-[:HAS*1..]->(f)) where id(f) = {obj.identity} return L.layer as layer',
                True).pop()['layer']
            json = {
                'Name': obj['name'],
                'Type': 'Folder',
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Last Updated': obj['lastUpdated'],
                'Tags': self.get_tags(obj.identity),
                'URI': 's3://' + layer + '/' + obj['folderPath'][:-1],
                "URI_ID": obj.get("uri", ""),
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'Count': self._recursive_object_count_s3(obj.identity),
                'id': obj.identity,
                'ParentItem': self.id,
                'isParent': True
            }

        if node_type == 'S3File':
            try:
                layer = self.neo4j.run_query(
                    f'match((L:S3Bucket)-[:HAS*1..]->(f)) where id(f) = {obj.identity} return L.layer as layer',
                    True).pop()['layer']
            except IndexError as e:
                layer = 'Default Layer'
            json = {
                'Name': obj['name'],
                'Type': 'Delimited Data File' if obj['detectedColumns'] else 'Other',
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Last Updated': obj['lastUpdated'],
                'Tags': self.get_tags(obj.identity),
                'URI': 's3://' + str(layer) + '/' + str(obj['objectKey']),
                "URI_ID": obj.get("uri", ""),
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'Logical Name': 'No Name available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No Name available',
                'Count': self._children_count(obj.identity),
                'id': obj.identity,
                'Size': obj['size'],
                'ParentItem': self.id,
                'isParent': True,
                'Average Rating': 0 if 'average_rating' not in obj.keys() else obj['average_rating'] if obj[
                    'average_rating'] else 0,
                'Point of Contact': self.get_point_of_contact(obj['bucketName'], obj['objectKey'])
            }
        if node_type == 'S3FileColumn':
            json = {
                'Name': obj['attr_nm'],
                'Type': obj['attr_data_typ'],
                'Position': obj['col_loc'],
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'attr_desc'] if isinstance(obj['attr_desc'], str) else 'No description available',
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Tags': self.get_tags(obj.identity),
                'URI': 's3://' + str(obj['bucketName']) + '/' + str(obj['objectKey']) + '::' + str(obj['name']),
                "URI_ID": obj.get("uri", ""),
                'Logical Name': 'No description available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No description available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'id': obj.identity,
                'ParentItem': self.id,
                'isParent': False
            }
        if node_type == 'S3FileDetectedColumn':
            json = {
                'Name': obj['name'],
                'Type': obj['dataType'],
                'Position': obj['ordinalPosition'],
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Tags': self.get_tags(obj.identity),
                'URI': 's3://' + obj['bucketName'] + '/' + obj['objectKey'] + '::' + obj['name'],
                "URI_ID": obj.get("uri", ""),
                'Logical Name': 'No description available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No description available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'id': obj.identity,
                'ParentItem': self.id,
                'isParent': False
            }

        if node_type == 'ObjectColumn':
            json = {
                'Name': obj['name'],
                'Type': obj['datatype'],
                'Description': obj['obj_desc'],
                "attr_id": obj['attr_id'],
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Last Updated': 'NA',
                'Tags': self.get_tags(obj.identity),
                'URI': 'ob://' + str(obj['obj_layer_nm']) + '/' + str(obj['name']),
                "URI_ID": obj.get("uri", ""),
                'Logical Name': 'No Name available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No Name available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'Count': self._children_count(obj.identity),
                'id': obj.identity,
                'ParentItem': self.id,
                'isParent': True
            }

        return json

    def _children_count(self, id):
        return self.neo4j.run_query(
            f"match((n)-[:HAS]->(m{{deleted:False}})) where id(n) = {id} return count(m) as c", True)[0]['c']

    def _recursive_object_count_s3(self, id):
        return self.neo4j.run_query(
            f"match((n)-[:HAS*1..]->(m:S3File{{deleted:False}})) where  id(n) = {id} return count(m) as obj_count",
            True)[0]['obj_count']

    def get_point_of_contact(self, bucket, object_key):
        try:
            db = AppDb()
            file_name = object_key.split('/')[-1]
            folder = '/'.join(object_key.split('/')[:-1])

            query = "select file_inst_id as file_id, e.bucket as bucket, folder_path, file_nm, poc_nm, poc_email, poc_ph " \
                    "from system_config.file_register as f " \
                    "join " \
                    "system_config.object_connection_meta as c " \
                    "on f.obj_id = c.obj_id " \
                    "join " \
                    "system_config.connection_meta as cm " \
                    "on c.conn_id = cm.id " \
                    "join " \
                    "system_config.object_poc_meta as p " \
                    "on f.obj_id = p.obj_id " \
                    "join " \
                    "system_config.connection_meta_ext as e " \
                    "on c.conn_id = e.conn_id " \
                    "where " \
                    f"folder_path = '{folder}' and e.bucket = '{bucket}' and file_nm = '{file_name}'"

            out = db.execute(query)

            if not out.empty:
                poc = {'name': out.loc[0, 'poc_nm'], 'email': out.loc[0, 'poc_email'], 'phone': out.loc[0, 'poc_ph']}
                return poc

        except Exception as e:
            self.log.exception(e)

    def get_tags(self, node_id):
        return self.neo4j.get_child_nodes(node_id, r_type='TagLink', child_type='Tag')
