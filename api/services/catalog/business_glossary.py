#   datamax/api/services/catalog/business_glossary.py
#   Copyright (C) 2019 Axtria.
#
#   Author(s):
#   Akshay Saini <akshay.saini@axtria.com>
#   Varun Singhal <varun.singhal@axtria.com>
from datetime import datetime
import uuid
import pandas as pd
from sqlalchemy.orm import Session
import json
from services import Service
from utils.exceptions import TagAlreadyExist, TagNotFound, GlossaryNotFound, GlossaryAlreadyExist, \
    UnsupportedImportedFile, ReadOnlyTag, ReadOnlyDataAsset
from vendor.appdb import AppDb
from vendor.neo4j import Neo4j


class DeleteTag(Service):
    def __init__(self, session: Session, name, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()
        self.name = name

    def execute(self):
        existing_nodes = self.neo4j.get_nodes('Tag', name=self.name, return_objects=True)
        if len(existing_nodes) == 0:
            raise TagNotFound
        self.neo4j.delete_nodes(*existing_nodes)
        self.add_response('message', 'Tag has been deleted.')


class UpdateTag(Service):
    def __init__(self, session: Session, node_id, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()
        self.node_id = node_id
        self.params = kwargs

    def execute(self):
        existing_node = self.neo4j.get_node_by_id(self.node_id)
        self.neo4j.update_node_attributes(existing_node,
                                          modified_on=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                          user_id=self.params.get('loggedin_userid'),
                                          user_name=self.params.get('loggedin_username'),
                                          **self.params)
        self.add_response('message', 'Tag has been updated.')


class ListTags(Service):
    def __init__(self, session: Session, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()

    def execute(self):
        df = self.neo4j.get_nodes('Tag',status='true')
        self.add_response('tags', df.to_dict(orient='records'))


class LinkTag(Service):
    def __init__(self, session: Session, node_id, name, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()
        self.node_id = node_id
        self.name = name
        self.params = kwargs

    def execute(self):
        start_node = self.neo4j.get_node_by_id(self.node_id)
        try:
            end_node = self.neo4j.get_nodes('Tag', name=self.name, return_objects=True)[0]
        except IndexError as e:
            self.log.exception(e)
            raise TagNotFound

        tag_links = self.neo4j.get_relationships('TagLink', nodes=[start_node, end_node], return_objects=True)
        if len(tag_links) != 0:
            return
            # raise TagAlreadyExist

        self.neo4j.create_relationship(start_node, end_node, label='TagLink',
                                       created_on=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                       user_id=self.params.get('loggedin_userid'),
                                       user_name=self.params.get('loggedin_username'))
        self.add_response('message', 'Added %s tag for the data object' % self.name)


class UnlinkTag(Service):
    def __init__(self, session: Session, node_id, name, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()
        self.node_id = node_id # object id
        self.name = name # tag name

    def execute(self):
        start_node = self.neo4j.get_node_by_id(self.node_id)
        try:
            end_node = self.neo4j.get_nodes('Tag', name=self.name, return_objects=True)[0]
        except IndexError as e:
            self.log.exception(e)
            raise TagNotFound
        tag_links = self.neo4j.get_relationships('TagLink', nodes=[start_node, end_node], return_objects=True)
        self.neo4j.delete_relationships(*tag_links)
        self.add_response('message', 'Tag %s has been unlinked' % self.name)


class AddGlossary(Service):
    def __init__(self, session: Session, category, subcategory, name, description, status, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()
        self.category = category
        self.subcategory = subcategory
        self.description = description
        self.status = status
        self.name = name
        self.params = kwargs

    def execute(self):
        if self.neo4j.get_nodes(self.category, name=self.name, return_objects=True):
            raise GlossaryAlreadyExist

        neo4j_query = f"""merge(n:Glossary{{name:"BusinessGlossary"}}) 
                          merge(m:{self.category}{{name:"{self.name}"}})set m += {{subcategory:"{self.subcategory}", 
                                                            description:"{self.description}",
                                                            status:"{self.status.lower()}",
                                                            created_on:"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                                                            user_id:"{self.params.get('loggedin_userid')}",
                                                            creator_user_name:"{self.params.get('loggedin_username')}",
                                                            modified_on:"",
                                                            modifier_user_id:"",
                                                            modifier_user_name:""}}
                          merge((n)-[:HAS]->(m)) """
        self.neo4j.run_query(neo4j_query)
        self.add_response('message', self.category + ' created successfully.')


class ListGlossary(Service):
    def __init__(self, session: Session, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()

    def execute(self):
        query = '''match(n:Glossary)-[:HAS]->(m) return properties(m)  as x,labels(m) as y, id(m) as z'''
        self.add_response('business_glossary', list(map(lambda x: dict(**x['x'], category=x['y'][0], id=x['z']),
                                                        self.neo4j.run_query(query, True))))


class UpdateGlossary(Service):
    def __init__(self, session: Session, node_id, status, category, subcategory, name, description, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()
        self.node_id = node_id
        self.status = status
        self.category = category
        self.subcategory = subcategory
        self.description = description
        self.name = name
        self.params = kwargs

    def execute(self):
        if self.name == 'Needs_Attention':
            raise ReadOnlyTag
        glossary_check = f"""match ((m)-[:HAS]->(n)) where id(n)={self.node_id} and m.name="BusinessGlossary" return count(m) as output"""
        if self.neo4j.run_query(glossary_check, True)[0]['output']:

            #  --- special case as per requirement: unlinking only Tags when status getting false ---
            #  ---and unlinking DataAssest if not relation exists ---
            if self.status.lower() == 'false':
                self.validity_check()

            query2 = f"""MATCH(n) where id(n) = {self.node_id} SET n += {{status: '{self.status.lower()}', name: '{self.name}',
                                                            subcategory: '{self.subcategory}',
                                                            description: '{self.description}',
                                                            modified_on:"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                                                            modifier_user_id:"{self.params.get('loggedin_userid')}",
                                                            modifier_user_name:"{self.params.get('loggedin_username')}"}}"""
            self.neo4j.run_query(query2, True)
            self.add_response('tags', "Status of node id = {} changed".format(self.node_id))
        else:
            raise GlossaryNotFound

    def validity_check(self):
        if self.category == 'Tag':
            unlink_query = f"""match ((n:Tag)<-[x:TagLink]-(m)) where id(n)={self.node_id} delete x"""
            self.neo4j.run_query(unlink_query, True)
        if self.category == 'DataAsset':
            data_asset_check = f"match ((n)-[r:DataAssetLink]->(m:DataAsset)) where id(m) = {self.node_id} return count(r) as op"
            if self.neo4j.run_query(data_asset_check, True)[0]['op']:
                raise ReadOnlyDataAsset


class SearchGlossary(Service):
    def __init__(self, session, search_text=None, attribute_name=None, **kwargs):
        super().__init__()
        self.neo4j = Neo4j()
        self.filter = attribute_name
        self.contains_text = search_text
        self.session = session
        self.list_set = []

    def execute(self):
        if self.filter is not None and self.filter != '':
            for key, values in json.loads(self.filter).items():
                temp = set({})
                for value in values:
                    if key == 'category':
                        query = f"""match ((n:Glossary)-[:HAS]->(m:{value})) return id(m) as output"""
                        temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
                    elif key == 'description':
                        query = f"""match ((n:Glossary)-[:HAS]->(m)) where m.{key} =~ "(?i).*{value}.*" return id(m) as output"""
                        temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
                    else:
                        query = f"""match ((n:Glossary)-[:HAS]->(m)) where m.{key}="{value}" return id(m) as output"""
                        temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
                self.list_set.append(temp)

        if self.contains_text is not None and self.contains_text != '':
            temp = set({})
            properties_to_be_match = ['name', 'description', 'subcategory']
            for property in properties_to_be_match:
                query = f"""match ((n:Glossary)-[:HAS]->(m)) where m.{property} =~ "(?i).*{self.contains_text}.*" return id(m) as output"""
                temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
            self.list_set.append(temp)

        final_output = []
        if len(self.list_set) > 0:
            output = self.list_set[0]
            for single_set in self.list_set[1:]:
                output = output.intersection(single_set)

            for single_id in output:
                query = f"""match(n:Glossary)-[:HAS]->(m) where id(m) = {single_id} return properties(m)  as x,labels(m) as y, id(m) as z"""
                final_output.extend(list(
                    map(lambda x: dict(**x['x'], category=x['y'][0], id=x['z']), self.neo4j.run_query(query, True))))

        self.add_response('status', "Success")
        self.add_response('contents', final_output)


class ExportGlossary(Service):
    def __init__(self, session, records=100000, **kwargs):
        super().__init__()
        self.neo4j = Neo4j()
        self.limit_records = records

    def execute(self):
        query = f'''match((n:Glossary)-[:HAS]->(m)) with m,n return m.name as value, labels(m) as category,
                    m.status as status, m.subcategory as sub_category, m.description as description, 
                    m.creator_user_name as creator_user_name, m.created_on as created_on, m.modified_on as modified_on, 
                    m.modifier_user_name as modifier_user_name limit {self.limit_records}'''
        dump = self.neo4j.run_query(query)
        dump = dump.rename(columns={"category": "Category", "created_on": "Created On",
                                    "description": "Description", "modified_on": "Modified On", "status": "Status",
                                    "sub_category": "Sub Category", "creator_user_name": "Creator User Name",
                                    "value": "Value", "modifier_user_name": "Modifier User Name"})
        ordered_cols = ['Category', 'Value', 'Sub Category', 'Status',
                        'Description', 'Created On', 'Creator User Name', 'Modified On', 'Modifier User Name']
        dump = dump[ordered_cols]
        print(dump)
        dump['Category'] = dump['Category'].apply(lambda x: x[0])
        dump = dump.fillna('')
        fn = 'dumps/glossary' + str(datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')) + ".csv"
        dump.to_csv(fn, index=None)

        self.add_response('file_path', fn)
        self.add_response('file_name', fn)


class ValidateGlossary(Service):
    def __init__(self, session, file, **kwargs):
        super().__init__()
        self.neo4j = Neo4j()
        self.file = file
        self.app_db = AppDb()
        self.params = kwargs
        self.imprt = pd.DataFrame()
        self.neo_data = pd.DataFrame()
        self.valid_imprt = pd.DataFrame()
        self.allowed_columns = {"category": "Category", "created_on": "Created On",
                           "description": "Description", "modified_on": "Modified On", "status": "Status",
                           "sub_category": "Sub Category", "creator_user_name": "Creator User Name",
                           "value": "Value", "modifier_user_name": "Modifier User Name"}

        self.allowed_values = {}
        # self.allowed_values = {"Tag": ["Business Tag", "Logical Tag", "Others"], "KPI": ["Quality", "Others"],"Abbreviations": []}

    def update_old_glossary(self, matched_dataset):
        for single_data in matched_dataset['match_checker']:
            self.neo_data.loc[single_data, 'Description'] = self.valid_imprt.loc[single_data, 'Description']
            self.neo_data.loc[single_data, 'Status'] = self.valid_imprt.loc[single_data, 'Status']
        for _i, row in self.neo_data.iterrows():
            neo4j_query = f"""merge(n:Glossary{{name:"BusinessGlossary"}}) 
                                merge(m:{row['Category']}{{name:"{row['Value']}"}})
                                    set m += {{subcategory:"{row['Sub Category']}", 
                                                description:"{row['Description']}",
                                                status:"{row['Status']}",
                                                modified_on:"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                                                modifier_user_id:"{self.params.get('loggedin_userid')}",
                                                modifier_user_name:"{self.params.get('loggedin_username')}"}}
                                      merge((n)-[:HAS]->(m)) """
            self.neo4j.run_query(neo4j_query)

    def adding_new_glossary(self, unmatched_dataset):
        for index, row in unmatched_dataset.iterrows():
            neo4j_query = f"""merge(n:Glossary{{name:"BusinessGlossary"}}) 
                                merge(m:{row['Category']}{{name:"{row['Value']}"}})
                                    set m += {{subcategory:"{row['Sub Category']}", 
                                                description:"{row['Description']}",
                                                status:"{row['Status']}",
                                                created_on:"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                                                user_id:"{self.params.get('loggedin_userid')}",
                                                creator_user_name:"{self.params.get('loggedin_username')}",
                                                modified_on:"",
                                                modifier_user_id:"",
                                                modifier_user_name:""}}
                                      merge((n)-[:HAS]->(m)) """
            self.neo4j.run_query(neo4j_query)

    def strip(self,text):
        try:
            return text.strip()
        except AttributeError:
            return text

    def clean_import(self):
        self.imprt['Status'] = self.imprt['Status'].str.lower()
        self.imprt['__fail_reason__'] = ''
        self.imprt['__status__'] = 'pass'
        # -- checking validity of categories, sub categories and status; and marking status --
        self.imprt.loc[~self.imprt["Status"].isin(['true', 'false']), ['__fail_reason__', '__status__']] \
            = ["Status is not valid", "failed"]

        self.imprt.loc[~self.imprt['Category'].isin(list(self.allowed_values.keys())), ['__fail_reason__', '__status__']] \
            = ["Category is not valid", "failed"]

        semi_valid_imprt = self.imprt[self.imprt['__status__'] == 'pass']
        semi_valid_imprt = semi_valid_imprt[semi_valid_imprt['Value']!="Needs_Attention"]

        for key, value in self.allowed_values.items():
            x = semi_valid_imprt[semi_valid_imprt["Category"] == key]
            self.valid_imprt = self.valid_imprt.append(x[x['Sub Category'].isin(value)])

        index_list = list(self.imprt[~semi_valid_imprt.isin(self.valid_imprt)].dropna().index)
        self.imprt.loc[index_list, ['__fail_reason__', '__status__']] = ["Sub Category is not valid", "failed"]

    def execute(self):
        glossary_meta = self.app_db.execute("select bg_id,bg_category,bg_sub_category from system_config.business_glossary_meta")
        for name , group in glossary_meta.groupby('bg_category'):
            self.allowed_values.update({name:list(group['bg_sub_category'])})

        query = f'''match((n:Glossary)-[:HAS]->(m)) with m,n return m.name as value, labels(m) as category,
                    m.status as status, m.subcategory as sub_category, m.description as description, 
                    m.creator_user_name as creator_user_name, m.created_on as created_on, m.modified_on as modified_on, 
                    m.modifier_user_name as modifier_user_name'''
        self.neo_data = self.neo4j.run_query(query)
        self.neo_data = self.neo_data.rename(columns=self.allowed_columns)
        self.neo_data['Category'] = self.neo_data['Category'].apply(lambda x: x[0])

        try:
            self.imprt = pd.read_csv(self.file, converters={'Status': str,
                                                            'Status': self.strip,
                                                            'Category': self.strip,
                                                            'Sub Category': self.strip},skipinitialspace=True).fillna('')
        except Exception as e:
            self.log.exception(e)
            raise UnsupportedImportedFile

        # -- checking import files columns matched with required structure --
        ic=set(self.imprt.columns)  # imported column
        ac=set(self.allowed_columns.values())  # allowed column
        mc=set({'Category','Description','Status','Sub Category','Value'}) # mandatory column
        if not (ic.issubset(ac) and mc.issubset(ic)):
            raise UnsupportedImportedFile

        self.clean_import()

        self.neo_data['match_checker'] = self.neo_data["Category"] + "." + self.neo_data["Value"] + "." + \
                                         self.neo_data["Sub Category"]
        self.valid_imprt['match_checker'] = self.valid_imprt["Category"] + "." + self.valid_imprt["Value"] + "." + \
                                            self.valid_imprt["Sub Category"]

        self.neo_data.set_index(self.neo_data['match_checker'], inplace=True)
        self.valid_imprt.set_index(self.valid_imprt['match_checker'], inplace=True)

        uid = uuid.uuid1()
        # -- updating macthed nodes --
        matched_dataset = self.valid_imprt[self.valid_imprt['match_checker'].isin(self.neo_data['match_checker'].unique())]
        if not matched_dataset.empty:
            matched_dataset.to_csv('dumps/matched'+str(uid)+'.csv')

        # -- inserting not macthed nodes --
        unmatched_dataset = self.valid_imprt[~self.valid_imprt['match_checker'].isin(self.neo_data['match_checker'].unique())]
        if not unmatched_dataset.empty:
            unmatched_dataset.to_csv('dumps/unmatched'+str(uid)+'.csv')

        fn = 'glo_log' + str(datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')) + ".csv"
        self.imprt.to_csv('dumps/report_' + fn)

        self.add_response('status', 'Success')
        self.add_response('report_id', fn)
        self.add_response('file_id',uid)
        self.add_response('failed_records', len(self.imprt) - len(self.valid_imprt) - 1)
        self.add_response('total_records', len(self.imprt) - 1)
        self.add_response('success_records', len(self.valid_imprt))


class ImportGlossary(Service):
    def __init__(self, uuid, session, **kwargs):
        super().__init__()
        self.uuid = uuid
        self.neo4j = Neo4j()
        self.params = kwargs

    def update_old_glossary(self, matched_dataset):
        for single_data in matched_dataset['match_checker']:
            self.neo_data.loc[single_data, 'Description'] = self.valid_imprt.loc[single_data, 'Description']
            self.neo_data.loc[single_data, 'Status'] = self.valid_imprt.loc[single_data, 'Status']
        for _i, row in self.neo_data.iterrows():
            neo4j_query = f"""merge(n:Glossary{{name:"BusinessGlossary"}}) 
                                merge(m:{row['Category']}{{name:"{row['Value']}"}})
                                    set m += {{subcategory:"{row['Sub Category']}", 
                                                description:"{row['Description']}",
                                                status:"{row['Status']}",
                                                modified_on:"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                                                modifier_user_id:"{self.params.get('loggedin_userid')}",
                                                modifier_user_name:"{self.params.get('loggedin_username')}"}}
                                      merge((n)-[:HAS]->(m)) """
            self.neo4j.run_query(neo4j_query)

    def adding_new_glossary(self, unmatched_dataset):
        for index, row in unmatched_dataset.iterrows():
            neo4j_query = f"""merge(n:Glossary{{name:"BusinessGlossary"}}) 
                                merge(m:{row['Category']}{{name:"{row['Value']}"}})
                                    set m += {{subcategory:"{row['Sub Category']}", 
                                                description:"{row['Description']}",
                                                status:"{row['Status']}",
                                                created_on:"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                                                user_id:"{self.params.get('loggedin_userid')}",
                                                creator_user_name:"{self.params.get('loggedin_username')}",
                                                modified_on:"",
                                                modifier_user_id:"",
                                                modifier_user_name:""}}
                                      merge((n)-[:HAS]->(m)) """
            self.neo4j.run_query(neo4j_query)

    def execute(self):

        # -- updating macthed nodes --
        try:
            matched_dataset = pd.read_csv('dumps/matched'+self.uuid+'.csv')
            self.update_old_glossary(matched_dataset)
        except Exception as e:
            self.log.debug(e)

        # -- inserting not macthed nodes --
        try:
            unmatched_dataset = pd.read_csv('dumps/unmatched' + self.uuid + '.csv')
            self.adding_new_glossary(unmatched_dataset)
        except Exception as e:
            self.log.debug(e)

        self.add_response('message', "Records imported sucessfully !!")

class GetGlossaryMeta(Service):
    def __init__(self, session: Session, **kwargs):
        super().__init__()
        self.session = session
        self.app_db = AppDb()
        self.glossary_meta = ''

    def execute(self):
        self.glossary_meta = self.app_db.execute("select bg_id,bg_category,bg_sub_category from system_config.business_glossary_meta")
        lst=[]
        for name , group in self.glossary_meta.groupby('bg_category'):
            lst.append({'category':name,'sub_category':list(group['bg_sub_category'])})
        self.add_response('status', 'success')
        self.add_response('content', lst)

