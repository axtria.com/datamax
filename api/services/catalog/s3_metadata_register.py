#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - S3 metadata register"""

from io import StringIO

import numpy as np
import pandas as pd

from services import Service
from vendor.appdb import AppDb
from vendor.neo4j import Neo4j


class S3MetadataRegister(Service):
    """Register metadata of S3 object to neo4j db"""

    neo4j = Neo4j()
    app_db = AppDb()

    filter_config = app_db.execute("select layer_id, name as layer_name, description, s3_path, connector_id "
                                   "from admn.layer where layer_type='s3' and is_active")
    filter_config['bucket'] = filter_config['s3_path'].str.split('/').str.get(2)
    filter_config['type'] = 'whitelist'
    filter_config['path'] = np.nan
    filter_config['extension'] = np.nan

    def __init__(self, session, s3_connector, event_type, awsRegion, bucketName, objectKey, objectSize, lastUpdated,
                 connector_id, bypass_filter=False, **kwargs):
        super().__init__()
        self.s3 = s3_connector

        self.event_type = event_type
        self.objectSize = objectSize

        self.key_properties = {}
        self.key_properties['awsRegion'] = awsRegion
        self.key_properties['bucketName'] = bucketName
        self.key_properties['objectKey'] = objectKey
        self.key_properties['connector_id'] = connector_id

        self.info = {}
        self.node_type = 'folder' if objectKey.endswith('/') else 'file'
        self.info['name'] = objectKey.replace('\\','/').replace('//','/').split('/')[-1]
        self.info['lastUpdated'] = lastUpdated

        self.bypass_filter = bypass_filter

        self.file_label = 'S3File'
        self.folder_label = 'S3Folder'
        self.bucket_label = 'S3Bucket'
        self.column_label = 'S3FileDetectedColumn'
        self.relationship_label = 'HAS'

        self.n_records_dtype_detection = 10
        self.node = None

        try:
            self.layer_name = str(S3MetadataRegister.filter_config.loc[
                                      S3MetadataRegister.filter_config['bucket'] == bucketName, 'layer_name'].iat[0])
            self.layer_id = str(S3MetadataRegister.filter_config.loc[
                                      S3MetadataRegister.filter_config['bucket'] == bucketName, 'layer_id'].iat[0])
        except:
            self.layer_name = bucketName
            self.layer_id = 0

    def execute(self):

        try:
            if not self.s3_filter(self.key_properties['bucketName'], self.key_properties['objectKey']):
                if not self.bypass_filter:
                    self.add_response('success', True)
                    self.add_response('message', 'Event not eligible for catalog registration')
                    return

            if self.node_type == 'file':

                self.node = S3MetadataRegister.neo4j.create_node(self.file_label, **self.key_properties)

                if self.event_type.startswith('ObjectCreated') or self.event_type == 'FullScan':
                    self.info['detectedColumns'] = self.detect_colums()
                    self.info['size'] = self.objectSize
                    self.info['deleted'] = False

                if self.event_type.startswith('ObjectRemoved'):
                    self.info['deleted'] = True

                S3MetadataRegister.neo4j.update_node_attributes(self.node, **self.info)
                self.create_parent_directories()

                self.add_response('success', True)

        except Exception as e:
            self.add_response('success', False)
            self.add_response('message', str(e))
            self.log.exception(e)

    def create_parent_directories(self):

        bucket = S3MetadataRegister.neo4j.create_node(self.bucket_label,
                                                      bucketName=self.key_properties['bucketName'],
                                                      awsRegion=self.key_properties['awsRegion'],
                                                      deleted=False
                                                      )
        S3MetadataRegister.neo4j.update_node_attributes(bucket, lastUpdated=self.info['lastUpdated'],
                                                        layer=self.layer_name, layer_id=self.layer_id)

        folders = self.key_properties['objectKey'].split('/')[:-1]

        last_node = bucket
        prev_folder_path = ''
        for folder in folders:
            curr_folder_path = prev_folder_path + folder + '/'
            curr_node = S3MetadataRegister.neo4j.create_node(self.folder_label,
                                                             name=folder,
                                                             folderPath=curr_folder_path,
                                                             awsRegion=self.key_properties['awsRegion'],
                                                             bucketName=self.key_properties['bucketName'],
                                                             deleted=False
                                                             )
            prev_folder_path = curr_folder_path
            S3MetadataRegister.neo4j.update_node_attributes(curr_node, lastUpdated=self.info['lastUpdated'])
            S3MetadataRegister.neo4j.create_relationship(last_node, curr_node, self.relationship_label)
            last_node = curr_node

        S3MetadataRegister.neo4j.create_relationship(last_node, self.node, self.relationship_label)

    def detect_colums(self):
        try:
            file_key = self.key_properties['bucketName'] + '/' + self.key_properties['objectKey']
            data = self.s3.read(file_key, self.n_records_dtype_detection)
            delimiter = self.get_delimiter(data[0])
            data = pd.read_csv(StringIO(''.join(data)), sep=delimiter)

            dt = data.dtypes
            dt = dt.replace('int32', 'INT')
            dt = dt.replace('int64', 'INT')
            dt = dt.replace('float32', 'FLOAT')
            dt = dt.replace('float64', 'FLOAT')
            dt = dt.replace('object', 'STRING')
            dt = dt.replace('bool', 'BOOL')

            for i, (col, dtype) in enumerate(zip(data.columns, dt)):
                col_node = S3MetadataRegister.neo4j.create_node(self.column_label,
                                                                name=col, dataType=dtype,
                                                                objectKey=self.key_properties['objectKey'],
                                                                ordinalPosition=i + 1,
                                                                deleted=False,
                                                                bucketName=self.key_properties['bucketName'])
                S3MetadataRegister.neo4j.create_relationship(self.node, col_node, self.relationship_label)
            return True

        except Exception as e:
            self.log.exception(e)
            return False

    def s3_filter(self, bucket, object_key):
        """Decides whether a s3 file should be monitored by neo4j catalog or not"""
        status = False

        whitelist = S3MetadataRegister.filter_config[(S3MetadataRegister.filter_config['type'] == 'whitelist') & (
                S3MetadataRegister.filter_config['bucket'] == bucket)]
        if whitelist.empty:
            return False

        for _, config in whitelist.iterrows():

            valid_path, valid_extension = False, False

            if not self.is_unrestricted(config['path']):
                if object_key.startswith(config['path']):
                    valid_path = True
            else:
                valid_path = True

            if not self.is_unrestricted(config['extension']):
                if object_key.endswith(config['extension']):
                    valid_extension = True
            else:
                valid_extension = True

            if valid_path and valid_extension:
                status = True
                break

        if not status:
            return False

        blacklist = S3MetadataRegister.filter_config[(S3MetadataRegister.filter_config['type'] == 'blacklist') & (
                S3MetadataRegister.filter_config['bucket'] == bucket)]
        if blacklist.empty:
            return True

        for _, config in blacklist.iterrows():

            invalid_path, invalid_extension = False, False

            if not self.is_unrestricted(config['path']):
                if object_key.startswith(config['path']):
                    invalid_path = True
            else:
                invalid_path = True

            if not self.is_unrestricted(config['extension']):
                if object_key.endswith(config['extension']):
                    invalid_extension = True
            else:
                invalid_extension = True

            if invalid_path and invalid_extension:
                return False

        return True

    def is_unrestricted(self, val):
        """Calculates whether `val` is np.nan or not. `val` can be object of any class"""
        if isinstance(val, float):
            if np.isnan(val):
                return True
        return False

    def get_delimiter(self, header_str):
        """Infer delimiter on the basis of header string"""
        delimiters = [',', '|', ';', ':', '\t']
        detected_delimiter = None
        max_count = 0
        for delimiter in delimiters:
            delimiter_count = header_str.count(delimiter)
            if 0 <= max_count < delimiter_count:
                detected_delimiter = delimiter
                max_count = delimiter_count
        return detected_delimiter
