#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
'''Datamax Catalog - Display list view'''

from services import Service
from vendor.neo4j import Neo4j
from models import Connector

class DisplayListBRMS(Service):
	'''Gets a list view of catalog'''
	neo4j = Neo4j()

	def __init__(self, session, connector_id, **kwargs):
		super().__init__()

		self.session = session
		self.connector_id = connector_id

		
	def execute(self):

		tables = []

		db = self.session.query(Connector).filter(Connector.id == int(self.connector_id)).first().connection['DBNAME']

		for _,s in DisplayListBRMS.neo4j.get_nodes('RedshiftSchema', deleted=False, database=db).iterrows():

			for _,t in DisplayListBRMS.neo4j.get_nodes('RedshiftTable', schema = s['name'], deleted=False, database=db).iterrows():

				table = {
					'name': t['name'],
					'schema': s['name'],
					'layer':s['name'],
					'description': '' if 'remarks' not in t.keys() else t['remarks'] if isinstance(t['remarks'],str) else ''
				}

				columns =[]

				for _,c in DisplayListBRMS.neo4j.get_nodes('RedshiftColumn', schema = s['name'], table = t['name'], deleted=False).iterrows():

					column = {
						'column_name': c['name'],
						'data_type': c['datatype']
						}
					columns.append(column)

				table['adaptor_meta'] = columns

				tables.append(table)

		self.add_response('contents', tables)
		self.add_response('status','success')