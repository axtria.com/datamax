table_metadata =  False

column_metadata = True

statistics = False
# statistics = {
#     "sum": False,
#     "min": True,
#     "max": True,
#     "avg": True,
#     "stddev": True,
#     "null_count": True,
#     "unique_count": True,
#     "percentiles": [25,50,75]
# }

sample_size = 10

histograms = False

histogram_bins = 10

custom_profiles = False#[

#     {
#         'title':'Top 10 Physicians',
#         'query':'select state_code as state, name, sum(sales) as total_sales from public.test_data_workflow group by state, name order by total_sales desc limit 10'
#     },

#     {
#         'title':'Sales per state',
#         'query':'select state_code as state, count(state_code) as record_count, sum(sales) as total_sales, sum(pde) as total_pde from test_data_workflow group by state_code order by state_code'
#     },

# ]



