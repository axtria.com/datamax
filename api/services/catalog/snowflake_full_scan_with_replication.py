#   datamax/api/vendor/snowflake_full_scan.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
'''Datamax Catalog - Snowflake full scan'''

from datetime import datetime

import pandas as pd
from numpy import random

from services import Service
from utils.helpers import thread
from vendor.appdb import AppDb
from vendor.neo4j import Neo4j
from vendor.snowflake import Snowflake

neo4j = Neo4j()
app_db = AppDb()


class SnowflakeFullScanWithReplication(Service):
    def __init__(self, session, sf_connector_id, replication_factor=10, **kwargs):
        """Register metadata of Snowflake to neo4j db"""
        super().__init__()

        self.session = session
        self.sf_connector_id = int(sf_connector_id)
        self.sf = Snowflake.initialize(self.sf_connector_id, session)
        self.over_the_network = True
        self.replication_factor = int(replication_factor)
        self.add_response('message', 'Request Acknowledged')

    @thread
    def execute(self):

        filter_config = app_db.execute("select data_lyr_id as layer_id, data_lyr_name as layer_name, description, schema_name, conn_id as connector_id "
                                       "from system_config.data_layer_meta where lyr_type='sf' and is_active")

        schemas = filter_config.loc[filter_config['connector_id'] == self.sf_connector_id, 'schema_name'].tolist()

        schemas_tuple = "('" + "','".join(schemas) + "')"

        sf_query = 'select ' \
                   't.table_schema as "table_schema", ' \
                   't.table_name as "table_name", ' \
                   't.table_type as "table_type", ' \
                   't.comment as "table_remarks", ' \
                   'c.column_name as "column_name", ' \
                   'c.data_type as "column_datatype", ' \
                   'c.ordinal_position as "column_ordinal_position", ' \
                   'c.comment as "column_remarks" ' \
                   'from ' \
                   f'"{self.sf.database}"."INFORMATION_SCHEMA"."TABLES" t join "{self.sf.database}"."INFORMATION_SCHEMA"."COLUMNS" c ' \
                   'on ' \
                   '(t.table_name = c.table_name and t.table_schema = c.table_schema) ' \
                   f'where t.table_schema in {schemas_tuple};'

        metadata = self.sf.execute(sf_query)
        metadata.fillna('No description available', inplace=True)
        metadata = pd.merge(metadata, filter_config, how='left', left_on='table_schema', right_on='schema_name')

        database = self.sf.database
        account_id = self.sf.account_id

        if self.over_the_network:
            for i in range(self.replication_factor):
                for _, row in metadata.iterrows():
                    neo4j_query = f'merge(a:SnowflakeAccount{{name:"{account_id}"}}) set a +=  {{deleted:false}} ' \
                                  f'merge(sf:Snowflake{{database:"{database}", account:"{account_id}"}}) set sf +=  {{deleted:false, connector_id: "{self.sf_connector_id}"}} ' \
                                  f'merge(s:SnowflakeSchema{{name:"{row.table_schema}", database:"{database}", account:"{account_id}"}}) set s +=  {{deleted:false, layer:"{row.layer_name}", remarks:"{row.description}", layer_id: "{row.layer_id}"}} ' \
                                  f'merge(t:SnowflakeTable{{name:"{row.table_name}___{i}", schema:"{row.table_schema}", type:"{row.table_type}", database:"{database}", account:"{account_id}"}}) set t +=  {{deleted:false, remarks:"{row.table_remarks}", layer_id: "{row.layer_id}", connector_id: "{self.sf_connector_id}"}} ' \
                                  f'merge(c:SnowflakeColumn{{name:"{row.column_name}", table:"{row.table_name}___{i}", schema:"{row.table_schema}", database:"{database}", account:"{account_id}"}}) set c +=  {{deleted:false, remarks:"{row.column_remarks}", ordinal_position:{row.column_ordinal_position}, datatype:"{row.column_datatype}"}} ' \
                                  'merge((a)-[:HAS]->(sf)) ' \
                                  'merge((sf)-[:HAS]->(s)) ' \
                                  'merge((s)-[:HAS]->(t)) ' \
                                  'merge((t)-[:HAS]->(c)) '

                    _ = neo4j.run_query(neo4j_query)

        else:

            fname = f'tmp_{self.uid()}.csv'
            fpath = f'{neo4j.get_import_dir()}{fname}'

            metadata.to_csv(fpath, index=None)

            neo4j_query = f'load csv with headers from "file:///{fname}" as row ' \
                          f'merge(a:SnowflakeAccount{{name:"{account_id}"}}) set a +=  {{deleted:false}} ' \
                          f'merge(sf:Snowflake{{database:"{database}", account:"{account_id}"}}) set sf +=  {{deleted:false}} ' \
                          f'merge(s:SnowflakeSchema{{name:row.table_schema, database:"{database}", account:"{account_id}"}}) set s +=  {{deleted:false, layer:row.layer_name, remarks:row.description}} ' \
                          f'merge(t:SnowflakeTable{{name:row.table_name, schema:row.table_schema, type:row.table_type, database:"{database}", account:"{account_id}"}}) set t +=  {{deleted:false, remarks:row.table_remarks}} ' \
                          f'merge(c:SnowflakeColumn{{name:row.column_name, table:row.table_name, schema:row.table_schema, database:"{database}", account:"{account_id}"}}) set c +=  {{deleted:false, remarks:row.column_remarks, ordinal_position:row.column_ordinal_position, datatype:row.column_datatype}} ' \
                          'merge((a)-[:HAS]->(sf)) ' \
                          'merge((sf)-[:HAS]->(s)) ' \
                          'merge((s)-[:HAS]->(t)) ' \
                          'merge((t)-[:HAS]->(c)) '

            _ = neo4j.run_query(neo4j_query)

        self.add_response('status', 'success')

    def uid(self):
        return hex(random.randint(10 ** 6)) + datetime.now().strftime('___%d_%b_%Y__%H_%M_%S_%f')
