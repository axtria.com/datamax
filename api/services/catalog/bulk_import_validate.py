#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Bulk Import Validate"""

from services import Service
from vendor.neo4j import Neo4j 
from datetime import datetime
import pandas as pd


class BulkImportValidate(Service):
	"""Bulk Import Validate

<!DOCTYPE html>
<html>
<head>
	<title>test</title>
</head>
<body>
<form action="http://HOST/dmx_dev/api/catalog/validate_import_catalog" method="POST", enctype="multipart/form-data">
	<input type="file" name="file">
	<input type="submit" name="">
</form>
</body>
</html>

	"""
	def __init__(self, session, file, user_id=-1, user_name='', **kwargs):
		super().__init__()

		self.neo4j = Neo4j()
		self.file = file
		self.user_id = int(user_id)
		self.user_name = user_name

	def execute(self):

		ts = datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')
		file_id = ts + '__' + self.file.filename

		dname = 'dumps/'
		fname = file_id
		path = dname + fname

		self.file.save(path)

		df = pd.read_csv(path).fillna('')
		df['__status__'] = ''
		df['__fail_reason__'] = ''

		tag_library = self.neo4j.get_nodes('Tag')

		total_records = len(df)

		ids = '[' + ', '.join(df.Id.astype(int).astype(str).tolist()) + ']'

		query = f"match(n) where id(n) in {ids} return id(n) as node_id"
		node_ids = self.neo4j.run_query(query)
		node_ids = node_ids['node_id']

		self.log.debug(f'total records in file: {total_records} | total records matched : {len(node_ids)}')

		df.loc[df['Id'].isin(node_ids),'__status__'] = 'success'

		df.loc[~df['Id'].isin(node_ids),'__status__'] = 'fail'
		df.loc[~df['Id'].isin(node_ids),'__fail_reason__'] = 'Node not found in neo4j'

		success_records = len(node_ids)
		failure_records = total_records - success_records

		dname = 'dumps/'
		fname = 'report_' + file_id
		path = dname + fname

		df.to_csv(path, index=None)

		self.add_response('status','success')
		self.add_response('total_records',total_records)
		self.add_response('success_records',success_records)
		self.add_response('failure_records',failure_records)
		self.add_response('report_id',file_id)