#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Bulk Import"""

from services import Service
from vendor.neo4j import Neo4j 
from datetime import datetime
import pandas as pd


class BulkImport(Service):
	"""Bulk Import catalog"""
	def __init__(self, session, report_id, user_id, user_name, **kwargs):
		super().__init__()

		self.neo4j = Neo4j()
		self.file_id = report_id
		self.user_id = int(user_id)
		self.user_name = user_name

	def execute(self):

		ts = datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')

		dname = 'dumps/'
		fname = self.file_id
		path = dname + fname

		df = pd.read_csv(path).fillna('')
		df['__status__'] = ''
		df['__fail_reason__'] = ''

		tag_library = self.neo4j.get_nodes('Tag')

		total_records = len(df)
		success_records = 0
		failure_records = 0

		for i, row in df.iterrows():
			try:
				query = f"""match(n) where id(n) = toInteger({row.Id}) 
							set n += {{
								tech_desc:"{row.Technical_Description}",
								buss_desc:"{row.Business_Description}",
								logi_desc:"{row.Logical_Description}",
								logi_name:"{row.Logical_Name}"
							}} return count(n) as found"""
				response = self.neo4j.run_query(query, True)

				if response[0]['found'] == 0:
					raise Exception('Node not found in neo4j')

				tags = [t.strip() for t in row.Tags.split(';')]
				tag_ids = tag_library.loc[tag_library['name'].isin(tags),'__id'].tolist()
				if tag_ids:
					query = f"""match(n) where id(n) = toInteger({row.Id}) 
								match(tag:Tag) where id(tag) in {tag_ids} 
								merge((n)-[r:TagLink{{user_id:{self.user_id}}}]->(tag)) 
								set r += {{user_name:"{self.user_name}", created_on:"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}"}}"""
					self.neo4j.run_query(query, True)
				success_records += 1
				df.loc[i,'__status__'] = 'success'
			except Exception as e:
				self.log.debug(f'Some error occured in row {i}')
				self.log.debug(e)
				failure_records += 1
				df.loc[i,'__status__'] = 'fail'
				df.loc[i,'__fail_reason__'] = str(e)

		dname = 'dumps/'
		report_id = ts + '__' + self.file_id
		fname = 'report_' + report_id
		path = dname + fname

		df.to_csv(path, index=None)

		self.add_response('status','success')
		self.add_response('total_records',total_records)
		self.add_response('success_records',success_records)
		self.add_response('failure_records',failure_records)
		self.add_response('report_id',report_id)






