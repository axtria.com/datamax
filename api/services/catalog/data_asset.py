from datetime import datetime
from sqlalchemy.orm import Session
from services import Service
from utils.exceptions import NoDataAvailable
from vendor.neo4j import Neo4j


class ListDataAsset(Service):
    def __init__(self, session: Session, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()

    def execute(self):
        df = self.neo4j.get_nodes('DataAsset', status='true')
        self.add_response('Data Asset', df.to_dict(orient='records'))


class LinkDataAsset(Service):
    def __init__(self, session: Session, file_node, data_asset_node, **kwargs):
        super().__init__()
        self.session = session
        self.neo4j = Neo4j()
        self.file_node = file_node
        self.data_asset_node = data_asset_node
        self.params = kwargs

    def execute(self):
        if not self.neo4j.check_node(self.file_node):
            raise NoDataAvailable
        if not self.neo4j.check_node(self.data_asset_node):
            raise NoDataAvailable
        # n1 is file node n2 is data asset node
        neo4j_query = f"""match(n1) where id(n1) = {self.file_node} with n1
                        match(n2) where id(n2) = {self.data_asset_node} with n1,n2
                        optional match (n1)-[l:DataAssetLink]->() delete l
                        merge((n1)-[r:DataAssetLink{{created_on:"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                                                user_id:{self.params.get('loggedin_userid')},
                                                user_name:"{self.params.get('loggedin_username')}"}}]->(n2)) return n1,n2"""
        self.neo4j.run_query(neo4j_query)
        self.add_response('message', 'Added DataAsset for the data object')


class GetDataAsset(Service):
    def __init__(self, session: Session, **kwargs):
        super().__init__()
        self.neo4j = Neo4j()

    def execute(self):
        query = '''match ((n:DataAsset)<-[:DataAssetLink]-(ob)) with n,ob
                match ((ob)-[:HAS]->(col)) return 
                n.name as name , n.description as description,
                ob.name as ob_name, ob.layer as layer_name,ob.uri as uri,
                col.name as col_name, col.desc as col_desc,col.buss_desc as buss_desc,col.datatype as datatype, col.logi_name as logi_name'''
        df = self.neo4j.run_query(query)
        output = []
        if df.empty:
            self.log.debug("No DataAsset Linked to object")
            self.add_response('status', 'failed')
            self.add_response('content', output)
            return
        df['unique_str'] = df['name'] + df['layer_name']
        for cat in df['unique_str'].unique():
            df1 = df.loc[df['unique_str'] == cat]
            detail_data_asset = df1[['col_name', 'col_desc', 'ob_name', 'datatype', 'logi_name', 'buss_desc', 'uri']].to_dict(orient='records')
            output.append({'dataasset': df1['name'].unique()[0],
                           'description': df1['description'].unique()[0],
                           'layer': df1['layer_name'].unique()[0],
                           'columns': detail_data_asset})
        self.add_response('status', 'success')
        self.add_response('content', output)

