#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Calalog Search Log"""

from services import Service
from vendor.appdb import AppDb 
from datetime import datetime

class SaveCatalogSearchQueryV2(Service):
	"""Saves Catalog search log"""
	def __init__(self, session, srch_criteria_id=None, srch_name=None, srch_text=None, srch_params=None, eff_srt_dt=None, eff_end_dt=None,
						is_actv=None, audit_batch_id=None, audit_insrt_id=None, audit_updt_id=None, **kwargs):
		super().__init__()

		self.srch_criteria_id = srch_criteria_id
		self.srch_name = srch_name
		self.srch_text = srch_text
		self.srch_params = srch_params
		self.eff_srt_dt = eff_srt_dt
		self.eff_end_dt = eff_end_dt
		self.is_actv = is_actv
		self.audit_batch_id = audit_batch_id
		self.audit_insrt_dt = 'NOW()'
		self.audit_insrt_id = audit_insrt_id
		self.audit_updt_dt = 'NOW()'
		self.audit_updt_id = audit_updt_id

		self.pg_table = 'system_config.catl_srch_criteria_meta'
		self.db = AppDb()

	def execute(self):

		if self.is_actv:
			assert self.is_actv.lower() in ['true','false'], 'Non boolean value for `is_actv`'
			self.is_actv = True if self.is_actv.lower() == 'true' else False 

		if self.srch_criteria_id is None:
			self._insert()
		else: 
			self._update()

		self.add_response('status','success')


	def _insert(self):

		assert self.srch_name is not None, '`srch_name` can not be empty for new query save'
		assert self.audit_insrt_id is not None, '`audit_insrt_id` can not be empty for new query save'
		self.audit_insrt_id = int(self.audit_insrt_id)
		self.srch_criteria_id = self._get_new_id()

		self.audit_updt_id = self.audit_insrt_id
		self.audit_updt_dt = self.audit_insrt_dt

		pg_columns = ['srch_criteria_id', 'srch_name', 'srch_text', 'srch_params', 'eff_srt_dt', 'eff_end_dt', 'is_actv',
						'audit_batch_id', 'audit_insrt_dt', 'audit_insrt_id', 'audit_updt_id', 'audit_updt_dt']

		query = self._prepare_insert_query(pg_columns)

		self.db.execute(query)
		self.add_response('query', query)


	def _update(self):

		assert self.audit_updt_id is not None, '`audit_updt_id` can not be empty for query update'
		self.srch_criteria_id = int(self.srch_criteria_id)
		self.audit_updt_id = int(self.audit_updt_id)

		pg_columns = ['srch_name', 'srch_text', 'srch_params', 'eff_srt_dt', 'eff_end_dt', 'is_actv',
						'audit_batch_id', 'audit_updt_dt', 'audit_updt_id']

		query = self._prepare_update_query(pg_columns)

		self.db.execute(query)
		self.add_response('query', query)


	def _get_new_id(self):

		response = self.db.execute(f'SELECT MAX(srch_criteria_id)+1 AS next_id FROM {self.pg_table}')
		next_id = response.at[0, 'next_id']
		if not next_id:
			next_id = 1

		return int(next_id)


	def _prepare_insert_query(self, column_list):

		column_names = []
		column_values = []

		for column in column_list:

			value = getattr(self, column)

			if value is not None:

				column_names.append(column)
				column_values.append(value)

		names_tuple = repr(tuple(column_names)).replace("'","")
		values_tuple = repr(tuple(column_values)).replace("'NOW()'","NOW()")

		query = f'INSERT INTO {self.pg_table} {names_tuple} VALUES {values_tuple}'

		return query


	def _prepare_update_query(self, column_list):

		name_value_equations = []

		for column in column_list:

			value = getattr(self, column)

			if value is not None:

				name_value_equations.append(f'{column} = {repr(value)}')

		name_value_equations = ', '.join(name_value_equations)
		name_value_equations = name_value_equations.replace("'NOW()'","NOW()")

		query = f'UPDATE {self.pg_table} SET {name_value_equations} WHERE srch_criteria_id = {self.srch_criteria_id}'

		return query


class GetCatalogSearchQueryV2(Service):
	"""Gets Catalog search log"""
	def __init__(self, session, srch_criteria_id=None, **kwargs):
		super().__init__()

		self.srch_criteria_id = srch_criteria_id
		self.pg_table = 'system_config.catl_srch_criteria_meta'


	def execute(self):

		if self.srch_criteria_id is None:
			query = f'SELECT * FROM {self.pg_table}'
		else:
			query = f'SELECT * FROM {self.pg_table} WHERE srch_criteria_id in ({self.srch_criteria_id})'

		output = AppDb().execute(query).to_dict(orient='records')

		self.add_response('contents', output)
		self.add_response('status', 'success')


class SaveCatalogSearchQuery(Service):
	"""Saves Catalog search log"""
	def __init__(self, session, search_text, user_id, name, connector_type=None, layer_name=None, last_updated_with_in=None,
					compare_operator=None, file_size_num=None, size_dimension=None, **kwargs):
		super().__init__()

		self.search_text = search_text
		self.created_by = int(user_id)
		self.name = name

		self.connector_type = connector_type
		self.layer_name = layer_name
		self.last_updated_with_in = last_updated_with_in
		self.compare_operator = compare_operator
		self.file_size_num = int(file_size_num) if file_size_num else file_size_num
		self.size_dimension = size_dimension

		self.created_date = 'NOW()' # for calling internal pg function


	def execute(self):

		pg_columns = ['search_text', 'created_by', 'name', 'connector_type', 'layer_name', 'last_updated_with_in', 'compare_operator', 'file_size_num', 'size_dimension', 'created_date']

		query = self._make_query(pg_columns)

		AppDb().execute(query)

		self.add_response('query', query)
		self.add_response('status','success')


	def _make_query(self, column_list):

		column_names = []
		column_values = []

		for column in column_list:

			value = getattr(self, column)

			if value is not None:

				column_names.append(column)
				column_values.append(value)

		column_names = repr(tuple(column_names)).replace("'","")
		column_values = repr(tuple(column_values)).replace("'NOW()')","NOW())")


		query = f'INSERT INTO catalog.catalog_search_log {column_names} VALUES {column_values}'

		return query


class GetCatalogSearchQuery(Service):
	"""Gets Catalog search log"""
	def __init__(self, session, user_id, **kwargs):
		super().__init__()

		self.user_id = user_id


	def execute(self):

		query = f'SELECT * FROM catalog.catalog_search_log WHERE created_by = {self.user_id}'

		output = AppDb().execute(query)

		self.add_response('contents', output.to_dict(orient='records'))
		self.add_response('status', 'success')

