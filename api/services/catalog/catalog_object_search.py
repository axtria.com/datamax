#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
#   Author: Akshay Saini <akshay.saini@axtria.com>


from datetime import datetime as dt
from dateutil import parser
import pandas as pd
import json
from services import Service
from utils.querybuilder import suggestion
from vendor.neo4j import Neo4j
import re


class CatalogObjectSearch(Service):
    neo4j = Neo4j()

    def __init__(self, session, search_string, search_on=None, max_results=100, layer_type=None, layer_name=None,
                 file_size=None, timestamp=None, **kwargs):
        super().__init__()

        self.session = session
        self.search_string = search_string
        self.search_on = search_on  # belongs to {None, 'tags','remarks'}
        self.max_results = int(max_results)

        self.layer_type = layer_type
        self.layer_name = layer_name
        self.file_size = file_size
        self.timestamp = timestamp

        self.final_result = []

        self.log.debug(self.__dict__)

    def execute(self):

        obj_index = 'objects_' + self.search_on if self.search_on else 'objects'
        col_index = 'columns_' + self.search_on if self.search_on else 'columns'
        tag_index = 'tag_search'

        obj_result = []
        col_result = []
        tag_result = []

        if self.search_string.isnumeric():
            obj_result += CatalogObjectSearch.neo4j.run_query(
                f'match(node) where (node:SnowflakeTable or node:S3File or node:Object) and id(node) = {self.search_string} return node, 1.0 as score',
                True)
            col_result += CatalogObjectSearch.neo4j.run_query(
                f'match((node)-[]->(col)) where (node:SnowflakeTable or node:S3File) and id(col) = {self.search_string} return node, 1.0 as score',
                True)

        obj_result += CatalogObjectSearch.neo4j.run_query(
            f'CALL db.index.fulltext.queryNodes("{obj_index}","{self.search_string}") yield node, score return node, score',
            True)
        col_result += CatalogObjectSearch.neo4j.run_query(
            f'CALL db.index.fulltext.queryNodes("{col_index}","{self.search_string}") yield node, score with node,score match (n)-[]->(node) return n as node, score',
            True)
        tag_result += CatalogObjectSearch.neo4j.run_query(
            f'CALL db.index.fulltext.queryNodes("{tag_index}","{self.search_string}") yield node, score with node,score match (n)-[]->(node) return n as node, score',
            True)

        obj_result = [self.prepare_objects(r['node'], r['score']) for r in obj_result+tag_result]
        col_result = [self.prepare_objects(r['node'], r['score']) for r in col_result]
        obj_ids = {i['id'] for i in obj_result}
        col_ids = {i['id'] for i in col_result}

        common_ids = obj_ids & col_ids

        for r in obj_result:

            if r['id'] in common_ids:
                r['matched_to'] = 'Object and column'
                for rc in col_result:
                    if rc['id'] == r['id']:
                        r['search_score'] = max(r['search_score'], rc['search_score'])
            else:
                r['matched_to'] = 'Object'

            self.final_result.append(r)

        self.final_result.sort(key=lambda x: x['search_score'], reverse=True)

        final_col_result = []
        for rc in col_result:
            if rc['id'] in common_ids:
                continue
            else:
                rc['matched_to'] = 'Column'
            final_col_result.append(rc)

        if final_col_result:
            final_col_result = pd.DataFrame(final_col_result)
            key_cols = list(set(final_col_result.columns) - {'search_score', 'Tags', "Size"})
            final_col_result = final_col_result.groupby(key_cols).max().reset_index().sort_values('search_score',
                                                                                                  ascending=False).fillna(
                0).to_dict(orient='records')

        self.final_result += final_col_result

        self.filter_layer()
        self.filter_file_size()
        self.filter_file_timestamp()

        self.add_response('contents', self.final_result[:self.max_results])
        self.add_response('status', 'success')

    def filter_layer(self):

        if self.layer_type:

            if self.layer_type == 'Redshift Schema':
                prefix = 'rs://'
            elif self.layer_type == 'S3 Bucket':
                prefix = 's3://'
            else:
                prefix = ''

            if self.layer_name:
                prefix += self.layer_name

            self.final_result = [r for r in self.final_result if r['URI'].startswith(prefix)]

    def filter_file_size(self):

        if self.layer_type == 'S3 Bucket':

            if self.file_size:

                if self.file_size[0] == 'G':
                    self.final_result = [r for r in self.final_result if r['Size'] >= int(self.file_size[1:])]
                elif self.file_size[0] == 'L':
                    self.final_result = [r for r in self.final_result if r['Size'] <= int(self.file_size[1:])]

    def filter_file_timestamp(self):

        if self.layer_type == 'S3 Bucket':

            if self.timestamp:
                self.timestamp = int(self.timestamp)

                dt_now = dt.now()

                self.final_result = [r for r in self.final_result if (
                        dt_now - dt.strptime(r['Last Updated'], '%Y-%m-%d %H:%M:%S')).days <= self.timestamp]

    def prepare_objects(self, obj, score):

        node_type = str(obj.labels)[1:]

        json = None
        if node_type == 'Object':
            json = {
                'Name': obj['name'],
                'Type': obj['obj_typ'],
                'Description': obj['obj_desc'],
                'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                  str) else '',
                'Last Updated': 'NA',
                'Tags': self.get_tags(obj.identity),
                'URI': 'ob://' + str(obj['obj_layer_nm']) + '/' + str(obj['name']),
                'Logical Name': 'No Name available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No Name available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'Count': self._children_count(obj.identity),
                'Size': obj['size'],
                'search_score': score,
                'id': obj.identity,
                'Layer Type': 'Object'
            }

        if node_type == 'SnowflakeTable':
            json = {
                'Name': obj['name'],
                'Type': obj['type'],
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'remarks' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                 str) else '',
                'Last Updated': 'NA',
                'Tags': self.get_tags(obj.identity),
                'URI': 'sf://' + str(obj['schema']) + '/' + str(obj['name']),
                'Logical Name': 'No Name available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No Name available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'Count': self._children_count(obj.identity),
                'Size': obj['size'],
                'id': obj.identity,
                'search_score': score,
                'Layer Type': 'Snowflake Schema'
            }

        if node_type == 'S3File':
            json = {
                'Name': obj['name'],
                'Type': 'Delimited Data File' if obj['detectedColumns'] else 'Other',
                'Description': 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available',
                'Comments': '' if 'remarks' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                                 str) else '',
                'Last Updated': obj['lastUpdated'],
                'Tags': self.get_tags(obj.identity),
                'URI': 's3://' + str(obj['bucketName']) + '/' + str(obj['objectKey']),
                'Logical Name': 'No Name available' if 'logi_name' not in obj.keys() else obj[
                    'logi_name'] if isinstance(obj['logi_name'], str) else 'No Name available',
                'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                    'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
                'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                    'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
                'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                    'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
                'Count': self._children_count(obj.identity),
                'Size': obj['size'],
                'id': obj.identity,
                'search_score': score,
                'Layer Type': 'S3 Bucket'
            }

        return json

    def _children_count(self, id):
        return CatalogObjectSearch.neo4j.run_query(
            f"match((n)-[:HAS]->(m{{deleted:False}})) where id(n) = {id} return count(m) as c", True)[0]['c']

    def _recursive_object_count_s3(self, id):
        return CatalogObjectSearch.neo4j.run_query(
            f"match((n)-[:HAS*1..]->(m:S3File{{deleted:False}})) where  id(n) = {id} return count(m) as obj_count",
            True)[0]['obj_count']

    def get_tags(self, node_id):
        # return ', '.join(map(lambda x: x['name'],
        #                      self.neo4j.get_child_nodes(node_id, r_type='TagLink', child_type='Tag')))
        return list(self.neo4j.get_child_nodes(node_id, r_type='TagLink', child_type='Tag'))


class search_by_attribute(Service):
    def __init__(self, session, search_text=None, attribute_name=None, **kwargs):
        super().__init__()
        self.neo4j = Neo4j()
        self.filter = attribute_name
        self.contains_text = search_text
        self.session = session
        self.list_set = []
        # self.log.debug(self.__dict__)

    def _children_count(self, id):
        return CatalogObjectSearch.neo4j.run_query(
            f"match((n)-[:HAS]->(m{{deleted:False}})) where id(n) = {id} return count(m) as c", True)[0]['c']

    def output_creator(self, obj):
        if str(obj.labels)[1:] == "S3File":
            URI = 's3://' + str(obj['bucketName']) + '/' + str(obj['objectKey'])
            node_type = 'Delimited Data File' if obj['detectedColumns'] else 'Other'
            layer_type = 'S3 Bucket'
            description = 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available'
        elif str(obj.labels)[1:] == "SnowflakeTable":
            URI = 'sf://' + str(obj['schema']) + '/' + str(obj['name'])
            node_type = obj['type']
            layer_type = 'Snowflake Schema'
            description = 'No description available' if 'remarks' not in obj.keys() else obj[
                    'remarks'] if isinstance(obj['remarks'], str) else 'No description available'
        elif str(obj.labels)[1:] == "Object":
            URI = 'ob://' + str(obj['obj_layer_nm']) + '/' + str(obj['name'])
            node_type = obj['object_type']
            layer_type = 'Object'
            description = obj['obj_desc']
        else:
            URI = None
            node_type = None
            layer_type = None
            description = "no description available",

        return ({
            'Name': obj['name'],
            'Type': node_type,
            'Description': description,
            'Comments': '' if 'comments' not in obj.keys() else obj['comments'] if isinstance(obj['comments'],
                                                                                              str) else '',
            'Last Updated': 'NA',
            'Tags': self.neo4j.get_child_nodes(obj.identity, r_type='TagLink', child_type='Tag'),
            'DataAsset': self.neo4j.get_child_nodes(obj.identity, r_type='DataAssetLink', child_type='DataAsset'),
            'URI': URI,
            "URI_ID": obj.get("uri", ""),
            "Layer_name":obj['layer'],
            'Logical Name': 'No Name available' if 'logi_name' not in obj.keys() else obj[
                'logi_name'] if isinstance(obj['logi_name'], str) else 'No Name available',
            'Business Description': 'No description available' if 'buss_desc' not in obj.keys() else obj[
                'buss_desc'] if isinstance(obj['buss_desc'], str) else 'No description available',
            'Technical Description': 'No description available' if 'tech_desc' not in obj.keys() else obj[
                'tech_desc'] if isinstance(obj['tech_desc'], str) else 'No description available',
            'Logical Description': 'No description available' if 'logi_desc' not in obj.keys() else obj[
                'logi_desc'] if isinstance(obj['logi_desc'], str) else 'No description available',
            'Count': self._children_count(obj.identity),
            'Size': obj['size'],
            # 'search_score': score,
            'id': obj.identity,
            'Layer Type': layer_type,
            'created_date': "" if obj.get('created_date') == "" else parser.parse(obj.get('created_date', "")).strftime('%d/%m/%Y %H:%M:%S'),
            'modified_date':"" if obj.get('modified_date') == "" else parser.parse(obj.get('modified_date', "")).strftime('%d/%m/%Y %H:%M:%S'),
            'last_updated_ts': "" if obj.get('last_updated_ts','') == ""
                            else parser.parse(obj.get('last_updated_ts', "").split(".")[0]).strftime('%d/%m/%Y %H:%M:%S'),
            'owner': obj.get('owner', ""),
        })

    def execute(self):
        if self.filter is not None and self.filter != '':
            # file_level_mapping = {"matching name": "actual attribute in node"}
            file_level_mapping = {"technical_name": "name", 'logical_name': 'logi_name', "description": 'remarks',"unique_id":"uri"}
            column_level_mapping = {"column_technical_name": "name", 'column_logical_name': 'logi_name'}
            if json.loads(self.filter).get('layer'):
                temp = set({})
                for layer in json.loads(self.filter)['layer']:
                    query = f"""CALL db.index.fulltext.queryNodes('layer_search', 'layer:"{layer}"') yield node, score 
                                with node,score
                                match (node:Object) return distinct(id(node)) as output
                                union
                                CALL db.index.fulltext.queryNodes('layer_search', 'layer:"{layer}"') yield node, score 
                                with node,score 
                                match ((node)-[:HAS*]->(m:SnowflakeTable)) return distinct(id(m)) as output"""
                    temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
                self.list_set.append(temp)

            if json.loads(self.filter).get('tag'):
                temp = set({})
                for layer in json.loads(self.filter)['tag']:
                    query = f"""CALL db.index.fulltext.queryNodes('tag_search', 'name:"{layer}"') yield 
                                node, score with node,score match ((m)-[:TagLink]->(node)) return id(m) as output"""
                    temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
                self.list_set.append(temp)

            for attrname, attr in file_level_mapping.items():
                temp = set({})
                if json.loads(self.filter).get(attrname):
                    for value in json.loads(self.filter)[attrname]:
                        query = f"""CALL db.index.fulltext.queryNodes('object_search','{attr}:"{value}"') yield 
                                    node, score with node,score match (node) return id(node) as output"""
                        temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
                    self.list_set.append(temp)

            for attrname, attr in column_level_mapping.items():
                temp = set({})
                if json.loads(self.filter).get(attrname):
                    for value in json.loads(self.filter)[attrname]:
                        query = f"""CALL db.index.fulltext.queryNodes('column_search','{attr}:"{value}"') yield 
                                    node, score with node,score match ((m)-[:HAS]->(node)) return id(m) as output"""
                        temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
                    self.list_set.append(temp)

        if self.contains_text is not None and self.contains_text != '':
            temp = set({})
            query =f"""CALL db.index.fulltext.queryNodes('object_input_search','*{self.contains_text}*') yield node, score
                        with node,score optional match ((m)-[:TagLink|:DataAssetLink]->(node)) return 
                        case labels(node)[0]
                        when "Tag" then id(m)
                        when "DataAsset" then id(m)
                        else id(node)
                        end as output
                        union
                        CALL db.index.fulltext.queryNodes('object_input_search','{self.contains_text}') yield node, score
                        with node,score optional match ((m)-[:TagLink|:DataAssetLink]->(node)) return 
                        case labels(node)[0]
                        when "Tag" then id(m)
                        when "DataAsset" then id(m)
                        else id(node)
                        end as output"""
            temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
            query = f"""match (n) where n.uri=~".*{self.contains_text}.*" return id(n) as output"""
            temp = temp.union(set(map(lambda x: x['output'], self.neo4j.run_query(query, True))))
            self.list_set.append(temp)

        if len(self.list_set) > 0:
            output = self.list_set[0]
            for single_set in self.list_set[1:]:
                output = output.intersection(single_set)

        final_output = []
        for single_id in output:
            intermediate_obj = self.neo4j.run_query(f"match (n) where id(n) = {single_id} return n", True)
            final_output.extend(self.output_creator(i['n']) for i in intermediate_obj)

        self.add_response('status', "Success")
        self.add_response('contents', final_output)


class AutoSuggestion(Service):
    def __init__(self, session,q, **kwargs):
        super().__init__()
        self.q = q
        self.neo4j = Neo4j()

    def execute(self):
        # added regex to avoid special character input from serach box to stop unwanted regex search on neo4j
        if len(self.q)>0 and bool(re.match(r'\w',self.q)):
            temp = set({})
            query_name = suggestion(self.q)

            for i in self.neo4j.run_query(query_name,True):
                temp = temp.union(i.values())
            all_values = list(temp)
            all_values.remove(None) if all_values.__contains__(None) else all_values
            self.log.debug(all_values)
            exp = re.compile(".*"+self.q+".*",re.IGNORECASE)
            op_name = list(filter(exp.match, all_values))
            # op1 = list(map(lambda x: re.sub(re.findall(self.q, x, re.IGNORECASE)[0],
            #                                 '<strong>' + re.findall(self.q, x, re.IGNORECASE)[0] + '</strong>',x), op))

            self.add_response('status', "Success")
            self.add_response('contents', op_name)
