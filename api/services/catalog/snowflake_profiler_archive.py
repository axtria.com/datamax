import pandas as pd

from vendor.snowflake import Snowflake
from vendor.neo4j import Neo4j
from services.catalog.util import uid, safe_html, valid_html
    
from services import Service

import services.catalog.snowflake_profiler_config as profiler_config

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.float_format', '{:.2f}'.format)


class SnowflakeProfiler(Service):
    """
    Profiles/calculates basic statistics of a table present in snowflake.
    """

    def __init__(self, session, node_id, config=profiler_config, statistics='false', cached='true',
                 **kwargs):
        """
        Parameters
        ----------
        table_name : string
            name of snowflake table
        """
        super().__init__()
        self.config = config
        self.cached = True if cached.strip().lower() == 'true' else False
        self.statistics = True if statistics.strip().lower() == 'true' else False

        self.all_columns = None
        self.numeric_columns = None
        self.categorical_columns = None
        self.profiling_dfs = None
        self.row_count = None
        self.messages = []

        node = Neo4j().get_node_by_id(node_id)
        self.table_schema = node['schema']
        self.table_name = node['name']
        self.snowflake = Snowflake.initialize(node['connector_id'], session)

    def execute(self, return_path=False):

        self.profiling_dfs = []

        if self.config.sample_size > 0:
            try:
                sample = self.snowflake.execute(
                    'select * from {schema}.{table} limit {size}'.format(schema=self.table_schema, table=self.table_name,
                                                                         size=self.config.sample_size))
                sample = sample.astype(str)
                self.profiling_dfs.append(('Sample', sample))
                self.get_json()
            except:
                json_obj={'title': "null" , 'content': "null" , 'columns': "null"}
                return self.add_response('response', json_obj)


    def get_json(self):
        """
        Returns json of `profiling_dfs`
        """
        json_obj = []
        for title, df in self.profiling_dfs:
            json_obj.append(
                {'title': title, 'content': df.fillna('').to_dict(orient='records'), 'columns': list(df.columns)})
        return self.add_response('response', json_obj)