#   datamax/api/vendor/snowflake_full_scan.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
'''Datamax Catalog - Snowflake full scan'''

from datetime import datetime

import pandas as pd
from numpy import random
from sqlalchemy import Sequence

from services import Service, notification
from services.api_exchange import ApiExchange
from utils.catalog_builder import get_snowflake_from_postgres, get_snowflake_data, insert_snowflake_to_neo4j, \
    get_data_layer_meta
from utils.exceptions import DataLayerNotFound, NoDataAvailable
from utils.helpers import thread
from vendor.appdb import AppDb
from vendor.neo4j import Neo4j
from vendor.snowflake import Snowflake

neo4j = Neo4j()
app_db = AppDb()


class SnowflakeFullScan(Service):
    def __init__(self, session, sf_connector_id, **kwargs):
        """Register metadata of Snowflake to neo4j db"""
        super().__init__()

        self.session = session
        self.sf_connector_id = int(sf_connector_id)
        self.sf = Snowflake.initialize(self.sf_connector_id, session)
        self.over_the_network = True
        self.add_response('message', 'Request Acknowledged')

    @thread
    def execute(self):

        filter_config = app_db.execute("select layer_id, name as layer_name, description, schema_name, connector_id "
                                       "from admn.layer where layer_type='sf' and is_active")

        schemas = filter_config.loc[filter_config['connector_id'] == self.sf_connector_id, 'schema_name'].tolist()

        schemas_tuple = "('" + "','".join(schemas) + "')"

        sf_query = 'select ' \
                   't.table_schema as "table_schema", ' \
                   't.table_name as "table_name", ' \
                   't.table_type as "table_type", ' \
                   't.comment as "table_remarks", ' \
                   'c.column_name as "column_name", ' \
                   'c.data_type as "column_datatype", ' \
                   'c.ordinal_position as "column_ordinal_position", ' \
                   'c.comment as "column_remarks" ' \
                   'from ' \
                   f'"{self.sf.database}"."INFORMATION_SCHEMA"."TABLES" t join "{self.sf.database}"."INFORMATION_SCHEMA"."COLUMNS" c ' \
                   'on ' \
                   '(t.table_name = c.table_name and t.table_schema = c.table_schema) ' \
                   f'where t.table_schema in {schemas_tuple};'

        metadata = self.sf.execute(sf_query)
        metadata.fillna('No description available', inplace=True)
        metadata = pd.merge(metadata, filter_config, how='left', left_on='table_schema', right_on='schema_name')

        database = self.sf.database
        account_id = self.sf.account_id

        if self.over_the_network:

            for _, row in metadata.iterrows():
                neo4j_query = f'merge(a:SnowflakeAccount{{name:"{account_id}"}}) set a +=  {{deleted:false}} ' \
                              f'merge(sf:Snowflake{{database:"{database}", account:"{account_id}"}}) set sf +=  {{deleted:false, connector_id: "{self.sf_connector_id}"}} ' \
                              f'merge(s:SnowflakeSchema{{name:"{row.table_schema}", database:"{database}", account:"{account_id}"}}) set s +=  {{deleted:false, layer:"{row.layer_name}", remarks:"{row.description}", layer_id: "{row.layer_id}"}} ' \
                              f'merge(t:SnowflakeTable{{name:"{row.table_name}", schema:"{row.table_schema}", type:"{row.table_type}", database:"{database}", account:"{account_id}"}}) set t +=  {{deleted:false, remarks:"{row.table_remarks}", layer_id: "{row.layer_id}", connector_id: "{self.sf_connector_id}"}} ' \
                              f'merge(c:SnowflakeColumn{{name:"{row.column_name}", table:"{row.table_name}", schema:"{row.table_schema}", database:"{database}", account:"{account_id}"}}) set c +=  {{deleted:false, remarks:"{row.column_remarks}", ordinal_position:{row.column_ordinal_position}, datatype:"{row.column_datatype}"}} ' \
                              'merge((a)-[:HAS]->(sf)) ' \
                              'merge((sf)-[:HAS]->(s)) ' \
                              'merge((s)-[:HAS]->(t)) ' \
                              'merge((t)-[:HAS]->(c)) '

                _ = neo4j.run_query(neo4j_query)

        else:

            fname = f'tmp_{self.uid()}.csv'
            fpath = f'{neo4j.get_import_dir()}{fname}'

            metadata.to_csv(fpath, index=None)

            neo4j_query = f'load csv with headers from "file:///{fname}" as row ' \
                          f'merge(a:SnowflakeAccount{{name:"{account_id}"}}) set a +=  {{deleted:false}} ' \
                          f'merge(sf:Snowflake{{database:"{database}", account:"{account_id}"}}) set sf +=  {{deleted:false}} ' \
                          f'merge(s:SnowflakeSchema{{name:row.table_schema, database:"{database}", account:"{account_id}"}}) set s +=  {{deleted:false, layer:row.layer_name, remarks:row.description}} ' \
                          f'merge(t:SnowflakeTable{{name:row.table_name, schema:row.table_schema, type:row.table_type, database:"{database}", account:"{account_id}"}}) set t +=  {{deleted:false, remarks:row.table_remarks}} ' \
                          f'merge(c:SnowflakeColumn{{name:row.column_name, table:row.table_name, schema:row.table_schema, database:"{database}", account:"{account_id}"}}) set c +=  {{deleted:false, remarks:row.column_remarks, ordinal_position:row.column_ordinal_position, datatype:row.column_datatype}} ' \
                          'merge((a)-[:HAS]->(sf)) ' \
                          'merge((sf)-[:HAS]->(s)) ' \
                          'merge((s)-[:HAS]->(t)) ' \
                          'merge((t)-[:HAS]->(c)) '

            _ = neo4j.run_query(neo4j_query)

        self.add_response('status', 'success')

    def uid(self):
        return hex(random.randint(10 ** 6)) + datetime.now().strftime('___%d_%b_%Y__%H_%M_%S_%f')


class SnowflakeFullScanV2(Service):
    def __init__(self, session, sf_connector_id, **kwargs):
        """Register metadata of Snowflake to neo4j db"""
        super().__init__()
        self.session = session
        self.app_db = AppDb()
        self.params = kwargs
        self.sf_connector_id = int(sf_connector_id)
        self.sf = Snowflake.initialize(self.sf_connector_id, session)
        self.add_response('message', 'Request Acknowledged')
        self.sf_records = pd.DataFrame()
        self.pg_records = pd.DataFrame()
        self.layer_df = pd.DataFrame()

    @thread
    def execute(self):
        self.pg_records = self.app_db.execute(f"""select
            DLM.schema_name as table_schema,
            DLM.data_lyr_name as layer_name,
            DLM.data_lyr_id as layer_id,
            OM.obj_nm as table_name,
            'BASE TABLE' as table_type,
            '' as table_remarks,
            ATRM.attr_nm as column_name,
            ATRm.attr_data_typ as column_datatype,
            ATRM.col_loc as column_ordinal_position,
            ATRM.attr_desc as column_remarks,
            OM.obj_uri_id as uri,
            OM.version as version,
            OM.obj_id as obj_id
            from system_config.object_meta as OM
            join system_config.data_layer_meta as DLM on DLM.data_lyr_id = OM.src_data_layer_id
            join system_config.connection_meta as CM on CM.id = DLM.conn_id
            join system_config.attr_meta as ATRM on OM.obj_id = ATRM.obj_id
            where DLM.schema_name is not null and DLM.lyr_type = 'sf' and CM.id = {self.sf.connection_id}""")

        self.layer_df = self.app_db.execute(f"""select distinct schema_name as table_schema, data_lyr_id as layer_id, data_lyr_name as layer_name from 
                                            system_config.data_layer_meta 
                                            where lyr_type = 'sf' and conn_id = {self.sf.connection_id}""")
        if not self.layer_df.get('table_schema')[0]:
            raise DataLayerNotFound

        schemas_tuple = "('" + "','".join(self.layer_df['table_schema']) + "')"

        sf_query = 'select ' \
                   't.table_schema as "table_schema", ' \
                   't.table_name as "table_name", ' \
                   't.table_type as "table_type", ' \
                   't.comment as "table_remarks", ' \
                   'c.column_name as "column_name", ' \
                   'c.data_type as "column_datatype", ' \
                   'c.ordinal_position as "column_ordinal_position", ' \
                   'c.comment as "column_remarks" ' \
                   'from ' \
                   f'"{self.sf.database}"."INFORMATION_SCHEMA"."TABLES" t join "{self.sf.database}"."INFORMATION_SCHEMA"."COLUMNS" c ' \
                   'on ' \
                   '(t.table_name = c.table_name and t.table_schema = c.table_schema) ' \
                   f'where t.table_schema in {schemas_tuple};'

        latest_ts = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f 0000')
        self.log.debug(f'latest_ts = {latest_ts}')

        self.sf_records = self.sf.execute(sf_query)
        if self.sf_records.empty:
            return

        self.resolve_layer_name()
        self.resolve_URI()
        self.resolve_attr_id()
        to_be_inserted = self.sf_records[~self.sf_records['uri'].isin(self.get_pg_uris())]
        to_be_inserted["audit_batch_id"] = "UI00100"
        to_be_inserted["audit_insrt_id"] = self.params.get('loggedin_userid')
        to_be_inserted['is_mandatory'] = True
        to_be_inserted["is_actv"] = True
        to_be_inserted["eff_srt_dt"] = datetime.today().strftime('%Y-%m-%d')
        to_be_inserted["eff_end_dt"] = '9999-12-31'
        to_be_inserted["audit_insrt_dt"] = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        to_be_inserted["layer_flg"] = "I"
        to_be_inserted["abslt_path"] = "test_data"
        self.app_db.insert(
            to_be_inserted[["audit_insrt_id","audit_batch_id","audit_insrt_dt","is_actv","attr_id", "is_mandatory",
                 "eff_end_dt","eff_srt_dt","uri", "column_ordinal_position", "column_datatype",
                 "column_name", "column_remarks", "obj_id"]]
                .rename(columns={"table_schema": "schema_name",
                                 "table_name": "obj_nm",
                                 "column_name": "attr_nm",
                                 "column_datatype": "attr_data_typ",
                                 "column_ordinal_position": "col_loc",
                                 "column_remarks": "attr_desc",
                                 "uri": "obj_uri_id"}),
            'attr_meta',

            'system_config')
        self.app_db.insert(to_be_inserted.drop_duplicates(subset=['uri'])
                           [["uri", "version", "obj_id","table_type","table_name","audit_insrt_id","audit_batch_id",
                             "audit_insrt_dt","is_actv","eff_end_dt","eff_srt_dt","layer_id"]].
                           rename(columns={"uri": "obj_uri_id","table_type":"obj_typ",
                                           "table_name":"obj_nm","layer_id":"src_data_layer_id"}),
                           'object_meta',
                           'system_config')

        for _, row in self.sf_records.iterrows():
            neo4j_query = f'merge(a:SnowflakeAccount{{name:"{self.sf.account_id}"}}) set a +=  {{deleted:false}} ' \
                          f'merge(sf:Snowflake{{database:"{self.sf.database}", account:"{self.sf.account_id}"}}) set sf +=  {{deleted:false, connector_id: "{self.sf.connection_id}"}} ' \
                          f'merge(s:SnowflakeSchema{{name:"{row.table_schema}", database:"{self.sf.database}", account:"{self.sf.account_id}"}}) set s +=  {{last_updated_ts:"{latest_ts}", deleted:false, layer:"{row.layer_name}", remarks:"{row.table_remarks}", layer_id: "{row.layer_id}"}} ' \
                          f'merge(t:SnowflakeTable{{uri: "{row.uri}"}}) set t +=  {{last_updated_ts:"{latest_ts}",{{deleted:false, name:"{row.table_name}", schema:"{row.table_schema}", type:"{row.table_type}", database:"{self.sf.database}", account:"{self.sf.account_id}", remarks:"{row.table_remarks}", layer_id: "{row.layer_id}", connector_id: "{self.sf.connection_id}",obj_id:"{row.obj_id}",version:"{row.version}",layer:"{row.layer_name}"}} ' \
                          f'merge(c:SnowflakeColumn{{name:"{row.column_name}", parent_uri_id: "{row.uri}"}}) set c +=  {{deleted:false,  table:"{row.table_name}", schema:"{row.table_schema}", database:"{self.sf.database}", account:"{self.sf.account_id}", remarks:"{row.column_remarks}", ordinal_position:{row.column_ordinal_position}, datatype:"{row.column_datatype}",attr_id:"{row.attr_id}"}} ' \
                          'merge((a)-[:HAS]->(sf)) ' \
                          'merge((sf)-[:HAS]->(s)) ' \
                          'merge((s)-[:HAS]->(t)) ' \
                          'merge((t)-[:HAS]->(c)) '
            _ = neo4j.run_query(neo4j_query)


        self.add_response('status', 'success')

    def get_pg_uris(self):
        try:
            return self.pg_records['uri'].unique()
        except Exception as e:
            self.log.exception(e)
            return []

    def resolve_layer_name(self):
        pg_records = self.layer_df[['layer_name', 'layer_id', 'table_schema']].set_index("table_schema")
        self.sf_records = pd.merge(self.sf_records, pg_records, how='left',
                                   left_on='table_schema', right_index=True)

    def resolve_attr_id(self):
        last_attr_id = \
            self.app_db.execute(f"""SELECT last_value FROM system_config.attr_meta_attr_id""")[
                'last_value'][0]
        self.sf_records['attr_id'] = range(last_attr_id + 1, last_attr_id + len(self.sf_records) + 1)

    def resolve_URI(self):
        """with respect to Postgres identify the new objects, and existing object changes """
        self.sf_records['key'] = self.sf_records['table_schema'] + '.' + self.sf_records['table_name']
        self.pg_records['key'] = self.pg_records['table_schema'] + '.' + self.pg_records['table_name']
        new_objects = self.sf_records[~self.sf_records['key'].isin(self.pg_records['key'].unique())]


        for each_key in new_objects['key'].unique():
            service = ApiExchange(self.session, name='get_object_instance', obj_id=None)
            service.execute()
            self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = service.response['Contents'][0][
                'obj_id']
            self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = service.response['Contents'][0][
                'version']
            self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = service.response['Contents'][0][
                'obj_uri_id']

        existing_objects = self.sf_records[self.sf_records['key'].isin(self.pg_records['key'].unique())]

        for each_key in existing_objects['key'].unique():
            # with no change
            sf_records_for_each_key = self.sf_records.loc[self.sf_records['key'] == each_key, ['column_name', 'column_datatype']]
            pg_records_for_each_key = self.pg_records.loc[self.pg_records['key'] == each_key, ['column_name', 'column_datatype']]
            # exact match - no change in uri, version
            if sf_records_for_each_key[~(sf_records_for_each_key['column_name'] + sf_records_for_each_key['column_datatype']).isin(pg_records_for_each_key['column_name'] + pg_records_for_each_key['column_datatype'])].empty:
                matched_record = self.pg_records.loc[self.pg_records['key'] == each_key, ['obj_id', 'version', 'uri']]
                obj_id, version, uri = matched_record.iloc[0, :].values
                self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = obj_id
                self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = version
                self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = uri
            # with changes - fetch new uri/version
            else:
                matched_record = self.pg_records.loc[self.pg_records['key'] == each_key, ['obj_id']]
                obj_id = matched_record.iloc[0, :].values[0]
                service = ApiExchange(self.session, name='get_object_instance', obj_id=str(obj_id))
                service.execute()
                self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = service.response['Contents'][0][
                    'obj_id']
                self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = service.response['Contents'][0][
                    'version']
                self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = service.response['Contents'][0][
                    'obj_uri_id']
















# this snowflake class for scaning is temporary, will be removed once layer scanning will be finalised till week end
class SnowflakeFullScanV3_temporary(Service):
    def __init__(self, session, sf_layer_id, **kwargs):
        """Register metadata of Snowflake to neo4j db"""
        super().__init__()
        self.session = session
        self.app_db = AppDb()
        self.params = kwargs
        self.sf_layer_id = int(sf_layer_id)
        self.sf = Snowflake.initialize_with_layer(self.sf_layer_id, session)
        self.add_response('message', 'Request Acknowledged')
        self.sf_records = pd.DataFrame()
        self.pg_records = pd.DataFrame()
        self.layer_df = pd.DataFrame()

    @thread
    def execute(self):
        self.pg_records = self.app_db.execute (get_snowflake_from_postgres(self.sf_layer_id))
        if len(self.pg_records) == 0:
            raise NoDataAvailable
        self.layer_df = self.app_db.execute(get_data_layer_meta(self.sf_layer_id))
        if not self.layer_df.get('table_schema')[0]:
            raise DataLayerNotFound

        schemas_tuple = "('" + "','".join(self.layer_df['table_schema']) + "')"

        sf_query = get_snowflake_data (self.sf.database,schemas_tuple)

        latest_ts = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f 0000')
        self.log.debug(f'latest_ts = {latest_ts}')

        self.sf_records = self.sf.execute(sf_query)
        if self.sf_records.empty:
            self.log.debug("No snowflake records received")
            return

        self.resolve_layer_name()
        self.log.debug("resolve_layer_name_called")
        self.resolve_URI()
        self.log.debug("resolve_URI_called")
        self.resolve_attr_id()
        self.log.debug("resolve_attr_id_called")

        to_be_inserted = self.sf_records[~self.sf_records['uri'].isin(self.get_pg_uris())]
        to_be_inserted["audit_batch_id"] = "UI00100"
        to_be_inserted["audit_insrt_id"] = self.params.get('loggedin_userid')
        to_be_inserted['is_mandatory'] = True
        to_be_inserted["is_actv"] = True
        to_be_inserted["eff_srt_dt"] = datetime.today().strftime('%Y-%m-%d')
        to_be_inserted["eff_end_dt"] = '9999-12-31'
        self.app_db.insert(
            to_be_inserted[["audit_insrt_id","audit_batch_id","created_date","modified_date","is_actv","attr_id", "is_mandatory",
                            "eff_end_dt","eff_srt_dt","uri", "column_ordinal_position", "column_datatype",
                            "column_name", "column_remarks", "obj_id"]]
            .rename(columns={"table_schema": "schema_name",
                             "table_name": "obj_nm",
                             "column_name": "attr_nm",
                             "column_datatype": "attr_data_typ",
                             "column_ordinal_position": "col_loc",
                             "column_remarks": "attr_desc",
                             "uri": "obj_uri_id",
                             "created_date":"audit_insrt_dt",
                             "modified_date":"audit_updt_dt"}),
            'attr_meta',

            'system_config')
        self.app_db.insert(to_be_inserted.drop_duplicates(subset=['uri'])
                           [["uri", "version", "obj_id","table_type","table_name","audit_insrt_id","audit_batch_id",
                             "created_date","is_actv","eff_end_dt","eff_srt_dt","layer_id","modified_date"]].
                           rename(columns={"uri": "obj_uri_id","table_type":"obj_typ",
                                           "table_name":"obj_nm","layer_id":"src_data_layer_id",
                                           "created_date":"audit_insrt_dt","modified_date":"audit_updt_dt"}),
                           'object_meta',
                           'system_config')

        email_data = set({})
        for _, row in self.sf_records.iterrows():
            neo4j_query = insert_snowflake_to_neo4j(self.sf,row,self.params,latest_ts)
            _ = neo4j.run_query(neo4j_query)
            email_data.add("""<p><b>Object id</b> : """ + str(row.uri) +""",<b>name</b> : """ + str(row.table_name) +""",<b>On layer</b> : """ + str(row.layer_name) + """</p>""")
        self.send_email(email_data)
        self.add_response('status', 'success')

    def get_pg_uris(self):
        try:
            return self.pg_records['uri'].unique()
        except Exception as e:
            self.log.exception(e)
            return []

    def resolve_layer_name(self):
        pg_records = self.layer_df[['layer_name', 'layer_id', 'table_schema']].set_index("table_schema")
        self.sf_records = pd.merge(self.sf_records, pg_records, how='left',
                                   left_on='table_schema', right_index=True)

    def resolve_attr_id(self):
        last_attr_id = \
            self.app_db.execute(f"""SELECT last_value FROM system_config.attr_meta_attr_id""")[
                'last_value'][0]
        self.sf_records['attr_id'] = range(last_attr_id + 1, last_attr_id + len(self.sf_records) + 1)

    def resolve_URI(self):
        """with respect to Postgres identify the new objects, and existing object changes """
        self.sf_records['key'] = self.sf_records['table_schema'] + '.' + self.sf_records['table_name']
        self.pg_records['key'] = self.pg_records['table_schema'] + '.' + self.pg_records['table_name']
        new_objects = self.sf_records[~self.sf_records['key'].isin(self.pg_records['key'].unique())]


        for each_key in new_objects['key'].unique():
            service = ApiExchange(self.session, name='get_object_instance', obj_id=None)
            service.execute()
            self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = service.response['Contents'][0][
                'obj_id']
            self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = service.response['Contents'][0][
                'version']
            self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = service.response['Contents'][0][
                'obj_uri_id']

        existing_objects = self.sf_records[self.sf_records['key'].isin(self.pg_records['key'].unique())]

        for each_key in existing_objects['key'].unique():
            # with no change
            sf_records_for_each_key = self.sf_records.loc[self.sf_records['key'] == each_key, ['column_name', 'column_datatype']]
            pg_records_for_each_key = self.pg_records.loc[self.pg_records['key'] == each_key, ['column_name', 'column_datatype','version']]
            # exact match - no change in uri, version ; copy postgres to sf dataframe
            sf_records_for_each_key['SF_UMD'] = sf_records_for_each_key['column_name'] + sf_records_for_each_key['column_datatype']
            pg_records_for_each_key['PG_UMD'] = pg_records_for_each_key['column_name'] + pg_records_for_each_key['column_datatype']
            df_pg = pg_records_for_each_key.sort_values('PG_UMD').groupby('version').agg([(','.join)])
            df_pg.columns = df_pg.columns.droplevel(1)
            df_pg.reset_index(inplace=True)
            df_sf = sf_records_for_each_key.sort_values('SF_UMD').agg(','.join).to_frame().transpose()
            if not df_sf.merge(df_pg, how='inner', left_on='SF_UMD', right_on='PG_UMD').dropna().empty:
                matched_version = df_sf.merge(df_pg, how='inner', left_on='SF_UMD', right_on='PG_UMD').dropna()['version'][0]
                matched_record = self.pg_records.loc[(self.pg_records['key'] == each_key) & (self.pg_records['version'] == matched_version), ['obj_id', 'version', 'uri']]
                obj_id, version, uri = matched_record.iloc[0, :].values
                self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = obj_id
                self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = version
                self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = uri
            # with changes - fetch new uri/version
            else:
                matched_record = self.pg_records.loc[self.pg_records['key'] == each_key, ['obj_id']]
                obj_id = matched_record.iloc[0, :].values[0]
                service = ApiExchange(self.session, name='get_object_instance', obj_id=str(obj_id))
                service.execute()
                self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = service.response['Contents'][0][
                    'obj_id']
                self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = service.response['Contents'][0][
                    'version']
                self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = service.response['Contents'][0][
                    'obj_uri_id']

    def send_email(self,email_data):
        email_string = ''
        for single_email_content in email_data:
            email_string += "" + single_email_content
        email_string = "<HTML><BODY>" + email_string + "</BODY></HTML>"
        # send email to uesrgroups
        service = notification.SendNotification(self.session, name='onboard notification', description=email_string,
                                                type='HTML', **self.params)
        service.execute()

