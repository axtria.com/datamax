from services import Service
from vendor.neo4j import Neo4j


class GetAttributes(Service):
    def __init__(self, session, **kwargs):
        super().__init__()
        self.session = session
        self.params = kwargs
        self.neo4j = Neo4j()
        # self.attr = {'matching name': 'name to display on UI')
        self.attr={'technical_name':'Technical Name',
                    'logical_name':'Logical Name',
                    'description':'Description',
                    'column_technical_name':'Column Technical Name',
                    'column_logical_name':'Column Logical Name',
                    'unique_id':'Unique ID',
                    'tag':'Tag',
                    'layer':'Layer'}
    def execute(self):
        final_output = []
        del self.params['loggedin_userid']
        del self.params['loggedin_username']
        # file_level_mapping = {"matching name": "actual attribute in node"}
        file_level_mapping = {"technical_name": "name", 'logical_name': 'logi_name', "description": 'remarks',"unique_id":"uri"}
        column_level_mapping = {"column_technical_name": "name", 'column_logical_name': 'logi_name'}
        if(len(self.params)>0):
            if(file_level_mapping.get(self.params['attr'])):
                value= file_level_mapping[self.params['attr']]
                df = self.neo4j.run_query(f"match(n) where (n:Object or n:SnowflakeTable) return distinct(n.{value}) as query")
                final_output.append({'attribute_name':self.params['attr'],'display_attribute_name':value,'unique_values':list(df.get('query', []))})

            elif(column_level_mapping.get(self.params['attr'])):
                value = column_level_mapping[self.params['attr']]
                df = self.neo4j.run_query(f"match((n)-[:HAS*1..]->(m)) where (n:Object or n:SnowflakeTable) return distinct(m.{value}) as query")
                final_output.append({'attribute_name':self.params['attr'],'display_attribute_name':value,'unique_values':list(df.get('query', []))})

            elif (self.params['attr']=='tag'):
                tags = self.neo4j.run_query(f"match(n:Tag) where n.status = 'true' return distinct(n.name) as query")
                final_output.append({'attribute_name':'tag','display_attribute_name':'Tag','unique_values': list(tags.get("query", []))})

            elif (self.params['attr'] == 'id'):
                URI = self.neo4j.run_query(f"match (n) where n:Object or n:SnowflakeTable return distinct(id(n)) as query "
                                           f"union "
                                           f"match ((n)-[]->(m)) where n:Object or n:SnowflakeTable return distinct(id(m)) as query")
                final_output.append({'attribute_name':'id','display_attribute_name':'URI','unique_values': list(URI.get("query", []))})

            elif (self.params['attr'] == 'layer'):
                Layer = self.neo4j.run_query(f"match(n) where (n:Object or n:SnowflakeSchema) return distinct(n.layer) as query")
                final_output.append({'attribute_name':'layer', 'display_attribute_name': 'Layer', 'unique_values': list(Layer.get("query", []))})
            else:
                final_output.append({'attribute_name': 'invalid attribute', 'display_attribute_name': 'invalid attribute','unique_values': 'invalid attribute'})
            self.add_response('Content', final_output)
        else:
            for key,value in self.attr.items():
                final_output.append({'attribute_name':key, 'display_attribute_name':value, 'unique_values': []})
            self.add_response('Content',final_output)
