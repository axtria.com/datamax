import pandas as pd
import numpy as np
from io import StringIO
from os import path
import seaborn, json
import matplotlib.pyplot as plt
from utils.querybuilder import column_metadata_query, table_metadata_query, record_count_query

from vendor.redshift import Redshift
from vendor.neo4j import Neo4j
from services.catalog.util import uid, safe_html, valid_html

from services import Service

import services.catalog.redshift_profiler_config as profiler_config

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.float_format', '{:.2f}'.format)

class RedshiftProfiler(Service):
    """
    Profiles/calculates basic statistics of a table present in redshift.
    """
    def __init__(self, session, rs_connector_id, node_id, config = profiler_config, statistics='false', cached = 'true', **kwargs):
        """
        Parameters
        ----------
        table_name : string
            name of redshift table
        """
        super().__init__()
        self.config = config
        self.cached = True if cached.strip().lower() == 'true' else False
        self.statistics = True if statistics.strip().lower() == 'true' else False

        self.redshift = Redshift.initialize(rs_connector_id, session)

        self.all_columns = None
        self.numeric_columns = None
        self.categorical_columns = None
        self.profiling_dfs = None
        self.row_count = None
        self.messages = []

 
        node = Neo4j().get_node_by_id(node_id)
        self.table_schema = node['schema']
        self.table_name = node['name']


    def execute(self, return_path=False):
        """
        Assumptions
        -----------
        If `standard_obj` is null no KPI's will be calcuted
        """

        if self.cached:
            try:
                dirname = 'tmp/'
                fname = self.table_schema + '__' + self.table_name + '.json'
                fpath = dirname + fname
                with open(fpath,'r') as f:                   
                    if return_path:
                        self.add_response('response', path.abspath(fpath))
                    else: 
                        self.add_response('response',json.loads(f.read()))
                self.log.debug('cached used')
                return
            except:self.log.exception("Cahed not used", exc_info=True)


        table_metadata = self.redshift.execute(table_metadata_query(self.table_schema, self.table_name))

        row_count = self.redshift.execute(record_count_query(self.table_schema, self.table_name))
        self.row_count = row_count.at[0,'record_count']

        table_metadata['row_count'] = self.row_count

        column_metadata = self.redshift.execute(column_metadata_query(self.table_schema, self.table_name))

        column_metadata = column_metadata.dropna(axis=1, how='all')
        column_metadata = column_metadata.loc[:,~column_metadata.columns.isin(table_metadata.columns)]
        column_metadata.set_index('name', inplace=True)


        self.all_columns = column_metadata['column_name']
        self.numeric_columns = column_metadata.loc[column_metadata['data_type'].str.lower().isin(Redshift.numeric_dtypes),'column_name']

        self.profiling_dfs = []

        if self.config.table_metadata:
            self.profiling_dfs.append(('Table Metadata',table_metadata))

        if self.config.column_metadata:  
            del column_metadata.index.name, column_metadata['column_name']          
            self.profiling_dfs.append(('Column Metadata', column_metadata))

        if self.config.sample_size > 0:
            sample = self.redshift.execute('select * from {schema}.{table} limit {size}'.format(schema=self.table_schema, table=self.table_name, size=self.config.sample_size))
            sample = sample.astype(str)
            self.profiling_dfs.append(('Sample',sample))

        try:
            if self.config.statistics and self.statistics:     
                statistics = self.basic_statistics()       
                self.profiling_dfs.append(('Statistics',statistics))
        except:
            self.log.exception('Some error occured while calculating stats.', exc_info=True)
            self.add_response('error','Some error occured while calculating stats.')

        if self.config.custom_profiles:
            for d in self.config.custom_profiles:
                try:
                    profile = self.redshift.execute(d['query'])
                    self.profiling_dfs.append((d['title'], profile))
                except:pass

        df_json = self.get_json()

        dirname = 'tmp/'
        fname = self.table_schema + '__' + self.table_name + '.json'
        fpath = dirname + fname
        with open(fpath,'w') as f:
            f.write(json.dumps(df_json, indent=4))

        if return_path:
            self.add_response('response',path.abspath(fpath))
        else: 
            self.add_response('response',df_json)


    def basic_statistics(self):
        """
        Calculates basic statistics of `table_name` like row_count, sum, min, mean, median etc.
        """

        query_parts = ['1 as i']
        
        if self.config.statistics['sum']:
            query_sum = ('sum(' + self.numeric_columns +') as '+ self.numeric_columns + '__sum').tolist()
            query_parts += query_sum

        if self.config.statistics['min'] or self.config.histograms:
            query_min = ('min(' + self.numeric_columns +') as '+ self.numeric_columns + '__min').tolist()
            query_parts += query_min

        if self.config.statistics['max'] or self.config.histograms:
            query_max = ('max(' + self.numeric_columns +') as '+ self.numeric_columns + '__max').tolist()
            query_parts += query_max

        if self.config.statistics['avg']:
            query_avg = ('avg(' + self.numeric_columns +') as '+ self.numeric_columns + '__mean').tolist()
            query_parts += query_avg

        if self.config.statistics['stddev']:
            query_std = ('stddev(' + self.numeric_columns +') as '+ self.numeric_columns + '__standard_dev').tolist()
            query_parts += query_std

        if self.config.statistics['null_count']:
            query_null_count = ('sum(case when '+ self.all_columns +' is null then 1 else 0 end) as ' + self.all_columns +'__null_count').tolist()
            query_parts += query_null_count

        if self.config.statistics['unique_count']:
            query_unique_count = ('count(distinct '+ self.all_columns +') as ' + self.all_columns +'__unique_values').tolist()
            query_parts += query_unique_count

        query = """select {parts} from {schema}.{table}""".format(
            parts = ', '.join(query_parts),
            schema = self.table_schema,
            table = self.table_name)

        df = self.redshift.execute(query)

        if self.config.statistics['percentiles'] and not self.numeric_columns.empty:
            percentiles = self.get_percentiles(percentiles=self.config.statistics['percentiles'])
            df = pd.concat([df,percentiles], axis=1)

        df = pd.wide_to_long(df,
            stubnames = self.all_columns,
            i = 'i',
            j = 'profile',
            sep = '__',
            suffix = '\\w+').reset_index()

        del df['i']

        df = df.set_index('profile').transpose()
        del df.columns.name

        if self.config.histograms:
            histograms = self.get_histograms(df)
            df = pd.concat([df,histograms], axis=1, sort=False)

        try:
            df['unique_values'] = df['unique_values'].astype(int)
            df['null_count'] = df['null_count'].astype(int)
        except:pass

        df.reset_index(inplace=True)
        df.rename(columns={'index':'column'}, inplace=True)

        return df

    def get_percentiles(self, percentiles=[25,50,75]):
        """
        Calculates 25 percentiles, 50 percentile (median), 75 percentile
        """
        query_parts = []
        for percentile in percentiles:
            parts = ('percentile_disc('+str(percentile/100)+') within group (order by '+self.numeric_columns+') over() as '+self.numeric_columns+'__'+str(percentile)+'_percentile').tolist()
            query_parts += parts

        query = """select distinct {parts} from {schema}.{table}""".format(
            parts = ', '.join(query_parts),
            schema = self.table_schema,
            table = self.table_name)

        df = self.redshift.execute(query)
        return df

    def get_histograms(self, statistics):

        self.categorical_columns = statistics[statistics['unique_values']<=self.config.histogram_bins].index

        histograms = []

        for col in self.categorical_columns:
            query = """select {col}, count({col}) as frequency_ from {schema}.{table} group by {col} order by {col}""".format(
                col = col,
                schema = self.table_schema,
                table = self.table_name)

            df = self.redshift.execute(query)
            

            dirname = 'tmp/'
            fname = self.table_schema + '_' + self.table_name + '_' + col + '_histogram__' + uid() + '.png'
            fpath = path.abspath(dirname + fname)

            plt.figure()
            plot = seaborn.barplot(x=df['frequency_'], y=df[col], orient='h', color='blue').get_figure()
            plot.savefig(fpath, dpi=600 ,bbox_inches='tight')

            histograms.append((col,
            safe_html('<a href="' + fpath + '" target="_blank"><img src="' + fpath + '" height=100 width=250 /></a>')
            ))

        query_parts = []

        stat_cols = list(filter(lambda c: c not in self.categorical_columns, self.numeric_columns))
        if stat_cols:
            for col in stat_cols:
                _, bins = np.histogram([statistics.loc[col,'min'], statistics.loc[col,'max']*1.01], bins = self.config.histogram_bins)
                for i in range(len(bins)-1):
                    min_, max_ = str(round(bins[i],6)), str(round(bins[i+1],6))
                    query_parts.append('sum(case when {col} >= {min_} and {col} < {max_} then 1 else 0 end) as {col}__{min__}__{max__}'
                                        .format(col=col, min_=min_, max_=max_, max__=max_.replace('.','_'), min__=min_.replace('.','_')))

            query = 'select ' + ', '.join(query_parts) + ' from {schema}.{table}'.format(schema=self.table_schema, table=self.table_name)
            hist_vals = self.redshift.execute(query)

            hist_vals = hist_vals.transpose().reset_index()
            hist_vals['col'] = hist_vals['index'].str.split('__').str[0]
            hist_vals['lb'] = hist_vals['index'].str.split('__').str[1].str.replace('_','.').astype(float)
            hist_vals['ub'] = hist_vals['index'].str.split('__').str[2].str.replace('_','.').astype(float)
            hist_vals['val'] = hist_vals[0]

            for col in stat_cols:
                hist = hist_vals.loc[hist_vals['col']==col, 'val'].values
                bins = np.array(hist_vals.loc[hist_vals['col']==col, 'lb'].tolist() + [hist_vals.loc[hist_vals['col']==col, 'ub'].tolist()[-1]])
                width = np.diff(bins)*0.95
                center = (bins[:-1] + bins[1:])/2

                dirname = 'tmp/'
                fname = self.table_schema + '_' + self.table_name + '_' + col + '_histogram__' + uid() + '.png'
                fpath = path.abspath(dirname + fname)
                
                plt.figure()
                fig, ax = plt.subplots(figsize=(10,4))
                ax.bar(center, hist, width=width)
                ax.set_xticks(bins)
                plt.xticks(rotation=60)
                plt.xlabel(col)
                plt.ylabel('frequency')
                plt.savefig(fpath, bbox_inches='tight', dpi=600)

                histograms.append((col,
                safe_html('<a href="' + fpath + '" target="_blank"><img src="' + fpath + '" height=100 width=250 /></a>')
                ))        

        df = pd.DataFrame(histograms, columns=['column','histogram']).set_index('column')
        del df.index.name

        return df


    def get_html(self):
        """
        Returns html of `profiling_dfs`
        """
        html_str = ''

        for title, df in self.profiling_dfs:
            html_str += '<h2>'+title+'</h2>'
            buf = StringIO()
            df.to_html(buf, justify='center', index=True, classes=['table','table-striped'])
            html_str += buf.getvalue() + '<br>'

        return valid_html(html_str)


    def get_json(self):
        """
        Returns json of `profiling_dfs`
        """
        json_obj = []

        for title, df in self.profiling_dfs:
            json_obj.append({'title':title, 'content':df.fillna('').to_dict(orient='records'), 'columns':list(df.columns)})

        return json_obj