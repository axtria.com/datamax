from services.catalog.business_glossary import ImportGlossary
from unittest.mock import patch
from unittest import mock
import pandas as pd
import numpy as np

valid_input = [{'Category': 'Tag', 'Value': 'DDD', 'Sub Category': 'Business Tag', 'Status': 'true','Description': 'hello','__fail_reason__':'','__status__':'pass'},
               {'Category': 'Tag', 'Value': 'Reference', 'Sub Category': 'Logical Tag', 'Status': 'true','Description': 'hello','__fail_reason__':'','__status__':'pass'},
               {'Category': 'Tag', 'Value': 'xxx', 'Sub Category': 'Others', 'Status': 'true','Description': 'hello','__fail_reason__':'','__status__':'pass'},
               {'Category': 'KPI', 'Value': 'kuch bhi', 'Sub Category': 'Quality', 'Status': 'true','Description': 'hello','__fail_reason__':'','__status__':'pass'},
               {'Category': 'KPI', 'Value': 'Customer', 'Sub Category': 'Others', 'Status': 'true','Description': 'hello','__fail_reason__':'','__status__':'pass'}]
invalid_input = [
    {'Category': 'Tag', 'Value': 'DDD', 'Sub Category': 'Business Tag', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'Tag', 'Value': 'Reference', 'Sub Category': 'Logical Tag', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'Tag', 'Value': 'xxx', 'Sub Category': 'Others', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'KPI', 'Value': 'kuch bhi', 'Sub Category': 'Quality', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'KPI', 'Value': 'Customer', 'Sub Category': 'Others', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'Abbreviations', 'Value': 'Test', 'Sub Category': '', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'Tag', 'Value': 'DDD', 'Sub Category': 'BusinessTag', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'Tag', 'Value': 'Reference', 'Sub Category': 'Logical Ta', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'Tag', 'Value': 'xxx', 'Sub Category': 'Other', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'KPI', 'Value': 'kuch bhi', 'Sub Category': 'uality', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'KPI', 'Value': 'Customer', 'Sub Category': 'Othrs', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'Abbreviations', 'Value': 'Test', 'Sub Category': '', 'Status': 'TRUE', 'Description': 'hello'},
    {'Category': 'Tag', 'Value': 'DDD', 'Sub Category': 'Business Tag', 'Status': 'TR', 'Description': 'hello'},
    {'Category': 'Tag', 'Value': 'Reference', 'Sub Category': 'Logical Tag', 'Status': 'TUE', 'Description': 'hello'},
    {'Category': 'Tag', 'Value': 'xxx', 'Sub Category': 'Others', 'Status': '0', 'Description': 'hello'},
    {'Category': 'KPI', 'Value': 'kuch bhi', 'Sub Category': 'Quality', 'Status': '1', 'Description': 'hello'},
    {'Category': 'KPI', 'Value': 'Customer', 'Sub Category': 'Others', 'Status': 'Fal', 'Description': 'hello'},
    {'Category': 'Abbreviations', 'Value': 'Test', 'Sub Category': '', 'Status': 'TRUE', 'Description': 'hello'}]
Noe4j_data = [
    {'category': ['KPI'], 'created_on': '2020-05-06 18:59:57', 'creator_user_name': '', 'description': 'ddddd',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'true', 'sub_category': 'Quality',
     'value': 'SM2'},
    {'category': ['Tag'], 'created_on': '2020-05-01 16:44:20', 'creator_user_name': 'system',
     'description': 'first description', 'modified_on': '2020-05-06 19:54:26',
     'modifier_user_name': '', 'status': 'true', 'sub_category': 'Quality', 'value': 'Demand1'},
    {'category': ['Tag'], 'created_on': '2020-04-30 20:21:12', 'creator_user_name': '', 'description': 'bekar desc',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'true', 'sub_category': 'Logical Tag',
     'value': 'kuch bhi'},
    {'category': ['xxx'], 'created_on': '2020-04-30 18:36:05', 'creator_user_name': '', 'description': 'ajay',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'FALSE', 'sub_category': 'new test',
     'value': 'New tag'},
    {'category': ['Tag'], 'created_on': '2020-04-30 20:11:04', 'creator_user_name': '', 'description': 'dasing desc',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'false', 'sub_category': 'Business Tag',
     'value': 'xxx'},
    {'category': ['Akshay'], 'created_on': '2020-04-30 18:36:04', 'creator_user_name': '', 'description': 'kunal',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'None', 'sub_category': 'new test',
     'value': 'tag3'},
    {'category': ['Akshay'], 'created_on': '2020-04-30 18:36:04', 'creator_user_name': '', 'description': 'kunal',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'None', 'sub_category': 'new test',
     'value': 'tag2'},
    {'category': ['Akshay'], 'created_on': '2020-04-30 18:36:04', 'creator_user_name': '', 'description': 'kunal',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'None', 'sub_category': 'new test',
     'value': 'tag1'},
    {'category': ['Akshay'], 'created_on': '2020-04-30 18:36:05', 'creator_user_name': '', 'description': '',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': '', 'sub_category': 'xxx',
     'value': 'New tag'},
    {'category': ['Tag'], 'created_on': '2020-04-30 20:11:04', 'creator_user_name': '', 'description': 'dasing desc',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'false', 'sub_category': 'Logical Tag',
     'value': 'Reference'},
    {'category': ['Tag'], 'created_on': '2020-04-30 18:36:04', 'creator_user_name': '', 'description': 'umesh',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'FALSE',
     'sub_category': 'logical Not tag', 'value': 'reference'},
    {'category': ['KPI'], 'created_on': '2020-04-30 18:36:04', 'creator_user_name': '', 'description': 'umesh',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'FALSE', 'sub_category': 'business Tag',
     'value': 'Xponent'},
    {'category': ['KPI'], 'created_on': '2020-04-30 20:11:05', 'creator_user_name': '', 'description': 'dasing desc',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'false', 'sub_category': 'Quality',
     'value': 'DDD'},
    {'category': ['Tag'], 'created_on': '2020-05-06 12:59:35', 'creator_user_name': '',
     'description': 'Tag-Business-Demand', 'modified_on': '2020-05-06 19:54:26',
     'modifier_user_name': '', 'status': 'true', 'sub_category': 'Business Tag', 'value': 'Demand'},
    {'category': ['Tag'], 'created_on': '2020-04-30 18:36:04', 'creator_user_name': '', 'description': 'umesh',
     'modified_on': '2020-05-06 19:54:26', 'modifier_user_name': '', 'status': 'FALSE', 'sub_category': 'test',
     'value': 'Deamnd'},
    {'category': ['Tag'], 'created_on': '2020-05-06 18:59:57', 'creator_user_name': '',
     'description': 'Tag-Business-Sales', 'modified_on': '2020-05-06 19:54:26',
     'modifier_user_name': '', 'status': 'true', 'sub_category': 'Logical Tag', 'value': 'Sales'}]


@patch('services.catalog.business_glossary.pd.read_csv', mock.MagicMock(return_value=pd.DataFrame(invalid_input)))
@patch('pandas.DataFrame.to_csv',mock.MagicMock(return_value=None))
@patch('vendor.neo4j.Neo4j.run_query', mock.MagicMock(return_value=pd.DataFrame(Noe4j_data)))
def test_import_glossary():
    service = ImportGlossary(session=None, file=None)
    service.execute()
    y = service.valid_imprt.drop('match_checker',axis=1)
    x = pd.DataFrame(valid_input)
    assert np.array_equal(x.values,y.values)
