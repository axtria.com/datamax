#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Edit Description"""

import pandas as pd 
from services import Service
from vendor.neo4j import Neo4j 

class EditAttribute(Service):
	"""Edit attribute in neo4j"""
	def __init__(self, session, node_id, attribute_key, value, **kwargs):
		super().__init__()

		self.neo4j = Neo4j()

		self.node_id = int(node_id)
		self.value = value
		self.attribute_key = attribute_key

	def execute(self):

		update_dict = {self.attribute_key: self.value}

		node = self.neo4j.get_node_by_id(self.node_id)

		self.neo4j.update_node_attributes(node, **update_dict)

		self.add_response('status','success')








