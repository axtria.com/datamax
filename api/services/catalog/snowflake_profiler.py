import pandas as pd
import numpy as np
import json

from vendor.snowflake import Snowflake
from vendor.neo4j import Neo4j

from services import Service

class SnowflakeProfiler(Service):
    """
    Profiles/calculates basic statistics of a table present in redshift.
    """
    def __init__(self, session, node_id, statistics='false', **kwargs):
        """

        """
        super().__init__()

        node = Neo4j().get_node_by_id(node_id)
        self.table_schema = node['schema']
        self.table_name = node['name']
        self.snowflake = Snowflake.initialize(node['connector_id'], session)

        self.statistics = True if statistics.strip().lower() == 'true' else False

        self.all_columns = None
        self.numeric_columns = None
        self.categorical_columns = None
        self.profiling_dfs = []
        self.snowflake_numeric_dtypes = ['NUMBER','DECIMAL','NUMERIC','INT', 'INTEGER', 'BIGINT', 'SMALLINT', 'TINYINT', 'BYTEINT',
                                            'FLOAT', 'FLOAT4', 'FLOAT8', 'DOUBLE', 'DOUBLE PRECISION', 'REAL']
        self.config = {

            'table_metadata':True,
            'column_metadata':True,
            'sample':True,
            'sample_size':10,
            'statistics': {

                'sum': False,
                'min': True,
                'max': True,
                'avg': True,
                'stddev': True,
                'null_count': True,
                'unique_count': True,
                'percentiles': [25,50,75]
            }
        }

    def execute(self, return_path=False):
        """
        """

        # Fetching table metadata. It it required even if it is not required in final response.
        table_metadata_query = f"select * from information_schema.tables where table_schema='{self.table_schema}' and table_name='{self.table_name}'"
        table_metadata = self.snowflake.execute(table_metadata_query)
        table_metadata = table_metadata.dropna(axis=1)
        table_metadata.columns = [c.lower() for c in table_metadata.columns]
        if self.config['table_metadata']:
            self.profiling_dfs.append(('Table Metadata',table_metadata))

        # Fetching column metadata. It it required even if it is not required in final response.
        column_metadata_query = f"select * from information_schema.columns where table_schema='{self.table_schema}' and table_name='{self.table_name}'"
        column_metadata = self.snowflake.execute(column_metadata_query)
        column_metadata = column_metadata.dropna(axis=1, how='all')
        column_metadata.columns = [c.lower() for c in column_metadata.columns]
        if self.config['column_metadata']:
            self.profiling_dfs.append(('Column Metadata',column_metadata))

        # Fetching sample data
        if self.config['sample']:            
            sample_data_query = f"select * from {self.table_schema}.{self.table_name} limit {self.config['sample_size']}"
            sample_data = self.snowflake.execute(sample_data_query)
            self.profiling_dfs.append(('Sample',sample_data))

        # Fetching stats
        if self.config['statistics'] and self.statistics:
            try:
                self.all_columns = column_metadata['column_name']
                self.numeric_columns = column_metadata.loc[column_metadata['data_type'].isin(self.snowflake_numeric_dtypes),'column_name']     
                statistics = self.basic_statistics()       
                self.profiling_dfs.append(('Statistics',statistics))
            except:
                self.log.exception('Some error occured while calculating stats.', exc_info=True)
                self.add_response('error','Some error occured while calculating stats.')

        df_json = self.get_json()
        self.add_response('response',df_json)


    def basic_statistics(self):
        """
        """

        query_parts = ['1 as I']
        
        if self.config['statistics']['sum']:
            query_sum = ('sum(' + self.numeric_columns +') as '+ self.numeric_columns + '__sum').tolist()
            query_parts += query_sum

        if self.config['statistics']['min']:
            query_min = ('min(' + self.numeric_columns +') as '+ self.numeric_columns + '__min').tolist()
            query_parts += query_min

        if self.config['statistics']['max'] or self.config.histograms:
            query_max = ('max(' + self.numeric_columns +') as '+ self.numeric_columns + '__max').tolist()
            query_parts += query_max

        if self.config['statistics']['avg']:
            query_avg = ('avg(' + self.numeric_columns +') as '+ self.numeric_columns + '__mean').tolist()
            query_parts += query_avg

        if self.config['statistics']['stddev']:
            query_std = ('stddev(' + self.numeric_columns +') as '+ self.numeric_columns + '__standard_dev').tolist()
            query_parts += query_std

        if self.config['statistics']['null_count']:
            query_null_count = ('sum(case when '+ self.all_columns +' is null then 1 else 0 end) as ' + self.all_columns +'__null_count').tolist()
            query_parts += query_null_count

        if self.config['statistics']['unique_count']:
            query_unique_count = ('count(distinct '+ self.all_columns +') as ' + self.all_columns +'__unique_values').tolist()
            query_parts += query_unique_count

        query = """select {parts} from {schema}.{table}""".format(
            parts = ', '.join(query_parts),
            schema = self.table_schema,
            table = self.table_name)

        df = self.snowflake.execute(query)

        if self.config['statistics']['percentiles'] and not self.numeric_columns.empty:
            percentiles = self.get_percentiles(percentiles=self.config['statistics']['percentiles'])
            df = pd.concat([df,percentiles], axis=1)

        df = pd.wide_to_long(df,
            stubnames = self.all_columns,
            i = 'I',
            j = 'profile',
            sep = '__',
            suffix = '\\w+').reset_index()

        del df['I']

        df = df.set_index('profile').transpose()
        del df.columns.name
        df.columns = [c.lower() for c in df.columns]

        try:
            df['unique_values'] = df['unique_values'].astype(int)
            df['null_count'] = df['null_count'].astype(int)
        except:pass

        df.reset_index(inplace=True)
        df.rename(columns={'index':'column'}, inplace=True)

        return df

    def get_percentiles(self, percentiles=[25,50,75]):
        """
        Calculates 25 percentiles, 50 percentile (median), 75 percentile
        """
        query_parts = []
        for percentile in percentiles:
            parts = ('percentile_cont('+str(percentile/100)+') within group (order by '+self.numeric_columns+') as '+self.numeric_columns+'__'+str(percentile)+'_percentile').tolist()
            query_parts += parts

        query = """select distinct {parts} from {schema}.{table}""".format(
            parts = ', '.join(query_parts),
            schema = self.table_schema,
            table = self.table_name)

        df = self.snowflake.execute(query)
        return df

    def get_json(self):
        """
        Returns json of `profiling_dfs`
        """
        json_obj = []

        for title, df in self.profiling_dfs:
            json_obj.append({'title':title, 'content':df.fillna('').to_dict(orient='records'), 'columns':list(df.columns)})

        return json_obj