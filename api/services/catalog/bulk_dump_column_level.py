#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Bulk Dump Column Level"""

import pandas as pd 
from services import Service
from vendor.neo4j import Neo4j 
from datetime import datetime
from flask import send_file
import json

class BulkDumpColumnLevel(Service):
	"""Bulk Dump catalog"""
	def __init__(self, session, layers=None, limit_records=1000000, **kwargs):
		super().__init__()

		self.neo4j = Neo4j()
		self.layers = layers
		self.limit_records = int(limit_records)

	def execute(self):

		layer_filter_condition = ''

		if self.layers:
			layer_ids = json.dumps(self.layers.split(','))
			layer_filter_condition = f'and layer.layer_id in {layer_ids}'

		query = f'''match((layer)-[*1..]->(object)-->(column)) 
					where ((layer:S3Bucket and object:S3File and column:S3FileDetectedColumn)
					or (layer:SnowflakeSchema and object:SnowflakeTable and column:SnowflakeColumn)) 
					{layer_filter_condition} 
					return 
					id(object) as object_id,
					id(column) as Id,
					object.name as Name, 
					layer.layer as Layer,
					column.name as Column_Name,
					column.logi_name as Logical_Name, 
					column.tech_desc as Technical_Description, 
					column.buss_desc as Business_Description, 
					column.logi_desc as Logical_Description
					limit {self.limit_records}'''

		dump = self.neo4j.run_query(query)

		object_mapping = self.neo4j.run_query('match((object:Object)-->(file:S3File)) return id(file) as object_id, object.name as Onboarded_Object')

		if not object_mapping.empty:
			dump = dump.merge(object_mapping, how='left', on='object_id')
		else:
			dump['Onboarded_Object'] = ''

		ordered_cols = ['Id', 'Name', 'Onboarded_Object', 'Layer', 'Column_Name','Logical_Name',
						'Technical_Description', 'Business_Description', 'Logical_Description']

		dump = dump[ordered_cols]
		dump = dump.fillna('')

		ts = datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')
		fname = f'dumps/{ts}.csv'

		dump.to_csv(fname, index=None)

		self.add_response('file_path',fname)
		self.add_response('file_name',f'catalog_dump_{ts}.csv')








