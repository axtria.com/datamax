from datetime import datetime

def uid():
    return datetime.now().strftime("%d_%m_%Y__%H_%M_%S_%f")

def safe_html(s):
    return s.replace('<','__LeftAngleBracket__').replace('>','__RightAngleBracket__')

def valid_html(s):
    return s.replace('__LeftAngleBracket__','<').replace('__RightAngleBracket__','>').replace('nan','').replace('NaN','')