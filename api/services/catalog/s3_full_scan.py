#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - S3 full scan"""

import pandas as pd

from services import Service
from services.catalog.s3_metadata_register import S3MetadataRegister
from utils.helpers import thread
from vendor.s3 import S3


class S3FullScan(Service):
    """Fully scans s3 and add/update its metadata to neo4j db if it's allowed by s3_filter_config"""

    def __init__(self, session, s3_connector_id, bucketName, **kwargs):
        super().__init__()

        self.session = session

        self.event_type = 'FullScan'
        self.awsRegion = None
        self.bucketName = bucketName
        self.connector_id = s3_connector_id

        self.s3 = S3.initialize(s3_connector_id, session)
        self.add_response('message', 'Request Acknowledged')

    @thread
    def execute(self, ContinuationToken=None):

        if ContinuationToken:
            response = self.s3.client.list_objects_v2(Bucket=self.bucketName, ContinuationToken=ContinuationToken)
        else:
            response = self.s3.client.list_objects_v2(Bucket=self.bucketName)

        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            self.awsRegion = response['ResponseMetadata']['HTTPHeaders']['x-amz-bucket-region']

            if response['Contents']:
                metadata = pd.DataFrame(response['Contents'])[['Key', 'LastModified', 'Size']]

                for i, obj in metadata.iterrows():
                    registration_service = S3MetadataRegister(self.session,
                                                              self.s3,
                                                              self.event_type,
                                                              self.awsRegion,
                                                              self.bucketName,
                                                              obj['Key'],
                                                              obj['Size'],
                                                              obj['LastModified'].strftime('%Y-%m-%d %H:%M:%S'),
                                                              self.connector_id)

                    registration_service.execute()
                    self.add_response(obj['Key'], registration_service.response)

            if response['IsTruncated'] == True:
                self.execute(ContinuationToken=response['NextContinuationToken'])
