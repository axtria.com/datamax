#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Add Comment and rating"""

from services import Service
from vendor.neo4j import Neo4j 
from datetime import datetime

class AddCommentAndRating(Service):
	"""Add comment and rating in neo4j"""
	def __init__(self, session, node_id, rating, user, comment='', **kwargs):
		super().__init__()

		self.neo4j = Neo4j()

		self.node_id = node_id
		self.comment = comment
		self.rating = int(rating)
		self.user = user

	def execute(self):

		node = self.neo4j.get_node_by_id(self.node_id)

		self._update(node)

		self.add_response('status','success')


	def _update(self, node):

		user_comments = node.get('user_comments').split(';') if 'user_comments' in node.keys() else []
		user_ratings = node.get('user_ratings').split(';') if 'user_ratings' in node.keys() else []
		rating_users = node.get('rating_users').split(';') if 'rating_users' in node.keys() else []
		rating_timestamps = node.get('rating_timestamps').split(';') if 'rating_timestamps' in node.keys() else []

		if self.user in rating_users:

			index = rating_users.index(self.user)

			user_comments.pop(index)
			user_ratings.pop(index)
			rating_users.pop(index)
			rating_timestamps.pop(index)

		user_comments.append(self.comment)
		user_ratings.append(str(self.rating))
		rating_users.append(self.user)
		rating_timestamps.append(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

		self.neo4j.update_node_attributes(node, user_comments = ';'.join(user_comments), user_ratings = ';'.join(user_ratings), rating_users = ';'.join(rating_users), rating_timestamps=';'.join(rating_timestamps))










