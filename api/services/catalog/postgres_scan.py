from sqlalchemy.orm import Session
from services import Service,notification
from utils.catalog_builder import insert_object_metadata_to_neo4j, get_object_abslt_path, \
    get_object_metadata
from utils.exceptions import NoDataAvailable
from vendor.appdb import AppDb
from vendor.neo4j import Neo4j
from datetime import datetime
import numpy as np

class PostgresFullScan(Service):
    def __init__(self,session, s3_connector_id, **kwargs):
        super().__init__()
        self.session = session
        self.app_db = AppDb()
        self.neo4j = Neo4j()
        self.s3_connector_id = s3_connector_id

    def execute(self):
        # create nodes in neo4j for objects with unique key as URI id.
        obj_records = self.app_db.execute(f"""select
            DLM.data_lyr_name as layer_name,
            DLM.data_lyr_id as layer_id,
            OM.obj_nm as object_name,
            OM.obj_typ as object_type,
            OM.obj_desc as desc,
            OM.has_header as header,
            OM.obj_extn as extension,
            OM.obj_encoding as encoding,
            OM.freq as frequency,
            OM.cntry_cd as country_code,
            OM.lang_cd as language_code,
            OM.obj_uri_id as uri,
            OM.version as version,
            OM.obj_id as obj_id,
            OM.busns_un_cd as busns_un_cd,
            ATRM.attr_nm as column_name,
            ATRM.attr_data_typ as column_datatype,
            ATRM.col_loc as column_ordinal_position,
            ATRM.attr_desc as column_remarks,
            ATRM.attr_id as attr_id
            from system_config.object_meta as OM
            join system_config.data_layer_meta as DLM on DLM.data_lyr_id = OM.src_data_layer_id
            join system_config.connection_meta as CM on CM.id = DLM.conn_id
            join system_config.attr_meta as ATRM on OM.obj_id = ATRM.obj_id
            where DLM.lyr_type = 's3' and CM.id = {self.s3_connector_id}""")
        for _, row in obj_records.iterrows():
            neo4j_query = f"""merge(t:Object{{uri:"{row.uri}"}}) set t += {{deleted:false, name: "{row.object_name}",layer:"{row.layer_name}",
                                                                object_type :"{row.object_type}",desc:"{row.desc}",
                                                                header:"{row.header}",extension:"{row.extension}",
                                                                encoding:"{row.encoding}",country_code:"{row.country_code}",
                                                                language_code:"{row.language_code}",version:"{row.version}",
                                                                business_unit_code:"{row.busns_un_cd}"}}
                          merge(c:ObjectColumn{{parent_uri:"{row.uri}", attr_id:"{row.attr_id}"}}) set c +=  {{deleted:false, name: "{row.column_name}",datatype:"{row.column_datatype}"}}
                          merge((t)-[:HAS]->(c)) """

            _ = self.neo4j.run_query(neo4j_query)

        # attach URI id, file inst id and other properties to already created s3 nodes
        # by searching using s3 key
        # also, create a link between obj node and s3 node basis of
        # file inst id
        file_records = self.app_db.execute(f"""
        select
            DLM.data_lyr_name as layer_name,
            DLM.data_lyr_id as layer_id,
            DLM.s3_path as bucket_name,
            OM.obj_nm as object_name,
            OM.obj_uri_id as uri,
            OM.version as version,
            OM.obj_id as obj_id,
            concat(ODLM.abslt_path,'/',FIP.part_nm) as object_key
            from system_config.object_meta as OM
            join system_config.data_layer_meta as DLM on DLM.data_lyr_id = OM.src_data_layer_id
            join system_config.connection_meta as CM on CM.id = DLM.conn_id
            join system_config.file_instnc as FI on FI.obj_id = OM.obj_id 
            join system_config.object_data_layer_meta as ODLM on ODLM.obj_id = OM.obj_id 
            join system_config.file_instnc_part as FIP on FIP.file_inst_id = FI.file_inst_id 
            where DLM.lyr_type = 's3' and CM.id = {self.s3_connector_id}""")

        for _, row in file_records.iterrows():
            self.neo4j.run_query(f""" 
                                MATCH (a: Object),(b: S3File) 
                                WHERE a.uri = "{row.uri}" 
                                AND b.objectKey = "{row.object_key}" 
                                AND b.bucketName = "{row.bucket_name.strip('s3://')}"
                                MERGE (a)-[r:HASFILE]->(b)
                                set b +=  {{uri:"{row.uri}", version: "{row.version}"}}
                                RETURN r""")

        self.add_response('status', 'Success')
        self.add_response('message', 'Object data mapped')

class PostgresFullScanV2(Service):
    def __init__(self,session, s3_layer_id, **kwargs):
        super().__init__()
        self.session = session
        self.app_db = AppDb()
        self.neo4j = Neo4j()
        self.layer_id = s3_layer_id
        self.params = kwargs

    def send_email(self,email_data):
        email_string = ''
        for single_email_content in email_data:
            email_string += "" + single_email_content
        email_string = "<HTML><BODY>" + email_string + "</BODY></HTML>"
        # send email to uesrgroups
        service = notification.SendNotification(self.session, name='onboard notification', description=email_string,
                                                type='HTML', **self.params)
        service.execute()

    def execute(self):

        latest_ts = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f 0000')
        self.log.debug(f'latest_ts = {latest_ts}')

        # create nodes in neo4j for objects with unique key as URI id.
        obj_records = self.app_db.execute(get_object_metadata(layer_id=self.layer_id))
        if len(obj_records) == 0:
            raise NoDataAvailable
        email_data = set({})

        for _, row in obj_records.iterrows():
            neo4j_query = insert_object_metadata_to_neo4j(row,latest_ts,self.params)
            _ = self.neo4j.run_query(neo4j_query)

            email_data.add("""<p><b>Object id</b> : """+str(row.uri)+""",<b>name</b> : """+str(row.object_name)+""",<b>On layer</b> : """+str(row.layer_name)+"""</p>""")

        self.send_email(email_data)

        # attach URI id, file inst id and other properties to already created s3 nodes
        # by searching using s3 key
        # also, create a link between obj node and s3 node basis of
        # file inst id
        file_records = self.app_db.execute(get_object_abslt_path(layer_id = self.layer_id))

        for _, row in file_records.iterrows():
            self.neo4j.run_query(f""" 
                                MATCH (a: Object),(b: S3File) 
                                WHERE a.uri = "{row.uri}" 
                                AND b.objectKey = "{row.object_key}" 
                                AND b.bucketName = "{row.bucket_name.strip('s3://')}"
                                MERGE (a)-[r:HASFILE]->(b)
                                set b +=  {{uri:"{row.uri}", version: "{row.version}"}}
                                RETURN r""")

        self.add_response('status', 'Success')
        self.add_response('message', 'Object data mapped')

class PostgresIncrementalScan(Service):
    def __init__(self,session, obj_id, **kwargs):
        super().__init__()
        self.session = session
        self.app_db = AppDb()
        self.neo4j = Neo4j()
        self.obj_id = obj_id
        self.params = kwargs

    def send_email(self,email_data):
        email_string = ''
        for single_email_content in email_data:
            email_string += "" + single_email_content
        email_string = "<HTML><BODY>" + email_string + "</BODY></HTML>"
        # send email to uesrgroups
        service = notification.SendNotification(self.session, name='onboard notification', description=email_string,
                                                type='HTML', **self.params)
        service.execute()

    def execute(self):

        latest_ts = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f 0000')
        self.log.debug(f'latest_ts = {latest_ts}')

        # create nodes in neo4j for objects with unique key as URI id.
        obj_records = self.app_db.execute(get_object_metadata(obj_id=self.obj_id))
        if len(obj_records) == 0:
            raise NoDataAvailable
        email_data = set({})
        for _, row in obj_records.iterrows():
            neo4j_query = insert_object_metadata_to_neo4j(row,latest_ts,self.params)
            _ = self.neo4j.run_query(neo4j_query)

            email_data.add("""<p><b>Object id</b> : """+str(row.uri)+""",<b>name</b> : """+str(row.object_name)+""",<b>On layer</b> : """+str(row.layer_name)+"""</p>""")

        self.send_email(email_data)

        # attach URI id, file inst id and other properties to already created s3 nodes
        # by searching using s3 key
        # also, create a link between obj node and s3 node basis of
        # file inst id
        file_records = self.app_db.execute(get_object_abslt_path(obj_id=self.obj_id))

        for _, row in file_records.iterrows():
            self.neo4j.run_query(f""" 
                                MATCH (a: Object),(b: S3File) 
                                WHERE a.uri = "{row.uri}" 
                                AND b.objectKey = "{row.object_key}" 
                                AND b.bucketName = "{row.bucket_name.strip('s3://')}"
                                MERGE (a)-[r:HASFILE]->(b)
                                set b +=  {{uri:"{row.uri}", version: "{row.version}"}}
                                RETURN r""")

        self.add_response('status', 'Success')
        self.add_response('message', 'Object data mapped')