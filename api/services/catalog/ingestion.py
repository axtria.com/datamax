#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Ingestion"""

import pandas as pd 
from services import Service
import json
from vendor.neo4j import Neo4j 
from vendor.s3 import S3
from services.catalog.s3_metadata_register import S3MetadataRegister


class Ingestion(Service):
	"""Ingestion"""
	def __init__(self, session, s3_connector_id, bucket, object_key, json_string, **kwargs):
		super().__init__()

		self.neo4j = Neo4j()

		self.session = session
		self.s3 = S3.initialize(s3_connector_id, session)
		self.bucket = bucket
		self.object_key = object_key
		self.json_string = json_string

	def execute(self):

		params = json.loads(self.json_string)

		files = self.neo4j.get_nodes('S3File', return_objects = True, objectKey=self.object_key, bucketName = self.bucket)

		if len(files) == 1:

			self.log('file already present in catalog')
			file = files[0]

		else:

			self.log('file not present in catalog')

			response = self.s3.client.list_objects_v2(Bucket=self.bucket, Prefix=self.object_key)

			if response['ResponseMetadata']['HTTPStatusCode'] == 200:
				awsRegion = response['ResponseMetadata']['HTTPHeaders']['x-amz-bucket-region']

			if response['Contents']:
				obj = response['Contents'][0]

			registration_service = S3MetadataRegister(self.session,
														self.s3,
														'ObjectCreated',
														awsRegion,
														self.bucket,
														obj['Key'],
														obj['Size'],
														obj['LastModified'].strftime('%Y-%m-%d %H:%M:%S'),
														True)	

			registration_service.execute()
			file = self.neo4j.get_nodes('S3File', return_objects = True, objectKey = self.object_key, bucketName = self.bucket)[0]

		file_update_dict = {i:j for i,j in params.items() if i != 'columns'}
		self.neo4j.update_node_attributes(file, **file_update_dict)

		try:
			for col_node in self.neo4j.get_nodes('S3FileDetectedColumn', return_objects = True, objectKey = self.object_key, bucketName = self.bucket):
				name = params['columns'][col_node['ordinalPosition']]['name']
				data_type = params['columns'][col_node['ordinalPosition']]['data_type']
				self.neo4j.update_node_attributes(col_node, name = name, dataType = data_type)
		except:pass








