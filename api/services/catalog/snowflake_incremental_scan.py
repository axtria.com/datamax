#   datamax/api/vendor/snowflake_incremental_scan.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
'''Datamax Catalog - Snowflake full scan'''

from datetime import datetime

import pandas as pd
from numpy import random
from sqlalchemy import Sequence

from services import Service, notification
from services.api_exchange import ApiExchange
from utils.exceptions import DataLayerNotFound
from utils.helpers import thread
from vendor.appdb import AppDb
from vendor.neo4j import Neo4j
from vendor.snowflake import Snowflake
from sqlalchemy.orm import Session
from config import SF_INC_SCAN_INTERVAL

neo4j = Neo4j()
app_db = AppDb()

class SnowflakeIncrementalScanV2(Service):
    def __init__(self, session, sf_connector_id, **kwargs):
        """Register metadata of Snowflake to neo4j db"""
        super().__init__()
        self.session = session
        self.app_db = AppDb()
        self.params = kwargs
        self.sf_connector_id = int(sf_connector_id)
        self.sf = Snowflake.initialize(self.sf_connector_id, session)
        self.sf.test()
        self.add_response('message', 'Request Acknowledged')
        self.sf_records = pd.DataFrame()
        self.pg_records = pd.DataFrame()
        self.layer_df = pd.DataFrame()

    def execute(self):
        self.pg_records = self.app_db.execute(f"""select
            DLM.schema_name as table_schema,
            DLM.data_lyr_name as layer_name,
            DLM.data_lyr_id as layer_id,
            OM.obj_nm as table_name,
            'BASE TABLE' as table_type,
            '' as table_remarks,
            ATRM.attr_nm as column_name,
            ATRm.attr_data_typ as column_datatype,
            ATRM.col_loc as column_ordinal_position,
            ATRM.attr_desc as column_remarks,
            OM.obj_uri_id as uri,
            OM.version as version,
            OM.obj_id as obj_id,
            OM.audit_insrt_dt as created_date,
            OM.audit_updt_dt as modified_date
            from system_config.object_meta as OM
            join system_config.data_layer_meta as DLM on DLM.data_lyr_id = OM.src_data_layer_id
            join system_config.connection_meta as CM on CM.id = DLM.conn_id
            join system_config.attr_meta as ATRM on OM.obj_uri_id = ATRM.obj_uri_id
            where DLM.schema_name is not null and DLM.lyr_type = 'sf' and CM.id = {self.sf.connection_id}""")

        self.layer_df = self.app_db.execute(f"""select distinct schema_name as table_schema, data_lyr_id as layer_id, data_lyr_name as layer_name from 
                                            system_config.data_layer_meta 
                                            where lyr_type = 'sf' and conn_id = {self.sf.connection_id}""")
        if not self.layer_df.get('table_schema')[0]:
            raise DataLayerNotFound

        schemas_tuple = "('" + "','".join(self.layer_df['table_schema']) + "')"
        layer_id_list = "['" + "','".join(self.layer_df['layer_id'].astype(str)) + "']"

        last_updated_ts = neo4j.run_query(f"""match(n:SnowflakeSchema) where n.layer_id in {layer_id_list} 
                                                return n.layer_id as layer_id, n.last_updated_ts as last_updated_ts""")
        try:
            self.log.debug(last_updated_ts)
            last_updated_ts = last_updated_ts.dropna()['last_updated_ts'].max()
            self.log.debug(f'last_updated_ts = {last_updated_ts}')
        except:
            raise Exception("No last updated timestamp found")

        sf_query = 'select ' \
                   't.table_schema as "table_schema", ' \
                   't.table_name as "table_name", ' \
                   't.table_type as "table_type", ' \
                   't.comment as "table_remarks", ' \
                   'c.column_name as "column_name", ' \
                   'c.data_type as "column_datatype", ' \
                   't.last_altered as "modified_date", ' \
                   't.CREATED as "created_date", ' \
                   'c.ordinal_position as "column_ordinal_position", ' \
                   'c.comment as "column_remarks" ' \
                   'from ' \
                   f'"{self.sf.database}"."INFORMATION_SCHEMA"."TABLES" t join "{self.sf.database}"."INFORMATION_SCHEMA"."COLUMNS" c ' \
                   'on ' \
                   '(t.table_name = c.table_name and t.table_schema = c.table_schema) ' \
                   f"where t.table_schema in {schemas_tuple} and t.last_altered > '{last_updated_ts}'::timestamp_tz;"

        latest_ts = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f 0000')
        self.log.debug(f'latest_ts = {latest_ts}')

        self.sf_records = self.sf.execute(sf_query)
        self.log.debug(f'record count = {len(self.sf_records)}')
        if self.sf_records.empty:
            return

        self.resolve_layer_name()
        self.resolve_URI()
        self.resolve_attr_id()
        to_be_inserted = self.sf_records[~self.sf_records['uri'].isin(self.get_pg_uris())]
        to_be_inserted["audit_batch_id"] = "UI00100"
        to_be_inserted["audit_insrt_id"] = self.params.get('loggedin_userid')
        to_be_inserted['is_mandatory'] = True
        to_be_inserted["is_actv"] = True
        to_be_inserted["eff_srt_dt"] = datetime.today().strftime('%Y-%m-%d')
        to_be_inserted["eff_end_dt"] = '9999-12-31'
        # to_be_inserted["audit_insrt_dt"] = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        # to_be_inserted["layer_flg"] = "I"
        # to_be_inserted["abslt_path"] = "test_data"
        self.app_db.insert(
            to_be_inserted[["audit_insrt_id","audit_batch_id","created_date","modified_date","is_actv","attr_id", "is_mandatory",
                            "eff_end_dt","eff_srt_dt","uri", "column_ordinal_position", "column_datatype",
                            "column_name", "column_remarks", "obj_id"]]
            .rename(columns={"table_schema": "schema_name",
                             "table_name": "obj_nm",
                             "column_name": "attr_nm",
                             "column_datatype": "attr_data_typ",
                             "column_ordinal_position": "col_loc",
                             "column_remarks": "attr_desc",
                             "uri": "obj_uri_id","created_date":"audit_insrt_dt",
                             "modified_date":"audit_updt_dt"}),
            'attr_meta',

            'system_config')
        self.app_db.insert(to_be_inserted.drop_duplicates(subset=['uri'])
                           [["uri", "version", "obj_id","table_type","table_name","audit_insrt_id","audit_batch_id",
                             "created_date","is_actv","eff_end_dt","eff_srt_dt","layer_id","modified_date"]].
                           rename(columns={"uri": "obj_uri_id","table_type":"obj_typ",
                                           "table_name":"obj_nm","layer_id":"src_data_layer_id",
                                           "created_date":"audit_insrt_dt","modified_date":"audit_updt_dt"}),
                           'object_meta',
                           'system_config')
        email_data = set({})
        for _, row in self.sf_records.iterrows():
            neo4j_query = f"""merge(a:SnowflakeAccount{{name:"{self.sf.account_id}"}}) set a +=  {{deleted:false}} 
                          merge(sf:Snowflake{{database:"{self.sf.database}", account:"{self.sf.account_id}"}}) set sf +=  {{deleted:false, connector_id: "{self.sf.connection_id}"}}
                          merge(s:SnowflakeSchema{{name:"{row.table_schema}", database:"{self.sf.database}", account:"{self.sf.account_id}"}}) set s +=  {{last_updated_ts:"{latest_ts}", deleted:false, layer:"{row.layer_name}", remarks:"{row.table_remarks}", layer_id: "{row.layer_id}"}}
                          merge(t:SnowflakeTable{{uri: "{row.uri}"}}) set t +=  {{last_updated_ts:"{latest_ts}",created_date:"{row.created_date}",modified_date:"{row.modified_date}",deleted:false, name:"{row.table_name}", schema:"{row.table_schema}", type:"{row.table_type}", database:"{self.sf.database}", account:"{self.sf.account_id}", remarks:"{row.table_remarks}", layer_id: "{row.layer_id}", connector_id: "{self.sf.connection_id}",obj_id:"{row.obj_id}",version:"{row.version}",layer:"{row.layer_name}"}}
                          merge(c:SnowflakeColumn{{name:"{row.column_name}", parent_uri_id: "{row.uri}"}}) set c +=  {{deleted:false,  table:"{row.table_name}", schema:"{row.table_schema}", database:"{self.sf.database}", account:"{self.sf.account_id}", remarks:"{row.column_remarks}", ordinal_position:{row.column_ordinal_position}, datatype:"{row.column_datatype}",attr_id:"{row.attr_id}"}}
                          merge((a)-[:HAS]->(sf))
                          merge((sf)-[:HAS]->(s))
                          merge((s)-[:HAS]->(t))
                          merge((t)-[:HAS]->(c))
                          merge(x:Glossary{{name:"BusinessGlossary"}})
                          merge(tag:Tag{{name:"Needs_Attention",status:"true"}}) set tag +=
                          {{created_on: "",user_id: "",creator_user_name: "",subcategory:"Others",
                          description:"Needs Attention for new onboardig objects",
                          modified_on: "{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                          modifier_user_id: "",
                          modifier_user_name: ""}}
                          merge((x)-[:HAS]->(tag))
                          merge((t)-[:TagLink]->(tag))"""

            _ = neo4j.run_query(neo4j_query)

            email_data.add("""<p><b>Object id</b> : """ + str(row.uri) +""",<b>name</b> : """ + str(row.table_name) +""",<b>On layer</b> : """ + str(row.layer_name) + """</p>""")

        self.send_email(email_data)
        self.add_response('status', 'success')

    def get_pg_uris(self):
        try:
            return self.pg_records['uri'].unique()
        except Exception as e:
            self.log.exception(e)
            return []

    def resolve_layer_name(self):
        pg_records = self.layer_df[['layer_name', 'layer_id', 'table_schema']].set_index("table_schema")
        self.sf_records = pd.merge(self.sf_records, pg_records, how='left',
                                   left_on='table_schema', right_index=True)

    def resolve_attr_id(self):
        last_attr_id = \
            self.app_db.execute(f"""SELECT last_value FROM system_config.attr_meta_attr_id""")[
                'last_value'][0]
        self.sf_records['attr_id'] = range(last_attr_id + 1, last_attr_id + len(self.sf_records) + 1)

    def resolve_URI(self):
        """with respect to Postgres identify the new objects, and existing object changes """
        self.sf_records['key'] = self.sf_records['table_schema'] + '.' + self.sf_records['table_name']
        self.pg_records['key'] = self.pg_records['table_schema'] + '.' + self.pg_records['table_name']
        new_objects = self.sf_records[~self.sf_records['key'].isin(self.pg_records['key'].unique())]


        for each_key in new_objects['key'].unique():
            service = ApiExchange(self.session, name='get_object_instance', obj_id=None)
            service.execute()
            self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = service.response['Contents'][0][
                'obj_id']
            self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = service.response['Contents'][0][
                'version']
            self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = service.response['Contents'][0][
                'obj_uri_id']

        existing_objects = self.sf_records[self.sf_records['key'].isin(self.pg_records['key'].unique())]

        for each_key in existing_objects['key'].unique():
            # with no change
            sf_records_for_each_key = self.sf_records.loc[self.sf_records['key'] == each_key, ['column_name', 'column_datatype']]
            pg_records_for_each_key = self.pg_records.loc[self.pg_records['key'] == each_key, ['column_name', 'column_datatype','version']]
            # exact match - no change in uri, version ; copy postgres to sf dataframe
            sf_records_for_each_key['SF_UMD'] = sf_records_for_each_key['column_name'] + sf_records_for_each_key['column_datatype']
            pg_records_for_each_key['PG_UMD'] = pg_records_for_each_key['column_name'] + pg_records_for_each_key['column_datatype']
            df_pg = pg_records_for_each_key.sort_values('PG_UMD').groupby('version').agg([(','.join)])
            df_pg.columns = df_pg.columns.droplevel(1)
            df_pg.reset_index(inplace=True)
            df_sf = sf_records_for_each_key.sort_values('SF_UMD').agg(','.join).to_frame().transpose()
            if not df_sf.merge(df_pg, how='inner', left_on='SF_UMD', right_on='PG_UMD').dropna().empty:
                matched_version = df_sf.merge(df_pg, how='inner', left_on='SF_UMD', right_on='PG_UMD').dropna()['version'][0]
                matched_record = self.pg_records.loc[(self.pg_records['key'] == each_key) & (self.pg_records['version'] == matched_version), ['obj_id', 'version', 'uri']]
                obj_id, version, uri = matched_record.iloc[0, :].values
                self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = obj_id
                self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = version
                self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = uri
            # with changes - fetch new uri/version
            else:
                matched_record = self.pg_records.loc[self.pg_records['key'] == each_key, ['obj_id']]
                obj_id = matched_record.iloc[0, :].values[0]
                service = ApiExchange(self.session, name='get_object_instance', obj_id=str(obj_id))
                service.execute()
                self.sf_records.loc[self.sf_records['key'] == each_key, 'obj_id'] = service.response['Contents'][0][
                    'obj_id']
                self.sf_records.loc[self.sf_records['key'] == each_key, 'version'] = service.response['Contents'][0][
                    'version']
                self.sf_records.loc[self.sf_records['key'] == each_key, 'uri'] = service.response['Contents'][0][
                    'obj_uri_id']


    def send_email(self,email_data):
        email_string = ''
        for single_email_content in email_data:
            email_string += "" + single_email_content
        email_string = "<HTML><BODY>" + email_string + "</BODY></HTML>"
        # send email to uesrgroups
        service = notification.SendNotification(self.session, name='onboard notification', description=email_string,
                                                type='HTML', **self.params)
        service.execute()


class SnowflakeIncrementalScanAllConnections(Service):
    def __init__(self, session, **kwargs):
        """Register metadata of Snowflake to neo4j db"""
        super().__init__()
        self.session = session
        self.app_db = AppDb()
        self.kwargs = kwargs

    def execute(self):
        conn_ids = self.app_db.execute("select id from system_config.connection_meta where is_active and connection_type='sf'")
        self.log.debug(conn_ids)
        for i in conn_ids['id']:
            try:
                self.log.debug('='*30+f'Connection ID:{i} STARTS'+'='*30)
                snowflake_incremental_scan_service = SnowflakeIncrementalScanV2(self.session, sf_connector_id=i, **self.kwargs)
                snowflake_incremental_scan_service.execute()
                del snowflake_incremental_scan_service
            except Exception as error:
                self.log.exception(f"Issue with Snowflake ID: {i}")
            finally:
                self.log.debug('='*30+f'Connection ID:{i} ENDS'+'='*30)

def snowflake_incremental_scan_scheduled():
    appdb = AppDb()
    session = Session(AppDb.engine)
    service = SnowflakeIncrementalScanAllConnections(session, loggedin_userid=0)
    service.execute()