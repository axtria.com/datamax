#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - S3 sync"""

import json
from datetime import datetime as dt 
from services import Service
from vendor.s3 import S3

from services.catalog.s3_metadata_register import S3MetadataRegister


class S3Sync(Service):
	"""Syncs metadata of S3 with neo4j"""
	def __init__(self, session, event_details :str, s3_connector_id :int, **kwargs):
		super().__init__()

		self.session = session
		self.event_details = json.loads(event_details)

		self.event_type  = self.event_details['eventName']
		self.awsRegion   = self.event_details['awsRegion']
		self.bucketName  = self.event_details['s3']['bucket']['name']
		self.objectKey   = self.event_details['s3']['object']['key']
		self.objectSize  = self.event_details['s3']['object']['size'] if 'size' in self.event_details['s3']['object'].keys() else None
		self.lastUpdated = dt.strptime(self.event_details['eventTime'],'%Y-%m-%dT%H:%M:%S.%fZ').strftime('%Y-%m-%d %H:%M:%S')


		self.s3 = S3.initialize(s3_connector_id, session)


	def execute(self):

		self.log.info(self.event_details)

		registration_service = S3MetadataRegister(self.session,
													self.s3,
													self.event_type,
													self.awsRegion,
													self.bucketName,
													self.objectKey,
													self.objectSize,
													self.lastUpdated)
		registration_service.execute()
		self.response = registration_service.response
 


