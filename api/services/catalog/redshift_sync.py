#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
'''Datamax Catalog - Redshift full scan'''

import pandas as pd 
from services import Service
from vendor.neo4j import Neo4j 
from vendor.redshift import Redshift
from models import Connector
from datetime import datetime
from numpy import random

neo4j = Neo4j()

class RedshiftSync(Service):
	'''Sync metadata of Redshift to neo4j db'''
	def __init__(self, session, rs_connector_id, **kwargs):
		super().__init__()

		self.session = session
		self.rs_connector_id = int(rs_connector_id)
		self.rs = Redshift.initialize(rs_connector_id, session)		 
		
	def execute(self):

		count_curr = self.rs.execute('select count(*) as count from stl_ddltext')

		try:
			count_prev = int(open(f'services/catalog/redshift_sync_cache/rs_connector_{self.rs_connector_id}.count', 'r').read())
		except:
			count_prev = -1

		if count_curr == count_curr:
			return

		try:
			filter_config = pd.read_csv('services/catalog/config/redshift_filter_config.csv')
		except Exception as e:
			self.log.exception(e)
			return

		schemas = filter_config.loc[filter_config['rs_connector_id'] == int(self.rs_connector_id),'table_schema'].tolist()

		schemas_tuple = "('" + "','".join(schemas) + "')" 

		rs_query =  'select '\
					't.table_schema as table_schema, '\
					't.table_name as table_name, '\
					't.table_type as table_type, '\
					't.remarks as table_remarks, '\
					'c.column_name as column_name, '\
					'c.data_type as column_datatype, '\
					'c.ordinal_position as column_ordinal_position, '\
					'c.remarks as column_remarks '\
					'from '\
					'svv_columns as c join svv_tables as t '\
					'on '\
					'(t.table_name = c.table_name and t.table_schema = c.table_schema) '\
					f'where t.table_schema in {schemas_tuple};'


		metadata = self.rs.execute(rs_query)
		metadata.fillna('No description available', inplace=True)

		fname = f'tmp_{self.uid()}.csv'
		fpath = f'C:/neo4j/import/{fname}'

		metadata.to_csv(fpath, index=None)

		rs_connector = self.session.query(Connector).filter(Connector.id == self.rs_connector_id).first()

		database = rs_connector.connection['DBNAME']
		host = rs_connector.connection['HOST']
		port = rs_connector.connection['PORT']

		neo4j_query = 	f'load csv with headers from "file:///{fname}" as row '\
						f'merge(r:Redshift{{database:"{database}", host:"{host}", port:{port}}}) '\
						'merge(s:RedshiftSchema{name:row.table_schema}) '\
						'merge(t:RedshiftTable{name:row.table_name, schema:row.table_schema, type:row.table_type, remarks:row.table_remarks}) '\
						'merge(c:RedshiftColumn{name:row.column_name, table:row.table_name, schema:row.table_schema, remarks:row.column_remarks, datatype:row.column_datatype, ordinal_position:row.column_ordinal_position}) '\
						'merge((r)-[:HAS]->(s)) '\
						'merge((s)-[:HAS]->(t)) '\
						'merge((t)-[:HAS]->(c)) ' 

		_ = neo4j.run_query(neo4j_query)

		self.add_response('status','success')



	def uid(self):
		return hex(random.randint(10**6)) + datetime.now().strftime('___%d_%b_%Y__%H_%M_%S_%f')
		
