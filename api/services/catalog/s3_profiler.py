from services import Service
from utils.exceptions import UnsupportedDateFormat, NoDataAvailable
from vendor.appdb import AppDb
from vendor.s3 import S3
from vendor.neo4j import Neo4j
import pandas as pd
from models import Layer
from io import StringIO


class S3Profiler(Service):
    """
    Profiles/calculates basic statistics of a table present in s3.
    """

    def __init__(self, session, s3_connector_id, node_id, n_records_dtype_detection=10, **kwargs):
        """
        Parameters
        ----------
        object_key : string
            name of redshift table
        """
        super().__init__()
        self.neo4j = Neo4j()
        self.node_id = int(node_id)
        self.n_records_dtype_detection = n_records_dtype_detection
        self.node = self.neo4j.get_node_by_id(self.node_id)

        self.s3 = S3.initialize(self.node['connector_id'], session)

    def execute(self, return_path=False):
        try:

            file_key = str(self.node['bucketName']) + '/' + str(self.node['objectKey'])

            data = self.s3.read(file_key, self.n_records_dtype_detection)

            delimiter = self.getDelimiter(data[0])
            df = pd.read_csv(StringIO(''.join(data)), sep=delimiter)

            json_obj = []
            json_obj.append(
                {'title': 'sample', 'content': df.fillna('').to_dict(orient='records'), 'columns': list(df.columns)})

            self.add_response('response', json_obj)
        except:
            self.log.debug('Exception occured', exc_info=True)
            self.add_response('response', 'Unable to fetch profling data')

    def getDelimiter(self, header_str):
        """Infer delimiter on the basis of header string"""
        delimiters = [',', '|', ';', ':', '\t']
        detected_delimiter = None
        max_count = 0
        for delimiter in delimiters:
            delimiter_count = header_str.count(delimiter)
            if 0 <= max_count < delimiter_count:
                detected_delimiter = delimiter
                max_count = delimiter_count
        return detected_delimiter


class ObjectFileProfiler(Service):
    def __init__(self, obj_uri_id, session, **kwargs):
        super().__init__()
        self.obj_uri_id = obj_uri_id
        self.app_db = AppDb()

    def execute(self):

        query = f"""select FI.obj_id, OB.obj_uri_id, FI.file_uri_id, max(FIP.part_nm) as file_name,
            max(ODLM.abslt_path) path,
            max( FIP.arrvl_tm)  arrvl_tm,
            'NA' as file_size
            from system_config.file_instnc_part as FIP
            join system_config.file_instnc as FI on FI.file_inst_id = FIP.file_inst_id
            join system_config.object_meta as OB on OB.obj_id = FI.obj_id
            join system_config.object_data_layer_meta as ODLM on ODLM.obj_id = FI.obj_id
            where OB.obj_uri_id = '{self.obj_uri_id}'
            and ODLM.is_actv = true
            and odlm.layer_flg = 'L'
            group by  FI.obj_id, OB.obj_uri_id, FI.file_uri_id"""


        # query = f"""select FIP.part_nm as file_name, FI.file_uri_id, ODLM.abslt_path, FIP.arrvl_tm ,
        #         'NA' as file_size, FI.obj_id, OB.obj_uri_id
        #         from system_config.file_instnc_part as FIP
        #         join system_config.file_instnc as FI on FI.file_inst_id = FIP.file_inst_id
        #         join system_config.object_meta as OB on OB.obj_id = FI.obj_id
        #         join system_config.object_data_layer_meta as ODLM on ODLM.obj_id = FI.obj_id
        #         where OB.obj_uri_id = '{self.obj_uri_id}'
        #         --and OB.is_actv = true
        #         ODLM.layer_flg = 'L'
        #         and  ODLM.is_actv = true """
        file_profiled_data = self.app_db.execute(query)
        if len(file_profiled_data.values) == 0:
            return
            # raise NoDataAvailable
        try:
            file_profiled_data['arrvl_tm'] = file_profiled_data['arrvl_tm'].dt.strftime('%d-%m-%Y %I:%M %p')
        except Exception as e:
            self.log.exception(e)
            raise UnsupportedDateFormat
        self.add_response('status', 'success')
        self.add_response('content', file_profiled_data.to_dict(orient='records'))
