#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Bulk Dump"""

import pandas as pd
from services import Service
from vendor.neo4j import Neo4j
from datetime import datetime
from flask import send_file
import json

class BulkDump(Service):
    """Bulk Dump catalog"""
    def __init__(self, session, layers=None, limit_records=1000000, **kwargs):
        super().__init__()

        self.neo4j = Neo4j()
        self.layers = layers
        self.limit_records = int(limit_records)

    def execute(self):

        layer_filter_condition = ''

        if self.layers:
            layer_ids = json.dumps(self.layers.split(','))
            layer_filter_condition = f'and layer.layer_id in {layer_ids}'

        query = f'''match((layer)-[*1..]->(object)) 
					where ((object:S3File and layer:S3Bucket) or (layer:SnowflakeSchema and object:SnowflakeTable))
					{layer_filter_condition} 
					with layer, object 
					optional match (object)-[:TagLink]->(tag) 
					return 
					id(object) as Id,
					object.name as Name,
					object.logi_name as Logical_Name, 
					layer.layer as Layer, 
					object.tech_desc as Technical_Description, 
					object.buss_desc as Business_Description, 
					object.logi_desc as Logical_Description, 
					collect(tag.name) as Tags, 
					object.average_rating as Average_Rating, 
					round(100*object.size/1024.0/1024.0)/100 as File_Size_MB, 
					object.record_count as Record_Count
					limit {self.limit_records}'''

        dump = self.neo4j.run_query(query)

        object_mapping = self.neo4j.run_query('match((object:Object)-->(file:S3File)) return id(file) as Id, object.name as Onboarded_Object')

        if not object_mapping.empty:
            dump = dump.merge(object_mapping, how='left', on='Id')
        else:
            dump['Onboarded_Object'] = ''

        ordered_cols = ['Id', 'Name', 'Logical_Name','Onboarded_Object', 'Layer',
                        'Technical_Description', 'Business_Description', 'Logical_Description',
                        'Tags', 'Average_Rating', 'File_Size_MB', 'Record_Count']

        dump = dump[ordered_cols]
        dump['Tags'] = dump['Tags'].apply(lambda tags: ';'.join(tags))
        dump = dump.fillna('')

        ts = datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')
        fname = f'dumps/{ts}.csv'

        dump.to_csv(fname, index=None)

        self.add_response('file_path',fname)
        self.add_response('file_name',f'catalog_dump_{ts}.csv')


class DumpNodes(Service):
    """Dump Selective catalog"""

    def __init__(self, session, uris, limit_records=1000000, col_level=False, **kwargs):
        super().__init__()
        self.col_level = col_level.lower()
        self.neo4j = Neo4j()
        self.limit_records = int(limit_records)
        self.uris = uris

    def execute(self):
        # column + object level data
        if self.col_level == 'true':
            query = f"""match ((n)-[:HAS]->(m)) where n.uri in {self.uris} with n,m
            optional match (n)-[:TagLink]->(tag)
            return      
            n.name as Object_Name,n.object_type as Object_Type, n.business_unit_code as Business_Unit_Code,
            n.buss_desc as Business_Description, n.country_code as Country_Code, n.desc as Description,
            n.layer as Layer, n.logi_name as Logical_Name, n.owner as Owner, n.uri as URI,
            n.version as Version, m.name as Column_Name, m.datatype as Column_DataType, id(n) as Id,
            collect(tag.name) as Tags limit {self.limit_records}"""

        # object level data
        else:
            query = f"""match (n) where n.uri in {self.uris} with n
                        optional match (n)-[:TagLink]->(tag)
                        return 
                        n.name as Object_Name,n.object_type as Object_Type, n.business_unit_code as Business_Unit_Code,
                        n.buss_desc as Business_Description, n.country_code as Country_Code, n.desc as Description,
                        n.layer as Layer, n.logi_name as Logical_Name, n.owner as Owner, n.uri as URI,
                        n.version as Version,
                        collect(tag.name) as Tags,id(n) as Id,
                        n.average_rating as Average_Rating, 
                        round(100*n.size/1024.0/1024.0)/100 as File_Size_MB, 
                        n.record_count as Record_Count 
                        limit {self.limit_records}"""

        df = pd.DataFrame(self.neo4j.run_query(query, True))
        df = df[sorted(df.columns.tolist())]
        ts = datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')
        fname = f'dumps/basket_dump_{ts}.csv'
        df.to_csv(fname, index=None)

        self.add_response('file_path', fname)
        self.add_response('file_name', f'catalog_dump_{ts}.csv')
