#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Nitin Sharma <nitin.sharma@axtria.com>
"""Datamax Catalog - Bulk Import Report"""

from services import Service

class BulkImportReport(Service):
	"""Bulk Dump catalog"""
	def __init__(self, session, report_id, **kwargs):
		super().__init__()

		self.report_id = report_id

	def execute(self):

		fname = 'report_' + self.report_id
		fpath = 'dumps/' + fname

		self.add_response('file_path',fpath)
		self.add_response('file_name',fname)








