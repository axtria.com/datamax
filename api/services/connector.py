#   datamax/api/services/connector.py
#   Copyright (C) 2020 Axtria.
#
#   Author (s):
#   Akshay Saini <akshay.saini@axtria.com>
#   Varun Singhal <varun.singhal@axtria.com>
"""Connector management."""
from sqlalchemy.orm import Session

from services import Service
from utils.helpers import decoder
from vendor.s3 import S3
from vendor.sftp import Sftp
from vendor.snowflake import Snowflake
from utils.exceptions import S3ConnectionTestFailed, SFTPConnectionTestFailed, SnowflakeConnectionTestFailed


class TestS3Connection(Service):
    def __init__(self, session: Session, access_key: str, secret_key: str, iamrole: str, **kwargs):
        super().__init__()
        self.session = session
        self.access_key = '' if access_key == "null" else access_key
        self.secret_key = '' if secret_key == "null" else decoder(secret_key)
        self.role = iamrole

    def execute(self):
        try:
            s3 = S3(self.access_key, self.secret_key, self.role)
            s3.list_buckets()
            self.add_response('message', 'Connection established.')
        except Exception as e:
            self.log.exception(e)
            raise S3ConnectionTestFailed


class TestSftpConnection(Service):
    def __init__(self, session: Session, username: str,
                 password: str, host: str, port: int, **kwargs):
        super().__init__()
        self.session = session
        self.user = username
        self.password = decoder(password)
        self.host = host
        self.port = int(port)

    def execute(self):
        try:
            sftp = Sftp(self.user, self.password, self.host, self.port)
            sftp.sftp_connector.listdir()
            self.add_response('message', 'Connection established.')
        except Exception as e:
            self.log.exception(e)
            raise SFTPConnectionTestFailed


class TestSnowflakeConnection(Service):
    def __init__(self, session: Session, user, password, account_id, warehouse, database, **kwargs):
        super().__init__()
        self.session = session
        self.user = user
        self.password = decoder(password)
        self.account_id = account_id
        self.warehouse = warehouse
        self.database = database

    def execute(self):
        try:
            sf = Snowflake(self.user, self.password, self.account_id, self.warehouse, self.database)
            sf.execute('select current_version()')
            self.add_response('message', 'Connection established.')
        except Exception as e:
            self.log.exception(e)
            raise SnowflakeConnectionTestFailed

class ListSnowFlakeSchema(Service):
    def __init__(self, session, sf_connector_id,**kwargs):
        super().__init__()
        self.postgres = session
        self.sf_connector_id = sf_connector_id

    def execute(self):
        try:
            sf = Snowflake.initialize(sf_connector_id=self.sf_connector_id,session=self.postgres )
            # below code to debug in case of env issues
            #sf = Snowflake(self.user, self.password, self.account_id, self.warehouse, self.database)
            sf.execute('select schema_name from information_schema.schemata order by schema_name')
            result = sf.execute('select schema_name::string from information_schema.schemata order by schema_name')
            self.add_response('response',dict(enumerate(result.values.flatten(),0)) )
        except Exception as e:
            self.log.exception(e)
            raise SnowflakeConnectionTestFailed
