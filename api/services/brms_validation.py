#   datamax/api/services/brms_validation.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import json
import logging
import uuid
from datetime import datetime

from sqlalchemy.orm import Session

from entity.scenario import ValidationScenario
from entity.workflow import Workflow
from models import ValidationWorkflow as tWorkflow, ValidationScenario as tScenario, \
    ValidationScenarioLog as tScenarioLog, Workspace as tWorkspace
from services import Service
from utils.brms_validation_grammar import tokenize
from utils.brms_validation_transformer import to_json
from utils.exceptions import BRMSPublishedWorkflowModified, BRMSDraftWorkflowExecuted, BRMSDuplicateWorkflowName, \
    BRMSInactiveScenarioExecuted, BRMSEmptyWorkflowPublished
from vendor.redshift import Redshift


class WorkflowGateway:
    @staticmethod
    def restore(session: Session, workflow_id: str) -> Workflow:
        logging.debug("restoring workflow from db %r" % workflow_id)
        db_workflow = session.query(tWorkflow).filter(tWorkflow.id == workflow_id).first()
        return Workflow.initialize(db_workflow.validation_meta)

    @staticmethod
    def save(session: Session, workflow: Workflow, *, strict: bool = True):
        if (workflow.status == 'PUBLISHED') & strict:
            # Undo the changes and raise exception.
            raise BRMSPublishedWorkflowModified
        to_commit = tWorkflow(id=workflow.workflow_id, name=workflow.name, description=workflow.description,
                              validation_meta=workflow.serialize, status=workflow.status, health=workflow.health)
        session.merge(to_commit)
        session.commit()


class ScenarioGateway:
    @staticmethod
    def restore(session: Session, scenario_id: int, workflow: Workflow, execution_schema: str,
                output_schema: str) -> ValidationScenario:
        logging.debug("restoring scenario from db %r" % scenario_id)
        db_scenario = session.query(tScenario).filter(tScenario.id == scenario_id).first()
        db_scenario.last_run = 'In progress'
        session.commit()
        return ValidationScenario.initialize_with_dataset(db_scenario.id, db_scenario.name, db_scenario.scenario_meta,
                                                          db_scenario.is_active, workflow,
                                                          execution_schema=execution_schema,
                                                          output_schema=output_schema)

    @staticmethod
    def save(session: Session, scenario: ValidationScenario):
        to_commit = tScenarioLog(scenario_id=scenario.scenario_id, scenario_log_meta=scenario.serialize)
        session.merge(to_commit)
        db_scenario = session.query(tScenario).filter(tScenario.id == scenario.scenario_id).first()
        db_scenario.last_run = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        session.commit()


class CreateWorkflow(Service):
    def __init__(self, session: Session, **kwargs):
        super().__init__()
        self.appdb = session

    def execute(self):
        workflow = Workflow()
        WorkflowGateway.save(self.appdb, workflow)
        self.add_response('workflow_id', workflow.workflow_id)


class AddWorkflowMeta(Service):
    def __init__(self, session: Session, validation_id: str, name: str, description: str,
                 selected_datasets: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow = WorkflowGateway.restore(self.appdb, validation_id)
        self.workflow.name = name
        self.workflow.description = description
        self.selected_datasets = json.loads(selected_datasets)

    def execute(self):
        # unique constraint in workflow name and workspace id.
        workflows = self.appdb.query(tWorkflow).filter(tWorkflow.name == self.workflow.name,
                                                       tWorkflow.id != self.workflow.workflow_id).all()
        if len(workflows) > 0:
            raise BRMSDuplicateWorkflowName

        self.workflow.truncate_selected_dataset()
        for selected_dataset in self.selected_datasets:
            self.workflow.add_selected_dataset(selected_dataset.get('name'),
                                               selected_dataset.get('adaptor_meta'),
                                               selected_dataset.get('schema'))
        WorkflowGateway.save(self.appdb, self.workflow)
        self.response.update(self.workflow.serialize)


class AddRule(Service):
    def __init__(self, session: Session, validation_id: str, rule_name: str, user_input: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow_id = validation_id
        self.rule_name = rule_name
        self.user_input = user_input

    def execute(self):
        workflow = WorkflowGateway.restore(self.appdb, self.workflow_id)
        tree = tokenize(self.user_input, workflow.datasets)
        json = to_json(tree, workflow.datasets)
        workflow.add_rule({'rule_name': self.rule_name, 'user_input': self.user_input, **json})
        workflow.add_dataset(json['output'], json['_output_metadata'])
        WorkflowGateway.save(self.appdb, workflow)
        self.response.update(workflow.serialize)


class UpdateRule(Service):
    def __init__(self, session: Session, validation_id: str, rule_name: str, user_input: str,
                 rule_id: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow_id = validation_id
        self.rule_name = rule_name
        self.user_input = user_input
        self.rule_id = rule_id

    def execute(self):
        workflow = WorkflowGateway.restore(self.appdb, self.workflow_id)
        tree = tokenize(self.user_input, workflow.datasets)
        json = to_json(tree, workflow.datasets)
        workflow.update_rule(self.rule_id,
                             {'rule_name': self.rule_name, 'user_input': self.user_input,
                              'rule_id': self.rule_id, **json})
        workflow.add_dataset(json['output'], json['_output_metadata'])
        WorkflowGateway.save(self.appdb, workflow)
        self.response.update(workflow.serialize)


class DeleteRule(Service):
    def __init__(self, session: Session, validation_id: str, rule_id: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow_id = validation_id
        self.rule_id = rule_id

    def execute(self):
        workflow = WorkflowGateway.restore(self.appdb, self.workflow_id)
        workflow.delete_rule_silently(self.rule_id)
        WorkflowGateway.save(self.appdb, workflow)
        self.response.update(workflow.serialize)


class SaveWorkflow(Service):
    def __init__(self, session: Session, validation_id: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow_id = validation_id

    def execute(self):
        workflow = WorkflowGateway.restore(self.appdb, self.workflow_id)
        WorkflowGateway.save(self.appdb, workflow)
        self.add_response('message', 'Saved successfully')


class PublishWorkflow(Service):
    def __init__(self, session: Session, validation_id: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow = WorkflowGateway.restore(self.appdb, validation_id)

    def execute(self):
        if len(self.workflow.rules.serialize) == 0:
            raise BRMSEmptyWorkflowPublished
        self.workflow.status = 'PUBLISHED'
        WorkflowGateway.save(self.appdb, self.workflow, strict=False)
        self.add_response('message', 'Published successfully')


class GetWorkflow(Service):
    def __init__(self, session: Session, validation_id: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow_id = validation_id

    def execute(self):
        workflow = WorkflowGateway.restore(self.appdb, self.workflow_id)
        self.response.update(workflow.serialize)


class EditWorkflow(Service):
    def __init__(self, session: Session, validation_id: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow = WorkflowGateway.restore(self.appdb, validation_id)

    def execute(self):
        if self.workflow.status == 'PUBLISHED':
            self.log.warning('Attempted to edit the published workflow')
            raise BRMSPublishedWorkflowModified
            # self.workflow.version += 1
            # self.workflow.workflow_id = uuid.uuid4().hex
            # self.workflow.status = "DRAFT"
            # WorkflowGateway.save(self.appdb, self.workflow)
        self.response.update(self.workflow.serialize)


class CopyWorkflow(Service):
    def __init__(self, session: Session, validation_id: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow = WorkflowGateway.restore(self.appdb, validation_id)

    def execute(self):
        self.workflow.workflow_id = uuid.uuid4().hex
        self.workflow.name = 'Copy of ' + self.workflow.name
        # reset the properties
        self.workflow.version = 1
        self.workflow.status = 'DRAFT'

        # unique constraint in workflow name and workspace id.
        workflows = self.appdb.query(tWorkflow).filter(tWorkflow.name == self.workflow.name,
                                                       tWorkflow.id != self.workflow.workflow_id).all()
        if len(workflows) > 0:
            raise BRMSDuplicateWorkflowName

        WorkflowGateway.save(self.appdb, self.workflow)
        self.response.update(self.workflow.serialize)


class AddParameterDefinition(Service):
    def __init__(self, session: Session, validation_id: str, parameter_name: str, help_text: str, description: str,
                 config_type: str, config_value: str, **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow_id = validation_id
        self.parameter_name = parameter_name
        self.definition = {'help_text': help_text, 'description': description,
                           'config_type': config_type, 'config_value': config_value}

    def execute(self):
        workflow = WorkflowGateway.restore(self.appdb, self.workflow_id)
        workflow.add_parameter_definition(self.parameter_name, self.definition)
        WorkflowGateway.save(self.appdb, workflow)
        self.response.update(workflow.serialize)


class ExecuteScenario(Service):
    def __init__(self, session: Session, scenario_id: int, workflow_id: str,
                 execution_schema: str = 'brms_instance', output_schema: str = 'brms_output', **kwargs):
        super().__init__()
        self.appdb = session
        self.workflow = WorkflowGateway.restore(self.appdb, workflow_id)
        self.scenario = ScenarioGateway.restore(self.appdb, scenario_id, self.workflow, execution_schema, output_schema)

        if self.workflow.status != 'PUBLISHED':
            raise BRMSDraftWorkflowExecuted

        if not self.scenario.is_active:
            raise BRMSInactiveScenarioExecuted

    def execute(self):
        workspace = self.appdb.query(tWorkspace).filter(tWorkspace.id == self.workflow.workspace_id).first()
        rs = Redshift.initialize(workspace.connector_id, self.appdb)

        for error_table_query in self.scenario.error_tables_queries:
            rs.execute(self.scenario.prepare_query(error_table_query))

        for rule in self.workflow.rules:
            query = self.scenario.prepare_query(rule.query)
            try:
                rs.execute(query)
            except Exception as e:
                self.log.exception(e)
                self.scenario.mark_fail()

        for success_table_query in self.scenario.success_tables_queries:
            rs.execute(self.scenario.prepare_query(success_table_query))

        for name, total_count_query, error_count_query in self.scenario.invalid_meta_queries:
            self.scenario.invalid_meta[name] = {
                'invalid_count': int(rs.execute(self.scenario.prepare_query(error_count_query)).iloc[0, 0]),
                'total_count': int(rs.execute(self.scenario.prepare_query(total_count_query)).iloc[0, 0])}

        ScenarioGateway.save(self.appdb, self.scenario)
        self.response.update(self.scenario.serialize)
