#   datamax/api/job/three_stage_validation.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from sqlalchemy.orm import Session

from services import Service
from services.data_validator.redshift_validation_strategy import RedshiftValidationStrategy
from services.data_validator.validation_engine import ValidationEngine, ArchieveConfigInstance
from services.data_validator.validation_response import RedshiftFailedRecordsToS3
from vendor.appdb import AppDb


class TwoStageRedshiftValidation(Service):
    def __init__(self, **kwargs):
        super().__init__()
        self.session = Session(AppDb.engine)
        self.kwargs = kwargs
        self.kwargs.update(session=self.session)

    def execute(self):
        
        rsvalstrategy = RedshiftValidationStrategy( **self.kwargs )
        service1 = ValidationEngine( stage=1,strategy= rsvalstrategy, **self.kwargs )
        service1.execute()
        service2 = RedshiftFailedRecordsToS3(stage=1, config_instance_id = service1.response.get("config_instance")[0].get(
                                                    "id"), **self.kwargs)
        service2.execute()
        service3 = ArchieveConfigInstance(stage=1, config_instance_id = service1.response.get("config_instance")[0].get(
                                                    "id"), **self.kwargs)
        service3.execute()
        self.add_response('config_instance', service1.response['config_instance'])
        self.add_response('rule_instance', service1.response['rule_instance'])
        self.add_response('data', service2.response['data'])
        
