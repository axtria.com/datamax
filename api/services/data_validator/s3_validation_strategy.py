#   datamax/api/services/data_validator/s3_validation_strategy.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>, Hemalatta Seksaria <hemalatta.seksaria@axtria.com>
import datetime

from models import Rules
from services.data_validator.validation_engine import ValidationStrategy
from utils.row_operation import inspect_delimiter
from vendor.s3 import S3


class S3ValidationStrategy(ValidationStrategy):
    def __init__(self, s3_connector_id: int, target_bucket, target_file_path: str, **kwargs):
        super().__init__(target_schema=target_bucket, target_table=target_file_path, detail_schema='')
        self.header = None  # initialize
        self.s3_connector_id = s3_connector_id
        self.s3_fail_count = []

    def prepare(self, session):
        self.s3 = S3.initialize(self.s3_connector_id, session)
        # read the first line of the file
        self.header = (self.s3.read(
            self.config_instance.target_schema + '/' + self.config_instance.target_table, 1)[0]).strip()

    def is_valid(self, rule_definition: Rules):
        return rule_definition.level == 's3'

    def execute(self, rule_definition, rule_instance_id, params) -> [int, str]:
        fail_count = getattr(self, rule_definition.pyfunctionname)(self.header, **params)
        self.s3_fail_count.append(fail_count)
        return fail_count, rule_definition.pyfunctionname + "(" + str(params) + ")"

    def finalize(self, rule_status):
        self.config_instance.status = 'Completed' if any(rule_status) else 'Failed'
        self.config_instance.end_date = datetime.datetime.utcnow()
        self.config_instance.fail_count = sum(self.s3_fail_count) if any(rule_status) else 0

    def is_column_duplicate(self, row: str, delimiter, **kwargs) -> int:
        if not delimiter:
            delimiter = inspect_delimiter(row)
        actual_length = len(row.split(delimiter))
        expected_length = len(set(row.split(delimiter)))
        return 0 if actual_length == expected_length else 1

    def is_column_ordered(self, row: str, delimiter, order, searchposition, **kwargs) -> int:
        """
        searchposition argument can have the following values:
            - B: Search From Begining
            - I: Search anywhere in the header
            - E: Search at the End of the header
            - C: Exact Match
        """
        if not delimiter:
            delimiter = inspect_delimiter(row)
        expected_str = ','.join(row.split(delimiter)).lower()
        actual_str = ','.join(order.split(',')).lower()
        matched = False
        if searchposition == 'I':
            matched = expected_str.count(actual_str) == 1 
        elif searchposition == 'B':
            matched = actual_str.startswith(expected_str) 
        elif searchposition == 'E':
            matched = actual_str.endswith(expected_str) 
        elif searchposition == 'C':
            matched = actual_str.endswith(expected_str) 
        return 0 if matched else 1

    def is_column_exist(self, row: str, delimiter, columns, **kwargs) -> int:
        if not delimiter:
            delimiter = inspect_delimiter(row)
        header = row.lower().split(delimiter)
        for column in columns.split(','):
            if column not in header:
                return 1  # fail
        return 0  # pass
