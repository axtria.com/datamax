#   datamax/api/services/data_validator/redshift_validation_strategy.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>, Hemalatta Seksaria <hemalatta.seksaria@axtria.com>
import datetime

from models import Rules
from services.data_validator.validation_engine import ValidationStrategy
from utils.querybuilder import create_table_query
from vendor.redshift import Redshift


class RedshiftValidationStrategy(ValidationStrategy):
    def __init__(self, rs_connector_id: int, target_schema, target_table,
                 detail_schema: str = 'validation', **kwargs):
        super().__init__(target_schema, target_table, detail_schema)
        self.rs_connector_id = rs_connector_id

    def prepare(self, session):
        # create redshift detail table
        self.redshift = Redshift.initialize(self.rs_connector_id, session)
        self.redshift.execute(create_table_query(self.config_instance.detail_schema_name,
                                                 self.config_instance.detail_table_name, [
                                                     ('DMX_id', 'int'), ('message', 'varchar'), ('rule_id', 'int'),
                                                     ('rule_instance_id', 'int'), ('process_id', 'int')]))

    def is_valid(self, rule_definition: Rules):
        return rule_definition.level != 's3'

    def execute(self, rule_definition: Rules, rule_instance_id: int, params: dict) -> [int, str]:
        self.redshift.execute(self._prepare_query(rule_definition.redshiftquery.format(**params), rule_definition.id,
                                                  rule_instance_id))
        return int(self.redshift.execute(self._rule_fail_count(rule_instance_id)).iat[0, 0]), \
               rule_definition.redshiftquery.format(**params)

    def finalize(self, rule_status):
        self.config_instance.fail_count = int(self.redshift.execute(self._fail_count_query()).iat[0, 0]) if any(
            rule_status) else 0
        self.config_instance.success_count = int(self.redshift.execute(self._success_count_query()).iat[0, 0]) if any(
            rule_status) else 0
        self.config_instance.status = 'Completed' if any(rule_status) else 'Failed'
        self.config_instance.end_date = datetime.datetime.utcnow()
        self.config_instance.total_count = self.config_instance.success_count + self.config_instance.fail_count

    def _prepare_query(self, query: str, rule_id: int, rule_instance_id: int):
        insert_query = """insert into {detail_schema_name}.{detail_table_name} (DMX_ID, message, rule_id, 
                          rule_instance_id, process_id) select s.DMX_ID, s.message, {rule_id} rule_id, 
                          {rule_instance_id}, {id} as process_id from ({query}) s"""
        return insert_query.format(rule_id=rule_id, query=query, rule_instance_id=rule_instance_id,
                                   **self.config_instance.serialize)

    def _rule_fail_count(self, rule_instance_id: int):
        query = """select count(1) from {detail_schema_name}.{detail_table_name} where  rule_instance_id = 
                    {rule_instance_id}"""
        return query.format(rule_instance_id=rule_instance_id, **self.config_instance.__dict__)

    def _fail_count_query(self):
        query = """select count(1) from {target_schema}.{target_table} t inner join 
                        (select distinct(dmx_id) from {detail_schema_name}.{detail_table_name}) d
                        on t.dmx_id = d.dmx_id"""
        return query.format(**self.config_instance.serialize)

    def _success_count_query(self):
        query = """select count(1) from {target_schema}.{target_table} t left join 
                    (select distinct(dmx_id) from {detail_schema_name}.{detail_table_name}) d
                    on t.dmx_id = d.dmx_id where d.dmx_id is null"""
        return query.format(**self.config_instance.serialize)
