#   datamax/api/services/data_validator/validation_engine.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>, Hemalatta Seksaria <hemalatta.seksaria@axtria.com>
import datetime
from abc import abstractmethod

from models import Config, ConfigInstance, Rules, RuleInstance
from services import Service
from vendor.redshift import Redshift

class ValidationStrategy:
    def __init__(self, target_schema, target_table, detail_schema):
        self.target_schema = target_schema
        self.target_table = target_table
        self.detail_schema = detail_schema
        self.config_instance = None  # initialized

    @abstractmethod
    def prepare(self, session):
        pass

    @abstractmethod
    def is_valid(self, rule_definition: Rules):
        pass

    @abstractmethod
    def execute(self, rule_definition: Rules, rule_instance_id: int, params: dict) -> [int, str]:
        pass

    @abstractmethod
    def finalize(self, rule_status: list):
        pass


class ValidationEngine(Service):
    def __init__(self, strategy: ValidationStrategy, session, config_id: int, stage: int = 1,
                 config_instance_id: int = None, rule_type: str = 'user_defined', **kwargs):
        super().__init__()
        self.strategy = strategy
        self.postgres = session
        self.config = self.extract_config(config_id)
        self.strategy.config_instance = self.register_config_instance(config_instance_id, config_id, rule_type,
                                                                      self.strategy.target_schema,
                                                                      self.strategy.target_table,
                                                                      self.strategy.detail_schema, stage)
        self.rule_type = rule_type
        self.rule_status = []

    def execute(self):
        self.strategy.prepare(self.postgres)
        for sequence, each_configured_rule in enumerate(self.config.params[self.rule_type]):
            self.log.debug("Executing for rule -- " + str(each_configured_rule))
            kwargs = dict([
                (parameter['param'], parameter['value']) for parameter in each_configured_rule['parameters']
            ])

            # override the existing config params
            kwargs.update({
                "schema": self.strategy.config_instance.target_schema,
                "tablename": self.strategy.config_instance.target_table,
                "bucket": self.strategy.config_instance.target_schema,
                "folder": self.strategy.config_instance.target_table})

            rule_definition = self.postgres.query(Rules).filter(Rules.id == each_configured_rule['id']).first()

            if not self.strategy.is_valid(rule_definition):
                continue

            rule_instance = self.register_rule_instance(rule_definition, kwargs)

            try:
                rule_instance.fail_count, rule_instance.query = self.strategy.execute(rule_definition, rule_instance.id,
                                                                                      kwargs)
                rule_instance.status = 'Completed'
                self.rule_status.append(True)
            except Exception as e:
                self.log.exception(e)
                rule_instance.fail_count = 0
                rule_instance.status = 'Failed'
                self.rule_status.append(False)

            # update rule instance status
            rule_instance.end_date = datetime.datetime.utcnow()

            self.postgres.add(rule_instance)
            self.postgres.commit()

        self.strategy.finalize(self.rule_status)
        self.postgres.add(self.strategy.config_instance)
        self.postgres.commit()

        self.prepare_response()

    def extract_config(self, config_id: int) -> Config:
        config = self.postgres.query(Config).filter(Config.id == config_id, Config.is_active == True).first()
        self.log.debug("configuration extracted by the system - {}".format(config))
        return config

    def register_config_instance(self, config_instance_id, config_id, rule_type, target_schema, target_table,
                                 detail_schema, stage) -> ConfigInstance:
        kwargs = {"config_id": config_id, "target_table": target_table, "target_schema": target_schema,
                  "status": "In progress", "detail_schema_name": detail_schema, "rule_type": rule_type,
                  "stage": stage}
        kwargs.update({"id": config_instance_id}) if config_instance_id else None
        config_instance = ConfigInstance(**kwargs)

        self.postgres.add(config_instance)
        self.postgres.flush()
        self.postgres.refresh(config_instance)

        config_instance.detail_table_name = config_instance.target_table + "_detail_" + str(config_instance.id)
        self.log.debug("registered the process with - {}".format(config_instance.serialize))
        return config_instance

    def register_rule_instance(self, rule_definition, kwargs):
        rule_instance = RuleInstance(config_instance_id=self.strategy.config_instance.id,
                                     rule_id=rule_definition.id,
                                     start_date=datetime.datetime.utcnow(),
                                     status='In progress',
                                     stage=self.strategy.config_instance.stage
                                     )
        self.postgres.add(rule_instance)
        self.postgres.flush()
        self.postgres.refresh(rule_instance)

        self.log.debug("registered the rule with rule_instance_id - {}".format(rule_instance.id))
        return rule_instance

    def prepare_response(self):
        self.add_response('config_instance', [c.serialize for c in self.postgres.query(ConfigInstance).filter(
            ConfigInstance.id == self.strategy.config_instance.id).order_by(ConfigInstance.stage.asc())])

        self.add_response('rule_instance', [r.serialize for r in self.postgres.query(RuleInstance).filter(
            RuleInstance.config_instance_id == self.strategy.config_instance.id).order_by(RuleInstance.id.asc())])


class ArchieveConfigInstance(Service):
    def __init__(self, session, rs_connector_id:int, config_instance_id: int,stage:int =1, **kwargs):
        super().__init__()
        self.postgres = session
        self.config_instance_id = config_instance_id
        self.rs_connector_id = rs_connector_id
        self.redshift = Redshift.initialize(self.rs_connector_id, session)
        self.stage = stage
        self.kwargs = kwargs

    def execute(self):
        config_instance = self.postgres.query(ConfigInstance).filter(ConfigInstance.id == self.config_instance_id,
                                                                     ConfigInstance.stage == self.stage).first()
        query = """drop table if exists {detail_schema_name}.{detail_table_name};""".format(**config_instance.__dict__)
        if not(config_instance.failure_schema_name is None or config_instance.failure_table_name is None):
            query = query + """drop table if exists {failure_schema_name}.{failure_table_name};""".format(**config_instance.__dict__)
        if not(config_instance.success_schema_name is None or config_instance.success_table_name is None):
             query = query + """drop table if exists {success_schema_name}.{success_table_name};""".format(**config_instance.__dict__)
        self.log.debug("query generated by ArchieveConfigInstance  - {}".format(query))

        self.redshift.execute(query)
        config_instance.is_archieved = True
        self.postgres.add(config_instance)
        self.postgres.commit()