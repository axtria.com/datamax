#   datamax/api/services/data_operations/read.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>, Hemalatta Seksaria <hemalatta.seksaria@axtria.com>
import json

from models import ConfigInstance
from services import Service
from utils.exceptions import InvalidValidationResponse, InvalidValidationResponseRerun
from vendor.redshift import Redshift
from services.data_movement.redshift_to_s3 import RedshiftQueryExportToS3


class ConstructSuccessRecords(Service):
    def __init__(self, session, rs_connector_id, config_instance_id: int, stage: int = 1, success_schema='validation',
                 success_table=None, shape: list = None, **kwargs):
        super().__init__()
        self.postgres = session
        self.redshift = Redshift.initialize(rs_connector_id, session)
        self.config_instance_id = config_instance_id
        self.stage = stage
        self.success_schema = success_schema
        self.success_table = success_table
        self.shape = json.loads(shape) if shape else None

    def execute(self):
        config_instance = self.postgres.query(ConfigInstance).filter(ConfigInstance.id == self.config_instance_id,
                                                                     ConfigInstance.stage == self.stage).first()

        if (config_instance.target_table.find("/") != -1) | (config_instance.target_table.find(".") != -1):
            self.log.error("Unable to populate success records for s3 based validation")
            raise InvalidValidationResponse
        if config_instance.success_table_name:
            self.log.error("Run already executed, service wont be able to run again.")
            raise InvalidValidationResponseRerun

        if self.success_table:
            config_instance.success_table_name = self.success_table
        else:
            config_instance.success_table_name = config_instance.target_table + "_success_" + str(config_instance.id)
        config_instance.success_schema_name = self.success_schema

        all_columns = "t.*"

        if self.shape:

            sub_query = ["t.dmx_id"]
            for column in self.shape:
                if column['dtype'] == 'date':
                    sub_query.append("to_date(" + column["name"] + ", '" + column["format"] + "') as " + column["name"])
                else:
                    sub_query.append(column["name"])

            all_columns = ",".join(sub_query)

        query = """select {all_columns} into {success_schema_name}.{success_table_name} from
                                {target_schema}.{target_table} t left join
                                (select distinct(dmx_id) from {detail_schema_name}.{detail_table_name}) d
                                on t.dmx_id = d.dmx_id
                                where d.dmx_id is null""".format(**config_instance.__dict__, all_columns=all_columns)
        self.redshift.execute(query)
        self.postgres.add(config_instance)
        self.add_response('config_instance', [c.serialize for c in self.postgres.query(ConfigInstance).filter(
            ConfigInstance.id == self.config_instance_id).order_by(ConfigInstance.stage.asc())])


class ConstructFailedRecords(Service):
    def __init__(self, session, rs_connector_id, config_instance_id: int, stage: int = 1,
                 failed_schema: str = 'validation', failed_table=None, **kwargs):
        super().__init__()
        self.postgres = session
        self.redshift = Redshift.initialize(rs_connector_id, session)
        self.config_instance_id = config_instance_id
        self.stage = stage
        self.failed_schema = failed_schema
        self.failed_table = failed_table

    def execute(self):
        config_instance = self.postgres.query(ConfigInstance).filter(ConfigInstance.id == self.config_instance_id,
                                                                     ConfigInstance.stage == self.stage).first()

        if (config_instance.target_table.find("/") != -1) | (config_instance.target_table.find(".") != -1):
            self.log.error("Unable to populate failed records for s3 based validation")
            raise InvalidValidationResponse
        if config_instance.failure_table_name:
            self.log.error("Run already executed, service wont be able to run again.")
            self.add_response('config_instance', [c.serialize for c in self.postgres.query(ConfigInstance).filter(
            ConfigInstance.id == self.config_instance_id).order_by(ConfigInstance.stage.asc())])
            return

        if self.failed_table:
            config_instance.failure_table_name = self.failed_table
        else:
            config_instance.failure_table_name = config_instance.target_table + "_failed_" + str(config_instance.id)
        config_instance.failure_schema_name = self.failed_schema

        query = """select t.* into {failure_schema_name}.{failure_table_name} from
                        {target_schema}.{target_table} t inner join 
                        (select distinct(dmx_id) from {detail_schema_name}.{detail_table_name}) d
                        on t.dmx_id = d.dmx_id """.format(**config_instance.__dict__)

        self.redshift.execute(query)
        self.postgres.add(config_instance)
        self.add_response('config_instance', [c.serialize for c in self.postgres.query(ConfigInstance).filter(
            ConfigInstance.id == self.config_instance_id).order_by(ConfigInstance.stage.asc())])


class RedshiftFailedRecordsToS3(Service):
    def __init__(self, session, config_instance_id: int,stage:int =1, **kwargs):
        super().__init__()
        self.postgres = session
        self.config_instance_id = config_instance_id
        self.stage = stage
        self.kwargs = kwargs

    def execute(self):
        config_instance = self.postgres.query(ConfigInstance).filter(ConfigInstance.id == self.config_instance_id,
                                                                     ConfigInstance.stage == self.stage).first()

        if (config_instance.target_table.find("/") != -1) | (config_instance.target_table.find(".") != -1):
            self.log.error("Unable to populate failed records for s3 based validation")
            raise InvalidValidationResponse

        query = """select d.*, t.rule_id, 
                          t.rule_instance_id, t.process_id   from
                        {detail_schema_name}.{detail_table_name} t inner join 
                        (select * from {target_schema}.{target_table}) d
                        on t.dmx_id = d.dmx_id """.format(**config_instance.__dict__)
        self.log.debug("query generated by ConstructRedshiftFailedRecordsQuery  - {}".format(query))
        services3export = RedshiftQueryExportToS3( query= query, session= self.postgres, fileprefix = config_instance.detail_table_name, parallel=False,header='',  **self.kwargs)
        services3export.execute()
        config_instance.failure_s3_path = services3export.response['filename']
        self.postgres.add(config_instance)
        self.postgres.commit()
        self.add_response('data', services3export.response['data'])
        
        
