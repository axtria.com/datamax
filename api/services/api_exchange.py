#   datamax/api/services/api_exchange.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import datetime
import re

import pandas as pd
from sqlalchemy import exc
from sqlalchemy.exc import ResourceClosedError
from sqlalchemy.sql.functions import count

from models import API, APIMeta, Connector
from services import Service
from utils.exceptions import ApiExchangeIntegrityFailed
from utils.data_quality_querybuilder import active_job_check_query,valid_job_check_query,auth_query
from utils.aws_secret_manager import get_secret
from vendor.appdb import AppDb
from vendor.redshift import Redshift
from utils.helpers import decoder
# from config import DQ_API_AUTH, REGION


class ApiExchange(Service):
    def __init__(self, session, name: str, **kwargs):
        super().__init__()
        self.name = name
        self.params = kwargs
        self.postgres = session

    def execute(self):
        for api, api_meta, connector in self.postgres.query(API, APIMeta, Connector).filter(
                API.name == self.name, API.status == 'published'). \
                join(APIMeta, APIMeta.api_id == API.id).join(Connector, Connector.id == APIMeta.connector_id).all():

            self.log.debug(api_meta.query)
            self.log.debug(self.params)

            engine = AppDb.engine
            if connector.connection_type == 'rs':
                engine = Redshift.initialize(rs_connector_id=connector.id, session=self.postgres).engine

            try:
                contents = pd.read_sql(api_meta.query, engine, params=self.params).to_dict(orient='records')
            except exc.IntegrityError as e:
                self.log.exception(e)
                raise ApiExchangeIntegrityFailed
            except ResourceClosedError:
                contents = {}
            
            self.add_response('Contents', contents)
            self.add_response('ResponseMetadata', {'HTTPStatusCode': 200, 'HTTPHeaders':
                {'date': datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), 'content-type': 'application/json',
                 'transfer-encoding': 'none', 'server': 'ApiExchange'}})
    

 
class ApiExchangeDq(Service):
    def __init__(self, session, name: str, **kwargs):
        super().__init__()
        self.name = name
        self.params = kwargs
        self.postgres = session

    def execute(self):
        for api, api_meta, connector in self.postgres.query(API, APIMeta, Connector).filter(
                API.name == self.name, API.status == 'published'). \
                join(APIMeta, APIMeta.api_id == API.id).join(Connector, Connector.id == APIMeta.connector_id).all():

            self.log.debug(api_meta.query)
            self.log.debug(self.params)

            app_db = AppDb()
            engine = app_db.engine
            if connector.connection_type == 'rs':
                engine = Redshift.initialize(rs_connector_id=connector.id, session=self.postgres).engine

            try:
                # secret = get_secret(DQ_API_AUTH,REGION)
                # username = secret["Username"]
                # password = secret["Password"]
                valid_dq_query = auth_query()
                valid_dq_query_result = app_db.execute(valid_dq_query)
                cred = valid_dq_query_result['connection'][0]
                username = cred['USERNAME']
                password = decoder(cred['PASSWORD'])

                if self.params['user'] == username and  self.params['pswd'] == password:
                    
                    jobid = self.params['jobid']
                    sql_query_valid_cnt = valid_job_check_query(jobid)
                    jobid_valid_cnt = app_db.execute(sql_query_valid_cnt)

                    sql_query_active_cnt = active_job_check_query(jobid)
                    jobid_actv_cnt = app_db.execute(sql_query_active_cnt)

                    if jobid_valid_cnt['count'][0] == 0:
                        contents = "Requested Job ID is not configured"
                    elif jobid_actv_cnt['count'][0] == 0:
                        contents = "Requested Job ID is configured but not Active"
                    else:
                        api_query_result = app_db.execute( api_meta.query,self.params)
                        contents = api_query_result.to_dict(orient='records')   
                else:
                    contents = "Authentication Error. Invalid Username or Password"
            except exc.IntegrityError as e:
                self.log.exception(e)
                raise ApiExchangeIntegrityFailed
            except ResourceClosedError:
                contents = {}

            self.add_response('Contents', contents)
            self.add_response('ResponseMetadata', {'HTTPStatusCode': 200, 'HTTPHeaders':
                {'date': datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), 'content-type': 'application/json',
                 'transfer-encoding': 'none', 'server': 'ApiExchange'}})

class ListCategory(Service):
    def __init__(self, session, **kwargs):
        super().__init__()
        self.postgres = session

    def execute(self):
        categories_exchange = pd.DataFrame(self.postgres.query(API.category, count(API.id)).
                                           filter(API.status == 'published').
                                           group_by(API.category).order_by(count(API.id).desc()).all(),
                                           columns=['category', 'count'])

        self.add_response('Contents', categories_exchange.to_dict(orient='records'))


class Summary(Service):
    def __init__(self, session, name: str, **kwargs):
        super().__init__()
        self.name = name
        self.postgres = session

    def execute(self):
        exchange_apis = []
        for _id, (api, api_meta) in enumerate(self.postgres.query(API, APIMeta).\
                                                     filter(API.category == self.name, API.status == 'published'). \
                                                     join(APIMeta, APIMeta.api_id == API.id).all()):
            parameters = set(re.findall(r'%[(\[](.*?)[)\]]\s*s', api_meta.query))
            parameters_str = ', '.join(parameters)
            url_str = '&'.join(map(lambda x: x + '={' + x + '}', parameters))
            url_str = '&' + url_str if url_str else url_str

            exchange_apis.append({'id': _id + 1, 'name': api.name, 'description': api.description,
                                  'parameters': parameters_str, 'output': '-', 'url': url_str})

        self.add_response('Contents', exchange_apis)
