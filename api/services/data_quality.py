#   datamax/api/services/data_quality.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Shivankit Bagla <shivankit.bagla@axtria.com>

import pandas as pd
import logging
from vendor.snowflake import Snowflake
from services.api_exchange import ApiExchangeDq
from snowflake.connector import connect
from snowflake.connector import errors
from services import Service
import utils.data_quality_querybuilder as query_builder

class DataQuality(Service):
    def __init__(self, session,  sf_connector_id: int,  name:str, **kwargs):
        super().__init__()
        self.log = logging.getLogger(self.__class__.__name__)
        self.kwargs = kwargs
        self.postgres = session
        sf_connector_id = sf_connector_id 
        self.name = name
        self.snowflake = Snowflake.initialize(sf_connector_id, session)
        self.results = pd.DataFrame()
        self.dv_response_repeat_flag=False
        self.dv_run_id = ''

    def execute(self):
        service1 = ApiExchangeDq( session=self.postgres, name=self.name, **self.kwargs )
        service1.execute()
        contents = service1.response['Contents']
        self.log.debug('commnds to execute in snowflake - %r' % str(contents))
        
        if isinstance(contents, str):
                temp_dict = {"0" : contents}
                self.results =self.results.append(temp_dict,ignore_index=True )

        else:
            for content in contents:
                sql_func_res = content['query']
                sfquery = sql_func_res['query']
                run_id = sql_func_res['run_id']
                job_id = sql_func_res['jobid']
                snapshot_query = sql_func_res['snapshot']
                if run_id == -1:
                    if self.dv_response_repeat_flag:
                      continue
                    else:
                       self.dv_response_repeat_flag = True
                       self.dv_run_id= sql_func_res['dv_runid']
                self.results = self.results.append(QueryHandler().execute(self.snowflake, sfquery,\
                                                    run_id, job_id, self.dv_run_id,snapshot_query ))

        self.log.debug("before the dictionary")
        self.results = self.results.reset_index(drop=True).to_dict(orient='list') 
        self.add_response('response',self.results)
        self.log.debug("after the dictionary")
        self.snowflake.conn.close()

class QueryHandler: 
    def __init__(self):  
        self.log = logging.getLogger(self.__class__.__name__)

    def query_maker(self, snowflake,run_id, job_id, dv_run_id, msg=None):
        self.log.info(dv_run_id)
        self.engine = snowflake.get_engine()
        var_schema = snowflake.schema_db_string
        
        if msg is None:
            if run_id == -1:
                tup = tuple(map(int, dv_run_id['run_id'].split(',')))
                tup_len = tup.__len__()
                var=tup[0] if tup_len == 1 else tup
                query_char = '=' if tup_len == 1 else 'in'
                sql_query = query_builder.dv_scnr_pass_query(var,var_schema,query_char)
            else:
                sql_query = query_builder.scnr_pass_query(var_schema, run_id)
            return sql_query
        else:
            di = {}
            sql_query = ''
            if (run_id == -1):
                for key, val in dv_run_id.items():
                    di[key] = str(val).split(',')
                df_postg = pd.DataFrame.from_dict(di)
                fetch_all_query = query_builder.sfk_fetch_all_query(var_schema,job_id)
                df_snowf = pd.read_sql_query(fetch_all_query, self.engine)
                df_postg['run_id'] = df_postg['run_id'].astype(int)
                df = pd.merge(df_postg, df_snowf, how='left', left_on='run_id', right_on='DQ_RUN_ID')

                for index, row in df.iterrows():
                    if pd.isna(row['DQ_RUN_ID']):
                        
                        job_id = row['job_id']
                        rule_id =  row['rule_id']
                        run_id_df = row['run_id']
                        no_run_query = query_builder.dv_scnr_no_run_query(var_schema,run_id_df,job_id,rule_id)
                        sql_query += no_run_query

                    elif row['RUN_STATUS'] == 'Started':
                        run_id_df = row['run_id']
                        query_fail_query = query_builder.dv_scnr_fail_query(var_schema,run_id_df,msg)
                        sql_query += query_fail_query

                    else:
                        run_id_df = row['run_id']
                        run_id_query = query_builder.dv_scnr_pass_run_id_query(var_schema,run_id_df)
                        sql_query += run_id_query 

            else:
                sql_query = query_builder.scnr_fail_query(var_schema,msg,run_id)

            return sql_query

    def response_setter(self,snowflake, run_id, job_id, dv_run_id, request_response=None):
        if request_response is None:
            sql_query = self.query_maker(snowflake,run_id, job_id, dv_run_id)
            # self.engine = snowflake.get_engine()
            # success_msg = self.engine.execute_string(sql_query)
            success_msg = snowflake.execute_all(sql_query)
            return success_msg
        else:
            sql_query = self.query_maker(snowflake, run_id, job_id, dv_run_id, request_response)
            # self.engine = snowflake.get_engine()
            # success_msg =  self.engine.execute_string(sql_query) 
            success_msg = snowflake.execute_all(sql_query)
            return success_msg
    
    def execute(self, snowflake, query, run_id, job_id, dv_run_id,snapshot_query):
        self.log.info(query)
        try:
            # self.engine = snowflake.get_engine()
            # self.engine.execute_string(query)
            snowflake.execute_all(query)
            success_msg = self.response_setter(snowflake,run_id, job_id, dv_run_id)
            return success_msg

        except errors.ProgrammingError as e:  
            error_res = e.msg
            sp = error_res.split("'")
            error_message = "".join(sp)
            self.log.info("Error while executing queries in snowflake :" + str(error_message))
            success_msg = self.response_setter(snowflake,run_id, job_id, dv_run_id, error_message)
            return success_msg

        finally:
            # self.engine = snowflake.get_engine()
            try:
                # snapshot_response = self.engine.execute_string(snapshot_query)
                snapshot_response = snowflake.execute_all(snapshot_query)
                self.log.info('snapshot executed successfully : ' + str(snapshot_response))
            except Exception as e:
                self.log.info("snapshot query : " + str(snapshot_query))
                self.log.info("snapshot query failed : " + str(e) )
                
