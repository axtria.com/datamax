services.data\_operations package
=================================

Submodules
----------

.. toctree::

   services.data_operations.api_docx
   services.data_operations.read
   services.data_operations.s3_buckets
   services.data_operations.s3_file_to_redshift_table

Module contents
---------------

.. automodule:: services.data_operations
   :members:
   :undoc-members:
   :show-inheritance:
