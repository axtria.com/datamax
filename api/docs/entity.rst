entity package
==============

Subpackages
-----------

.. toctree::

   entity.tests

Submodules
----------

.. toctree::

   entity.datasets
   entity.parameters
   entity.rules
   entity.scenario
   entity.selected_datasets
   entity.tags
   entity.workflow

Module contents
---------------

.. automodule:: entity
   :members:
   :undoc-members:
   :show-inheritance:
