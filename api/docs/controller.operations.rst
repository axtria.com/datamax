controller.operations module
============================

.. automodule:: controller.operations
   :members:
   :undoc-members:
   :show-inheritance:
