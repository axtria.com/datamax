.. DataMAx documentation master file, created by
   sphinx-quickstart on Wed May  8 12:50:56 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DataMAx's documentation!
===================================

***************
Getting started
***************

- Create your project
- Add connector to the project for the resources to be utilised, like S3, redshift, etc.

*********
Features
*********

- Project management
- Connectors
- Files
- Validation configuration
- Validation execution
- Data management
- Business rule management system

*******
Set up
*******

To setup the instance of the DataMax, you'll need:

- EC2
- Postgres database

Also, install all the python packages being listed in the `requirements.txt` file.

********
Modules
********



*************
Contributions
*************

- Ajaya Kumar Nayak <ajaya.nayak@axtria.com>
- Akshay Saini <akshay.saini@axtria.com>
- Kunal Bajaj <kunal.bajaj@axtria.com>
- Nitin Sharma <nitin.sharma@axtria.com>
- Shashank Senger <shashank.senger@axtria.com>
- Shilpa Gupta <shilpa.gupta@axtria.com>
- Varun Singhal <varun.singhal@axtria.com>


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
