import logging
from logging.handlers import TimedRotatingFileHandler

from sqlalchemy.orm import Session

from migration import m_01_2019_27nov_15dec, m_02_2019_16dec_25dec, m_03_2020_26dec_02feb
from vendor.appdb import AppDb

logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("logs/system.log", when="midnight", interval=1),
                              logging.StreamHandler()],
                    format='%(asctime)s  - %(levelname)s  - %(name)s - %(message)s')

if __name__ == '__main__':
    session = Session(AppDb.engine)
    m_01_2019_27nov_15dec.invoke(session)
    m_02_2019_16dec_25dec.invoke(session)
    m_03_2020_26dec_02feb.invoke(session)
