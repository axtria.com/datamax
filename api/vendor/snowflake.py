import logging

from snowflake.connector import connect
from sqlalchemy.orm import Session
from sqlalchemy import and_, or_, func
import pandas as pd
import snowflake.connector

from models import Connector, Layer
from utils.helpers import decoder


class Snowflake:

    def __init__(self, user, password, account_id, warehouse, database,connection_id,role=None,schema=None):
        self.log = logging.getLogger(self.__class__.__name__)
        self.user = user
        self.account_id = account_id
        self.database = database
        self.connection_id = connection_id
        self.schema_db_string = database + '.' + str(schema)
        self.conn = snowflake.connector.connect(user=self.user, password=password, account=self.account_id,
                                                database=self.database, warehouse=warehouse, insecure_mode=True,
                                                ocsp_fail_open=False, login_timeout=5, network_timeout=5,role=role,schema=schema)

    @classmethod
    def initialize(cls, sf_connector_id, session):
        sf_connector = session.query(Connector).filter(Connector.id == sf_connector_id).first()
        user = sf_connector.connection['USER']
        password = decoder(sf_connector.connection['PASSWORD'])
        account_id = sf_connector.connection['ACCOUNT_ID']
        database = sf_connector.connection['DATABASE']
        warehouse = sf_connector.connection.get('WAREHOUSE', None)
        role = sf_connector.connection.get('ROLE', None)
        schema = sf_connector.connection.get('SCHEMA',None)
        return cls(user=user, password=password, account_id=account_id, database=database, warehouse=warehouse,role=role,
                    schema=schema, connection_id  = sf_connector_id)

    @classmethod
    def initialize_with_layer(cls, layer_id, session):
        connection_id = session.query(Layer.connector_id).filter(and_(Layer.layer_id == layer_id , Layer.is_active == True)).first()[0]
        sf_connector = session.query(Connector).filter(Connector.id == int(connection_id)).first()
        user = sf_connector.connection['USER']
        password = decoder(sf_connector.connection['PASSWORD'])
        account_id = sf_connector.connection['ACCOUNT_ID']
        database = sf_connector.connection['DATABASE']
        warehouse = sf_connector.connection.get('WAREHOUSE', None)
        role = sf_connector.connection.get('ROLE', None)
        schema = sf_connector.connection.get('SCHEMA', None)
        return cls(user=user, password=password, account_id=account_id, database=database, warehouse=warehouse,
                   role=role,schema=schema, connection_id  = connection_id)

    def execute(self, query):
        self.log.debug(query)
        result = pd.read_sql(query, self.conn)
        return result

    def execute_all(self, query):
        self.log.debug(query)
        msg = self.conn.execute_string(query)
        return msg

    def get_engine(self):
        return self.conn

    def test(self):
        try:
            self.execute('select current_version()')
            return "Successful"
        except:
            raise Exception("Connection Failure")
