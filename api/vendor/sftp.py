#   datamax/api/vendor/sftp.py
#   Copyright (C) 2020 Axtria.
#
#   Author:
#   Neha Verma <neha.verma@axtria.com>
#   Varun Singhal <varun.singhal@axtria.com>
import logging

import pysftp
from sqlalchemy.orm import Session

from models import Connector
from utils.exceptions import SFTPConnectionFailed
from utils.helpers import decoder


class Sftp:

    def __init__(self, user: str, password: str, host: str, port: int):
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.debug(user)
        try:
            cnopts = pysftp.CnOpts()
            cnopts.hostkeys = None
            self.sftp_connector = pysftp.Connection(host=host, username=user, password=password, port=port, cnopts=cnopts)
        except Exception as e:
            self.log.exception(e)
            raise SFTPConnectionFailed

    @classmethod
    def initialize(cls, sftp_connector_id: int, session: Session):
        connector = session.query(Connector).filter(Connector.id == sftp_connector_id).first()
        return cls(user=connector.connection['USERNAME'],
                   password=decoder(connector.connection['PASSWORD']),
                   host=connector.connection['HOST'],
                   port=connector.connection['PORT'])

    def get_folders(self):
        if self.sftp_connector:
            return "Connection successfully established ... "
        else:
            return "Authentication Failed"
