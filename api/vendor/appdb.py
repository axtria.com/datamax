import logging

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.exc import ResourceClosedError
from sqlalchemy.orm import Session

from config import SQLALCHEMY_DATABASE_URI, POOL_SIZE


class AppDb:
    try:
        engine = create_engine(SQLALCHEMY_DATABASE_URI,  isolation_level="AUTOCOMMIT", pool_size=POOL_SIZE)
        status = True
    except Exception as e:
        logging.exception(e)
        status = False

    def __init__(self):
        """Singleton behavior to avoid multiple instances of engine to form for a given database.

        The configuration is received from the SQLALCHEMY_DATABASE_URL which is being set in the `config.py`.
        This configuration file is also used by the Flask SQLAlchemy to connect to the postgres db. Silently kill
        the exception, in case credentials are incorrect, as there are chances that UI is created to
        feed in the new credentials.

        """
        self.log = logging.getLogger(self.__class__.__name__)

    def execute(self, query,param=None):
        session = Session(AppDb.engine)
        self.log.debug(query)
        try:
            return pd.read_sql(query, AppDb.engine, params=param)
        except ResourceClosedError:
            return None
        finally:
            session.close()

            
    def insert(self, df: pd.DataFrame, table_name, schema, index=False, index_label=None, dtype = None):
        self.log.debug("inserting %r records in %r . %r" %(len(df), schema, table_name))
        df.to_sql(name=table_name, con=AppDb.engine, schema=schema, if_exists='append',index=index, index_label=index_label, dtype = dtype )
