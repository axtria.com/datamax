import logging
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.exc import ResourceClosedError
from sqlalchemy.orm import Session
from models import Connector
from utils.exceptions import RedshiftConnectionFailed
from utils.helpers import decoder


class Redshift:
    """Redshift class to connect and execute queries on Redshift cluster.
    Credentials are taken from config file and connected to cluster whenever object is called.
    No explicit operator required for commit DML statements as autocommit is True.

    """
    numeric_dtypes = ['smallint', 'integer', 'bigint', 'numeric', 'real', 'double precision']

    def __init__(self, user, password, host, port, dbname):
        self.log = logging.getLogger(self.__class__.__name__)
        try:
            self.engine = create_engine("postgresql+psycopg2://" + user + ":" + password
                                        + "@" + host + ":" + port + "/" +
                                        dbname, isolation_level="AUTOCOMMIT")
        except Exception as e:
            self.log.exception(e)
            raise RedshiftConnectionFailed

    @classmethod
    def initialize(cls, rs_connector_id: int, session: Session):
        rs_connector = session.query(Connector).filter(Connector.id == rs_connector_id).first()
        return cls(user=rs_connector.connection['USERNAME'], password=decoder(rs_connector.connection['PASSWORD']),
                   host=rs_connector.connection['HOST'], port=rs_connector.connection['PORT'],
                   dbname=rs_connector.connection['DBNAME'])

    @classmethod
    def initialize_with_connector(cls, rs_connector: Connector):
        return cls(user=rs_connector.connection['USERNAME'], password=decoder(rs_connector.connection['PASSWORD']),
                   host=rs_connector.connection['HOST'], port=rs_connector.connection['PORT'],
                   dbname=rs_connector.connection['DBNAME'])

    def execute(self, query):
        self.log.debug(query)
        try:
            return pd.read_sql(query, self.engine)
        except ResourceClosedError:
            return None


