import logging

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.exc import ResourceClosedError
from sqlalchemy.orm import Session

from models import Connector
from utils.exceptions import PostgresConnectionFailed
from utils.helpers import decoder


class Postgres:
    """Postgres class to connect and execute queries on postgres database.
    """

    def __init__(self, user, password, host, port, dbname):
        self.log = logging.getLogger(self.__class__.__name__)
        try:
            self.engine = create_engine("postgres://" + user + ":" + password
                                        + "@" + host + ":" + port + "/" +
                                        dbname, isolation_level="AUTOCOMMIT")
        except Exception as e:
            self.log.exception(e)
            raise PostgresConnectionFailed

    @classmethod
    def initialize(cls, pg_connector_id: int, session: Session):
        pg_connector = session.query(Connector).filter(Connector.id == pg_connector_id).first()
        return cls(user=pg_connector.connection['USERNAME'], password=decoder(pg_connector.connection['PASSWORD']),
                   host=pg_connector.connection['HOST'], port=pg_connector.connection['PORT'],
                   dbname=pg_connector.connection['DBNAME'])

    @classmethod
    def initialize_with_connector(cls, pg_connector: Connector):
        return cls(user=pg_connector.connection['USERNAME'], password=decoder(pg_connector.connection['PASSWORD']),
                   host=pg_connector.connection['HOST'], port=pg_connector.connection['PORT'],
                   dbname=pg_connector.connection['DBNAME'])

    def execute(self, query):
        self.log.debug(query)
        try:
            return pd.read_sql(query, self.engine)
        except ResourceClosedError:
            return None
