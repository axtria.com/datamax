#   datamax/api/vendor/neo4j.py
#   Copyright (C) 2019 Axtria.
#
#   Author(s):
#   Akshay Saini <akshay.saini@axtria.com>
#   Nitin Sharma <nitin.sharma@axtria.com>
#   Varun Singhal <varun.singhal@axtria.com>
import json
import logging
from datetime import datetime
from os import system

import pandas as pd
import py2neo as n4j

from config import NEO4J_URI, NEO4J_USER, NEO4J_PASSWORD, NEO4J_IMPORT_DIR


class Neo4j:
    def __init__(self):
        """
        Initializes neo4j instance from ConfigNeo4j class.
        
        Examples:
            >>> neo4j = Neo4j()
        """
        self.log = logging.getLogger(self.__class__.__name__)
        self.conn = n4j.Graph(uri=NEO4J_URI, auth=(NEO4J_USER, NEO4J_PASSWORD))

    def run_query(self, query: str, return_objects: bool = False) -> pd.DataFrame or list:
        """
        Runs cypher query and returns the output in a form of `pd.DataFrame` or list of neo4j objects/records. 
        
        Parameters:
            query : valid cypher query

        Returns:
            return_objects : if True output of the query will be in the form of list of records/objects
                             if False output of the query will be in the form of `pd.DataFrame`
            
        Examples:
            >>> neo4j = Neo4j()
            >>> nodes_df = neo4j.run_query('match(n:Validation) return n.code, n.id, n.description')
            >>> relationships = neo4j.run_query('match(()-[r]->()) return r', True)
        """
        self.log.debug(query)
        output = self.conn.run(query)
        return output.data() if return_objects else output.to_data_frame()

    def create_node(self, label: str, merge: bool = True, **attributes) -> n4j.Node:
        """
        Creates a single node of provided label and attributes.
        If there's already a node presents having the same label and attributes then by
        default it won't create a new node. For a duplicate node, set `merge` to False.

        Attributes
        label : label of the node
        
        merge : if True new node will be created iff it's going to be unique node by considering label and attributes
                if False new node wil always be created and may lead to duplicate nodes.
                
        **attributes : attributes/properties of the node
            
        Examples:
            >>> n4j_conn = Neo4j()
            >>> node1 = n4j_conn.create_node('Validation',rule_id=1, name='unique_check')
            >>> node2 = n4j_conn.create_node('Table', name='Physicians')
        """
        mode = 'merge' if merge else 'create'
        parsed_attributes = self._parse_attributes(attributes)
        cypher = f'{mode}(n:{label}{parsed_attributes}) return n'
        out = self.run_query(cypher, return_objects=True)
        node_obj = out.pop()['n']
        return node_obj

    def create_relationship(self, start_node: n4j.Node, end_node: n4j.Node, label: str, merge=True,
                            **attributes) -> n4j.Relationship:
        """
        Creates a relationship between provided start_node and end_node with the provided attributes.
        If the given relationship is already present in the graph, then by default this method won't make a duplicate relationship.
        For duplicate relationship set `merge` to False
        
        Parameters
        ----------
        start_node : starting node of 
        
        label : label of the node
        
        merge : if True new node will be created iff it's going to be unique node by considering label and attributes
                if False new node wil always be created and may lead to duplicate nodes.
                
        **attributes : attributes/properties of the node
            
        Examples
        --------
        ex. rel1 = n4j_conn.create_relationship(node1, node2, 'ColumnValidation', createdby='admin)
        """
        mode = 'merge' if merge else 'create'

        parsed_attributes = self._parse_attributes(attributes)

        cypher = f'match(n1) where id(n1) = {start_node.identity} with n1 match(n2) where id(n2) = {end_node.identity} with n1,n2 {mode}((n1)-[r:{label}{parsed_attributes}]->(n2)) return r'

        out = self.run_query(cypher, return_objects=True)

        relationship_obj = out.pop()['r']

        return relationship_obj

    def delete_nodes(self, *nodes: n4j.Node) -> int:
        """
        Deletes nodes
        
        Parameters
        ----------
                
        *nodes : unwrapped list of nodes that'd be deleted
            
        Examples:
            >>> n4j_conn = Neo4j()
            >>> delete_count = n4j_conn.delete_nodes(node1, node2, node3, node4,...., nodeN)
        
            >>> nodes = [node1, node2, node3, node4,...., nodeN]
            >>> delete_count = n4j_conn.delete_nodes(*nodes)
        """

        ids = repr([node.identity for node in nodes])  # Fetching internal ids of nodes
        cypher = f'match(n) where id(n) in {ids} detach delete n return count(n) as del_count'
        out = self.run_query(cypher, return_objects=True)
        del_count = out.pop()['del_count']
        return del_count

    def delete_relationships(self, *relationships: n4j.Relationship) -> int:
        """
        Deletes realtionships
        
        Parameters
        ----------
                
        *relationships : unwrapped list of relationships that'd be deleted
            
        Examples
        --------
        delete_count = n4j_conn.delete_relationships(relationship1, relationship2,...., relationshipN)
        
        relationships = [relationship1, relationship2, relationship3,...., relationshipN]
        delete_count = n4j_conn.delete_relationships(*relationships)
        """

        ids = repr([relationship.identity for relationship in relationships])

        cypher = f'match(()-[r]->()) where id(r) in {ids} delete r return count(r) as del_count'

        out = self.run_query(cypher, return_objects=True)

        del_count = ['del_count']

        return del_count

    def get_node_by_id(self, _id):
        out = self.run_query(f'match(n) where id(n) = {_id} return n', return_objects=True)
        node = out.pop()['n']
        return node

    def get_nodes(self, label: str = None, return_objects=False, **attributes) -> pd.DataFrame or list:
        """
        Fetches nodes of provided label and attributes from the graph.
        
        Parameters
            label : label of the nodes. `None` for all labels.
            return_objects : if True output of the query will be in the form of list of records/objects
            if False output of the query will be in the form of `pd.DataFrame`.
            attributes : attributes/properties of the node
            
        Examples:
            >>> n4j_conn = Neo4j()
            >>> tags = n4j_conn.get_nodes('Tag')
            >>> current_students = n4j_conn.get_nodes('Student', return_objects = True, batch = 'current')
        """
        node_matcher = n4j.NodeMatcher(self.conn)

        if label:
            nodes = list(node_matcher.match(label, **attributes))
        else:
            nodes = list(node_matcher.match(**attributes))

        if return_objects:
            return nodes

        df = pd.DataFrame(nodes)
        df['__id'] = [node.identity for node in nodes]
        df['__label'] = [list(node.labels)[0] for node in nodes]
        return df

    def get_child_nodes(self, node_id: int, r_type: str, child_type: str):
        cypher = f'match(n)-[r:{r_type}]->(x:{child_type}) where id(n) = {node_id} return properties(r),x.name'
        child_data = []
        for i in self.run_query(cypher, return_objects=True):
            try:
                data = i['properties(r)']
                data.update({'name': i['x.name']})
                child_data.append(data)
            except Exception as e:
                self.log.exception(e)
        return child_data

    def get_relationships(self, label: str = None, return_objects=False, nodes=None,
                          **attributes) -> list or pd.DataFrame:
        """
        Fetches relationships of provided label and attributes from the graph.
        
        Parameters:
            label : label/type of the relationships. `None` for all labels/types.
            return_objects : if True output of the query will be in the form of list of records/objects
                             if False output of the query will be in the form of `pd.DataFrame`.
            nodes: Sequence or Set of start and end nodes (None means any node);
                   a Set implies a match in any direction
            attributes : attributes/properties of the relationships
            
        Examples:
            >>> n4j_conn = Neo4j()
            >>> relationships = n4j_conn.get_relationships('KNOWS', True, created_date = '20-12-2018')
        """
        self.__init__()
        relationship_matcher = n4j.RelationshipMatcher(self.conn)

        if label:
            relationships = list(relationship_matcher.match(r_type=label, nodes=nodes, **attributes))
        else:
            relationships = list(relationship_matcher.match(nodes=nodes, **attributes))

        if return_objects:
            return relationships

        df = pd.DataFrame(relationships)
        df['__start_node'] = [relationship.start_node for relationship in relationships]
        df['__end_node'] = [relationship.end_node for relationship in relationships]
        df['__id'] = [relationship.identity for relationship in relationships]
        df['__label'] = [list(relationship.types())[0] for relationship in relationships]
        return df

    def create_nodes_from_csv(self, abspath: str, label: str, column_attribute_mapping: dict,
                              column_dtype_mapping: dict, merge=True) -> int:
        """
        Loads nodes from a csv file.
        Assumption: by default all dtypes will be str.
        
        Parameters
        ----------
                
        abspath : abspath of csv file to be loaded. file should be ',' delimitted
        
        label : label of the to be uploaded data
                         
        column_attribute_mapping : mapping of csv columns to node attributes. Columns that are not present this mapping will be skipped.
        
        column_dtype_mapping : mapping of csv columns to their corresponding dtype. default dtype is `str`
        
        merge : if True new node will be created iff it's going to be unique node by considering label and attributes
                if False new node wil always be created and may lead to duplicate nodes.
            
        Examples
        --------

        success_count = n4j_conn.create_nodes_from_csv(r'C:\data.csv', 'Validation', {'id':'rule_id','description':'desc'}, {'id':int, description':str})
        """
        abspath = abspath.replace('/', '\\')
        fname = abspath.split('\\')[-1]

        destination_fname = datetime.now().strftime("%m_%d_%Y__%H_%M_%S_%f") + '___' + fname
        destination_path = NEO4J_IMPORT_DIR + destination_fname

        cp_command = f'copy "{abspath}" "{destination_path}"'
        system(cp_command)

        cols_map_query_str = []

        for col, attribute in column_attribute_mapping.items():

            if col in column_dtype_mapping.keys():
                if column_dtype_mapping[col] == int:
                    tmp = attribute + ' : toInteger(line.' + col + ')'
                if column_dtype_mapping[col] == float:
                    tmp = attribute + ' : toFloat(line.' + col + ')'
                if column_dtype_mapping[col] == bool:
                    tmp = attribute + ' : toBoolean(line.' + col + ')'
            else:
                tmp = attribute + ' :line.' + col

            cols_map_query_str.append(tmp)

        cols_map_query_str = '{' + ', '.join(cols_map_query_str) + '}'

        mode = 'merge' if merge else 'create'

        cypher = f'LOAD CSV WITH HEADERS FROM "file:///{destination_fname}" AS line {mode} (n:{label}{cols_map_query_str}) return count(n) as node_count'
        out = self.run_query(cypher, True)

        node_count = out.pop()['node_count']
        return node_count

    def create_multiple_relationship(self, label: str, start_label: str = None, end_label: str = None,
                                     start_on: list or str = None, end_on: list or str = None, merge=True,
                                     **attributes) -> int:
        """
        Adds relationship on the basis of label and/or attributes mat
        
        Parameters
        ----------

        label : label of the to be created relationships
                         
        start_label : starting node's label of the new formed relationship
        
        end_label : end node's label of the new formed relationship
        
        start_on : attribute or list of attributes of start_node, which'd be used as key 
        
        end_on : attribute or list of attributes of end_node, which'd be used as key 
        
        merge : if True new relationship will be created iff it's going to be unique relationship by considering label and attributes
                if False new relationship wil always be created and may lead to duplicate nodes.
            
        **attributes : attributes/properties of the relationships
        
        Examples
        --------

        success_count = n4j_conn.create_multiple_relationship('PLAYS', 'Person', 'Movie', 'id', 'actor_id', True, enabled = True)
        """

        if isinstance(start_on, str) and isinstance(end_on, str):
            start_on = [start_on]
            end_on = [end_on]

        match_part = 'match (s' + (f':{start_label}),(e' if start_label else '),(e') + (
            f':{end_label}) ' if end_label else ') ')

        mode = 'merge' if merge else 'create'
        parsed_attributes = self._parse_attributes(attributes) if attributes else ''

        main_part = f' with s,e {mode} (s)-[r:{label}{parsed_attributes}]->(e) return count(r) as relationship_count'

        if start_on and end_on:
            condition_part = 'where ' + ' and '.join(['s.' + s + ' = ' + 'e.' + e for s, e in zip(start_on, end_on)])
            cypher = match_part + condition_part + main_part
        else:
            cypher = match_part + main_part

        out = self.run_query(cypher, True)
        relationship_count = out.pop()['relationship_count']

        return relationship_count

    def update_node_attributes(self, *nodes: n4j.Node, **attributes):
        """
        updates attributes of nodes
        
        Parameters
        ----------
        nodes : unwrapped list of nodes that'd be updated
                         
        **attributes : attributes/properties of the nodes
        
        Examples
        --------

        n4j_conn.update_node_attributes(node1, node2, node3, node4, enabled=True, last_enabled_on='20-12-2018')
        """
        ids = [node.identity for node in nodes]
        parsed_attributes = self._parse_attributes(attributes)

        cypher = f'match (n) where id(n) in {ids} set n += {parsed_attributes} return n'
        out = self.run_query(cypher, True)

        nodes = [element['n'] for element in out]

        return nodes

    def update_relationship_attributes(self, *relationships: n4j.Relationship, **attributes):
        """
        updates attributes of relationships
        
        Parameters
        ----------

        relationships : unwrapped list of relationships that'd be updated
                         
        **attributes : attributes/properties of the relationships
        
        Examples
        --------

        n4j_conn.update_node_attributes(node1, node2, node3, node4, enabled=True, last_enabled_on='20-12-2018')
        """
        ids = [relationship.identity for relationship in relationships]
        parsed_attributes = self._parse_attributes(attributes)

        cypher = f'match (()-[r]->()) where id(r) in {ids} set r += {parsed_attributes} return r'
        out = self.run_query(cypher, True)

        relationships = [element['r'] for element in out]

        return relationships

    def remove_orphan_nodes(self, label: str = None, **attributes) -> int:
        """
        Removes nodes which have no connections.
        if label and attributes are given then it fitlers out on the basis of label and attributes and then deletes the filtered nodes.
        
        Parameters
        ----------
        label : label of the nodes of which orphan nnodes will be deleted
                         
        **attributes : attributes/properties of the nodes
        
        Examples
        --------

        deleted_orphan_count = n4j_conn.remove_orphan_nodes()
        
        """
        label = f':{label}' if label else ''
        attributes = self._parse_attributes(attributes) if attributes else ''

        cypher = f'MATCH (n{label}{attributes}) WHERE NOT (n)--() DELETE n RETURN COUNT(n) as orphan_count'

        out = self.run_query(cypher, True)
        orphan_count = out.pop()['orphan_count']

        return orphan_count

    def merge_duplicate_nodes(self, label: str = None, attributes: list = []) -> int:
        """
        Merge duplicate nodes on basis of same label or/and attributes.
        If no label or attributes are given nodes which have same label and exact same attribute will be merged.
        
        Parameters
        ----------
        label : label of the nodes 
                         
        **attributes : attributes/properties of the nodes
        
        Examples
        --------

        merge_count = n4j_conn.merge_duplicate_nodes()
        
        """
        label = f':{label}' if label else ''
        partial_query = ['n.' + attribute + ' as ' + attribute for attribute in attributes]

        partial_query.append(
            'COLLECT(n) AS nodelist, COUNT(*) AS __count WHERE __count > 1 CALL apoc.refactor.mergeNodes(nodelist) YIELD node RETURN count(node) as merge_count')

        partial_query = ','.join(partial_query)

        cypher = f'match(n{label}) with {partial_query}'

        out = self.run_query(cypher, True)
        merge_count = out.pop()['merge_count']

        return merge_count

    def merge_duplicate_relationships(self, label: str = None, attributes: list = []) -> int:
        """
        Merge duplicate relationships on basis of same label or/and attributes.
        If no label or attributes are given relationships which have same label and exact same attribute will be merged.
        
        Parameters
        ----------
        label : label of the relationships
                         
        **attributes : attributes/properties of the nodes
        
        Examples
        --------

        merge_count = n4j_conn.merge_duplicate_relationships()
        
        """
        label = f':{label}' if label else ''
        partial_query = ['r.' + attribute + ' as ' + attribute for attribute in attributes]

        partial_query.append(
            'COLLECT(r) AS rlist, COUNT(*) AS __count WHERE __count > 1 CALL apoc.refactor.mergeRelationships(rlist) YIELD rel RETURN count(rel) as merge_count')

        partial_query = ','.join(partial_query)

        cypher = f'match(()-[r{label}]->()) with {partial_query}'

        out = self.run_query(cypher, True)
        merge_count = out.pop()['merge_count']

        return merge_count

    def subgraph(self, *nodes: n4j.Node) -> n4j.Subgraph:
        """
        Returns subgraph of the given nodes.
        
        Parameters
        ----------
        *nodes : unwrapped list of nodes
        
        Examples
        --------

        sub = n4j_conn.subgraph(node1, node2)        
        """
        node_ids = [node.identity for node in nodes]
        cypher = f'match((s)-[r]-(e)) where id(s) in {node_ids} and id(e) in {node_ids} return r'
        relationships = self.run_query(cypher, True)
        relationships = [element['r'] for element in relationships]
        subgraph = n4j.Subgraph(nodes, relationships)
        return subgraph

    def view(self, node: n4j.Node, degree=1) -> n4j.Subgraph:
        """
        Returns view/neighbours of a node in a form of subgraph
        
        Parameters
        ----------
        node : center node of the view
        
        degree : degree of neighbours around the center node
        
        Examples
        --------
        view = n4j_conn.view(node1, 2)   
        """
        nodes = {node}
        relationships = set()
        ids = {node.identity}

        for deg in range(degree):
            cypher = f'match((n)-[r]-(m)) where id(n) in {list(ids)} return r,m'
            out = self.run_query(cypher, True)

            nodes |= {element['m'] for element in out}
            relationships |= {element['r'] for element in out}
            ids |= {element['m'].identity for element in out}

        subgraph = n4j.Subgraph(nodes, relationships)
        return subgraph

    def shortest_path(self, start_node: n4j.Node, end_node: n4j.Node) -> n4j.Path:
        """
        Returns shortest path between start_node and end_node using dijkstra algorithm
        
        Parameters
        ----------
        node : center node of the view
        
        degree : degree of neighbours around the center node
        
        Examples
        --------
        shortest_path = n4j_conn.shortest_path(node1, node2)   
        """
        cypher = f'match (start),(end) where id(start) = {start_node.identity} and id(end) = {end_node.identity} CALL apoc.algo.dijkstra(start, end, "","") yield path, weight return path'
        out = self.run_query(cypher, True)

        path = out.pop()['path']
        return path

    def _parse_attributes(self, attributes: dict) -> str:
        """
        Parses dictionary into neo4j format attributes.
        
        ex. d = {'att1':'lorem', 'val':9}
            parsed_att = self._parse_attributes(d)
            print(parsed_att)
            
            #out
            {att:"lorem", val:9}
        """
        parsed_attributes = json.dumps(attributes)
        for key in attributes.keys():
            parsed_attributes = parsed_attributes.replace('"' + key + '":', key + ':')
        return parsed_attributes

    def get_vis_js_json(self, subgraph: n4j.Subgraph) -> dict:

        nodes = list(subgraph.nodes)
        relationships = list(subgraph.relationships)

        nodes_df = pd.DataFrame()
        nodes_df['id'] = [node.identity for node in nodes]
        nodes_df['label'] = [f"<b>{list(node.labels)[0]}</b>\n\n{self._get_properties(node)}" for node in nodes]
        nodes_df['color'] = [self._string_to_light_color(list(node.labels)[0]) for node in nodes]
        nodes_df['font'] = [{'multi': 'html', 'align': 'left'} for _ in range(len(nodes_df))]

        try:
            coordinates = pd.read_json('cache/coordinates.json').transpose()
            nodes_df = nodes_df.merge(coordinates, left_on='id', right_index=True, how='left')
        except:
            pass

        edges_df = pd.DataFrame()
        edges_df['from'] = [edge.start_node.identity for edge in relationships]
        edges_df['to'] = [edge.end_node.identity for edge in relationships]
        edges_df['label'] = [f"<b>{set(edge.types()).pop()}</b>\n{self._get_properties(edge)}" for edge in
                             relationships]
        edges_df['color'] = [{'color': 'black'} for _ in range(len(edges_df))]
        edges_df['smooth'] = [{'enabled': False} for _ in range(len(edges_df))]
        edges_df['font'] = [{'multi': 'html', 'align': 'left', 'size': 12} for _ in range(len(edges_df))]
        edges_df['arrows'] = [{'to': {'enabled': True}} for _ in range(len(edges_df))]

        data = {}
        data['nodesJSON'] = nodes_df.to_json(orient='records')
        data['edgesJSON'] = edges_df.to_json(orient='records')

        return data

    def get_import_dir(self):
        return NEO4J_IMPORT_DIR

    def _get_properties(self, x):
        s = [f'{i} 🡆 {j}' for i, j in x.items()]
        return '\n'.join(s)

    def _string_to_light_color(self, s):
        H = str(abs(hash(s.lower()))).zfill(9)
        r = hex(100 + int(H[0:3]) % 155)[2:]
        g = hex(100 + int(H[3:6]) % 155)[2:]
        b = hex(100 + int(H[6:9]) % 155)[2:]
        color = '#' + r + g + b
        return color

    def check_node(self,id):
        output = self.conn.run(f'match (n) where id(n)={id} return n')
        df = output.to_data_frame()
        return True if(len(df)>0) else False

