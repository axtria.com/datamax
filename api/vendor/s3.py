#   datamax/api/vendor/s3.py
#   Copyright (C) 2019 Axtria.
#
#   Author(s):
#   Neha Verma <neha.verma@axtria.com>
#   Varun Singhal <varun.singhal@axtria.com>
import gzip
import io
import logging
import zipfile
from io import BytesIO
import os
import boto3
import pandas as pd
import s3fs
from sqlalchemy.orm import Session
from models import Connector
from utils.exceptions import S3FileReadError
from utils.helpers import decoder
from datetime import datetime

class S3:

    def __init__(self, access_key: str, secret_key: str, role: str):
        """S3 connection is established for reusable purpose, using the profile which is requested by
        the client. Types of connection involve -

            1. boto3 based client connection
            2. file system based connection

        :param profile: the environment to be used to establish the connection
        :func: ConfigS3
        """
        self.log = logging.getLogger(self.__class__.__name__)
        if access_key == '':
            self.client = boto3.client('s3')
            self.fs_connection = s3fs.S3FileSystem()
            self.resource = boto3.resource('s3')
        else:
            self.client = boto3.client('s3',
                                       aws_access_key_id=access_key,
                                       aws_secret_access_key=secret_key)
            self.fs_connection = s3fs.S3FileSystem(key=access_key, secret=secret_key)
            self.resource = boto3.resource('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_key)
        self.access_key = access_key
        self.secret_key = secret_key
        self.role_name = role

    @classmethod
    def initialize(cls, connector_id, session: Session):
        s3_connector = session.query(Connector).filter(Connector.id == connector_id).first()
        return cls(access_key=s3_connector.connection['ACCESS_KEY'],
                   secret_key=decoder(s3_connector.connection['SECRET_KEY']),
                   role=s3_connector.connection['ROLE'])

    @classmethod
    def initialize_with_access_key(cls, access_key, secret_key):
        return cls(access_key=access_key,
                   secret_key=decoder(secret_key),
                   role='')

    def copy(self, source, target):
        """Copy file from source bucket/key to the given destination. It is assumed that both the buckets are
        accessible using the credentials mentioned in the INI file.
        """
        self.client.copy({'Bucket': source['bucket'], 'Key': source['key']}, target['bucket'], target['key'])

    def obj_bucket(self, bucket_name):
        keys = []

        resp = self.client.list_objects_v2(Bucket=bucket_name)

        for obj in resp['Contents']:
            keys.append(obj['Key'])
        return keys

    def analyse(self, bucket: str, key: str):
        return self.client.head_object(Bucket=bucket, Key=key)

    def upload_file_to_s3(self, file, bucket_name, location):
        """Uploads the given file to specified destination(bucket and prefix is given). It is assumed that both the
        buckets are accessible using the credentials mentioned in the INI file.
        Args : file location
                bucket
                prefix : inside the bucket path
        """
        keys = self.get_keys(bucket_name, location)
        logging.debug("keys %s" % keys)
        flag = 0
        for i in keys:
            if i == (location + str(file).split('\\')[-1:][0]):
                flag = 1
            else:
                pass
        if flag == 0:
            try:
                self.client.upload_file(file, bucket_name, location + str(file).split('\\')[-1:][0])
                return "true"
            except FileNotFoundError:

                return "false"
        else:
            return "file with similar name already present.Please try again"

    def download(self, bucket, folder, file):
        flag = 0
        keys = self.get_keys(bucket, folder)
        download_folder = os.path.expanduser("~")+ '//Downloads//'
        ts = datetime.now().strftime('%Y_%m_%d__%H_%M_%S_%f')
        logging.debug("Download path  %s" % download_folder)
        for k in keys:
            if k == (folder + file):
                self.client.download_file(bucket,folder + file,download_folder+ts+file)
                flag = 1
            else:
                pass
        if flag == 1:
            return "Success"
        else:
            return "Fail"

    def remove(self, bucket, folder, file):
        flag = 0
        keys = self.get_keys(bucket, folder )
        for k in keys:
            if k == (folder +  file):
                obj = self.resource.Object(bucket, folder + file)
                obj.delete()
                flag = 1
            else:
                pass
        if flag == 1:
            return "Success"
        else:
            return "Fail"

    def read(self, filename, rows):
        """Read the specific no. of rows from the S3 file.

        This utility makes sure that the file is not downloaded on the application server. This method
        calls the actual implementation based on the file extension. It is based on the
        principle of least knowledge. This allows to avoid tight coupling between clients and subsystems.

        :Author: Varun Singhal

        Args:
            filename (str): filename with extension
            rows (int): no. of rows to read.

        Returns:
            list: comma separated list items where each item is the content of the row.
        """
        if filename.endswith('.gz'):
            return self._read_gz(filename, rows)

        else:
            return self._read_default(filename, rows)

    def _read_default(self, filename, rows) -> list:
        records = []
        self.log.debug("Reading %s for %d rows" % (filename, rows))

        try:
            with self.fs_connection.open(filename, 'rb') as f:
                for _ in range(rows):
                    records.append(f.readline().decode('utf-8'))
        except Exception as e:
            self.log.exception(e)
            raise S3FileReadError

        self.log.debug(records)
        return records

    def _read_gz(self, filename, rows) -> list:
        records = []
        self.log.debug("Reading %s for %d rows" % (filename, rows))

        try:
            with gzip.open(self.fs_connection.open(filename, 'rb'), 'rt') as f:
                for _ in range(rows):
                    records.append(f.readline())
        except Exception as e:
            self.log.exception(e)
            raise S3FileReadError

        self.log.debug(records)
        return records

    def get_keys(self, bucket, folder_prefix) -> list:
        """This utility will return list of all the keys present in the specified bucket and prefix.
        """
        paginator = self.client.get_paginator("list_objects")
        if folder_prefix == '/':
            page_iterator = paginator.paginate(Bucket=bucket)
        else:
            page_iterator = paginator.paginate(Bucket=bucket, Prefix=folder_prefix)
        bucket_object_list = []
        for page in page_iterator:
            if "Contents" in page:
                for key in page["Contents"]:
                    keyString = key["Key"]
                    bucket_object_list.append(keyString)

        return bucket_object_list

    def explorer(self, bucket, prefix, no_of_files) -> list:
        self.log.debug("Explorer %s/%s for %d rows" % (bucket, prefix, no_of_files))
        r = self.client.list_objects_v2(Bucket=bucket, Prefix=prefix)
        return list(map(lambda y: y["Key"], filter(lambda x: x["Size"] != 0, r["Contents"])))[: no_of_files]

    def read_excel(self, bucketname, filename, count) -> list:
        self.log.debug("Reading %s for 5 rows" % filename)
        try:
            obj = self.client.get_object(Bucket=bucketname, Key=filename)
            data = obj['Body'].read()
            records = [pd.read_excel(io.BytesIO(data), encoding='utf-8', nrows=count).columns.fillna(
                '').values.tolist()] + \
                      pd.read_excel(io.BytesIO(data), encoding='utf-8', nrows=count).fillna('').values.tolist()
        except Exception as e:
            self.log.exception(e)
            raise S3FileReadError
        self.log.debug(records)
        return records

    def read_flat_files(self, bucketname, filename, rows):
        self.log.debug("Explorer %s/%s for %d rows" % (bucketname, filename, 5))
        bucket = self.resource.Bucket(bucketname)
        records = []
        i = 0
        for obj in bucket.objects.filter(Prefix=filename):
            for line in obj.get()['Body']._raw_stream:
                if i < rows:
                    records.append(line.decode('utf-8-sig'))
                    i = i + 1
                else:
                    break
            return records

    def put_object(self, bucket, key, resultout):
        return self.client.put_object(Bucket=bucket, Key=key, Body=resultout)

    def get_objects(self, bucket, prefix):

        s3_bucket = self.resource.Bucket(bucket)
        objects = s3_bucket.objects.filter(Prefix=prefix)
        return objects

    def get_buckets(self, bucket):
        s3_bucket = self.resource.Bucket(bucket)
        return s3_bucket

    def last_modified_time(self, bucket, prefix):
        suffix = ''
        resp = self.client.list_objects_v2(Bucket=bucket, Prefix=prefix)

        for obj in resp['Contents']:
            key = obj['Key']
            lastmodified = obj['LastModified']
        return lastmodified

    def s3_file_upload(self, bucket, location, file_obj, override):
        fi = file_obj.filename
        keys = self.get_keys(bucket, location)
        self.log.debug("List of all the Keys %s" % keys)
        flag = 0
        for i in keys:
            if i == (location + '/' + fi):
                flag = 1
            else:
                pass
        if flag == 0:
            self.log.debug("Uploading.......")
            self.client.upload_fileobj(file_obj,  bucket, location + "/" + fi,
                                       {"ServerSideEncryption":"aws:kms"})
            return "Success"
        else:
            self.log.debug("Key already exists prompt message for override")
            if override == 'True':
                self.client.upload_fileobj(file_obj, bucket, location + "/" + fi,{"ServerSideEncryption":"aws:kms"})
                self.log.debug("Successful Upload")
                return "Success"
            else:
                return "Fail"

    def get_zip_files(self, bucket, filename):
        records = []
        self.log.debug("opening %s/%s file" % (bucket, filename))
        obj = self.resource.Object(bucket_name=bucket, key=filename)
        buffer = zipfile.ZipFile(BytesIO(obj.get()["Body"].read()))
        for i in buffer.namelist():
            if i.endswith('/'):
                pass
            else:
                records.append(i)
        return records

    def read_zip(self, bucket, folder_prefix, file_name, act_filename, count_rows):

        zip_obj = self.resource.Object(bucket_name=bucket, key=folder_prefix + '/' + file_name)
        buffer = BytesIO(zip_obj.get()["Body"].read())
        z = zipfile.ZipFile(buffer)
        for zip_files in z.namelist():
            if zip_files == act_filename:
                file_info = z.getinfo(zip_files)
                try:
                    self.resource.meta.client.upload_fileobj(
                        z.open(zip_files),
                        Bucket='datamax1',
                        Key=f'{zip_files}'
                    )
                    records = zip_files
                except:
                    return "File not found"
            else:
                pass
        return records

    def read_fwf(self, bucket, folder_prefix, delimiter, column_names, file_name, count):
        with self.fs_connection.open(bucket + "/" + folder_prefix +  file_name) as f:
            index = []
            column_n = []
            for i in delimiter.split(','):
                index.append(int(i))
            for j in column_names.split(','):
                column_n.append(j)
            s3_file = pd.read_fwf(f, engine='python', widths=index, header=None, names=column_n,
                                  keep_default_na=False, na_values=['', 'null', 'NULL', '(Empty)'], na_filter=True,
                                  encoding='UTF-8', nrows=count).fillna('')

            return s3_file


    '''
    def read_excel_gz(self,bucket,filename):
        rows=5
        LOCAL_PATH = '\\temp\\' + os.path.basename(filename)

        try:
            self.resource.Bucket(bucket).download_file(filename, LOCAL_PATH)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise
        shutil.unpack_archive(LOCAL_PATH, extract_dir='\\temp\\', format='gztar')
        filelist = glob.glob(LOCAL_PATH[0:-2] + '*')
        print(filelist)
        if os.path.exists(LOCAL_PATH[0:-2] + 'xls'):
            records = pd.read_excel(bytearray(LOCAL_PATH[0:-2] + 'xls'), header=None).fillna('').values.tolist()

        if os.path.exists(LOCAL_PATH[0:-2] + 'xlsx'):
            records = pd.read_excel(LOCAL_PATH[0:-2] + 'xlsx' , header=None, nrows=rows).fillna('').values.tolist()
        if os.path.exists(LOCAL_PATH[0:-2] + 'csv'):
            records = pd.read_csv(LOCAL_PATH[0:-2]+'csv' , header= None,nrows=5).fillna('').astype(
                str).values.tolist()
        if os.path.exists(LOCAL_PATH[0:-2] + 'txt' ):
            records = pd.read_csv(LOCAL_PATH[0:-2] + 'txt' , header=None, nrows=5).fillna('').astype(
                str).values.tolist()
        filelist = glob.glob(LOCAL_PATH[0:-2] + '*')
        #print(filelist)
        for filePath in filelist:
            try:
                os.remove(filePath)
            except:
                print("Error while deleting file : ", filePath)

        return records

    def gz_file_extension( self,filename,bucketname):
        self.log.debug("opening %s/%s file" % (bucketname, filename))
        rows=5
        bucket = self.resource.Bucket(bucketname)
        LOCAL_PATH = '\\temp\\' + os.path.basename(filename)

        try:
            self.resource.Bucket(bucketname).download_file(filename, LOCAL_PATH)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise

        shutil.unpack_archive(LOCAL_PATH, extract_dir='\\temp\\', format='gztar')
        print(LOCAL_PATH[0:-2])
        filelist = glob.glob(LOCAL_PATH[0:-2] + '*')
        filelist = filelist[:-1]
    '''
    def list_buckets(self):
        """Fetch the buckets in s3.
        Parameters:
        Returns:
            List: 
        To list the buckets in s3 connection.
        """
        buckets = self.client.list_buckets()
        return buckets["Buckets"]