#   datamax/api/utils/brms_grammar.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import logging

from flashtext import KeywordProcessor
from lark import Lark, Tree

from entity.datasets import Datasets

log = logging.getLogger(__name__)

parser = Lark("""
        start: "filter"i TABLE "using columns"i columns "where"i condition -> filter_rule
             | "filter"i TABLE "using columns"i columns -> filter_column
             | "filter"i TABLE "where"i condition -> filter_row
             | "derive columns in"i TABLE "with"i column_expressions -> derived
             | "analytical derive columns in"i TABLE "using"i ANALYTICAL_FUNCTION "on"i COLUMN "of type"i DTYPE "as"i COLUMN "partitioned by"i columns "in"i ORDER_BY "order of"i COLUMN -> analytical
             | "join"i TABLE join_rules "with distinct to get"i table_columns -> join_distinct
             | "join"i TABLE join_rules "to get"i table_columns -> join
             | "aggregate"i TABLE "using"i columns "to derive"i column_expressions -> aggregate
             | "aggregate"i TABLE "using"i columns "to derive"i column_expressions "having"i condition -> aggregate_having
             | "dedupe"i TABLE "on"i columns -> dedupe
             | "union"i TABLE "and"i TABLE -> union
             | "union"i TABLE "using"i columns "and"i TABLE "using"i columns -> union_filter
             | "union all"i TABLE "and"i TABLE -> union_all
             | "union all"i TABLE "using"i columns "and"i TABLE "using"i columns -> union_all_filter
             | "partition"i TABLE "to derive"i partition_expressions -> partition
             | "custom"i PROCEDURE "with"i parameters "to get"i custom_columns -> custom

        columns: (COLUMN ","?)+

        table_columns: (TABLE_COLUMN ","?)+

        condition:  "(" COLUMN OPERATOR VALUE ")" ","?
                | "({" FORMULA "}" OPERATOR VALUE ")" ","?
                | "or("i condition+  ")" ","? -> or_rule
                | "and("i condition+ ")" ","? -> and_rule

        column_expressions: column_expression+
        column_expression: BASIC_FORMULA "as"i COLUMN "of type"i DTYPE ","? -> formula
                | "{" FORMULA "} as"i COLUMN "of type"i DTYPE ","? -> formula
                | case_expression "as"i COLUMN "of type"i DTYPE ","? -> case
        
        partition_expressions: partition_expression+
        partition_expression: BASIC_FORMULA "as"i COLUMN "over ("i columns ")"i -> partition_formula

        case_expression: "case"i when_then+ "end"i
        when_then: "when"i condition "then"i BASIC_FORMULA -> when_rule 
                | "when"i condition "then {"i FORMULA "}" -> when_rule
                | "else"i BASIC_FORMULA -> else_rule
                | "else {"i FORMULA "}" -> else_rule

        join_rules: join_rule+ -> join_rules
        join_rule: "and"i TABLE "using"i JOIN_TYPE "join on"i join_expressions -> join_rule

        join_expressions: join_expression+
        join_expression: TABLE_COLUMN JOIN_OPERATOR TABLE_COLUMN ","? -> join_expr

        parameters: parameter+
        
        parameter: BASIC_PARAMETER ","?
                | "{" PARAMETER "}" ","?
        
        custom_columns: custom_column+
        
        custom_column: COLUMN "of type"i DTYPE ","? -> new_custom_column
                | TABLE "." COLUMN ","? -> existing_custom_column
                

        TABLE: /[0-9a-zA-Z_-//]+/
        COLUMN: /[0-9a-zA-Z#_*//-//]+/
        OPERATOR: /([<>=!]+)|(is (not)?)|((not)? like)/i
        JOIN_OPERATOR: /[<>=!]+/i
        VALUE: /[0-9a-zA-Z_'\$ \-\/.//%#]+/
        FORMULA: /[0-9a-zA-Z#%_[\]\*//(//)//+//-\/'\.//\$ ]+/
        BASIC_FORMULA: /[0-9a-zA-Z#_*//(//)//+//-\/'\.//\$]+/
        JOIN_TYPE: "left"i|"right"i|"outer"i|"inner"i
        TABLE_COLUMN: /[0-9a-zA-Z#_*//.//-//]+/
        LEFT_TABLE: TABLE
        RIGHT_TABLE: TABLE
        PROCEDURE: TABLE_COLUMN
        BASIC_PARAMETER: /[0-9a-zA-Z_#'.//\$\-\/=]+/
        PARAMETER: /[0-9a-zA-Z_#'.//\$ =,]+/
        ANALYTICAL_FUNCTION: /[0-9a-zA-Z_]+/
        ORDER_BY: "asc"i|"desc"i
        DTYPE: "date"i|"numeric"i|"varchar"i|"integer"i|"double"i|"boolean"i|"float"i|"number"i|"text"i


        %import common.LETTER
        %import common.INT -> NUMBER
        %import common.WS
        %ignore WS
    """)

keywords = {
    "numeric": ["number"],
    "varchar": ["text"]
}

keyword_processor = KeywordProcessor()
keyword_processor.add_keywords_from_dict(keywords)


def case_treatment(tree: Tree, datasets: Datasets, dataset_name=""):
    if tree.data == 'custom':
        return tree
    for child in tree.children:
        if isinstance(child, Tree):
            case_treatment(child, datasets, dataset_name)
        else:
            if child.type == 'TABLE':
                dataset_name = datasets.resolve_table_name(child.value)
                child.value = dataset_name
            if child.type == 'COLUMN':
                child.value = datasets.resolve_column_name(dataset_name, child.value)
            if child.type == 'TABLE_COLUMN':
                table_name, column_name = child.value.split('.')
                table_name = datasets.resolve_table_name(table_name)
                column_name = datasets.resolve_column_name(table_name, column_name)
                child.value = table_name + '.' + column_name
    return tree


def tokenize(text, datasets: Datasets):
    text = keyword_processor.replace_keywords(text)
    tokens = parser.parse(text)
    tokens = case_treatment(tokens, datasets)
    log.debug(tokens)
    return tokens
