import json
import logging
import time
from functools import wraps
from threading import Thread

from cryptography.fernet import Fernet

from utils.exceptions import InvalidJSON


def timer(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        f = func(*args, **kwargs)
        end = time.time()
        logging.info("%s - %s : %4.2f second(s)" % (args[0].__class__.__name__, func.__name__, (end - start)))
        return f

    return wrapper


def debugger(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        logging.info("===================== %s.%s() ====================" % (args[0].__class__.__name__, func.__name__))
        logging.info("(%r,%r)" % (args, kwargs))
        f = func(*args, **kwargs)
        logging.info(" %r " % f)
        logging.info("=======================================================================================")
        return f

    return wrapper


def thread(func):
    """
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        logging.info("=============== Thread: %s.%s() ================" % (args[0].__class__.__name__, func.__name__))
        thread_func = Thread(target=func, args=args, kwargs=kwargs, daemon=True)
        thread_func.start()
        return thread_func

    return wrapper


def validate_json_structure(input_json: str) -> dict:
    try:
        return json.loads(input_json)
    except Exception as e:
        logging.exception(e)
        raise InvalidJSON


f = Fernet('mmExmaYZBo2IFlv9c2_4NpeL8RoRDlyvHqo90WVYXxI=')


def decoder(token: str) -> str:
    if len(token) != 0:
        return f.decrypt(token.encode()).decode()
    return token


def encoder(token: str) -> str:
    return f.encrypt(token.encode()).decode()