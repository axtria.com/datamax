import json
from glob import glob

import pandas as pd

standard_objects = []

for so_path in glob('../../knowledgebase/so.*.json'):
    so = json.load(open(so_path, 'r'))
    df = pd.DataFrame(so['attributes'])
    df['src_name'] = so['src_name']
    df['object_name'] = so['object_name']
    df['path'] = so_path
    standard_objects.append(df)

standard_objects = pd.concat(standard_objects).reset_index(drop=True)

__column_alias = json.load(open('../../knowledgebase/column_alias.json', 'r'))
column_alias = {k: i for i, j in __column_alias.items() for k in j}


def get_confidence_level(actual_column_names: list):
    raw_column_df = pd.DataFrame({'raw_col': actual_column_names})
    raw_column_df['raw_col'] = raw_column_df['raw_col'].str.lower()
    raw_column_df['alias'] = raw_column_df['raw_col'].map(column_alias)

    _df = pd.merge(raw_column_df, standard_objects, left_on='alias', right_on='actual_col_name', how='left')
    out = _df.groupby(['src_name', 'object_name'])['path'].count().reset_index().sort_values('path', ascending=False)
    out['confidence'] = out['path'] * 100 / out['path'].sum()
    out['match_percentage'] = out['path'] * 100 / len(actual_column_names)
    out.rename(columns={'path': 'col_match_count'}, inplace=True)
    out = out.sort_values(by='confidence', ascending=False)
    return out.to_dict(orient='records')
