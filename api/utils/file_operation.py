import csv
from encoding_tools import TheSoCalledGreatEncoder

_TEXT_QUALIFIERS = [',', '|','*', ';', ':', '\t','"']
_DATE_SEPERATOR = ['-',':','/']


def get_file_extension(file_name):
    file_ext = (str(file_name).split('.')[-1])
    if file_ext == file_name:
        return 'STANDARD FILE'
    return file_ext


def get_file_delimiter(sample_records):
    sniffer = csv.Sniffer()
    dialect = sniffer.sniff(str(sample_records[1]))
    return dialect.delimiter


def get_file_encoding(sample_records, file_type='D'):
    encoder = TheSoCalledGreatEncoder()

    if file_type == 'F':
        for i in sample_records.columns:
            encoder.load_str(str(sample_records[i]))
    else :
        encoder.load_str(str(sample_records[1]))
    return encoder.encoding


def get_text_qualifier(sample_records):

    detected_text_qualifier = sample_records[1][0]
    flag = 0
    for t in _TEXT_QUALIFIERS:
        if detected_text_qualifier == t:
            flag=1
        else:
            flag = 2
    if flag == 1:
        return detected_text_qualifier.strip()
    else:
        return 'None'


def get_date_seperator(row):
    detected_sep = None
    max_count = 0
    for sep in _DATE_SEPERATOR:
        sep_count = row.count(sep)
        if 0 <= max_count < sep_count:
            detected_sep = sep
            max_count = sep_count
    return detected_sep
