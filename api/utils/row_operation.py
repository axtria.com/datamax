_DELIMITERS = [',', '|', ';', ':', '\t']


def inspect_delimiter(row: str):
    detected_delimiter = None
    max_count = 0
    for delimiter in _DELIMITERS:
        delimiter_count = row.count(delimiter)
        if 0 <= max_count < delimiter_count:
            detected_delimiter = delimiter
            max_count = delimiter_count
    return detected_delimiter


def inspect_columns(row: str, delimiter: str) -> list:
    return row.strip().split(delimiter)
