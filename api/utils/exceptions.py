class CustomException(Exception):
    message = "Something went wrong"


class ConfigInstanceNotFound(CustomException):
    message = """Raised when config instance we are looking for is not found in the system"""
    pass


class RuleDefinitionNotFound(CustomException):
    message = """Raised when rule definition we are looking for is not found in the system"""
    pass


class RuleInstanceNotFound(CustomException):
    message = """Raised when the rule instance is not found in the system."""
    pass


class ConfigNotFound(CustomException):
    message = """Raised when config id provided is not present in the system"""
    pass


class PostgresConfigurationNotFound(CustomException):
    message = """Raised when postgres connection settings are not found in the INI file"""
    pass


class PostgresConnectionFailed(CustomException):
    message = """Raised when postgres connection is not authenticated successfully."""
    pass


class RedshiftConfigurationNotFound(CustomException):
    message = """Raised when redshift connection settings are not found in the INI file"""
    pass


class RedshiftConnectionFailed(CustomException):
    message = """Raised when redshift connection is not authenticated successfully."""
    pass


class SFTPConnectionFailed(CustomException):
    message = """Raised when SFTP connection is not authenticated successfully."""
    pass


class S3ConfigurationNotFound(CustomException):
    message = """Raised when S3 connection settings are not found in the INI file"""
    pass


class S3FileReadError(CustomException):
    message = """Raised when file is not found at S3 or system is unable to read the file using the credentials."""
    pass


class InvalidValidationResponse(CustomException):
    message = """Raised when you are attempting to populate success/failed records for S3 validation"""
    pass


class InvalidValidationResponseRerun(CustomException):
    message = """Raised when you are attempting to re-run success/failed records"""
    pass


class DuplicateConfigFound(CustomException):
    message = """Raised when you are attempting to add same config again into the system"""
    pass


class InvalidConfigFound(CustomException):
    message = """Raised when you enter invalid config to be processed in the system"""
    pass


class RuleTypeNotFound(CustomException):
    message = """Raised when using out-dated config, rule type - user defined not found in the given config id."""
    pass


class InvalidJSON(CustomException):
    message = """Raised when JSON provided is illegal."""
    pass


class ApiExchangeIntegrityFailed(CustomException):
    message = """Duplicate found. Integrity failed."""
    pass


class BRMSParameterNotFound(CustomException):
    message = """Define the parameter first before operating on it."""
    pass


class BRMSDatasetNotFound(CustomException):
    message = """Define dataset first before get operation on it."""
    pass


class BRMSRuleNotFound(CustomException):
    message = """Define rule first before any operations on it"""
    pass


class BRMSInvalidRule(CustomException):
    pass


class BRMSDuplicateRuleId(CustomException):
    message = """Duplicate request to add the same rule Id."""
    pass


class BRMSPublishedWorkflowModified(CustomException):
    message = """Not allowed to modify the published workflow"""
    pass


class BRMSDraftWorkflowExecuted(CustomException):
    message = """Execution not allowed on DRAFT workflow"""
    pass


class BRMSDateFormatUnsupported(CustomException):
    message = """Date format mentioned is not yet supported."""
    pass


class BRMSDatasetInUse(CustomException):
    message = """Output of the rule is in use by other rules"""
    pass


class BRMSMergingStrategyNotDefined(CustomException):
    message = """Merger strategy not defined in the BRMS Grammar"""
    pass


class BRMSDuplicateColumnFound(CustomException):
    message = """Duplicate column found"""
    pass


class BRMSDuplicateWorkflowName(CustomException):
    message = """Duplicate workflow name found."""
    pass


class BRMSInactiveScenarioExecuted(CustomException):
    message = """Inactive scenario can not be executed."""
    pass


class BRMSEmptyWorkflowPublished(CustomException):
    message = """No rules found in the workflow."""
    pass


class BRMSRuleIntegrityFailed(CustomException):
    """Raised when rule integrity fails."""

    def __init__(self, message):
        super().__init__(message)
        self.message = message


class BRMSInvalidDatasetName(CustomException):
    message = """Invalid dataset name."""
    pass


class TagAlreadyExist(CustomException):
    message = """Tag already exists within the system."""
    pass


class TagNotFound(CustomException):
    message = """Tag not found in the system."""
    pass


class S3ConnectionTestFailed(CustomException):
    message = """connection failed"""
    pass


class SFTPConnectionTestFailed(CustomException):
    message = """connection failed"""
    pass


class SnowflakeConnectionTestFailed(CustomException):
    message = """connection failed"""
    pass

class DataLayerNotFound(CustomException):
    message = """No data layer found in the system."""
    pass

class GlossaryNotFound(CustomException):
    message = """Specified node ID not found"""
    pass

class GlossaryAlreadyExist(CustomException):
    message = """Dataset already exists"""
    pass

class UnsupportedImportedFile(CustomException):
    message = "Received an empty file or not a valid file"
    pass

class UnsupportedDateFormat(CustomException):
    message = "Received date is not in date format"
    pass

class NoDataAvailable(CustomException):
    message = "No Data available"
    pass

class ReadOnlyTag(CustomException):
    message = "Tag is not modifiable"
    pass

class ReadOnlyDataAsset(CustomException):
    message = "DataAsset is already in use"
    pass