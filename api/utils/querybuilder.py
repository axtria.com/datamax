EXTERNAL_SCHEMA = 'spectrum'


def load_query(schema: str, table_name: str, location: str, delimiter: str) -> str:
    query = """
            copy {schema}.{tablename} from '{location}' 
            access_key_id '{access_key}'
            secret_access_key '{secret_key}'
            delimiter {delimiter};
            """.format(schema=schema, tablename=table_name, location=location, delimiter=delimiter,
                       access_key='{access_key}', secret_key='{secret_key}')
    return query


def create_external_query(table_name: str, columns: list, delimiter: str, location: str, file_format: str,
                          tbl_properties: list) -> str:
    tbl_properties = ','.join(tbl_properties)
    query = 'create external table {external_schema}.{table_name} (' + \
            ' , '.join([column + ' varchar ' for column in columns]) + \
            ") row format delimited " + \
            "fields terminated by '{delimiter}' stored as {file_format} location '{location}' "
    return query.format(table_name=table_name, delimiter=delimiter, file_format=file_format, location=location,
                        external_schema=EXTERNAL_SCHEMA, tbl_properties=tbl_properties)


def copy_query(new_schema: str, new_table_name: str, old_schema: str, old_table_name: str, extra='',
               columns: list = None) -> str:
    if columns is None:
        query = create_query_without_schema(new_schema=new_schema, new_table_name=new_table_name, old_schema=old_schema,
                                            old_table_name=old_table_name, extra=extra)
    else:
        query = create_query_with_schema(new_schema=new_schema, new_table_name=new_table_name, old_schema=old_schema,
                                         old_table_name=old_table_name, columns=columns, extra=extra)
    return query


def create_query_without_schema(new_schema: str, new_table_name: str, old_schema: str, old_table_name: str,
                                extra='') -> str:
    query = """create table {new_schema}.{new_table_name} as
                select {extra} * from {old_schema}.{old_table_name}"""
    return query.format(new_schema=new_schema, new_table_name=new_table_name, old_schema=old_schema,
                        old_table_name=old_table_name, extra=extra)


def create_query_with_schema(new_schema: str, new_table_name: str, old_schema: str, old_table_name: str, columns: list,
                             extra='') -> str:
    query = 'create table {new_schema}.{new_table_name} (' + \
            '{extra}' + \
            ' , '.join([column + ' varchar ' for column in columns]) + \
            ') ;' + \
            ' insert into {new_schema}.{new_table_name} ( ' + \
            ' , '.join([column for column in columns]) + \
            ' ) ' + \
            'select * from {old_schema}.{old_table_name} ;'

    return query.format(new_schema=new_schema, new_table_name=new_table_name, old_schema=old_schema,
                        old_table_name=old_table_name, extra=extra, columns=columns)


def unload_query(schema, table_name, location, parallel, header, access_key, secret_key) -> str:
    query = """UNLOAD('select * from {schema}.{table}')
    TO '{location}'
    parallel {parallel}
    access_key_id '{access_key}'
    secret_access_key '{secret_access}'
    allowoverwrite
    {header}
    """.format(schema=schema, table=table_name, location=location, parallel=parallel, header=header,
               access_key=access_key, secret_access=secret_key)
    return query

def unload_export_query(query, location, parallel, header, access_key, secret_key) -> str:
    query = """UNLOAD('{query}')
    TO '{location}'
    parallel {parallel}
    access_key_id '{access_key}'
    secret_access_key '{secret_access}'
    allowoverwrite
    header {header}
    """.format(query=query, location=location, parallel=parallel, header=header,
               access_key=access_key, secret_access=secret_key)
    return query


def column_metadata_query(schema: str, table_name: str) -> str:
    query = """select *, column_name as name from svv_columns
                where table_name = '{tablename}' and table_schema = '{schema}';
            """.format(schema=schema, tablename=table_name)
    return query


def table_metadata_query(schema: str, table_name: str) -> str:
    query = """select * from svv_tables
            where table_name = '{tablename}' and table_schema = '{schema}';
            """.format(tablename=table_name, schema=schema)
    return query


def duplicate_query(schema: str, table_name: str, column_names: list) -> str:
    query = """select count(*) as duplicate_count from {schema}.{table_name} group by {columns} having count(*) > 1""" \
        .format(schema=schema, table_name=table_name, columns=','.join(column_names))
    return query


def null_count_query(schema: str, table_name: str, column_names: list) -> str:
    query = "select count(*) from {schema}.{table_name} where " + \
            " and ".join(map(lambda x: x + " is null", column_names))
    return query.format(schema=schema, table_name=table_name)


def record_count_query(schema: str, table_name: str):
    return "select count(*) as record_count from {schema}.{table_name}".format(schema=schema, table_name=table_name)


def statistics_numeric_query(schema: str, table_name: str, column_name: str) -> str:
    query = "select sum({column_name}) as sum, min({column_name}) as min, max({column_name}) as max, " \
            "avg({column_name}) as avg, stddev(cast({column_name} as float)) as stddev, " \
            "sum(case when {column_name} is null then 1 else 0 end) as null_count, " \
            "count(distinct {column_name}) as unique_count " \
            "from {schema}.{table_name}".format(column_name=column_name, schema=schema, table_name=table_name)
    # "percentile_disc(0.25) within group (order by {column_name}) over() as percentile_25," \
    # "percentile_disc(0.50) within group (order by {column_name}) over() as percentile_50," \
    # "percentile_disc(0.75) within group (order by {column_name}) over() as percentile_75 " \
    return query


def statistics_varchar_query(schema: str, table_name: str, column_name: str) -> str:
    query = "select  " \
            "sum(case when {column_name} is null then 1 else 0 end) as null_count, " \
            "count(distinct {column_name}) as unique_count " \
            "from {schema}.{table_name}".format(column_name=column_name, schema=schema, table_name=table_name)
    return query


def unique_value_query(schema: str, table_name: str, column_name: str) -> str:
    query = "select distinct({column_name}) as name from {schema}.{table_name}"
    return query.format(schema=schema, table_name=table_name, column_name=column_name)


def create_table_query(schema: str, table_name: str, columns: list):
    query = "create table if not exists {schema}.{table_name} ({columns_str})"
    return query.format(schema=schema, table_name=table_name,
                        columns_str=','.join(map(lambda x: x[0] + ' ' + x[1], columns)))

def suggestion(q):
    return f"""CALL db.index.fulltext.queryNodes('suggestion','{q}*') yield node, score
            return node.name as name, node.logi_name as logi_name, node.uri as uri, node.layer as layer limit 10
            union
            CALL db.index.fulltext.queryNodes('suggestion','{q}') yield node, score
            return node.name as name, node.logi_name as logi_name, node.uri as uri, node.layer as layer limit 10"""

