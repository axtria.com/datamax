#   datamax/api/utils/catalog_builder.py
#   Copyright (C) 2020 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import enum
import json
from datetime import datetime


class NodeType(enum.Enum):
    layer = 'Layer'
    object = 'Object'
    s3_file = 'S3File'
    s3_column = 'S3FileColumn'


def __build_properties(attributes: dict) -> str:
    parsed_attributes = json.dumps(attributes)
    for key in attributes.keys():
        parsed_attributes = parsed_attributes.replace('"' + key + '":', key + ':')
    return parsed_attributes


def __build_where_clause(label, attributes: dict) -> str:
    parsed_attributes = []
    for key, value in attributes.items():
        if isinstance(value, str):
            parsed_attributes.append(label + "." + key + " = '" + value + "'")
        else:
            parsed_attributes.append(label + "." + key + " = " + str(value) + "")
    return " AND ".join(parsed_attributes)


def __build_unique_key(unique_keys: dict) -> str:
    parsed_attributes = json.dumps(unique_keys)
    for key in unique_keys.keys():
        parsed_attributes = parsed_attributes.replace('"' + key + '":', key + ':')
    return parsed_attributes


def __create_node(label: str, unique_key: dict, **attributes):
    query = "MERGE (n:" + label + " " + __build_unique_key(unique_key) + ") " + \
            "SET n += " + __build_properties(attributes)
    return query


def __create_relationship(parent_node, child_node, parent_properties: dict, child_properties: dict,
                          relationship_type='HAS'):
    query = "MATCH (a:" + parent_node + " ),(b:" + child_node + ") " + \
            "WHERE " + __build_where_clause('a', parent_properties) + " " + \
            "AND " + __build_where_clause('b', child_properties) + " " + \
            "MERGE (a)-[r:" + relationship_type + "]->(b)" + \
            "RETURN r"
    return query


def add_layer(layer_id, name, connector_id, schema_name, **kwargs):
    return __create_node(NodeType.layer.value, unique_key={'layer_id': layer_id}, name=name, connector_id=connector_id,
                         schema_name=schema_name, **kwargs)


def add_onboarded_object(obj_id, obj_nm, conn_id=0, schema_name='', **kwargs):
    return __create_node(NodeType.object.value, unique_key={'obj_id': obj_id}, name=obj_nm,
                         conn_id=conn_id, schema_name=schema_name, **kwargs)


def link_onboarded_object_with_layer(obj_id, connection_id, schema_name):
    return __create_relationship(NodeType.layer.value, NodeType.object.value,
                                 parent_properties={'schema_name': schema_name, 'connector_id': connection_id},
                                 child_properties={'schema_name': schema_name, 'conn_id': connection_id,
                                                   'obj_id': obj_id})


# def add_s3_file(conn_id, file_inst_id, file_nm, **kwargs):
#     return __create_node(NodeType.s3_file.value, unique_key={'file_inst_id': file_inst_id}, name=file_nm,
#                          deleted=False, detectedColumns=True, connector_id=conn_id, **kwargs)


def link_s3_file_with_onboarded_object(file_inst_id, object_id=0, **kwargs):
    # object_id = 0 if not object_id else object_id
    return __create_relationship(NodeType.object.value, NodeType.s3_file.value,
                                 parent_properties={'obj_id': object_id},
                                 child_properties={'obj_id': object_id, 'file_inst_id': file_inst_id})


def add_meta_attribute(attr_id, **kwargs):
    return __create_node(NodeType.s3_column.value, unique_key={'attr_id': attr_id}, **kwargs)


def link_meta_attribute_with_object(attr_id, obj_id):
    return __create_relationship(NodeType.object.value, NodeType.s3_column.value,
                                 parent_properties={'obj_id': obj_id},
                                 child_properties={'attr_id': attr_id}, relationship_type='HasColumn')


def link_object_with_s3_file(object_id, bucket_name, object_key):
    return __create_relationship(NodeType.object.value, NodeType.s3_file.value,
                                 parent_properties={'obj_id': object_id},
                                 child_properties={'bucketName': bucket_name, 'objectKey': object_key})


def update_s3_file(bucket_name, object_key, **kwargs):
    return __create_node(NodeType.s3_file.value, unique_key={'bucketName': bucket_name,
                                                             'objectKey': object_key}, **kwargs)

def get_snowflake_from_postgres(sf_layer_id):
    return f"""select
            DLM.schema_name as table_schema,DLM.data_lyr_name as layer_name,DLM.data_lyr_id as layer_id,
            OM.obj_nm as table_name,'BASE TABLE' as table_type,'' as table_remarks,
            ATRM.attr_nm as column_name,ATRm.attr_data_typ as column_datatype,ATRM.col_loc as column_ordinal_position,
            ATRM.attr_desc as column_remarks,OM.obj_uri_id as uri,OM.version as version,
            OM.obj_id as obj_id,OM.audit_insrt_dt as created_date,OM.audit_updt_dt as modified_date
            from system_config.object_meta as OM
            join system_config.data_layer_meta as DLM on DLM.data_lyr_id = OM.src_data_layer_id
            join system_config.attr_meta as ATRM on OM.obj_uri_id = ATRM.obj_uri_id
            where DLM.schema_name is not null and DLM.lyr_type = 'sf' and DLM.data_lyr_id = {sf_layer_id}"""

def get_snowflake_data(database,schemas_tuple):
    return f"""select t.table_schema as "table_schema", t.table_name as "table_name", t.table_type as "table_type", 
                    t.comment as "table_remarks",t.last_altered as "modified_date", t.CREATED as "created_date", 
                    c.column_name as "column_name", c.data_type as "column_datatype", c.ordinal_position as 
                    "column_ordinal_position",c.comment as "column_remarks" from 
                    "{database}"."INFORMATION_SCHEMA"."TABLES" 
                    t join "{database}"."INFORMATION_SCHEMA"."COLUMNS" c on 
                    (t.table_name = c.table_name and t.table_schema = c.table_schema) where t.table_schema in {schemas_tuple};"""

def insert_snowflake_to_neo4j(sf,row,params,latest_ts):
    return f"""merge(a:SnowflakeAccount{{name:"{sf.account_id}"}}) set a +=  {{deleted:false}} 
              merge(sf:Snowflake{{database:"{sf.database}", account:"{sf.account_id}"}}) set sf +=  {{deleted:false, connector_id: "{sf.connection_id}"}}
              merge(s:SnowflakeSchema{{name:"{row.table_schema}", database:"{sf.database}", account:"{sf.account_id}"}}) set s +=  {{last_updated_ts:"{latest_ts}", deleted:false, layer:"{row.layer_name}", remarks:"{row.table_remarks}", layer_id: "{row.layer_id}"}}
              merge(t:SnowflakeTable{{uri: "{row.uri}"}}) set t +=  {{last_updated_ts:"{latest_ts}",created_date:"{row.created_date}",modified_date:"{row.modified_date}",deleted:false, name:"{row.table_name}", schema:"{row.table_schema}", type:"{row.table_type}", database:"{sf.database}", account:"{sf.account_id}", remarks:"{row.table_remarks}", layer_id: "{row.layer_id}", connector_id: "{sf.connection_id}",obj_id:"{row.obj_id}",version:"{row.version}",layer:"{row.layer_name}"}}
              merge(c:SnowflakeColumn{{name:"{row.column_name}", parent_uri_id: "{row.uri}"}}) set c +=  {{deleted:false,  table:"{row.table_name}", schema:"{row.table_schema}", database:"{sf.database}", account:"{sf.account_id}", remarks:"{row.column_remarks}", ordinal_position:{row.column_ordinal_position}, datatype:"{row.column_datatype}",attr_id:"{row.attr_id}"}}
              merge((a)-[:HAS]->(sf))
              merge((sf)-[:HAS]->(s))
              merge((s)-[:HAS]->(t))
              merge((t)-[:HAS]->(c))
              merge(x:Glossary{{name:"BusinessGlossary"}})
              merge(tag:Tag{{name:"Needs_Attention",status:"true"}}) set tag +=
              {{created_on: "",user_id: "",creator_user_name: "",subcategory:"Others",
              description:"Needs Attention for new onboardig objects",
              modified_on: "{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
              modifier_user_id: "{params.get('loggedin_userid')}",
              modifier_user_name: "{params.get('loggedin_username')}"}}
              merge((x)-[:HAS]->(tag))
              merge((t)-[:TagLink]->(tag))"""

def get_data_layer_meta(layer_id):
    return f"""select distinct schema_name as table_schema, data_lyr_id as layer_id, data_lyr_name as layer_name from 
                                                system_config.data_layer_meta 
                                                where lyr_type = 'sf' and data_lyr_id = {layer_id}"""

def get_object_metadata(obj_id=None,layer_id=None):
    if obj_id is not None and layer_id is None:
        where = "OM.is_actv = true and ATRM.is_actv = true and OM.obj_id = "+obj_id
    if obj_id is None and layer_id is not None:
        where = "DLM.lyr_type = 's3' and OM.is_actv = true and ATRM.is_actv = true and DLM.data_lyr_id = "+layer_id
    return f"""select
            DLM.data_lyr_name as layer_name,
            DLM.data_lyr_id as layer_id,
            OM.obj_nm as object_name,
            OM.obj_typ as object_type,
            OM.obj_desc as desc,
            OM.has_header as header,
            OM.obj_extn as extension,
            OM.obj_encoding as encoding,
            OM.freq as frequency,
            OM.cntry_cd as country_code,
            OM.lang_cd as language_code,
            OM.obj_uri_id as uri,
            OM.version as version,
            OM.obj_id as obj_id,
            OM.busns_un_cd as busns_un_cd,
            OM.audit_insrt_dt as created_date,
            coalesce (cast(OM.audit_updt_dt as varchar),'') as modified_date,
            concat(UM.first_name,' ',UM.last_name) as owner,
            ATRM.attr_nm as column_name,
            ATRM.attr_data_typ as column_datatype,
            ATRM.col_loc as column_ordinal_position,
            ATRM.attr_desc as column_remarks,
            ATRM.attr_id as attr_id
            from system_config.object_meta as OM
            join system_config.user_meta as UM on UM.user_id = cast (OM.audit_insrt_id as int) 
            join system_config.data_layer_meta as DLM on DLM.data_lyr_id = OM.src_data_layer_id
            join system_config.attr_meta as ATRM on OM.obj_id = ATRM.obj_id
            where """+ where


def insert_object_metadata_to_neo4j(row,latest_ts,params):
    return f"""merge(t:Object{{uri:"{row.uri}"}}) set t += {{last_updated_ts:"{latest_ts}",deleted:false, name: "{row.object_name}",layer:"{row.layer_name}",
                                                                    object_type :"{row.object_type}",desc:"{row.desc}",
                                                                    header:"{row.header}",extension:"{row.extension}",
                                                                    encoding:"{row.encoding}",country_code:"{row.country_code}",
                                                                    language_code:"{row.language_code}",version:"{row.version}",
                                                                    business_unit_code:"{row.busns_un_cd}",created_date:"{row.created_date}",
                                                                    modified_date:"{row.modified_date}",owner:"{row.owner}"}}
                              merge(c:ObjectColumn{{parent_uri:"{row.uri}", attr_id:"{row.attr_id}"}}) set c +=  {{deleted:false, name: "{row.column_name}",datatype:"{row.column_datatype}"}}
                              merge((t)-[:HAS]->(c)) 
                              merge(x:Glossary{{name:"BusinessGlossary"}})
                              merge(tag:Tag{{name:"Needs_Attention",status:"true"}}) set tag +=
                              {{created_on: "",user_id: "",creator_user_name: "",description:"",
                              modified_on: "{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
                              modifier_user_id: "{params.get('loggedin_userid')}",
                              modifier_user_name: "{params.get('loggedin_username')}"}}
                              merge((x)-[:HAS]->(tag))
                              merge((t)-[:TagLink]->(tag))"""


def get_object_abslt_path(obj_id=None,layer_id=None):
    if obj_id is not None and layer_id is None:
        where = "OM.obj_id = "+obj_id
    if obj_id is None and layer_id is not None:
        where = "DLM.lyr_type = 's3' and OM.is_actv = true and DLM.data_lyr_id = "+layer_id
    return f"""select DLM.data_lyr_name as layer_name,DLM.data_lyr_id as layer_id,DLM.s3_path as bucket_name,
            OM.obj_nm as object_name,OM.obj_uri_id as uri,OM.version as version,
            OM.obj_id as obj_id,concat(ODLM.abslt_path,'/',FIP.part_nm) as object_key
            from system_config.object_meta as OM
            join system_config.data_layer_meta as DLM on DLM.data_lyr_id = OM.src_data_layer_id
            join system_config.file_instnc as FI on FI.obj_id = OM.obj_id 
            join system_config.object_data_layer_meta as ODLM on ODLM.obj_id = OM.obj_id 
            join system_config.file_instnc_part as FIP on FIP.file_inst_id = FI.file_inst_id 
            where """+where