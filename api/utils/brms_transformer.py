#   datamax/api/utils/brms_transformer.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import re

from lark import Transformer

from entity.datasets import Datasets
from utils.exceptions import BRMSRuleIntegrityFailed


class TreeToJson(Transformer):
    def __init__(self, datasets: Datasets):
        super().__init__()
        self.datasets = datasets

    def filter_rule(self, tree):
        output = {}
        if 'condition' not in tree[-1]:
            output['rules'] = [tree[-1]]
        else:
            output.update(tree[-1])
        if '*' in tree[1]['columns']:
            output_metadata = self.datasets.get_metadata(tree[0].value)
            output['columns'] = list(map(lambda x: x['column_name'], output_metadata))
        else:
            output['columns'] = tree[1]['columns']
            output_metadata = self.datasets.get_metadata(tree[0].value, output['columns'])
        return {'filter': output, 'input': tree[0].value, '_output_metadata': output_metadata, 'rule_type': 'filter'}

    def filter_row(self, tree):
        output = {}
        if 'condition' not in tree[-1]:
            output['rules'] = [tree[-1]]
        else:
            output.update(tree[-1])
        output_metadata = self.datasets.get_metadata(tree[0].value)
        return {'filter': output, 'input': tree[0].value, '_output_metadata': output_metadata, 'rule_type': 'filter'}

    def filter_column(self, tree):
        output = {}
        if '*' in tree[1]['columns']:
            output_metadata = self.datasets.get_metadata(tree[0].value)
            output['columns'] = list(map(lambda x: x['column_name'], output_metadata))
        else:
            output['columns'] = tree[1]['columns']
            output_metadata = self.datasets.get_metadata(tree[0].value, output['columns'])
        return {'filter': output, 'input': tree[0].value, '_output_metadata': output_metadata, 'rule_type': 'filter'}

    def join(self, tree):
        output = {'input': [], 'join': {'rules': [], 'columns': []}, '_output_metadata': [], 'rule_type': 'join'}
        output['input'].append(tree[0].value)

        for rule in tree[1]:
            output['join']['rules'].append(rule)
            output['input'].append(rule['join_input'])

        for merged_name, merged_metadata in self.datasets.get_merged_names(tree[2]):
            output['join']['columns'].append(merged_name)
            output['_output_metadata'].append(merged_metadata)
        return output

    def join_distinct(self, tree):
        output = {'input': [], 'join': {'rules': [], 'columns': [], 'distinct': 'distinct'},
                  '_output_metadata': [], 'rule_type': 'join'}
        output['input'].append(tree[0].value)

        for rule in tree[1]:
            output['join']['rules'].append(rule)
            output['input'].append(rule['join_input'])

        for merged_name, merged_metadata in self.datasets.get_merged_names(tree[2]):
            output['join']['columns'].append(merged_name)
            output['_output_metadata'].append(merged_metadata)
        return output

    def derived(self, tree):
        original_columns = self.datasets.get_metadata(tree[0].value)
        derived_columns = list(map(lambda x: {"column_name": x["column_alias"], "data_type": x["data_type"],
                                              "derived": True}, tree[1]))
        output_metadata = original_columns + derived_columns
        return {'input': tree[0].value, 'derived': tree[1], '_output_metadata': output_metadata, 'rule_type': 'derived'}

    def analytical(self, tree):
        original_columns = self.datasets.get_metadata(tree[0].value)
        derived_columns = [{"column_name": tree[4].value, "data_type": tree[3].value, "derived": True}]
        output_metadata = original_columns + derived_columns
        analytical = dict(function_name=tree[1].value, column=tree[2].value,
                          group_by=tree[5]['columns'], order_column=tree[7].value, order_type=tree[6].value,
                          dtype=tree[3].value, alias=tree[4].value)
        return {'input': tree[0].value, 'analytical': analytical, '_output_metadata': output_metadata,
                'rule_type': 'analytical'}

    def aggregate(self, tree):
        output = {'group_by': tree[1]['columns'], 'agg_expr': tree[2]}
        categorical_columns = self.datasets.get_metadata(tree[0].value, tree[1]['columns'])
        aggregated_columns = list(map(lambda x: {"column_name": x['column_alias'], "data_type": x["data_type"],
                                                 "derived": True}, tree[2]))
        output_metadata = categorical_columns + aggregated_columns
        return {'aggregate': output, 'input': tree[0].value, '_output_metadata': output_metadata,
                'rule_type': 'aggregate'}

    def aggregate_having(self, tree):
        output = {'group_by': tree[1]['columns'], 'agg_expr': tree[2]}
        if 'condition' not in tree[-1]:
            output.update({'having': {'rules': [tree[3]]}})
        else:
            output.update({'having': tree[3]})
        categorical_columns = self.datasets.get_metadata(tree[0].value, tree[1]['columns'])
        aggregated_columns = list(map(lambda x: {"column_name": x['column_alias'], "data_type": x["data_type"],
                                                 "derived": True}, tree[2]))
        output_metadata = categorical_columns + aggregated_columns
        return {'aggregate': output, 'input': tree[0].value, '_output_metadata': output_metadata,
                'rule_type': 'aggregate'}

    def partition(self, tree):
        original_columns = self.datasets.get_metadata(tree[0].value)
        derived_columns = list(map(lambda x: {"column_name": x["column_alias"], "data_type": 'numeric',
                                              "derived": True}, tree[1]))
        output_metadata = original_columns + derived_columns
        return {'partition': tree[1], 'input': tree[0].value, '_output_metadata': output_metadata,
                'rule_type': 'partition'}

    def dedupe(self, tree):
        output_metadata = self.datasets.get_metadata(tree[0].value)
        return {'input': tree[0].value, 'dedupe': tree[1]['columns'], '_output_metadata': output_metadata,
                'rule_type': 'dedupe'}

    def union(self, tree):
        output = {'union': {'columns': []}, 'input': [tree[0].value, tree[1].value], '_output_metadata': [],
                  'rule_type': 'union'}
        for merged_name, merged_metadata in self.datasets.get_merged_names([tree[0].value + ".*"]):
            output['union']['columns'].append(merged_name)
            output['_output_metadata'].append(merged_metadata)
        return output

    def union_filter(self, tree):
        output = {'union': {tree[0].value: tree[1]['columns'], tree[2].value: tree[3]['columns'], 'columns': []},
                  'input': [tree[0].value, tree[2].value], '_output_metadata': [], 'rule_type': 'union'}
        table_columns = map(lambda x: tree[0].value + '.' + x, tree[1]['columns'])
        for merged_name, merged_metadata in self.datasets.get_merged_names(list(table_columns)):
            output['union']['columns'].append(merged_name)
            output['_output_metadata'].append(merged_metadata)
        return output

    def union_all(self, tree):
        output = {'union': {'columns': []}, 'input': [tree[0].value, tree[1].value],
                  '_output_metadata': [], 'rule_type': 'union'}
        for merged_name, merged_metadata in self.datasets.get_merged_names([tree[0].value + ".*"]):
            output['union']['columns'].append(merged_name)
            output['_output_metadata'].append(merged_metadata)
        return output

    def union_all_filter(self, tree):
        output = {'union': {tree[0].value: tree[1]['columns'], tree[2].value: tree[3]['columns'], 'columns': []},
                  'input': [tree[0].value, tree[2].value], '_output_metadata': [], 'rule_type': 'union'}
        table_columns = map(lambda x: tree[0].value + '.' + x, tree[1]['columns'])
        for merged_name, merged_metadata in self.datasets.get_merged_names(list(table_columns)):
            output['union']['columns'].append(merged_name)
            output['_output_metadata'].append(merged_metadata)
        return output

    def custom(self, tree):
        input_tables, output_metadata = tree[2]
        new_columns = list(filter(lambda x: x.get('derived'), output_metadata))
        return {'custom': {'name': tree[0].value, 'parameters': tree[1], 'columns': new_columns},
                '_output_metadata': output_metadata, 'input': input_tables, 'rule_type': 'custom'}

    def columns(self, tree):
        return {"columns": [column.value for column in tree]}

    def or_rule(self, tree):
        return {'condition': 'OR', 'rules': tree}

    def and_rule(self, tree):
        return {'condition': 'AND', 'rules': tree}

    def condition(self, tree):
        output = {}
        for child in tree:
            output[child.type] = child.value.strip()
        return output

    def join_rules(self, tree):
        return tree

    def join_rule(self, tree):
        return {"join_input": tree[0].value, "join_type": tree[1].value, "join_expr": tree[2]}

    def join_expressions(self, tree):
        return tree

    def partition_expressions(self, tree):
        return tree

    def join_expr(self, tree):
        return tree[0].value, tree[1].value, tree[2].value

    def table_columns(self, tree):
        return [child.value for child in tree]

    def formula(self, tree):
        return {'formula': tree[0].value, "column_alias": tree[1].value, "data_type": tree[2].value}

    def partition_formula(self, tree):
        return {'formula': tree[0].value, "column_alias": tree[1].value, "partition_by": tree[2]}

    def case(self, tree):
        return {'case': tree[0], 'column_alias': tree[1].value, "data_type": tree[2].value}

    def case_expression(self, tree):
        return tree

    def when_rule(self, tree):
        return {'when': tree[0], 'then': tree[1].value}

    def else_rule(self, tree):
        return {'else': tree[0].value}

    def column_expressions(self, tree):
        return tree

    def parameters(self, tree):
        return [child for child in tree]

    def parameter(self, tree):
        return tree[0].value

    def custom_columns(self, tree):
        input = []
        output = []
        for table, child in tree:
            output += child
            input.append(table)
        return [i for i in input if i], output

    def new_custom_column(self, tree):
        return None, [{'column_name': tree[0].value, 'data_type': tree[1].value, 'derived': True}]

    def existing_custom_column(self, tree):
        table, column = tree[0].value, tree[1].value
        if column == '*':
            return table, self.datasets.get_metadata(table)
        return table, self.datasets.get_metadata(table, [column])


class JSONIntegrity(Transformer):
    def __init__(self, datasets: Datasets, json: dict):
        super().__init__()
        self.json = json
        self.message = ''
        self.input_metadata = {}
        self.__prepare_input_metadata(datasets)

    def filter_rule(self, tree):
        self.__check_unique(self.json['filter']['columns'])
        self.__check_existence(self.json['filter']['columns'],
                               self.input_metadata[self.json['input']])
        return self.message

    def filter_column(self, tree):
        self.__check_unique(self.json['filter']['columns'])
        self.__check_existence(self.json['filter']['columns'],
                               self.input_metadata[self.json['input']])
        return self.message

    def filter_row(self, tree):
        return self.message

    def join(self, tree):
        output_columns = {}
        for table, column in map(lambda x: x[0].split('.'), self.json['join']['columns']):
            columns = output_columns.get(table, [])
            columns.append(column)
            output_columns[table] = columns
        for table, columns in output_columns.items():
            self.__check_unique(columns)
        for table, column in filter(lambda y: y[1] != '*', (map(lambda x: x.value.split('.'), tree[2].children))):
            self.__check_existence([column], self.input_metadata[table])
        return self.message

    def join_distinct(self, tree):
        output_columns = {}
        for table, column in map(lambda x: x[0].split('.'), self.json['join']['columns']):
            columns = output_columns.get(table, [])
            columns.append(column)
            output_columns[table] = columns
        for table, columns in output_columns.items():
            self.__check_unique(columns)
        for table, column in filter(lambda y: y[1] != '*', (map(lambda x: x.value.split('.'), tree[2].children))):
            self.__check_existence([column], self.input_metadata[table])
        return self.message

    def derived(self, tree):
        derived_columns = list(map(lambda x: x['column_alias'], self.json['derived']))
        self.__check_unique(derived_columns)
        self.__check_non_existence(derived_columns, self.input_metadata[self.json['input']])
        return self.message

    def analytical(self, tree):
        self.__check_unique(self.json['analytical']['group_by'])
        self.__check_existence(self.json['analytical']['group_by'] +
                               [self.json['analytical']['column']] +
                               [self.json['analytical']['order_column']],
                               self.input_metadata[self.json['input']])
        self.__check_non_existence([self.json['analytical']['alias']], self.input_metadata[self.json['input']])
        return self.message

    def aggregate(self, tree):
        self.message = ''
        self.__check_unique(self.json['aggregate']['group_by'])
        self.__check_existence(self.json['aggregate']['group_by'], self.input_metadata[self.json['input']])
        column_aliases = list(map(lambda x: x['column_alias'], self.json['aggregate']['agg_expr']))
        self.__check_unique(column_aliases)
        self.__check_non_existence(column_aliases, self.input_metadata[self.json['input']])
        return self.message

    def aggregate_having(self, tree):
        self.message = ''
        return self.message

    def partition(self, tree):
        for each_partition in self.json['partition']:
            self.__check_unique(each_partition['partition_by']['columns'])
            self.__check_existence(each_partition['partition_by']['columns'], self.input_metadata[self.json['input']])
            self.__check_non_existence([each_partition['column_alias']], self.input_metadata[self.json['input']])
        column_aliases = list(map(lambda x: x['column_alias'], self.json['partition']))
        self.__check_unique(column_aliases)
        return self.message

    def dedupe(self, tree):
        self.__check_unique(self.json['dedupe'])
        self.__check_existence(self.json['dedupe'], self.input_metadata[self.json['input']])
        return self.message

    def union(self, tree):
        column_count = []
        table_1, table_2 = self.json['input']
        columns_1 = list(map(lambda x: x['column_name'], self.input_metadata[table_1]))
        columns_2 = list(map(lambda x: x['column_name'], self.input_metadata[table_2]))

        self.__check_unique(columns_1)
        self.__check_existence(columns_1, self.input_metadata[table_1])
        column_count.append(len(columns_1))

        self.__check_unique(columns_2)
        self.__check_existence(columns_2, self.input_metadata[table_2])
        column_count.append(len(columns_2))

        self.__check_length(column_count)
        for column_1, column_2 in zip(columns_1, columns_2):
            self.__check_datatype(table_1, column_1, table_2, column_2)

        return self.message

    def union_filter(self, tree):
        column_count = []
        table_1, table_2 = self.json['input']

        self.__check_unique(self.json['union'][table_1])
        self.__check_existence(self.json['union'][table_1], self.input_metadata[table_1])
        column_count.append(len(self.json['union'][table_1]))

        self.__check_unique(self.json['union'][table_2])
        self.__check_existence(self.json['union'][table_2], self.input_metadata[table_2])
        column_count.append(len(self.json['union'][table_2]))

        self.__check_length(column_count)
        for column_1, column_2 in zip(self.json['union'][table_1], self.json['union'][table_2]):
            self.__check_datatype(table_1, column_1, table_2, column_2)

        return self.message

    def union_all(self, tree):
        column_count = []
        table_1, table_2 = self.json['input']
        columns_1 = list(map(lambda x: x['column_name'], self.input_metadata[table_1]))
        columns_2 = list(map(lambda x: x['column_name'], self.input_metadata[table_2]))

        self.__check_unique(columns_1)
        self.__check_existence(columns_1, self.input_metadata[table_1])
        column_count.append(len(columns_1))

        self.__check_unique(columns_2)
        self.__check_existence(columns_2, self.input_metadata[table_2])
        column_count.append(len(columns_2))

        self.__check_length(column_count)
        for column_1, column_2 in zip(columns_1, columns_2):
            self.__check_datatype(table_1, column_1, table_2, column_2)

        return self.message

    def union_all_filter(self, tree):
        column_count = []
        table_1, table_2 = self.json['input']

        self.__check_unique(self.json['union'][table_1])
        self.__check_existence(self.json['union'][table_1], self.input_metadata[table_1])
        column_count.append(len(self.json['union'][table_1]))

        self.__check_unique(self.json['union'][table_2])
        self.__check_existence(self.json['union'][table_2], self.input_metadata[table_2])
        column_count.append(len(self.json['union'][table_2]))

        self.__check_length(column_count)
        for column_1, column_2 in zip(self.json['union'][table_1], self.json['union'][table_2]):
            self.__check_datatype(table_1, column_1, table_2, column_2)

        return self.message

    def custom(self, tree):
        return self.message

    def condition(self, tree):
        for child in tree:
            if child.type == 'COLUMN':
                self.__check_existence([child.value], self.input_metadata[self.json['input']])

    def join_expr(self, tree):
        table_1, column_1 = tree[0].value.split('.')
        self.__check_existence([column_1], self.input_metadata[table_1])
        table_2, column_2 = tree[2].value.split('.')
        self.__check_existence([column_2], self.input_metadata[table_2])
        self.__check_datatype(table_1, column_1, table_2, column_2)

    def __check_unique(self, objects: list):
        seen = set()
        dupes = []
        for obj in objects:
            if obj in seen:
                dupes.append(obj)
            seen.add(obj)
        if len(dupes) > 0:
            self.message += 'Expected names to be unique.\nDuplicate found - ' + ', '.join(dupes) + '\n'

    def __check_existence(self, objects: list, metadata: list):
        not_found = ', '.join(filter(lambda x: x.lower() not in map(lambda y: y['column_name'].lower(),
                                                                    metadata), objects))
        if len(not_found) > 0:
            self.message += 'Unexpected column names found - ' + not_found + '\n'

    def __check_non_existence(self, objects: list, metadata: list):
        found = ', '.join(filter(lambda x: x.lower() in map(lambda y: y['column_name'].lower(),
                                                            metadata), objects))
        if len(found) > 0:
            self.message += 'Expected names to be unique.\nDuplicate found - ' + found + '\n'

    def __check_datatype(self, table_1, column_1, table_2, column_2):
        dtype_1 = self.__identify_dtype(column_1, table_1)
        dtype_2 = self.__identify_dtype(column_2, table_2)
        if dtype_1 != dtype_2:
            self.message += 'Columns ' + column_1 + ', ' + column_2 + ' used do not have same data types.\n'

    def __check_length(self, column_count: list):
        if len(set(column_count)) != 1:
            self.message += 'Expected same count of columns in both the datasets\n'

    def __identify_dtype(self, column_1, table_1):
        for metadata in self.input_metadata[table_1]:
            if metadata['column_name'].lower() == column_1.lower():
                return metadata['data_type']
        return ''

    def __prepare_input_metadata(self, datasets):
        if isinstance(self.json['input'], list):
            for input_data in self.json['input']:
                self.input_metadata[input_data] = datasets.get_metadata(input_data)
        else:
            self.input_metadata[self.json['input']] = datasets.get_metadata(self.json['input'])


class TreeToPostgreSQL(Transformer):
    def __init__(self, json: dict):
        super().__init__()
        self.json = json

    def __add_braces(self, table_column: str):
        table, column = table_column.split('.')
        return '{schema_' + table + '}.{' + table + '}.' + column

    def filter_rule(self, tree):
        output = 'select '
        if 'columns' in tree[1]:
            output += ', '.join(tree[1]['columns'])
        else:
            output += '*'
        output += ' from {schema_' + tree[0].value + '}.{' + tree[0].value + '}'
        output += ' where ' + tree[-1]
        return output

    def filter_column(self, tree):
        output = 'select '
        output += ', '.join(tree[1]['columns'])
        output += ' from {schema_' + tree[0].value + '}.{' + tree[0].value + '}'
        return output

    def filter_row(self, tree):
        output = 'select '
        output += '*'
        output += ' from {schema_' + tree[0].value + '}.{' + tree[0].value + '}'
        output += ' where ' + tree[-1]
        return output

    def join(self, tree):
        query = """select {columns} from {left_table} {join_rules}"""
        return query.format(
            columns=', '.join(map(lambda x: self.__add_braces(x[0]) + ' as ' + x[1], self.json['join']['columns'])),
            left_table='{schema_' + tree[0].value + '}.{' + tree[0].value + '}', join_rules=tree[1])

    def join_distinct(self, tree):
        query = """select distinct {columns} from {left_table} {join_rules}"""
        return query.format(
            columns=', '.join(map(lambda x: self.__add_braces(x[0]) + ' as ' + x[1], self.json['join']['columns'])),
            left_table='{schema_' + tree[0].value + '}.{' + tree[0].value + '}', join_rules=tree[1])

    def derived(self, tree):
        query = """select *, {derived_columns} from {input_table}"""
        return query.format(derived_columns=tree[1],
                            input_table='{schema_' + tree[0].value + '}.{' + tree[0].value + '}')

    def analytical(self, tree):
        query = "select *, cast({function_name}({column}) over(partition by {group_by} order by {order_column} " \
                "{order_type} rows unbounded preceding) as {dtype}) as {alias} " \
                "from {input_table}"
        return query.format(input_table='{schema_' + tree[0].value + '}.{' + tree[0] + '}',
                            function_name=tree[1], column=tree[2],
                            group_by=','.join(tree[5]['columns']),
                            order_column=tree[7], order_type=tree[6], dtype=tree[3], alias=tree[4])

    def aggregate(self, tree):
        output = """select {columns}, {aggregate_fields} from {input_table} group by {columns}"""
        return output.format(columns=', '.join(tree[1]['columns']),
                             input_table='{schema_' + tree[0].value + '}.{' + tree[0].value + '}',
                             aggregate_fields=tree[2])

    def aggregate_having(self, tree):
        output = """select {columns}, {aggregate_fields} from {input_table} group by {columns} having {condition}"""
        return output.format(
            columns=', '.join(tree[1]['columns']),
            input_table='{schema_' + tree[0].value + '}.{' + tree[0].value + '}',
            aggregate_fields=tree[2],
            condition=tree[-1])

    def partition(self, tree):
        output = """select *, {partition_expressions} from {input_table}"""
        return output.format(input_table='{schema_' + tree[0].value + '}.{' + tree[0].value + '}',
                             partition_expressions=tree[1])

    def dedupe(self, tree):
        join_key = []
        for column in tree[1]['columns']:
            join_key.append("A." + column + " = B." + column)
        output = "select A.* from {table} A inner join " \
                 "(select {columns}, count(*) from {table} group by {columns} having count(*) = 1) B " \
                 "on {join_key}"
        return output.format(table='{schema_' + tree[0].value + '}.{' + tree[0].value + '}',
                             columns=', '.join(tree[1]['columns']),
                             join_key=' and '.join(join_key))

    def union(self, tree):
        all_columns = ', '.join(map(lambda x: self.__add_braces(x[0]) + ' as ' + x[1], self.json['union']['columns']))
        return 'select {} from {} union select * from {}'.format(all_columns,
                                                                 '{schema_' + tree[0].value + '}.{' +
                                                                 tree[0].value + '}',
                                                                 '{schema_' + tree[1].value + '}.{' +
                                                                 tree[1].value + '}')

    def union_filter(self, tree):
        all_columns = ', '.join(map(lambda x: self.__add_braces(x[0]) + ' as ' + x[1], self.json['union']['columns']))
        return """select {} from {} union select {} from {}""".format(all_columns,
                                                                      '{schema_' + tree[0].value + '}.{' +
                                                                      tree[0].value + '}',
                                                                      ', '.join(tree[3]['columns']),
                                                                      '{schema_' + tree[2].value + '}.{' +
                                                                      tree[2].value + '}')

    def union_all(self, tree):
        all_columns = ', '.join(map(lambda x: self.__add_braces(x[0]) + ' as ' + x[1], self.json['union']['columns']))
        return 'select {} from {} union all select * from {}'.format(all_columns,
                                                                     '{schema_' + tree[0].value + '}.{' +
                                                                     tree[0].value + '}',
                                                                     '{schema_' + tree[1].value + '}.{' +
                                                                     tree[1].value + '}')

    def union_all_filter(self, tree):
        all_columns = ', '.join(map(lambda x: self.__add_braces(x[0]) + ' as ' + x[1], self.json['union']['columns']))
        return """select {} from {} union all select {} from {}""".format(all_columns,
                                                                          '{schema_' + tree[0].value + '}.{' +
                                                                          tree[0].value + '}',
                                                                          ', '.join(tree[3]['columns']),
                                                                          '{schema_' + tree[2].value + '}.{' +
                                                                          tree[2].value + '}')

    def custom(self, tree):
        input_tables = self.json['input']
        existing_columns = list(map(lambda y: y['column_name'],
                                    filter(lambda x: x.get('derived', False) is False, self.json['_output_metadata'])))
        new_columns = list(map(lambda y: y['column_name'],
                               filter(lambda x: x.get('derived'), self.json['_output_metadata'])))
        new_column = new_columns[0]
        query = """select {existing_columns}, {udf}({parameters}) as {new_column} from {input}"""
        return query.format(udf=tree[0].value, existing_columns=', '.join(existing_columns),
                            new_column=new_column, parameters=tree[1],
                            input='{schema_' + input_tables[0] + '}.{' + input_tables[0] + '}')

    def columns(self, tree):
        return {"columns": [column.value for column in tree]}

    def or_rule(self, tree):
        return '(' + ') or ('.join(tree) + ')'

    def and_rule(self, tree):
        return '(' + ') and ('.join(tree) + ')'

    def condition(self, tree):
        return tree[0].value.strip() + ' ' + tree[1].value.strip() + ' ' + tree[2].value.strip()

    def join_rules(self, tree):
        return ' '.join(tree)

    def join_rule(self, tree):
        return """{join_type} join {right_table} on {key}""".format(join_type=tree[1].value,
                                                                    right_table='{schema_' + tree[0].value +
                                                                                '}.{' + tree[0].value + '}',
                                                                    key=tree[2])

    def join_expressions(self, tree):
        return ' and '.join(tree)

    def partition_expressions(self, tree):
        return ', '.join(tree)

    def join_expr(self, tree):
        return self.__add_braces(tree[0].value) + ' ' + tree[1].value + ' ' + self.__add_braces(tree[2].value)

    def table_columns(self, tree):
        return ', '.join(map(lambda x: x.value, tree))

    def formula(self, tree):
        return 'cast(' + tree[0].value + ' as ' + tree[2].value + ') as ' + tree[1].value

    def partition_formula(self, tree):
        return tree[0].value + ' over (partition by ' + ', '.join(tree[2]['columns']) + ') as ' + tree[1].value

    def case(self, tree):
        return 'cast(' + tree[0] + ' as ' + tree[2].value + ') as ' + tree[1].value

    def case_expression(self, tree):
        return 'case ' + ' '.join(tree) + ' end'

    def when_rule(self, tree):
        return ' when ' + tree[0] + ' then ' + tree[1].value

    def else_rule(self, tree):
        return ' else ' + tree[0].value

    def column_expressions(self, tree):
        return ', '.join(tree)

    def parameters(self, tree):
        return ', '.join(map(lambda x: x.strip(), tree))

    def parameter(self, tree):
        return tree[0].value


def to_json(tree, datasets: Datasets):
    json = TreeToJson(datasets).transform(tree)
    integrity_messages = JSONIntegrity(datasets, json).transform(tree)
    if integrity_messages:
        raise BRMSRuleIntegrityFailed(message=integrity_messages)
    query = TreeToPostgreSQL(json).transform(tree).replace('%', '%%')
    parameters = re.findall(r'(\$[\w]+)', query)
    json.update({'query': query, 'parameters': parameters})
    return json
