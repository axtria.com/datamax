#   datamax/api/utils/tests/test_brms_transformer_custom.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>

from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'C4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'},
            {'column_name': 'ds2#C3', 'data_type': 'numeric'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}]
}
datasets = Datasets.initialize(dict_datasets)


def test_custom_component():
    tree = tokenize("custom fn_custom_function with p1 to get c3 of type float, ds1.*", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['p1'],
                           'columns': [{'column_name': 'c3', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c3', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'input': ['ds1'],
                'query': 'select c1, c2, fn_custom_function(p1) as c3 from {schema_ds1}.{ds1}',
                'rule_type': 'custom',
                'parameters': []}
    assert actual == expected


def test_custom_component_multi_columns():
    tree = tokenize("custom fn_custom_function with p1 to get c19 of type float, c6 of type float, ds1.*, "
                    "ds2.c3", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['p1'],
                           'columns': [{'column_name': 'c19', 'data_type': 'float', 'derived': True},
                                       {'column_name': 'c6', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c19', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'c6', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'}],
                'input': ['ds1', 'ds2'],
                'rule_type': 'custom',
                'query': 'select c1, c2, c3, fn_custom_function(p1) as c19 from {schema_ds1}.{ds1}',
                'parameters': []}
    assert actual == expected


def test_custom_component_with_parameter():
    tree = tokenize("custom fn_custom_function with $quarter to get fn_custom_column of type float,"
                    "ds1.*", datasets)
    actual = to_json(tree, datasets)
    print(actual)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['$quarter'],
                           'columns': [{'column_name': 'fn_custom_column', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'fn_custom_column', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'input': ['ds1'],
                'rule_type': 'custom',
                'query': 'select c1, c2, fn_custom_function($quarter) as fn_custom_column from {schema_ds1}.{ds1}',
                'parameters': ['$quarter']}
    assert actual == expected


def test_custom_component_with_comma_inside_complex_parameter():
    tree = tokenize("custom fn_custom_function with {p1,p2} to get c3 of type float, ds1.*", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['p1,p2'],
                           'columns': [{'column_name': 'c3', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c3', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'input': ['ds1'],
                'query': 'select c1, c2, fn_custom_function(p1,p2) as c3 from {schema_ds1}.{ds1}',
                'rule_type': 'custom',
                'parameters': []}
    assert actual == expected


def test_custom_component_with_hashed_parameter():
    tree = tokenize("custom fn_custom_function with ds1#c1, $quarter to get ds1.c1, new_col of type float", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['ds1#c1', '$quarter'],
                           'columns': [{'column_name': 'new_col', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'new_col', 'data_type': 'float', 'derived': True}],
                'input': ['ds1'],
                'rule_type': 'custom',
                'query': 'select c1, fn_custom_function(ds1#c1, $quarter) as new_col from {schema_ds1}.{ds1}',
                'parameters': ['$quarter']}
    assert actual == expected


def test_custom_component_with_spaced_parameter_value():
    tree = tokenize("custom fn_custom_function with ds1, {'My Brand'} to get ds1.*, new_col of type varchar",
                    datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['ds1', "'My Brand'"],
                           'columns': [{'column_name': 'new_col', 'data_type': 'varchar', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'new_col', 'data_type': 'varchar', 'derived': True}],
                'input': ['ds1'],
                'rule_type': 'custom',
                'query': "select c1, c2, fn_custom_function(ds1, 'My Brand') as new_col from {schema_ds1}.{ds1}",
                'parameters': []}
    assert actual == expected


def test_custom_component_with_spaced_parameter_value_should_separate_out_values():
    tree = tokenize("custom fn_custom_function with ds1, 'My Brand' to get ds1.*, c3 of type float", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['ds1', "'My", "Brand'"],
                           'columns': [{'column_name': 'c3', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'float', 'derived': True}],
                'input': ['ds1'],
                'rule_type': 'custom',
                'query': "select c1, c2, fn_custom_function(ds1, 'My, Brand') as c3 from {schema_ds1}.{ds1}",
                'parameters': []}
    assert actual == expected


def test_custom_component_with_dates_with_dashed_as_parameter():
    tree = tokenize("custom fn_custom_function with ds1, 21-10-2019 to get ds1.*, c5 of type float", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['ds1', '21-10-2019'],
                           'columns': [{'column_name': 'c5', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c5', 'data_type': 'float', 'derived': True}],
                'input': ['ds1'],
                'rule_type': 'custom',
                'query': 'select c1, c2, fn_custom_function(ds1, 21-10-2019) as c5 from {schema_ds1}.{ds1}',
                'parameters': []}
    assert actual == expected


def test_custom_component_with_dates_with_slashed_as_parameter():
    tree = tokenize("custom fn_custom_function with ds1, 21/10/2019 to get ds1.*, c3 of type float", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['ds1', '21/10/2019'],
                           'columns': [{'column_name': 'c3', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'float', 'derived': True}],
                'input': ['ds1'],
                'rule_type': 'custom',
                'query': 'select c1, c2, fn_custom_function(ds1, 21/10/2019) as c3 from {schema_ds1}.{ds1}',
                'parameters': []}
    assert actual == expected


def test_custom_component_with_condtions_as_parameter():
    tree = tokenize("custom fn_custom_function with ds1, a=b to get ds1.*, c4 of type float", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['ds1', 'a=b'],
                           'columns': [{'column_name': 'c4', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c4', 'data_type': 'float', 'derived': True}],
                'input': ['ds1'],
                'rule_type': 'custom',
                'query': 'select c1, c2, fn_custom_function(ds1, a=b) as c4 from {schema_ds1}.{ds1}',
                'parameters': []}
    assert actual == expected


def test_custom_component_with_complex_formula_condtions_as_parameter():
    tree = tokenize("custom fn_custom_function with ds1, {a = b} to get ds1.*, c3 of type float", datasets)
    actual = to_json(tree, datasets)
    expected = {'custom': {'name': 'fn_custom_function', 'parameters': ['ds1', 'a = b'],
                           'columns': [{'column_name': 'c3', 'data_type': 'float', 'derived': True}]},
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'float', 'derived': True}],
                'input': ['ds1'],
                'rule_type': 'custom',
                'query': 'select c1, c2, fn_custom_function(ds1, a = b) as c3 from {schema_ds1}.{ds1}',
                'parameters': []}
    assert actual == expected
