#   datamax/api/utils/tests/test_brms_transformer_partition.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json
from utils.exceptions import BRMSRuleIntegrityFailed

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'C4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'},
            {'column_name': 'ds2#C3', 'data_type': 'numeric'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}]
}
datasets = Datasets.initialize(dict_datasets)


def test_partition_single_condition():
    tree = tokenize('partition ds1 to derive sum(nrx) as sum_nrx over (c1, c2)', datasets)
    actual = to_json(tree, datasets)
    expected = {
        'partition': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx', 'partition_by': {'columns': ['c1', 'c2']}}],
        'input': 'ds1', '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                             {'column_name': 'c2', 'data_type': 'varchar'},
                                             {'column_name': 'sum_nrx', 'data_type': 'numeric', 'derived': True}],
        'query': 'select *, sum(nrx) over (partition by c1, c2) as sum_nrx from {schema_ds1}.{ds1}',
        'rule_type': 'partition',
        'parameters': []}
    assert actual == expected


def test_aggregate_multi_columns():
    tree = tokenize('partition ds1 to derive sum(nrx) as sum_nrx over (c1, c2) '
                    'count(hcp_id) as count_hcp over (c1)', datasets)
    actual = to_json(tree, datasets)
    expected = {
        'partition': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx', 'partition_by': {'columns': ['c1', 'c2']}},
                      {'formula': 'count(hcp_id)', 'column_alias': 'count_hcp', 'partition_by': {'columns': ['c1']}}],
        'input': 'ds1', '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                             {'column_name': 'c2', 'data_type': 'varchar'},
                                             {'column_name': 'sum_nrx', 'data_type': 'numeric', 'derived': True},
                                             {'column_name': 'count_hcp', 'data_type': 'numeric', 'derived': True}],
        'query': 'select *, sum(nrx) over (partition by c1, c2) as sum_nrx, count(hcp_id) over (partition by c1) '
                 'as count_hcp from {schema_ds1}.{ds1}',
        'rule_type': 'partition',
        'parameters': []}
    assert actual == expected


def test_partition_single_condition_with_unalike_case():
    tree = tokenize('partition ds1 to derive sum(nrx) as sum_nrx over (C1, C2)', datasets)
    actual = to_json(tree, datasets)
    expected = {
        'partition': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx', 'partition_by': {'columns': ['c1', 'c2']}}],
        'input': 'ds1', '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                             {'column_name': 'c2', 'data_type': 'varchar'},
                                             {'column_name': 'sum_nrx', 'data_type': 'numeric', 'derived': True}],
        'query': 'select *, sum(nrx) over (partition by c1, c2) as sum_nrx from {schema_ds1}.{ds1}',
        'rule_type': 'partition',
        'parameters': []}
    assert actual == expected


def test_aggregate_multi_columns_with_unalike_case():
    tree = tokenize('Partition ds2 to derive sum(nrx) as sum_nrx over (c1, C2, c5) '
                    'count(hcp_id) as count_hcp over (c1, C2)', datasets)
    actual = to_json(tree, datasets)
    expected = {'partition': [
        {'formula': 'sum(nrx)', 'column_alias': 'sum_nrx', 'partition_by': {'columns': ['c1', 'c2', 'C5']}},
        {'formula': 'count(hcp_id)', 'column_alias': 'count_hcp', 'partition_by': {'columns': ['c1', 'c2']}}],
        'input': 'ds2',
        '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                             {'column_name': 'c2', 'data_type': 'varchar'},
                             {'column_name': 'c3', 'data_type': 'numeric'},
                             {'column_name': 'C4', 'data_type': 'varchar'},
                             {'column_name': 'C5', 'data_type': 'varchar'},
                             {'column_name': 'sum_nrx', 'data_type': 'numeric', 'derived': True},
                             {'column_name': 'count_hcp', 'data_type': 'numeric', 'derived': True}],
        'query': 'select *, sum(nrx) over (partition by c1, c2, C5) as sum_nrx, '
                 'count(hcp_id) over (partition by c1, c2) as count_hcp from {schema_ds2}.{ds2}',
        'rule_type': 'partition',
        'parameters': []}
    assert actual == expected


def test_partition_single_condition_with_hashed_columns():
    tree = tokenize('partition ds3 to derive sum(nrx) as sum_nrx over (ds1#c1, ds2#c5)', datasets)
    actual = to_json(tree, datasets)
    expected = {'partition': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx',
                               'partition_by': {'columns': ['ds1#c1', 'ds2#C5']}}],
                'input': 'ds3',
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C3', 'data_type': 'numeric'},
                                     {'column_name': 'sum_nrx', 'data_type': 'numeric', 'derived': True}],
                'query': 'select *, sum(nrx) over (partition by ds1#c1, ds2#C5) as sum_nrx from {schema_ds3}.{ds3}',
                'rule_type': 'partition',
                'parameters': []}
    assert actual == expected


def test_partition_single_condition_with_hashed_dataset():
    tree = tokenize('partition d-s4 to derive sum(nrx) as sum_nrx over (d-s5#c1, ds#c2)', datasets)
    actual = to_json(tree, datasets)
    expected = {'partition': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx',
                               'partition_by': {'columns': ['d-s5#c1', 'ds#c2']}}],
                'input': 'd-s4',
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'numeric', 'derived': True}],
                'query': 'select *, sum(nrx) over (partition by d-s5#c1, ds#c2) as sum_nrx from {schema_d-s4}.{d-s4}',
                'rule_type': 'partition',
                'parameters': []}
    assert actual == expected

# --------------------------------
# Integrity negative - test cases
# --------------------------------


def test_partition_with_duplicate_columns():
    tree = tokenize('partition ds1 to derive sum(nrx) as sum_nrx over (c1, c1)', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_partition_with_multiple_duplicate_columns():
    tree = tokenize('partition ds1 to derive sum(nrx) as sum_nrx over (c1, c1, c2, c2)', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1, c2\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_partition_with_non_existent_column():
    tree = tokenize('partition ds1 to derive sum(nrx) as sum_nrx over (c1, c9)', datasets)
    expected = 'Unexpected column names found - c9\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_partition_with_multiple_non_existent_column():
    tree = tokenize('partition ds1 to derive sum(nrx) as sum_nrx over (c8, c9)', datasets)
    expected = 'Unexpected column names found - c8, c9\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_partition_rule_with_duplicate_column_alias_with_existing_columns():
    tree = tokenize('partition ds1 to derive sum(nrx) as c2 over (c1)', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c2\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_aggregate_rule_using_duplicate_column_alias():
    tree = tokenize('partition ds1 to derive sum(nrx) as sum_nrx over (c1, c2) '
                    'count(hcp_id) as sum_nrx over (c1)', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - sum_nrx\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected
