#   datamax/api/utils/tests/test_brms_transformer_union.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json
from utils.exceptions import BRMSRuleIntegrityFailed

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'C2', 'data_type': 'varchar'},
            {'column_name': 'C3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'},
            {'column_name': 'ds2#C3', 'data_type': 'numeric'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}],
    'rs1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}],
    'rs2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar', 'derived': True},
            {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}],
    'rs3': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'numeric', 'derived': True}],
}
datasets = Datasets.initialize(dict_datasets)
datasets.merging_strategy = '_'


def test_union():
    tree = tokenize('union ds1 and rs1', datasets)
    actual = to_json(tree, datasets)
    expected = {'union': {'columns': [('ds1.c1', 'c1'), ('ds1.c2', 'c2')]},
                'input': ['ds1', 'rs1'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select {schema_ds1}.{ds1}.c1 as c1, {schema_ds1}.{ds1}.c2 as c2 from {schema_ds1}.{ds1} '
                         'union select * from {schema_rs1}.{rs1}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


def test_union_specific_columns():
    tree = tokenize('union ds1 using c1, c2 and ds2 using c1, c2', datasets)
    actual = to_json(tree, datasets)
    print(actual)
    expected = {'union': {'ds1': ['c1', 'c2'], 'ds2': ['c1', 'C2'], 'columns': [('ds1.c1', 'c1'), ('ds1.c2', 'c2')]},
                'input': ['ds1', 'ds2'], '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                                              {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select {schema_ds1}.{ds1}.c1 as c1, {schema_ds1}.{ds1}.c2 as c2 from {schema_ds1}.{ds1} '
                         'union select c1, C2 from {schema_ds2}.{ds2}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


def test_union_all():
    tree = tokenize('union all ds1 and rs1', datasets)
    actual = to_json(tree, datasets)
    expected = {'union': {'columns': [('ds1.c1', 'c1'), ('ds1.c2', 'c2')]},
                'input': ['ds1', 'rs1'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select {schema_ds1}.{ds1}.c1 as c1, {schema_ds1}.{ds1}.c2 as c2 from {schema_ds1}.{ds1} '
                         'union all select * from {schema_rs1}.{rs1}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


def test_union_all_with_specific_columns():
    tree = tokenize('union all ds1 using c1, c2 and ds2 using c1, c2', datasets)
    actual = to_json(tree, datasets)
    expected = {'union': {'ds1': ['c1', 'c2'], 'ds2': ['c1', 'C2'], 'columns': [('ds1.c1', 'c1'), ('ds1.c2', 'c2')]},
                'input': ['ds1', 'ds2'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select {schema_ds1}.{ds1}.c1 as c1, {schema_ds1}.{ds1}.c2 as c2 from {schema_ds1}.{ds1} '
                         'union all select c1, C2 from {schema_ds2}.{ds2}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


def test_union_all_with_specific_columns_with_unalike_case():
    tree = tokenize('uniOn All ds1 using C1, C2 and ds2 using c1, C2', datasets)
    actual = to_json(tree, datasets)
    expected = {'union': {'ds1': ['c1', 'c2'], 'ds2': ['c1', 'C2'], 'columns': [('ds1.c1', 'c1'), ('ds1.c2', 'c2')]},
                'input': ['ds1', 'ds2'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select {schema_ds1}.{ds1}.c1 as c1, {schema_ds1}.{ds1}.c2 as c2 from {schema_ds1}.{ds1} '
                         'union all select c1, C2 from {schema_ds2}.{ds2}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


def test_union_all_with_specific_columns_with_unalike_case_in_hashed_columns():
    tree = tokenize('uniOn All ds3 using ds1#C1, dS2#c5 and ds2 using c1, C2', datasets)
    actual = to_json(tree, datasets)
    expected = {'union': {'ds3': ['ds1#c1', 'ds2#C5'], 'ds2': ['c1', 'C2'],
                          'columns': [('ds3.ds1#c1', 'ds1#c1'), ('ds3.ds2#C5', 'ds2#C5')]}, 'input': ['ds3', 'ds2'],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}],
                'query': 'select {schema_ds3}.{ds3}.ds1#c1 as ds1#c1, {schema_ds3}.{ds3}.ds2#C5 as ds2#C5 '
                         'from {schema_ds3}.{ds3} union all select c1, C2 from {schema_ds2}.{ds2}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


def test_union_with_hashed_dataset():
    tree = tokenize('union d-s4 and ds1', datasets)
    actual = to_json(tree, datasets)
    expected = {'union': {'columns': [('d-s4.d-s5#c1', 'd-s5#c1'), ('d-s4.ds#c2', 'ds#c2')]},
                'input': ['d-s4', 'ds1'],
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'}],
                'query': 'select {schema_d-s4}.{d-s4}.d-s5#c1 as d-s5#c1, {schema_d-s4}.{d-s4}.ds#c2 as ds#c2 '
                         'from {schema_d-s4}.{d-s4} union select * from {schema_ds1}.{ds1}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


def test_union_with_derived_column():
    tree = tokenize('union rs1 and ds1', datasets)
    actual = to_json(tree, datasets)
    expected = {'union': {'columns': [('rs1.c1', 'c1'), ('rs1.c2', 'c2')]},
                'input': ['rs1', 'ds1'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}],
                'query': 'select {schema_rs1}.{rs1}.c1 as c1, {schema_rs1}.{rs1}.c2 as c2 '
                         'from {schema_rs1}.{rs1} union select * from {schema_ds1}.{ds1}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


def test_union_with_both_derived_columns():
    tree = tokenize('union rs1 using c1, c2 and rs2 using c4, c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'union': {'rs1': ['c1', 'c2'], 'rs2': ['c4', 'C5'],
                          'columns': [('rs1.c1', 'c1'), ('rs1.c2', 'c2')]},
                'input': ['rs1', 'rs2'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}],
                'query': 'select {schema_rs1}.{rs1}.c1 as c1, {schema_rs1}.{rs1}.c2 as c2 from {schema_rs1}.{rs1} '
                         'union select c4, C5 from {schema_rs2}.{rs2}',
                'rule_type': 'union',
                'parameters': []}
    assert actual == expected


# --------------------------------
# Integrity negative - test cases
# --------------------------------


def test_union_rule_with_duplicate_columns():
    tree = tokenize('union ds1 using c1, c2 and ds2 using c1, c1', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_union_other_rule_with_duplicate_columns():
    tree = tokenize('union ds1 using c1, c1 and ds2 using c1, c2', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    print(e.value.message)
    assert e.value.message == expected


def test_union_both_rule_with_duplicate_columns():
    tree = tokenize('union ds1 using c1, c1 and ds2 using c1, c1', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1\n' \
               'Expected names to be unique.\nDuplicate found - c1\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    print(e.value.message)
    assert e.value.message == expected


def test_union_with_mismatch_count_of_columns():
    tree = tokenize('union ds1 using c1, c2 and ds2 using c1', datasets)
    expected = 'Expected same count of columns in both the datasets\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_union_with_non_existent_column():
    tree = tokenize('union ds1 using c1, c4 and ds2 using c1, c2', datasets)
    expected = 'Unexpected column names found - c4\n' \
               'Columns c4, C2 used do not have same data types.\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_union_other_with_mismatch_count_of_columns():
    tree = tokenize('union ds1 using c1 and ds2 using c1, c2', datasets)
    expected = 'Expected same count of columns in both the datasets\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_union_default_with_mismatch_count_of_columns():
    tree = tokenize('union ds1 and ds2', datasets)
    expected = 'Expected same count of columns in both the datasets\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_union_other_with_non_existent_column():
    tree = tokenize('union ds1 using c1, c2 and ds2 using c1, c9', datasets)
    expected = 'Unexpected column names found - c9\nColumns c2, c9 used do not have same data types.\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_union_mismatch_data_type_of_columns():
    tree = tokenize('union ds1 using c1, c2 and ds2 using c1, c3', datasets)
    expected = 'Columns c2, C3 used do not have same data types.\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_union_default_mismatch_data_type_of_columns():
    tree = tokenize('union ds1 and rs3', datasets)
    expected = 'Columns c2, c2 used do not have same data types.\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected
