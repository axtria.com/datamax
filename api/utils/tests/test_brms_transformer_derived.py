#   datamax/api/utils/tests/test_brms_transformer_derived.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json
from utils.exceptions import BRMSRuleIntegrityFailed

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}]
}
datasets = Datasets.initialize(dict_datasets)


def test_derived_column():
    tree = tokenize('derive columns in ds1 with sales/goals as attainment of type double', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': 'sales/goals',
                             'column_alias': 'attainment',
                             'data_type': 'double'}],
                'query': 'select *, cast(sales/goals as double) as attainment from {schema_ds1}.{ds1}',
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'attainment', 'data_type': 'double', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_with_parameter():
    tree = tokenize('derive columns in ds1 with $factor*nrx as normalized_nrx of type float', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': '$factor*nrx',
                             'column_alias': 'normalized_nrx',
                             'data_type': 'float'}],
                'query': 'select *, cast($factor*nrx as float) as normalized_nrx from {schema_ds1}.{ds1}',
                'parameters': ['$factor'],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'normalized_nrx', 'data_type': 'float', 'derived': True}]
                }
    assert actual == expected


def test_derived_multi_columns():
    tree = tokenize('derive columns in ds1 with $factor*nrx as normalized_nrx of type float, '
                    'sales/goals as attainment of type float', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': '$factor*nrx',
                             'column_alias': 'normalized_nrx',
                             'data_type': 'float'},
                            {'formula': 'sales/goals',
                             'column_alias': 'attainment',
                             'data_type': 'float'}],
                'query': 'select *, cast($factor*nrx as float) as normalized_nrx, cast(sales/goals as float) as '
                         'attainment from {schema_ds1}.{ds1}',
                'parameters': ['$factor'],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'normalized_nrx', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'attainment', 'data_type': 'float', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_paranthesis():
    tree = tokenize('derive columns in ds1 with {(0.5 * nrx) + 0.5} as normalized_nrx of type float, '
                    '{sales/goals} as attainment of type float', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': '(0.5 * nrx) + 0.5',
                             'column_alias': 'normalized_nrx',
                             'data_type': 'float'},
                            {'formula': 'sales/goals',
                             'column_alias': 'attainment',
                             'data_type': 'float'}],
                'query': 'select *, cast((0.5 * nrx) + 0.5 as float) as normalized_nrx, '
                         'cast(sales/goals as float) as attainment from {schema_ds1}.{ds1}',
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'normalized_nrx', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'attainment', 'data_type': 'float', 'derived': True}
                                     ]
                }
    assert actual == expected


def test_derived_column_space_value_with_advanced_formula():
    tree = tokenize("derive columns in ds1 with {'MY BRAND'} as brand of type varchar", datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': "'MY BRAND'",
                             'column_alias': 'brand',
                             'data_type': 'varchar'}],
                'query': "select *, cast('MY BRAND' as varchar) as brand from {schema_ds1}.{ds1}",
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'brand', 'data_type': 'varchar', 'derived': True},
                                     ]
                }
    assert actual == expected


def test_derived_column_when_then():
    tree = tokenize("derive columns in ds1 with case when (c1 > 0) then 's1' else 's2' end as segment "
                    "of type varchar", datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'case':
                                 [{'when':
                                       {'COLUMN': 'c1', 'OPERATOR': '>', 'VALUE': '0'},
                                   'then': "'s1'"},
                                  {'else': "'s2'"}],
                             'column_alias': 'segment',
                             'data_type': 'varchar'}],
                'query': "select *, cast(case  when c1 > 0 then 's1'  else 's2' end as varchar) as segment "
                         "from {schema_ds1}.{ds1}",
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'segment', 'data_type': 'varchar', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_when_then_with_parameters():
    tree = tokenize("derive columns in ds1 with case when (c1 > $threshold) then 's1' else 's2' end as segment"
                    " of type varchar", datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'case':
                                 [{'when':
                                       {'COLUMN': 'c1', 'OPERATOR': '>', 'VALUE': '$threshold'},
                                   'then': "'s1'"},
                                  {'else': "'s2'"}],
                             'column_alias': 'segment',
                             'data_type': 'varchar'}],
                'query': "select *, cast(case  when c1 > $threshold then 's1'  else 's2' end as varchar) as segment "
                         "from {schema_ds1}.{ds1}",
                'parameters': ['$threshold'],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'segment', 'data_type': 'varchar', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_with_formula_incase():
    tree = tokenize("derive columns in ds2 with Case when (c3 > 5) then 700+100 when (c3 < 5) then "
                    "500 else 700+200 end as new_column of type integer", datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds2',
                'derived': [{'case': [{'when': {'COLUMN': 'c3', 'OPERATOR': '>', 'VALUE': '5'}, 'then': '700+100'},
                                      {'when': {'COLUMN': 'c3', 'OPERATOR': '<', 'VALUE': '5'}, 'then': '500'},
                                      {'else': '700+200'}],
                             'column_alias': 'new_column',
                             'data_type': 'integer'}],
                'query': 'select *, cast(case  when c3 > 5 then 700+100  when c3 < 5 then 500  '
                         'else 700+200 end as integer) as new_column from {schema_ds2}.{ds2}',
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'},
                                     {'column_name': 'new_column', 'data_type': 'integer', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_unalike_case():
    tree = tokenize('derive columns in ds1 with c1/C4 as attainment of type double', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': 'c1/C4',
                             'column_alias': 'attainment',
                             'data_type': 'double'}],
                'query': 'select *, cast(c1/C4 as double) as attainment from {schema_ds1}.{ds1}',
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'attainment', 'data_type': 'double', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_with_hash_column_in_input():
    tree = tokenize('derive columns in ds3 with sales/goals as attainment of type double', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds3',
                'derived': [{'formula': 'sales/goals',
                             'column_alias': 'attainment',
                             'data_type': 'double'}],
                'query': 'select *, cast(sales/goals as double) as attainment from {schema_ds3}.{ds3}',
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'},
                                     {'column_name': 'attainment', 'data_type': 'double', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_number_as_numeric():
    tree = tokenize('derive columns in ds1 with sales/goals as attainment of type number', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': 'sales/goals',
                             'column_alias': 'attainment',
                             'data_type': 'numeric'}],
                'query': 'select *, cast(sales/goals as numeric) as attainment from {schema_ds1}.{ds1}',
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'attainment', 'data_type': 'numeric', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_text_as_varchar():
    tree = tokenize("derive columns in ds1 with 'BRAND' as brand of type text", datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': "'BRAND'",
                             'column_alias': 'brand',
                             'data_type': 'varchar'}],
                'query': "select *, cast('BRAND' as varchar) as brand from {schema_ds1}.{ds1}",
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'brand', 'data_type': 'varchar', 'derived': True}]
                }
    assert actual == expected


def test_derived_column_with_dashed_dataset():
    tree = tokenize('derive columns in d-s4 with sales/goals as attain-ment of type double', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'd-s4',
                'derived': [{'formula': 'sales/goals', 'column_alias': 'attain-ment', 'data_type': 'double'}],
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'},
                                     {'column_name': 'attain-ment', 'data_type': 'double', 'derived': True}],
                'query': 'select *, cast(sales/goals as double) as attain-ment from {schema_d-s4}.{d-s4}',
                'rule_type': 'derived',
                'parameters': []}
    assert actual == expected


def test_derived_column_when_then_with_conditional_formula():
    tree = tokenize("derive columns in ds1 with case when ({day(c1)} > 0) then {'s1'} else 's2' end as segment "
                    "of type varchar", datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'case':
                                 [{'when':
                                       {'FORMULA': 'day(c1)', 'OPERATOR': '>', 'VALUE': '0'},
                                   'then': "'s1'"},
                                  {'else': "'s2'"}],
                             'column_alias': 'segment',
                             'data_type': 'varchar'}],
                'query': "select *, cast(case  when day(c1) > 0 then 's1'  else 's2' end as varchar) as segment "
                         "from {schema_ds1}.{ds1}",
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'segment', 'data_type': 'varchar', 'derived': True}]
                }
    assert actual == expected


def test_derived_with_square_bracket():
    tree = tokenize('derive columns in ds1 with {dbo.[net_working_days] (Data_Period,Data_Period)} as attainment '
                    'of type number', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1',
                'derived': [{'formula': 'dbo.[net_working_days] (Data_Period,Data_Period)',
                             'column_alias': 'attainment',
                             'data_type': 'numeric'}],
                'query': 'select *, cast(dbo.[net_working_days] (Data_Period,Data_Period) as numeric) as '
                         'attainment from {schema_ds1}.{ds1}',
                'parameters': [],
                'rule_type': 'derived',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'attainment', 'data_type': 'numeric', 'derived': True}]
                }
    assert actual == expected


def test_derived_with_percent_character():
    tree = tokenize("Derive columns in DS2 with {concat(Attainment,'%')} as normalised_nrx of type Text", datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds2', 'derived': [{'formula': "concat(Attainment,'%')",
                                             'column_alias': 'normalised_nrx',
                                             'data_type': 'varchar'}],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'},
                                     {'column_name': 'normalised_nrx', 'data_type': 'varchar', 'derived': True}],
                'rule_type': 'derived',
                'query': "select *, cast(concat(Attainment,'%%') as varchar) as normalised_nrx from {schema_ds2}.{ds2}",
                'parameters': []}
    assert actual == expected


def test_derived_with_hashed_value():
    tree = tokenize("Derive columns in DS3 with Case when (ds1#c1 = ds2#C5) then {'ABC'} "
                    "else {'CDE'} end as is_brand of type Text ", datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds3', 'derived': [{'case': [{'when': {'COLUMN': 'ds1#c1',
                                                                'OPERATOR': '=',
                                                                'VALUE': 'ds2#C5'},
                                                       'then': "'ABC'"},
                                                      {'else': "'CDE'"}], 'column_alias': 'is_brand',
                                             'data_type': 'varchar'}],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'},
                                     {'column_name': 'is_brand', 'data_type': 'varchar', 'derived': True}],
                'rule_type': 'derived',
                'query': "select *, cast(case  when ds1#c1 = ds2#C5 then 'ABC'  else 'CDE' end as varchar) as "
                         "is_brand from {schema_ds3}.{ds3}",
                'parameters': []}
    assert actual == expected


# --------------------------------
# Integrity negative - test cases
# --------------------------------


def test_derived_rule_with_duplicate_column_alias_with_existing_columns():
    tree = tokenize('derive columns in ds1 with sales/goals as c2 of type double', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c2\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_derived_rule_using_duplicate_column_alias():
    tree = tokenize('derive columns in ds1 with $factor*nrx as c5 of type float, '
                    'sales/goals as c5 of type float', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c5\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected
