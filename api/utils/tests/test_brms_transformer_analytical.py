import pytest

from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json
from utils.exceptions import BRMSRuleIntegrityFailed

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'},
            {'column_name': 'ds2#C3', 'data_type': 'numeric'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}]
}
datasets = Datasets.initialize(dict_datasets)


def test_analytical_phrase():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c2 of type numeric as'
                    'new_column partitioned by c3,c4 in asc order of c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds2',
                'analytical': {'function_name': 'cumulative_sum', 'column': 'c2',
                               'group_by': ['c3', 'c4'], 'order_column': 'C5', 'order_type': 'asc', 'dtype': 'numeric',
                               'alias': 'new_column'},
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'},
                                     {'column_name': 'new_column', 'data_type': 'numeric', 'derived': True}],
                'query': 'select *, cast(cumulative_sum(c2) over(partition by c3,c4 order by c5 asc rows '
                         'unbounded preceding) as numeric) '
                         'as new_column from {schema_ds2}.{ds2}',
                'rule_type': 'analytical',
                'parameters': []}

    assert actual == expected


def test_analytical_datasets_alternate_cases():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c2 of type numeric as'
                    'new_column partitioned by C3,C4 in asc order of c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds2',
                'analytical': {'function_name': 'cumulative_sum', 'column': 'c2', 'group_by': ['c3', 'c4'],
                               'order_column': 'C5', 'order_type': 'asc', 'dtype': 'numeric', 'alias': 'new_column'},
                'query': 'select *, cast(cumulative_sum(c2) over(partition by c3,c4 order by c5 asc rows unbounded '
                         'preceding) as numeric) '
                         'as new_column from {schema_ds2}.{ds2}',
                'parameters': [],
                'rule_type': 'analytical',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'},
                                     {'column_name': 'new_column', 'data_type': 'numeric', 'derived': True}],
                }
    assert actual == expected


def test_analytical_phrase_with_hashed_column_names():
    tree = tokenize('analytical derive columns in ds3 using cumulative_sum on ds1#c1 of type numeric as'
                    'new_column partitioned by ds2#c5 in asc order of ds2#c3', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds3',
                'analytical': {'function_name': 'cumulative_sum', 'column': 'ds1#c1', 'group_by': ['ds2#C5'],
                               'order_column': 'ds2#C3', 'order_type': 'asc', 'dtype': 'numeric',
                               'alias': 'new_column'},
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C3', 'data_type': 'numeric'},
                                     {'column_name': 'new_column', 'data_type': 'numeric', 'derived': True}],
                'query': 'select *, cast(cumulative_sum(ds1#c1) over(partition by ds2#C5 order by ds2#c3 asc rows '
                         'unbounded preceding) '
                         'as numeric) as new_column from {schema_ds3}.{ds3}',
                'rule_type': 'analytical',
                'parameters': []}

    assert actual == expected


def test_analytical_phrase_with_dashed_dataset():
    tree = tokenize('analytical derive columns in d-s4 using cumulative_sum on d-s5#c1 of type numeric as'
                    'new_column partitioned by ds#c2 in asc order of d-s5#c1', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'd-s4',
                'analytical': {'function_name': 'cumulative_sum', 'column': 'd-s5#c1', 'group_by': ['ds#c2'],
                               'order_column': 'd-s5#c1', 'order_type': 'asc', 'dtype': 'numeric',
                               'alias': 'new_column'},
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'},
                                     {'column_name': 'new_column', 'data_type': 'numeric', 'derived': True}],
                'query': 'select *, cast(cumulative_sum(d-s5#c1) over(partition by ds#c2 order by d-s5#c1 asc rows '
                         'unbounded preceding) as '
                         'numeric) as new_column from {schema_d-s4}.{d-s4}',
                'rule_type': 'analytical',
                'parameters': []}
    assert actual == expected

# --------------------------------
# Integrity negative - test cases
# --------------------------------


def test_analytical_to_group_on_duplicate_columns():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c2 of type numeric as'
                    'new_column partitioned by c1,c1 in asc order of c5', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_analytical_to_group_on_multiple_duplicate_columns():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c2 of type numeric as'
                    'new_column partitioned by c1,c1, c2, c2 in asc order of c5', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1, c2\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_analytical_to_group_non_existent_column():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c2 of type numeric as'
                    'new_column partitioned by c1,cx in asc order of c5', datasets)
    expected = 'Unexpected column names found - cx\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_analytical_to_group_multiple_non_existent_column():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c2 of type numeric as'
                    'new_column partitioned by c1,cx, cy in asc order of c5', datasets)
    expected = 'Unexpected column names found - cx, cy\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_analytical_function_on_non_existent_column():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c9 of type numeric as'
                    'new_column partitioned by c1, c2 in asc order of c5', datasets)
    expected = 'Unexpected column names found - c9\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_analytical_order_on_non_existent_column():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c2 of type numeric as'
                    'new_column partitioned by c1, c2 in asc order of c9', datasets)
    expected = 'Unexpected column names found - c9\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_analytical_rule_with_duplicate_column_alias_with_existing_columns():
    tree = tokenize('analytical derive columns in ds2 using cumulative_sum on c2 of type numeric as'
                    'c2 partitioned by c1, c2 in asc order of c5', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c2\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected
