#   datamax/api/utils/tests/test_brms_validation_transformer.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.datasets import Datasets
from utils.brms_validation_grammar import tokenize
from utils.brms_validation_transformer import to_json

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'C2', 'data_type': 'varchar'},
            {'column_name': 'C3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}]
}
datasets = Datasets.initialize(dict_datasets)


def test_data_type_integer():
    tree = tokenize('data type of ds2.c1 is integer', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'data_type_integer', 'input': 'ds2', 'column': 'c1',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}],
                'query': "insert into {schema_error_ds2}.{error_ds2} (select * , 'c1 value is not integer' as "
                         "dmx_message from {schema_ds2}.{ds2} where cast(c1 as varchar) !~ '^(-)?[0-9]+$' )",
                'parameters': [], 'output': 'ds2'}
    assert actual == expected


def test_data_type_integer_with_hashed_dataset():
    tree = tokenize('data type of d-s4.ds#c2 is integer', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'data_type_integer', 'input': 'd-s4', 'column': 'ds#c2',
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'}],
                'query': "insert into {schema_error_d-s4}.{error_d-s4} (select * , 'ds#c2 value is not integer' as"
                         " dmx_message from {schema_d-s4}.{d-s4} where cast(ds#c2 as varchar) !~ '^(-)?[0-9]+$' )",
                'parameters': [], 'output': 'd-s4'}
    assert actual == expected


def test_data_type_integer_case_treatment():
    tree = tokenize('data type of ds2.C1 is integer', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'data_type_integer', 'column': 'c1', 'input': 'ds2', 'output': 'ds2',
                'parameters': [],
                'query': "insert into {schema_error_ds2}.{error_ds2} (select * , 'c1 value is not integer' as "
                         "dmx_message from {schema_ds2}.{ds2} where cast(c1 as varchar) !~ '^(-)?[0-9]+$' )",
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_data_type_float():
    tree = tokenize('data type of ds2.c1 is float', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'data_type_float', 'column': 'c1', 'input': 'ds2', 'output': 'ds2',
                'parameters': [],
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not float' as "
                         "dmx_message from {schema_ds2}.{ds2} where cast(c1 as varchar) !~ '^[0-9]*.?[0-9]*$' )",
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_data_type_date():
    tree = tokenize('data type of ds2.c1 is date with format of YYYY-MM-DD', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'data_type_date', 'input': 'ds2', 'column': 'c1', 'format': 'YYYY-MM-DD',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not date "
                         "with - YYYY-MM-DD' as dmx_message from {schema_ds2}.{ds2} where cast(c1 as varchar) !~ "
                         "'^[0-9][0-9][0-9][0-9]-(((0)[0-9])|((1)[0-2]))-(((0)[0-9])|([1-2][0-9])|([3][0-1]))$')",
                'parameters': [], 'output': 'ds2',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_data_type_date_with_invalid_format():
    with pytest.raises(Exception):
        tree = tokenize('data type of ds2.c1 is date with format of YYYY/MM-DD', datasets)
        to_json(tree, datasets)


def test_unique_values():
    tree = tokenize('unique values in ds2.c1', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'unique_values', 'column': 'c1', 'input': 'ds2', 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *,  'c1 value is not unique' as "
                         "dmx_message from {schema_ds2}.{ds2} A where exists (select 'x' from (select c1, "
                         "count(0) from {schema_ds2}.{ds2} group by c1 having count(0) > 1) B where A.c1 = B.c1))",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_allowed_values():
    tree = tokenize('allowed values in ds2.c1 are q1, q2, q3, q4', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'allowed_values', 'column': 'c1',
                'values': "'q1', 'q2', 'q3', 'q4'", 'input': 'ds2', 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not allowed' as "
                         "dmx_message from {schema_ds2}.{ds2} where c1 not in ('q1', 'q2', 'q3', 'q4'))",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_allowed_values_with_space():
    tree = tokenize('allowed values in ds2.c1 are true value, first, third value', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'allowed_values', 'input': 'ds2', 'column': 'c1',
                'values': "'true value', 'first', 'third value'",
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}],
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not allowed' as "
                         "dmx_message from {schema_ds2}.{ds2} where c1 not in ('true value', 'first', 'third value'))",
                'parameters': [], 'output': 'ds2'}
    assert actual == expected


def test_column_length():
    tree = tokenize('length of values in ds2.c1 = 2', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'column_length', 'column': 'c1', 'operator': '=', 'value': '2',
                'input': 'ds2', 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value length is not = 2'  "
                         "as dmx_message from {schema_ds2}.{ds2} where not (length(cast(c1 as varchar)) = 2))",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_alphanumeric_only():
    tree = tokenize('allowed values in ds2.c1 are alphanumeric', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'alphanumeric_only', 'column': 'c1', 'input': 'ds2', 'parameters': [], 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not alphanumeric' as "
                         "dmx_message from {schema_ds2}.{ds2} where cast(c1 as varchar) !~ '^[a-zA-Z0-9]*$')",
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_email_only():
    tree = tokenize('allowed values in ds2.c1 are email', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'email_only', 'column': 'c1', 'input': 'ds2', 'parameters': [], 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not email' as "
                         "dmx_message from {schema_ds2}.{ds2} where cast(c1 as varchar) NOT LIKE '%_@__%.__%')",
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_alpha_only():
    tree = tokenize('allowed values in ds2.c1 are alpha', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'alpha_only', 'column': 'c1', 'input': 'ds2', 'parameters': [], 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not alpha' as "
                         "dmx_message from {schema_ds2}.{ds2} where cast(c1 as varchar) ~* '[^A-Z]')",
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_uppercase_only():
    tree = tokenize('allowed values in ds2.c1 are upper case', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'uppercase_only', 'column': 'c1', 'input': 'ds2', 'parameters': [], 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not upper case' as "
                         "dmx_message from {schema_ds2}.{ds2} where cast(c1 as varchar) ~ '[^A-Z]')",
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_self_referenced():
    tree = tokenize('referenced values of ds2.c1 are in ds2.c2', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'referenced', 'columns': ['c1', 'C2'], 'input': ['ds2', 'ds2'], 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is not referenced' "
                         "as dmx_message from {schema_ds2}.{ds2} x where x.c1 in (select A.c1 from "
                         "{schema_ds2}.{ds2} A left join {schema_ds2}.{ds2} B on A.c1 = B.C2 where B.C2 is null))",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_cross_referenced():
    tree = tokenize('referenced values of ds1.c1 are in ds2.c2', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'referenced', 'input': ['ds1', 'ds2'], 'columns': ['c1', 'C2'], 'output': 'ds1',
                'query': "insert into {schema_error_ds1}.{error_ds1} (select *, 'c1 value is not referenced' as"
                         " dmx_message from {schema_ds1}.{ds1} x where x.c1 in (select A.c1 from "
                         "{schema_ds1}.{ds1} A left join {schema_ds2}.{ds2} B on A.c1 = B.C2 where B.C2 is null))",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_cross_referenced_existing():
    tree = tokenize('referenced values of ds1.c1 are in existing schema2.table2.c2', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'referenced', 'input': ['ds1', 'schema2.table2'],
                'output': 'ds1',
                'referenced_conditions': [{'column': 'c2',
                                           'schema': 'schema2',
                                           'table': 'table2'}],
                'query': "insert into {schema_error_ds1}.{error_ds1} (select *, 'c1 value "
                         "is not referenced' as dmx_message from {schema_ds1}.{ds1} x where "
                         "(x.c1 in (select A.c1 from {schema_ds1}.{ds1} A left join "
                         "schema2.table2 B on A.c1 = B.c2 where B.c2 is null)))",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_cross_referenced_existing_two_tables():
    tree = tokenize('referenced values of ds1.c1 are in existing schema2.table2.c2, schema3.table3.c3', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'referenced', 'input': ['ds1', 'schema2.table2', 'schema3.table3'],
                'output': 'ds1',
                'referenced_conditions': [{'schema': 'schema2', 'table': 'table2', 'column': 'c2'},
                                          {'schema': 'schema3', 'table': 'table3', 'column': 'c3'}],
                'query': "insert into {schema_error_ds1}.{error_ds1} (select *, 'c1 value "
                         "is not referenced' as dmx_message from {schema_ds1}.{ds1} x where "
                         "(x.c1 in (select A.c1 from {schema_ds1}.{ds1} A left join "
                         "schema2.table2 B on A.c1 = B.c2 where B.c2 is null)) and (x.c1 in "
                         "(select A.c1 from {schema_ds1}.{ds1} A left join schema3.table3 B "
                         "on A.c1 = B.c3 where B.c3 is null)))",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                }
    assert actual == expected


def test_cross_referenced_existing_table_with_filter():
    tree = tokenize('referenced values of ds1.c1 are in existing schema2.table2.c2 where (c2 > 0)', datasets)
    actual = to_json(tree, datasets)
    # expected = {}
    # assert actual == expected


def test_cross_referenced_existing_table_with_nested_filter():
    tree = tokenize('referenced values of ds1.c1 are in existing schema2.table2.c2 where '
                    'or((c2 > 0)(c4 = 9))', datasets)
    actual = to_json(tree, datasets)
    # expected = {}
    # assert actual == expected


def test_cross_referenced_existing_two_tables_with_one_filter():
    tree = tokenize('referenced values of ds1.c1 are in existing schema2.table2.c2 where (c2 > 0), '
                    'schema3.table3.c3', datasets)
    actual = to_json(tree, datasets)
    # expected = {}
    # assert actual == expected


def test_cross_referenced_existing_two_tables_with_two_filters():
    tree = tokenize('referenced values of ds1.c1 are in existing schema2.table2.c2 where (c2 > 0), '
                    'schema3.table3.c3 where (c4 = 0)', datasets)
    actual = to_json(tree, datasets)
    # expected = {}
    # assert actual == expected


def test_mandatory_column():
    tree = tokenize('mandatory column ds2.c1', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'mandatory_column', 'column': 'c1', 'input': 'ds2', 'output': 'ds2',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 value is mandatory' as "
                         "dmx_message from {schema_ds2}.{ds2} where c1 is null)",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_compare_numeric():
    tree = tokenize('compare numeric in ds2.c1 > ds2.c2', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'compare_numeric', 'input': ['ds2', 'ds2'], 'columns': ['c1', 'C2'], 'operator': '>',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 comparison failed with C2' as "
                         "dmx_message from {schema_ds2}.{ds2} where not "
                         "({schema_ds2}.{ds2}.c1 > {schema_ds2}.{ds2}.C2))",
                'parameters': [], 'output': 'ds2',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_compare_numeric_among_two_tables():
    tree = tokenize('compare numeric in ds2.c1 > ds1.c1', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'compare_numeric', 'input': ['ds2', 'ds1'], 'columns': ['c1', 'c1'], 'operator': '>',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 comparison failed with c1' as "
                         "dmx_message from {schema_ds2}.{ds2} where not "
                         "({schema_ds2}.{ds2}.c1 > {schema_ds1}.{ds1}.c1))",
                'parameters': [], 'output': 'ds2',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_compare_date_among_two_tables():
    tree = tokenize('compare date in ds2.c1 = ds1.c1 with format of YYYY-MM-DD, YYYY-MM-DD', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'compare_date', 'input': ['ds2', 'ds1'], 'columns': ['c1', 'c1'], 'operator': '=',
                'formats': ['YYYY-MM-DD', 'YYYY-MM-DD'],
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'The value of c1 column is  "
                         "not = c1 value' from {schema_ds2}.{ds2} where {schema_ds2}.{ds2}.c1 ~ "
                         "'^[0-9][0-9][0-9][0-9]-(((0)[0-9])|((1)[0-2]))-(((0)[0-9])|([1-2][0-9])|([3][0-1]))$' "
                         "and {schema_ds1}.{ds1}.c1 ~ "
                         "'^[0-9][0-9][0-9][0-9]-(((0)[0-9])|((1)[0-2]))-(((0)[0-9])|([1-2][0-9])|([3][0-1]))$' "
                         "and not (to_date({schema_ds2}.{ds2}.c1,'YYYY-MM-DD') = "
                         "to_date({schema_ds1}.{ds1}.c1,'YYYY-MM-DD')))",
                'parameters': [], 'output': 'ds2',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }

    assert actual == expected


def test_compare_date_within_a_table():
    tree = tokenize('compare date in ds2.c1 > ds2.c2 with format of YYYY/MM/DD, YYYY-MM-DD', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'compare_date', 'input': ['ds2', 'ds2'], 'columns': ['c1', 'C2'], 'operator': '>',
                'formats': ['YYYY/MM/DD', 'YYYY-MM-DD'],
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'The value of c1 column is  not > C2 "
                         "value' from {schema_ds2}.{ds2} where {schema_ds2}.{ds2}.c1 ~ "
                         "'^[0-9][0-9][0-9][0-9]/(((0)[0-9])|((1)[0-2]))/(((0)[0-9])|([1-2][0-9])|([3][0-1]))$' "
                         "and {schema_ds2}.{ds2}.C2 ~ "
                         "'^[0-9][0-9][0-9][0-9]-(((0)[0-9])|((1)[0-2]))-(((0)[0-9])|([1-2][0-9])|([3][0-1]))$' "
                         "and not (to_date({schema_ds2}.{ds2}.c1,'YYYY/MM/DD') > "
                         "to_date({schema_ds2}.{ds2}.C2,'YYYY-MM-DD')))",
                'parameters': [], 'output': 'ds2',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_range_numeric():
    tree = tokenize('range of numeric in ds2.c1 is between 10 and 20', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'range_numeric', 'input': 'ds2', 'column': 'c1', 'values': ['10', '20'],
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'c1 not in range' from "
                         "{schema_ds2}.{ds2} where not ({schema_ds2}.{ds2}.c1 between 10 and 20))",
                'parameters': [], 'output': 'ds2',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_range_date():
    tree = tokenize('range of date in ds2.c1 is between 2019/01/12 and 2019/10/12 with format of YYYY/DD/MM', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'range_date', 'input': 'ds2', 'column': 'c1', 'values': ['2019/01/12', '2019/10/12'],
                'format': 'YYYY/DD/MM',
                'query': "insert into {schema_error_ds2}.{error_ds2} (select *, 'The values of a date column is not in"
                         " the given date range' from {schema_ds2}.{ds2} where  c1 ~ "
                         "'^[0-9][0-9][0-9][0-9]/(((0)[0-9])|([1-2][0-9])|([3][0-1]))/(((0)[0-9])|((1)[0-2]))$' "
                         "and not ( to_date(c1,'YYYY/DD/MM') between to_date('2019/01/12','YYYY/DD/MM') and "
                         "to_date('2019/10/12','YYYY/DD/MM')))",
                'parameters': [], 'output': 'ds2',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'C2', 'data_type': 'varchar'},
                                     {'column_name': 'C3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_range_date_invalid_format():
    tree = tokenize('range of date in ds2.c1 is between 2019/01/12 and 2019/10/12 with format of YYYY-DD/MM', datasets)
    with pytest.raises(Exception):
        to_json(tree, datasets)


def test_composite_key():
    tree = tokenize('unique values in ds1 c1, c2', datasets)
    actual = to_json(tree, datasets)
    expected = {'rule_type': 'composite_key', 'input': 'ds1', 'columns': ['c1', 'c2'],
                'query': "insert into {schema_error_ds1}.{error_ds1} (select *, 'unique constraint violated' "
                         "from {schema_ds1}.{ds1} cd where exists (select 'x' from (select c1,c2, count(0) from "
                         "{schema_ds1}.{ds1} group by c1,c2 having count(0) > 1) cc  where cc.c1 = cd.c1 "
                         "and cc.c2 = cd.c2))",
                'parameters': [], 'output': 'ds1',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected
