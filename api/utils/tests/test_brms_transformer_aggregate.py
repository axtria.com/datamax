#   datamax/api/utils/tests/test_brms_transformer_aggregate.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json
from utils.exceptions import BRMSRuleIntegrityFailed

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'C4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'},
            {'column_name': 'ds2#C3', 'data_type': 'numeric'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}]
}
datasets = Datasets.initialize(dict_datasets)


def test_aggregate_single_condition():
    tree = tokenize('Aggregate ds1 using c1, c2 to derive sum(nrx) as sum_nrx of type double', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['c1', 'c2'],
                              'agg_expr': [{'formula': 'sum(nrx)',
                                            'column_alias': 'sum_nrx',
                                            'data_type': 'double'}]},
                'input': 'ds1',
                'query': 'select c1, c2, cast(sum(nrx) as double) as sum_nrx from {schema_ds1}.{ds1} group by c1, c2',
                'parameters': [],
                'rule_type': 'aggregate',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'double', 'derived': True}]
                }
    assert actual == expected


def test_aggregate_multi_columns():
    tree = tokenize('Aggregate ds1 using c1, c2 to derive sum(nrx) as sum_nrx of type float, '
                    'count(hcp_id) as count_hcp of type integer', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['c1', 'c2'],
                              'agg_expr': [{'formula': 'sum(nrx)',
                                            'column_alias': 'sum_nrx',
                                            'data_type': 'float'},
                                           {'formula': 'count(hcp_id)',
                                            'column_alias': 'count_hcp',
                                            'data_type': 'integer'}
                                           ]},
                'input': 'ds1',
                'query': 'select c1, c2, cast(sum(nrx) as float) as sum_nrx, cast(count(hcp_id) as integer) as '
                         'count_hcp from {schema_ds1}.{ds1} group by c1, c2',
                'parameters': [],
                'rule_type': 'aggregate',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'count_hcp', 'data_type': 'integer', 'derived': True}]
                }
    assert actual == expected


def test_aggregate_single_having_condition():
    tree = tokenize('Aggregate ds1 using c1, c2 to derive sum(nrx) as sum_nrx of type float having (nrx > 1)', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['c1', 'c2'],
                              'agg_expr': [{'formula': 'sum(nrx)',
                                            'column_alias': 'sum_nrx',
                                            'data_type': 'float'}
                                           ],
                              'having': {'rules':
                                             [{'COLUMN': 'nrx', 'OPERATOR': '>', 'VALUE': '1'}]}
                              },
                'input': 'ds1',
                'query': 'select c1, c2, cast(sum(nrx) as float) as sum_nrx from {schema_ds1}.{ds1} group by c1, c2 '
                         'having nrx > 1',
                'parameters': [],
                'rule_type': 'aggregate',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'float', 'derived': True}]
                }
    assert actual == expected


def test_aggregate_single_having_condition_multiple():
    tree = tokenize('Aggregate ds1 using c1, c2 to derive sum(nrx) as sum_nrx of type float having '
                    'OR((nrx > 1)(is_active = true))', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['c1', 'c2'],
                              'agg_expr': [{'formula': 'sum(nrx)',
                                            'column_alias': 'sum_nrx',
                                            'data_type': 'float'}
                                           ],
                              'having': {'condition': 'OR',
                                         'rules':
                                             [{'COLUMN': 'nrx', 'OPERATOR': '>', 'VALUE': '1'},
                                              {'COLUMN': 'is_active', 'OPERATOR': '=', 'VALUE': 'true'}]
                                         }
                              },
                'input': 'ds1',
                'query': 'select c1, c2, cast(sum(nrx) as float) as sum_nrx from {schema_ds1}.{ds1} group by c1, c2 '
                         'having (nrx > 1) or (is_active = true)',
                'parameters': [],
                'rule_type': 'aggregate',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'float', 'derived': True}]
                }
    assert actual == expected


def test_aggregate_single_condition_with_unalike_case():
    tree = tokenize('Aggregate ds1 using C1, C2 to derive sum(nrx) as sum_nrx of type double', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['c1', 'c2'],
                              'agg_expr': [{'formula': 'sum(nrx)',
                                            'column_alias': 'sum_nrx',
                                            'data_type': 'double'}]},
                'input': 'ds1',
                'query': 'select c1, c2, cast(sum(nrx) as double) as sum_nrx from {schema_ds1}.{ds1} group by c1, c2',
                'parameters': [],
                'rule_type': 'aggregate',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'double', 'derived': True}]
                }
    assert actual == expected


def test_aggregate_multi_columns_with_unalike_case():
    tree = tokenize('Aggregate ds2 using c1, C2, c5 to derive sum(nrx) as sum_nrx of type float, '
                    'count(hcp_id) as count_hcp of type integer', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['c1', 'c2', 'C5'],
                              'agg_expr': [{'formula': 'sum(nrx)',
                                            'column_alias': 'sum_nrx',
                                            'data_type': 'float'},
                                           {'formula': 'count(hcp_id)',
                                            'column_alias': 'count_hcp',
                                            'data_type': 'integer'}
                                           ]},
                'input': 'ds2',
                'query': 'select c1, c2, C5, cast(sum(nrx) as float) as sum_nrx, cast(count(hcp_id) as integer) as '
                         'count_hcp from {schema_ds2}.{ds2} group by c1, c2, C5',
                'parameters': [],
                'rule_type': 'aggregate',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'float', 'derived': True},
                                     {'column_name': 'count_hcp', 'data_type': 'integer', 'derived': True}]
                }
    assert actual == expected


def test_aggregate_single_having_condition_with_dataset_in_unalike_case():
    tree = tokenize('Aggregate ds1 using c1, c2 to derive sum(nrx) as sum_nrx of type float having (nrx > 1)', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['c1', 'c2'],
                              'agg_expr': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx', 'data_type': 'float'}],
                              'having': {'rules': [{'COLUMN': 'nrx', 'OPERATOR': '>', 'VALUE': '1'}]}},
                'input': 'ds1',
                'query': 'select c1, c2, cast(sum(nrx) as float) as sum_nrx from {schema_ds1}.{ds1} group by '
                         'c1, c2 having nrx > 1',
                'parameters': [],
                'rule_type': 'aggregate',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'float', 'derived': True}]
                }
    assert actual == expected


def test_aggregate_single_condition_with_hashed_columns():
    tree = tokenize('Aggregate ds3 using ds1#c1, ds2#c5 to derive sum(nrx) as sum_nrx of type double', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['ds1#c1', 'ds2#C5'],
                              'agg_expr': [{'formula': 'sum(nrx)',
                                            'column_alias': 'sum_nrx',
                                            'data_type': 'double'}]},
                'input': 'ds3',
                'query': 'select ds1#c1, ds2#C5, cast(sum(nrx) as double) as sum_nrx from {schema_ds3}.{ds3} group by '
                         'ds1#c1, ds2#C5',
                'parameters': [],
                'rule_type': 'aggregate',
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'double', 'derived': True}]
                }
    assert actual == expected


def test_aggregate_single_condition_with_dashed_dataset():
    tree = tokenize('Aggregate d-s4 using d-s5#c1, ds#c2  to derive sum(nrx) as sum_nrx of type double', datasets)
    actual = to_json(tree, datasets)
    expected = {'aggregate': {'group_by': ['d-s5#c1', 'ds#c2'],
                              'agg_expr': [{'formula': 'sum(nrx)', 'column_alias': 'sum_nrx', 'data_type': 'double'}]},
                'input': 'd-s4',
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'},
                                     {'column_name': 'sum_nrx', 'data_type': 'double', 'derived': True}],
                'query': 'select d-s5#c1, ds#c2, cast(sum(nrx) as double) as sum_nrx from {schema_d-s4}.{d-s4} '
                         'group by '
                         'd-s5#c1, ds#c2',
                'parameters': [],
                'rule_type': 'aggregate'}
    assert actual == expected


# --------------------------------
# Integrity negative - test cases
# --------------------------------


def test_aggregate_with_duplicate_columns():
    tree = tokenize('Aggregate ds1 using c1, c1 to derive sum(nrx) as sum_nrx of type double', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_aggregate_with_multiple_duplicate_columns():
    tree = tokenize('Aggregate ds1 using c1, c1, c2, c2 to derive sum(nrx) as sum_nrx of type double', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1, c2\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_aggregate_with_non_existent_column():
    tree = tokenize('Aggregate ds1 using c1, c9 to derive sum(nrx) as sum_nrx of type double', datasets)
    expected = 'Unexpected column names found - c9\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_aggregate_with_multiple_non_existent_column():
    tree = tokenize('Aggregate ds1 using c1, c9, c4 to derive sum(nrx) as sum_nrx of type double', datasets)
    expected = 'Unexpected column names found - c9, c4\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_aggregate_rule_with_duplicate_column_alias_with_existing_columns():
    tree = tokenize('Aggregate ds1 using c1, c2 to derive sum(nrx) as c1 of type double', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - c1\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected


def test_aggregate_rule_using_duplicate_column_alias():
    tree = tokenize('Aggregate ds1 using c1, c2 to derive sum(nrx) as sum_nrx of type float, '
                    'count(hcp_id) as sum_nrx of type integer', datasets)
    expected = 'Expected names to be unique.\nDuplicate found - sum_nrx\n'
    with pytest.raises(BRMSRuleIntegrityFailed) as e:
        to_json(tree, datasets)
    assert e.value.message == expected
