schema = 'system_config.dq_job_config'

def auth_query():
    sql_query = "select connection from system_config.connection_meta where name='dq_api_auth'"
    return sql_query

def active_job_check_query(job_id):
    sql_query =  """select count(1) from {schema} where dq_job_id = '{jobid}' and is_actv = true 
                     and is_job_actv = true""".format(jobid=job_id,schema=schema)
    return sql_query

def valid_job_check_query(job_id):
    sql_query = """select count(1) from {schema}
                 where dq_job_id  = '{jobid}' """.format(jobid=job_id,schema=schema)
    return sql_query                                    

def dv_scnr_pass_query(var,var_schema,query_char):
    sql_query = """UPDATE {var_schema}.DQ_JOB_RUN SET EXEC_RSLT = 'Pass' , run_end_date=to_timestamp_ntz(current_timestamp()) WHERE 
                {var_schema}.DQ_JOB_RUN.DQ_RUN_ID {query_char} {var}; select * from {var_schema}.DQ_JOB_RUN 
                where DQ_RUN_ID {query_char} {var}""" .format(var_schema=var_schema,var=var,query_char=query_char )
    return sql_query

def scnr_pass_query(var_schema,run_id):
    sql_query =  """UPDATE {var_schema}.DQ_JOB_RUN  SET EXEC_RSLT = 'Pass' , run_end_date=to_timestamp_ntz(current_timestamp()) 
                     WHERE {var_schema}.DQ_JOB_RUN.DQ_RUN_ID ={run_id}; select * from {var_schema}.DQ_JOB_RUN where 
                     DQ_RUN_ID={run_id};""".format(var_schema=var_schema, run_id=run_id)
    return sql_query

def scnr_fail_query(var_schema,msg,run_id):
    sql_query = """UPDATE {var_schema}.DQ_JOB_RUN SET EXEC_RSLT = '{error_msg}', RUN_STATUS = 'Query Fail' ,
                    run_end_date=to_timestamp_ntz(current_timestamp()) WHERE {var_schema}.DQ_JOB_RUN.DQ_RUN_ID={run_id}; 
                    select * from {var_schema}.DQ_JOB_RUN
                     where DQ_RUN_ID={run_id}""".format(var_schema=var_schema, error_msg= msg,run_id=run_id)    
    return sql_query

def sfk_fetch_all_query(var_schema,job_id):
    sql_query = "select * from {var_schema}.dq_job_run where dq_job_id = '{jobid}'".format(var_schema=var_schema, jobid=job_id)
    return sql_query

def dv_scnr_no_run_query(var_schema,run_id,job_id,rule_id):
    sql_query = """insert into {var_schema}.dq_job_run(dq_run_id,dq_job_id,rule_id,run_end_date,run_status,exec_rslt) values
                    ({run_id}, '{job_id}', '{rule_id}', to_timestamp_ntz(current_timestamp()) , 'No Run', 'No Run'); select * from 
                    {var_schema}.DQ_JOB_RUN where DQ_RUN_ID = {run_id} ;""".format(var_schema=var_schema, run_id= run_id, job_id = job_id, rule_id =  rule_id)
    return sql_query

def dv_scnr_fail_query(var_schema,run_id,msg):
    sql_query = """update {var_schema}.dq_job_run set run_status = 'Query Fail', exec_rslt = '{msg}' , 
                    run_end_date=to_timestamp_ntz(current_timestamp()) where dq_run_id = {run_id}; select * from {var_schema}.DQ_JOB_RUN
                    where DQ_RUN_ID={run_id};""".format (var_schema=var_schema, run_id= run_id,msg= msg)
    return sql_query

def dv_scnr_pass_run_id_query(var_schema,run_id):
    sql_query = """update {var_schema}.dq_job_run set exec_rslt = 'Pass', run_end_date=to_timestamp_ntz(current_timestamp())
                    where dq_run_id = {run_id}; select * from {var_schema}.DQ_JOB_RUN where DQ_RUN_ID={run_id};
                    """.format(var_schema=var_schema, run_id=run_id)
    return sql_query

def tuple_maker(df):
    tup = tuple(map(int, df.split(',')))
    tup_len = tup.__len__()
    var=tup[0] if tup_len == 1 else tup
    return var

def kpi_output_insrt_query(var_schema,rule_id,dq_run_id,bk_attr_val):
    rule_id = tuple_maker(rule_id)
    dq_run_id = tuple_maker(dq_run_id)
    bk_attr_val = tuple_maker(bk_attr_val)

    sql_query = """insert into {var_schema}.DQ_JOB_RUN (RULE_ID,DQ_RUN_ID,BK_ATTR_VAL)
                 values ({rule_id},{dq_run_id},{bk_attr_val})""".format(rule_id=rule_id,dq_run_id=dq_run_id,bk_attr_val=bk_attr_val)
    return sql_query