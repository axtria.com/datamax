#   datamax/api/utils/brms_validation_transformer.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import logging
import re

from lark import Transformer

from entity.datasets import Datasets
from utils.exceptions import BRMSDateFormatUnsupported


class TreeToJson(Transformer):
    def __init__(self, datasets: Datasets):
        super().__init__()
        self.datasets = datasets

    def data_type_integer(self, tree):
        return {"rule_type": "data_type_integer", 'input': tree[0].value, 'column': tree[1].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def data_type_float(self, tree):
        return {"rule_type": "data_type_float", 'input': tree[0].value, 'column': tree[1].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def data_type_date(self, tree):
        return {"rule_type": 'data_type_date', 'input': tree[0].value, 'column': tree[1].value,
                'format': tree[2].value, "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def unique_values(self, tree):
        return {"rule_type": "unique_values", 'input': tree[0].value, 'column': tree[1].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def allowed_values(self, tree):
        return {"rule_type": "allowed_values", 'input': tree[0].value, 'column': tree[1].value,
                'values': tree[2], "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def column_length(self, tree):
        return {"rule_type": 'column_length', 'input': tree[0].value, 'column': tree[1].value,
                'operator': tree[2].value, 'value': tree[3].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def alphanumeric_only(self, tree):
        return {"rule_type": 'alphanumeric_only', 'input': tree[0].value, 'column': tree[1].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def email_only(self, tree):
        return {"rule_type": 'email_only', 'input': tree[0].value, 'column': tree[1].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def alpha_only(self, tree):
        return {"rule_type": 'alpha_only', 'input': tree[0].value, 'column': tree[1].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def uppercase_only(self, tree):
        return {"rule_type": 'uppercase_only', 'input': tree[0].value, 'column': tree[1].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def referenced(self, tree):
        return {"rule_type": 'referenced', 'input': [tree[0].value, tree[2].value],
                'columns': [tree[1].value, tree[3].value],
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def referenced_parameter(self, tree):
        referenced_conditions = []
        input_tables = [tree[0].value]

        for input_table, table_column_expr in tree[2]:
            input_tables.append(input_table)
            referenced_conditions.append(table_column_expr)

        return {"rule_type": 'referenced', 'input': input_tables,
                'referenced_conditions': referenced_conditions,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def mandatory_column(self, tree):
        return {"rule_type": 'mandatory_column', 'input': tree[0].value, 'column': tree[1].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def compare_numeric(self, tree):
        return {"rule_type": 'compare_numeric', 'input': [tree[0].value, tree[3].value],
                'columns': [tree[1].value, tree[4].value], 'operator': tree[2].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def range_numeric(self, tree):
        return {"rule_type": 'range_numeric', 'input': tree[0].value, 'column': tree[1].value,
                'values': [tree[2].value, tree[3].value],
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def compare_date(self, tree):
        return {"rule_type": 'compare_date', 'input': [tree[0].value, tree[3].value],
                'columns': [tree[1].value, tree[4].value], 'operator': tree[2].value,
                'formats': [tree[5].value, tree[6].value],
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def range_date(self, tree):
        return {"rule_type": 'range_date', 'input': tree[0].value, 'column': tree[1].value,
                'values': [tree[2].value, tree[3].value], 'format': tree[4].value,
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def composite_key(self, tree):
        return {"rule_type": 'composite_key', 'input': tree[0].value, 'columns': tree[1],
                "_output_metadata": self.datasets.get_metadata(tree[0].value)}

    def values(self, tree):
        return ', '.join(map(lambda x: "'" + x.value.strip() + "'", tree))

    def columns(self, tree):
        return list(map(lambda x: x.value, tree))

    def cross_ref_exprs(self, tree):
        return tree

    def table_column(self, tree):
        return tree[0].value + '.' + tree[1].value, \
               {'schema': tree[0].value, 'table': tree[1].value, 'column': tree[2].value}

    def table_column_filter(self, tree):
        return tree[0].value + '.' + tree[1].value, \
               {'schema': tree[0].value, 'table': tree[1].value, 'column': tree[2].value}

    def or_rule(self, tree):
        return {'condition': 'OR', 'rules': tree}

    def and_rule(self, tree):
        return {'condition': 'AND', 'rules': tree}

    def condition(self, tree):
        output = {}
        for child in tree:
            output[child.type] = child.value.strip()
        return output


class TreeToPostgreSQL(Transformer):
    def __init__(self, json: dict):
        super().__init__()
        self.log = logging.getLogger(self.__class__.__name__)
        self.json = json
        self.date_patterns = {
            'YYYY-MM-DD': '^[0-9][0-9][0-9][0-9]-(((0)[0-9])|((1)[0-2]))-(((0)[0-9])|([1-2][0-9])|([3][0-1]))$',
            'YYYY-DD-MM': '^[0-9][0-9][0-9][0-9]-(((0)[0-9])|([1-2][0-9])|([3][0-1]))-(((0)[0-9])|((1)[0-2]))$',
            'YYYY/MM/DD': '^[0-9][0-9][0-9][0-9]/(((0)[0-9])|((1)[0-2]))/(((0)[0-9])|([1-2][0-9])|([3][0-1]))$',
            'YYYY/DD/MM': '^[0-9][0-9][0-9][0-9]/(((0)[0-9])|([1-2][0-9])|([3][0-1]))/(((0)[0-9])|((1)[0-2]))$',
            'DD-MON-YYYY': '^(([1-9])|([0-2][0-9])|([3][0-1]))-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\d{4}$'
        }

    def data_type_integer(self, tree):
        query = "insert into {error_table} (select * , '{column} value is not integer' as dmx_message from " \
                "{input_table} where cast({column} as varchar) !~ '^(-)?[0-9]+$' )"
        return query.format(error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            column=self.json['column'])

    def data_type_float(self, tree):
        query = "insert into {error_table} (select *, '{column} value is not float' as dmx_message from " \
                "{input_table} where cast({column} as varchar) !~ '^[0-9]*.?[0-9]*$' )"
        return query.format(error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            column=self.json['column'])

    def data_type_date(self, tree):
        query = "insert into {error_table} (select *, '{column} value is not date with - " \
                "{format}' as dmx_message from {input_table} " \
                "where cast({column} as varchar) !~ '{pattern}')"
        try:
            pattern = self.date_patterns[self.json['format']]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSDateFormatUnsupported
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            column=self.json['column'], format=self.json['format'], pattern=pattern)

    def unique_values(self, tree):
        query = "insert into {error_table} (select *,  '{column} value is not unique' as dmx_message from " \
                "{input_table} A " \
                "where exists (select 'x' from (select {column}, count(0) from {input_table} group by {column} " \
                "having count(0) > 1) B where A.{column} = B.{column}))"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            column=self.json['column'])

    def allowed_values(self, tree):
        query = "insert into {error_table} (select *, '{column} value is not allowed' as dmx_message " \
                "from {input_table} where {column} not in ({values}))"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            column=self.json['column'], values=self.json['values'])

    def column_length(self, tree):
        query = "insert into {error_table} (select *, '{column} value length is not {operator} {value}' " \
                " as dmx_message from {input_table} where not (length(cast({column} as varchar)) {operator} {value}))"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            column=self.json['column'], value=self.json['value'], operator=self.json['operator'])

    def alphanumeric_only(self, tree):
        query = "insert into {error_table} (select *, '{column} value is not alphanumeric' as dmx_message " \
                "from {input_table} " \
                "where cast({column} as varchar) !~ '^[a-zA-Z0-9]*$')"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            column=self.json['column'])

    def email_only(self, tree):
        query = "insert into {error_table} (select *, '{column} value is not email' as dmx_message from " \
                "{input_table} where cast({column} as varchar) NOT LIKE '%_@__%.__%')"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            column=self.json['column'])

    def alpha_only(self, tree):
        query = "insert into {error_table} (select *, '{column} value is not alpha' as dmx_message from " \
                "{input_table} where cast({column} as varchar) ~* '[^A-Z]')"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            column=self.json['column'])

    def uppercase_only(self, tree):
        query = "insert into {error_table} (select *, '{column} value is not upper case' as dmx_message " \
                "from {input_table} where cast({column} as varchar) ~ '[^A-Z]')"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            column=self.json['column'])

    def referenced(self, tree):
        query = "insert into {error_table} (select *, '{column} value is not referenced' as dmx_message from " \
                "{input_table} x where x.{column} in (select A.{column} from {input_table} A left join " \
                "{parent_table} B " \
                "on A.{column} = B.{parent_column} where B.{parent_column} is null))"
        return query.format(input_table='{schema_' + self.json['input'][0] + '}.{' + self.json['input'][0] + '}',
                            column=self.json['columns'][0], parent_column=self.json['columns'][1],
                            parent_table='{schema_' + self.json['input'][1] + '}.{' + self.json['input'][1] + '}',
                            error_table='{schema_error_' + self.json['input'][0] +
                                        '}.{error_' + self.json['input'][0] + '}')

    def referenced_parameter(self, tree):
        query = "insert into {error_table} (select *, '{parent_column} value is not referenced' " \
                "as dmx_message from {input_table} x where " + tree[2] + ")"
        return query.format(input_table='{schema_' + self.json['input'][0] + '}.{' + self.json['input'][0] + '}',
                            parent_column=tree[1].value,
                            error_table='{schema_error_' + self.json['input'][0] +
                                        '}.{error_' + self.json['input'][0] + '}')

    def mandatory_column(self, tree):
        query = "insert into {error_table} (select *, '{column} value is mandatory' as dmx_message " \
                "from {input_table} where {column} is null)"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            column=self.json['column'],
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}')

    def compare_numeric(self, tree):
        query = "insert into {error_table} (select *, '{column} comparison failed with " \
                "{other_column}' as dmx_message from {input_table} " \
                "where not ({input_table}.{column} {operator} {other_input}.{other_column}))"
        return query.format(input_table='{schema_' + self.json['input'][0] + '}.{' + self.json['input'][0] + '}',
                            column=self.json['columns'][0],
                            operator=self.json['operator'],
                            other_input='{schema_' + self.json['input'][1] + '}.{' + self.json['input'][1] + '}',
                            other_column=self.json['columns'][1],
                            error_table='{schema_error_' + self.json['input'][0] +
                                        '}.{error_' + self.json['input'][0] + '}')

    def range_numeric(self, tree):
        query = "insert into {error_table} (select *, '{column} not in range' from {input_table} " \
                "where not ({input_table}.{column} between {value_1} and {value_2}))"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            column=self.json['column'],
                            value_1=self.json['values'][0], value_2=self.json['values'][1],
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}')

    def compare_date(self, tree):
        query = "insert into {error_table} (select *, 'The value of {column_1} column is " \
                " not {operator} {column_2} value' from {input_table} " \
                "where {input_table}.{column_1} ~ '{pattern_1}' and {other_input}.{column_2} ~ '{pattern_2}' " \
                "and not (to_date({input_table}.{column_1},'{format_1}') {operator} " \
                "to_date({other_input}.{column_2},'{format_2}')))"
        try:
            pattern_1 = self.date_patterns[self.json['formats'][0]]
            pattern_2 = self.date_patterns[self.json['formats'][1]]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSDateFormatUnsupported
        return query.format(input_table='{schema_' + self.json['input'][0] + '}.{' + self.json['input'][0] + '}',
                            operator=self.json['operator'],
                            other_input='{schema_' + self.json['input'][1] + '}.{' + self.json['input'][1] + '}',
                            column_1=self.json['columns'][0], column_2=self.json['columns'][1],
                            format_1=self.json['formats'][0], format_2=self.json['formats'][1],
                            pattern_1=pattern_1, pattern_2=pattern_2,
                            error_table='{schema_error_' + self.json['input'][0] +
                                        '}.{error_' + self.json['input'][0] + '}')

    def range_date(self, tree):
        query = "insert into {error_table} (select *, 'The values of a date column is not in the " \
                "given date range' from {input_table} where  {colname} ~ '{pattern}' and not " \
                "( to_date({colname},'{dateformat}') " \
                "between to_date('{startdateval}','{dateformat}') and to_date('{enddateval}','{dateformat}')))"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            colname=self.json['column'], dateformat=self.json['format'],
                            pattern=self.date_patterns[self.json['format']],
                            startdateval=self.json['values'][0], enddateval=self.json['values'][1],
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}')

    def composite_key(self, tree):
        query = "insert into {error_table} (select *, 'unique constraint violated' from {input_table} cd " \
                "where exists " \
                "(select 'x' from (select {colnames}, count(0) from {input_table} group by " \
                "{colnames} having count(0) > 1) " \
                "cc  where {colnames_expr}))"
        return query.format(input_table='{schema_' + self.json['input'] + '}.{' + self.json['input'] + '}',
                            error_table='{schema_error_' + self.json['input'] + '}.{error_' + self.json['input'] + '}',
                            colnames=','.join(self.json['columns']),
                            colnames_expr=' and '.join(map(lambda x: 'cc.' + x + ' = cd.' + x, self.json['columns'])))

    def cross_ref_exprs(self, tree):
        return ' and '.join(tree)

    def table_column(self, tree):
        return "(x.{parent_column} in (select A.{parent_column} from {input_table} A left join " + \
               tree[0].value + "." + tree[1].value + " " \
               "B on A.{parent_column} = B." + tree[2].value + " where B." + tree[2] + " is null))"

    def table_column_filter(self, tree):
        return "(x.{parent_column} in (select A.{parent_column} from {input_table} A left join " + \
               tree[0].value + "." + tree[1].value + \
               "B on A.{parent_column} = B." + tree[2].value + " where B." + tree[2] + " is null))"

    def or_rule(self, tree):
        return '(' + ') or ('.join(tree) + ')'

    def and_rule(self, tree):
        return '(' + ') and ('.join(tree) + ')'

    def condition(self, tree):
        return tree[0].value.strip() + ' ' + tree[1].value.strip() + ' ' + tree[2].value.strip()


def to_json(tree, datasets: Datasets):
    json = TreeToJson(datasets).transform(tree)
    query = TreeToPostgreSQL(json).transform(tree)
    parameters = re.findall(r'(\$[\w]+)', query)
    output = json['input'] if isinstance(json['input'], str) else json['input'][0]
    json.update({'query': query, 'parameters': parameters, 'output': output})
    return json
