#   datamax/api/utils/brms_validation_grammar.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import logging

from flashtext import KeywordProcessor
from lark import Lark, Tree

from entity.datasets import Datasets

log = logging.getLogger(__name__)

parser = Lark("""
        start: "data type of"i TABLE "." COLUMN "is integer"i -> data_type_integer
            | "data type of"i TABLE "." COLUMN "is float"i -> data_type_float
            | "data type of"i TABLE "." COLUMN "is date with format of"i FORMAT -> data_type_date
            | "allowed values in"i TABLE "." COLUMN "are alphanumeric"i -> alphanumeric_only
            | "allowed values in"i TABLE "." COLUMN "are email"i -> email_only
            | "allowed values in"i TABLE "." COLUMN "are alpha"i -> alpha_only
            | "allowed values in"i TABLE "." COLUMN "are upper case"i -> uppercase_only
            | "allowed values in"i TABLE "." COLUMN "are"i values -> allowed_values
            | "referenced values of"i TABLE "." COLUMN "are in"i TABLE "." COLUMN -> referenced
            | "referenced values of"i TABLE "." COLUMN "are in existing"i cross_ref_exprs  -> referenced_parameter
            | "mandatory column"i TABLE "."  COLUMN -> mandatory_column
            | "unique values in"i TABLE "." COLUMN -> unique_values
            | "unique values in"i TABLE " " columns -> composite_key
            | "length of values in"i TABLE "." COLUMN OPERATOR INT -> column_length
            | "compare date in"i TABLE "." COLUMN OPERATOR TABLE "." COLUMN "with format of"i FORMAT "," FORMAT -> compare_date
            | "compare numeric in"i TABLE "." COLUMN OPERATOR TABLE "." COLUMN -> compare_numeric
            | "range of date in"i TABLE "." COLUMN "is between"i BASIC_VALUE "and"i BASIC_VALUE "with format of"i FORMAT -> range_date
            | "range of numeric in"i TABLE "." COLUMN "is between"i NUMERIC_VALUE "and"i NUMERIC_VALUE -> range_numeric

        columns: (COLUMN ","?)+
        values: (VALUE ","?)+
        
        cross_ref_exprs: cross_ref_expr+
        cross_ref_expr: (PARAMETER "." PARAMETER "." COLUMN ","?) -> table_column
                    | (PARAMETER "." PARAMETER "." COLUMN "where" condition) -> table_column_filter
        
        condition:  "(" COLUMN OPERATOR VALUE ")" ","?
                | "({" FORMULA "}" OPERATOR VALUE ")" ","?
                | "or("i condition+  ")" ","? -> or_rule
                | "and("i condition+ ")" ","? -> and_rule
                

        TABLE: /[0-9a-zA-Z_-//]+/
        COLUMN: /[0-9a-zA-Z#_*//-//]+/
        FORMAT: /[0-9a-zA-Z_\-\/]+/
        FORMULA: /[0-9a-zA-Z#_[\]\*//(//)//+//-\/'\.//\$ ]+/
        OPERATOR: /([<>=!]+)|(is (not)?)/
        BASIC_OPERATOR: /[<>=]+/
        VALUE: /[0-9a-zA-Z_'$\/ ]+/
        BASIC_VALUE: /[0-9a-zA-Z_'$\/]+/
        NUMERIC_VALUE: /[0-9a-zA-Z_'$]+/
        PARAMETER: INT | /[0-9a-zA-Z_#$]+/

        %import common.LETTER
        %import common.INT
        %import common.WS
        %ignore WS
    """)

keywords = {
}

keyword_processor = KeywordProcessor()
keyword_processor.add_keywords_from_dict(keywords)


def case_treatment(tree: Tree, datasets: Datasets, dataset_name=""):
    for child in tree.children:
        if isinstance(child, Tree):
            case_treatment(child, datasets, dataset_name)
        else:
            if child.type == 'TABLE':
                dataset_name = datasets.resolve_table_name(child.value)
                child.value = dataset_name
            if child.type == 'COLUMN':
                child.value = datasets.resolve_column_name(dataset_name, child.value)
    return tree


def tokenize(text, datasets: Datasets):
    text = keyword_processor.replace_keywords(text)
    tokens = parser.parse(text)
    tokens = case_treatment(tokens, datasets)
    log.debug(tokens)
    return tokens
