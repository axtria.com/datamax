from datetime import datetime
from os import path

import matplotlib.pyplot as plt
import pandas as pd
import seaborn

from vendor.redshift import Redshift


def generate_histogram(redshift: Redshift, table_schema: str, table_name: str, columns: list):
    histograms = []

    for column in columns:
        query = """select {col}, count({col}) as frequency from {schema}.{table} group by {col} order by {col}""".format(
            col=column, schema=table_schema, table=table_name)
        df = redshift.execute(query)

        dirname = 'tmp/plot/'
        fname = table_schema + '_' + table_name + '_' + column + '_histogram__' + datetime.now().strftime(
            "%m_%d_%Y__%H_%M_%S_%f") + '.png'
        fpath = path.abspath(dirname + fname)

        plt.figure()
        plot = seaborn.barplot(x=df['frequency'], y=df[column], orient='h', color='blue').get_figure()
        plot.savefig(fpath, dpi=600, bbox_inches='tight')

        histograms.append((column, __safe_html(
            '<a href="' + fpath + '" target="_blank"><img src="' + fpath + '" height=60 width=150 /></a>')))

    df = pd.DataFrame(histograms, columns=['column', 'histogram']).set_index('column')
    del df.index.name

    return df


def __safe_html(s: str):
    return s.replace('<', '__LeftAngleBracket__').replace('>', '__RightAngleBracket__')
