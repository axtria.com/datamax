#   datamax/api/views/validation.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from flask import render_template, Blueprint

views_validation = Blueprint('views_validation', __name__)


@views_validation.route('login')
def login():
    return render_template('validation/login.html')


@views_validation.route('index')
def index():
    """List of all the files"""
    return render_template('validation/index.html')


@views_validation.route('add_s3')
def file_add_s3():
    return render_template('validation/add_s3.html')


@views_validation.route('add_redshift')
def file_add_redshift():
    return render_template('validation/add_redshift.html')


@views_validation.route('list_config')
def list_config():
    return render_template('validation/list_config.html')


@views_validation.route('/add')
def add_config():
    return render_template('validation/add_config.html')


@views_validation.route('/edit')
def edit_config():
    return render_template('validation/edit_config.html')


@views_validation.route('/list_config_instance')
def list_config_instance():
    return render_template('validation/list_config_instance.html')
