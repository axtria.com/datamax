#   datamax/api/views/exchange.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from flask import Blueprint, render_template

views_exchange = Blueprint('views_exchange', __name__)


@views_exchange.route('/login')
def login():
    """

    Returns:

    """
    return render_template('api_exchange/login.html')


@views_exchange.route('/add')
def add_api():
    """API Exchange - add APIs into the system using the user interface.

    Returns:

    """
    return render_template('api_exchange/add.html')


@views_exchange.route('/published')
def published_api():
    """

    Returns:

    """
    return render_template('api_exchange/published.html')


@views_exchange.route('/all')
def all_api():
    """

    Returns:

    """
    return render_template('api_exchange/all.html')


@views_exchange.route('/edit')
def edit_api():
    """

    Returns:

    """
    return render_template('api_exchange/edit.html')


@views_exchange.route('/copy')
def copy_api():
    """

    Returns:

    """
    return render_template('api_exchange/copy.html')


@views_exchange.route('/category')
def category_api():
    """

    Returns:

    """
    return render_template('api_exchange/category.html')


@views_exchange.route('/summary')
def summary_api():
    """

    Returns:

    """
    return render_template('api_exchange/summary.html')
