#   datamax/api/entity/workflow.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from uuid import uuid4

from entity import Entity
from entity.datasets import Datasets
from entity.parameters import Parameters
from entity.rules import Rules
from entity.selected_datasets import SelectedDatasets
from entity.tags import Tags
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json
from utils.exceptions import BRMSDatasetInUse


class Workflow(Entity):
    def __init__(self):
        super().__init__()
        self.datasets = Datasets()
        self.parameters = Parameters()
        self.rules = Rules()
        self.tags = Tags()
        self.selected_datasets = SelectedDatasets()
        self.workflow_id = uuid4().hex
        self.name = ""
        self.description = ""
        self.workspace_id = 1  # default workspace
        self.status = "DRAFT"  # default status
        self.health = True  # default status

    @classmethod
    def initialize(cls, workflow_meta: dict, *, merging_strategy: str = '_'):
        workflow = cls()
        workflow.workflow_id = workflow_meta['META']['workflow_id']
        workflow.name = workflow_meta['META']['name']
        workflow.description = workflow_meta['META']['description']
        workflow.workspace_id = int(workflow_meta['META']['workspace_id'])
        workflow.status = workflow_meta['META']['status']
        workflow.health = workflow_meta['META']['health']
        workflow.datasets.merging_strategy = merging_strategy

        for dataset_name, selected_dataset in workflow_meta['SELECTED_DATASETS'].items():
            workflow.selected_datasets.add_dataset(dataset_name, **selected_dataset)
        for dataset_name, data_dictionary in workflow_meta['DATASETS'].items():
            workflow.datasets.add_dataset(dataset_name, data_dictionary)
        for rule in workflow_meta['RULES']:
            workflow.add_rule(rule)
        for parameter_name, parameter_definition in workflow_meta['PARAMETERS'].items():
            workflow.add_parameter_definition(parameter_name, parameter_definition)

        return workflow

    @property
    def serialize(self):
        return {'META': {'workflow_id': self.workflow_id,
                         'name': self.name,
                         'description': self.description,
                         'workspace_id': self.workspace_id,
                         'status': self.status,
                         'health': self.health},
                'DATASETS': self.datasets.serialize,
                'SELECTED_DATASETS': self.selected_datasets.serialize,
                'PARAMETERS': self.parameters.serialize,
                'RULES': self.rules.serialize
                }

    def add_dataset(self, name: str, data_dictionary: list):
        self.datasets.add_dataset(name, data_dictionary)

    def remove_dataset(self, rule_id: str):
        rule = self.rules.get_rule(rule_id)
        self.datasets.remove_dataset(rule.output)

    def __refresh_dataset(self, dataset_name: str):
        # scan the usage and rebuild the output metadata.
        refreshed_metadata = None
        for rule in self.rules:
            if rule.output == dataset_name:
                refreshed_metadata = rule.output_metadata
        self.datasets.remove_dataset(dataset_name)
        if refreshed_metadata:
            self.datasets.add_dataset(dataset_name, refreshed_metadata)

    def get_dataset_names(self) -> list:
        return self.datasets.get_names()

    def get_dataset(self, rule_id: str) -> str:
        rule = self.rules.get_rule(rule_id)
        return rule.output

    def add_selected_dataset(self, name: str, data_dictionary: list, schema: str):
        # to add workflow meta
        self.selected_datasets.add_dataset(name, data_dictionary, schema)
        self.datasets.add_dataset(name, data_dictionary)

    def truncate_selected_dataset(self):
        for selected_dataset in self.selected_datasets:
            self.datasets.remove_dataset(selected_dataset)
        self.selected_datasets.truncate()

    def add_parameter_definition(self, name: str, definition: dict):
        self.parameters.add_definition(name, definition)

    def add_rule(self, rule_definition: dict):
        self.rules.add_rule(rule_definition)
        for parameter in rule_definition.get('parameters', []):
            self.parameters.add_parameter(parameter)

    def update_rule(self, rule_id: str, rule_definition: dict):
        old_rule = self.rules.get_rule(rule_id)
        for parameter in old_rule.parameters:
            parameter_in_use = False
            for rule in self.rules:
                if ((parameter in rule.parameters) & (rule_id != rule.rule_id)) | (
                        parameter in rule_definition['parameters']):
                    # parameter already exist in other rule so no need to remove it.
                    # OR parameter already exists in new rule definition with same name.
                    parameter_in_use = True
                    self.log.debug("parameter found to be in use")
                    break
            if not parameter_in_use:
                self.parameters.remove_parameter(parameter)

        self.rules.update_rule(rule_id, rule_definition)
        self.log.debug("updated rule {} with definition".format(rule_id))
        for parameter in rule_definition.get('parameters', []):
            self.parameters.add_parameter(parameter)

    def delete_rule(self, rule_id: str):
        old_rule = self.rules.get_rule(rule_id)
        # raise exception if the output of old rule is in use by future rules
        for rule in self.rules.get_rules_after(rule_id):
            inputs = rule.input if isinstance(rule.input, list) else [rule.input]
            if (old_rule.output in inputs) & (rule_id != rule.rule_id):
                raise BRMSDatasetInUse

        for parameter in old_rule.parameters:
            parameter_in_use = False
            for rule in self.rules:
                if (parameter in rule.parameters) & (rule_id != rule.rule_id):
                    # parameter already exist in other rule so no need to remove it.
                    parameter_in_use = True
                    break
            if not parameter_in_use:
                self.parameters.remove_parameter(parameter)

        self.rules.remove_rule(rule_id)
        self.__refresh_dataset(old_rule.output)  # scan the previous usage and rebuild the output metadata.

    def delete_rule_silently(self, rule_id: str):
        old_rule = self.rules.get_rule(rule_id)

        for parameter in old_rule.parameters:
            parameter_in_use = False
            for rule in self.rules:
                if (parameter in rule.parameters) & (rule_id != rule.rule_id):
                    # parameter already exist in other rule so no need to remove it.
                    parameter_in_use = True
                    break
            if not parameter_in_use:
                self.parameters.remove_parameter(parameter)

        self.rules.remove_rule(rule_id)
        self.__refresh_dataset(old_rule.output)  # scan the previous usage and rebuild the output metadata.

    def lock(self, rule_id: str):
        self.log.debug("applying lock against rule id %s" % rule_id)
        # reinitialize datasets
        self.datasets.truncate()
        for selected_dataset in self.selected_datasets:
            self.datasets.add_dataset(selected_dataset, self.selected_datasets.get_dataset(selected_dataset))
        # update metadata for each rule prior to lock
        for rule in self.rules:
            if rule.rule_id == rule_id:
                return
            self.datasets.add_dataset(rule.output, rule.output_metadata)

    def unlock(self, rule_id: str):
        self.log.debug("applying unlock to the rule id %s" % rule_id)
        locked_rule = self.rules.get_rule(rule_id)
        self.add_dataset(locked_rule.output, locked_rule.output_metadata)
        # unlock by re-entering all the rules
        for rule in self.rules.get_rules_after(rule_id):
            tree = tokenize(rule.user_input, self.datasets)
            json = to_json(tree, self.datasets)
            self.update_rule(rule.rule_id, {'rule_name': rule.rule_name,
                                            'user_input': rule.user_input,
                                            'output': rule.output,
                                            'rule_id': rule.rule_id,
                                            **json})
            self.add_dataset(rule.output, json['_output_metadata'])

    @property
    def visualize(self):
        nodes = []
        edges = []

        for dataset_name in self.datasets.get_names():
            nodes.append({'id': dataset_name, 'label': dataset_name, 'type': 'dataset'})
        for rule in self.rules:
            nodes.append({'id': rule.rule_id, 'label': rule.rule_name, 'type': rule.rule_type})
            if isinstance(rule.input, list):
                for each_input in rule.input:
                    edges.append({'from': each_input, 'to': rule.rule_id})
            else:
                edges.append({'from': rule.input, 'to': rule.rule_id})
            edges.append({'from': rule.rule_id, 'to': rule.output})

        return {'nodes': nodes, 'edges': edges}
