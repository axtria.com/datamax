#   datamax/api/entity/rules.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import copy
from uuid import uuid4

from entity import Entity
from utils.exceptions import BRMSRuleNotFound


class Rule(Entity):
    def __init__(self, rule_name, user_input, input, output, parameters, rule_type, _output_metadata, rule_id=None,
                 query=None, **kwargs):
        super().__init__()
        self.rule_id = uuid4().hex if rule_id is None else rule_id
        self.rule_name = rule_name
        self.rule_type = rule_type
        self.input = input
        self.user_input = user_input
        self.output = output
        self.query = query
        self.parameters = parameters
        self.output_metadata = _output_metadata
        self.definition = kwargs

    def update(self, rule_name, input, user_input, output, parameters, rule_type, _output_metadata,
               query=None, **kwargs):
        self.input = input
        self.rule_name = rule_name
        self.user_input = user_input
        self.output = output
        self.query = query
        self.parameters = parameters
        self.rule_type = rule_type
        self.output_metadata = _output_metadata
        self.definition = kwargs

    @property
    def serialize(self):
        return {'rule_id': self.rule_id,
                'rule_name': self.rule_name,
                'rule_type': self.rule_type,
                'input': self.input,
                'user_input': self.user_input,
                'output': self.output,
                'query': self.query,
                'parameters': self.parameters,
                '_output_metadata': self.output_metadata,
                **self.definition}

    def copy(self):
        return copy.deepcopy(self)


class Rules(Entity):
    def __init__(self):
        super().__init__()
        self.__rules = []

    def add_rule(self, rule_definition: dict) -> str:
        rule = Rule(**rule_definition)
        self.__rules.append(rule)
        return rule.rule_id

    def add_rule_by_index(self, rule_definition: dict, index: int) -> str:
        rule = Rule(**rule_definition)
        self.__rules.insert(index, rule)
        return rule.rule_id

    def update_rule(self, rule_id: str, rule_definition: dict):
        rule_definition.update({'rule_id': rule_id})  # fail safe in case rule id is not present in definition.
        for index, rule in enumerate(self.__rules):
            if rule.rule_id == rule_id:
                rule.update(**rule_definition)
                return
        raise BRMSRuleNotFound

    def remove_rule(self, rule_id) -> None:
        for rule in self.__rules:
            if rule.rule_id == rule_id:
                self.__rules.remove(rule)
                return
        raise BRMSRuleNotFound

    def get_rule(self, rule_id) -> Rule:
        for rule in self.__rules:
            if rule.rule_id == rule_id:
                return rule
        raise BRMSRuleNotFound

    def get_rules_after(self, rule_id) -> list:
        encountered_rule = False
        for rule in self.__rules:
            if encountered_rule:
                yield rule
            if rule_id == rule.rule_id:
                encountered_rule = True

    @property
    def serialize(self):
        return [rule.serialize for rule in self.__rules]

    def __iter__(self):
        return self.__rules.__iter__()
