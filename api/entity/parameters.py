#   datamax/api/entity/parameters.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from entity import Entity
from utils.exceptions import BRMSParameterNotFound


class Parameter(Entity):
    def __init__(self, name):
        super().__init__()
        self.name = name
        self.help_text = ""
        self.description = ""
        self.config_type = ""
        self.config_value = ""

    def add_definition(self, help_text, description, config_type, config_value, **kwargs):
        self.help_text = help_text
        self.description = description
        self.config_type = config_type
        self.config_value = config_value

    @property
    def serialize(self):
        return {'name': self.name,
                'help_text': self.help_text,
                'description': self.description,
                'config_type': self.config_type,
                'config_value': self.config_value}


class Parameters(Entity):
    def __init__(self):
        super().__init__()
        self.__parameters = {}

    def add_parameter(self, name: str):
        if self.__parameters.get(name) is None:
            self.log.debug('Adding a new parameter - ' + name)
            self.__parameters[name] = Parameter(name)
        else:
            self.log.warning('Parameter already found, ignored addition ' + name)

    def remove_parameter(self, name: str):
        try:
            del self.__parameters[name]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSParameterNotFound

    def get_parameter(self, name: str) -> Parameter:
        try:
            return self.__parameters[name]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSParameterNotFound

    def add_definition(self, name: str, meta: dict):
        try:
            self.__parameters[name].add_definition(**meta)
        except KeyError as e:
            self.log.exception(e)
            raise BRMSParameterNotFound

    def truncate(self):
        self.__parameters = {}

    def get_queries(self):
        output = []
        query = "select distinct({column}) as {column} from {schema}.{table} where {column} is not null"
        for name, parameter in self.__parameters.items():
            if parameter.config_type == 'Table column':
                schema, table, column = parameter.config_value.split(',')
                output.append((name, query.format(column=column, schema=schema, table=table)))
        return output

    @property
    def serialize(self):
        return {key: value.serialize for key, value in self.__parameters.items()}
