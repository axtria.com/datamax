#   datamax/api/entity/selected_datasets.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from entity import Entity
from utils.exceptions import BRMSDatasetNotFound


class SelectedDatasets(Entity):
    def __init__(self):
        super().__init__()
        self.__datasets = {}

    def add_dataset(self, name, meta: list, schema: str):
        self.__datasets[name] = {"meta": meta, "schema": schema}

    def remove_dataset(self, name):
        try:
            del self.__datasets[name]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSDatasetNotFound

    def get_dataset(self, name):
        try:
            return self.__datasets[name]["meta"]  # return only meta to others
        except KeyError as e:
            self.log.exception(e)
            raise BRMSDatasetNotFound

    def get_schema(self, name):
        try:
            return self.__datasets[name]["schema"]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSDatasetNotFound

    def truncate(self):
        self.__datasets = {}

    def __iter__(self):
        return self.__datasets.__iter__()

    @property
    def serialize(self):
        return self.__datasets
