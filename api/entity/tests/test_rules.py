#   datamax/api/entity/tests/test_rules.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.rules import Rules
from utils.exceptions import BRMSRuleNotFound

rule_with_parameter = {'user_input': 'filter ds1 where (sales > $threshold)',
                       'query': 'select * from ds1 where sales > $threshold',
                       'output': 'rs1',
                       'rule_name': 'active sales data',
                       'filter': {},
                       'input': 'ds1',
                       'parameters': ['$threshold'],
                       'rule_type': 'filter',
                       '_output_metadata': []
                       }

rule_without_parameter = {'user_input': 'filter ds1 where (sales > $threshold)',
                          'query': 'select * from ds1 where sales > $threshold',
                          'output': 'rs1',
                          'rule_name': 'active sales data',
                          'filter': {},
                          'input': 'ds1',
                          'parameters': [],
                          'rule_type': 'filter',
                          '_output_metadata': []
                          }


def test_empty_rules():
    rules = Rules()
    assert len(rules.serialize) == 0


def test_add_rule():
    rules = Rules()
    rules.add_rule(rule_with_parameter)
    assert len(rules.serialize) == 1


def test_remove_rule():
    rules = Rules()
    rule_id = rules.add_rule(rule_with_parameter)
    rules.remove_rule(rule_id)
    assert len(rules.serialize) == 0


def test_get_rule_with_parameter():
    rules = Rules()
    to_add = rule_with_parameter.copy()
    rule_id = rules.add_rule(to_add)
    to_add['rule_id'] = rule_id
    assert rules.get_rule(rule_id).serialize == to_add


def test_get_rule_without_parameter():
    rules = Rules()
    to_add = rule_without_parameter.copy()
    rule_id = rules.add_rule(to_add)
    to_add['rule_id'] = rule_id
    assert rules.get_rule(rule_id).serialize == to_add


def test_remove_undefined_rule():
    rules = Rules()
    with pytest.raises(BRMSRuleNotFound):
        rules.remove_rule('1')


def test_get_undefined_rule():
    rules = Rules()
    with pytest.raises(BRMSRuleNotFound):
        rules.get_rule('1')


def test_update_rule():
    rules = Rules()
    rule_id = rules.add_rule(rule_with_parameter)
    rules.update_rule(rule_id, rule_with_parameter)
    assert rules.get_rule(rule_id).serialize == {'rule_id': rule_id, **rule_with_parameter}


def test_add_rule_without_id():
    rules = Rules()
    rule_id = rules.add_rule(rule_with_parameter)
    assert rule_id is not None


def test_update_rule_should_not_change_sequence():
    rules = Rules()
    first_rule = rules.add_rule(rule_with_parameter)
    second_rule = rules.add_rule(rule_without_parameter)
    rules.update_rule(first_rule, rule_without_parameter)
    assert first_rule == rules.serialize[0]['rule_id']
    assert second_rule == rules.serialize[1]['rule_id']
