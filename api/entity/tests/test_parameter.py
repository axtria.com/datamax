#   datamax/api/entity/tests/test_parameter.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.parameters import Parameters
from utils.exceptions import BRMSParameterNotFound


def test_empty_parameters():
    parameters = Parameters()
    assert isinstance(parameters, Parameters)
    assert len(parameters.serialize) == 0


def test_add_parameter():
    parameters = Parameters()
    parameters.add_parameter('$product')
    assert len(parameters.serialize) == 1


def test_add_definition():
    parameters = Parameters()
    parameters.add_parameter('$product')
    parameters.add_definition('$product', {'help_text': 'product name',
                                           'description': 'picklist item',
                                           'config_type': 'picklist',
                                           'config_value': 'q1,q2'})
    product = parameters.get_parameter('$product')
    assert product.help_text == 'product name'
    assert product.description == 'picklist item'
    assert product.config_type == 'picklist'
    assert product.config_value == 'q1,q2'


def test_add_same_parameter_again_should_not_change_definition():
    parameters = Parameters()
    parameters.add_parameter('$product')
    parameters.add_definition('$product', {'help_text': 'product name',
                                           'description': 'picklist item',
                                           'config_type': 'picklist',
                                           'config_value': 'q1,q2'})
    parameters.add_parameter('$product')
    product = parameters.get_parameter('$product')
    assert len(parameters.serialize) == 1
    assert product.help_text == 'product name'
    assert product.description == 'picklist item'
    assert product.config_type == 'picklist'
    assert product.config_value == 'q1,q2'


def test_serialize():
    parameters = Parameters()
    parameters.add_parameter('$threshold')
    parameters.add_parameter('$product')
    actual = parameters.serialize
    expected = {'$threshold': {'name': '$threshold', 'help_text': "", 'description': "",
                               "config_type": "", "config_value": ""},
                '$product': {'name': '$product', 'help_text': "", 'description': "",
                             "config_type": "", "config_value": ""}}
    assert actual == expected


def test_get_parameter_from_empty_list_should_raise():
    parameters = Parameters()
    with pytest.raises(BRMSParameterNotFound):
        parameters.get_parameter('$product')


def test_add_definition_in_empty_list_should_raise():
    parameters = Parameters()
    with pytest.raises(BRMSParameterNotFound):
        parameters.add_definition('$product', {})


def test_truncate_method():
    parameters = Parameters()
    parameters.add_parameter('$product')
    parameters.add_parameter('$threshold')
    parameters.truncate()
    assert len(parameters.serialize) == 0


def test_get_queries():
    parameters = Parameters()
    parameters.add_parameter('$product')
    parameters.add_definition('$product', {'config_type': 'Table column', 'config_value': 's1,t1,c1',
                                           'help_text': '', 'description': ''})
    actual = parameters.get_queries()
    expected = [('$product', 'select distinct(c1) as c1 from s1.t1 where c1 is not null')]
    assert actual == expected


def test_get_queries_for_multi_params():
    parameters = Parameters()
    parameters.add_parameter('$threshold')
    parameters.add_parameter('$product')
    parameters.add_definition('$product', {'config_type': 'Table column', 'config_value': 's1,t1,c1',
                                           'help_text': '', 'description': ''})
    actual = parameters.get_queries()
    expected = [('$product', 'select distinct(c1) as c1 from s1.t1 where c1 is not null')]
    assert actual == expected


def test_get_queries_for_multi_table_columns():
    parameters = Parameters()
    parameters.add_parameter('$product')
    parameters.add_definition('$product', {'config_type': 'Table column', 'config_value': 's1,t1,c1',
                                           'help_text': '', 'description': ''})
    parameters.add_parameter('$product2')
    parameters.add_definition('$product2', {'config_type': 'Table column', 'config_value': 's2,t2,c2',
                                            'help_text': '', 'description': ''})
    actual = parameters.get_queries()
    expected = [('$product', 'select distinct(c1) as c1 from s1.t1 where c1 is not null'),
                ('$product2', 'select distinct(c2) as c2 from s2.t2 where c2 is not null')]
    assert actual == expected
