#   datamax/api/entity/tests/test_selected_datasets.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.selected_datasets import SelectedDatasets
from utils.exceptions import BRMSDatasetNotFound


@pytest.fixture
def sd():
    _sd = SelectedDatasets()
    _sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                            {'column_name': 'c2', 'data_type': 'numeric'}], "brms_instance")
    return _sd


def test_empty_selected_datasets():
    sd = SelectedDatasets()
    assert len(sd.serialize) == 0


def test_add_selected_dataset(sd):
    assert len(sd.serialize) == 1


def test_remove_selected_dataset(sd):
    sd.remove_dataset('ds1')
    assert len(sd.serialize) == 0


def test_get_selected_dataset():
    sd = SelectedDatasets()
    sd.add_dataset('ds1', [], None)
    assert sd.get_dataset('ds1') is not None


def test_get_dataset_from_empty_raises_error():
    sd = SelectedDatasets()
    with pytest.raises(BRMSDatasetNotFound):
        sd.get_dataset('ds1')


def test_delete_dataset_from_empty_raises_error():
    sd = SelectedDatasets()
    with pytest.raises(BRMSDatasetNotFound):
        sd.remove_dataset('ds1')


def test_truncate_datasets():
    sd = SelectedDatasets()
    sd.add_dataset('ds1', [], None)
    sd.add_dataset('ds2', [], None)
    sd.truncate()
    assert len(sd.serialize) == 0


def test_schema(sd):
    actual = sd.get_schema('ds1')
    expected = 'brms_instance'
    assert actual == expected