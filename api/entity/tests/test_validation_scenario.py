#   datamax/api/entity/tests/test_validation_scenario.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.scenario import ValidationScenario
from entity.workflow import Workflow


@pytest.fixture
def ad_scenario():
    db_workflow = {
        "META": {"workflow_id": "36f1a0cf9a7a4060b61f833e946fae6e",
                 "name": "hello",
                 "description": "h",
                 "workspace_id": 1,
                 "status": "DRAFT",
                 "health": True
                 },
        "DATASETS": {"ds1": [
            {
                "column_name": "c1",
                "data_type": "varchar"
            },
            {
                "column_name": "c2",
                "data_type": "varchar"
            },
            {
                "column_name": "c3",
                "data_type": "numeric"
            },
            {
                "column_name": "c4",
                "data_type": "varchar"
            }
        ]
        },
        "SELECTED_DATASETS": {
            'ds1': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                             {'column_name': 'c2', 'data_type': 'varchar'},
                             {'column_name': 'c3', 'data_type': 'numeric'},
                             {'column_name': 'c4', 'data_type': 'varchar'}],
                    "schema": ""}
        },
        "PARAMETERS": {},
        "RULES": [
            {
                "rule_id": "e6a6458c801e4612bf9ce92d825647c7",
                "rule_name": "r1",
                "input": "ds1",
                "user_input": "data type of ds1.c1 is integer",
                "output": "ds1",
                "query": "insert into {schema_error_ds1}.{error_ds1} (select * , 'c1 value is not integer' as "
                         "dmx_message from {schema_ds1}.{ds1} where cast(c1 as varchar) !~ '^(-)?[0-9]+$' )",
                "parameters": [],
                "rule_type": "data_type_integer",
                "column": "c1",
                "_output_metadata": [
                    {
                        "column_name": "c1",
                        "data_type": "varchar"
                    },
                    {
                        "column_name": "c2",
                        "data_type": "varchar"
                    },
                    {
                        "column_name": "c3",
                        "data_type": "numeric"
                    },
                    {
                        "column_name": "c4",
                        "data_type": "varchar"
                    }
                ]
            },
            {
                "rule_id": "75a74e2d042d436bbe1c6f574a85174a",
                "rule_name": "r1",
                "input": "ds1",
                "user_input": "data type of ds1.c3 is integer",
                "output": "ds1",
                "query": "insert into {schema_error_ds1}.{error_ds1} (select * , 'c3 value is not integer' as "
                         "dmx_message from {schema_ds1}.{ds1} where cast(c3 as varchar) !~ '^(-)?[0-9]+$' )",
                "parameters": [],
                "rule_type": "data_type_integer",
                "column": "c3",
                "_output_metadata": [
                    {
                        "column_name": "c1",
                        "data_type": "varchar"
                    },
                    {
                        "column_name": "c2",
                        "data_type": "varchar"
                    },
                    {
                        "column_name": "c3",
                        "data_type": "numeric"
                    },
                    {
                        "column_name": "c4",
                        "data_type": "varchar"
                    }
                ]
            }
        ]
    }
    parameters = {"$threshold": "0.5", "$quarter": "Q2", "$quarter_1": "Q3"}
    return ValidationScenario.initialize_with_adaptor('brms_instance', Workflow.initialize(db_workflow),
                                                      input_adaptors={"ds1": "actual_ds1"}, parameters=parameters,
                                                      input_adaptor_columns={"ds1": ["c1", "c3"]})


@pytest.fixture
def ds_scenario():
    db_workflow = {
        "META": {"workflow_id": "36f1a0cf9a7a4060b61f833e946fae6e",
                 "name": "hello",
                 "description": "h",
                 "workspace_id": 1,
                 "status": "DRAFT",
                 "health": True
                 },
        "DATASETS": {"ds1": [
            {
                "column_name": "c1",
                "data_type": "varchar"
            },
            {
                "column_name": "c2",
                "data_type": "varchar"
            },
            {
                "column_name": "c3",
                "data_type": "numeric"
            },
            {
                "column_name": "c4",
                "data_type": "varchar"
            }
        ]
        },
        "SELECTED_DATASETS": {
            'ds1': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                             {'column_name': 'c2', 'data_type': 'varchar'},
                             {'column_name': 'c3', 'data_type': 'numeric'},
                             {'column_name': 'c4', 'data_type': 'varchar'}],
                    "schema": "brms_instance"}
        },
        "PARAMETERS": {},
        "RULES": [
            {
                "rule_id": "e6a6458c801e4612bf9ce92d825647c7",
                "rule_name": "r1",
                "input": "ds1",
                "user_input": "data type of ds1.c1 is integer",
                "output": "ds1",
                "query": "insert into {schema_error_ds1}.{error_ds1} (select * , 'c1 value is not integer' as "
                         "dmx_message from {schema_ds1}.{ds1} where cast(c1 as varchar) !~ '^(-)?[0-9]+$' )",
                "parameters": [],
                "rule_type": "data_type_integer",
                "column": "c1",
                "_output_metadata": [
                    {
                        "column_name": "c1",
                        "data_type": "varchar"
                    },
                    {
                        "column_name": "c2",
                        "data_type": "varchar"
                    },
                    {
                        "column_name": "c3",
                        "data_type": "numeric"
                    },
                    {
                        "column_name": "c4",
                        "data_type": "varchar"
                    }
                ]
            },
            {
                "rule_id": "75a74e2d042d436bbe1c6f574a85174a",
                "rule_name": "r1",
                "input": "ds1",
                "user_input": "data type of ds1.c3 is integer",
                "output": "ds1",
                "query": "insert into {schema_error_ds1}.{error_ds1} (select * , 'c3 value is not integer' as "
                         "dmx_message from {schema_ds1}.{ds1} where cast(c3 as varchar) !~ '^(-)?[0-9]+$' )",
                "parameters": [],
                "rule_type": "data_type_integer",
                "column": "c3",
                "_output_metadata": [
                    {
                        "column_name": "c1",
                        "data_type": "varchar"
                    },
                    {
                        "column_name": "c2",
                        "data_type": "varchar"
                    },
                    {
                        "column_name": "c3",
                        "data_type": "numeric"
                    },
                    {
                        "column_name": "c4",
                        "data_type": "varchar"
                    }
                ]
            }
        ]
    }
    scenario_meta = {"parameters": [{"name": "$threshold", "value": "0.5"}, {"name": "$quarter", "value": "Q2"},
                                    {"name": "$quarter_1", "value": "Q3"}]}
    return ValidationScenario.initialize_with_dataset(1, 's1', scenario_meta, True, Workflow.initialize(db_workflow),
                                                      execution_schema='brms_instance', output_schema='brms_output')


def test_scenario_id(ds_scenario):
    assert ds_scenario.scenario_id == '1'


def test_output_dataset(ds_scenario):
    assert ds_scenario.serialize['OUTPUT']['success_ds1'] == 'brms_output.success_ds1_1'
    assert ds_scenario.serialize['OUTPUT']['error_ds1'] == 'brms_output.error_ds1_1'


def test_input_dataset(ds_scenario):
    assert ds_scenario.serialize['OUTPUT']['ds1'] == 'brms_instance.ds1'


def test_prepare_query_to_adds_to_serialize(ds_scenario):
    ds_scenario.prepare_query("insert into {schema_error_ds1}.{error_ds1} (select * , 'c1 value is not integer' as "
                              "dmx_message from {schema_ds1}.{ds1} where cast(c1 as varchar) !~ '^(-)?[0-9]+$' )")
    assert 'QUERY' in ds_scenario.serialize
    assert 'OUTPUT' in ds_scenario.serialize
    assert len(ds_scenario.serialize['QUERY']) == 1


def test_prepare_query_scenario(ds_scenario):
    actual = ds_scenario.prepare_query("insert into {schema_error_ds1}.{error_ds1} (select * , "
                                       "'c1 value is not integer' as "
                                       "dmx_message from {schema_ds1}.{ds1} where "
                                       "cast(c1 as varchar) !~ '^(-)?[0-9]+$')")
    expected = "insert into brms_output.error_ds1_{0} (select * , 'c1 value is not integer' as dmx_message " \
               "from brms_instance.ds1 where cast(c1 as varchar) !~ '^(-)?[0-9]+$')"
    assert actual == expected.format(ds_scenario.scenario_id)


def test_create_success_table_query(ds_scenario):
    actual = ds_scenario.prepare_query(ds_scenario.success_tables_queries[1])
    expected = "create table brms_output.success_ds1_{0} as (select c1, c2, c3, c4 from brms_instance.ds1 " \
               "except select c1, c2, c3, c4 from brms_output.error_ds1_{0})"
    assert len(ds_scenario.success_tables_queries) == 2
    assert actual == expected.format(ds_scenario.scenario_id)


def test_create_error_table_query(ds_scenario):
    actual = ds_scenario.prepare_query(ds_scenario.error_tables_queries[1])
    expected = "create table brms_output.error_ds1_{0} (like brms_instance.ds1 including defaults, " \
               "dmx_message varchar)"
    assert len(ds_scenario.error_tables_queries) == 2
    assert actual == expected.format(ds_scenario.scenario_id)


def test_invalid_meta_table_query(ds_scenario):
    name, total_count_query, error_count_query = ds_scenario.invalid_meta_queries[0]
    actual = ds_scenario.prepare_query(total_count_query)
    expected = "select count(*) as total_count from brms_instance.ds1"
    assert actual == expected
    actual = ds_scenario.prepare_query(error_count_query)
    expected = "select count(*) from (select distinct c1, c2, c3, c4 from brms_output.error_ds1_{0}) as t"
    assert actual == expected.format(ds_scenario.scenario_id)


def test_scenario_id_with_adaptor(ad_scenario):
    assert ad_scenario.scenario_id is not None


def test_output_adaptor(ad_scenario):
    assert ad_scenario.serialize['OUTPUT']['success_ds1'] == "brms_instance.success_actua" \
                                                             "l_ds1_{0}".format(ad_scenario.scenario_id)
    assert ad_scenario.serialize['OUTPUT']['error_ds1'] == "brms_instance.error_actua" \
                                                             "l_ds1_{0}".format(ad_scenario.scenario_id)


def test_input_adaptor(ad_scenario):
    assert ad_scenario.serialize['OUTPUT']['ds1'] == 'brms_instance.actual_ds1'


def test_prepare_query_to_adds_to_serialize_for_adaptor(ad_scenario):
    ad_scenario.prepare_query("insert into {schema_error_ds1}.{error_ds1} (select * , 'c1 value is not integer' as "
                              "dmx_message from {schema_ds1}.{ds1} where cast(c1 as varchar) !~ '^(-)?[0-9]+$' )")
    assert 'QUERY' in ad_scenario.serialize
    assert 'OUTPUT' in ad_scenario.serialize
    assert len(ad_scenario.serialize['QUERY']) == 1


def test_prepare_query_scenario_for_adaptor(ad_scenario):
    actual = ad_scenario.prepare_query("insert into {schema_error_ds1}.{error_ds1} (select * , "
                                       "'c1 value is not integer' as "
                                       "dmx_message from {schema_ds1}.{ds1} where "
                                       "cast(c1 as varchar) !~ '^(-)?[0-9]+$')")
    expected = "insert into brms_instance.error_actual_ds1_{0} (select * , 'c1 value is not integer' as dmx_message " \
               "from brms_instance.actual_ds1 where cast(c1 as varchar) !~ '^(-)?[0-9]+$')"
    assert actual == expected.format(ad_scenario.scenario_id)


def test_create_success_table_query_for_adaptor(ad_scenario):
    actual = ad_scenario.prepare_query(ad_scenario.success_tables_queries[1])
    expected = "create table brms_instance.success_actual_ds1_{0} as (select c1, c3 from " \
               "brms_instance.actual_ds1 except select c1, c3 from brms_instance.error_actual_ds1_{0})"
    assert len(ad_scenario.success_tables_queries) == 2
    assert actual == expected.format(ad_scenario.scenario_id)


def test_create_error_table_query_for_adaptor(ad_scenario):
    actual = ad_scenario.prepare_query(ad_scenario.error_tables_queries[1])
    expected = "create table brms_instance.error_actual_ds1_{0} (like brms_instance.actual_ds1 including defaults, " \
               "dmx_message varchar)"
    assert len(ad_scenario.error_tables_queries) == 2
    assert actual == expected.format(ad_scenario.scenario_id)


def test_invalid_meta_table_query_for_adaptor(ad_scenario):
    name, total_count_query, error_count_query = ad_scenario.invalid_meta_queries[0]
    actual = ad_scenario.prepare_query(total_count_query)
    expected = "select count(*) as total_count from brms_instance.actual_ds1"
    assert actual == expected
    actual = ad_scenario.prepare_query(error_count_query)
    expected = "select count(*) from (select distinct c1, c3 from brms_instance.error_actual_ds1_{0}) as t"
    assert actual == expected.format(ad_scenario.scenario_id)