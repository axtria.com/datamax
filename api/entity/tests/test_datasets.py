#   datamax/api/entity/tests/test_datasets.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.datasets import Datasets
from utils.exceptions import BRMSDatasetNotFound, BRMSMergingStrategyNotDefined, BRMSDuplicateColumnFound, \
    BRMSInvalidDatasetName


def test_empty_selected_datasets():
    sd = Datasets()
    assert len(sd.serialize) == 0


def test_initialize():
    sd = Datasets.initialize({'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
                                      {'column_name': 'c2', 'data_type': 'numeric'}]})
    assert len(sd.serialize) == 1


def test_initialize_multiple_datasets():
    sd = Datasets.initialize({'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
                                      {'column_name': 'c2', 'data_type': 'numeric'}],
                              'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
                                      {'column_name': 'c2', 'data_type': 'numeric'}]})
    assert len(sd.serialize) == 2


def test_add_selected_dataset():
    sd = Datasets()
    sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    assert len(sd.serialize) == 1


def test_remove_selected_dataset():
    sd = Datasets()
    sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    sd.remove_dataset('ds1')
    assert len(sd.serialize) == 0


def test_get_selected_dataset():
    sd = Datasets()
    sd.add_dataset('ds1', [])
    assert sd.get_dataset('ds1') is not None


def test_get_dataset_from_empty_raises_error():
    sd = Datasets()
    with pytest.raises(BRMSDatasetNotFound):
        sd.get_dataset('ds1')


def test_delete_dataset_from_empty_raises_error():
    sd = Datasets()
    with pytest.raises(BRMSDatasetNotFound):
        sd.remove_dataset('ds1')


def test_truncate_datasets():
    sd = Datasets()
    sd.add_dataset('ds1', [])
    sd.add_dataset('ds2', [])
    sd.truncate()
    assert len(sd.serialize) == 0


def test_get_all_metadata():
    sd = Datasets()
    sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.get_metadata('ds1')
    expected = [{'column_name': 'c1', 'data_type': 'varchar'},
                {'column_name': 'c2', 'data_type': 'numeric'}]
    assert actual == expected


def test_get_all_metadata_by_upper_case():
    sd = Datasets()
    sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.get_metadata('DS1')
    expected = [{'column_name': 'c1', 'data_type': 'varchar'},
                {'column_name': 'c2', 'data_type': 'numeric'}]
    assert actual == expected


def test_get_all_metadata_by_alternate_case():
    sd = Datasets()
    sd.add_dataset('dS1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.get_metadata('Ds1')
    expected = [{'column_name': 'c1', 'data_type': 'varchar'},
                {'column_name': 'c2', 'data_type': 'numeric'}]
    assert actual == expected


def test_get_specific_metadata():
    sd = Datasets()
    sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.get_metadata('ds1', ['c1', 'c2'])
    expected = [{'column_name': 'c1', 'data_type': 'varchar'},
                {'column_name': 'c2', 'data_type': 'numeric'}]
    assert actual == expected


def test_get_specific_metadata_by_upper_case():
    sd = Datasets()
    sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.get_metadata('DS1', ['C1'])
    expected = [{'column_name': 'c1', 'data_type': 'varchar'}]
    assert actual == expected


def test_get_specific_metadata_by_alternate_case():
    sd = Datasets()
    sd.add_dataset('dS1', [{'column_name': 'C1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.get_metadata('Ds1', ['c1'])
    expected = [{'column_name': 'C1', 'data_type': 'varchar'}]
    assert actual == expected


def test_get_specific_metadata_by_alternate_case_in_multiple_datasets():
    sd = Datasets()
    sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar'}])
    sd.add_dataset('ds2', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'},
                           {'column_name': 'c4', 'data_type': 'varchar'},
                           {'column_name': 'C5', 'data_type': 'varchar'}])
    actual = sd.get_metadata('ds2', ['C1', 'c5'])
    expected = [{'column_name': 'c1', 'data_type': 'varchar'},
                {'column_name': 'C5', 'data_type': 'varchar'}]
    assert actual == expected


def test_resolve_table_name():
    sd = Datasets()
    sd.add_dataset('dS1', [{'column_name': 'C1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.resolve_table_name('ds1')
    expected = 'dS1'
    assert actual == expected


def test_resolve_column_name():
    sd = Datasets()
    sd.add_dataset('dS1', [{'column_name': 'C1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.resolve_column_name('Ds1', 'C2')
    expected = 'c2'
    assert actual == expected


def test_resolve_column_name_not_found():
    sd = Datasets()
    sd.add_dataset('dS1', [{'column_name': 'C1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'numeric'}])
    actual = sd.resolve_column_name('Ds1', 'C3')
    expected = 'C3'
    assert actual == expected


def test_get_names():
    sd = Datasets()
    sd.add_dataset('ds1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar'}])
    sd.add_dataset('ds2', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'},
                           {'column_name': 'c4', 'data_type': 'varchar'},
                           {'column_name': 'C5', 'data_type': 'varchar'}])
    actual = sd.get_names()
    expected = ['ds1', 'ds2']
    assert actual == expected


def test_default_merger_strategy():
    sd = Datasets()
    assert sd.merging_strategy == '#'


def test_allowed_merger_strategy():
    sd = Datasets()
    sd.merging_strategy = '_'
    assert sd.merging_strategy == '_'
    sd.merging_strategy = '#'
    assert sd.merging_strategy == '#'


def test_not_allowed_merger_strategy():
    sd = Datasets()
    with pytest.raises(BRMSMergingStrategyNotDefined):
        sd.merging_strategy = '$'


def test_hashed_merger_strategy():
    sd = Datasets()
    sd.merging_strategy = '#'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.c1'])
    expected = [(('rs1.c1', 'rs1#c1'), {'column_name': 'rs1#c1', 'data_type': 'varchar'})]
    assert actual == expected


def test_hashed_merger_strategy_on_derived_columns():
    sd = Datasets()
    sd.merging_strategy = '#'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.c2'])
    expected = [(('rs1.c2', 'c2'), {'column_name': 'c2', 'data_type': 'varchar', 'derived': True})]
    assert actual == expected


def test_hashed_merger_strategy_on_all_columns():
    sd = Datasets()
    sd.merging_strategy = '#'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.*'])
    expected = [(('rs1.c1', 'rs1#c1'), {'column_name': 'rs1#c1', 'data_type': 'varchar'}),
                (('rs1.c2', 'c2'), {'column_name': 'c2', 'data_type': 'varchar', 'derived': True})]
    assert actual == expected


def test_hashed_merged_strategy_on_duplicate_columns():
    sd = Datasets()
    sd.merging_strategy = '#'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    sd.add_dataset('rs2', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'},
                           {'column_name': 'c4', 'data_type': 'varchar', 'derived': True},
                           {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.c1', 'rs1.c2', 'rs2.c1'])
    expected = [(('rs1.c1', 'rs1#c1'), {'column_name': 'rs1#c1', 'data_type': 'varchar'}),
                (('rs1.c2', 'c2'), {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}),
                (('rs2.c1', 'rs2#c1'), {'column_name': 'rs2#c1', 'data_type': 'varchar'})]
    assert actual == expected


def test_hashed_merged_strategy_on_duplicate_derived_columns():
    sd = Datasets()
    sd.merging_strategy = '#'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    sd.add_dataset('rs2', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True},
                           {'column_name': 'c3', 'data_type': 'numeric'},
                           {'column_name': 'c4', 'data_type': 'varchar', 'derived': True},
                           {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}])
    with pytest.raises(BRMSDuplicateColumnFound):
        sd.get_merged_names(['rs1.c1', 'rs1.c2', 'rs2.c2'])


def test_hashed_merged_strategy_on_asterisk_columns():
    sd = Datasets()
    sd.merging_strategy = '#'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    sd.add_dataset('rs2', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'},
                           {'column_name': 'c4', 'data_type': 'varchar', 'derived': True},
                           {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.*', 'rs2.c1', 'rs2.C5'])
    expected = [(('rs1.c1', 'rs1#c1'), {'column_name': 'rs1#c1', 'data_type': 'varchar'}),
                (('rs1.c2', 'c2'), {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}),
                (('rs2.c1', 'rs2#c1'), {'column_name': 'rs2#c1', 'data_type': 'varchar'}),
                (('rs2.C5', 'C5'), {'column_name': 'C5', 'data_type': 'varchar', 'derived': True})]
    assert actual == expected


def test_dashed_merger_strategy():
    sd = Datasets()
    sd.merging_strategy = '_'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.c1'])
    expected = [(('rs1.c1', 'c1'), {'column_name': 'c1', 'data_type': 'varchar'})]
    assert actual == expected


def test_dashed_merger_strategy_on_derived_columns():
    sd = Datasets()
    sd.merging_strategy = '_'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.c2'])
    expected = [(('rs1.c2', 'c2'), {'column_name': 'c2', 'data_type': 'varchar', 'derived': True})]
    assert actual == expected


def test_dashed_merger_strategy_on_all_columns():
    sd = Datasets()
    sd.merging_strategy = '_'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.*'])
    expected = [(('rs1.c1', 'c1'), {'column_name': 'c1', 'data_type': 'varchar'}),
                (('rs1.c2', 'c2'), {'column_name': 'c2', 'data_type': 'varchar', 'derived': True})]
    assert actual == expected


def test_dashed_merged_strategy_on_duplicate_columns():
    sd = Datasets()
    sd.merging_strategy = '_'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    sd.add_dataset('rs2', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'},
                           {'column_name': 'c4', 'data_type': 'varchar', 'derived': True},
                           {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.c1', 'rs1.c2', 'rs2.c1'])
    expected = [(('rs1.c1', 'rs1_c1'), {'column_name': 'rs1_c1', 'data_type': 'varchar'}),
                (('rs1.c2', 'c2'), {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}),
                (('rs2.c1', 'rs2_c1'), {'column_name': 'rs2_c1', 'data_type': 'varchar'})]
    assert actual == expected


def test_dashed_merged_strategy_on_asterisk_columns():
    sd = Datasets()
    sd.merging_strategy = '_'
    sd.add_dataset('rs1', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}])
    sd.add_dataset('rs2', [{'column_name': 'c1', 'data_type': 'varchar'},
                           {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'},
                           {'column_name': 'c4', 'data_type': 'varchar', 'derived': True},
                           {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}])
    actual = sd.get_merged_names(['rs1.*', 'rs2.c1', 'rs2.C5'])
    expected = [(('rs1.c1', 'rs1_c1'), {'column_name': 'rs1_c1', 'data_type': 'varchar'}),
                (('rs1.c2', 'c2'), {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}),
                (('rs2.c1', 'rs2_c1'), {'column_name': 'rs2_c1', 'data_type': 'varchar'}),
                (('rs2.C5', 'C5'), {'column_name': 'C5', 'data_type': 'varchar', 'derived': True})]
    assert actual == expected


def test_dataset_name_integrity_allowed_characters():
    sd = Datasets()
    sd.merging_strategy = "_"
    sd.add_dataset('rs', [])
    sd.add_dataset('rs1', [])
    sd.add_dataset('rs_1', [])
    sd.add_dataset('_sd_1', [])
    sd.add_dataset('r', [])
    assert len(sd.serialize) == 5


def test_dataset_name_integrity_disallowed_characters():
    sd = Datasets()
    sd.merging_strategy = '_'
    with pytest.raises(BRMSInvalidDatasetName):
        sd.add_dataset('$rs1', [])
        sd.add_dataset('%rw1', [])
        sd.add_dataset('12rs', [])
        sd.add_dataset('rs 12', [])
        sd.add_dataset("rs'12'", [])
    assert len(sd.serialize) == 0


def test_dataset_name_integrity_allowed_characters_in_hashed_strategy():
    sd = Datasets()
    sd.merging_strategy = '#'
    sd.add_dataset('rs', [])
    sd.add_dataset('r-s1', [])
    sd.add_dataset('rs#_1', [])
    sd.add_dataset('_sd_1#', [])
    sd.add_dataset('r', [])
    assert len(sd.serialize) == 5


def test_dataset_name_integrity_disallowed_characters_in_hashed_strategy():
    sd = Datasets()
    sd.merging_strategy = '#'
    with pytest.raises(BRMSInvalidDatasetName):
        sd.add_dataset('$rs1', [])
        sd.add_dataset('%rw1', [])
        sd.add_dataset('12rs', [])
        sd.add_dataset('rs 12', [])
        sd.add_dataset("rs'12'", [])
    assert len(sd.serialize) == 0


def test_dataset_name_exceed_length_should_raise_error():
    sd = Datasets()
    with pytest.raises(BRMSInvalidDatasetName):
        sd.add_dataset('r'*128, [])
    assert len(sd.serialize) == 0
