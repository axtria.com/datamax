import re
from uuid import uuid4

from flashtext import KeywordProcessor

from entity import Entity
from entity.workflow import Workflow


class ETLScenario(Entity):
    def __init__(self, scenario_id: str, scenario_name: str, is_active: bool, workflow_id: str, *,
                 execution_schema: str, output_schema: str):
        super().__init__()
        self.scenario_id = scenario_id
        self.is_active = is_active
        self.keyword_processor = KeywordProcessor()
        self.format_kwargs = {'scenario_id': scenario_id,
                              'execution_schema': execution_schema, 'output_schema': output_schema}
        self.__meta = {'scenario_id': scenario_id, 'scenario_name': scenario_name, 'workflow_id': workflow_id,
                       'status': 'passed'}
        self.outputs = {}
        self.__queries = []

        self.log.debug(self.format_kwargs)

    @classmethod
    def initialize_with_adaptor(cls, schema: str, workflow: Workflow, input_adaptors: dict, parameters: dict,
                                output_adaptors: list):
        scenario_id = uuid4().hex
        scenario_name = workflow.name

        scenario = cls(scenario_id=scenario_id, scenario_name=scenario_name, is_active=True,
                       workflow_id=workflow.workflow_id, execution_schema=schema, output_schema=schema)

        scenario.format_kwargs['schema'] = schema

        # all datasets
        for dataset_name in workflow.get_dataset_names():
            scenario.format_kwargs['schema_' + dataset_name] = schema
            scenario.format_kwargs[dataset_name] = dataset_name + '_' + scenario.scenario_id
            scenario.outputs[dataset_name] = schema + '.' + dataset_name + '_' + scenario.scenario_id

        # selected datasets
        for adaptor_name, actual_name in input_adaptors.items():
            scenario.format_kwargs['schema_' + adaptor_name] = schema
            scenario.format_kwargs[adaptor_name] = actual_name
            scenario.outputs[adaptor_name] = schema + '.' + actual_name

        # output datasets
        for dataset_name in output_adaptors:
            scenario.format_kwargs['schema_' + dataset_name] = schema

        # parameters
        for name, value in parameters.items():
            scenario.keyword_processor.add_keyword(name, value)

        return scenario

    @classmethod
    def initialize_with_dataset(cls, scenario_id: int, scenario_name: str, scenario_meta: dict, is_active: bool,
                                workflow: Workflow, *, execution_schema: str, output_schema: str):

        scenario = cls(scenario_id=str(scenario_id), scenario_name=scenario_name, is_active=is_active,
                       workflow_id=workflow.workflow_id, execution_schema=execution_schema, output_schema=output_schema)

        # all datasets
        for dataset_name in workflow.get_dataset_names():
            scenario.format_kwargs['schema_' + dataset_name] = execution_schema
            scenario.format_kwargs[dataset_name] = dataset_name + '_' + scenario.scenario_id
            scenario.outputs[dataset_name] = execution_schema + '.' + dataset_name + '_' + scenario.scenario_id

        # selected datasets
        for dataset_name in workflow.selected_datasets:
            scenario.format_kwargs['schema_' + dataset_name] = workflow.selected_datasets.get_schema(dataset_name)
            scenario.format_kwargs[dataset_name] = dataset_name
            scenario.outputs[dataset_name] = workflow.selected_datasets.get_schema(dataset_name) + '.' + dataset_name

        # output datasets
        for dataset_name in scenario_meta.get('output_adaptors', []):
            scenario.format_kwargs['schema_' + dataset_name] = output_schema
            scenario.outputs[dataset_name] = output_schema + '.' + dataset_name + '_' + scenario.scenario_id

        # parameters
        for parameter in scenario_meta.get('parameters', []):
            scenario.keyword_processor.add_keyword(parameter['name'], parameter['value'])

        return scenario

    def prepare_query(self, query: str):
        query = self.keyword_processor.replace_keywords(query.format(**self.format_kwargs))
        query = re.sub(r'([^. ]+#[^, ]+)', r'"\1"', query)
        self.log.debug(query)
        self.__queries.append(query)
        return query

    def mark_fail(self):
        self.__meta['status'] = 'failed'

    @property
    def serialize(self):
        return {'META': self.__meta, 'QUERY': self.__queries, 'OUTPUT': self.outputs}


class ValidationScenario(Entity):
    def __init__(self, scenario_id: str, scenario_name: str, is_active: bool, workflow_id: str, *,
                 execution_schema: str, output_schema: str):
        """

        Args:
            scenario_id:
            scenario_name:
            is_active:
            workflow_id:
            execution_schema: Not in use.
            output_schema:
        """
        super().__init__()
        self.scenario_id = scenario_id
        self.is_active = is_active
        self.keyword_processor = KeywordProcessor()
        self.format_kwargs = {'scenario_id': scenario_id,
                              'execution_schema': execution_schema, 'output_schema': output_schema}
        self.__meta = {'scenario_id': scenario_id, 'scenario_name': scenario_name, 'workflow_id': workflow_id,
                       'status': 'passed'}
        self.outputs = {}
        self.invalid_meta = {}

        self.error_tables_queries = []
        self.success_tables_queries = []
        self.invalid_meta_queries = []  # (name, total_count_query, error_count_query)

        self.__queries = []

        self.log.debug(self.format_kwargs)

    @classmethod
    def initialize_with_adaptor(cls, schema: str, workflow: Workflow, input_adaptors: dict, parameters: dict,
                                input_adaptor_columns: dict):
        scenario_id = uuid4().hex
        scenario_name = workflow.name

        scenario = cls(scenario_id=scenario_id, scenario_name=scenario_name, is_active=True,
                       workflow_id=workflow.workflow_id, execution_schema=schema, output_schema=schema)

        scenario.format_kwargs['schema'] = schema

        for adaptor_name, actual_name in input_adaptors.items():
            # selected datasets
            scenario.format_kwargs['schema_' + adaptor_name] = schema
            scenario.format_kwargs[adaptor_name] = actual_name
            scenario.outputs[adaptor_name] = schema + '.' + actual_name

            # error datasets
            scenario.format_kwargs['schema_error_' + adaptor_name] = schema
            scenario.format_kwargs['error_' + adaptor_name] = 'error_' + actual_name + '_' + scenario.scenario_id
            scenario.outputs['error_' + adaptor_name] = schema + '.error_' + \
                                                        actual_name + '_' + scenario.scenario_id

            # success datasets
            scenario.format_kwargs['schema_success_' + adaptor_name] = schema
            scenario.format_kwargs['success_' + adaptor_name] = 'success_' + actual_name + '_' + scenario.scenario_id
            scenario.outputs['success_' + adaptor_name] = schema + '.success_' + \
                                                          actual_name + '_' + scenario.scenario_id

            adaptor_columns_str = ', '.join(map(lambda x: x['column_name'],
                                                workflow.datasets.get_metadata(
                                                    adaptor_name, input_adaptor_columns[adaptor_name])
                                                ))

            # create error tables
            scenario.error_tables_queries += ValidationScenario.create_error_table_query(adaptor_name)

            # create success tables
            scenario.success_tables_queries += ValidationScenario.create_success_table_query(adaptor_name,
                                                                                             adaptor_columns_str)

            # invalid meta queries
            scenario.invalid_meta_queries.append((adaptor_name,
                                                  ValidationScenario.create_total_count_query(adaptor_name),
                                                  ValidationScenario.create_error_count_query(adaptor_name,
                                                                                              adaptor_columns_str)))

        # parameters
        for name, value in parameters.items():
            scenario.keyword_processor.add_keyword(name, value)

        return scenario

    @classmethod
    def initialize_with_dataset(cls, scenario_id: int, scenario_name: str, scenario_meta: dict, is_active: bool,
                                workflow: Workflow, *, execution_schema: str, output_schema: str):

        scenario = cls(scenario_id=str(scenario_id), scenario_name=scenario_name, is_active=is_active,
                       workflow_id=workflow.workflow_id, execution_schema=execution_schema, output_schema=output_schema)

        for dataset_name in workflow.selected_datasets:
            # selected datasets
            scenario.format_kwargs['schema_' + dataset_name] = workflow.selected_datasets.get_schema(dataset_name)
            scenario.format_kwargs[dataset_name] = dataset_name
            scenario.outputs[dataset_name] = workflow.selected_datasets.get_schema(dataset_name) + '.' + dataset_name

            # error datasets
            scenario.format_kwargs['schema_error_' + dataset_name] = output_schema
            scenario.format_kwargs['error_' + dataset_name] = 'error_' + dataset_name + '_' + scenario.scenario_id
            scenario.outputs['error_' + dataset_name] = output_schema + '.error_' + \
                                                        dataset_name + '_' + scenario.scenario_id

            # success datasets
            scenario.format_kwargs['schema_success_' + dataset_name] = output_schema
            scenario.format_kwargs['success_' + dataset_name] = 'success_' + dataset_name + '_' + scenario.scenario_id
            scenario.outputs['success_' + dataset_name] = output_schema + '.success_' + \
                                                          dataset_name + '_' + scenario.scenario_id

            dataset_columns_str = ', '.join(map(lambda x: x['column_name'],
                                                workflow.datasets.get_metadata(dataset_name)))

            # create error tables
            scenario.error_tables_queries += ValidationScenario.create_error_table_query(dataset_name)

            # create success tables
            scenario.success_tables_queries += ValidationScenario.create_success_table_query(dataset_name,
                                                                                             dataset_columns_str)

            # invalid meta queries
            scenario.invalid_meta_queries.append((dataset_name,
                                                  ValidationScenario.create_total_count_query(dataset_name),
                                                  ValidationScenario.create_error_count_query(dataset_name,
                                                                                              dataset_columns_str)))

            # parameters
        for parameter in scenario_meta.get('parameters', []):
            scenario.keyword_processor.add_keyword(parameter['name'], parameter['value'])

        return scenario

    def prepare_query(self, query: str):
        query = self.keyword_processor.replace_keywords(query.format(**self.format_kwargs))
        query = re.sub(r'([^. ]+#[^, ]+)', r'"\1"', query)
        self.log.debug(query)
        self.__queries.append(query)
        return query

    def mark_fail(self):
        self.__meta['status'] = 'failed'

    @staticmethod
    def create_error_table_query(dataset_name: str) -> list:
        return ['drop table if exists {schema_error_' + dataset_name + '}.{error_' + dataset_name + '}',
                'create table {schema_error_' + dataset_name + '}.{error_' + dataset_name +
                '} (like {schema_' + dataset_name + '}.{' + dataset_name +
                '} including defaults, dmx_message varchar)']

    @staticmethod
    def create_success_table_query(dataset_name: str, columns: str) -> list:
        return ['drop table if exists {schema_success_' + dataset_name + '}.{success_' + dataset_name + '}',
                "create table {schema_success_" + dataset_name + '}.{success_' + dataset_name +
                '} as (select ' + columns + ' from {schema_' + dataset_name + '}.{' + dataset_name +
                '} except select ' + columns + ' from {schema_error_' + dataset_name +
                '}.{error_' + dataset_name + '})']

    @staticmethod
    def create_total_count_query(dataset_name: str) -> str:
        return 'select count(*) as total_count from {schema_' + dataset_name + '}.{' + dataset_name + '}'

    @staticmethod
    def create_error_count_query(dataset_name: str, columns: str) -> str:
        return 'select count(*) from (select distinct ' + columns + ' from {schema_error_' + dataset_name + \
               '}.{error_' + dataset_name + '}) as t'

    @property
    def serialize(self):
        return {'META': self.__meta, 'QUERY': self.__queries, 'OUTPUT': self.outputs,
                'INVALID_META': self.invalid_meta}
