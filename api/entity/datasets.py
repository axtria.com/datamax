#   datamax/api/entity/datasets.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import re
from collections import Counter

from entity import Entity
from utils.exceptions import BRMSDatasetNotFound, BRMSMergingStrategyNotDefined, BRMSDuplicateColumnFound, \
    BRMSInvalidDatasetName


class Datasets(Entity):
    def __init__(self):
        super().__init__()
        self.__datasets = {}
        self.__keys = {}
        self.__merging_strategy = '#'

    @classmethod
    def initialize(cls, dict_datasets: dict):
        instance = cls()
        for name, data_dictionary in dict_datasets.items():
            instance.add_dataset(name, data_dictionary)
        return instance

    @property
    def merging_strategy(self):
        return self.__merging_strategy

    @merging_strategy.setter
    def merging_strategy(self, merger_strategy):
        if merger_strategy not in ['#', '_']:
            raise BRMSMergingStrategyNotDefined
        self.__merging_strategy = merger_strategy

    def add_dataset(self, name, data_dictionary):
        self.__validate_dataset(name)
        self.__datasets[name] = data_dictionary
        self.__keys[name.lower()] = name

    def remove_dataset(self, name):
        try:
            key = self.__keys[name.lower()]
            del self.__datasets[key]
            del self.__keys[name.lower()]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSDatasetNotFound

    def get_dataset(self, name):
        try:
            return self.__datasets[self.__keys[name.lower()]]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSDatasetNotFound

    def truncate(self):
        self.__datasets = {}

    def get_metadata(self, dataset_name: str, columns=None) -> list:
        key = self.__keys[dataset_name.lower()]
        metadata = self.__datasets[key]
        if columns:
            return list(filter(lambda x: x['column_name'].lower() in map(lambda y: y.lower(), columns), metadata))
        return metadata

    def resolve_table_name(self, table_name: str):
        try:
            return self.__keys[table_name.lower()]
        except KeyError:
            raise BRMSDatasetNotFound

    def resolve_column_name(self, table_name: str, column_name: str):
        try:
            metadata = self.__datasets[self.__keys[table_name.lower()]]
        except KeyError as e:
            self.log.exception(e)
            raise BRMSDatasetNotFound
        else:
            try:
                return list(filter(lambda x: x['column_name'].lower() == column_name.lower(), metadata))[0][
                    'column_name']
            except IndexError as e:
                self.log.exception(e)
                return column_name

    def get_names(self) -> list:
        return list(self.__datasets.keys())

    def get_merged_names(self, table_columns: list) -> list:
        """For direct consumption by the transformer. Structure of the response::

        [((old_name, new_name), {"column_name": new_name, "data_type": data_type, 'derived': derived}), ]

        Args:
            table_columns (str): table and column joined by dot.

        Returns:

        """
        if self.__merging_strategy == '#':
            return self.__generate_hashed(table_columns)
        elif self.__merging_strategy == '_':
            return self.__generate_dashed(table_columns)
        raise BRMSMergingStrategyNotDefined

    def __generate_hashed(self, table_columns: list):
        output = []
        all_derived_columns = []

        for table_column in table_columns:
            table, column = table_column.split('.')
            all_columns_metadata = self.get_metadata(table) if column == '*' else self.get_metadata(table, [column])
            for column_metadata in all_columns_metadata:
                column_name = column_metadata.get('column_name')
                data_type = column_metadata.get("data_type")
                derived = column_metadata.get('derived', False)
                # if column name has # or derived key then do nothing in logic
                if ('#' in column_name) | derived:
                    old_name = table + '.' + column_name
                    new_name = column_name
                else:
                    old_name = table + '.' + column_name
                    new_name = table.split('.')[-1] + '#' + column_name
                if derived:
                    output.append(((old_name, new_name),
                                   {"column_name": new_name, "data_type": data_type, 'derived': derived}))
                    all_derived_columns.append(column_name)
                else:
                    output.append(((old_name, new_name),
                                   {"column_name": new_name, "data_type": data_type}))

        if len(self.__get_hashed_duplicates(all_derived_columns)) > 0:
            self.log.error("duplicate column found in the derived columns list - %r" % all_derived_columns)
            raise BRMSDuplicateColumnFound
        return output

    def __get_hashed_duplicates(self, columns: list) -> list:
        return [k for k, v in Counter(columns).items() if v > 1]

    def __generate_dashed(self, table_columns: list):
        output = []
        duplicate_columns = self.__get_duplicates(table_columns)
        self.log.debug('Duplicate columns found while merging datasets %r' % duplicate_columns)

        for table_column in table_columns:
            table, column = table_column.split('.')
            all_columns_metadata = self.get_metadata(table) if column == '*' else self.get_metadata(table, [column])
            for column_metadata in all_columns_metadata:
                column_name = column_metadata.get('column_name')
                data_type = column_metadata.get("data_type")
                derived = column_metadata.get('derived', False)
                # if column name has # or derived key then do nothing in logic
                if column_name not in duplicate_columns:
                    old_name = table + '.' + column_name
                    new_name = column_name
                else:
                    old_name = table + '.' + column_name
                    new_name = table.split('.')[-1] + '_' + column_name
                if derived:
                    output.append(((old_name, new_name),
                                   {"column_name": new_name, "data_type": data_type, 'derived': derived}))
                else:
                    output.append(((old_name, new_name),
                                   {"column_name": new_name, "data_type": data_type}))
        return output

    def __get_duplicates(self, table_columns) -> list:
        all_columns = []

        for table_column in table_columns:
            table, column = table_column.split('.')
            if column == '*':
                all_columns += list(map(lambda x: x['column_name'], self.get_metadata(table)))
            else:
                all_columns.append(column)
        return [k for k, v in Counter(all_columns).items() if v > 1]

    def __validate_dataset(self, name: str):
        if self.__merging_strategy == '#':
            if not re.match('^[a-zA-Z_][a-zA-Z_0-9#-]{0,126}$', name):
                raise BRMSInvalidDatasetName
        elif self.__merging_strategy == '_':
            if not re.match('^[a-zA-Z_][a-zA-Z_0-9]{0,126}$', name):
                raise BRMSInvalidDatasetName

    @property
    def serialize(self):
        return self.__datasets
