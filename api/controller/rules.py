#   datamax/api/controller/rules.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from flask import Blueprint, request, jsonify

from models import Rules

rules = Blueprint('rules', __name__)

from app import db


@rules.route('/')
def index():
    return jsonify(rules=[r.serialize for r in db.session.query(Rules).
                   order_by(Rules.id.desc()).all()])


@rules.route('/search')
def search():
    """Fetch all list of all the rules present in the system.

        Parameters:
            dtype (string): [Optional] data type on which the rules apply. Accepted values "varchar|numeric|any|file|date"
            level (string): [Optional] output is generated at row|column|file level.
            is_active (bool): [Optional] default is set to True, to show only active rules.


        Returns:
            dict : The dictionary has a format as follows

        Examples:
            Rules which apply on any data type

            >>> import  requests
            >>> request.get("http://localhost/api/rules/search?")
            {
                "rules": [
                    {
                    "id" : "int",
                    "name": "string",
                    "description": "string",
                    "defaultparameters": [
                        {"param": "string", "value": "string"},
                        ("param": "string", "value": "string")
                    ]},
                    {
                       ...
                    },
                ]
            }

            Rules which apply on `varchar` data type

            >>> import  requests
            >>> request.get("http://localhost/api/rules/search?dtype=varchar")
            {
                "rules": [
                    {
                    "id" : "int",
                    "name": "string",
                    "description": "string",
                    "defaultparameters": [
                        {"param": "string", "value": "string"},
                        ("param": "string", "value": "string")
                    ]},
                    {
                       ...
                    },
                ]
            }

            Rules which apply on `varchar` data type and `column` level.

            >>> import  requests
            >>> request.get("http://localhost/api/rules/search?dtype=varchar&level=column")
            {
                "rules": [
                    {
                    "id" : "int",
                    "name": "string",
                    "description": "string",
                    "defaultparameters": [
                        {"param": "string", "value": "string"},
                        ("param": "string", "value": "string")
                    ]},
                    {
                       ...
                    },
                ]
            }

        """
    # params
    is_active = request.args.get('is_active', type=bool, default=True)
    dtype = request.args.get('dtype', type=str, default=None)
    level = request.args.get('level', type=str, default=None)

    # filter
    search_filter = [Rules.is_active == is_active]
    search_filter.append(Rules.datatype == dtype) if dtype else None
    search_filter.append(Rules.level == level) if level else None

    # query
    all_rules = db.session.query(Rules).filter(*search_filter)

    return jsonify(rules=[r.serialize for r in all_rules])
