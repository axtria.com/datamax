#   datamax/api/controller/broker.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from flask import Blueprint, request

from services import ServiceFactory

broker = Blueprint('broker', __name__)


@broker.route('/')
def index():
    return "Broker between Databases"


@broker.route('/redshift')
def execute_at_redshift():
    """broker between redshift and postgres db.

        executes the function on postgres to generate the query
        executes this query on redshift

        Parameters:
            rs_connector_id (int): to assign the Redshift resource to be used for the execution.
            name (string): postgres function to generate the query

        Returns:
            dict: 

        Examples:
            

        """
    
    service = ServiceFactory('db_broker', 'RedshiftBroker', request)
    return service.invoke()