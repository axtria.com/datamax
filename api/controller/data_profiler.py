# # datamax/api/data_profiler.py
#
# from flask import Blueprint, jsonify, request
#
# from src.services.data_profiler.source import Source
# from src.services.data_profiler.staging import Staging
# from src.vendor.neptune import Neptune
# from src.vendor.redshift import Redshift
# from src.vendor.s3 import S3
#
# data_profiler = Blueprint('data_profiler', __name__)
#
# # Vendors
# s3 = S3()
# redshift = Redshift()
# catalog = Neptune()
#
#
# @data_profiler.route('/')
# def index():
#     pass
#
#
# @data_profiler.route('/source')
# def data_profile_source():
#     service = Source(catalog, redshift, s3, **request.args.to_dict(flat=True))
#     service.invoke()
#     return jsonify(service.response)
#
#
# @data_profiler.route('/staging')
# def data_profile_staging():
#     service = Staging(catalog, redshift, s3, **request.args.to_dict(flat=True))
#     service.invoke()
#     return jsonify(service.response)
