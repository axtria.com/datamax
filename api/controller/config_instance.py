#   datamax/api/controller/config_instance.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
"""Config instance module.

See Also:

    :func:`~controller.project` To manage projects

    :func:`~controller.config` To manage configuration of validation engine

    :func:`~controller.validation` To execute the validations on S3 and Redshift.

"""
from flask import Blueprint, jsonify, request

from models import ConfigInstance, RuleInstance

config_instance = Blueprint('config_instance', __name__)

from app import db


@config_instance.route('/')
def index():
    """Fetch the instances of validation run in reverse chronological order.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/config_instance/')
        {
          "config_instances": [
            {
              "config_id": 294,
              "created_by": null,
              "detail_schema_name": "testing_stage",
              "detail_table_name": "calls_4u_detail_2590",
              "end_date": "Fri, 19 Jul 2019 15:04:54 GMT",
              "fail_count": 0,
              "failure_schema_name": null,
              "failure_table_name": null,
              "id": 2590,
              "rule_type": "user_defined",
              "stage": 1,
              "start_date": "Fri, 19 Jul 2019 15:04:47 GMT",
              "status": "Completed",
              "success_count": 38862,
              "success_schema_name": null,
              "success_table_name": null,
              "target_schema": "testing_stage",
              "target_table": "calls_4u"
            },
            {
              "config_id": 181,
              "created_by": null,
              "detail_schema_name": null,
              "detail_table_name": "novartis-internal/Inputs/CDW/all_file/CALLS_4U.txt.gz_detail_2589",
              "end_date": "Fri, 19 Jul 2019 15:04:45 GMT",
              "fail_count": 0,
              "failure_schema_name": null,
              "failure_table_name": null,
              "id": 2589,
              "rule_type": "user_defined",
              "stage": 1,
              "start_date": "Fri, 19 Jul 2019 15:04:44 GMT",
              "status": "Completed",
              "success_count": null,
              "success_schema_name": null,
              "success_table_name": null,
              "target_schema": "novartis-internal-new",
              "target_table": "novartis-internal/Inputs/CDW/all_file/CALLS_4U.txt.gz"
            }
          ]
        }

    """
    return jsonify(config_instances=[c.serialize for c in db.session.query(ConfigInstance).
                   order_by(ConfigInstance.start_date.desc()).all()])


@config_instance.route('/get')
def get():
    """Get the detail of a given instance id.

    Parameters:
        config_instance_id (int): instance id generated for a given validation run.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/config_instance/get?id=2590')
        {
          "config_instances": [
            {
              "config_id": 294,
              "created_by": null,
              "detail_schema_name": "testing_stage",
              "detail_table_name": "calls_4u_detail_2590",
              "end_date": "Fri, 19 Jul 2019 15:04:54 GMT",
              "fail_count": 0,
              "failure_schema_name": null,
              "failure_table_name": null,
              "id": 2590,
              "rule_type": "user_defined",
              "stage": 1,
              "start_date": "Fri, 19 Jul 2019 15:04:47 GMT",
              "status": "Completed",
              "success_count": 38862,
              "success_schema_name": null,
              "success_table_name": null,
              "target_schema": "testing_stage",
              "target_table": "calls_4u"
            }
          ],
          "rule_instances": [
            {
              "config_instance_id": 2590,
              "end_date": "Fri, 19 Jul 2019 15:04:50 GMT",
              "fail_count": 0,
              "id": 4601,
              "query": "select False  as status,  '' as message, DMX_ID as DMX_ID from  testing_stage.calls_4u where MDM_ID is null or len(trim(MDM_ID)) = 0",
              "rule_id": 1035,
              "stage": 1,
              "start_date": "Fri, 19 Jul 2019 15:04:50 GMT",
              "status": "Completed"
            },
            {
              "config_instance_id": 2590,
              "end_date": "Fri, 19 Jul 2019 15:04:51 GMT",
              "fail_count": 0,
              "id": 4602,
              "query": "select False  as status,  '' as message, DMX_ID as DMX_ID from  testing_stage.calls_4u where ZIP is null or len(trim(ZIP)) = 0",
              "rule_id": 1035,
              "stage": 1,
              "start_date": "Fri, 19 Jul 2019 15:04:51 GMT",
              "status": "Completed"
            }
          ]
        }

    """
    config_instance_id = request.args.get('id', type=int, default=None)
    config_instances = db.session.query(ConfigInstance).filter(ConfigInstance.id == config_instance_id).all()
    rule_instances = db.session.query(RuleInstance).filter(RuleInstance.config_instance_id == config_instance_id).all()
    if config_instances:
        return jsonify(config_instances=[c.serialize for c in config_instances],
                       rule_instances=[r.serialize for r in rule_instances])
    return jsonify(config_instances=[], rule_instances=[])


@config_instance.route('/get_by_config')
def get_by_config():
    """Get the instances of validation run for a given config id.

    Parameters:
        config_id (int): id of the config id

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/config_instance/get_by_config?id=294')
        {
          "config_instances": [
            {
              "config_id": 294,
              "created_by": null,
              "detail_schema_name": "testing_stage",
              "detail_table_name": "calls_4u_detail_2590",
              "end_date": "Fri, 19 Jul 2019 15:04:54 GMT",
              "fail_count": 0,
              "failure_schema_name": null,
              "failure_table_name": null,
              "id": 2590,
              "rule_type": "user_defined",
              "stage": 1,
              "start_date": "Fri, 19 Jul 2019 15:04:47 GMT",
              "status": "Completed",
              "success_count": 38862,
              "success_schema_name": null,
              "success_table_name": null,
              "target_schema": "testing_stage",
              "target_table": "calls_4u"
            },
            {
              "config_id": 294,
              "created_by": null,
              "detail_schema_name": "testing_stage",
              "detail_table_name": "calls_4u_detail_2588",
              "end_date": "Fri, 19 Jul 2019 15:04:05 GMT",
              "fail_count": 0,
              "failure_schema_name": null,
              "failure_table_name": null,
              "id": 2588,
              "rule_type": "user_defined",
              "stage": 1,
              "start_date": "Fri, 19 Jul 2019 15:03:58 GMT",
              "status": "Completed",
              "success_count": 38862,
              "success_schema_name": null,
              "success_table_name": null,
              "target_schema": "testing_stage",
              "target_table": "calls_4u"
            }
          ]
        }

    """
    config_id = request.args.get('id', type=int, default=None)
    return jsonify(config_instances=[c.serialize for c in db.session.query(ConfigInstance)
                   .filter(ConfigInstance.config_id == config_id).order_by(ConfigInstance.start_date.desc()).all()])
