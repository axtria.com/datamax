# # datamax/api/data_ingestion.py
#
from flask import Blueprint, render_template, request, jsonify
#
# from src.services.data_ingestion.source import Source
# from src.services.data_ingestion.staging import Staging
# from src.vendor.neptune import Neptune
# from src.vendor.redshift import Redshift

from services import ServiceFactory

#
data_ingestion = Blueprint('data_ingestion', __name__)


#
# # Vendors

# redshift = Redshift()
# catalog = Neptune()
#
#
@data_ingestion.route('/')
def demo():
    return jsonify('hello')


# @data_ingestion.route('/')
# def index():
#     context = {}
#     return render_template('data_ingestion/index.html', **context)
#
#
# @data_ingestion.route('/source')
# def source():
#     service = Source(catalog, redshift, s3, **request.args.to_dict(flat=True))
#     service.execute()
#     return jsonify(service.response)
#
#
# @data_ingestion.route('/staging')
# def staging():
#     service = Staging(catalog, s3, redshift, **request.args.to_dict(flat=True))
#     service.execute()
#     return jsonify(service.response)
#
#
# @data_ingestion.route('/cdm')
# def cdm():
#     pass

@data_ingestion.route('/get_bucket')
def get_bucket():
    '''
    http://localhost:5001/api/data_ingestion/get_bucket?s3_connector_id=12&bucket_name=ABC
    '''
    service = ServiceFactory('data_validator.s3_buckets', 'S3buckets', request)
    return service.invoke()
    # s3buckets= S3buckets(1,'axtiadata2')
    # return jsonify(s3buckets.display_buckets())
