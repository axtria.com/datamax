#   datamax/api/controller/exchange.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import datetime
from functools import wraps

import jwt
import re
from flask import Blueprint, request, jsonify, session
from sqlalchemy import and_, or_, func
from werkzeug.security import generate_password_hash, check_password_hash

from models import User, UserGroup, UserGroupUserMapping, UserGroupFunctionalityMapping, FunctionalityMaster
from services import ServiceFactory
from utils.admn_permission import usermanagment


api_exchange = Blueprint('api_exchange', __name__)

from app import db, secret


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = __resolve_token(request)
        if not token:
            return jsonify({'message': '', 'traceback': 'Token is missing'}), 403
        try:
            data = jwt.decode(token, secret)
            session['loggedin_username'] = data['user']
            session['loggedin_userid'] = data['loggedin_userid']
        except Exception as e:
            return jsonify({'message': '', 'traceback': 'Token is invalid'}), 403
        return f(*args, **kwargs)

    return decorated


def __resolve_token(request):
    if request.headers.get('Authorization'):
        return request.headers.get('Authorization').split(' ')[1]
    elif request.cookies.get('token'):
        return request.cookies.get('token')
    return request.args.get('token', type=str, default=None)


@api_exchange.route('/', methods=['get', 'post'])
@token_required
def index():
    """API Exchange - to deliver records against the pre-defined queries.



    Returns:
        dict: JSON based record set

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/exchange?name=rules&id=1023')

    """

    service = ServiceFactory('api_exchange', 'ApiExchange', request)
    resp = service.invoke()

    # if not isinstance(resp, tuple):
    #     service = ServiceFactory('notification', 'SendNotification', request,{"response":str(resp.response)})
    #     service.invoke()
    return resp
    
    


@api_exchange.route('/list_category')
def list_category():
    """API Exchange - to deliver the categories of the API.



    Returns:
        dict: JSON based record set

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/exchange/list_category')

    """

    service = ServiceFactory('api_exchange', 'ListCategory', request)
    return service.invoke()


@api_exchange.route('/summary')
def summary():
    """API Exchange - to deliver the summary of the APIs based on category.



    Returns:
        dict: JSON based record set

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/exchange/summary?name=category_1')

    """

    service = ServiceFactory('api_exchange', 'Summary', request)
    return service.invoke()


@api_exchange.route('/add_auth', methods=['get', 'post'])
@token_required
def add_api_auth():
    """                        
    Add the new use into the database
    Returns:
        json: return parameter is json with actin summary ( text )         

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/exchange/add_auth?user_name=test110&password=datamax&'
        ...              'firstname=fistname&lastname=lastname&is_ldap_user=false&is_local_user=false&'
        ...              'email=email@axtria.com')
        {"Contents":"User Added Successfully."}
    """
    module_name = request.args.get('user_name', type=str)
    password = request.args.get('password', type=str)
    firstname = request.args.get('firstname', type=str)
    lastname = request.args.get('lastname', type=str)

    is_ldap_user = True \
        if (request.args.get('is_ldap_user') is not None
            and request.args.get('is_ldap_user') == 'true') \
        else False
    is_local_user = True if (request.args.get('is_local_user') is not None and request.args.get(
        'is_local_user') == 'true') else False
    is_admin = True if (request.args.get('is_admin') is not None and request.args.get('is_admin') == 'true') else False
    email = request.args.get('email', type=str)
    if (db.session.query(User).filter(User.username == module_name).count() == 0):
        # auth = APIAuth(name=module_name, password=generate_password_hash(password))
        user = User(username=module_name, password=generate_password_hash(password),
                    firstname=firstname, lastname=lastname, email=email, is_active=True, is_local_user=is_local_user,
                    is_ldap_user=is_ldap_user, created_by=int(session['loggedin_userid']),
                    created_date=datetime.datetime.utcnow(),
                    is_admin=is_admin, modified_by=int(session['loggedin_userid']),
                    modified_date=datetime.datetime.utcnow())
        admingroup = db.session.query(UserGroup).filter(and_(func.lower(UserGroup.name) == 'admin', UserGroup.is_active == True)).first()
        defaultgroups = db.session.query(UserGroup).filter(and_(func.lower(UserGroup.group_type) == 'd', UserGroup.is_active == True)).all()
        is_adduser_access = db.session.query(FunctionalityMaster).filter(
            and_(FunctionalityMaster.name == usermanagment.CREATE_USER, FunctionalityMaster.is_active == True)).join(UserGroupFunctionalityMapping, and_(UserGroupFunctionalityMapping.is_active == True)).join(
            UserGroupUserMapping,
            and_(UserGroupUserMapping.usergroup_id == UserGroupFunctionalityMapping.usergroup_id, UserGroupUserMapping.is_active == True)).join(User, and_(
            UserGroupUserMapping.user_id == User.user_id, User.is_active == True)).filter(User.user_id == int(session['loggedin_userid'])).count()
        if (is_adduser_access > 0):
            db.session.add(user)
            db.session.commit()
            user = db.session.query(User).filter(and_(User.username == module_name, User.is_active == True)).first()
            [db.session.add(UserGroupUserMapping(user_id=user.user_id, usergroup_id=group.usergroup_id,
                                                created_date=datetime.datetime.utcnow(),
                                                created_by=int(session['loggedin_userid']),
                                                modified_by=int(session['loggedin_userid']),
                                                modified_date=datetime.datetime.utcnow())) for group in defaultgroups]
            if (is_admin):
                usergroupmapping = UserGroupUserMapping(user_id=user.user_id, usergroup_id=admingroup.usergroup_id,
                                                        created_date=datetime.datetime.utcnow(),
                                                        created_by=int(session['loggedin_userid']),
                                                        modified_by=int(session['loggedin_userid']),
                                                        modified_date=datetime.datetime.utcnow())
                db.session.add(usergroupmapping)
            db.session.commit()
            return jsonify(Contents='User Added Successfully.')
        else:
            return jsonify({'message': 'You are not authorized to add user', 'traceback': 'You are not authorized to add user'}), 403
    else:
        return jsonify({'message': 'Username is already registerd', 'traceback': 'Username is already registerd'}), 403 


@api_exchange.route('/reset_auth', methods=['get', 'post'])
@token_required
def edit_api_auth():
    """                        
    To reset the user password
    Admin user can call the api without old_password, however for a normal user it is mandatory to supply old_password and new_password
    Returns:
        json: return parameter is json with actin summary ( text )         

    Examples:
        >>> import requests
        >>> requests.get('http://34.211.106.14/dmx_dev/api/exchange/reset_auth?user_name=xxx&old_password=xxx'
        ...              '&new_password=xxx')
        {"Contents":"User Password Reset Successfully."}
    """
    name = request.args.get('user_name', type=str)
    name = '' if str.lower(name) == 'null' else name
    old_password = request.args.get('old_password', type=str)
    new_password = request.args.get('new_password', type=str)
    user_id = -1 if request.args.get('userid', type=str) is None or request.args.get('userid',
                                                                                     type=str) == 'null' else request.args.get(
        'userid', type=int)

    new_password = generate_password_hash(new_password)
    if ((session['loggedin_userid'] == user_id) or (func.lower(session['loggedin_username']) == func.lower(name))):
        userquery = db.session.query(User).filter(and_(or_(User.user_id == user_id, User.username == name), User.is_active == True))
        user = userquery.first()
        if (userquery.count() == 1 and check_password_hash(user.password, old_password)):
            userquery.update({User.password: new_password, User.modified_by: session['loggedin_userid'],
                              User.modified_date: datetime.datetime.utcnow()}, synchronize_session=False)
            db.session.commit()
            return jsonify(Contents='User Password Reset Successfully.')
        else:
            return jsonify({'message': 'You are not authorized to reset password', 'traceback': 'You are not authorized to reset password'}), 403
    elif ((db.session.query(FunctionalityMaster).filter(and_(FunctionalityMaster.name == usermanagment.RESET_PASSWORD,FunctionalityMaster.is_active == True)).join(
            UserGroupFunctionalityMapping, and_(UserGroupFunctionalityMapping.is_active == True,UserGroupFunctionalityMapping.functionality_id == FunctionalityMaster.functionality_id )).join(
                UserGroupUserMapping, and_(UserGroupUserMapping.is_active == True, UserGroupUserMapping.usergroup_id == UserGroupFunctionalityMapping.usergroup_id)).join(User, and_(
        UserGroupUserMapping.user_id == User.user_id,  User.is_active == True)).filter(
        User.user_id == int(session['loggedin_userid'])).count()) >= 1):
        db.session.query(User).filter(or_(User.user_id == user_id, User.username == name)).update(
            {User.password: new_password, User.modified_by: session['loggedin_userid'],
             User.modified_date: datetime.datetime.utcnow()}, synchronize_session=False)
        db.session.commit()
        return jsonify(Contents='User Password Reset Successfully.')
    else:
        return jsonify({'message': 'You are not authorized to reset password', 'traceback': 'You are not authorized to reset password'}), 403


@api_exchange.route('/authorize', methods=['GET', 'POST'])
def authorize():
    """                        
    To authenticate the user
    Returns:
        json: return parameter is json with actin summary ( text )         
    Examples:
        >>> import requests
        >>> requests.get('http://34.211.106.14/dmx_dev/api/exchange/reset_auth?user_name=xxx&old_password=xxx'
        ...              '&new_password=xxx')
        {"Contents":"Access granted. Valid signature.","token":"eyJ0eXAiOiJKV1qiuJJKXKJhJ7mF1GvKTEv5Yc8"}

    In case of invalid request
        >>> import requests
        >>> requests.get('http://34.211.106.14/dmx_dev/api/exchange/reset_auth?user_name=xxx&old_password=xxx'
        ...              '&new_password=xxx')
            {"traceback":"Access denied. Invalid signature."}
    """
    if request.method == 'POST':
        module_name = request.form.get('module_name', type=str)
        password = request.form.get('password', type=str)
    else:
        module_name = request.args.get('module_name', type=str)
        password = request.args.get('password', type=str)

    user = db.session.query(User).filter(and_(User.username == module_name , User.is_active == True, User.is_rec_active == True)).first()
    if (user is None):
        return jsonify(traceback='Invalid credentials, or user is inactive.'), 403
    auth = check_password_hash(user.password, password)
    if auth:
        token = jwt.encode({'user': module_name, 'loggedin_userid': user.user_id,
                            'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=4)},
                           secret, algorithm='HS256')
        response = jsonify(Contents='Access granted. Valid signature.', token=token.decode('UTF-8'))
        response.set_cookie('token', token.decode('UTF-8'))
        return response
    return jsonify(traceback='Access denied. Invalid signature.'), 403
