#   datamax/api/controller/project.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
"""Project management API.

Project module to manage context within the application. It is assumed that user access management
is driven separately to validate the request received.

See Also:

    :func:`~controller.operations.encrypt` Encryption API to encode string.

"""
import datetime

from flask import Blueprint, jsonify, request

from models import Project

project = Blueprint('project', __name__)

from app import db


@project.route('/')
def index():
    """Fetch all the projects configured in the system.

    Returns:
        JSON: which has  details regarding the projects

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/project/')
        {
          "projects": [
            {
              "created_by": null,
              "creation_date": null,
              "description": "Some dummy text",
              "id": 1,
              "is_active": true,
              "modification_date": null,
              "modified_by": null,
              "name": "SalesIQ"
            },
            {
              "created_by": null,
              "creation_date": null,
              "description": "Novartis Reporting",
              "id": 7,
              "is_active": true,
              "modification_date": null,
              "modified_by": null,
              "name": "Novartis"
            }
          ]
        }

    """
    return jsonify(projects=[c.serialize for c in db.session.query(Project).
                   order_by(Project.creation_date.desc()).all()])


@project.route('/active')
def active():
    """Fetch all the `active` projects configured in the system.

    Returns:
        JSON: which has  details regarding the projects

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/project/active/')
        {
          "projects": [
            {
              "created_by": null,
              "creation_date": null,
              "description": "Some dummy text",
              "id": 1,
              "is_active": true,
              "modification_date": null,
              "modified_by": null,
              "name": "SalesIQ"
            },
            {
              "created_by": null,
              "creation_date": null,
              "description": "Novartis Reporting",
              "id": 7,
              "is_active": true,
              "modification_date": null,
              "modified_by": null,
              "name": "Novartis"
            }
          ]
        }

    """
    return jsonify(projects=[c.serialize for c in db.session.query(Project).filter(Project.is_active == True).
                   order_by(Project.creation_date.desc()).all()])


@project.route('/add')
def add():
    """Add project to the system.

    Parameters:
        name (str): name of the project to be added.
        description (str): describe what this project is all about.
        is_active (bool): [Optional] project is assumed to be added in active state unless provided.

    Returns:
        dict: JSON which has message in case, project has been added to the system.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/project/add?name=SalesIQ&description=some dummy text')
        {
            "message": "project added successfully"
        }

    """
    name = request.args.get('name')
    description = request.args.get('description')
    is_active = request.args.get('is_active', type=bool, default=True)
    new_project = Project(name=name, description=description, is_active=is_active,
                          creation_date=datetime.datetime.utcnow())
    db.session.add(new_project)
    db.session.commit()
    return jsonify(message='project added successfully')


@project.route('/update')
def update():
    """Update the project settings by providing the id of the project.

    Parameters:
        id (int): ID of the project within the system.
        name (str): name of the project to be added.
        description (str): describe what this project is all about.
        is_active (bool): [Optional] project is assumed to be added in active state unless provided.

    Returns:
        dict: JSON having the latest description of the project being updated.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/project/updated?id=1&name=SalesIQ&description=text')
        {
          "project": {
            "created_by": null,
            "creation_date": null,
            "description": "some text",
            "id": 1,
            "is_active": true,
            "modification_date": "Sun, 21 Jul 2019 00:00:00 GMT",
            "modified_by": null,
            "name": "SalesIQ"
          }
        }

    """
    id = request.args.get('id', type=int)
    name = request.args.get('name')
    description = request.args.get('description')
    is_active = request.args.get('is_active', type=bool, default=True)

    project_to_update = db.session.query(Project).filter(Project.id == id).first()
    project_to_update.name = name
    project_to_update.description = description
    project_to_update.is_active = is_active
    project_to_update.modification_date = datetime.datetime.utcnow()

    db.session.commit()
    return jsonify(project=project_to_update.serialize)


@project.route('/get')
def get():
    """Fetch a specific project from the system.

    Parameters:
        id (int): project ID to be fetched.

    Returns:
        dict: JSON of the record present in the system.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/project/get?id=1')
        {
          "project": {
            "created_by": null,
            "creation_date": null,
            "description": "some text",
            "id": 1,
            "is_active": true,
            "modification_date": "Sun, 21 Jul 2019 00:00:00 GMT",
            "modified_by": null,
            "name": "SalesIQ"
          }
        }

    """
    project_id = request.args.get('id', type=int)
    get_project = db.session.query(Project).filter(Project.id == project_id).first()
    if get_project:
        return jsonify(project=get_project.serialize)
    return jsonify(project={})


@project.route('/delete')
def delete():
    """Delete the project from the system.

    Parameters:
        id (int): project ID to be deleted.

    Returns:
        dict: JSON with the message - deleted successfully.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/project/delete?id=1')
        {
            "message": "deleted successfully"
        }

    """
    project_id = request.args.get('id', type=int)
    project_to_delete = db.session.query(Project).filter(Project.id == project_id).first()
    if project_to_delete:
        db.session.delete(project_to_delete)
        db.session.commit()
        return jsonify(message='deleted successfully')
    return jsonify(message='something went wrong')
