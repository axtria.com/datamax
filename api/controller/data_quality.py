#   datamax/api/controller/data_quality.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Shivankit Bagla <shivankit.bagla@axtria.com>
from flask import Blueprint, request

from services import ServiceFactory

data_quality = Blueprint('data_quality', __name__)


@data_quality.route('/')
def index():
    return "Data Quality Microservice"

@data_quality.route('/snowflake')
def execute_at_snowflake():
    """API between snowflake and postgres db.

        executes the function on postgres to generate the query
        executes this query on snowflake

        Parameters:
            sf_connector_id (int): to assign the Snowflake resource to be used for the execution.
            name (string): postgres function to generate the query

        Returns:
            dict: 

        Examples:
            

        """
    
    service = ServiceFactory('data_quality', 'DataQuality', request)
    return service.invoke()