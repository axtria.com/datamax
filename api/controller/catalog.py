#   datamax/api/controller/catalog.py
#   Copyright (C) 2019 Axtria.
#
#   Author(s):
#   Akshay Saini <akshay.saini@axtria.com>
#   Nitin Sharma <nitin.sharma@axtria.com>
#   Varun Singhal <varun.singhal@axtria.com>

"""DataMax Catalog APIs.

These set of APIs work as interface between datamax catalog moudule and neo4j DB

Notes:
    To allow construction of rules by the non-programmers.
    A rule designer that gives a text area for a person to type in statements -
    is really a programming environment, not a business user-focused tool.
"""

from flask import Blueprint, request

from controller.exchange import token_required
from services import ServiceFactory, FileFactory

catalog = Blueprint('catalog', __name__)


@catalog.route('/')
def index():
    """Catalog Home"""
    return 'Catalog Home'


@catalog.route("/s3_trigger")
def s3_trigger():
    """
    Syncs s3 object's metadata with neo4j catalog. It gets called by aws lambda trigger `datamaxCatalogS3Trigger`.

    It adds/updates metadata iff it is allowed by s3_filter config i.e. if it is configured to be monitored.

    Parameters:
        s3_connector_id (int): Connector ID
        event_details (str): strigified json of lambda event

    Returns:
        response (json)

    Notes:
        In case of delete event, this service does the soft delete from neo4j catalog
        by setting `isDeleted` attribute of the File to True.
        Gets called only for a single object at a time.
    """
    service = ServiceFactory('catalog.s3_sync', 'S3Sync', request)
    return service.invoke()


@catalog.route("/redshift_trigger")
def redshift_trigger():
    """
    Incremental scan to sync redshift metadata with neo4j.
    """
    service = ServiceFactory('catalog.redshift_sync', 'RedshiftSync', request)
    return service.invoke()


@catalog.route("/display_list_view")
def display_list_view():
    """
    Provides list of all objects present in neo4j catalog in its original hierarchy.

    Parameters: 
        None
    
    Returns:
        response (json) : list of objects
    """
    service = ServiceFactory('catalog.display_list_view', 'DisplayListView', request)
    return service.invoke()


# @catalog.route("/display_requested_view")
# def display_requested_view():
#     """
#     Provides list of requested objects. By default it provides list of layers.
#     To fetch children objects of a layer/folder/object we need to pass its node_is i.e. URI
#
#     Parameters:
#         node_id (int) : (optional) node_id(URI) of the parent
#
#     Returns:
#         response (json) : list of requested objects
#     """
#     service = ServiceFactory('catalog.display_requested_view', 'DisplayRequestedView', request)
#     return service.invoke()


@catalog.route("/display_requested_view")
def display_requested_view():
    """
    Provides list of requested objects. By default it provides list of layers.
    To fetch children objects of a layer/folder/object we need to pass its node_is i.e. URI

    Parameters:
        node_id (int) : (optional) node_id(URI) of the parent

    Returns:
        response (json) : list of requested objects
    """
    service = ServiceFactory('catalog.display_requested_view_v2', 'DisplayRequestedView', request)
    return service.invoke()


@catalog.route("/display_object_list_brms")
def display_list_brms():
    """
    Provides list of objects for brms module for a specific connector.

    Parameters:
        connector_id (int) : connector id of s3/redshift/...

    Returns:
        response (json) : list of requested objects
    """
    service = ServiceFactory('catalog.display_list_brms', 'DisplayListBRMS', request)
    return service.invoke()


@catalog.route("/edit_attribute")
def edit_description():
    """
    Provides way to edit attribute associated with with metadata.

    Parameters:
        node_id (int) : node ID(URI) of the object
        attribute_key (str): name of the attribute which needs to be changed
        value : new value of the  attribute

    Returns:
        response (json)

    Notes:
        This service os only for single valued attributes. So, won't work for tags.
    """
    service = ServiceFactory('catalog.edit_attribute', 'EditAttribute', request)
    return service.invoke()


@catalog.route("/ingestion")
def ingestion():
    """
    Store metadata of ingested file in neo4j
    """
    service = ServiceFactory('catalog.ingestion', 'Ingestion', request)
    return service.invoke()


@catalog.route("/catalog_object_search")
def catalog_object_search():
    """
    Returns list of objects using indexed search on neo4j.
    It accepts search string or URI.
    URI should be exact match.

    Parameters:
        search_string (str) : search string
        search_on (str) : (Optional) attribute on which will be applied. eg. remarks, tags
        max_results (int) : (Optional - Default 100) number of max results

        layer_type (str) : (Optional) filter seach on layer type. eg. S3 Bucket, Redshift Schema
        layer_name (str) : (Optional) filter search on layer name. eg. datamax1
        file_size (str) : (Optiona) size in bytes. eg ">300000", "<400000"
        timestamp(int) : (Optional) filter on timestamp on the basis of number of days. 

    Returns:
        reponse (json)

    Notes:
        file_size parameter only works on s3 and its value should always be preceded by either "<" or ">"

    """
    service = ServiceFactory('catalog.catalog_object_search', 'CatalogObjectSearch', request)
    return service.invoke()


@catalog.route("/export_catalog")
def export_catalog():
    """
    Export requested catalog objects on a csv file

    Parameters:
        layers (str) : (Optional) comma separated set of layer_type and layer name. eg. "Redshift Layer:public, S3 Bucket:datamax1"

    Returns:
        response (json/csv)
    """
    service = FileFactory('catalog.bulk_dump', 'BulkDump', request)
    return service.invoke()

@catalog.route("/export_nodes",methods=["GET", "POST"])
def export_nodes():
    """
    Export requested selective catalog objects on a csv file

    Parameters:
        uri (str) : List of URI id which user want to export

    Returns:
        response (json/csv)
    """
    service = FileFactory('catalog.bulk_dump', 'DumpNodes', request)
    return service.invoke()



@catalog.route("/export_catalog_column_level")
def export_catalog_column_level():
    """
    Export requested catalog objects on a csv file

    Parameters:
        layers (str) : (Optional) comma separated set of layer_type and layer name. eg. "Redshift Layer:public, S3 Bucket:datamax1"

    Returns:
        response (json/csv)
    """
    service = FileFactory('catalog.bulk_dump_column_level', 'BulkDumpColumnLevel', request)
    return service.invoke()





@catalog.route("/validate_import_catalog", methods=['POST'])
def validate_import_catalog():
 
    service = ServiceFactory('catalog.bulk_import_validate', 'BulkImportValidate', request)
    return service.invoke()


@catalog.route("/import_catalog")
def import_catalog():

    service = ServiceFactory('catalog.bulk_import', 'BulkImport', request)
    return service.invoke()


@catalog.route("/import_catalog_column_level")
def import_catalog_column_level():

    service = ServiceFactory('catalog.bulk_import_column_level', 'BulkImportColumnLevel', request)
    return service.invoke()


@catalog.route("/import_catalog_report")
def bulk_import_report():

    service = FileFactory('catalog.bulk_import_report', 'BulkImportReport', request)
    return service.invoke()





@catalog.route("/user_rating")
def user_rating():
    """
    Add/update user ratings of a catalog object.
    """
    service = ServiceFactory('catalog.user_rating', 'UserRating', request)
    return service.invoke()


@catalog.route("/add_comment_and_rating")
def add_comment_and_rating():
    """
    Add/update user ratings of a catalog object.
    """
    service = ServiceFactory('catalog.add_comment_and_rating', 'AddCommentAndRating', request)
    return service.invoke()


@catalog.route("/view_comment_and_rating")
def view_comment_and_rating():
    """
    View user ratings of a catalog object.
    """
    service = ServiceFactory('catalog.view_comment_and_rating', 'ViewCommentAndRating', request)
    return service.invoke()


@catalog.route("/save_catalog_search_query")
def save_catalog_search_query():
    """
    Saves catalog search query.
    """
    service = ServiceFactory('catalog.search_log', 'SaveCatalogSearchQueryV2', request)
    return service.invoke()


@catalog.route("/get_catalog_search_query")
def get_catalog_search_query():
    """
    Gets catalog search query.
    """
    service = ServiceFactory('catalog.search_log', 'GetCatalogSearchQueryV2', request)
    return service.invoke()


# ##########
# Tags
# ##########


@catalog.route("/delete_tag")
@token_required
def delete_tag():
    """
    Deletes tag from the tag library.

    Parameters:
        tag_name (str) : tag name to be deleted from the library.

    Returns:
        response (json)

    Raises:
        TagNotFound: Tag does not exist in the library.
    """
    service = ServiceFactory('catalog.business_glossary', 'DeleteTag', request)
    return service.invoke()


@catalog.route("/update_tag")
@token_required
def update_tag():
    """
    Updates tag in the tag library.

    Parameters:
        tag_name (str) : tag name to be updated.
        params (dict): attributes with key - value pair to be updated.

    Returns:
        response (json)

    Raises:
        TagNotFound: Tag does not exist in the library.
    """
    service = ServiceFactory('catalog.business_glossary', 'UpdateTag', request)
    return service.invoke()


@catalog.route("/list_tags")
@token_required
def list_tags():
    """
    List tags from the tag library.

    Returns:
        response (json)
    """
    service = ServiceFactory('catalog.business_glossary', 'ListTags', request)
    return service.invoke()


@catalog.route("/link_tag")
@token_required
def link_tag():
    """
    link tags from the tag library.

    Parameters:
        node_id: node id of the data object.
        name: name of the tag

    Returns:
        response (json)

    Raises:
        TagNotFound: Tag does not exists.
    """
    service = ServiceFactory('catalog.business_glossary', 'LinkTag', request)
    return service.invoke()


@catalog.route("/unlink_tag")
@token_required
def unlink_tag():
    """
    Unlink tags from the tag library.

    Parameters:
        node_id: node id of the data object.
        name: name of the tag.

    Returns:
        response (json)

    Raises:
        TagNotFound: Tag does not exists.
    """
    service = ServiceFactory('catalog.business_glossary', 'UnlinkTag', request)
    return service.invoke()

@catalog.route("/add_business_glossary")
@token_required
def add_business_glossary():
    """
    Unlink tags from the tag library.

    Parameters:
        node_id: node id of the data object.
        name: name of the tag.

    Returns:
        response (json)

    Raises:
        GlossaryAlreadyExists: raises with duplicated entry found (unique check in category and name)
    """
    service = ServiceFactory('catalog.business_glossary', 'AddGlossary', request)
    return service.invoke()

@catalog.route("/list_business_glossary")
def list_business_glossary():
    """
    List all the Node related to business glossary

    Parameters:
        none

    Returns:
        response (json)

    """
    service = ServiceFactory('catalog.business_glossary', 'ListGlossary', request)
    return service.invoke()

@catalog.route("/update_business_glossary")
@token_required
def update_business_glossary():
    """
    Update paramater and label of glossary nodes

    Parameters:
        node_id: node id of the data object.
        name: value of the glossary.
        category: label of node (to categorise tag, kpi, abbreviation etc)
        subcategory: sub category of glossary
        status: boolean status of node( is active or not)

    Returns:
        response (json)

    Raises:
        GlossaryNotFound: node id passed in API call doesn't matched with database
    """
    service = ServiceFactory('catalog.business_glossary', 'UpdateGlossary', request)
    return service.invoke()

@catalog.route("/search_glossary",methods=["GET", "POST"])
def search_glossary():
    """
    Search and filter the glossary data.

    Parameters:
        attribute_name: passes json of list to filter out the glossary dataset.
        contains_text:

    Returns:
        response (json)

    """
    service = ServiceFactory('catalog.business_glossary', 'SearchGlossary', request)
    return service.invoke()\

@catalog.route("/export_glossary")
def export_glossary():
    """
    Export Glossary Data

    Parameters:
        None

    Returns:
        response (json)

    """

    service = FileFactory('catalog.business_glossary', 'ExportGlossary', request)
    return service.invoke()

@catalog.route("/validate_glossary",methods=['POST'])
@token_required
def validate_glossary():
    """
    Search and filter the valid record and import into glossary data.

    Parameters:
        file : file path (post call)

    Returns:
        response (json)

    """
    service = ServiceFactory('catalog.business_glossary', 'ValidateGlossary', request)
    return service.invoke()

@catalog.route("/import_glossary",methods=['GET','POST'])
@token_required
def import_glossary():
    """
    Search and filter the valid record and import into glossary data.

    Parameters:
        file : file path (post call)

    Returns:
        response (json)

    """
    service = ServiceFactory('catalog.business_glossary', 'ImportGlossary', request)
    return service.invoke()



# #############
# Scanner
# #############


@catalog.route("/snowflake_full_scan")
@token_required
def snowflake_full_scan():
    """
    Scans Snowflake DB
    """
    service = ServiceFactory('catalog.snowflake_full_scan', 'SnowflakeFullScanV3_temporary', request)
    return service.invoke()

@catalog.route("/snowflake_full_scan_v2")
def snowflake_full_scan_v2():
    """
    Scans Snowflake DB
    """
    service = ServiceFactory('catalog.snowflake_full_scan', 'SnowflakeFullScanV2', request)
    return service.invoke()

@catalog.route("/snowflake_incremental_scan_all")
def snowflake_incremental_scan_all():
    """
    Scans Snowflake DB
    """
    service = ServiceFactory('catalog.snowflake_incremental_scan', 'SnowflakeIncrementalScanAllConnections', request)
    return service.invoke()

@catalog.route("/snowflake_incremental_scan_individual")
def snowflake_incremental_scan_individual():
    """
    Scans Snowflake DB
    """
    service = ServiceFactory('catalog.snowflake_incremental_scan', 'SnowflakeIncrementalScanV2', request)
    return service.invoke()

@catalog.route("/snowflake_full_scan_with_replication")
def snowflake_full_scan_with_replication():
    """
    Scans Snowflake DB
    """
    service = ServiceFactory('catalog.snowflake_full_scan_with_replication', 'SnowflakeFullScanWithReplication', request)
    return service.invoke()


@catalog.route("/s3_full_scan")
def s3_full_scan():
    """
    Syncs ALL s3 objects' metadata with neo4j catalog.
    It adds/updates metadata iff it is allowed by s3_filter config i.e. if it is configured to be monitored.
    It runs asynchronously.

    Parameters:
        s3_connector_id (int): Connector ID
        bucketName (str): name of bucket

    Returns:
        response (json)

    Notes:
        If number of files, which needs to be updated on neo4j, are very high, then it can
        take a big amount of time as it tries to read few top lines of each of those files in order to
        gather column names and column datatypes of delimitted files.
    """
    service = ServiceFactory('catalog.s3_full_scan', 'S3FullScan', request)
    return service.invoke()


@catalog.route("/postgres_full_scan")
def postgres_full_scan():
    service = ServiceFactory('catalog.postgres_scan', 'PostgresFullScanV2', request)
    return service.invoke()


@catalog.route("/postgres_incremental_scan")
def postgres_incremental_scan():
    service = ServiceFactory('catalog.postgres_scan', 'PostgresIncrementalScan', request)
    return service.invoke()


@catalog.route("/redshift_full_scan")
def redshift_full_scan():
    """
    Scan metadata from redshift using system tables `svv_columns` and `svv_tables`.
    It is a schema level scan and it only scans those schemas which are configured to be monitored in redshift_filter_config.

    Parameters:
        rs_connector_id (int) : Connector ID

    Returns:
        response (json)
    """
    service = ServiceFactory('catalog.redshift_full_scan', 'RedshiftFullScan', request)
    return service.invoke()


# ############
# Profiler
# ############

@catalog.route("/redshift_profiler")
def redshift_profiler():
    """
    Computes table and column metadata along with sample and some basic statistics for a redshift table.

    Parameters:
        rs_connector_id (int) : Redshift connection ID
        node_id (int) : node ID(URI) of the redshift table
        statistics (bool) : calculates basic statistics like mean & median iff `statistics` is true
        cached (bool) : enabled/disable caching

    Returns:
        response (json)
    """
    service = ServiceFactory('catalog.redshift_profiler', 'RedshiftProfiler', request)
    return service.invoke()


@catalog.route("/s3_profiler")
def s3_profiler():
    """
    Provides sample data of the requested s3 file.

    Parameters:
        s3_connector_id (int) : S3 Connector ID
        node_id (int) : URI/node_id of the s3 file

    Returns:
        response (json)

    Notes:
        only works for text delimitted files.
    """
    service = ServiceFactory('catalog.s3_profiler', 'S3Profiler', request)
    return service.invoke()


@catalog.route("/snowflake_profiler")
def snowflake_profiler():
    """
    Provides sample data of the requested s3 file.

    Parameters:
        s3_connector_id (int) : S3 Connector ID
        node_id (int) : URI/node_id of the s3 file

    Returns:
        response (json)

    Notes:
        only works for text delimitted files.
    """
    service = ServiceFactory('catalog.snowflake_profiler', 'SnowflakeProfiler', request)
    return service.invoke()

@catalog.route("/object_file_profiler")
def object_file_profiler():
    """
    Provides data for requested object URI ID

    Parameters:
        obj_uri_id (int) : uri id of the corresponding object.

    Returns:
        response (json)

    """
    service = ServiceFactory('catalog.s3_profiler', 'ObjectFileProfiler', request)
    return service.invoke()


@catalog.route("/get_attributes")
def get_attributes():
    """
    Return all possible attributes which will be used for search_by_attribute API

    Returns:
        response (json)
    """
    service = ServiceFactory('catalog.get_attributes', 'GetAttributes', request)
    return service.invoke()


@catalog.route("/search_by_attribute", methods=["GET", "POST"])
def search_by_attribute():
    """
    filter and search Neo4j Data

    Parameters:
        search_text : Search text on filtered data (if filter applied) else search on all nodes
        attribute_name : name of the attribute in which filter are being applied

    Returns:
        response (json)
    """
    service = ServiceFactory('catalog.catalog_object_search', 'search_by_attribute', request)
    return service.invoke()

@catalog.route("/suggestions")
def suggestions():
    """
    filter and search Neo4j Data to populate the suggestion related to input received

    Parameters:
        q:

    Returns:
        response (json)
    """
    service = ServiceFactory('catalog.catalog_object_search', 'AutoSuggestion', request)
    return service.invoke()


@catalog.route("/get_glossary_meta")
def get_glossary_meta():
    """
    list down all glossary meta data

    Parameters:
        None
    Returns:
        response (json)
    """
    service = ServiceFactory('catalog.business_glossary', 'GetGlossaryMeta', request)
    return service.invoke()

@ catalog.route("/list_data_asset")
def list_data_asset():
        """
        list down all the node related to data asset label (just like list tags)

        Parameters:
            None
        Returns:
            response (json)
        """
        service = ServiceFactory('catalog.data_asset', 'ListDataAsset', request)
        return service.invoke()

@ catalog.route("/link_data_asset")
def link_data_asset():
        """
        Create and maintains only one link between Data Asset node and File/object/table node

        Parameters:
            data_asset_node : node id of data asset
            file_node : node id of file,object,table node.

        Returns:
            response (json)
        """
        service = ServiceFactory('catalog.data_asset', 'LinkDataAsset', request)
        return service.invoke()

@ catalog.route("/get_data_asset")
def get_data_asset():
        """
        Create and maintains only one link between Data Asset node and File/object/table node

        Parameters:
            None

        Returns:
            response (json)
        """
        service = ServiceFactory('catalog.data_asset', 'GetDataAsset', request)
        return service.invoke()




