#   datamax/api/controller/validation.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from flask import Blueprint, request

from services import ServiceFactory, JobFactory
from services.data_validator.redshift_validation_strategy import RedshiftValidationStrategy
from services.data_validator.s3_validation_strategy import S3ValidationStrategy

validation = Blueprint('validation', __name__)


@validation.route('/')
def index():
    return "Validation Engine"


@validation.route('/redshift')
def validation_at_redshift():
    """Run validation on Redshift using metadata from postgres db.

        Validation engine service will receive the configuration via service call to the along with the
        target table and target schema name on which the validation rules have to be applied.
        This engine is responsible for fetching the configuration based on `config_id` received.
        Once done, it will populate the record in ``summary`` table for a given profile which will have all
        the meta data about the instance run.
        For example, the time taken and no. of records passed/failed. It will also have ``details``
        table which will have all the ``DMX_ID`` which have failed for the given run.

        Parameters:
            rs_connector_id (int): to assign the Redshift resource to be used for the execution.
            config_id (int): the configuration id already present in the system, required to run the validations
            target_table (string): [Optional] table on which the validation has to be executed
            target_schema (string): [Optional] target schema
            detail_schema (string):  schema in which the detailed table is created to store the failed records
            config_instance_id (int): [Optional] to be provided to chain the validation engine request. In case
                    provided, it expects a `stage` no. which will uniquely identify the instance.
            stage (int): [Optional] in case of chained subsets of validations, mention the stage at which it has
                    to be executed.
            rule_type (string): [Optional] default has been set to `user_defined`.

        Returns:
            dict: which has the ``id`` of the validation instance run.

        Examples:
            Run a single validation rule using the config id

            >>> import requests
            >>> requests.get('http://localhost:5001/api/validation/redshift?rs_connector_id=12'
            ...              '&config_id=10&target_table=s3_e8f3658a7d6c40eeb2efc2076d969d3a&target_schema=source')
            {
              "config_instance": [
                {
                  "config_id": 10,
                  "created_by": null,
                  "detail_schema_name": "validation",
                  "detail_table_name": "s3_e8f3658a7d6c40eeb2efc2076d969d3a_detail_4",
                  "end_date": "Sat, 18 May 2019 22:04:16 GMT",
                  "fail_count": null,
                  "failure_schema_name": null,
                  "failure_table_name": null,
                  "id": 4,
                  "start_date": "Sat, 18 May 2019 22:04:05 GMT",
                  "status": "Completed",
                  "success_count": null,
                  "success_schema_name": null,
                  "success_table_name": null,
                  "target_schema": "source",
                  "target_table": "s3_e8f3658a7d6c40eeb2efc2076d969d3a"
                }
              ],
              "rule_instance": [
                {
                  "config_instance_id": 4,
                  "end_date": "Sat, 18 May 2019 22:04:14 GMT",
                  "fail_count": 24,
                  "id": 4,
                  "query": "SELECT dmx_id, FALSE as status, '' as message FROM source.s3_e8f3658a7d6c40eeb2efc2076d969d3a where not( length(market_id) = 6)",
                  "rule_id": 1009,
                  "sequence": 0,
                  "start_date": "Sat, 18 May 2019 22:04:15 GMT",
                  "status": "Completed"
                }
              ]
            }

        """
    strategy = RedshiftValidationStrategy(**request.args.to_dict(flat=True))
    service = ServiceFactory('data_validator.validation_engine', 'ValidationEngine', request,
                             {'strategy': strategy})
    return service.invoke()


@validation.route('/s3')
def validation_at_s3():
    """Run validation on S3 file without scanning the complete file.

        The service called upon will run the pre-processing rules at S3 layer. Example, check for duplicate
        column name, which will raise exception in data movement as the table cannot have same column names.

        Parameters:
            s3_connector_id (int): to assign the S3 resource to be used for the execution.
            config_id (int): the configuration id already present in the system, required to run the validations
            target_bucket (string): target bucket
            target_file_path (string): target file path with extension, currently supporting TEXTFILE.
            config_instance_id (int): [Optional] to be provided to chain the validation engine request. In case
                    provided, it expects a `stage` no. which will uniquely identify the instance.
            stage (int): [Optional] in case of chained subsets of validations, mention the stage at which it has
                    to be executed.
            rule_type (string): [Optional] default has been set to `user_defined`.


        Returns:
            dict: which has the ``id`` of the validation instance run.

        Examples:
            Check for duplicate columns and sequence of the column using validation service at S3.

            >>> import requests
            >>> requests.get('http://127.0.0.1:5000/api/validation/s3?s3_connector_id=12&config_id=30'
            ...        '&target_bucket=datamax1&target_file_path=poc/symphony_sales/Symphony_Sales.csv&stage=1')
            {
              "config_instance": [
                {
                  "config_id": 6,
                  "created_by": null,
                  "detail_schema_name": "validation",
                  "detail_table_name": "s3_e8f3658a7d6c40eeb2efc2076d969d3a_detail_4",
                  "end_date": "Sat, 18 May 2019 22:04:16 GMT",
                  "fail_count": null,
                  "failure_schema_name": null,
                  "failure_table_name": null,
                  "id": 4,
                  "start_date": "Sat, 18 May 2019 22:04:05 GMT",
                  "status": "Completed",
                  "success_count": null,
                  "success_schema_name": null,
                  "success_table_name": null,
                  "target_schema": "source",
                  "target_table": "s3_e8f3658a7d6c40eeb2efc2076d969d3a"
                }
              ],
              "rule_instance": [
                {
                  "config_instance_id": 4,
                  "end_date": "Sat, 18 May 2019 22:04:14 GMT",
                  "fail_count": 24,
                  "id": 4,
                  "query": "SELECT dmx_id, FALSE as status, '' as message FROM source.s3_e8f3658a7d6c40eeb2efc2076d969d3a where not( length(market_id) = 6)",
                  "rule_id": 1009,
                  "sequence": 0,
                  "start_date": "Sat, 18 May 2019 22:04:15 GMT",
                  "status": "Completed"
                }
              ]
            }

        """
    strategy = S3ValidationStrategy(**request.args.to_dict(flat=True))
    service = ServiceFactory('data_validator.validation_engine', 'ValidationEngine', request, {"strategy": strategy})
    return service.invoke()


@validation.route('/redshift-s3')
def validation_end_to_end():
    """Run the validation at both Redshift and S3 layer.

    Parameters:

    Examples:

    """
    job = JobFactory('three_stage_validation', 'ThreeStageValidation', request)
    job.invoke()


@validation.route('/success_records')
def success_records():
    """Publish the success records for the given run.

    Parameters:
        rs_connector_id (string): to assign the Redshift resource to be used for the execution.
        config_instance_id (int): the instance id which is used to reference the previous runs.
        success_schema (string): [Optional] default has been set to `validation`.
        stage (int): [Optional] default has been set to 1, in case of chained validations for different instances
                they can be identified by the stage no. provided.
        success_table (string): [Optional] default is created at runtime using the format
                `tablename_success_<config_instance_id>`
        dtypes (JSON): [Optional] if provided will create the success table with the given data type in redshift.

    Returns:
        dict: which has a boolean to show the status of successfully published the success records into the S3.

    Examples:
            >>> import requests
            >>> requests.get('http://localhost:5001/api/validation/success_records?rs_connector_id=12'
            ...              '&config_instance_id=18')
            {
              "config_instance": [
                {
                  "config_id": 32,
                  "created_by": null,
                  "detail_schema_name": null,
                  "detail_table_name": "poc/symphony_sales/Symphony_Sales.csv_detail_31",
                  "end_date": "Fri, 31 May 2019 17:16:43 GMT",
                  "fail_count": 1,
                  "failure_schema_name": null,
                  "failure_table_name": null,
                  "id": 31,
                  "rule_type": "user_defined",
                  "stage": 1,
                  "start_date": "Fri, 31 May 2019 17:16:23 GMT",
                  "status": "Completed",
                  "success_count": NaN,
                  "success_schema_name": null,
                  "success_table_name": null,
                  "target_schema": "datamax1",
                  "target_table": "poc/symphony_sales/Symphony_Sales.csv",
                  "total_count": NaN
                },
                {
                  "config_id": 32,
                  "created_by": null,
                  "detail_schema_name": "validation",
                  "detail_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198_detail_31",
                  "end_date": "Fri, 31 May 2019 17:17:53 GMT",
                  "fail_count": 0,
                  "failure_schema_name": null,
                  "failure_table_name": null,
                  "id": 31,
                  "rule_type": "system_defined",
                  "stage": 2,
                  "start_date": "Fri, 31 May 2019 17:17:16 GMT",
                  "status": "Completed",
                  "success_count": 24.0,
                  "success_schema_name": "validation",
                  "success_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198",
                  "target_schema": "temp",
                  "target_table": "s3_807d3fe81ccc41f9bb6517b01ae7c198",
                  "total_count": 24.0
                },
                {
                  "config_id": 32,
                  "created_by": null,
                  "detail_schema_name": "validation",
                  "detail_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198_detail_31",
                  "end_date": "Fri, 31 May 2019 17:18:52 GMT",
                  "fail_count": 1,
                  "failure_schema_name": "validation",
                  "failure_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198_failed_31",
                  "id": 31,
                  "rule_type": "user_defined",
                  "stage": 3,
                  "start_date": "Fri, 31 May 2019 17:18:34 GMT",
                  "status": "Completed",
                  "success_count": 23.0,
                  "success_schema_name": "validation",
                  "success_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198_success_31",
                  "target_schema": "validation",
                  "target_table": "s3_807d3fe81ccc41f9bb6517b01ae7c198",
                  "total_count": 24.0
                }
              ]
            }

    """
    service = ServiceFactory('data_validator.validation_response', 'ConstructSuccessRecords', request)
    return service.invoke()


@validation.route('/failed_records')
def failed_records():
    """Publish the failed records for the given run.

        Parameters:
            rs_connector_id (int): to assign the Redshift resource to be used for the execution.
            instance_id (int): the instance id which is used to reference the previous runs.
            success_schema (string): [Optional] default has been set to `validation`.
            stage (int): [Optional] default has been set to 1, in case of chained validations for different instances
                    they can be identified by the stage no. provided.
            failed_table (string): [Optional] default is created at runtime using the format
                    `tablename_failed_<config_instance_id>`

        Returns:
            dict: which has a boolean to show the status of successfully published the failed records into the S3.

        Examples:
            >>> import requests
            >>> requests.get('http://localhost:5000/api/validation/failed_records?rs_connector_id=12'
            ...              '&config_instance_id=18')
            {
              "config_instance": [
                {
                  "config_id": 32,
                  "created_by": null,
                  "detail_schema_name": null,
                  "detail_table_name": "poc/symphony_sales/Symphony_Sales.csv_detail_31",
                  "end_date": "Fri, 31 May 2019 17:16:43 GMT",
                  "fail_count": 1,
                  "failure_schema_name": null,
                  "failure_table_name": null,
                  "id": 31,
                  "rule_type": "user_defined",
                  "stage": 1,
                  "start_date": "Fri, 31 May 2019 17:16:23 GMT",
                  "status": "Completed",
                  "success_count": NaN,
                  "success_schema_name": null,
                  "success_table_name": null,
                  "target_schema": "datamax1",
                  "target_table": "poc/symphony_sales/Symphony_Sales.csv",
                  "total_count": NaN
                },
                {
                  "config_id": 32,
                  "created_by": null,
                  "detail_schema_name": "validation",
                  "detail_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198_detail_31",
                  "end_date": "Fri, 31 May 2019 17:17:53 GMT",
                  "fail_count": 0,
                  "failure_schema_name": null,
                  "failure_table_name": null,
                  "id": 31,
                  "rule_type": "system_defined",
                  "stage": 2,
                  "start_date": "Fri, 31 May 2019 17:17:16 GMT",
                  "status": "Completed",
                  "success_count": 24.0,
                  "success_schema_name": "validation",
                  "success_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198",
                  "target_schema": "temp",
                  "target_table": "s3_807d3fe81ccc41f9bb6517b01ae7c198",
                  "total_count": 24.0
                },
                {
                  "config_id": 32,
                  "created_by": null,
                  "detail_schema_name": "validation",
                  "detail_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198_detail_31",
                  "end_date": "Fri, 31 May 2019 17:18:52 GMT",
                  "fail_count": 1,
                  "failure_schema_name": "validation",
                  "failure_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198_failed_31",
                  "id": 31,
                  "rule_type": "user_defined",
                  "stage": 3,
                  "start_date": "Fri, 31 May 2019 17:18:34 GMT",
                  "status": "Completed",
                  "success_count": 23.0,
                  "success_schema_name": "validation",
                  "success_table_name": "s3_807d3fe81ccc41f9bb6517b01ae7c198_success_31",
                  "target_schema": "validation",
                  "target_table": "s3_807d3fe81ccc41f9bb6517b01ae7c198",
                  "total_count": 24.0
                }
              ]
            }

        """
    service = ServiceFactory('data_validator.validation_response', 'ConstructFailedRecords', request)
    return service.invoke()

@validation.route('/redshift/failedrecordstos3')
def failed_records_tos3():
    """Run the validaiton and export failed records to s3.

        Parameters:
            rs_connector_id (int): to assign the Redshift resource to be used for the execution.
            s3_connector_id (int): to assign the S3 resource to be used for the execution.
            config_id (int): the configuration id already present in the system, required to run the validations
            target_table (string): [Optional] table on which the validation has to be executed
            target_schema (string): [Optional] target schema
            detail_schema (string):  schema in which the detailed table is created to store the failed records
            destination_bucket (string): S3 bucket name
            destination_object (string): folder path into which the file has to be dumped
            parallel (string): [Optional] to save the file on s3 parellely

        Returns:
            JSON: 

            Examples:
        To dump multiple files (parallel fashion) having the header object will have a request format.

        >>> import requests
        >>> requests.get("http://localhost:5000/api/redshift/failedrecordstos3?rs_connector_id=12&s3_connector_id=3&"
        ...              "target_schema=source&target_table=source&config_id=1&detail_schema=validation&"
        ...              "destination_bucket=datamax1&destination_object=poc/sample/&parallel=True")
        ...
        {
          "config_instance": [
              {
                  "config_id": 299,
                  "created_by": null,
                  "detail_schema_name": "testing_stage",
                  "detail_table_name": "aft_ddd_wkly_detail_4102",
                  "end_date": "Tue, 30 Jul 2019 10:21:39 GMT",
                  "fail_count": 1130322,
                  "failure_s3_path": null,
                  "failure_schema_name": null,
                  "failure_table_name": null,
                  "id": 4102,
                  "is_archieved": false,
                  "rule_type": "user_defined",
                  "stage": 1,
                  "start_date": "Tue, 30 Jul 2019 10:21:14 GMT",
                  "status": "Completed",
                  "success_count": 1891423,
                  "success_s3_path": null,
                  "success_schema_name": null,
                  "success_table_name": null,
                  "target_schema": "testing_stage",
                  "target_table": "aft_ddd_wkly"
              }
          ],
          "data": {
              "AcceptRanges": "bytes",
              "ContentLength": 568911069,
              "ContentType": "application/x-www-form-urlencoded; charset=utf-8",
              "ETag": "\"2611f3017cfba5e232daac7d1284cb19-9\"",
              "LastModified": "Tue, 30 Jul 2019 10:22:06 GMT",
              "Metadata": {},
              "ResponseMetadata": {
                  "HTTPHeaders": {
                      "accept-ranges": "bytes",
                      "content-length": "568911069",
                      "content-type": "application/x-www-form-urlencoded; charset=utf-8",
                      "date": "Tue, 30 Jul 2019 10:22:55 GMT",
                      "etag": "\"2611f3017cfba5e232daac7d1284cb19-9\"",
                      "last-modified": "Tue, 30 Jul 2019 10:22:06 GMT",
                      "server": "AmazonS3",
                      "x-amz-id-2": "2yOgERE1RmSzDmtZkyx9B//OGo9LvUESRvI1zlXIlEalxREIT60+Z7uOZ9zJOhVyy+iFssdgHPU=",
                      "x-amz-request-id": "85F391075F6A8DD1",
                      "x-amz-server-side-encryption": "AES256"
                  },
                  "HTTPStatusCode": 200,
                  "HostId": "2yOgERE1RmSzDmtZkyx9B//OGo9LvUESRvI1zlXIlEalxREIT60+Z7uOZ9zJOhVyy+iFssdgHPU=",
                  "RequestId": "85F391075F6A8DD1",
                  "RetryAttempts": 0
              },
              "ServerSideEncryption": "AES256"
          },
          "rule_instance": [
              {
                  "config_instance_id": 4102,
                  "end_date": "Tue, 30 Jul 2019 10:21:26 GMT",
                  "fail_count": 0,
                  "id": 9042,
                  "query": "with varformat as (select case when 'YYMMDD' =  'YYYY-MM-DD' then '^[0-9][0-9][0-9][0-9]-(((0)[0-9])|((1)[0-2]))-(((0)[0-9])|([1-2][0-9])|([3][0-1]))$' when 'YYMMDD' =  'YYYY-DD-MM' then '^[0-9][0-9][0-9][0-9]-(((0)[0-9])|([1-2][0-9])|([3][0-1]))-(((0)[0-9])|((1)[0-2]))$' when 'YYMMDD' =  'YYYY/MM/DD' then '^[0-9][0-9][0-9][0-9]/(((0)[0-9])|((1)[0-2]))/(((0)[0-9])|([1-2][0-9])|([3][0-1]))$' when 'YYMMDD' =  'YYYY/DD/MM' then '^[0-9][0-9][0-9][0-9]/(((0)[0-9])|([1-2][0-9)|([3][0-1]))/(((0)[0-9])|((1)[0-2]))$' when 'YYMMDD' = 'DD-MON-YY' then '^(((0)[0-9])|([1-2][0-9])|([3][0-1]))-(JAN(UARY)?|FEB(RUARY)?|MAR(CH)?|APR(IL)?|MAY|JUN(E)?|JUL(Y)?|AUG(UST)?|SEP(TEMBER)?|OCT(OBER)?|NOV(EMBER)?|DEC(EMBER)?)-([0-9][0-9])$' end as format) select dmx_id, FALSE as status, '' as message from testing_stage.aft_ddd_wkly cross join varformat where WEEK_ENDT !~ varformat.format",
                  "rule_id": 1025,
                  "stage": 1,
                  "start_date": "Tue, 30 Jul 2019 10:21:23 GMT",
                  "status": "Completed"
              },
              {
                  "config_instance_id": 4102,
                  "end_date": "Tue, 30 Jul 2019 10:21:29 GMT",
                  "fail_count": 1130322,
                  "id": 9043,
                  "query": "select False  as status,  '' as message, DMX_ID as DMX_ID from  testing_stage.aft_ddd_wkly where sra2 is null or len(trim(sra2)) = 0",
                  "rule_id": 1035,
                  "stage": 1,
                  "start_date": "Tue, 30 Jul 2019 10:21:27 GMT",
                  "status": "Completed"
              },
              {
                  "config_instance_id": 4102,
                  "end_date": "Tue, 30 Jul 2019 10:21:33 GMT",
                  "fail_count": 1130322,
                  "id": 9044,
                  "query": "select False  as status,  '' as message, DMX_ID as DMX_ID from  testing_stage.aft_ddd_wkly where OUTLET_ZIP is null or len(trim(OUTLET_ZIP)) = 0",
                  "rule_id": 1035,
                  "stage": 1,
                  "start_date": "Tue, 30 Jul 2019 10:21:30 GMT",
                  "status": "Completed"
              },
              {
                  "config_instance_id": 4102,
                  "end_date": "Tue, 30 Jul 2019 10:21:36 GMT",
                  "fail_count": 0,
                  "id": 9045,
                  "query": "SELECT dmx_id, FALSE as status, '' as message FROM testing_stage.aft_ddd_wkly where not (length(OUTLET_ZIP) = 5)",
                  "rule_id": 1009,
                  "stage": 1,
                  "start_date": "Tue, 30 Jul 2019 10:21:34 GMT",
                  "status": "Completed"
              }
          ]
        }

    """
    service = ServiceFactory('data_validator.two_stage_redshift_validation', 'TwoStageRedshiftValidation', request)
    return service.invoke()
