#   datamax/api/controller/config.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
"""Configuration module for the validation engine.

See Also:

    :func:`~controller.project` To manage projects to which config belongs.

"""
import json
import logging

from flask import Blueprint, jsonify, request

from models import Config

config = Blueprint('config', __name__)

from app import db


@config.route('/')
def index():
    """Fetch the latest configs from the postgres db.

    Returns:
        dict: a JSON object

    Examples:
        Get a few configs from the postgres db.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/config/')
        configs: [{
              "created_by": null,
              "creation_date": "Thu, 20 Jun 2019 07:34:17 GMT",
              "file_id": null,
              "id": 97,
              "is_active": true,
              "modification_date": "Thu, 20 Jun 2019 07:34:17 GMT",
              "modified_by": null,
              "name": "CT_All",
              "params": {
                "system_defined": [],
                "user_defined": []
              },
              "std_object": "Unknown",
              "version": 9
            },
            {
              "created_by": null,
              "creation_date": "Thu, 20 Jun 2019 07:33:22 GMT",
              "file_id": null,
              "id": 96,
              "is_active": true,
              "modification_date": "Thu, 20 Jun 2019 07:33:22 GMT",
              "modified_by": null,
              "name": "Test_101",
              "params": {
                "system_defined": [
                  {
                    "id": 9002,
                    "parameters": [
                      {
                        "datatype": "any",
                        "param": "colname",
                        "type": "column",
                        "value": "id"
                      },
                      {
                        "param": "tablename",
                        "value": ""
                      },
                      {
                        "param": "schema",
                        "type": "schema",
                        "value": ""
                      }
                    ]
                  }
                ],
                "user_defined": [
                  {
                    "id": "1010",
                    "parameters": [
                      {
                        "param": "colname",
                        "value": "hex"
                      },
                      {
                        "param": "operator",
                        "value": "<"
                      },
                      {
                        "param": "thresholdval",
                        "value": "4"
                      }
                    ]
                  }]
    """
    return jsonify(configs=[c.serialize for c in db.session.query(Config).
                   order_by(Config.creation_date.desc()).all()])


@config.route('/add', methods=['GET', 'POST'])
def add():
    """Add configuration into the postgres db for a given file.

    Save the configuration in ``config`` table of the postgres db and generate a unique ID which will be used by
    other services to retrieve the configuration required to run the validation. The API support both GET and POST
    protocol.

    Parameters:
        file_id (int): file id
        project_id (int): project reference to which the config belongs
        name (string): Identifier for the unique configuration which share different versions
        std_object (string): [Optional] mapping of the current config to any specific standard object
        user_defined (JSON): [Optional] the configuration required to run the validations.
        system_defined (JSON): [Optional] these include the data type checks.

    Returns:
        dict: a JSON object which has the ``config_id`` generated for the given configuration.

    Examples:
        Add configuration against the standard object.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/config/add?project=DEFAULT&user_defined='
        ...              '[{ "parameters": [ { "param": "colname", "value": "NRX_AMOUNT" }, { "param":'
        ...              '"tablename", "value": "s3__121_1212" } ], "description": "Check if all values in'
        ...              'column are of float data type", "id": 1004, "name": "CheckFloatDataType" }]&'
        ...              'std_object=unknown&name=dummy retail sales&project_id=12')
        {
            "config": [
                {
                    "created_by": null,
                    "creation_date": "Mon, 27 May 2019 09:46:12 GMT",
                    "id": 27,
                    "is_active": true,
                    "modification_date": "Mon, 27 May 2019 09:46:12 GMT",
                    "modified_by": null,
                    "name": "dummy retail sales",
                    "params": [
                        {
                            "datatype": "varchar",
                            "id": 1009,
                            "name": "CheckColumnLength",
                            "parameters": [
                                {
                                    "param": "tablename",
                                    "value": "s3_e8f3658a7d6c40eeb2efc2076d969d3a"
                                },
                                {
                                    "datatype": "varchar",
                                    "param": "colname",
                                    "type": "column",
                                    "value": "market_id"
                                },
                                {
                                    "param": "operator",
                                    "value": "="
                                },
                                {
                                    "param": "thresholdval",
                                    "value": "6"
                                },
                                {
                                    "param": "schema",
                                    "type": "schema",
                                    "value": "source"
                                }
                            ]
                        },
                        {
                            ...
                        }
                    ],
                    "std_object": "unknown",
                    "version": 1
                }
            ]
        }

    """
    name = request.args.get('name', type=str)
    std_object = request.args.get('std_object', type=str, default=None)
    user_defined = request.args.get('user_defined', type=str, default="[]")
    system_defined = request.args.get('system_defined', type=str, default="[]")
    file_id = request.args.get('file_id', type=int)
    project_id = request.args.get('project_id', type=int)

    config_params = {"user_defined": json.loads(user_defined), "system_defined": json.loads(system_defined)}

    old_config = db.session.query(Config).filter(Config.name == name).order_by(Config.version.desc()).first()

    # in case of exact match - return the current old config queried
    if old_config:
        if (old_config.params == config_params) & (old_config.std_object == std_object) & (
                old_config.file_id == file_id) & (old_config.project_id == project_id):
            return jsonify(config=old_config.serialize)

    # register the new config
    version = old_config.version + 1 if old_config else 1
    new_config = Config(params=config_params, name=name, version=version, std_object=std_object, file_id=file_id,
                        is_active=True, project_id=project_id)
    db.session.add(new_config)
    db.session.commit()

    return jsonify(conifg=new_config.serialize)


@config.route('/search')
def search():
    """Retrieve the configuration from the postgres db.

    Fetch the configurations from postgres for a given standard object.

    Parameters:
        name (string): [Optional] filter result on pattern given on user defined name.
        version (int): [Optional] the version of the config to be fetched
        std_object (string): [Optional] standard object for which the configuration has to be fetched.
        connector_id (int): [Optional] connection id for the resource.

    Returns:
        dict : a JSON object with all the filled ``parameters``.

    Examples:
        Search for all the versions of `config` of the standard object.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/config/search?'
        ...              'std_object=SHS.Retail Sales')
        {
          "configs": [
            {
              "created_by": null,
              "creation_date": "Tue, 14 May 2019 17:05:43 GMT",
              "id": 1,
              "is_active": true,
              "modification_date": "Tue, 14 May 2019 17:05:43 GMT",
              "modified_by": null,
              "name": "symphony sales config",
              "params": [
                {
                  "description": "Check if all values in column are of float data type",
                  "id": 1004,
                  "name": "CheckFloatDataType",
                  "parameters": [
                    {
                      "param": "colname",
                      "value": "NRX_AMOUNT"
                    },
                    {
                      "param": "tablename",
                      "value": "s3__121_1212"
                    }
                  ]
                }
              ],
              "std_object": "SHS.Retail Sales",
              "version": 1
            }
          ]
        }

        Search by providing incomplete name in the parameter.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/config/search?'
        ...              'name=sales')
        {
          "configs": [
            {
              "created_by": null,
              "creation_date": "Tue, 14 May 2019 17:05:43 GMT",
              "id": 1,
              "is_active": true,
              "modification_date": "Tue, 14 May 2019 17:05:43 GMT",
              "modified_by": null,
              "name": "symphony sales config",
              "params": [
                {
                  "description": "Check if all values in column are of float data type",
                  "id": 1004,
                  "name": "CheckFloatDataType",
                  "parameters": [
                    {
                      "param": "colname",
                      "value": "NRX_AMOUNT"
                    },
                    {
                      "param": "tablename",
                      "value": "s3__121_1212"
                    }
                  ]
                }
              ],
              "std_object": "SHS.Retail Sales",
              "version": 1
            }
          ]
        }


    """
    is_active = request.args.get('is_active', type=bool, default=True)
    std_object = request.args.get('std_object', type=str, default=None)
    version = request.args.get('version', type=int, default=None)
    name = request.args.get('name', type=str, default=None)
    project_id = request.args.get('project_id', type=int, default=None)

    search_filter = [Config.is_active == is_active]
    search_filter.append(Config.std_object == std_object) if std_object else None
    search_filter.append(Config.version == version) if version else None
    search_filter.append(Config.name.like('%' + name + '%')) if name else None
    search_filter.append(Config.project_id == project_id) if project_id else None

    configs = db.session.query(Config).filter(*search_filter).all()
    return jsonify(configs=[c.serialize for c in configs])


@config.route('/get')
def get():
    """Retrieve the configuration from the postgres db.

    Fetch the configurations from postgres for a given standard object.

    Parameters:
        id (int): the id of the config to be fetched

    Returns:
        dict : a JSON object with all the filled ``parameters``.

    Examples:
        Fetch the specific config using the id.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/config/get?'
        ...              'id=1')
        {
          "config": [
            {
              "created_by": null,
              "creation_date": "Tue, 14 May 2019 17:05:43 GMT",
              "id": 1,
              "is_active": true,
              "modification_date": "Tue, 14 May 2019 17:05:43 GMT",
              "modified_by": null,
              "name": "symphony sales config",
              "params": [
                {
                  "description": "Check if all values in column are of float data type",
                  "id": 1004,
                  "name": "CheckFloatDataType",
                  "parameters": [
                    {
                      "param": "colname",
                      "value": "NRX_AMOUNT"
                    },
                    {
                      "param": "tablename",
                      "value": "s3__121_1212"
                    }
                  ]
                }
              ],
              "std_object": "SHS.Retail Sales",
              "version": 1
            }
          ]
        }


    """
    config_id = request.args.get('id', type=int)
    get_config = db.session.query(Config).filter(Config.id == config_id).first()
    if get_config:
        return jsonify(config=get_config.serialize)
    return jsonify(config={})


@config.route('/build', methods=['GET', 'POST'])
def build():
    """Build the data type based configuration rules by reading rules from postgres db.

    Depending upon the type of columns provided by the `request` the configuration containing the `dtype`
    checks is built at run-time. The sample input of the shape is as follows:
    [{"name": "col1", "dtype": "numeric"}, {"name": "col2", "dtype": "varchar"},
    {"name": "col3", "dtype":"date", "format": "YYYY-MM-DD"}]

    Using this shape, the automated rules are generated which will run the system based validation
    to consume the data into redshift table with exact dtypes.


    Parameters:
        shape (JSON): a collection with both column name and its dtype

    Returns:
        dict : a JSON object with all the filled ``parameters``.

    Examples:
        Fetch the specific config using the id.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/config/build?'
        ...              'shape=[{"name": "col_1", "dtype": "numeric"}, '
        ...              '{"name": "col2", "dtype": "varchar}, {"name":"col3", "dtype": "date",'
        ...              '"format": "YYYY-MM-DD"}]')
        {
          "config": [
            {
              "created_by": null,
              "creation_date": "Tue, 14 May 2019 17:05:43 GMT",
              "id": 1,
              "is_active": true,
              "modification_date": "Tue, 14 May 2019 17:05:43 GMT",
              "modified_by": null,
              "name": "symphony sales config",
              "params": [
                {
                  "description": "Check if all values in column are of float data type",
                  "id": 1004,
                  "name": "CheckFloatDataType",
                  "parameters": [
                    {
                      "param": "colname",
                      "value": "NRX_AMOUNT"
                    },
                    {
                      "param": "tablename",
                      "value": "s3__121_1212"
                    }
                  ]
                }
              ],
              "std_object": "SHS.Retail Sales",
              "version": 1
            }
          ]
        }


    """
    shape = json.loads(request.args.get('shape', type=str))
    build_config = []

    try:
        for column in shape:
            if column["dtype"] == 'numeric':
                build_config.append(_rule_numeric(column["name"]))
            elif column["dtype"] == 'date':
                build_config.append(_rule_date(column["name"], column["format"]))
        return jsonify(system_defined=build_config)
    except Exception as e:
        logging.exception(e)
        return jsonify(message='Invalid shape')


@config.route('/update', methods=['GET', 'POST'])
def update():
    """Update the configuration into the postgres db for a given file.

        Save the configuration in ``config`` table of the postgres db and generate a unique ID which will be used by
        other services to retrieve the configuration required to run the validation. The API support both GET and POST
        protocol.

        Parameters:
            file_id (int): file id
            connector_id (int): connector id of the resource.
            name (string): Identifier for the unique configuration which share different versions
            std_object (string): [Optional] mapping of the current config to any specific standard object
            user_defined (JSON): [Optional] the configuration required to run the validations.
            system_defined (JSON): [Optional] these include the data type checks.

        Returns:
            dict: a JSON object which has the ``config_id`` generated for the given configuration.

        Examples:
            Add configuration against the standard object.

            >>> import requests
            >>> requests.get('http://localhost:5001/api/config/update?id=99&user_defined='
            ...              '[{ "parameters": [ { "param": "colname", "value": "NRX_AMOUNT" }, { "param":'
            ...              '"tablename", "value": "s3__121_1212" } ], "description": "Check if all values in'
            ...              'column are of float data type", "id": 1004, "name": "CheckFloatDataType" }]&'
            ...              'std_object=unknown&name=dummy retail sales&file_id=2&version=1&connector_id=1')
            {
                "config": [
                    {
                        "created_by": null,
                        "creation_date": "Mon, 27 May 2019 09:46:12 GMT",
                        "id": 27,
                        "is_active": true,
                        "modification_date": "Mon, 27 May 2019 09:46:12 GMT",
                        "modified_by": null,
                        "name": "dummy retail sales",
                        "params": [
                            {
                                "datatype": "varchar",
                                "id": 1009,
                                "name": "CheckColumnLength",
                                "parameters": [
                                    {
                                        "param": "tablename",
                                        "value": "s3_e8f3658a7d6c40eeb2efc2076d969d3a"
                                    },
                                    {
                                        "datatype": "varchar",
                                        "param": "colname",
                                        "type": "column",
                                        "value": "market_id"
                                    },
                                    {
                                        "param": "operator",
                                        "value": "="
                                    },
                                    {
                                        "param": "thresholdval",
                                        "value": "6"
                                    },
                                    {
                                        "param": "schema",
                                        "type": "schema",
                                        "value": "source"
                                    }
                                ]
                            },
                            {
                                ...
                            }
                        ],
                        "std_object": "unknown",
                        "version": 2
                    }
                ]
            }

    """
    id = request.args.get('id', type=int)
    name = request.args.get('name', type=str)
    std_object = request.args.get('std_object', type=str)
    user_defined = request.args.get('user_defined', type=str, default="[]")
    system_defined = request.args.get('system_defined', type=str, default="[]")
    file_id = request.args.get('file_id', type=int)
    project_id = request.args.get('project_id', type=int)
    is_active = request.args.get('is_active', type=bool)
    version = request.args.get('version', type=int)
    config_params = {"user_defined": json.loads(user_defined), "system_defined": json.loads(system_defined)}

    db.session.query(Config).filter(Config.id == id).update({
        "name": name, "version": version + 1, "std_object": std_object, "file_id": file_id, "is_active": is_active,
        "project_id": project_id, "params": config_params})
    db.session.commit()
    return jsonify(config={"name": name, "version": version + 1, "std_object": std_object, "file_id": file_id,
                           "is_active": is_active, "project_id": project_id, "params": config_params})


@config.route('/delete')
def delete():
    """Delete the configuration from the postgres db.

    Parameters:
        config_id (int): id of the configuration.

    Examples:
         Delete the configuration from the system using the API call.

         >>> import requests
         >>> requests.get('http://localhost:5001/api/config/delete?config_id=12')
         {
            "message": "deleted successfully"
         }
    """
    config_id = request.args.get('id', type=int)
    config_to_delete = db.session.query(Config).filter(Config.id == config_id).first()
    if config_to_delete:
        db.session.delete(config_to_delete)
        db.session.commit()
        return jsonify(message='deleted successfully')
    return jsonify(message='something went wrong')


def _rule_varchar(name: str, format=None):
    pass


def _rule_numeric(name: str, format=None):
    rule_id = 9002
    parameters = [{"type": "column", "param": "colname", "value": name, "datatype": "any"},
                  {"param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]
    return {"parameters": parameters, "id": rule_id}


def _rule_date(name: str, format: str):
    rule_id = 9001
    parameters = [{"type": "column", "param": "colname", "value": name, "datatype": "date"},
                  {"type": "table", "param": "tablename", "value": ""},
                  {"type": "param", "param": "format", "value": format, "datatype": "varchar"},
                  {"type": "schema", "param": "schema", "value": ""}]
    return {"parameters": parameters, "id": rule_id}
