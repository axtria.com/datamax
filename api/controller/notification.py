import datetime
from functools import wraps

import jwt
import re
from flask import Blueprint, request, jsonify, session
from sqlalchemy import and_, or_, func
from werkzeug.security import generate_password_hash, check_password_hash

from models import User, UserGroup, UserGroupUserMapping, UserGroupFunctionalityMapping, FunctionalityMaster
from services import ServiceFactory
from utils.admn_permission import usermanagment

from controller.exchange import token_required


notification = Blueprint('notification', __name__)

from app import db, secret

@notification.route('/add_notification', methods=['get', 'post'])
@token_required
def addnotification():
    """Notification - to insert the notification.
    Returns:
        dict: Returns the notification successfully aded

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/notification/add_notification?name=Outage&details=notification content&'
        ...             'title=notificationtitle&usergroup_ids=1,2,3&all_users=true')

        {"Contents":"Notification sent successfully."}

    """
    service = ServiceFactory('notification', 'SendNotification', request)
    resp = service.invoke()
    return resp