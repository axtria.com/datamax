import datetime

from flask import Blueprint, jsonify, request

from models import Test

test = Blueprint('test', __name__)
import re
from app import db


#Insert_Rows = [(200,'Dummy_User_1','Pass@123'),(211,'Dummy_User_2','Pass@123')]
@test.route('/add')
def add():
    m=[]
    Item_list = request.args.get('user_list')
    x = Item_list.split('),(')
    for i in x:
        y = i.split(',')
        m.append(y)
    for a in m :
        l= len(m)
        sc = a[0].strip('(')
        #sc=sc[1:-1]
        id = int(sc)
        username = (str(a[1]))[1:-1]
        password = (str(a[2].strip(')')))[1:-1]
        new_project = Test(id=id, username=username, password=password)
        db.session.add(new_project)
        db.session.commit()
    return jsonify(message='Users Table has been updated successfully')
