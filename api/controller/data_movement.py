#   datamax/api/controller/data-movement.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
"""Data movement between Redshift and S3."""

from flask import Blueprint, request

from services import ServiceFactory

data_movement = Blueprint('data_movement', __name__)


@data_movement.route("/s3_to_redshift")
def s3_to_redshift():
    """Move a folder from S3 to Redshift table.

    API based service to move the file present in a folder to the Redshift table. It is assumed that the folder
    has same file with same structure in the folder. This service uses Redshift Spectrum to migrate the data to
    Redshift.

    Parameters:
        s3_connector_id (int): Connections will be established at run time for the given S3
        rs_connector_id (int): connections will be established at run time for given Redshift
        source_bucket (int): S3 source bucket name
        source_object (string): S3 folder ending with a trailing slash
        destination_schema (string): Redshift based schema to be used to populate the file
        header_object (string): S3 file path ending with extension to create the table
        destination_table (string): [optional] Redshift table to be created
        delimiter (string): [optional] S3 file delimiter
        file_format (string): [optional] default is set to 'TEXTFILE'
        data_header (bool): [optional] True if the files inside the source object has header in them as first line.

    Returns:
        dict

    Examples:
        Single file having the header object will have a request format

            >>> import requests
            >>> requests.get('http://localhost:5001/api/data_movement/s3_to_redshift?s3_connector_id=1'
            ...                 '&rs_connector_id=2&source_bucket=datamax1'
            ...                 '&source_object=poc/symphony_sales/&destination_schema=source&header_object='
            ...                 'poc/symphony_sales/Symphony_Sales.csv&data_header=True')

    Notes:
        ``Profile`` is assumed to have *Redshift Spectrum* enabled as the service uses spectrum query to
            transfer the file from S3 to Redshift.
    """
    service = ServiceFactory('data_movement.s3_to_redshift', 'S3ToRedshift', request)
    return service.invoke()


@data_movement.route("/redshift_to_s3")
def redshift_to_s3():
    """Dump data from Redshift based table to S3.

    For a given table name and schema, the table is dumped to the S3 layer.

    Parameters:
        s3_connector_id (int):  Connections will be established at run time for the given profile
        rs_connector_id (int): Connection to Redshift will be established for the given ID.
        source_schema (string): schema name in which the source table is present
        source_table (string): table name which has to be copied
        destination_bucket (string): S3 bucket name
        destination_object (string): folder path into which the file has to be dumped
        parallel (bool): True - to write chunks of file into the destination folder
        header (string): 'HEADER' if headers are required in the S3 file, else leave empty.

    Returns:
        JSON:

    Examples:
        To dump multiple files (parallel fashion) having the header object will have a request format.

        >>> import requests
        >>> requests.get("http://localhost:5000/api/data_movement/redshift_to_s3?project=DEFAULT&source_schema=source"
        ...                 "&source_table=s3_new_table&destination_bucket=datamax1&destination_object=poc/sample/"
        ...                 "&parallel=True&header=HEADER")

    """
    service = ServiceFactory('data_movement.redshift_to_s3', 'RedshiftToS3', request)
    return service.invoke()


@data_movement.route("/redshift_to_redshift")
def redshift_to_redshift():
    """Create new table from old table and migrate the data.

    Parameters:
        rs_connector_id (string):  Connections will be established at run time for the given redshift
        source_schema (string): schema name in which the source table is present
        source_table (string): table name which has to be copied
        destination_schema (string): destination schema name
        destination_table (string): [Optional] in case not found, then same name is used in the destination schema


    Returns:
        dict

    Notes:
        - `destination_bucket` is missing then, it will be same as source bucket.
        - `destination_object` is missing then, it will be same as source object.

    """
    service = ServiceFactory('data_movement.redshift_to_redshift', 'RedshiftToRedshift', request)
    return service.invoke()


@data_movement.route("/s3_to_s3")
def s3_to_s3():
    """Copy a folder in S3 to S3.

    Utility to copy the file from S3 to S3, for a given profile.

    Parameters:
        s3_connector_id (int): Connections will be established at run time for the given ID.
        source_bucket (int): S3 source bucket name
        source_object (string): S3 folder ending with a trailing slash
        destination_bucket (string): S3 bucket name
        destination_object (string): folder path into which the file has to be dumped

    Returns:
        dict

    """
    service = ServiceFactory('data_movement.s3_to_s3', 'S3ToS3', request)
    return service.invoke()
