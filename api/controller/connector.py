#   datamax/api/controller/connector.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
"""Connector management API.

Connector module to manage connections with the resources like Redshift, S3, SFTP and Snowflake.
It is assumed that passwords have been encrypted before hand using the API mentioned below.
As there is a decoder algorithm written which decodes the password by default.

See Also:

    :func:`~controller.project` To manage projects

    :func:`~controller.operations.encrypt` Encryption API to encode string.

Notes:
    Encryption used by the system is Fernet cryptography.

"""

from flask import Blueprint, request

from services import ServiceFactory

connector = Blueprint('connector', __name__)


@connector.route('/')
def index():
    return "Test Connector Management"


@connector.route('/test_s3')
def test_s3():
    service = ServiceFactory('connector', 'TestS3Connection', request)
    return service.invoke()


@connector.route('/test_sftp')
def test_sftp():
    service = ServiceFactory('connector', 'TestSftpConnection', request)
    return service.invoke()


@connector.route('/test_snowflake')
def test_snowflake():
    service = ServiceFactory('connector', 'TestSnowflakeConnection', request)
    return service.invoke()

@connector.route('/list_snowflake_schemas')
def list_sf_schemas():
    service = ServiceFactory('connector', 'ListSnowFlakeSchema', request)
    return service.invoke()