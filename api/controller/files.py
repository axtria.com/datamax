#   datamax/api/controller/files.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
"""Files management module.

File API is used to store the structure and access information of the files
which are being used by the system.

See Also:

    :func:`~controller.project` To manage projects

    :func:`~controller.connector` Connection management
"""
import datetime
import json

from flask import Blueprint, jsonify, request


from models import Files

files = Blueprint('files', __name__)

from app import db


@files.route('/')
def index():
    """Fetch all the files present in the system.

    Returns:
        dict: JSON of the files stored in the system.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/files/')
        {
            "files": [
                {
                    "id": 1,
                    "name": "alignment_file_csv",
                    "creation_date": null,
                    "created_by": null,
                    "modification_date": null,
                    "modification_by": null,
                    "is_active": True,
                    "shape": [],
                    "connector_id": 1
                },
                ...
            ]
        }

    """
    return jsonify(files=[c.serialize for c in db.session.query(Files).
                   order_by(Files.creation_date.desc()).all()])


@files.route('/search')
def search():
    """Search all the files present in the system.

    Parameters:
        name (str): [Optional] name of the file
        project_id (int): [Optional] project to which the file belong
        is_active (bool): [Optional] status of the file

    Returns:
        dict: JSON of the files stored in the system.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/files/search?name=alignment')
        {
            "files": [
                {
                    "id": 1,
                    "name": "alignment_file_csv",
                    "description": "dummy",
                    "creation_date": null,
                    "created_by": null,
                    "modification_date": null,
                    "modification_by": null,
                    "is_active": True,
                    "shape": [],
                    "connector_id": 1
                },
                ...
            ]
        }

        """
    name = request.args.get('name', type=str, default=None)
    project_id = request.args.get('project_id', type=int, default=None)
    is_active = request.args.get('is_active', type=bool, default=True)
    search_filter = [Files.is_active == is_active]
    search_filter.append(Files.connector.project_id == project_id) if project_id else None
    search_filter.append(Files.name.like('%' + name + '%')) if name else None
    return jsonify(files=[c.serialize for c in db.session.query(Files).filter(*search_filter).all()])


@files.route('/add')
def add():
    """Register a file into the system.

    Parameters:
        name (str): name of the file
        description (str): what this file is all about?
        is_active (bool): [Optional] status of the file
        connector_id (int): ID of the connector to be used to ingest or read the file
        shape (dict): structure of the file/ schema of the redshift table with data types

    Returns:
        dict: message - file added successfully.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/files/add?name=alignment_file_csv&description=dummy&'
        ...              'connector_id=1')
        {
            "message": "file added successfully"
        }


    """
    name = request.args.get('name')
    description = request.args.get('description')
    is_active = request.args.get('is_active', type=bool, default=True)
    connector_id = request.args.get('connector_id', type=int)
    shape = json.loads(request.args.get('shape', type=str, default='[]'))
    new_file = Files(name=name, description=description, is_active=is_active, connector_id=connector_id, shape=shape,
                     creation_date=datetime.datetime.utcnow())
    db.session.add(new_file)
    db.session.commit()
    return jsonify(message='file added successfully')


@files.route('/update')
def update():
    """Update a file present in the system.

    Parameters:
        id (int): ID of the file to be updated.
        name (str): name of the file
        description (str): what this file is all about?
        is_active (bool): [Optional] status of the file - assumed to be true.
        connector_id (int): ID of the connector to be used to ingest or read the file
        shape (dict): structure of the file/ schema of the redshift table with data types

    Returns:
        dict: latest copy of the record present in the system.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/files/update?name=alignment_file_csv&description=dummy&'
        ...              'connector_id=1&id=1')
        {
            "file":
                {
                    "id": 1,
                    "name": "alignment_file_csv",
                    "description": "dummy",
                    "creation_date": null,
                    "created_by": null,
                    "modification_date": null,
                    "modification_by": null,
                    "is_active": True,
                    "shape": [],
                    "connector_id": 1
                }
        }

        """
    id = request.args.get('id', type=int)
    name = request.args.get('name')
    description = request.args.get('description')
    is_active = request.args.get('is_active', type=bool, default=True)
    connector_id = request.args.get('connector_id', type=int)
    shape = json.loads(request.args.get('shape', type=str, default='[]'))

    file_to_update = db.session.query(Files).filter(Files.id == id).first()
    file_to_update.name = name
    file_to_update.description = description
    file_to_update.is_active = is_active
    file_to_update.connector_id = connector_id
    file_to_update.shape = shape
    file_to_update.modification_date = datetime.datetime.utcnow()

    db.session.commit()
    return jsonify(file=file_to_update.serialize)


@files.route('/get')
def get():
    """Fetch a file present in the system.

    Parameters:
        id (int): ID of the file to be fetched from the system.

    Returns:
        dict: latest copy of the record present in the system.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/files/get?id=1')
        {
            "file":
                {
                    "id": 1,
                    "name": "alignment_file_csv",
                    "description": "dummy",
                    "creation_date": null,
                    "created_by": null,
                    "modification_date": null,
                    "modification_by": null,
                    "is_active": True,
                    "shape": [],
                    "connector_id": 1
                }
        }

    """
    file_id = request.args.get('id', type=int)
    get_file = db.session.query(Files).filter(Files.id == file_id).first()
    if get_file:
        return jsonify(file=get_file.serialize)
    return jsonify(file={})


@files.route('/delete')
def delete():
    """Delete a file from the system.

    Parameters:
        id (int): ID of the file to be deleted.

    Returns:
        dict: message - deleted successfully.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/files/delete?id=1')
        {
            "message": "deleted successfully"
        }

    """
    file_id = request.args.get('id', type=int)
    file_to_delete = db.session.query(Files).filter(Files.id == file_id).first()
    if file_to_delete:
        db.session.delete(file_to_delete)
        db.session.commit()
        return jsonify(message='deleted successfully')
    return jsonify(message='something went wrong')
