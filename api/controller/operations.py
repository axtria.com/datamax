#   datamax/api/controller/operations.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from flask import Blueprint, request, jsonify,render_template

from services import ServiceFactory
from utils.helpers import encoder
from services import ServiceFactory, FileFactory

operations = Blueprint('operations', __name__)


@operations.route('/read_header')
def read_header():
    """Read header row from the S3 file.

    Header object is always a file with some extension like .csv, .txt, etc. to fetch the header row.

    Parameters:
        s3_connector_id (int): Connection ID to access S3 resource.
        source_bucket (str): Source bucket name
        header_object (str): header object file name
        delimiter (str): [Optional] delimiter is self inspected in case, the delimiter is missing.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/operations/read_header?s3_connector_id=1&source_bucket=datamax1&'
        ...              'header_object=poc/symphony_sales/Symphony_Sales.csv')
    """
    service = ServiceFactory('data_operations.read', 'Header', request)
    return service.invoke()


@operations.route('/read_sample')
def read_sample():
    """Read few sample records from the source file.

    Source object can be a folder or a single file. In case, a folder is provided it will use the
    S3 explorer utility to fetch the file in the given folder. Else, the same file is used to capture
    the data records.

    Parameters:
        s3_connector_id (int): Connection ID to access S3 resource.
        source_bucket (str): Source bucket name
        source_object (str): Source files present in the given folder/file.
        data_header (bool): [Optional] default is True, in case the source object has header in it.
        delimiter (str): [Optional] delimiter is self inspected in case, the delimiter is missing.
        count_rows (int): [Optional] count of rows to be fetched from the file

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/operations/read_rows?s3_connector_id=1&source_bucket=datamax1&'
        ...              'source_object=poc/symphony_sales/Symphony_Sales.csv')


        Second example, in case source object is referring a folder instead of a file.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/operations/read_rows?s3_connector_id=1&source_bucket=datamax1&'
        ...              'source_object=poc/symphony_sales/')


    """
    service = ServiceFactory('data_operations.read', 'Rows', request)
    return service.invoke()


@operations.route('/encrypt', methods=['POST'])
def encrypt():
    """Encrypt the string provided as input.

    Parameters:
        q (string): The message string that needs to be encrypted.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/operations/encrypt?q=hello')
        {
          "encrypted": "gAAAAABdHH1UbiwYvrLwMGWjsVVKv3T0DYGRFE6NcKcKSSaEw5wEtAbYFzT5jPFs6zYWGS3kaxJQ02BWRNAcYVLyAEiJdJ-X5w=="
        }
    """
    q = request.form.get('q', type=str)
    return jsonify(encrypted=encoder(q))


@operations.route('/get_shape')
def get_shape():
    """Get column properties and its data type from redshift.

    Parameters:
        rs_connector_id (int): Connection ID to access RS resource.
        table_name (str): table name
        schema (str): schema name

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/operations/get_shape?rs_connector_id=2&table_name=etl_test&'
        ...              'schema=public')
    """
    service = ServiceFactory('data_operations.read', 'Shape', request)
    return service.invoke()


@operations.route('/upload', methods=['GET', 'POST'])
def upload():
    service = ServiceFactory('data_operations.s3_buckets', 'Upload', request,
                             response={'file_obj': request.files['file']})
    return service.invoke()


@operations.route('/sample_file')
def sample_file():
    service = ServiceFactory('data_operations.read', 'SampleFile', request)
    return service.invoke()


@operations.route('/file_properties')
def file_properties():
    service = ServiceFactory('data_operations.read', 'FileProperties', request)
    return service.invoke()


@operations.route('/attribute_info')
def attribute_info():
    service = ServiceFactory('data_operations.read', 'AttributeInfo', request)
    return service.invoke()


@operations.route('/dynamic_table')
def dynamic_table():
    service = ServiceFactory('data_operations.s3_file_to_redshift_table', 'RedshiftTableCreation', request)
    return service.invoke()


@operations.route('/zip_data')
def zip_data():
    service = ServiceFactory('data_operations.read', 'GetZipContents', request)
    return service.invoke()


@operations.route('/download')
def download_key():
    service = ServiceFactory('data_operations.s3_buckets', 'DownloadKey', request)
    return service.invoke()


@operations.route('/delete')
def delete_key():
    service = ServiceFactory('data_operations.s3_buckets', 'DeleteKey', request)
    return service.invoke()


@operations.route('/sftp_folders')
def sftp_files():
    service = ServiceFactory('data_operations.sftp_op', 'GetFolders', request)
    return service.invoke()


@operations.route('/storage_locations')
def storage_locations():
    service = ServiceFactory('data_operations.s3_buckets', 'StorageLoc', request)
    return service.invoke()

@operations.route('/attribute_info_fwf')
def attributes_info_fwf():
    service = ServiceFactory('data_operations.read', 'attribute_info_FwF', request)
    return service.invoke()

@operations.route('/get_buckets')
def get_buckets():
    """To list the buckets on s3.

        Parameters:
            s3_connector_id (int): to assign the S3 resource to be used for the execution.

        Returns:
            JSON: 

            Examples:
        To list buckets on `s3_connector_id: 17`.

        >>> import requests
        >>> requests.get("http://localhost:5000/api/operations/get_buckets?s3_connector_id=17" )
        ...
        {
        "buckets": [
            {
            "CreationDate": "2020-01-21T11:23:25+00:00",
            "Name": "abc-gbx-s3-archive-dev"
            },
            {
            "CreationDate": "2020-03-06T10:00:16+00:00",
            "Name": "abc-gbx-s3-inbound-dev"
            },
            {
            "CreationDate": "2020-01-21T11:21:28+00:00",
            "Name": "abc-gbx-s3-landing-dev"
            },
            {
            "CreationDate": "2020-03-06T10:01:11+00:00",
            "Name": "abc-gbx-s3-test-dev"
            },
            {
            "CreationDate": "2020-01-21T11:12:35+00:00",
            "Name": "abc-s3-logs-nvs-gb"
            }
        ]
        }
    """
    service = ServiceFactory('data_operations.s3_buckets', 'GetBuckets', request)
    return service.invoke()


@operations.route('/sf_table_config')
def sf_tab_check():
    service = ServiceFactory('data_operations.snowflake_op', 'SfExistingTableCheck', request)
    return service.invoke()


'''
@operations.route('/sf_table_list')
def sf_tab_list():
    service = ServiceFactory('data_operations.snowflake_op', 'SfTableList', request)
    return service.invoke()
'''


@operations.route('/sf_table_meta')
def sf_tab_meta():
    service = ServiceFactory('data_operations.snowflake_op', 'SfTableMeta', request)
    return service.invoke()


@operations.route('/sf_table_meta_data')
def sf_table_meta_data():
    service = ServiceFactory('data_operations.snowflake_op', 'SfTableMetaData', request)
    return service.invoke()

@operations.route('/export_info')
def export_info():
    service = FileFactory('data_operations.export_dq', 'ExportDQ', request)
    return service.invoke()



