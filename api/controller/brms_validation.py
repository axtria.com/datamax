#   datamax/api/controller/brms_validation.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
"""Business Rule Management API for Validation.


See Also:

    :func:`~controller.exchange` API Exchange module to carry out database operations.

Notes:
    To allow construction of rules by the non-programmers.
    A rule designer that gives a text area for a person to type in statements -
    is really a programming environment, not a business user-focused tool.
"""
from flask import Blueprint, request

from services import ServiceFactory

brms_validation = Blueprint('brms_validation', __name__)


@brms_validation.route('/')
def index():
    """BRMS Engine for validation.
    """
    return "BRMS Engine for validation"


@brms_validation.route('/create_validation')
def create_validation():
    """Create validation workflow for the BRMS.

    Workflow is defined for a specific file adaptor. These file adaptors enable polling of metadata
    (column names and their data type) from  in-application database.

    Each validation workflow is a template of rules in the saved state.

    Returns:
        dict: a JSON object which has the ``workflow_id`` generated for the given request.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/create_validation')
        {
          "workflow_id": "46a0aea31273498a896b7761561c34de"
        }
    """
    service = ServiceFactory('brms_validation', 'CreateWorkflow', request)
    return service.invoke()


@brms_validation.route('/add_validation_meta')
def add_validation_meta():
    """Add meta information to the workflow.

    Meta information has the following attributes:
        - name: name of the workflow
        - description: description of the workflow
        - status: can be DRAFT or PUBLISHED
        - workflow_id: string based unique Id

    Parameters:
        validation_id (str): unique Id of the validation
        name (str): name of the workflow
        description (str): description of the validation
        selected_datasets (str): comma separated file adaptor names

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/add_validation_meta?name=hello&description=some%20'
        ...              'desc&selected_datasets=ds1&validation_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": {
            "ds1": [
              {
                "column_name": "c1",
                "data_type": "varchar"
              },
              {
                "column_name": "c2",
                "data_type": "varchar"
              },
              {
                "column_name": "c3",
                "data_type": "numeric"
              },
              {
                "column_name": "c4",
                "data_type": "varchar"
              }
            ]
          },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": {},
          "RULES": [],
          "SELECTED_DATASETS": {
            "ds1": [
              {
                "column_name": "c1",
                "data_type": "varchar"
              },
              {
                "column_name": "c2",
                "data_type": "varchar"
              },
              {
                "column_name": "c3",
                "data_type": "numeric"
              },
              {
                "column_name": "c4",
                "data_type": "varchar"
              }
            ]
          }
        }

    """
    service = ServiceFactory('brms_validation', 'AddWorkflowMeta', request)
    return service.invoke()


@brms_validation.route('/add_rule')
def add_rule():
    """Add a rule to the validation workflow.

    Each business rule in BRMS is a component. All components have the following schema:
        * Rule: which carries the parameters required for prepared SQL query
        * Input: defines the metadata of the input dataset
        * Output: defines the metadata of the output dataset

    All components can be configured using text input.

    Parameters:
        validation_id (str): unique Id of the validation
        rule_name (str): business name given to the rule
        user_input (str): user text based on validation to be configured in the system.

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Examples:
        Add a rule without a parameter.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/add_rule?rule_name=first%20rule'
        ...              '&user_input=data type of column c1 is integer&validation_id=46a0aea31273498a896b7761561c34de'
        {
          "DATASETS": { ... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": {},
          "RULES": [
            {
                'rule_id': "9a8a8b2d3c504e99859f6de291b90fcc",
                'rule_name': 'data_type_integer',
                'column': 'c1',
                'input': 'ds1',
                'output': 'ds1',
                'parameters': [],
                'query': "update {schema}.{ds1} set dmx_message = concat(dmx_message, 'c1 value is not integer;') "
                         "where c1 !~ '^(-)?[0-9]+$'",
            }
          ],
          "SELECTED_DATASETS": { ... }
        }
    """
    service = ServiceFactory('brms_validation', 'AddRule', request)
    return service.invoke()


@brms_validation.route('/update_rule')
def update_rule():
    """Update a validation rule in the workflow.

    Each validation rule in BRMS for validation is a component. All components have the following schema:
        * Rule: which carries the parameters required for prepared SQL query
        * Input: defines the metadata of the input dataset
        * Output: defines the metadata of the output dataset

    All components can be configured using text input.

    Parameters:
        validation_id (str): unique Id of the validation
        rule_name (str): business name given to the rule
        user_input (str): user text based on components being configured in the system.
        rule_id (str): rule Id used to recognize the rule which has to be updated.

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Examples:
        Add a rule without a parameter.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/update_rule?rule_name=first%20rule'
        ...              '&user_input=data type of column c1 is float&rule_id=9a8a8b2d3c504e99859f6de291b90fcc'
        ...              '&validation_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [
            {
                'rule_id': "9a8a8b2d3c504e99859f6de291b90fcc",
                'rule_name': 'data_type_float',
                'column': 'c1',
                'input': 'ds1',
                'output': 'ds1',
                'parameters': [],
                'query': "update {schema}.{ds1} set dmx_message = concat(dmx_message, 'c1 value is not float;') "
                         "where c1 !~ '^[0-9]*.?[0-9]*$'",
            }
          ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms_validation', 'UpdateRule', request)
    return service.invoke()


@brms_validation.route('/delete_rule')
def delete_rule():
    """Delete a validation rule within the workflow.

    Parameters:
        validation_id (str): unique Id of the validation
        rule_id (str): rule Id used to recognize the rule which has to be updated.

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Examples:

        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/delete_rule?'
        ...              'rule_id=9a8a8b2d3c504e99859f6de291b90fcc'
        ...              '&validation_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [
          ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms_validation', 'DeleteRule', request)
    return service.invoke()


@brms_validation.route('/add_parameter_definition')
def add_parameter_definition():
    """Add parameter definition in the workflow.

    Parameters can be configured within rule composer in which the user has the option to
    select one of the following:

        * Picklist - dedicated input like Boolean values
        * Freehand text with alphanumeric or numeric constraint

    Parameters:
        validation_id (str): unique Id of the validation.
        parameter_name (str): name given to the parameter with $ sign.
        help_text (str): business specific help text to be displayed in scenario screen.
        description (str): describe the parameter being configured by the user.
        config_type (str): picklist, column, numeric or text.
        config_value (str): will vary based on the config_type
            * picklist - string with comma-separated options
            * numeric - None
            * text - None

    Returns:
        dict: a JSON object of the workflow generated for the given request.


    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/add_parameter_definition?parameter_name=$threshold'
        ...              '&config_type=numeric&config_value=&validation_id=46a0aea31273498a896b7761561c34de'
        ...              '&help_text=some%20threshold%20numeric%20value&description=describe%20the%20threshold')
        {
          "DATASETS": {... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": {
            "$threshold": {
              "config_type": "numeric",
              "config_value": "",
              "description": "describe the threshold",
              "help_text": "some threshold numeric value",
              "name": "$threshold"
            }
          },
          "RULES": [
            ...
          ],
          "SELECTED_DATASETS": { ... }
          }
        }

    """
    service = ServiceFactory('brms_validation', 'AddParameterDefinition', request)
    return service.invoke()


@brms_validation.route('/get_validation')
def get_validation():
    """Get a workflow as JSON.

    Structure of JSON::

    {"DATASETS": {}, "META": {}, "SELECTED_DATASETS": {}, "RULES": [], "PARAMETERS": {}}

    Parameters:
        validation_id (str): Unique validation id used to identify the workflow.

    Returns:
        dict: a JSON object which has the data of the workflow.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/get_validation?'
        ...              'validation_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "",
            "name": "",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [ ... ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms_validation', 'GetWorkflow', request)
    return service.invoke()


@brms_validation.route('/save_validation')
def save_validation():
    """Save BRMS workflow.

    Parameters:
        validation_id (str):  Unique workflow id used to identify the workflow.

    Returns:
        dict: a JSON object which has the message that the workflow has been saved.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/save_validation?'
        ...              'validation_id=46a0aea31273498a896b7761561c34de')
        {
          "message" : "Saved successfully"
        }

    """
    service = ServiceFactory('brms_validation', 'SaveWorkflow', request)
    return service.invoke()


@brms_validation.route('/publish_validation')
def publish_validation():
    """Publish the BRMS Validation workflow.

    Parameters:
        validation_id (str):  Unique validation id used to identify the workflow.

    Returns:
        dict: a JSON object which has the message that the workflow has been saved.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/publish_validation?'
        ...              'validation_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "",
            "name": "",
            "status": "PUBLISHED",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [ ... ],
          "SELECTED_DATASETS": { ... }
        }

        """
    service = ServiceFactory('brms_validation', 'PublishWorkflow', request)
    return service.invoke()


@brms_validation.route('/edit_validation')
def edit_validation():
    """Edit the BRMS Validation workflow.

    An API which raises BRMS based exception when requested with published workflow id.

    Parameters:
        validation_id (str):  Unique validation id used to identify the workflow.

    Returns:
        dict: a JSON object which has the message that the workflow has been saved.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/edit_validation?'
        ...              'validation_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "",
            "name": "",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [ ... ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms_validation', 'EditWorkflow', request)
    return service.invoke()


@brms_validation.route('/copy_validation')
def copy_validation():
    """Copy the BRMS Validation workflow.

    An API which creates a new workflow in DRAFT state using the copy of the workflow
    of the id with which it has been requested.

    Parameters:
        validation_id (str):  Unique validation id used to identify the workflow.

    Returns:
        dict: a JSON object which has the message that the workflow has been saved.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/copy_validation?'
        ...              'validation_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "",
            "name": "",
            "status": "DRAFT",
            "workflow_id": "72a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [ ... ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms_validation', 'CopyWorkflow', request)
    return service.invoke()


@brms_validation.route('/execute_validation')
def execute_validation():
    """Execute the ACTIVE scenario of validation.

    Assumption:
        Adaptor name should not be exactly same as actual table name.

    Parameters:
        scenario_id (str): unique scenario id to identify the scenario to be executed.
        validation_id (str): unique workflow id to identify the workflow.
        output_schema (str): [OPTIONAL] output tables are created in the schema - brms_output
        execution_schema (str): [OPTIONAL] intermediate tables are created in the schema - brms_instance

    Returns:
        dict: A JSON object with exact queries and output table names for reference.

    See Also:
        :func:`~controller.brms.publish_workflow` Change the state of the validation to published.

    Raises:
        BRMSDraftWorkflowExecuted: This exception is raised when a draft version of the workflow
        is triggered to execute.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms_validation/execute_validation?'
        ...              'validation_id=46a0aea31273498a896b7761561c34de'
        ...              '&scenario_id=3')
        {
          'META': {
            'scenario_id': '3',
            'workflow_id': '4f21746f4ca148d6b9e2626e4c0e7ef7',
            'status': 'passed'
          },
          'QUERY': [
            ...
          ],
          'OUTPUT': {
              'ds1': '...',
              'error_ds1': '...',
              'success_ds1': '...',
            },
          'INVALID_META': {
            'ds1': {
              'invalid_count': 750,
              'total_count': 4000
            }
          }
        }
    """
    service = ServiceFactory('brms_validation', 'ExecuteScenario', request)
    return service.invoke()
