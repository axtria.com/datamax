/***************************** CREATE SCRIPTS *****************************************************************/
create schema admn;
CREATE TABLE admn.user(
	firstname varchar(50) NULL,
	lastname varchar(50) NULL,
	is_active bool NULL,
	created_date timestamp NOT NULL,
	modified_date timestamp NULL,
	is_deleted bool NULL,
	user_id serial NOT NULL,
	username varchar(50) NOT NULL,
	email varchar(250),
	is_ldap_user bool NULL,
	is_local_user bool NULL,
	created_by int NULL,
	modified_by int NULL,
	is_admin bool NULL,
	password varchar(250),
	CONSTRAINT user_id_pkey PRIMARY KEY (user_id),
	CONSTRAINT user_username_ukey UNIQUE (username)
);



CREATE TABLE admn.usergroup (
	"name" varchar(50) NULL,
	description varchar(400) NULL,
	is_active bool NULL,
	created_date timestamp NOT NULL,
	modified_date timestamp NULL,
	is_deleted bool NULL,
	usergroup_id serial NOT NULL,
	created_by int NULL,
	modified_by int NULL,
	CONSTRAINT usergroup_id_pkey PRIMARY KEY (usergroup_id),
	CONSTRAINT usergroup_name_ukey UNIQUE (name)
);




CREATE TABLE admn.usergroup_user_mapping (
	usergroup_id int NULL,
	user_id int NULL,
	created_date timestamp NOT NULL,
	modified_date timestamp NULL,
	created_by int NULL,
	modified_by int NULL,
	usergroup_user_mapping_id serial NOT NULL,
	CONSTRAINT usergroup_user_mapping_pkey PRIMARY KEY (usergroup_user_mapping_id)
);



CREATE TABLE admn.layer (
	"name" varchar(50) NULL,
	layer_type varchar(10) NULL,
	description varchar(250) NULL,
	s3_path varchar(400) NULL,
	schema_name varchar(50) NULL,
	connector_id int NULL,
	is_active bool NULL,
	created_by int NOT NULL,
	created_date timestamp NOT NULL,
	modified_date timestamp NULL,
	modified_by int NULL,
	layer_id serial NOT NULL,
	CONSTRAINT layer_pkey PRIMARY KEY (layer_id)
);

CREATE TABLE admn."module" (
	module_id serial NOT NULL,
	"name" varchar(50) NULL,
	descritpion varchar(250) NULL,
	created_by int NOT NULL,
	created_date timestamp NOT NULL,
	modified_date timestamp NULL,
	modified_by int NULL,
	CONSTRAINT module_pkey PRIMARY KEY (module_id)
);


CREATE TABLE admn.module_layer_mapping (
	module_layer_mapping_id serial NOT NULL,
	module_id int NULL,
	layer_id int NULL,
	created_by int NOT NULL,
	created_date timestamp NOT NULL,
	modified_date timestamp NULL,
	modified_by int NULL,
	CONSTRAINT module_layer_mapping_pkey PRIMARY KEY (module_layer_mapping_id)
);


CREATE TABLE admn.usergroup_layer_mapping (
	usergroup_layer_mapping_id serial NOT NULL,
	layer_id int NOT NULL,
	usergroup_id int NULL,
	created_by int NOT NULL,
	created_date timestamp NOT NULL,
	modified_date timestamp NULL,
	modified_by int NULL,
	CONSTRAINT usergroup_layer_mapping_pkey PRIMARY KEY (usergroup_layer_mapping_id)
);


create table admn.functionality_master(
  functionality_id serial NOT NULL,
	"name" varchar(250) NULL UNIQUE,
	description varchar(400),
	module_id int NULL,
	CONSTRAINT functionality_pkey PRIMARY KEY (functionality_id)
);

CREATE TABLE admn.usergroup_functionality_mapping (
	usergroup_functionality_mapping_id serial NOT NULL,
	functionality_id int NOT NULL,
	usergroup_id int NULL,
	created_by int NOT NULL,
	created_date timestamp NOT NULL,
	modified_date timestamp NULL,
	modified_by int NULL,
	CONSTRAINT usergroup_functionality_mapping_pkey PRIMARY KEY (usergroup_functionality_mapping_id)
);


ALTER TABLE admn.usergroup_layer_mapping ADD CONSTRAINT usergroup_layer_mapping_created_by_fkey FOREIGN KEY (created_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.usergroup_layer_mapping ADD CONSTRAINT usergroup_layer_mapping_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.usergroup_layer_mapping ADD CONSTRAINT usergroup_layer_mapping_layer_id_fkey FOREIGN KEY (layer_id) REFERENCES admn.layer(layer_id);
ALTER TABLE admn.usergroup_layer_mapping ADD CONSTRAINT usergroup_layer_mapping_usergroup_id_fkey FOREIGN KEY (usergroup_id) REFERENCES admn.usergroup(usergroup_id);


ALTER TABLE admn.functionality_master ADD CONSTRAINT functionality_master_module_id_fkey FOREIGN KEY (module_id) REFERENCES admn.module(module_id);



ALTER TABLE admn.usergroup_functionality_mapping ADD CONSTRAINT usergroup_functionality_mapping_created_by_fkey FOREIGN KEY (created_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.usergroup_functionality_mapping ADD CONSTRAINT usergroup_functionality_mapping_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.usergroup_functionality_mapping ADD CONSTRAINT usergroup_functionality_mapping_functionality_id_fkey FOREIGN KEY (functionality_id) REFERENCES admn.functionality_master(functionality_id);
ALTER TABLE admn.usergroup_functionality_mapping ADD CONSTRAINT usergroup_functionality_mapping_usergroup_id_fkey FOREIGN KEY (usergroup_id) REFERENCES admn.usergroup(usergroup_id);

ALTER TABLE admn.layer ADD CONSTRAINT layer_connector_id_fkey FOREIGN KEY (connector_id) REFERENCES public.connector(id);
ALTER TABLE admn.layer ADD CONSTRAINT layer_created_by_fkey FOREIGN KEY (created_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.layer ADD CONSTRAINT layer_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES admn.user(user_id);

ALTER TABLE admn."module" ADD CONSTRAINT module_created_by_fkey FOREIGN KEY (created_by) REFERENCES admn.user(user_id);
ALTER TABLE admn."module" ADD CONSTRAINT module_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES admn.user(user_id);

ALTER TABLE admn.module_layer_mapping ADD CONSTRAINT module_layer_mapping_created_by_fkey FOREIGN KEY (created_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.module_layer_mapping ADD CONSTRAINT module_layer_mapping_layer_id_fkey FOREIGN KEY (layer_id) REFERENCES admn.layer(layer_id);
ALTER TABLE admn.module_layer_mapping ADD CONSTRAINT module_layer_mapping_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.module_layer_mapping ADD CONSTRAINT module_layer_mapping_module_id_fkey FOREIGN KEY (module_id) REFERENCES admn.module(module_id);

ALTER TABLE admn.usergroup ADD CONSTRAINT usergroup_created_by_fkey FOREIGN KEY (created_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.usergroup ADD CONSTRAINT usergroup_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES admn.user(user_id);

ALTER TABLE admn.usergroup_user_mapping ADD CONSTRAINT usergroup_user_mapping_user_id_fkey FOREIGN KEY (user_id) REFERENCES admn.user(user_id);
ALTER TABLE admn.usergroup_user_mapping ADD CONSTRAINT usergroup_user_mapping_usergroup_id_fkey FOREIGN KEY (usergroup_id) REFERENCES admn.usergroup(usergroup_id);
ALTER TABLE admn.usergroup_user_mapping ADD CONSTRAINT usergroup_created_by_fkey FOREIGN KEY (created_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.usergroup_user_mapping ADD CONSTRAINT usergroup_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES admn.user(user_id);


--ALTER TABLE admn.user ADD CONSTRAINT user_username_fkey FOREIGN KEY (username) REFERENCES public.api_exchange_auth(name);
ALTER TABLE admn.user ADD CONSTRAINT user_created_by_fkey FOREIGN KEY (created_by) REFERENCES admn.user(user_id);
ALTER TABLE admn.user ADD CONSTRAINT user_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES admn.user(user_id);



CREATE TABLE admn.access_request
(
   request_id        serial        NOT NULL,
   request_type      varchar(50),
   user_id           integer,
   usergroup_id      integer[],
   layer_id          integer[],
   status            varchar(50),
   module_id         integer[],
   functionality_id  integer[],
   created_by        integer       NOT NULL,
   created_date      timestamp     NOT NULL,
   modified_date     timestamp,
   modified_by       integer
);

-- Column request_id is associated with sequence admn.access_request_request_id_seq

ALTER TABLE admn.access_request
   ADD CONSTRAINT access_request_pkey
   PRIMARY KEY (request_id);

/*
ALTER TABLE access_request
  ADD CONSTRAINT access_request_functionality_id_fkey FOREIGN KEY (functionality_id)
  REFERENCES admn.functionality_master (functionality_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE access_request
  ADD CONSTRAINT access_request_layer_id_fkey FOREIGN KEY (layer_id)
  REFERENCES admn.layer (layer_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE access_request
  ADD CONSTRAINT access_request_module_id_fkey FOREIGN KEY (module_id)
  REFERENCES admn.module (module_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE access_request
  ADD CONSTRAINT access_request_usergroup_id_fkey FOREIGN KEY (usergroup_id)
  REFERENCES admn.usergroup (usergroup_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;
  
*/

ALTER TABLE admn.access_request
  ADD CONSTRAINT access_request_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.access_request
  ADD CONSTRAINT access_request_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.access_request
  ADD CONSTRAINT access_request_user_id_fkey FOREIGN KEY (user_id)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;



COMMIT;
/*************************************************************************************************************/


/***************************** DROP SCRIPTS *****************************************************************/
/*
DROP TABLE admn.access_request;
DROP TABLE admn.usergroup_layer_mapping;
drop table admn.usergroup_functionality_mapping;
drop table admn.functionality_master;
drop table admn.module_layer_mapping;
drop table admn.module;
drop table admn.layer;
drop table admn.usergroup_user_mapping;
drop table admn.usergroup;
drop table admn.user;



drop schema admn;

*/
/********************************************************************************************************************************/
CREATE OR REPLACE FUNCTION admn.fn_action_request_process (req_id INTEGER,action TEXT,loggedin_userid INTEGER) RETURNS json LANGUAGE plpgsql
AS
$body$ DECLARE outval json;

BEGIN 
if (select lower(status) from admn.access_request where request_id = req_id limit 1) = 'new' then
    IF (action = 'approve') THEN 
        INSERT INTO admn.usergroup_layer_mapping
        (
          layer_id,
          usergroup_id,
          created_by,
          created_date,
          modified_by,
          modified_date
        )
        SELECT r2.layer_id,
               r1.usergroup_id,
               loggedin_userid,
               NOW(),
               loggedin_userid,
               NOW()
        FROM (SELECT UNNEST(r.usergroup_id) usergroup_id
              FROM admn.access_request r
              WHERE r.request_id = req_id
              AND   status = 'new') r1
          CROSS JOIN (SELECT UNNEST(r.layer_id) layer_id
                      FROM admn.access_request r
                      WHERE r.request_id = req_id) r2
          LEFT JOIN admn.usergroup_layer_mapping mapping
                 ON r2.layer_id = mapping.layer_id
                AND r1.usergroup_id = mapping.usergroup_id
        WHERE mapping.usergroup_id IS NULL
        AND   mapping.layer_id IS NULL;

        INSERT INTO admn.usergroup_layer_mapping
        (
          layer_id,
          usergroup_id,
          created_by,
          created_date,
          modified_by,
          modified_date
        )
        SELECT r2.functionality_id,
               r1.usergroup_id,
               loggedin_userid,
               NOW(),
               loggedin_userid,
               NOW()
        FROM (SELECT UNNEST(r.usergroup_id) usergroup_id
              FROM admn.access_request r
              WHERE r.request_id = req_id
              AND   status = 'new') r1
          CROSS JOIN (SELECT UNNEST(r.functionality_id) functionality_id
                      FROM admn.access_request r
                      WHERE r.request_id = req_id) r2
          LEFT JOIN admn.usergroup_functionality_mapping mapping
                 ON r2.functionality_id = mapping.functionality_id
                AND r1.usergroup_id = mapping.usergroup_id
        WHERE mapping.usergroup_id IS NULL
        AND   mapping.functionality_id IS NULL;

        INSERT INTO admn.usergroup_layer_mapping
        (
          layer_id,
          usergroup_id,
          created_by,
          created_date,
          modified_by,
          modified_date
        )
        WITH ds
        AS
        (SELECT 'CATALOG.VIEW' functionality,
               'catalog' module UNION SELECT 'ADMN.VIEW' functionality,
               'admin' module UNION SELECT 'BRMS.VIEW' functionality,
               'brms' module UNION SELECT 'DQ.VIEW' functionality,
               'dq' module UNION SELECT 'INGESTION.VIEW' functionality,
               'ingestion' module) SELECT fm.functionality_id,
               r1.usergroup_id,
               loggedin_userid,
               NOW(),
               loggedin_userid,
               NOW()
        FROM (SELECT UNNEST(r.usergroup_id) usergroup_id
              FROM admn.access_request r
              WHERE r.request_id = req_id
              AND   status = 'new') r1
          CROSS JOIN (SELECT UNNEST(r.module_id) module_id
                      FROM admn.access_request r
                      WHERE r.request_id = req_id) r2
          INNER JOIN admn.module m ON m.module_id = r2.module_id
          INNER JOIN ds ON ds.module = m.name
          INNER JOIN admn.functionality_master fm ON fm.name = ds.functionality
          LEFT JOIN (SELECT * FROM admn.usergroup_functionality_mapping) mapping
                 ON fm.functionality_id = mapping.functionality_id
                AND r1.usergroup_id = mapping.usergroup_id
        WHERE mapping.usergroup_id IS NULL
        AND   mapping.functionality_id IS NULL;

        UPDATE admn.access_request
           SET status = 'approved',
               modified_date = NOW(),
               modified_by = loggedin_userid
        WHERE request_id = req_id
        AND   status = 'new';

      select row_to_json(y) into outval from (select *, 'Request approved. Accesses granted.' as message  from admn.access_request where request_id = req_id)y;

    ELSIF action = 'reject' THEN 
        UPDATE admn.access_request
           SET status = 'rejected',
               modified_date = NOW(),
               modified_by = loggedin_userid
        WHERE request_id = req_id
        AND   status = 'new';
    
        select row_to_json(y) into outval from (select *, 'Request rejected.' as message  from admn.access_request where request_id = req_id)y;
 
    ELSE 
      select row_to_json(y) into outval from (select *, 'Invalid request action.' as message  from  admn.access_request where request_id = req_id)y;

    END IF;
  else
      select row_to_json(y) into outval from (select *, 'Request is already processed.' as message  from  admn.access_request where request_id = req_id)y;
  end if; 

    RETURN outval;

END;

$body$ VOLATILE COST 100;

COMMIT;

-- **********************************DROP functions *****************************************

DROP FUNCTION admn.fn_action_request_process;

