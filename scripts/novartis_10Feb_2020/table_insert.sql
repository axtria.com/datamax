
insert into admn.user(firstname, lastname, is_active, created_date, modified_date, is_deleted,  username, is_ldap_user, is_local_user, created_by, modified_by, is_admin)
values( 'system', 'system', true, now(), now(), false,  system, false, true, 1, 1, true);


update admn.user
set password = 'pbkdf2:sha256:50000$FI5altmd$a3847d556928dca6255d8b3c59642ccda5e23f923723302ff459b063fac5b877'
where username = 'system' ;


insert into admn.usergroup (
	name,
	description ,
	is_active,
	created_date ,
	created_by 
) values ( 'admin', 'administrator user' ,true, now(), 1  );


insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('admin','administration module',1, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('brms','brms module',1, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('catalog','catalog module',1, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('dq','dq module',1, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('ingestion','ingestion module',1, now());

insert into admn.functionality_master (
	name,
	module_id ,
  description
) values('ADMN.CREATE_USER',1,'create user permission' );


insert into admn.functionality_master (
	name,
	module_id ,
	description
) values('ADMN.CREATE_USERGROUP',1,'create usergroup permission');



insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.EDIT_USER',1,'edit user permission');
 

insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.EDIT_USERGROUP',1,'edit user group permission');
 

insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.RESET_PASSWORD',1,'edit user group permission');
 

insert into admn.functionality_master (
name, 
module_id, 
description
) values('ADMN.VIEW',1,'view permission for admin module');


insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.VIEW',(select module_id from admn.module where name = 'brms' limit 1),'all permission for BRMS module');


insert into admn.functionality_master (
name, 
module_id, 
description
) values('CATALOG.VIEW',(select module_id from admn.module where name = 'catalog' limit 1),'all permission for CATALOG module');



insert into admn.functionality_master (
name, 
module_id, 
description
) values('DQ.VIEW',(select module_id from admn.module where name = 'dq' limit 1),'all permission for DQ module');



insert into admn.functionality_master (
name, 
module_id, 
description
) values('INGESTION.VIEW',(select module_id from admn.module where name = 'ingestion' limit 1),'all permission for INGESTION module');


/*insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.CREATE_WORKSPACE',(select module_id from admn.module where name = 'brms' limit 1),'create workspace permission for BRMS module');

insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.EDIT_WORKSPACE',(select module_id from admn.module where name = 'brms' limit 1),'edit workspace permission for BRMS module');

insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.DELETE_WORKSPACE',(select module_id from admn.module where name = 'brms' limit 1),'delete workspace permission for BRMS module');

insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.CREATE_WORKFLOW',(select module_id from admn.module where name = 'brms' limit 1),'create workflow permission for BRMS module');
*/





insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.CREATE_USER' limit 1) ,1,1,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.CREATE_USERGROUP' limit 1),1,1,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.EDIT_USER' limit 1),1,1,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.EDIT_USERGROUP' limit 1),1,1,now());
	

insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.RESET_PASSWORD' limit 1),1,1,now());

	
insert into admn.usergroup_user_mapping (
	usergroup_id ,
	user_id ,
	created_date,
	created_by 
) values ( 1 , 1, now(), 1 );


	
/*INSERT INTO admn.layer
(
  name,
  layer_type,
  description,
  s3_path,
  schema_name,
  connector_id,
  is_active,
  created_by,
  created_date,
  modified_date,
  modified_by
)
VALUES
(
  'staging',
  'rs',
  'redshift staging',
  NULL,
  'dmx_stg',
  2,
  TRUE,
  1,
  '2019-12-18 08:49:10.862',
  NULL,
  NULL
);

INSERT INTO admn.layer
(
  name,
  layer_type,
  description,
  s3_path,
  schema_name,
  connector_id,
  is_active,
  created_by,
  created_date,
  modified_date,
  modified_by
)
VALUES
(
  'datamax_ingestion',
  'rs',
  'redshift ingestion',
  NULL,
  'dmx_ingestion',
  2,
  TRUE,
  1,
  now(),
 now(),
 1
);

INSERT INTO admn.layer
(
  name,
  layer_type,
  description,
  s3_path,
  schema_name,
  connector_id,
  is_active,
  created_by,
  created_date,
  modified_date,
  modified_by
)
VALUES
(
  'datamax1',
  's3',
  'datamax bucket',
  NULL,
  's3://datamax1',
  7,
  TRUE,
  1,
  now(),
 now(),
 1
);

INSERT INTO admn.layer
(
  name,
  layer_type,
  description,
  s3_path,
  schema_name,
  connector_id,
  is_active,
  created_by,
  created_date,
  modified_date,
  modified_by
)
VALUES
(
  'public',
  'rs',
  'redshift landing',
  NULL,
  'dmx_lnd',
  2,
  TRUE,
  1,
  now(),
 now(),
 1
);

INSERT INTO admn.layer
(
  name,
  layer_type,
  description,
  s3_path,
  schema_name,
  connector_id,
  is_active,
  created_by,
  created_date,
  modified_date,
  modified_by
)
VALUES
(
  'brms_test',
  'rs',
  'redshift datawarehouse',
  NULL,
  'dmx_dwh',
  2,
  TRUE,
  1,
  now(),
 now(),
 1
);
*/
commit;




