CREATE OR REPLACE FUNCTION admn.fn_action_request_process(req_id integer, action text, loggedin_userid integer)
  RETURNS json
  LANGUAGE plpgsql
AS
$body$
DECLARE outval json;
is_selfgroup_req boolean;

BEGIN 
if (select lower(status) from admn.access_request where request_id = req_id limit 1) = 'new' then
    IF (action = 'approve') THEN

    IF (select count(1) from ( SELECT UNNEST(r.usergroup_id) usergroup_id
                  FROM admn.access_request r
                  WHERE r.request_id = req_id
                  AND   status = 'new' )x where usergroup_id = -1) >0 THEN
          is_selfgroup_req = TRUE;
            
                 
            insert into admn.usergroup (name,description ,is_active,group_type,created_date ,created_by,modified_date ,modified_by )
            select username,'self group for ' || username, true,'S',now(), loggedin_userid,now(), loggedin_userid
            from admn.user u inner join  (select r.user_id FROM admn.access_request r
                          WHERE r.request_id = req_id)req on req.user_id = u.user_id
                          where ( select count(1) 
                                from admn.usergroup_user_mapping m 
                                inner join admn.usergroup g on m.user_id = req.user_id and g.usergroup_id = m.usergroup_id and lower(g.group_type) = 's' ) = 0 ;
                                
                  insert into admn.usergroup_user_mapping(usergroup_id, user_id, created_date ,created_by,modified_date ,modified_by )
                  select  g.usergroup_id,r1.user_id,now(), loggedin_userid,now(), loggedin_userid 
                  from (select * from admn.usergroup where lower(group_type) = 's')g
                  cross join (select user_id from admn.access_request where request_id = req_id)r1
                  left join admn.usergroup_user_mapping m on g.usergroup_id = m.usergroup_id and r1.user_id = m.user_id
                  where m.user_id is null and m.usergroup_id is null;
        END IF;
       
        INSERT INTO admn.usergroup_layer_mapping
        (
          layer_id,
          usergroup_id,
          created_by,
          created_date,
          modified_by,
          modified_date
        )
        SELECT r2.layer_id,
               r1.usergroup_id,
               loggedin_userid,
               NOW(),
               loggedin_userid,
               NOW()
        FROM (
        
        select * from (
            SELECT UNNEST(r.usergroup_id) usergroup_id
              FROM admn.access_request r
              WHERE r.request_id = req_id )temp
 union
 select g.usergroup_id from 
 (select user_id from admn.user where admn.user.user_id = (select user_id FROM admn.access_request where request_id = req_id ))u
        INNER JOIN admn.usergroup_user_mapping umap on umap.user_id = u.user_id
 INNER JOIN admn.usergroup g on g.usergroup_id = umap.usergroup_id
 where is_selfgroup_req and lower(g.group_type) = 's'
              ) r1
          inner join admn.usergroup ugp on ugp.usergroup_id = r1.usergroup_id
          CROSS JOIN (SELECT UNNEST(r.layer_id) layer_id
                      FROM admn.access_request r
                      WHERE r.request_id = req_id) r2
          LEFT JOIN admn.usergroup_layer_mapping mapping
                 ON r2.layer_id = mapping.layer_id
                AND r1.usergroup_id = mapping.usergroup_id
        WHERE mapping.usergroup_id IS NULL
        AND   mapping.layer_id IS NULL ;

        INSERT INTO admn.usergroup_functionality_mapping
        (
          functionality_id,
          usergroup_id,
          created_by,
          created_date,
          modified_by,
          modified_date
        )
        SELECT r2.functionality_id,
               r1.usergroup_id,
               loggedin_userid,
               NOW(),
               loggedin_userid,
               NOW()
        FROM (SELECT UNNEST(r.usergroup_id) usergroup_id
              FROM admn.access_request r
              WHERE r.request_id = req_id
              AND   status = 'new') r1
          CROSS JOIN (SELECT UNNEST(r.functionality_id) functionality_id
                      FROM admn.access_request r
                      WHERE r.request_id = req_id) r2
          LEFT JOIN admn.usergroup_functionality_mapping mapping
                 ON r2.functionality_id = mapping.functionality_id
                AND r1.usergroup_id = mapping.usergroup_id
        WHERE mapping.usergroup_id IS NULL
        AND   mapping.functionality_id IS NULL;

        INSERT INTO admn.usergroup_functionality_mapping
        (
          functionality_id,
          usergroup_id,
          created_by,
          created_date,
          modified_by,
          modified_date
        )
        WITH ds
        AS
        (SELECT 'CATALOG.VIEW' functionality,
               'catalog' module UNION SELECT 'ADMN.VIEW' functionality,
               'admin' module UNION SELECT 'BRMS.VIEW' functionality,
               'brms' module UNION SELECT 'DQ.VIEW' functionality,
               'dq' module UNION SELECT 'INGESTION.VIEW' functionality,
               'ingestion' module) SELECT fm.functionality_id,
               r1.usergroup_id,
               loggedin_userid,
               NOW(),
               loggedin_userid,
               NOW()
        FROM (SELECT UNNEST(r.usergroup_id) usergroup_id
              FROM admn.access_request r
              WHERE r.request_id = req_id
              AND   status = 'new') r1
          CROSS JOIN (SELECT UNNEST(r.module_id) module_id
                      FROM admn.access_request r
                      WHERE r.request_id = req_id) r2
          INNER JOIN admn.module m ON m.module_id = r2.module_id
          INNER JOIN ds ON ds.module = m.name
          INNER JOIN admn.functionality_master fm ON fm.name = ds.functionality
          LEFT JOIN (SELECT * FROM admn.usergroup_functionality_mapping) mapping
                 ON fm.functionality_id = mapping.functionality_id
                AND r1.usergroup_id = mapping.usergroup_id
        WHERE mapping.usergroup_id IS NULL
        AND   mapping.functionality_id IS NULL;

        UPDATE admn.access_request
           SET status = 'approved',
               modified_date = NOW(),
               modified_by = loggedin_userid
        WHERE request_id = req_id
        AND   status = 'new';

      select row_to_json(y) into outval from (select *, 'Request approved. Accesses granted.' as message  from admn.access_request where request_id = req_id)y;

    ELSIF action = 'reject' THEN 
        UPDATE admn.access_request
           SET status = 'rejected',
               modified_date = NOW(),
               modified_by = loggedin_userid
        WHERE request_id = req_id
        AND   status = 'new';
    
        select row_to_json(y) into outval from (select *, 'Request rejected.' as message  from admn.access_request where request_id = req_id)y;
 
    ELSE 
      select row_to_json(y) into outval from (select *, 'Invalid request action.' as message  from  admn.access_request where request_id = req_id)y;

    END IF;
  else
      select row_to_json(y) into outval from (select *, 'Request is already processed.' as message  from  admn.access_request where request_id = req_id)y;
  end if; 

    RETURN outval;

END;
$body$
  VOLATILE
  COST 100;


COMMIT;
