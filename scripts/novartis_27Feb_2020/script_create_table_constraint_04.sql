

ALTER TABLE admn.functionality_master
  ADD CONSTRAINT functionality_master_module_id_fkey FOREIGN KEY (module_id)
  REFERENCES admn.module (module_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.layer
  ADD CONSTRAINT layer_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.layer
  ADD CONSTRAINT layer_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.layer
  ADD CONSTRAINT layer_connector_id_fkey FOREIGN KEY (connector_id)
  REFERENCES datamax_config.connection_meta (id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.module
  ADD CONSTRAINT module_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.module
  ADD CONSTRAINT module_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.module_layer_mapping
  ADD CONSTRAINT module_layer_mapping_layer_id_fkey FOREIGN KEY (layer_id)
  REFERENCES admn.layer (layer_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.module_layer_mapping
  ADD CONSTRAINT module_layer_mapping_module_id_fkey FOREIGN KEY (module_id)
  REFERENCES admn.module (module_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.module_layer_mapping
  ADD CONSTRAINT module_layer_mapping_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.module_layer_mapping
  ADD CONSTRAINT module_layer_mapping_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn."user"
  ADD CONSTRAINT user_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn."user"
  ADD CONSTRAINT user_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup
  ADD CONSTRAINT usergroup_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup
  ADD CONSTRAINT usergroup_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_functionality_mapping
  ADD CONSTRAINT usergroup_functionality_mapping_functionality_id_fkey FOREIGN KEY (functionality_id)
  REFERENCES admn.functionality_master (functionality_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_functionality_mapping
  ADD CONSTRAINT usergroup_functionality_mapping_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_functionality_mapping
  ADD CONSTRAINT usergroup_functionality_mapping_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_functionality_mapping
  ADD CONSTRAINT usergroup_functionality_mapping_usergroup_id_fkey FOREIGN KEY (usergroup_id)
  REFERENCES admn.usergroup (usergroup_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_layer_mapping
  ADD CONSTRAINT usergroup_layer_mapping_layer_id_fkey FOREIGN KEY (layer_id)
  REFERENCES admn.layer (layer_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_layer_mapping
  ADD CONSTRAINT usergroup_layer_mapping_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_layer_mapping
  ADD CONSTRAINT usergroup_layer_mapping_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_layer_mapping
  ADD CONSTRAINT usergroup_layer_mapping_usergroup_id_fkey FOREIGN KEY (usergroup_id)
  REFERENCES admn.usergroup (usergroup_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_user_mapping
  ADD CONSTRAINT usergroup_created_by_fkey FOREIGN KEY (created_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_user_mapping
  ADD CONSTRAINT usergroup_modified_by_fkey FOREIGN KEY (modified_by)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_user_mapping
  ADD CONSTRAINT usergroup_user_mapping_user_id_fkey FOREIGN KEY (user_id)
  REFERENCES admn."user" (user_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE admn.usergroup_user_mapping
  ADD CONSTRAINT usergroup_user_mapping_usergroup_id_fkey FOREIGN KEY (usergroup_id)
  REFERENCES admn.usergroup (usergroup_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;