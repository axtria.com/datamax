

CREATE TABLE IF NOT EXISTS admn.functionality_master
(
   functionality_id  serial         NOT NULL,
   name              varchar(250),
   description       varchar(400),
   module_id         integer
);

-- Column functionality_id is associated with sequence admn.functionality_master_functionality_id_seq

ALTER TABLE admn.functionality_master
   ADD CONSTRAINT functionality_pkey
   PRIMARY KEY (functionality_id);

ALTER TABLE admn.functionality_master
   ADD CONSTRAINT functionality_master_name_key UNIQUE (name);



CREATE TABLE IF NOT EXISTS admn.layer
(
   name           varchar(50),
   layer_type     varchar(10),
   description    varchar(250),
   s3_path        varchar(400),
   schema_name    varchar(50),
   connector_id   integer,
   is_active      boolean,
   created_by     integer        NOT NULL,
   created_date   timestamp      NOT NULL,
   modified_date  timestamp,
   modified_by    integer,
   layer_id       serial         NOT NULL
);

-- Column layer_id is associated with sequence admn.layer_layer_id_seq

ALTER TABLE admn.layer
   ADD CONSTRAINT layer_pkey
   PRIMARY KEY (layer_id);

CREATE TABLE IF NOT EXISTS admn.module
(
   module_id      serial         NOT NULL,
   name           varchar(50),
   descritpion    varchar(250),
   created_by     integer        NOT NULL,
   created_date   timestamp      NOT NULL,
   modified_date  timestamp,
   modified_by    integer
);

-- Column module_id is associated with sequence admn.module_module_id_seq

ALTER TABLE admn.module
   ADD CONSTRAINT module_pkey
   PRIMARY KEY (module_id);

CREATE TABLE IF NOT EXISTS admn.module_layer_mapping
(
   module_layer_mapping_id  serial      NOT NULL,
   module_id                integer,
   layer_id                 integer,
   created_by               integer     NOT NULL,
   created_date             timestamp   NOT NULL,
   modified_date            timestamp,
   modified_by              integer
);

-- Column module_layer_mapping_id is associated with sequence admn.module_layer_mapping_module_layer_mapping_id_seq

ALTER TABLE admn.module_layer_mapping
   ADD CONSTRAINT module_layer_mapping_pkey
   PRIMARY KEY (module_layer_mapping_id);

CREATE TABLE IF NOT EXISTS admn."user"
(
   firstname      varchar(50),
   lastname       varchar(50),
   is_active      boolean,
   created_date   timestamp      NOT NULL,
   modified_date  timestamp,
   is_deleted     boolean,
   user_id        serial         NOT NULL,
   username       varchar(50)    NOT NULL,
   is_ldap_user   boolean,
   is_local_user  boolean,
   created_by     integer,
   modified_by    integer,
   is_admin       boolean,
   password       varchar(250),
   email          varchar(250)
);

-- Column user_id is associated with sequence admn.user_user_id_seq

ALTER TABLE admn."user"
   ADD CONSTRAINT user_id_pkey
   PRIMARY KEY (user_id);

ALTER TABLE admn."user"
   ADD CONSTRAINT user_username_ukey UNIQUE (username);



CREATE TABLE IF NOT EXISTS admn.usergroup
(
   name           varchar(50),
   description    varchar(400),
   is_active      boolean,
   created_date   timestamp      NOT NULL,
   modified_date  timestamp,
   is_deleted     boolean,
   usergroup_id   serial         NOT NULL,
   created_by     integer,
   modified_by    integer,
   group_type     char(1)
);

-- Column usergroup_id is associated with sequence admn.usergroup_usergroup_id_seq

ALTER TABLE admn.usergroup
   ADD CONSTRAINT usergroup_id_pkey
   PRIMARY KEY (usergroup_id);

ALTER TABLE admn.usergroup
   ADD CONSTRAINT usergroup_name_ukey UNIQUE (name);



CREATE TABLE IF NOT EXISTS admn.usergroup_functionality_mapping
(
   usergroup_functionality_mapping_id  serial NOT NULL,
   functionality_id                    integer     NOT NULL,
   usergroup_id                        integer,
   created_by                          integer     NOT NULL,
   created_date                        timestamp   NOT NULL,
   modified_date                       timestamp,
   modified_by                         integer
);

ALTER TABLE admn.usergroup_functionality_mapping
   ADD CONSTRAINT usergroup_functionality_mapping_pkey
   PRIMARY KEY (usergroup_functionality_mapping_id);

CREATE TABLE IF NOT EXISTS admn.usergroup_layer_mapping
(
   usergroup_layer_mapping_id  serial      NOT NULL,
   layer_id                    integer     NOT NULL,
   usergroup_id                integer,
   created_by                  integer     NOT NULL,
   created_date                timestamp   NOT NULL,
   modified_date               timestamp,
   modified_by                 integer
);

-- Column usergroup_layer_mapping_id is associated with sequence admn.usergroup_layer_mapping_usergroup_layer_mapping_id_seq

ALTER TABLE admn.usergroup_layer_mapping
   ADD CONSTRAINT usergroup_layer_mapping_pkey
   PRIMARY KEY (usergroup_layer_mapping_id);

CREATE TABLE IF NOT EXISTS admn.usergroup_user_mapping
(
   usergroup_id               integer     NOT NULL,
   user_id                    integer     NOT NULL,
   created_date               timestamp   NOT NULL,
   modified_date              timestamp,
   created_by                 integer,
   modified_by                integer,
   usergroup_user_mapping_id  serial      NOT NULL
);

-- Column usergroup_user_mapping_id is associated with sequence admn.usergroup_user_mapping_usergroup_user_mapping_id_seq

ALTER TABLE admn.usergroup_user_mapping
   ADD CONSTRAINT usergroup_user_mapping_pkey
   PRIMARY KEY (usergroup_user_mapping_id);


-- ****** public schema tables **************************



CREATE TABLE IF NOT EXISTS api_exchange
(
   id           serial        NOT NULL,
   name         varchar       NOT NULL,
   version      integer       DEFAULT 1 NOT NULL,
   description  varchar,
   status       varchar,
   category     varchar,
   created_on   timestamptz   DEFAULT now()
);

ALTER TABLE api_exchange
   ADD CONSTRAINT api_exchange_pkey
   PRIMARY KEY (name, version);

ALTER TABLE api_exchange
   ADD CONSTRAINT api_exchange_id_key UNIQUE (id);



CREATE TABLE IF NOT EXISTS api_exchange_auth
(
   id         serial    NOT NULL,
   name       varchar,
   password   varchar,
   is_active  boolean
);

-- Column id is associated with sequence public.api_exchange_auth_id_seq

ALTER TABLE api_exchange_auth
   ADD CONSTRAINT api_exchange_auth_pkey
   PRIMARY KEY (id);

ALTER TABLE api_exchange_auth
   ADD CONSTRAINT api_exchange_auth_name_key UNIQUE (name);



CREATE TABLE IF NOT EXISTS api_exchange_meta
(
   id            serial    NOT NULL,
   api_id        integer,
   query         varchar,
   connector_id  integer,
   sequence      integer
);


-- ****** datamax_config schema tables **************************

CREATE TABLE IF NOT EXISTS datamax_config.connection_meta
(
   id                 serial    NOT NULL,
   name               varchar,
   description        varchar,
   project_id         integer,
   connection         json,
   created_by         integer,
   creation_date      date,
   modified_by        integer,
   modification_date  date,
   is_active          boolean,
   connection_type    varchar,
   status	      varchar	
);

-- Column id is associated with sequence datamax_config.connection_meta_id_seq

ALTER TABLE datamax_config.connection_meta
   ADD CONSTRAINT connection_meta_pkey
   PRIMARY KEY (id);




-- Column id is associated with sequence public.api_exchange_meta_id_seq

ALTER TABLE api_exchange_meta
   ADD CONSTRAINT api_exchange_meta_pkey
   PRIMARY KEY (id);


ALTER TABLE api_exchange_meta
  ADD CONSTRAINT api_exchange_meta_api_id_fkey FOREIGN KEY (api_id)
  REFERENCES api_exchange (id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE api_exchange_meta
  ADD CONSTRAINT api_exchange_meta_connector_id_fkey FOREIGN KEY (connector_id)
  REFERENCES datamax_config.connection_meta (id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;



COMMIT;
