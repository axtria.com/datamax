
insert into admn.user(firstname, lastname, is_active, created_date, modified_date, is_deleted,  username, is_ldap_user, is_local_user, created_by, modified_by, is_admin)
values( 'system', 'system', true, now(), now(), false, 'system', false, true, 1, 1, true);


update admn.user
set password = 'pbkdf2:sha256:50000$FI5altmd$a3847d556928dca6255d8b3c59642ccda5e23f923723302ff459b063fac5b877'
where username = 'system' ;


insert into admn.usergroup (
	name,
	description ,
	is_active,
	created_date ,
	created_by 
) values ( 'admin', 'administrator user' ,true, now(), 1  );

insert into admn.usergroup (
	name,
	description ,
	is_active,
	created_date ,
	created_by 
) values ( 'catalog', 'catalog user' ,true, now(), 1  );



insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('admin','administration module',1, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('brms','brms module',1, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('catalog','catalog module',1, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('dq','dq module',1, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('ingestion','ingestion module',1, now());

insert into admn.functionality_master (
	name,
	module_id ,
  description
) values('ADMN.CREATE_USER',1,'create user permission' );


insert into admn.functionality_master (
	name,
	module_id ,
	description
) values('ADMN.CREATE_USERGROUP',1,'create usergroup permission');



insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.EDIT_USER',1,'edit user permission');
 

insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.EDIT_USERGROUP',1,'edit user group permission');
 

insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.RESET_PASSWORD',1,'edit user group permission');
 

insert into admn.functionality_master (
name, 
module_id, 
description
) values('ADMN.VIEW',1,'view permission for admin module');


insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.VIEW',(select module_id from admn.module where name = 'brms' limit 1),'all permission for BRMS module');


insert into admn.functionality_master (
name, 
module_id, 
description
) values('CATALOG.VIEW',(select module_id from admn.module where name = 'catalog' limit 1),'all permission for CATALOG module');



insert into admn.functionality_master (
name, 
module_id, 
description
) values('DQ.VIEW',(select module_id from admn.module where name = 'dq' limit 1),'all permission for DQ module');



insert into admn.functionality_master (
name, 
module_id, 
description
) values('INGESTION.VIEW',(select module_id from admn.module where name = 'ingestion' limit 1),'all permission for INGESTION module');


insert into admn.functionality_master (
name, 
module_id, 
description
) values('CATALOG.ADD_TAG',(select module_id from admn.module where name = 'catalog' limit 1),'all permission for CATALOG module');

insert into admn.functionality_master (
name, 
module_id, 
description
) values('CATALOG.DELETE_TAG',(select module_id from admn.module where name = 'catalog' limit 1),'all permission for CATALOG module');


insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.CREATE_USER' limit 1) ,1,1,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.CREATE_USERGROUP' limit 1),1,1,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.EDIT_USER' limit 1),1,1,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.EDIT_USERGROUP' limit 1),1,1,now());
	

insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.RESET_PASSWORD' limit 1),1,1,now());



insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'CATALOG.VIEW' limit 1),2,1,now());

insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'CATALOG.ADD_TAG' limit 1),2,1,now());

insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'CATALOG.DELETE_TAG' limit 1),2,1,now());

	
insert into admn.usergroup_user_mapping (
	usergroup_id ,
	user_id ,
	created_date,
	created_by 
) values ( 1 , 1, now(), 1 );


insert into admn.usergroup_user_mapping (
	usergroup_id ,
	user_id ,
	created_date,
	created_by 
) values ( 2 , 1, now(), 1 );

---connector_id - 1:postgres ,2: s3, 3:snowflake


INSERT INTO datamax_config.connection_meta
(
  name,
  description,
  project_id,
  connection,
  created_by,
  creation_date,
  modified_by,
  modification_date,
  is_active,
  connection_type
)
VALUES
(
  'novartis_postgres',
  'local postgres here',
  999,
  NULL,
  1,
  now(),
  1,
  now(),
  TRUE,
  'local'
);

INSERT INTO datamax_config.connection_meta
(
  name,
  description,
  project_id,
  connection,
  created_by,
  creation_date,
  modified_by,
  modification_date,
  is_active,
  connection_type
)
VALUES
(
  'novartis_s3',
  'Ingestion File Here',
  999,
  CAST('{"ACCESS_KEY": "", "SECRET_KEY": "","ROLE":""}' AS JSON),
  1,
  now(),
  1,
  now(),
  TRUE,
  's3'
);

INSERT INTO datamax_config.connection_meta
(
  name,
  description,
  project_id,
  connection,
  created_by,
  creation_date,
  modified_by,
  modification_date,
  is_active,
  connection_type
)
VALUES
(
  'novartis_snowflake',
  'Glassbox Dev Snowflake schema',
  999,
  CAST('{"ACCOUNT_ID": "lqa15945.us-east-1", "PASSWORD": "gAAAAABeVPO4GCG52z4grYbo1XoRYXRGeHtQJXpIbREeB2m1uGjYkxtj1rR9C26LUfvOhKExIJgVsG-zq-SMBY5cL4S7NM-Pww==", "USER": "NVSDEV", "WAREHOUSE": "COMPUTE_WH", "DATABASE": "GLASSBOX_DEV"}' AS JSON),
  1,
  now(),
  1,
  now(),
  TRUE,
  'sf'
);


INSERT INTO admn.layer
(
  name,
  layer_type,
  description,
  s3_path,
  schema_name,
  connector_id,
  is_active,
  created_by,
  created_date,
  modified_date,
  modified_by
)
VALUES
(
  'ingestion',
  's3',
  's3 ingestion',
  's3://novartisrsbusnvglassboxdev001',
   NULL,
  (select id from datamax_config.connection_meta where lower(name) = 'novartis_s3' limit 1), 
  TRUE,
  1,
  now(),
 now(),
 1
);
	
INSERT INTO admn.layer
(
  name,
  layer_type,
  description,
  s3_path,
  schema_name,
  connector_id,
  is_active,
  created_by,
  created_date,
  modified_date,
  modified_by
)
VALUES
(
  'staging',
  'sf',
  'Glassbox dev snowflake staging',
   NULL,
  'STAGE',
  (select id from datamax_config.connection_meta where lower(name) = 'novartis_snowflake' limit 1),
  TRUE,
  1,
  now(),
  now(),
  1
);


-- --------------------------------------------------------------------------------------------------

INSERT INTO api_exchange_auth
(
  name,
  password,
  is_active
)
VALUES
(
  'system',
  'pbkdf2:sha256:150000$NjXrRFG2$862eacee742b14ad95e408da04672bda40d3847098ca56a66e9aba0df67ff7de',
  TRUE
);

-- --------------------------------------------------------------------------------------------------

INSERT INTO api_exchange (name,version,description,status,category,created_on) 
VALUES
  ('get_users',1,'Return all the users and permissions','published','administration',now()),
  ('delete_connection',1,'This will soft delete the connection from datamax_config.connection_meta','published','administration',now()),
  ('list_connections',1,'Get the list of connections for the Connector Management screen - DMX-1083','published','administration',now()),
  ('get_connector_layers',1,'This API will fetch all the layers details corresponding to the connectors DMX-1112','published','administration',now()),
  ('add_connection_s3',1,'Add S3 connection into the system','published','administration',now()),
  ('add_connection_snowflake',1,'Add snowflake connection into the system','published','administration',now()),
  ('add_connection_sftp',1,'Add sftp connection into the system','published','administration',now()),
  ('update_connection_s3',1,'Add S3 connection into the system','published','administration',now()),
  ('update_connection_snowflake',1,'Update snowflake connection into the system','published','administration',now()),
  ('update_connection_sftp',1,'Update sftp connection into the system','published','administration',now());


-- -------------------------------------------------------------------------------------------


INSERT INTO api_exchange_meta (api_id,query,connector_id,sequence) 
VALUES
  ((select id from api_exchange where name = 'delete_connection' and lower(status) = 'published' limit 1),'update datamax_config.connection_meta set is_active = ''false'' where id = %(connection_id)s',1,1),


  ((select id from api_exchange where name = 'add_connection_snowflake' and lower(status) = 'published' limit 1),'insert into datamax_config.connection_meta (name, description, connection, is_active, connection_type,created_by,creation_date,modified_by,modification_date) values(%(c_name)s, %(description)s, cast(concat('{"USER": "', %(user)s, '", "ACCOUNT_ID": "', %(account_id)s, '", "PASSWORD": "', %(password)s, '", "DATABASE": "', %(database)s, '"}') as json), 'true', 'sf',cast(%(loggedin_userid)s as int),now(),cast(%(loggedin_userid)s as int),now())',1,1),


  ((select id from api_exchange where name = 'add_connection_sftp' and lower(status) = 'published' limit 1),'insert into datamax_config.connection_meta (name, description, connection, is_active, connection_type,created_by,creation_date,modified_by,modification_date) values(%(c_name)s, %(description)s, cast(concat('{"USERNAME": "', %(username)s, '", "HOST": "', %(host)s, '", "PASSWORD": "', %(password)s, '", "PORT": "', %(port)s, '"}') as json), 'true', 'sftp',cast(%(loggedin_userid)s as int),now(),cast(%(loggedin_userid)s as int),now())',1,1),


  ((select id from api_exchange where name = 'update_connection_s3' and lower(status) = 'published' limit 1),'update datamax_config.connection_meta set name = %(c_name)s, description = %(description)s, connection = cast(concat('{"ACCESS_KEY": "', %(access_key)s, '", "SECRET_KEY": "', %(secret_key)s, '", "ROLE": "', %(rolename)s, '"}') as json), is_active = 'true', connection_type = 's3',created_by = cast(%(loggedin_userid)s as int),creation_date = now(),modified_by = cast(%(loggedin_userid)s as int),modification_date = now() where id = %(connector_id)s',1,1),


  ((select id from api_exchange where name = 'update_connection_snowflake' and lower(status) = 'published' limit 1),'update datamax_config.connection_meta set name = %(c_name)s, description = %(description)s, connection = cast(concat('{"USER": "', %(user)s, '", "ACCOUNT_ID": "', %(account_id)s, '", "PASSWORD": "', %(password)s, '", "DATABASE": "', %(database)s, '"}') as json), is_active = 'true', connection_type = 'sf',created_by = cast(%(loggedin_userid)s as int),creation_date = now(),modified_by = cast(%(loggedin_userid)s as int),modification_date = now() where id = %(connector_id)s',1,1),


  ((select id from api_exchange where name = 'get_users' and lower(status) = 'published' limit 1),'SELECT u.user_id, u.firstname, u.lastname, u.email, u.is_active, u.created_date, u.modified_date, u.is_deleted, u.username, u.is_ldap_user, u.is_local_user, u.created_by, u.modified_by, (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(ugp))) FROM (SELECT ug.*, (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(p))) FROM (SELECT m.module_id, m.name module_name, json_agg((select x from ( select f.functionality_id,f.name,f.description)x) ) as functionality FROM admn.usergroup_functionality_mapping pf INNER JOIN admn.functionality_master f ON pf.functionality_id = f.functionality_id INNER JOIN admn.module m ON m.module_id = f.module_id WHERE pf.usergroup_id = ug.usergroup_id group by m.module_id, m.name) p) AS modules, (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(l))) FROM (SELECT lr1.layer_id, lr1.name FROM admn.layer lr1 INNER JOIN admn.usergroup_layer_mapping ulm on ulm.layer_id = lr1.layer_id INNER JOIN admn.usergroup_user_mapping ugp1 on ugp1.usergroup_id = ulm.usergroup_id and ugp1.user_id = u.user_id )l) layers, (SELECT COUNT(1) FROM admn.usergroup_user_mapping users where users.usergroup_id = ug.usergroup_id) as usercount FROM admn.usergroup ug INNER JOIN admn.usergroup_user_mapping m ON m.usergroup_id = ug.usergroup_id WHERE u.user_id = m.user_id) ugp) AS usergroupdetail, CASE WHEN a.user_id IS NULL THEN FALSE ELSE TRUE END AS is_admin FROM admn.user u LEFT JOIN (SELECT umap.user_id FROM admn.usergroup_user_mapping umap INNER JOIN admn.usergroup ug1 ON ug1.usergroup_id = umap.usergroup_id WHERE ug1.name = ''admin'') a ON a.user_id = u.user_id WHERE (NULLIF(%(user_id)s,''null'') IS NULL AND NULLIF(%(user_name)s,''null'') IS NULL) OR ((NULLIF(%(user_id)s,''null'') IS NOT NULL AND u.user_id = CAST(NULLIF(%(user_id)s,''null'') AS INT)) OR (NULLIF(%(user_name)s,''null'') IS NOT NULL AND u.username = NULLIF(%(user_name)s,''null''))) LIMIT cast(coalesce( NULLIF(%(limit)s,''null''),''100'') as int)',1,1),


((select id from api_exchange where name = 'list_connections' and lower(status) = 'published' limit 1),'select m.id, m.name, m.description, m.connection_type ,m.connection , m.creation_date as created_on, 
m.modification_date as modified_on, u1.firstname || '' '' || u1.lastname created_by , u2.firstname || '' '' || u2.lastname modified_by, status from datamax_config.connection_meta m inner join admn.user u1
on u1.user_id = m.created_by inner join admn.user u2 
on u2.user_id = m.modified_by
 where m.is_active = ''true'' and lower(m.connection_type) = ''local'';',1,1),


  ((select id from api_exchange where name = 'get_connector_layers' and lower(status) = 'published' limit 1),'select l.name, l.layer_type, l.s3_path,l.schema_name,l.is_active,l.connector_id, u1.firstname || '' '' || u1.lastname created_by, l.created_date, l.modified_date, u2.firstname || '' '' || u2.lastname modified_by from admn.layer l inner join admn.user u1
on u1.user_id = l.created_by inner join admn.user u2 
on u2.user_id = l.modified_by where connector_id = %(connector_id)s',1,1),


  ((select id from api_exchange where name = 'add_connection_s3' and lower(status) = 'published' limit 1),'insert into datamax_config.connection_meta 
(name, description, connection, is_active, connection_type,created_by,creation_date,modified_by,modification_date) 
values(%(c_name)s, %(description)s, cast(concat('{"ACCESS_KEY": "', %(access_key)s, '", "SECRET_KEY": "', %(secret_key)s, '", "ROLE": "', %(rolename)s, '"}') as json), 'true', 's3',cast(%(loggedin_userid)s as int),now(),cast(%(loggedin_userid)s as int),now())',1,1),


  ((select id from api_exchange where name = 'update_connection_sftp' and lower(status) = 'published' limit 1),'update datamax_config.connection_meta set name = %(c_name)s, description = %(description)s, connection = cast(concat('{"USERNAME": "', %(username)s, '", "HOST": "', %(host)s, '", "PASSWORD": "', %(password)s, '", "PORT": "', %(port)s, '"}') as json), is_active = 'true', connection_type = 'sftp',created_by = cast(%(loggedin_userid)s as int),creation_date = now(),modified_by = cast(%(loggedin_userid)s as int),modification_date = now() where id = %(connector_id)s',1,1);



commit;