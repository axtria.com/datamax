import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def sendNotification():
    recepients_list = "shashank.senger@novartis.com"
    subject = 'Subject'
    message = "Test Message from shilpa" 
    sendemail(recepients_list,subject,message)

def sendemail(to_addr_list, subject, message):
    from_addr = 'shashank.senger@novartis.com'    
    
    
    s = smtplib.SMTP('mail.novartis.com')
    s.starttls()
        
    msg = MIMEMultipart()    
    msg['From'] = from_addr
    msg['To'] = to_addr_list
    msg['Subject'] = "test mail"
    
    body_text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttps://www.python.org"
        
    msg.attach(MIMEText(body_text, 'plain'))
        
    try:    
        s.set_debuglevel(1)
        s.send_message(msg)
        print('notification sent')
    except:
        print('error sending notification')
    
    s.quit()  

sendNotification()