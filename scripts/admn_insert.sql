select * from admn.user

insert into admn.user(firstname, lastname, is_active, created_date, modified_date, is_deleted,  username, is_ldap_user, is_local_user, created_by, modified_by, is_admin, password)
select firstname, lastname, is_active, created_date, modified_date, is_deleted,  username, is_ldap_user, is_local_user, created_by, modified_by, is_admin, password
from public.api_exchange_auth

select * from public.api_exchange_auth
insert into admn.user(username, password, is_active,created_date)
select name, password,is_active ,now()
from  public.api_exchange_auth

update admn.user
set firstname = username,lastname = username, is_deleted = false, is_ldap_user = false, is_local_user = true, is_admin = true
where username = 'system'

commit;

select * from admn.user 
select * from api_exchange where status = 'published' and name like '%user%'
select * from api_exchange_meta where api_id = 312

update api_exchange




insert into admn.usergroup (
	"name",
	description ,
	is_active,
	created_date ,
	created_by 
) values ( %(name)s, %(description)s,true, now(), %(loggedinuser)s  );

insert into admn.usergroup (
	name,
	description ,
	is_active,
	created_date ,
	created_by 
) values ( 'admin', 'administrator user' ,true, now(), 5  );
commit;

insert into admn.usergroup_user_mapping (
	usergroup_id ,
	user_id ,
	created_date,
	created_by 
) values ( %(usergroup_id)s , %(user_id)s, now(), %(loggedinuser)s );

insert into admn.usergroup_user_mapping (
	usergroup_id ,
	user_id ,
	created_date,
	created_by 
) values ( 1 , 5, now(), 5 );


insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('admin','administration module',5, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('brms','brms module',5, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('catalog','catalog module',5, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('dq','dq module',5, now());

insert into  admn."module" (
	name ,
	descritpion,
	created_by ,
	created_date
) values('ingestion','ingestion module',5, now());

insert into admn.functionality_master (
	name,
	module_id ,
  description
) values('ADMN.CREATE_USER',1,'create user permission' );


insert into admn.functionality_master (
	name,
	module_id ,
	description
) values('ADMN.CREATE_USERGROUP',1,'create usergroup permission');



insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.EDIT_USER',1,'edit user permission');
 

insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.EDIT_USERGROUP',1,'edit user group permission');
 

insert into admn.functionality_master (
  name, 
  module_id, 
  description
 ) values('ADMN.RESET_PASSWORD',1,'edit user group permission');
 

insert into admn.functionality_master (
name, 
module_id, 
description
) values('ADMN.VIEW',1,'view permission for admin module');


insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.VIEW',(select module_id from admn.module where name = 'brms' limit 1),'all permission for BRMS module');


insert into admn.functionality_master (
name, 
module_id, 
description
) values('CATALOG.VIEW',(select module_id from admn.module where name = 'catalog' limit 1),'all permission for CATALOG module');



insert into admn.functionality_master (
name, 
module_id, 
description
) values('DQ.VIEW',(select module_id from admn.module where name = 'dq' limit 1),'all permission for DQ module');



insert into admn.functionality_master (
name, 
module_id, 
description
) values('INGESTION.VIEW',(select module_id from admn.module where name = 'ingestion' limit 1),'all permission for INGESTION module');


insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.CREATE_WORKSPACE',(select module_id from admn.module where name = 'brms' limit 1),'create workspace permission for BRMS module');

insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.EDIT_WORKSPACE',(select module_id from admn.module where name = 'brms' limit 1),'edit workspace permission for BRMS module');

insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.DELETE_WORKSPACE',(select module_id from admn.module where name = 'brms' limit 1),'delete workspace permission for BRMS module');

insert into admn.functionality_master (
name, 
module_id, 
description
) values('BRMS.CREATE_WORKFLOW',(select module_id from admn.module where name = 'brms' limit 1),'create workflow permission for BRMS module');

insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.CREATE_USER' limit 1) ,1,5,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.CREATE_USERGROUP' limit 1),1,5,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.EDIT_USER' limit 1),1,5,now());
	
insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.EDIT_USERGROUP' limit 1),1,5,now());
	

insert into admn.usergroup_functionality_mapping(functionality_id,usergroup_id ,
	created_by,
	created_date )
	values((select functionality_id from admn.functionality_master where name = 'ADMN.RESET_PASSWORD' limit 1),1,5,now());
	
------- insert into layer table

insert into admn.layer(name, layer_type, description, s3_path, schema_name, connector_id, is_active, created_by,created_date)
values( 'datamax', 's3', 'datamax bucket',null, 's3://datamax1',7,true,5,now());
insert into admn.layer(name, layer_type, description, s3_path, schema_name, connector_id, is_active, created_by,created_date)
values( 'ingestion', 'rs', 'redshift ingestion',null, 'dmx_ingestion',2,true,5,now());
insert into admn.layer(name, layer_type, description, s3_path, schema_name, connector_id, is_active, created_by,created_date)
values( 'landing', 'rs', 'redshift landing',null, 'dmx_lnd',2,true,5,now());
insert into admn.layer(name, layer_type, description, s3_path, schema_name, connector_id, is_active, created_by,created_date)
values( 'staging', 'rs', 'redshift staging',null, 'dmx_stg',2,true,5,now());
insert into admn.layer(name, layer_type, description, s3_path, schema_name, connector_id, is_active, created_by,created_date)
values( 'dwh', 'rs', 'redshift datawarehouse',null, 'dmx_dwh',2,true,5,now());


commit;



-- get_users
-- http://127.0.0.1:5000/dmx/api/exchange?name=get_users&username=system&limit=10
SELECT u.user_id, u.firstname, u.lastname, u.email, u.is_active, u.created_date, u.modified_date, u.is_deleted, u.username, u.is_ldap_user, u.is_local_user, u.created_by, u.modified_by, (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(ugp))) FROM (SELECT ug.usergroup_id, ug.name, (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(p))) FROM (SELECT  m.module_id, m.name module_name,
json_agg((select x from ( select f.functionality_id,f.name)x) ) as functionality FROM admn.usergroup_functionality_mapping pf INNER JOIN admn.functionality_master f ON pf.functionality_id = f.functionality_id INNER JOIN admn.module m ON m.module_id = f.module_id WHERE pf.usergroup_id = ug.usergroup_id group by m.module_id, m.name) p) AS modules, (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(l))) FROM (SELECT lr1.layer_id, lr1.name FROM admn.layer lr1 INNER JOIN admn.usergroup_layer_mapping ulm on ulm.layer_id = lr1.layer_id INNER JOIN admn.usergroup_user_mapping ugp1 on ugp1.usergroup_id = ulm.usergroup_id and ugp1.user_id = u.user_id )l) layers FROM admn.usergroup ug INNER JOIN admn.usergroup_user_mapping m ON m.usergroup_id = ug.usergroup_id WHERE u.user_id = m.user_id) ugp) AS usergroupdetail, CASE WHEN a.user_id IS NULL THEN FALSE ELSE TRUE END AS is_admin FROM admn.user u LEFT JOIN (SELECT umap.user_id FROM admn.usergroup_user_mapping umap INNER JOIN admn.usergroup ug1 ON ug1.usergroup_id = umap.usergroup_id WHERE ug1.name = 'admin') a ON a.user_id = u.user_id WHERE (NULLIF(%(user_id)s,'null') IS NULL AND NULLIF(%(user_name)s,'null') IS NULL) OR ((NULLIF(%(user_id)s,'null') IS NOT NULL AND u.user_id = CAST(NULLIF(%(user_id)s,'null') AS INT)) OR (NULLIF(%(user_name)s,'null') IS NOT NULL AND u.username = NULLIF(%(user_name)s,'null'))) LIMIT cast(coalesce( NULLIF(%(limit)s,'null'),'100') as int)

-- add_usergroup
-- http://127.0.0.1:5000/dmx/api/exchange?name=add_usergroup&groupname=test_02&description=testusergroup
insert into admn.usergroup (name,description ,is_active,created_date ,created_by ) 
select %(groupname)s, %(description)s,  true,now(), cast(%(loggedin_userid)s as int)   
where exists(
select * from admn.user u inner join admn.usergroup_user_mapping ugp
on ugp.user_id = u.user_id and u.user_id = cast(%(loggedin_userid)s as int)
inner join admn.usergroup_functionality_mapping f on f.usergroup_id = ugp.usergroup_id
inner join admn.functionality_master fm on fm.functionality_id = f.functionality_id
and fm.name = 'ADMN.CREATE_USERGROUP'
);

--edit_usergroup
-- http://127.0.0.1:5000/dmx/api/exchange?name=edit_usergroup&description=testupdatehere&groupname=test_02&group_id=null&is_active=null&is_deleted=null
update admn.usergroup set description = (select case when NULLIF(%(description)s,'null') is NULL then ugp.description else %(description)s end as fn), is_active = (select case when NULLIF(%(is_active)s ,'null') is NULL then ugp.is_active else cast(NULLIF(%(is_active)s ,'null') as bool) end as active),
is_deleted = (select case when NULLIF(%(is_deleted)s ,'null') is NULL then ugp.is_deleted else cast(NULLIF(%(is_deleted)s ,'null') as bool) end as deleted),
modified_by = %(loggedin_userid)s,  modified_date = now() from (select * from admn.usergroup temp
where  (NULLIF(%(groupname)s,'null') is not NULL and temp.name = %(groupname)s) or (NULLIF(%(group_id)s, 'null') is not NULL and temp.usergroup_id = cast(NULLIF(%(group_id)s, 'null') as int) )  )ugp
where ugp.usergroup_id = admn.usergroup.usergroup_id and 
(select count(1) > 0 from admn.user user1
inner join admn.usergroup_user_mapping ugp1
on ugp1.user_id = user1.user_id and user1.user_id = cast(NULLIF(cast(%(loggedin_userid)s as varchar(10)),'null') as int)
inner join admn.usergroup_functionality_mapping f on f.usergroup_id = ugp1.usergroup_id
inner join admn.functionality_master fm on fm.functionality_id = f.functionality_id
and fm.name = 'ADMN.EDIT_USERGROUP') ;
select * from admn.usergroup where  (NULLIF(%(groupname)s,'null') is not NULL and name = %(groupname)s) or (NULLIF(%(group_id)s, 'null') is not NULL and usergroup_id = cast(NULLIF(%(group_id)s, 'null') as int) );

-- add user
-- http://127.0.0.1:5000/dmx/api/exchange/add_auth?module_name=hemalatta.seksaria&password=datamax&firstname=Hemalatta&lastname=Seksaria&is_ldap_user=false&is_local_user=true&email=hemalatta.seksaria@axtria.com&is_admin=false

insert into admn.user ( firstname, lastname, email, is_active, created_date, is_deleted, username, is_ldap_user, is_local_user, created_by, is_admin, password )  select %(firstname)s, %(lastname)s,%(email)s, %(is_active)s, now(), %(is_deleted)s, %(username)s, %(is_ldap_user)s, %(is_local_user)s, %(loggedin_user_id)s, %(is_admin)s, %(password)s   where exists( select * from admn.user u inner join admn.usergroup_user_mapping ugp on ugp.user_id = u.user_id and u.user_id = %(loggedin_user_id)s  inner join admn.usergroup_functionality_mapping f on f.usergroup_id = ugp.usergroup_id and f.name = 'ADMN.CREATE_USER' );
--edit user
-- http://127.0.0.1:5000/dmx/api/exchange?name=edit_user&username=null&user_id=9&firstname=Hemalatta&lastname=null&is_active=null&is_deleted=null&is_local_user=null&is_ldap_user=null&email=hemalatta.seksaria@axtria.com
UPDATE admn.user SET firstname = (SELECT CASE WHEN NULLIF(%(firstname)s,'null') IS NULL THEN u.firstname ELSE %(firstname)s END AS fn), lastname = (SELECT CASE WHEN NULLIF(%(lastname)s,'null') IS NULL THEN u.lastname ELSE %(lastname)s END AS LN), email = (SELECT CASE WHEN NULLIF(%(email)s,'null') IS NULL THEN u.email ELSE %(email)s END AS mail), is_active = (SELECT CASE WHEN NULLIF(%(is_active)s,'null') IS NULL THEN u.is_active ELSE CAST(NULLIF(%(is_active)s,'null') AS BOOL) END AS active), is_deleted = (SELECT CASE WHEN NULLIF(%(is_deleted)s,'null') IS NULL THEN u.is_deleted ELSE CAST(NULLIF(%(is_deleted)s,'null') AS BOOL) END AS deleted), is_ldap_user = (SELECT CASE WHEN NULLIF(%(is_ldap_user)s,'null') IS NULL THEN u.is_ldap_user ELSE CAST(NULLIF(%(is_ldap_user)s,'null') AS BOOL) END AS ldapuser), is_local_user = (SELECT CASE WHEN NULLIF(%(is_local_user)s,'null') IS NULL THEN u.is_local_user ELSE CAST(NULLIF(%(is_local_user)s,'null') AS BOOL) END AS localuser),is_admin = (SELECT CASE WHEN NULLIF(%(is_admin)s,'null') IS NULL THEN u.is_admin ELSE CAST(NULLIF(%(is_admin)s,'null') AS BOOL) END AS isadmin),modified_by = %(loggedin_userid)s, modified_date = NOW() FROM (SELECT * FROM admn.user TEMP WHERE (NULLIF(%(username)s,'null') IS NOT NULL AND lower(temp.username) = lower(%(username)s)) OR (NULLIF(%(user_id)s,'null') IS NOT NULL AND temp.user_id = CAST(NULLIF(%(user_id)s,'null') AS INT))) u WHERE u.user_id = admn.user.user_id AND (SELECT COUNT(1) > 0 FROM admn.user user1 INNER JOIN admn.usergroup_user_mapping ugp ON ugp.user_id = user1.user_id AND user1.user_id = CAST (NULLIF (CAST (%(loggedin_userid)s AS VARCHAR (10)),'null') AS INT) INNER JOIN admn.usergroup_functionality_mapping f ON f.usergroup_id = ugp.usergroup_id INNER JOIN admn.functionality_master fm ON fm.functionality_id = f.functionality_id AND fm.name = 'ADMN.EDIT_USER');
 
with ds as
(select usergroup_id from admn.usergroup  where lower(name) = 'admin' limit 1)
INSERT INTO admn.usergroup_user_mapping( usergroup_id, user_id, created_by,created_date, modified_by, modified_date)
select (select ds.usergroup_id from ds limit 1) as usergroup_id,
(select user_id from admn.user u where (NULLIF(%(username)s,'null') IS NOT NULL AND lower(u.username) = lower(%(username)s))OR (NULLIF(%(user_id)s,'null') IS NOT NULL AND u.user_id = CAST(NULLIF(%(user_id)s,'null') AS INT) )limit 1) as user_id,
%(loggedin_userid)s as created_by,  now()as created_date, %(loggedin_userid)s as modified_by, now() as modified_date
where ( NULLIF(%(is_admin)s,'null') IS not NULL and CAST(NULLIF(%(is_admin)s,'null') AS BOOL))
and ( select count(1) from admn.usergroup_user_mapping map where map.usergroup_id = ( select ds.usergroup_id from ds limit 1)
and map.user_id = (select user_id from admn.user u where (NULLIF(%(username)s,'null') IS NOT NULL AND lower(u.username) = lower(%(username)s))OR (NULLIF(%(user_id)s,'null') IS NOT NULL AND u.user_id = CAST(NULLIF(%(user_id)s,'null') AS INT) )limit 1) )=0;

DELETE  from admn.usergroup_user_mapping mapping USING (select usergroup_id from admn.usergroup  where lower(name) = 'admin')ugp
where mapping.usergroup_id = ugp.usergroup_id and ( NULLIF(%(is_admin)s,'null') IS not NULL and not CAST(NULLIF(%(is_admin)s,'null') AS BOOL)) 
and mapping.user_id = (select user_id from admn.user u where (NULLIF(%(username)s,'null') IS NOT NULL AND lower(u.username) = lower(%(username)s))OR (NULLIF(%(user_id)s,'null') IS NOT NULL AND u.user_id = CAST(NULLIF(%(user_id)s,'null') AS INT) )limit 1);
SELECT * FROM admn.user WHERE (NULLIF(%(username)s,'null') IS NOT NULL AND lower(username) = lower(%(username)s)) OR (NULLIF(%(user_id)s,'null') IS NOT NULL AND user_id = CAST(NULLIF(%(user_id)s,'null') AS INT));

-- add user to usergroup
-- http://127.0.0.1:5000/dmx/api/exchange?name=add_user_to_usergroup&group_id=4&user_ids=1,2,3,4,5&groupname=null
with ds1 as
(
select usergroup_id from admn.usergroup where
 (NULLIF(%(groupname)s,'null') is not NULL and name = %(groupname)s) or (NULLIF(%(group_id)s, 'null') is not NULL and usergroup_id = cast(NULLIF(%(group_id)s, 'null') as int))
limit 1
) ,
ds as
(
select x.user_id , ds1.usergroup_id from 
(select cast(user_id as int) user_id from unnest(string_to_array(%(user_ids)s,','))user_id)x
left join ds1 on 1=1
inner join admn.user u on u.user_id = x.user_id
left join admn.usergroup_user_mapping mapping on mapping.usergroup_id = ds1.usergroup_id and mapping.user_id = x.user_id
where mapping.usergroup_id  is null
)
insert into admn.usergroup_user_mapping(user_id, usergroup_id,created_date,created_by)
select user_id, usergroup_id,now(),%(loggedin_userid)s from ds
left join (select count(1) accesscount from admn.user user1 inner join admn.usergroup_user_mapping ugp1 on ugp1.user_id = user1.user_id 
and user1.user_id = cast(NULLIF(cast(%(loggedin_userid)s as varchar(10)),'null') as int) inner join admn.usergroup_functionality_mapping f on f.usergroup_id = ugp1.usergroup_id 
inner join admn.functionality_master fm on fm.functionality_id = f.functionality_id and fm.name = 'ADMN.EDIT_USERGROUP')access on 1=1
where access.accesscount > 0  ;
select u.username, ugp.name,u.user_id, ugp.usergroup_id,mapping.created_date from admn.usergroup_user_mapping mapping inner join admn.user u on u.user_id = mapping.user_id
inner join admn.usergroup ugp on ugp.usergroup_id = mapping.usergroup_id
where (NULLIF(%(groupname)s,'null') is not NULL and ugp.name = %(groupname)s) or (NULLIF(%(group_id)s, 'null') is not NULL and mapping.usergroup_id = cast(NULLIF(%(group_id)s, 'null') as int) );
-- remove user from usergroup
-- http://127.0.0.1:5000/dmx/api/exchange?name=remove_user_from_usergroup&group_id=4&user_ids=1,2,3,4,5&groupname=null
with ds1 as
  (
  select usergroup_id from admn.usergroup where
   (NULLIF(%(groupname)s,'null') is not NULL and name = %(groupname)s) or (NULLIF(%(group_id)s, 'null') is not NULL and usergroup_id = cast(NULLIF(%(group_id)s, 'null') as int))
  limit 1
  ) ,
  ds as
  (
  select x.user_id , ds1.usergroup_id from 
  (select cast(user_id as int) user_id from unnest(string_to_array(%(user_ids)s,','))user_id)x
  left join ds1 on 1=1
  inner join admn.user u on u.user_id = x.user_id
  inner join admn.usergroup_user_mapping mapping on mapping.usergroup_id = ds1.usergroup_id and mapping.user_id = x.user_id
  )
delete from admn.usergroup_user_mapping
using (select * from ds left join
  (select count(1) accesscount from admn.user user1 inner join admn.usergroup_user_mapping ugp1 on ugp1.user_id = user1.user_id 
  and user1.user_id = cast(NULLIF(cast(%(loggedin_userid)s as varchar(10)),'null') as int) inner join admn.usergroup_functionality_mapping f on f.usergroup_id = ugp1.usergroup_id 
  inner join admn.functionality_master fm on fm.functionality_id = f.functionality_id and fm.name = 'ADMN.EDIT_USERGROUP')access on 1=1
  where access.accesscount > 0)temp 
 where admn.usergroup_user_mapping.user_id = temp.user_id and admn.usergroup_user_mapping.usergroup_id = temp.usergroup_id;
 select u.username, ugp.name,u.user_id, ugp.usergroup_id,mapping.created_date from admn.usergroup_user_mapping mapping inner join admn.user u on u.user_id = mapping.user_id
inner join admn.usergroup ugp on ugp.usergroup_id = mapping.usergroup_id
where (NULLIF(%(groupname)s,'null') is not NULL and ugp.name = %(groupname)s) or (NULLIF(%(group_id)s, 'null') is not NULL and mapping.usergroup_id = cast(NULLIF(%(group_id)s, 'null') as int) );



--add_module_to_usergroup
--sample url : http://34.211.106.14/dmx_master/api/exchange?name=add_module_to_usergroup&module_id=2&usergroup_id=7&loggedin_userid=5

insert into admn.usergroup_functionality_mapping (functionality_id, usergroup_id,created_by, created_date) select fm.functionality_id, %(usergroup_id)s, %(loggedin_userid)s, now() from admn.functionality_master fm left join admn.usergroup_functionality_mapping mapping on mapping.functionality_id = fm.functionality_id and NULLIF(%(module_id)s, 'null') is not NULL and NULLIF(%(usergroup_id)s, 'null') is not NULL and mapping.usergroup_id = cast(NULLIF(%(usergroup_id)s, 'null') as int) where (select count(1) > 0 from admn.user user1 inner join admn.usergroup_user_mapping ugp1 on ugp1.user_id = user1.user_id and user1.user_id = cast(NULLIF(cast(%(loggedin_userid)s as varchar(10)),'null') as int) inner join admn.usergroup_functionality_mapping f on f.usergroup_id = ugp1.usergroup_id inner join admn.functionality_master fm on fm.functionality_id = f.functionality_id and fm.name = 'ADMN.EDIT_USERGROUP') and mapping.functionality_id is null and fm.module_id = cast(NULLIF(%(module_id)s, 'null') as int); select distinct fm.module_id, m.name as module ,ugp.name as usergroup, ugp.usergroup_id, mapping.created_date from admn.usergroup_functionality_mapping mapping inner join admn.functionality_master fm on fm.functionality_id = mapping.functionality_id inner join admn.module m on m.module_id = fm.module_id inner join admn.usergroup ugp on ugp.usergroup_id = mapping.usergroup_id and NULLIF(%(usergroup_id)s, 'null') is not NULL and mapping.usergroup_id = cast(NULLIF(%(usergroup_id)s, 'null') as int);

--remove_module_from_usergroup
-- sample url : http://34.211.106.14/dmx_master/api/exchange?name=remove_module_from_usergroup&module_id=8&usergroup_id=9&loggedin_userid=5&mapping_id=null

delete from admn.usergroup_functionality_mapping 
where admn.usergroup_functionality_mapping.usergroup_functionality_mapping_id in(
select mapping.usergroup_functionality_mapping_id from  admn.usergroup_functionality_mapping mapping
inner join admn.functionality_master fm 
on fm.functionality_id = mapping.functionality_id
inner join 
(select count(1) > 0 as access from admn.user user1
inner join admn.usergroup_user_mapping ugp1
on ugp1.user_id = user1.user_id and user1.user_id = cast(NULLIF(cast(%(loggedin_userid)s as varchar(10)),'null') as int)
inner join admn.usergroup_functionality_mapping f on f.usergroup_id = ugp1.usergroup_id
inner join admn.functionality_master fm on fm.functionality_id = f.functionality_id
and fm.name = 'ADMN.EDIT_USERGROUP')temp  
on temp.access = true
where
  (NULLIF(%(module_id)s, 'null') is  NULL and NULLIF(%(usergroup_id)s, 'null') is NULL and 
   NULLIF(%(mapping_id)s, 'null') is not NULL and usergroup_functionality_mapping_id = cast(NULLIF(%(mapping_id)s, 'null') as int)
  )
    or
    ( (NULLIF(%(module_id)s, 'null') is not NULL and fm.module_id = cast(NULLIF(%(module_id)s, 'null') as int))
		and
    (NULLIF(%(usergroup_id)s, 'null') is not NULL and mapping.usergroup_id = cast(NULLIF(%(usergroup_id)s, 'null') as int))
  )
);
select distinct fm.module_id, m.name as module ,ugp.name as usergroup, ugp.usergroup_id, mapping.created_date from admn.usergroup_functionality_mapping mapping inner join admn.functionality_master fm on fm.functionality_id = mapping.functionality_id inner join admn.module m on m.module_id = fm.module_id inner join admn.usergroup ugp on ugp.usergroup_id = mapping.usergroup_id and NULLIF(%(usergroup_id)s, 'null') is not NULL and mapping.usergroup_id = cast(NULLIF(%(usergroup_id)s, 'null') as int);

--add_layer_to_usergroup
--sample url : http://34.211.106.14/dmx_master/api/exchange?name=add_layer_to_usergroup&layer_ids=2,300&usergroup_id=10&loggedin_userid=5
with ds as
( 
select cast(layer_id as int) layer_id from unnest(string_to_array(%(layer_ids)s,','))layer_id
)
INSERT INTO admn.usergroup_layer_mapping
(
  layer_id,
  usergroup_id,
  created_by,
  created_date
)
SELECT l.layer_id,
       %(usergroup_id)s,
       %(loggedin_userid)s,
       NOW()
FROM ds 
INNER JOIN (SELECT COUNT(1) > 0 aces
       FROM admn.user user1
         INNER JOIN admn.usergroup_user_mapping ugp1
                 ON ugp1.user_id = user1.user_id
                AND user1.user_id = CAST (NULLIF (CAST (%(loggedin_userid)s AS VARCHAR (10)),'null') AS INT)
         INNER JOIN admn.usergroup_functionality_mapping f ON f.usergroup_id = ugp1.usergroup_id
         INNER JOIN admn.functionality_master fm
                 ON fm.functionality_id = f.functionality_id
                AND fm.name = 'ADMN.EDIT_USERGROUP')aces
on aces.aces = true
inner join admn.layer l on l.layer_id = ds.layer_id
LEFT JOIN admn.usergroup_layer_mapping mapping
         ON mapping.layer_id = l.layer_id
        AND NULLIF (%(usergroup_id)s,'null') IS NOT NULL
        AND mapping.usergroup_id = CAST (NULLIF (%(usergroup_id)s,'null') AS INT)
where   mapping.layer_id is null;
select l.layer_id, l.name as layer ,ugp.name as usergroup, ugp.usergroup_id, mapping.created_date from admn.usergroup_layer_mapping mapping inner join admn.usergroup ugp on ugp.usergroup_id = mapping.usergroup_id and NULLIF(%(usergroup_id)s, 'null') is not NULL and mapping.usergroup_id = cast(NULLIF(%(usergroup_id)s, 'null') as int)
inner join admn.layer l on l.layer_id = mapping.layer_id;
--remove_layer_from_usergroup
--sample url : http://34.211.106.14/dmx_master/api/exchange?name=remove_layer_from_usergroup&layer_ids=3&usergroup_id=10&loggedin_userid=5
with ds as
( 
select cast(layer_id as int) layer_id, cast(NULLIF(%(usergroup_id)s, 'null') as int) usergroup_id from unnest(string_to_array(%(layer_ids)s,','))layer_id
)
DELETE FROM admn.usergroup_layer_mapping
using ( 
select ds.layer_id, ds.usergroup_id from ds 
INNER JOIN (SELECT COUNT(1) > 0 aces
       FROM admn.user user1
         INNER JOIN admn.usergroup_user_mapping ugp1
                 ON ugp1.user_id = user1.user_id
                AND user1.user_id = CAST (NULLIF (CAST (%(loggedin_userid)s AS VARCHAR (10)),'null') AS INT)
         INNER JOIN admn.usergroup_functionality_mapping f ON f.usergroup_id = ugp1.usergroup_id
         INNER JOIN admn.functionality_master fm
                 ON fm.functionality_id = f.functionality_id
                AND fm.name = 'ADMN.EDIT_USERGROUP')aces
on aces.aces = true )temp
where temp.layer_id = admn.usergroup_layer_mapping.layer_id and temp.usergroup_id = admn.usergroup_layer_mapping.usergroup_id;
select l.layer_id, l.name as layer ,ugp.name as usergroup, ugp.usergroup_id, mapping.created_date 
from 
admn.usergroup_layer_mapping mapping inner join admn.usergroup ugp on ugp.usergroup_id = mapping.usergroup_id and NULLIF(%(usergroup_id)s, 'null') is not NULL and mapping.usergroup_id = cast(NULLIF(%(usergroup_id)s, 'null') as int)
inner join admn.layer l on l.layer_id = mapping.layer_id; 

-- add module_layer_mapping
-- sample url : http://34.211.106.14/dmx_master/api/exchange?name=add_module_layer_mapping&layer_ids=2,3&module_ids=9&loggedin_userid=5
WITH ds AS
(
  SELECT CAST(layer_id AS INT) layer_id
  FROM UNNEST(STRING_TO_ARRAY(% (layer_ids) s,',')) layer_id
),
ds1 AS
(
  SELECT x.module_id,
         ds.layer_id
  FROM (SELECT CAST(module_id AS INT) module_id
        FROM UNNEST(STRING_TO_ARRAY(% (module_ids) s,',')) module_id) x
    JOIN ds ON 1 = 1
) INSERT INTO admn.module_layer_mapping
(
  module_id,
  layer_id,
  created_by,
  created_date
)
SELECT ds1.module_id,
       ds1.layer_id,
       %(loggedin_userid) s,
       NOW()
FROM ds1
  INNER JOIN (SELECT COUNT(1) > 0 aces
              FROM admn.user user1
                INNER JOIN admn.usergroup_user_mapping ugp1
                        ON ugp1.user_id = user1.user_id
                       AND user1.user_id = CAST (NULLIF (CAST (% (loggedin_userid) s AS VARCHAR (10)),'null') AS INT)
                INNER JOIN admn.usergroup_functionality_mapping f ON f.usergroup_id = ugp1.usergroup_id
                INNER JOIN admn.functionality_master fm
                        ON fm.functionality_id = f.functionality_id
                       AND fm.name = 'ADMN.EDIT_USERGROUP') aces ON aces.aces = TRUE
  INNER JOIN admn.layer l ON l.layer_id = ds1.layer_id
  INNER JOIN admn.module m ON m.module_id = ds1.module_id
  LEFT JOIN admn.module_layer_mapping mapping
         ON mapping.layer_id = l.layer_id
        AND mapping.module_id = m.module_id
WHERE mapping.module_layer_mapping_id IS NULL;

SELECT l.layer_id,
       l.name AS layer,
       m.module_id,
       m.name AS module,
       mapping.created_date
FROM admn.module_layer_mapping mapping
  INNER JOIN admn.module m ON m.module_id = mapping.module_id
  INNER JOIN admn.layer l ON l.layer_id = mapping.layer_id;


--remove module_layer_mapping
--sample url :http://34.211.106.14/dmx_master/api/exchange?name=remove_module_layer_mapping&layer_ids=2,3&module_ids=9&mapping_ids=5&loggedin_userid=5
with ds1 as
(
	select case when layer_id = 'null' then -1 else cast(layer_id as int) end as layer_id from unnest(string_to_array(%(layer_ids)s,','))layer_id

),
ds as
(
	select x.module_id, ds1.layer_id from
	(
		select case when module_id = 'null' then -1 else cast(module_id as int) end as module_id from unnest(string_to_array(%(module_ids)s,','))module_id
	)x
	join ds1 on 1=1
	inner join admn.layer on admn.layer.layer_id = ds1.layer_id
	inner join admn.module on admn.module.module_id = x.module_id
	where x.module_id != -1 and ds1.layer_id != -1
	
)
DELETE FROM admn.module_layer_mapping
using ( 
select ds.layer_id, ds.module_id from ds 
INNER JOIN (SELECT COUNT(1) > 0 aces
       FROM admn.user user1
         INNER JOIN admn.usergroup_user_mapping ugp1
                 ON ugp1.user_id = user1.user_id
                AND user1.user_id = CAST (NULLIF (CAST (%(loggedin_userid)s AS VARCHAR (10)),'null') AS INT)
         INNER JOIN admn.usergroup_functionality_mapping f ON f.usergroup_id = ugp1.usergroup_id
         INNER JOIN admn.functionality_master fm
                 ON fm.functionality_id = f.functionality_id
                AND fm.name = 'ADMN.EDIT_USERGROUP')aces
on aces.aces = true )temp
where temp.layer_id = admn.module_layer_mapping.layer_id and temp.module_id = admn.module_layer_mapping.module_id;
with ds as
(
select * from (
select case when module_layer_mapping_id = 'null' then -1 else cast(module_layer_mapping_id as int) end as module_layer_mapping_id from unnest(string_to_array(%(mapping_ids)s,','))module_layer_mapping_id) temp
where temp.module_layer_mapping_id != -1

)
DELETE FROM admn.module_layer_mapping
using ( 
select ds.module_layer_mapping_id from ds 
INNER JOIN (SELECT COUNT(1) > 0 aces
       FROM admn.user user1
         INNER JOIN admn.usergroup_user_mapping ugp1
                 ON ugp1.user_id = user1.user_id
                AND user1.user_id = CAST (NULLIF (CAST (%(loggedin_userid)s AS VARCHAR (10)),'null') AS INT)
         INNER JOIN admn.usergroup_functionality_mapping f ON f.usergroup_id = ugp1.usergroup_id
         INNER JOIN admn.functionality_master fm
                 ON fm.functionality_id = f.functionality_id
                AND fm.name = 'ADMN.EDIT_USERGROUP')aces
on aces.aces = true )temp
where temp.module_layer_mapping_id = admn.module_layer_mapping.module_layer_mapping_id;

select l.layer_id, l.name as layer, m.module_id, m.name as module , mapping.created_date 
from admn.module_layer_mapping mapping 
inner join admn.module m on m.module_id = mapping.module_id 
inner join admn.layer l on l.layer_id = mapping.layer_id;
-- test_admn_query
select u.user_id, ( select array_to_json(array_agg(row_to_json(ugp))) from( select ug.usergroup_id, ug.name, (select array_to_json(array_agg(row_to_json(p))) from ( select f.functionality_id, f.name, m.module_id, m.name module_name from admn.usergroup_functionality_mapping pf inner join admn.functionality_master f on pf.functionality_id = f.functionality_id inner join admn.module m on m.module_id = f.module_id where pf.usergroup_id = ug.usergroup_id )p ) as permissions from admn.usergroup ug inner join admn.usergroup_user_mapping m on m.usergroup_id = ug.usergroup_id where u.user_id = m.user_id )ugp )as usergroupdetail from admn.user u where (NULLIF(%(user_id)s, 'null') is NULL and NULLIF(%(user_name)s, 'null') is NULL) or (( NULLIF(%(user_id)s, 'null') is not NULL and u.user_id = cast(NULLIF(%(user_id)s, 'null') as int) ) or (NULLIF(%(user_name)s, 'null') is not NULL or u.username = NULLIF(%(user_name)s, 'null') ))



--get_usergroups
--description : to retrieve the usergroups and user member details
--sample url http://34.211.106.14/dmx_dev/api/exchange?name=get_usergroups&userdetail=true&group_id=null&accessdetail=true
select ugp.*, count(1) userscount, case when %(userdetail)s = TRUE then json_agg((select x from (select u.firstname, u.lastname, u.is_active,u.created_date, u.modified_date, u.is_deleted, u.user_id, u.username, u.is_ldap_user, u.is_local_user, u.created_by,u.modified_by,u.is_admin,u.email)as x)) else null end as userdetails,
case when %(accessdetail)s = TRUE then (select ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(p))) FROM (SELECT  m.module_id, m.name module_name,
json_agg((select x from ( select f.functionality_id,f.name)x) ) as functionality FROM admn.usergroup_functionality_mapping pf INNER JOIN admn.functionality_master f
 ON pf.functionality_id = f.functionality_id INNER JOIN admn.module m ON m.module_id = f.module_id WHERE pf.usergroup_id = ugp.usergroup_id group by m.module_id, m.name) p) else null end 
 as  modules,
case when  %(accessdetail)s = TRUE then (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(l))) FROM (SELECT lr1.layer_id, lr1.name FROM admn.layer lr1 INNER JOIN admn.usergroup_layer_mapping ulm on ulm.layer_id = lr1.layer_id and ugp.usergroup_id = ulm.usergroup_id)l)  else null end 
 as  layers
from admn.usergroup ugp inner join admn.usergroup_user_mapping map on map.usergroup_id = ugp.usergroup_id inner join admn.user u on u.user_id = map.user_id where (NULLIF(%(group_id)s, 'null') is NULL or ugp.usergroup_id = cast(NULLIF(%(group_id)s, 'null') as int)) group by ugp.name,ugp.description,ugp.is_active,ugp.created_date,ugp.modified_date,ugp.is_deleted,ugp.usergroup_id,ugp.created_by




--add_access_request
--http://34.211.106.14/dmx_admn/api/exchange?name=add_access_request&group_ids=2,3&layer_ids=2,3&module_ids=null&functionality_ids=null&type=group
with dsusergroup as
(
select ugp.usergroup_id from (select  usergroup_id from unnest(string_to_array(%(group_ids)s,','))usergroup_id)x
inner join admn.usergroup ugp on nullif(x.usergroup_id,'null') is not null and ugp.usergroup_id = cast(x.usergroup_id as int)
)

,
dslayer as
(
select l.layer_id from (select  layer_id from unnest(string_to_array(%(layer_ids)s,','))layer_id)x
inner join admn.layer l on nullif(x.layer_id,'null') is not null and l.layer_id = cast(x.layer_id as int)
)
,
dsmodule as
(
select m.module_id from admn.module m
inner join 
(select  module_id from unnest(string_to_array(%(module_ids)s,','))module_id)x
 on nullif(x.module_id,'null') is not null and m.module_id = cast(x.module_id as int)
)
,
dsfunctionality as
(
select f.functionality_id from (select  functionality_id from unnest(string_to_array(%(functionality_ids)s,','))functionality_id)x
inner join admn.functionality_master f on nullif(x.functionality_id,'null') is not null and f.functionality_id = cast(x.functionality_id as int)
)
insert into admn.access_request(request_type, user_id,usergroup_id,layer_id, status, module_id, functionality_id,created_by,created_date,  modified_by,modified_date)
values( (select case when nullif(%(type)s,'null') is null or %(type)s = 'self' then 'self' else 'group' end as request_type), cast(%(loggedin_userid)s as int), array(select usergroup_id from dsusergroup),array(select layer_id from dslayer), 'new',array(select module_id from dsmodule),  array(select functionality_id from dsfunctionality),cast(%(loggedin_userid)s as int), now(), cast(%(loggedin_userid)s as int), now());

select * from admn.access_request where request_id = currval('admn.access_request_request_id_seq') ;


--get_access_request
--http://34.211.106.14/dmx_admn/api/exchange?name=get_access_request&userid=null&requestid=null
select request_id ,
 json_object_agg(ugp.usergroup_id, ugp.name)filter (where ugp.usergroup_id is not null) usergroup,
 json_object_agg(m.module_id, m.name) filter (where m.module_id is not null) module,
 json_object_agg(l.layer_id, l.name ) filter (where l.layer_id is not null) layer, 
 json_object_agg(f.functionality_id, f.name) filter (where f.functionality_id is not null)  functionality 
from (
select *, unnest(usergroup_id) ugp_id,unnest(module_id) m_id, unnest(layer_id) l_id ,unnest(functionality_id) f_id from admn.access_request r)x
left join admn.usergroup ugp on ugp.usergroup_id = x.ugp_id
left join admn.module m on m.module_id = x.m_id
left join admn.layer l on l.layer_id = x.l_id
left join admn.functionality_master f on f.functionality_id = x.f_id
where nullif(%(requestid)s,'null') is null or request_id = cast(nullif(%(requestid)s,'null') as int) and
nullif(%(userid)s,'null') is null or  user_id = cast(nullif(%(userid)s,'null') as int)
group by request_id


--http://34.211.106.14/dmx_admn/api/exchange?name=execute_access_request&request_id=5&action=approve

select admn.fn_action_request_process (%(request_id)s, %(action)s,%(loggedin_userid)s) as data
