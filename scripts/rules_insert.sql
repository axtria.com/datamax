﻿INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1023,
  'CheckDateColumnValue(datecolumnvarchar)',
  'Check if  values are before/after/equals/not equals a date in given date column',
  'select dmx_id, FALSE as status, '''' as message from {schema}.{tablename} where not fn_comparedate({colname} ,''{operator}'' ,''{inputdateval}'',''{dateformat}'' )',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "creationdate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "operator", "value": ">|<|=|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "inputdateval", "value": "2019-02-02", "datatype": "date"}, {"type": "param", "param": "dateformat", "value": "%Y%m%d", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'datevarcha',
  'Check if values of the {colname} is {operator} than {inputdateval} with format {dateformat}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1028,
  'CheckTotalNulls',
  'Check if total count of null values in table is less than threshold ',
  'SELECT ''select sum(null_count) as total_null_count from (select \''spacer\'' AS column_name, 0 AS null_count '' as query union all SELECT ''union all select \'''' || "column" || ''\'', count(1) from '' || tablename || '' where '' || "column" || '' is null'' as query FROM pg_table_def WHERE tablename = {tablename} union all select '')'' as query; ',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "thresholdval", "value": "30", "datatype": "numeric"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'table',
  TRUE,
  'any',
  'Check if total count of null values in the given file is less than the threshold - {thresholdval}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1027,
  'CheckReach',
  'Check if reach is <,<=,>,>=,= threshold ( Reach is %age of total population, e.g. %age of total calls <= total calls)',
  'select  0 as DMX_ID ,((select count(1) from {schema}.{tablename} where {colname} > {thresholdval})/(select count(1)*1.0 from {schema}.{tablename}))*100 > {thresholdreach} as status, '''' as message',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "column", "param": "colname", "value": "", "datatype": "numeric"}, {"type": "param", "param": "thresholdval", "value": "5", "datatype": "numeric"}, {"type": "param", "param": "thresholdreach", "value": "60", "datatype": "numeric"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'column',
  TRUE,
  'numeric',
  'Check if percentage of records which are greater than {thresholdval} in {colname} is less than {thresholdreach}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1014,
  'CheckAlphanumeric',
  'Check if all values in column is alphanumeric',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} !~ ''^[a-zA-Z0-9]*$''',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "id", "datatype": "varchar"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in {colname} are alphanumeric'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  9002,
  'CheckNumericType',
  'Check if all values in column are of float data type',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} !~ ''\^[+-]?((\\d+(\.\\d*)?)|(\.\\d+))\$''',
  NULL,
  'system',
  '2019-05-27',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "NRX_AMOUNT", "datatype": "any"}, {"param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'check if all the values in {colname} are of numeric data type'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  9001,
  'CheckDateType',
  'Check if values follows input Date format',
  'with varformat as (select case when ''{format}'' =  ''YYYY-MM-DD'' then ''^[0-9][0-9][0-9][0-9]-(((0)[0-9])|((1)[0-2]))-(((0)[0-9])|([1-2][0-9])|([3][0-1]))$'' when ''{format}'' =  ''YYYY-DD-MM'' then ''^[0-9][0-9][0-9][0-9]-(((0)[0-9])|([1-2][0-9])|([3][0-1]))-(((0)[0-9])|((1)[0-2]))$'' when ''{format}'' =  ''YYYY/MM/DD'' then ''^[0-9][0-9][0-9][0-9]/(((0)[0-9])|((1)[0-2]))/(((0)[0-9])|([1-2][0-9])|([3][0-1]))$'' when ''{format}'' =  ''YYYY/DD/MM'' then ''^[0-9][0-9][0-9][0-9]/(((0)[0-9])|([1-2][0-9)|([3][0-1]))/(((0)[0-9])|((1)[0-2]))$'' end as format) select dmx_id, FALSE as status, '''' as message from {schema}.{tablename} cross join varformat where {colname} !~ varformat.format',
  NULL,
  'system',
  '2019-05-27',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "salesdate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "format", "value": "%Y%m%d", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'date',
  'check if all the values in {colname} are of date data type with format {format}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1024,
  'CheckDateColumnRange(datecolumnvarchar)',
  'Check if values of a date column is in the given date range',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} WHERE not fn_checkdaterange({colname} ,''{startdateval}'' , ''{enddateval}'',''{dateformat}'')',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "salesdate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "startdateval", "value": "2019-01-01", "datatype": "date"}, {"type": "param", "param": "dateformat", "value": "%Y%m%d", "datatype": "varchar"}, {"type": "param", "param": "enddateval", "value": "2019-04-01", "datatype": "date"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'datevarcha',
  'Check if values of the {colname} is in the between {startdateval} and {enddateval} which follow date format {dateformat}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1022,
  'CompareDateColumns(datecolumnvarchar)',
  'Check value of End Date column is >/</>=/<=/= than or equal to Start Date value',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} WHERE not fn_comparedatecolumns({colstartdate}, ''{operator}'', {colenddate},''{colstartdateformat}'',''{colenddateformat}'')',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colstartdate", "value": "startdate", "datatype": "date"}, {"type": "column", "param": "colenddate", "value": "enddate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "colstartdateformat", "value": "%Y%m%d", "datatype": "varchar"}, {"type": "param", "param": "colenddateformat", "value": "%Y%m%d", "datatype": "varchar"}, {"type": "param", "param": "operator", "value": ">|<|=|<=|>=", "datatype": "picklist"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'multicolumn',
  TRUE,
  'datevarcha',
  'Check value of {colenddate} (with format {colenddateformat}) is {operator} than {colstartdate} (with format {colstartdateformat})'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  2001,
  'checkDuplicateColumn',
  'check if the file has any duplicate column name',
  NULL,
  'is_column_duplicate',
  'system',
  NULL,
  NULL,
  NULL,
  CAST('[{"param": "bucket", "value": ""}, {"param": "folder", "value": ""}, {"type": "param", "param": "delimiter", "value": "", "datatype": "varchar"}]' AS JSONB),
  's3',
  TRUE,
  'file',
  'Check if the file has duplicate column name, delimited by {delimiter}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1021,
  'CheckWeekDay(datecolumnvarchar)',
  'Check that no Date shall fall on certain week days ( M/T/W/T/F/S/S)',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} WHERE  not fn_extract(''DOW'',{colname},''{dateformat}'') in ({weekdaynum})',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "creationdate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"text": "week day", "type": "param", "param": "weekdaynum", "value": "2", "datatype": "numeric", "description": "values like 2 or 2,3,4. 0 -Sunday...6-Saturday"}, {"type": "param", "param": "dateformat", "value": "%Y%m%d", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'datevarcha',
  'Check that no date in {colname} (with format {dateformat}) should fall on specific week day {weekdaynum} (values like 2,3,4,5 where 0 - Sunday... 6-Saturday)'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1018,
  'CheckRegex',
  'Check if all values in column matches with input regular expression ',
  'select dmx_id,FALSE as status , '''' as message from {schema}.{tablename} where {colname} !~ ''{regex}''',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "salesteam", "datatype": "varchar"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "regex", "value": "[^A-Z]", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in {colname} matches with the {regex} regular expression'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1026,
  'CheckColumnRatio',
  'Check if ratio of values of two columns is <,<=,>,>=,= threshold',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} where COALESCE({col1} / NULLIF({col2},0), 0){operator} {thresholdval}',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "column", "param": "col1", "value": "trx", "datatype": "numeric"}, {"type": "column", "param": "col2", "value": "nrx", "datatype": "numeric"}, {"type": "param", "param": "operator", "value": ">|<|=|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "thresholdval", "value": "1", "datatype": "numeric"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'numeric',
  'Check if {col1} / {col2} is {operator} {thresholdval}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1017,
  'CheckCaptialAlphabets',
  'Check if all values in column consist Capital Alphabets',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} !~ ''[^A-Z]''',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "salesteam", "datatype": "varchar"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in {colname} consists of characters in upper case'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1016,
  'CheckCharacter',
  'Check if all values in column consist characters only',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} !~* ''[^A-Z]''',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "name", "datatype": "varchar"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in {colname} consists of characters only'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1015,
  'CheckEmailFormat',
  'Check if all values in column is of email format',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} NOT LIKE ''%_@__%.__%''',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "email", "datatype": "varchar"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if the values of {colname} are of email format'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1033,
  'CheckAggForDuration',
  'Check if aggregated sell for a certain duration ( month/quarter/year) <,<=,>,>= certain amount of sell',
  'select DMX_ID, status, '''' as message from (select duration, false as status,value from (select {measure}({colname}) value,duration from (select {colname}, EXTRACT(''{valduration}'' from {coldate}) as duration from {schema}.{tablename})temp group by duration) x ) y inner join {schema}.{tablename} on y.duration = EXTRACT(''{valduration}'' from {tablename}.{coldate}) where (not y.value {operator} {threshold})',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "column", "param": "colname", "value": "sales_amt", "datatype": "numeric"}, {"type": "column", "param": "coldate", "value": "eventdate", "datatype": "varchar"}, {"type": "param", "param": "operator", "value": ">|<|=|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "threshold", "value": "1000", "datatype": "numeric"}, {"type": "param", "param": "measure", "value": "sum|count|max|min", "datatype": "picklist"}, {"type": "param", "param": "valduration", "value": "year", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'date',
  'Check if aggregated {measure} of {colname} for a given {valduration} duration in {coldate} is {operator} than {threshold}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1030,
  'CompareDateColumns',
  'Check value of End Date column is >/</>=/<=/= than or equal to Start Date value',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} WHERE not ({colstartdate} {operator} {colenddate})',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colstartdate", "value": "startdate", "datatype": "date"}, {"type": "column", "param": "colenddate", "value": "enddate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "operator", "value": ">|<|=|<=|>=", "datatype": "picklist"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'multicolumn',
  TRUE,
  'date',
  'Check value of {colenddate}  is {operator} than {colstartdate}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1032,
  'CheckDateColumnRange',
  'Check if values of a date column is in the given date range',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} WHERE not ( {colname} between to_date(''{startdateval}'',''{dateformat}'') and to_date(''{enddateval}'',''{dateformat}''))',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "salesdate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "startdateval", "value": "2019-01-01", "datatype": "date"}, {"type": "param", "param": "enddateval", "value": "2019-04-01", "datatype": "date"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'date',
  'Check if values of the {colname} is in the between {startdateval} and {enddateval}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1031,
  'CheckDateColumnValue',
  'Check if  values are before/after/equals/not equals a date in given date column',
  'select dmx_id, FALSE as status, '''' as message from {schema}.{tablename} where not ({colname} {operator} to_date(''{inputdateval}'',''{dateformat}'' ))',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "creationdate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "operator", "value": ">|<|=|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "inputdateval", "value": "2019-02-02", "datatype": "date"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'date',
  'Check if values of the {colname} is {operator} than {inputdateval}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1013,
  'CheckDateDataType',
  'Check if all values in column is of date data type',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} !~ ''\^\\d\\d\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])\$'' ',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "salesdate", "datatype": "varchar"}, {"param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in {colname} are of date data type'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1020,
  'CheckAggForDuration(datecolumnvarchar)',
  'Check if aggregated sell for a certain duration ( month/quarter/year) <,<=,>,>= certain amount of sell',
  'select DMX_ID, status, '''' as message from (select duration, false as status,value from (select {measure}({colname}) value,duration from (select {colname}, fn_extract(''{valduration}'',{coldate},''{dateformat}'') as duration from {schema}.{tablename})temp group by duration) x ) y inner join {schema}.{tablename} on y.duration = fn_extract(''{valduration}'',{tablename}.{coldate},''{dateformat}'') where (not y.value {operator} {threshold})',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "column", "param": "colname", "value": "sales_amt", "datatype": "numeric"}, {"type": "column", "param": "coldate", "value": "eventdate", "datatype": "varchar"}, {"type": "param", "param": "operator", "value": ">|<|=|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "threshold", "value": "1000", "datatype": "numeric"}, {"type": "param", "param": "dateformat", "value": "%Y%m%d", "datatype": "varchar"}, {"type": "param", "param": "measure", "value": "sum|count|max|min", "datatype": "picklist"}, {"type": "param", "param": "valduration", "value": "year", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'datevarcha',
  'Check if aggregated {measure} of {colname} for a given {valduration} duration in {coldate} (with format {dateformat})  is {operator} than {threshold}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1010,
  'CheckColumnValue',
  'Check column value is ( =, >, <,>=,<=) the threshold provided',
  'select dmx_id, FALSE as status, '''' as message from {schema}.{tablename} where {colname} {operator} {thresholdval}',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "column", "param": "colname", "value": "calls", "datatype": "varchar"}, {"type": "param", "param": "operator", "value": "<|>|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "thresholdval", "value": "10", "datatype": "numeric"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'numeric',
  'Check if {colname} is {operator} than {thresholdval}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1009,
  'CheckColumnLength',
  'Check length of column value is ( =, >, <,>=,<=) the threshold provided',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} where not (length({colname}) {operator} {thresholdval})',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "column", "param": "colname", "value": "ID", "datatype": "varchar"}, {"type": "param", "param": "operator", "value": "<|>|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "thresholdval", "value": "10", "datatype": "numeric"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if {colname} has character length {operator} than {thresholdval}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1008,
  'CheckUniqueValues',
  'Check if row values are in the list of unique set of values',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} WHERE {colname} NOT IN {listval}',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "column", "param": "colname", "value": "channels", "datatype": "varchar"}, {"type": "param", "param": "listval", "value": "", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if row values of {colname} are in the {listval}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1007,
  'CheckTableRowCount',
  'Check Tables row count is <, <= ,>= , > , = the threshold provided',
  'select case when t.total {operator} {thresholdval} then True else false end as status,case when t.total {operator} {thresholdval} then (''total rows ('' || cast(t.total as varchar) || '') {operator} {thresholdval} '') else (''total rows ( '' || cast(t.total as varchar) || '') not {operator} 1000 '') end as message , 0 as DMX_ID  from(select count(1) total from {schema}.{tablename}) t',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "operator", "value": ">|<|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "thresholdval", "value": "1000", "datatype": "numeric"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'column',
  TRUE,
  'any',
  'Check if the row count is {operator} than {thresholdval}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1004,
  'CheckFloatDataType',
  'Check if all values in column are of float data type',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} !~ ''\^[+-]?((\\d+(\.\\d*)?)|(\.\\d+))\$''',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "NRX_AMOUNT", "datatype": "any"}, {"param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in the {colname} are float data type'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1003,
  'CheckYearMonthFormat',
  'Check if all values in column follows YYYYMM format',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} !~ ''\^\\d\\d\\d\\d\\d\\d\$'' ',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "YEARMONTH", "datatype": "varchar"}, {"param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in the {colname} are following YYYYMM format'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1002,
  'CheckIntegerDataType',
  'Check if all values in column is of integer data type',
  'select dmx_id,FALSE as status, '''' as message from {schema}.{tablename} where {colname} !~ ''\^[+-]?(\\d+)\$'' ',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "ID", "datatype": "any"}, {"param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in the {colname} is of integer data type'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1012,
  'CheckReferenceColumns',
  'Check Reference Column based on two tables',
  'select a.dmx_id,FALSE as status, '''' as message FROM {schema}.{tablename} a left join {schema2}.{tablename2} b on a.{joincoltbl1}=b.{joincoltbl2} WHERE b.{joincoltbl2} IS NULL',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "tablename2", "value": "", "datatype": "varchar"}, {"type": "column", "param": "joincoltbl1", "value": "productid", "datatype": "varchar"}, {"type": "param", "param": "joincoltbl2", "value": "productid", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}, {"type": "param", "param": "schema2", "value": "", "datatype": "varchar"}]' AS JSONB),
  'multitable',
  TRUE,
  'any',
  'Check if current file has all the values in the given {joincoltbl1} matched with {joincoltbl2} of schema - {schema2} and table - {tablename2}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1001,
  'CheckNullThreshold',
  'Check if count of null values not greater than threshold value',
  'select s.total*100.0/nullif(rowcount,0) > {threshold}  as status,  (''total null count : '' || cast(s.total as varchar(100)) || '' and threshold is '' || cast({threshold} as varchar(100 ))) as message, 0 as DMX_ID from ( select (select count(1) as total from {schema}.{tablename} where {colname} is null)total,(select count(1)  from {schema}.{tablename})rowcount) s',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "ID", "datatype": "any"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "threshold", "value": "0", "datatype": "numeric"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'column',
  TRUE,
  'any',
  'Check if count of null values in the {colname} is not greater than {threshold}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1034,
  'CheckNotNULL',
  'Check if the column is not null',
  'select False  as status,  '''' as message, DMX_ID as DMX_ID from  {schema}.{tablename} where {colname} is null',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "ID", "datatype": "any"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'column',
  TRUE,
  'any',
  'Check column {colname} is not null'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1029,
  'CheckWeekDay',
  'Check that no Date shall fall on certain week days ( M/T/W/T/F/S/S)',
  'SELECT dmx_id, FALSE as status, '''' as message FROM {schema}.{tablename} WHERE  not 
  extract(DOW from {colname}) in ({weekdaynum})',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "creationdate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"text": "week day", "type": "param", "param": "weekdaynum", "value": "2", "datatype": "numeric", "description": "values like 2 or 2,3,4. 0 -Sunday...6-Saturday"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'date',
  'Check that no date in {colname} should fall on specific week day {weekdaynum} (values like 2,3,4,5 where 0 - Sunday... 6-Saturday)'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1005,
  'CheckColumnUnique',
  'Check if all values in column are unique',
  'select case when t.total = 0 then true else false end as status, cast(t.message as varchar(248)) || ''..'' as message, 0 as DMX_ID from(select listagg({colname},'','') as message, count(1) as total from       (select {colname}, count(1) as total from {schema}.{tablename} group by {colname} having count(1) >1 )s)t',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "ID", "datatype": "any"}, {"param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'column',
  TRUE,
  'any',
  'Check if all the values in the {colname} are unique'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  2002,
  'checkColumnOrder',
  'check if the file has the specified order',
  NULL,
  'is_column_ordered',
  'system',
  NULL,
  NULL,
  NULL,
  CAST('[{"param": "bucket", "value": ""}, {"param": "folder", "value": ""}, {"type": "param", "param": "order", "value": ", , ,", "datatype": "varchar"}, {"type": "param", "param": "delimiter", "value": "", "datatype": "varchar"}, {"type": "param", "param": "searchposition", "value": "C", "datatype": "varchar"}]' AS JSONB),
  's3',
  TRUE,
  'file',
  'Check if the file has columns in specified order - {order} delimited by {delimiter}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  2003,
  'checkColumnExist',
  'check if the file has the specified columns',
  NULL,
  'is_column_exist',
  'system',
  NULL,
  NULL,
  NULL,
  CAST('[{"param": "bucket", "value": ""}, {"param": "folder", "value": ""}, {"type": "param", "param": "columns", "value": ", , ,", "datatype": "varchar"}, {"type": "param", "param": "delimiter", "value": "", "datatype": "varchar"}]' AS JSONB),
  's3',
  TRUE,
  'file',
  'Check if the file has the columns - {columns} delimited by {delimiter}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1035,
  'CheckNotBLANKNULL',
  'Check if the column is not null or blank',
  'select False  as status,  '''' as message, DMX_ID as DMX_ID from  {schema}.{tablename} where {colname} is null or len(trim({colname})) = 0',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "ID", "datatype": "any"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'column',
  TRUE,
  'any',
  'Check column {colname} is not null'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1025,
  'CheckDateFormat(datecolumnvarchar)',
  'Check if values follows input Date format',
  'with varformat as (select case when ''{dateformat}'' =  ''YYYY-MM-DD'' then ''^[0-9][0-9][0-9][0-9]-(((0)[0-9])|((1)[0-2]))-(((0)[0-9])|([1-2][0-9])|([3][0-1]))$'' when ''{dateformat}'' =  ''YYYY-DD-MM'' then ''^[0-9][0-9][0-9][0-9]-(((0)[0-9])|([1-2][0-9])|([3][0-1]))-(((0)[0-9])|((1)[0-2]))$'' when ''{dateformat}'' =  ''YYYY/MM/DD'' then ''^[0-9][0-9][0-9][0-9]/(((0)[0-9])|((1)[0-2]))/(((0)[0-9])|([1-2][0-9])|([3][0-1]))$'' when ''{dateformat}'' =  ''YYYY/DD/MM'' then ''^[0-9][0-9][0-9][0-9]/(((0)[0-9])|([1-2][0-9)|([3][0-1]))/(((0)[0-9])|((1)[0-2]))$'' when ''{dateformat}'' = ''DD-MON-YY'' then ''^(((0)[0-9])|([1-2][0-9])|([3][0-1]))-(JAN(UARY)?|FEB(RUARY)?|MAR(CH)?|APR(IL)?|MAY|JUN(E)?|JUL(Y)?|AUG(UST)?|SEP(TEMBER)?|OCT(OBER)?|NOV(EMBER)?|DEC(EMBER)?)-([0-9][0-9])$'' end as format) select dmx_id, FALSE as status, '''' as message from {schema}.{tablename} cross join varformat where {colname} !~ varformat.format',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "column", "param": "colname", "value": "salesdate", "datatype": "date"}, {"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "format", "value": "YYY-MM-DD", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}]' AS JSONB),
  'row',
  TRUE,
  'varchar',
  'Check if all the values in {colname} follows date format - {dateformat}'
);

INSERT INTO rules
(
  id,
  name,
  description,
  redshiftquery,
  pyfunctionname,
  createdby,
  creationdate,
  modifiedby,
  modificatindate,
  parameters,
  level,
  is_active,
  datatype,
  ui_description
)
VALUES
(
  1036,
  'CheckReferenceThreshold',
  'Check referential inetgrity threshold',
  'select  '''' as message, 0 as DMX_ID, not(((select count(distinct a.{joincoltbl1}) FROM {schema}.{tablename} a left join {schema2}.{tablename2} b on a.{joincoltbl1}=b.{joincoltbl2} WHERE b.{joincoltbl2} IS NULL)*100.0/(select count(distinct {joincoltbl1}) as total from {schema}.{tablename} )) {operator} {thresholdval}) as status',
  NULL,
  'system',
  '2019-04-18',
  NULL,
  NULL,
  CAST('[{"type": "table", "param": "tablename", "value": ""}, {"type": "param", "param": "tablename2", "value": "", "datatype": "varchar"}, {"type": "column", "param": "joincoltbl1", "value": "productid", "datatype": "varchar"}, {"type": "param", "param": "joincoltbl2", "value": "productid", "datatype": "varchar"}, {"type": "schema", "param": "schema", "value": ""}, {"type": "param", "param": "schema2", "value": "", "datatype": "varchar"}, {"type": "param", "param": "operator", "value": ">|<|=|<=|>=", "datatype": "picklist"}, {"type": "param", "param": "thresholdval", "value": "0", "datatype": "numeric"}]' AS JSONB),
  'multitable',
  TRUE,
  'any',
  'Check if current file has all the values in the given {joincoltbl1} matched with {joincoltbl2} of schema - {schema2} and table - {tablename2}, operator is {operator}, threshold is {thresholdval}'
);


COMMIT;
