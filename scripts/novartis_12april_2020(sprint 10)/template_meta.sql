
/*select * from admn.event_meta;
select * from admn.template_meta
delete from admn.template_meta;commit;
select * from admn.template_meta*/
INSERT INTO admn.template_meta
(
  template_desc,
  event_id,
  has_attachment,
  attachment_path,
  template_title,
  template_body,
  is_active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  effctv_strt_dt,
  effctv_end_dt
)
VALUES
(
  'add user to usergroup template',
  (select event_id from admn.event_meta where lower(event_name) = lower('Add User To Usergroup') limit 1 ),
  FALSE,
  NULL,
  'Add user notificaiton',
  'Users: {users} has been added to Usergroup : {usergroups}',
  TRUE,
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  now(),
  now()
);


INSERT INTO admn.template_meta
(
  template_desc,
  event_id,
  has_attachment,
  attachment_path,
  template_title,
  template_body,
  is_active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  effctv_strt_dt,
  effctv_end_dt
)
VALUES
(
  'remove user from usergroup template',
  (select event_id from admn.event_meta where lower(event_name) = lower('Remove User From Usergroup') limit 1 ),
  FALSE,
  NULL,
  'Remove user notificaiton',
  'Users: {users} has been removed from Usergroup : {usergroups}',
  TRUE,
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  now(),
  now()
);
INSERT INTO admn.template_meta
(
  template_desc,
  event_id,
  has_attachment,
  attachment_path,
  template_title,
  template_body,
  is_active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  effctv_strt_dt,
  effctv_end_dt
)
VALUES
(
  'add layer to usergroup template',
  (select event_id from admn.event_meta where lower(event_name) = lower('Add Layer To Usergroup') limit 1 ),
  FALSE,
  NULL,
  'Add layer notificaiton',
  'Layers: {layers} has been added to Usergroup : {usergroups}',
  TRUE,
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  now(),
  now()
);


INSERT INTO admn.template_meta
(
  template_desc,
  event_id,
  has_attachment,
  attachment_path,
  template_title,
  template_body,
  is_active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  effctv_strt_dt,
  effctv_end_dt
)
VALUES
(
  'remove layer from usergroup template',
  (select event_id from admn.event_meta where lower(event_name) = lower('Remove Layer From Usergroup') limit 1 ),
  FALSE,
  NULL,
  'Remove layer notificaiton',
  'Layers: {layers} has been removed from Usergroup : {usergroups}',
  TRUE,
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  now(),
  now()
);
-- ---------


INSERT INTO admn.template_meta
(
  template_desc,
  event_id,
  has_attachment,
  attachment_path,
  template_title,
  template_body,
  is_active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  effctv_strt_dt,
  effctv_end_dt
)
VALUES
(
  'add functionality to usergroup template',
  (select event_id from admn.event_meta where lower(event_name) = lower('Add Functionality To Usergroup') limit 1 ),
  FALSE,
  NULL,
  'Add permission notification',
  'Permissions: {functionality} has been added to Usergroup : {usergroups}',
  TRUE,
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  now(),
  now()
);


INSERT INTO admn.template_meta
(
  template_desc,
  event_id,
  has_attachment,
  attachment_path,
  template_title,
  template_body,
  is_active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  effctv_strt_dt,
  effctv_end_dt
)
VALUES
(
  'remove functionality from usergroup template',
  (select event_id from admn.event_meta where lower(event_name) = lower('Remove Functionality From Usergroup') limit 1 ),
  FALSE,
  NULL,
  'Remove permission notification',
  'Permissions: {functionality} has been removed from Usergroup : {usergroups}',
  TRUE,
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  now(),
  now()
);

INSERT INTO admn.template_meta
(
  template_desc,
  event_id,
  has_attachment,
  attachment_path,
  template_title,
  template_body,
  is_active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  effctv_strt_dt,
  effctv_end_dt
)
VALUES
(
  'add usergroup template',
  (select event_id from admn.event_meta where lower(event_name) = lower('New Usergroup') limit 1 ),
  FALSE,
  NULL,
  'Add usergroup notification',
  'New Usergrup Added : {usergroups}. Users: {users}. Layers: {layers}. Permissions: {functionality}',
  TRUE,
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  now(),
  now()
);


INSERT INTO admn.template_meta
(
  template_desc,
  event_id,
  has_attachment,
  attachment_path,
  template_title,
  template_body,
  is_active,
  created_by,
  created_date,
  modified_by,
  modified_date,
  effctv_strt_dt,
  effctv_end_dt
)
VALUES
(
  'delete usergroup template',
  (select event_id from admn.event_meta where lower(event_name) = lower('Delete Usergroup') limit 1 ),
  FALSE,
  NULL,
  'Delete usergroup notification',
  'Usergrup Deleted : {usergroups}.',
  TRUE,
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  (select user_id from admn.user where lower(username) = lower('system') limit 1),
  now(),
  now(),
  now()
);




commit;


select * from admn.event_meta
select * from admn.template_meta
