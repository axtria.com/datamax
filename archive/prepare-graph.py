import uuid

import boto3
import pandas as pd
from itertools import permutations


def generate_id():
    return str(uuid.uuid4())


def lambda_handler(event, context):
    bucket = event['bucket']
    object_key = event['object_key']
    access_key = event['access_key']
    secret_key = event['secret_key']
    region_name = event['region_name']
    return prepare_graph(bucket, object_key, access_key, secret_key, region_name)


def create_vertex_and_edges(filename: str):
    data = [(filename, pd.read_csv(filename, delimiter='|'))]

    ##########

    table_nodes = pd.DataFrame()
    table_nodes['~label'] = filename
    table_nodes['type'] = 'table'
    table_nodes['_parent'] = 'none'

    ##########

    columns = []
    _table = []

    for table_name, df in data:
        columns += list(df.columns)
        _table += [table_name] * df.shape[1]

    column_nodes = pd.DataFrame()
    column_nodes['~label'] = columns
    column_nodes['type'] = 'column'
    column_nodes['_parent'] = _table

    #########

    values = []
    _column = []

    for table_name, df in data:
        for col in df.columns:
            if str(df[col].dtype) == 'object' or df[col].nunique() < len(df) / 10:
                iter_values = df[col].unique().tolist()
                values += iter_values
                _column += [table_name + '.' + col] * len(iter_values)

    value_nodes = pd.DataFrame()
    value_nodes['~label'] = values
    value_nodes['~label'] = value_nodes['~label'].astype(str)
    value_nodes['type'] = 'value'
    value_nodes['_parent'] = _column

    #########

    nodes = pd.concat([table_nodes, column_nodes, value_nodes]).reset_index(drop=True)

    nodes['~id'] = [generate_id() for _ in range(len(nodes))]
    nodes['alias'] = nodes['~label']

    # nodes[['~id', '~label', 'type', 'alias']].to_csv('nodes.csv', index=None)

    #########

    edges = nodes.loc[nodes['type'].isin(['column', 'value']), ['~id', '_parent','type']]
    edges.rename(columns={'~id': '~to'}, inplace=True)

    table_id_mapping = nodes.loc[nodes['type'] == 'table', ['~label', '~id']]

    column_id_mapping = nodes.loc[nodes['type'] == 'column', ['~label', '_parent', '~id']]
    column_id_mapping['~label'] = column_id_mapping['_parent'] + '.' + column_id_mapping['~label']
    del column_id_mapping['_parent']

    mapping = pd.concat([table_id_mapping, column_id_mapping])
    mapping.rename(columns={'~label': '_parent', '~id': '~from'}, inplace=True)

    edges = edges.merge(mapping, on='_parent')

    edges['~label'] = edges['type'].replace({'column':'TC','value':'CV'})

    reverse_edges = edges.copy()
    reverse_edges.rename(columns={'~to':'~from','~from':'~to'}, inplace=True)

    edges = pd.concat([edges,reverse_edges], sort=True)

    edges['~id'] = [generate_id() for _ in range(len(edges))]

    # edges[['~id', '~from', '~to']].to_csv('edges.csv', index=None)

    #######
    matched_alias = nodes.loc[nodes['alias'].duplicated(keep=False),['alias','~id']]
    matched_alias = matched_alias.groupby('alias').agg({'~id':lambda x: permutations(x,2)})
    weak_bonds = []
    for i in matched_alias['~id']: weak_bonds += i
    weak_bonds = pd.DataFrame(weak_bonds, columns=['~to','~from'])
    weak_bonds['~label'] = 'WB'
    weak_bonds['~id'] = [generate_id() for _ in range(len(weak_bonds))]


    return nodes[['~id', '~label', 'type', 'alias']], edges[['~id', '~from', '~to','~label']], weak_bonds[['~id', '~from', '~to','~label']]


def prepare_graph(bucket, object_key: str, access_key, secret_key, region_name):
    print()
    print('+++++ USER INPUT +++++')
    print('source = ' + bucket + '/' + object_key)
    print('access_key = ' + access_key)
    print('secret_key = ' + secret_key)
    print('region_name = ' + region_name)

    filename = object_key.split('/')[-2]

    # ************* TASK 1: DOWNLOAD THE FILE *************
    s3 = boto3.client('s3', region_name=region_name, aws_access_key_id=access_key, aws_secret_access_key=secret_key)
    s3.download_file(bucket, object_key, filename)

    # ************* TASK 2: CREATE EDGES & VERTEX *************
    vertex, edges, weak_bonds = create_vertex_and_edges(filename)

    # ************* TASK 3: UPLOAD THE FILE *************
    s3 = boto3.client('s3',
                      region_name='us-west-2',
                      aws_access_key_id='AKIAJGAJRQ7JBXND27UQ',
                      aws_secret_access_key='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')
    vertex.to_csv('vertex.csv', index=False)
    edges.to_csv('edges.csv', index=False)
    weak_bonds.to_csv('weak_bonds.csv', index=False)
    s3.upload_file('vertex.csv', 'salesiq-mapserver', 'DM/sample/demo/vertex.csv')
    s3.upload_file('edges.csv', 'salesiq-mapserver', 'DM/sample/demo/edges.csv')
    s3.upload_file('weak_bonds.csv', 'salesiq-mapserver', 'DM/sample/demo/weak_bonds.csv')


if __name__ == '__main__':
    lambda_handler({
        'bucket': 'salesiq-mapserver',
        'object_key': 'DM/Brandmax/dim_product/000',
        'access_key': 'AKIAJEIHDL2BZSDCACBQ',
        'secret_key': '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',
        'region_name': 'us-west-2'
    }, None)
