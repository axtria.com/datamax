import sys
import os
import datetime

from  psycopg2.extras import json
from psycopg2 import sql
from enum import Enum
sys.path.append(os.getcwd())

from api.vendor import Postgres
from src.gateway.database import Database
from src.gateway.validation import Validation
from api.utils import LOGGER, log_entry_exit
from src.services.data_validator.validationresponse import ValidationResponse
from src.services.data_validator.redshift.customvalidations import Validations


TEMP_SCHEMA = 'validation'
' source is where the SID file is - staging/CDM/DWH'
class Refshift_Validate ( Validation):
    
    def __init__(self, redshift: Database, postgresdb : Postgres):  
        super().__init__()
        self.postgresdb = postgresdb
        self.redshift = redshift
        self.queryLevel = Enum
        self.pyVal = Validations(redshift, postgresdb)
    
    @log_entry_exit
    def validate(self, source:str, so:str, tablename:str, schema:str):
        try:
            LOGGER.debug("Begin : {0}. Parameteres - source:  {1} , so: {2}, tablename: {3}".format("validate", source,str, tablename))
            
            df =  self.postgresdb.fetchDictionaryRecords(self.getSOValidationRulesQuery(so))
            LOGGER.debug("standard object validation rules are : {0}".format( json.dumps(df) ))

            df = self.populateRedShiftQueryParam(tablename, df,schema)
            LOGGER.debug("standard object validation rules with parameters populated : {0}".format(json.dumps(df)))

            insertiontime = datetime.datetime.now()
            mstrInsert = self.postgresdb.fetch("Insert into validationprocessmstr(source, so, description,status,startdate) values('{0}','{1}','{2}','{3}','{4}'); SELECT currval(pg_get_serial_sequence('validationprocessmstr', 'id'));".format(source, so, "so validation",
            "InProgress",insertiontime))
            processid = mstrInsert[0][0]
            LOGGER.debug("validationprocessmstr process id : {0}".format(processid)) 
            # below code will move into util.py file
            

            queryCreateValDetail = self.getValidationQuery_detail_create(tablename,TEMP_SCHEMA)
            LOGGER.debug("detail table query : {0}".format(processid))

            self.redshift.executeddl(queryCreateValDetail)
            LOGGER.debug("detail table created") 

            
            queryCreateValSummary = self.getValidationQuery_summary_create(tablename,TEMP_SCHEMA)
            LOGGER.debug("summary table query : {0}".format(processid))

            self.redshift.executeddl(queryCreateValSummary)
            LOGGER.debug("summary table created") 

            totalRows = (self.redshift.execute("select count(1) from {1}.{0}".format(tablename, schema))).iloc[0,0]

            valresp = ValidationResponse(processid, totalRows)
            LOGGER.debug("Total rows are {0}".format(totalRows))
            if len(df) > 0 :
                for i in range(0,len(df)): 
                    seqid = i + 1
                    errormsg = ''
                    self.postgresdb.execute(self.getInsertProcessQuery(processid, seqid, 'InProgress',df[i]))
                    query = self.getValidationQuery_detail(df[i]['redshiftquery'], df[i]['level'],tablename, df[i]['ruleid'],processid, seqid,schema)
                    failCount = 0  
                    successCount = 0
                    try:
                        if((df[i]['level']).lower() == QueryLevel.Row.name.lower()):
                            if(df[i]['pyfunctionname'] == None) :      
                                retdf = self.redshift.execute(query)    
                                failCount = retdf.iloc[0,0]
                                errormsg = 'success' if failCount == 0 else 'fail' 
                            else:
                                retdf, errormsg = self.pyVal.__getattribute__(df[i]['pyfunctionname'])(df[i])
                                failCount = retdf.iloc[0,0]
                        else:
                            if(df[i]['pyfunctionname'] == None) :      
                                retdf = self.redshift.execute(query)
                                # result dataset is status, message for column level query 
                                failCount = 0 if retdf.iloc[0,0] == True else 1
                                errormsg = retdf.iloc[0,1] 
                            else:
                                retdf, errormsg = self.pyVal.__getattribute__(df[i]['pyfunctionname'])(df[i])
                                failCount = retdf.iloc[0,0]
                             
                    except Exception as e:
                        # print( e.message, e.args)
                        # errormsg = e.message
                        pass
                    self.postgresdb.execute("update validationprocesses set status = 'Completed' , enddate = now(), failurecount = {0} , successcount = {3} where processid={1} and seqid = {2}".format( failCount, processid, seqid, totalRows - failCount))
                    valresp.addRule(seqid, df[i]['ruleid'] , df[i]['level'] , df[i]['description'] , df[i]['parameters'],failCount, True if failCount ==0 else False, errormsg  )
                    try:
                        self.postgresdb.execute( self.getValidationQuery_summary( valresp.rules[len(valresp.rules) -1] , processid,TEMP_SCHEMA,tablename))
                    except Exception as e:
                        LOGGER.error(e)
                failCount = 0  
                successCount = 0
                successCount = self.redshift.execute(self.getValidationQuery_success(tablename,schema)).iloc[0,0]
                failCount = self.redshift.execute(self.getValidationQuery_failure(tablename,schema)).iloc[0,0]

                valresp.erroneousrecords = failCount
                valresp.successrecords = successCount
                valresp.successpath = "{0}.{1}_success".format(TEMP_SCHEMA, tablename)
                valresp.failurepath = "{0}.{1}_failure".format(TEMP_SCHEMA, tablename)
                valresp.detailpath = "{0}.{1}_success".format(TEMP_SCHEMA, tablename)
                valresp.summarypath = "{0}.{1}_success".format(TEMP_SCHEMA, tablename)
                valresp.destination = "redshift"

                # self.postgresdb.execute(self.getValidationQuery_summary_update(valresp, TEMP_SCHEMA,tablename))

                self.postgresdb.execute("update validationprocessmstr set status = 'Completed' , enddate = now(), failurecount = {1} , successcount = {2} where id={0};commit; ".format( processid, failCount,successCount))
                LOGGER.debug(valresp.__dict__)
                self.printresult(valresp.__dict__)
        except Exception as ex:            
            self.add_response("validationerror","validation failed")
        self.add_response("success", valresp)
        LOGGER.info("validation response is : {0}".format(json.dumps(valresp.__dict__)))





    # ############ BEGIN :: UTILITY METHODS ###################################
    @log_entry_exit
    def populateRedShiftQueryParam(self, tablename:str, config,schema:str):
        for rec in config:
            redshiftquery = rec['redshiftquery']
            for val in rec['parameters']:
                if(val['param'] == 'tablename'):
                    val['value'] = schema +  "." + tablename    
                redshiftquery = redshiftquery.replace('{' + val['param'] + '}', 
                val['value'])
            rec['redshiftquery'] = redshiftquery
            redshiftquery = ''
        return config
    
        
    def enqueRules(self):
        '''enque the validation rules'''
        return 0
        # for rule in json_normalize(config)

    @log_entry_exit
    def printresult(self,summary):
        print(summary)

    @log_entry_exit
    def getValidationQuery_detail(self, query :str, level : str, tablename:str, ruleid: int, processid:int, seqid:int,schema:str):
        LOGGER.debug("parameteres are : query : {0}, level : {1} , tablename : {2},  ruleid : {3}, processid : {4}, seqid : {5}".format(
            query, level, tablename, ruleid, processid, seqid))
        ret = query
        if( level.lower() == QueryLevel.Row.name.lower()):
            ret = '''insert into {5}.{0}_detail( DMX_ID, status, ruleid, processid, seqid) select t.DMX_ID, 0 status, {2} ruleid, {3} processid,{4} seqid  from {6}.{0} t inner join ({1}) s on t.DMX_ID = s.DMX_ID;  select count(1) from {5}.{0}_detail where  ruleid = {2} and processid = {3} and seqid = {4} ;'''.format(
                tablename, query,ruleid,processid, seqid,TEMP_SCHEMA,schema)
        else :
            ret = query
        LOGGER.debug("query formed :{0}".format(ret))
        return ret

    @log_entry_exit
    def getValidationQuery_failure(self,  tablename:str,schema:str):
        LOGGER.debug("parameteres are : tablename : {0}".format(tablename))
        ret = ""
        ret = "drop table if exists {1}.{0}_failure ;select * into {1}.{0}_failure from (select t.* from {2}.{0} t inner join ( select distinct DMX_ID from {1}.{0}_detail) s on s.DMX_ID = t.DMX_ID) final;commit;select count(1) from {1}.{0}_failure;".format(tablename, TEMP_SCHEMA,schema)
        LOGGER.debug("query formed :{0}".format(ret))
        return ret

    @log_entry_exit
    def getValidationQuery_success(self,  tablename:str,schema :str):
        LOGGER.debug("parameteres are : tablename : {0}".format(tablename))
        ret = ""
        ret = "drop table if exists {1}.{0}_success ;select * into {1}.{0}_success from (select t.* from {2}.{0} t left join ( select distinct DMX_ID from {1}.{0}_detail) s on s.DMX_ID = t.DMX_ID where s.DMX_ID is null ) final;commit;select count(1) from {1}.{0}_success;".format(tablename, TEMP_SCHEMA,schema)
        LOGGER.debug("query formed :{0}".format(ret))
        return ret

    @log_entry_exit
    def getSOValidationRulesQuery(self, name:str):
        LOGGER.debug("parameteres are : name : {0}".format(name))
        ret = "select case when c.parameters is null then rules.defaultparameters else c.parameters end as parameters, rules.redshiftquery, c.id, c.ruleid, c.source, c.so, c.isexcluded, c.prerequisites , rules.level, rules.description, rules.pyfunctionname  from suggestioncriteria c inner join rules on rules.id = c.ruleid  where so = '{0}'".format(name)
        LOGGER.debug("query formed :{0}".format(ret))
        return ret
        
    @log_entry_exit
    def getInsertProcessQuery(self,  processid:int, seqid:int, status:str,config ):
        LOGGER.debug("parameteres are : processid : {0}, seqid : {1}, status : {2}, config : {3}".format(processid, seqid, status, config))
        insertiontime = datetime.datetime.now()
        ret = sql.SQL("""insert into validationprocesses(  seqid,processid, redshiftquery,parameters, status, startdate) values({0},{1},{2},{3},{4},{5});SELECT currval(pg_get_serial_sequence('validationprocesses', 'id'));""").format( sql.Literal(seqid),sql.Literal(processid), sql.Literal(config['redshiftquery']), sql.Literal(json.dumps(config['parameters'])),sql.Literal(status), sql.Literal(insertiontime ))
        LOGGER.debug("query formed :{0}".format(ret))
        return ret
    
    @log_entry_exit
    def getValidationQuery_detail_create(self, tablename : str, TEMP_SCHEMA:str):
        LOGGER.debug("parameteres are : tablename : {0}, TEMP_SCHEMA : {1}".format(tablename, TEMP_SCHEMA))
        ret = "drop table if exists {1}.{0}_detail;" \
              "create table {1}.{0}_detail(DMX_id int, status bool , ruleid int, processid int, seqid int)".format(tablename, TEMP_SCHEMA)
        LOGGER.debug("query formed :{0}".format(ret))
        return ret

    @log_entry_exit
    def getValidationQuery_summary_create(self, tablename : str, TEMP_SCHEMA:str):
        ret = "drop table if exists {0}.{1}_summary;create table {0}.{1}_summary(processid int, seqid int, ruleid int, description varchar(200),parameteres varchar(2000),erroneousrecords int, message varchar(1000), successpath varchar(1000), failurepath varchar(1000), detailpath varchar(1000) ,summarypath varchar(1000), destination varchar(10) );".format(TEMP_SCHEMA,tablename) 
        return ret
    
    @log_entry_exit
    def getValidationQuery_summary(self, detail, processid,TEMP_SCHEMA,tablename):
        description = detail["description"]
        ret = sql.SQL("""insert into {0}.{1}( processid, seqid, ruleid, description,  erroneousrecords,message ) values({2},{3},{4},{5},{6},{7})""").format(sql.Identifier(TEMP_SCHEMA), sql.Identifier(tablename + '_summary'), 
            sql.Literal(processid), sql.Literal(detail["seqid"]), sql.Literal(detail["id"]), 
            sql.Literal(description), sql.Literal(detail["erroneousrecords"]), 
            sql.Literal(detail["message"]))
        return ret

    def getValidationQuery_summary_update(self, detail, TEMP_SCHEMA,tablename):
        ret = sql.SQL("""update  %s.%s_summary set successpath = %s,failurepath = %s, detailpath = %s, summarypath = %s,  destination = %s """).format(
            sql.Literal(TEMP_SCHEMA), sql.Literal(tablename),
            sql.Literal("successpath"), sql.Literal(detail["failurepath"]), sql.Literal(detail["detailpath"]), 
            sql.Literal(detail["summarypath"]), sql.Literal(detail["destination"])
        ) 
        return ret

    # ############ END :: UTILITY METHODS ###################################


class QueryLevel(Enum):
        Row = 1
        Column = 2
        File = 3

 
