import pandas as pd
import numpy as np
from timeit import default_timer as dt
from datetime import datetime
from faker import Faker; f = Faker()

df = pd.read_csv('dd_zip2terr_1000.csv',header=None, low_memory=False)

#1 unique test
time = []
for c in df.columns:
    s = dt()
    _ = df[c].duplicated()
    e = dt()
    time.append(e-s)
    
print('1. Unique test on',len(df),'records')
print(pd.Series(time).describe())


#2 null test
time = []
for c in df.columns:
    s = dt()
    _ = df[c].isnull()
    e = dt()
    time.append(e-s)
    
print('\n\n2. Null test on',len(df),'records')
print(pd.Series(time).describe())


#3 values in a set test
time = []
for c in df.columns:
    vals = df[c].unique()[:df[c].nunique()//5]
    s = dt()
    _ = df[c].isin(vals)
    e = dt()
    time.append(e-s)
    
print('\n\n3. Values in a set test on',len(df),'records')
print(pd.Series(time).describe())


#4 datatype test
time = []
dtypes = df.dtypes.copy()
np.random.shuffle(dtypes)

for i,c in enumerate(df.columns):
    s = dt()
    _ = df[c].dtype == dtypes[i]
    e = dt()
    time.append(e-s)
    
print('\n\n4. datatype test on',len(df),'records')
print(pd.Series(time).describe())


#5 valid email id test
df['fake_email_id'] = [f.email() for _ in range(len(df))]

s = dt()
_ = df['fake_email_id'].str.match('[^@]+@[^@]+\.[^@]+')
e = dt()

    
print('\n\n5. valid email test on',len(df),'records')
print('time taken',e-s)


#6 valid date format test
date_col = 9
date_format = '%Y-%m-%d %H:%M:%S.%f'

s = dt()
_= pd.to_datetime(df[date_col], format=date_format, errors='coerce').notnull()
e = dt()

print('\n\n6. Date format test on',len(df),'records')
print('time taken',e-s)


#7 Date Range Check
date_col = 9
date_format = '%Y-%m-%d %H:%M:%S.%f'

s = dt()
_ = pd.to_datetime(df[date_col], format=date_format, errors='coerce') < datetime.now()
e = dt()

print('\n\n7 and 8 Date Range & Date constraint test on',len(df),'records')
print('time taken',e-s)


#9 Date Order Check
date_col = 9
date_col2 = 10
date_format = '%Y-%m-%d %H:%M:%S.%f'

s = dt()
_ = pd.to_datetime(df[date_col], format=date_format) < pd.to_datetime(df[date_col2], format=date_format)
e = dt()

print('\n\n8 Date Order test on',len(df),'records')
print('time taken',e-s)


#10 Field Length check
s = dt()
_ = df['fake_email_id'].str.len() <= 20
e = dt()
print('\n\n10 Field length check on',len(df),'records')
print('time taken',e-s)


#11 Column value comparision
s = dt()
_ = df[0] > 5000
_ = df[0] == df[1]
e = dt()
print('\n\n11 Column value comparision on',len(df),'records')
print('time taken',(e-s)/2)


#12 Upper case check
s = dt()
_ = df['fake_email_id'].str.isupper()
e = dt()
print('\n\n12 Upper case check on',len(df),'records')
print('time taken',e-s)


# 13 Reference check
s = dt()
_ = df[0].isin(df[1])
e = dt()
print('\n\n13 Reference check on',len(df),'records')
print('time taken',e-s)



