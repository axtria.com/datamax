import boto3
import pandas as pd


def validate_file(nodes_filename, edges_filename):
    nodes = pd.read_csv(nodes_filename)
    edges = pd.read_csv(edges_filename)
    ne = pd.merge(nodes, edges, how='left', left_on='~id', right_on='~from')
    orphan_nodes = ne[ne['~from'].isnull()]
    en = pd.merge(edges,nodes, how='left', left_on='~from', right_on='~id')
    orphan_edges = en[en['~id_x'].isnull()]

    wb_ = pd.merge(edges, nodes, how='left', left_on='~from', right_on='~id')
    wb = pd.merge(wb_, nodes, how='left', left_on='~to', right_on='~id')

    invalid_weak_bonds_1 = wb[(wb['alias_x'] != wb['alias_y']) & (wb['~label'] == 'WB')]
    invalid_weak_bonds_2 = wb[(wb['alias_x'] == wb['alias_y']) & (wb['~label'] != 'WB')]


    print("+++++ ORPHAN Nodes +++++")
    print(orphan_nodes)

    print("+++++ ORPHAN edges +++++")
    print(orphan_edges)

    print("+++++ Weak bonds invalid +++++")
    print(invalid_weak_bonds_1)
    print(invalid_weak_bonds_2)



def lambda_handler(event, context):
    bucket = event['bucket']
    object_key = event['object_key']
    access_key = event['access_key']
    secret_key = event['secret_key']
    region_name = event['region_name']
    return prepare_graph(bucket, object_key, access_key, secret_key, region_name)


def prepare_graph(bucket, object_key: str, access_key, secret_key, region_name):
    print()
    print('+++++ USER INPUT +++++')
    print('source = ' + bucket + '/' + object_key)
    print('access_key = ' + access_key)
    print('secret_key = ' + secret_key)
    print('region_name = ' + region_name)

    n_filename = 'vertex.csv'
    e_filename = 'edges.csv'

    # ************* TASK 1: DOWNLOAD THE FILE *************
    s3 = boto3.client('s3', region_name=region_name, aws_access_key_id=access_key, aws_secret_access_key=secret_key)
    s3.download_file(bucket, object_key + n_filename, n_filename)
    s3.download_file(bucket, object_key + e_filename, e_filename)

    # ************* TASK 2: VALIDATE THE FILE *************
    validate_file(n_filename, e_filename)


if __name__ == '__main__':
    lambda_handler({
        'bucket': 'salesiq-mapserver',
        'object_key': 'DM/sample/demo/',
        'access_key': 'AKIAJEIHDL2BZSDCACBQ',
        'secret_key': 'wlSwlm4Po4xpvE9GW2dg2QbVY7xoGGpxUkxc/BrU',
        'region_name': 'us-west-2'
    }, None)
