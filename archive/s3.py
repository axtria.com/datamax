import pandas as pd
import s3fs 
from io import StringIO
        
def getDelimiter(header_str):
    delimiters = [',','|',';',':','\t']
    detected_delimiter = None
    max_count = 0
    for delimiter in delimiters:
        delimiter_count = header_str.count(delimiter)
        if 0 <= max_count < delimiter_count:
            detected_delimiter = delimiter
            max_count = delimiter_count
    return detected_delimiter

def getHeader(s3_conn, s3_path, delimiter='infer'):
    header = None
    try:
        with s3_conn.open(s3_path,'rb') as f:
            header = f.readline().decode()
        if delimiter == 'infer':
            delimiter = getDelimiter(header)
        header = header.strip().split(delimiter)
        return header
    except:
        return 'Some error occured'

def getDtypes(s3_conn, s3_path, header=True, delimiter='infer', n_records_for_dtype_detection=10):
    try:
        with s3_conn.open(s3_path,'rb') as f:
            if n_records_for_dtype_detection == -1:
                data = f.read().decode()
            else:
                data = ''
                for _ in range(n_records_for_dtype_detection):
                    data += f.readline().decode()
        if delimiter == 'infer':
            delimiter = getDelimiter(data)
        header = None if header == False else 0    
        df = pd.read_csv(StringIO(data), sep=delimiter, header=header)
    except:
        return 'Some error occured'
    dt = df.dtypes   
    dt = dt.replace('int32','BIGINT')
    dt = dt.replace('int64','BIGINT')
    dt = dt.replace('float32','DOUBLE PRECISION')
    dt = dt.replace('float64','DOUBLE PRECISION')
    dt = dt.replace('object','VARCHAR(255)')   
    return dt.tolist()

def getRedshiftCreateQury(tablename,
                          s3_conn=None, s3_path=None, delimiter='infer', header=True,
                          columns=None, dtypes=None):
    if not columns:
        columns = getHeader(s3_conn, s3_path, delimiter)
        
    if not dtypes:
        dtypes = getDtypes(s3_conn, s3_path, header, delimiter, n_records_for_dtype_detection=10)
    
    query = 'CREATE TABLE IF NOT EXISTS ' + tablename + '(' +','.join([col + ' ' + dt for col, dt in zip(columns, dtypes)]) + ')'
    return query
######################################
    

key    = 'AKIAIFUJCZNX2KD7HUEA'
secret = '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'

s3_conn = s3fs.S3FileSystem(key=key,secret=secret)
s3_path = 'datamax1/Sales_1960_to_2018_with_header.csv'

h = getHeader(s3_conn, s3_path)
print(h)

dt = getDtypes(s3_conn, s3_path, n_records_for_dtype_detection=5)
print(dt)

q = getRedshiftCreateQury('sales', columns=h, dtypes=dt)
print(q)


















