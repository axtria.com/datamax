from datetime import datetime

def uid():
    return

def safe_html(s):
    return s.replace('<','__LeftAngleBracket__').replace('>','__RightAngleBracket__')

def valid_html(s):
    return s.replace('__LeftAngleBracket__','<').replace('__RightAngleBracket__','>').replace('nan','').replace('NaN','')