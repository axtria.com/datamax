from src.entity.node import Column, Value
from src.services import Service
from api.utils import generate_histogram
from api.utils.knowledgebase import get_confidence_level
from api.utils import column_metadata_query, statistics_numeric_query, statistics_varchar_query, \
    record_count_query, null_count_query, unique_value_query
from api.vendor import Catalog, Database, Storage


class Source(Service):
    def __init__(self, catalog: Catalog, redshift: Database, s3: Storage, source_id: str):
        super().__init__()
        self.catalog = catalog
        self.redshift = redshift
        self.s3 = s3
        self.source_id = source_id

    def execute(self):
        # request catalog to fetch node properties
        table = self.catalog.get_table(self.source_id)
        self.log.debug("++++ source table ++++")
        self.log.debug(table)

        # table_metadata = self.redshift.execute(table_metadata_query(table.schema, table.name))
        column_metadata = self.redshift.execute(column_metadata_query(table.schema, table.name))

        std_objs = get_confidence_level(list(column_metadata['name']))
        self.log.debug("++++++++ std objs +++++++++++")
        self.log.debug(std_objs)
        # update the catalog node of table using separate service

        # table profile
        # table.duplicate_count = self.redshift.execute(duplicate_query(table.schema, table.name,
        #                                                             column_metadata['name']))
        table.duplicate_count = 0
        table.null_count = self.redshift.execute(null_count_query(table.schema, table.name,
                                                                  column_metadata['name'])).iat[0, 0]
        table.record_count = self.redshift.execute(record_count_query(table.schema, table.name)).iat[0, 0]
        table.std_obj_profile = std_objs
        table.std_obj = std_objs[0]['src_name'] + '.' + std_objs[0]['object_name']  # Too much detailed
        self.catalog.update_source(table.id, table)

        # register columns
        for index, row in column_metadata.iterrows():
            try:
                stats = self.redshift.execute(statistics_numeric_query(table.schema, table.name, row['name']))
            except Exception:
                stats = self.redshift.execute(statistics_varchar_query(table.schema, table.name, row['name']))
            column = Column(**row.to_dict(), **stats.to_dict(orient='records')[0])
            self.catalog.register_source(column)

            # add relationship
            self.catalog.add_relation(table, column, "TabCol")

            # find unique values - if categorical and then create nodes and build it's relationship
            if column.unique_count / table.record_count < 0.4:
                items = self.redshift.execute(unique_value_query(table.schema, table.name, column.name))
                for _, item in items.iterrows():
                    value = Value(name=item['name'])
                    self.catalog.register_source(value)
                    self.catalog.add_relation(column, value, 'ColVal')

        # optimise using the S3 files to upload to neptune
        # generate HTML with histograms

        df = generate_histogram(self.redshift, table.schema, table.name, column_metadata['name'])
