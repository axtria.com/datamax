from api.vendor import Redshift
import pandas as pd
from io import StringIO
from os import path
import seaborn
import matplotlib.pyplot as plt


from src.services.data_profiler.util import uid, safe_html, valid_html

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.float_format', '{:.2f}'.format)

class RedshiftProfiler:
    """
    Profiles/calculates basic statistics of a table present in redshift.
    """
    def __init__(self, table_schema:str, table_name: str, standard_obj: str = None):
        """
        Parameters
        ----------
        table_name : string
            name of redshift table
        """
        self.table_schema = table_schema
        self.table_name = table_name
        self.standard_obj = standard_obj

        self.redshift = Redshift()
        self.max_rows_for_percentiles = 10**7 # use None for no limit
        self.histogram_bin_count = 20

        self.all_columns = None
        self.numeric_columns = None
        self.categorical_columns = None
        self.profiling_dfs = None
        self.messages = []


    def execute(self):
        """
        Assumptions
        -----------
        If `standard_obj` is null no KPI's will be calcuted
        """
        table_metadata, column_metadata = self.redshift.get_metadata(self.table_name, schema=self.table_schema)

        self.all_columns = column_metadata['column_name']
        self.numeric_columns = column_metadata.loc[column_metadata['data_type'].str.lower().isin(Redshift.numeric_dtypes),'column_name']

        row_count, statistics = self.basic_statistics()

        table_metadata['row_count'] = row_count

        column_metadata = column_metadata.dropna(axis=1, how='all')
        column_metadata = column_metadata.loc[:,~column_metadata.columns.isin(table_metadata.columns)]
        column_metadata.set_index('column_name', inplace=True)
        del column_metadata.index.name

        self.profiling_dfs = [

        ('Table Metadata',table_metadata),
        ('Column Metadata', column_metadata),
        ('Statistics',statistics)
        ]

        #histograms = self.get_histograms()
        #self.profiling_dfs.append(('histograms',histograms))


        html = self.get_html()

        if return_html_path:
            dirname = 'tmp/html/'
            fname = self.table_schema + '_' + self.table_name + '__' + uid() + '.html'
            fpath = dirname + fname
            with open(fpath,'w') as f:
                f.write(html)
            return path.abspath(fpath)
        else: 
            return html


    def basic_statistics(self):
        """
        Calculates basic statistics of `table_name` like row_count, sum, min, mean, median etc.
        """

        query_parts = []

        query_row_count = 'count(*) as row_count'
        query_parts.append(query_row_count)

        query_sum = ('sum(' + self.numeric_columns +') as '+ self.numeric_columns + '__sum').tolist()
        query_parts += query_sum

        query_min = ('min(' + self.numeric_columns +') as '+ self.numeric_columns + '__min').tolist()
        query_parts += query_min

        query_max = ('max(' + self.numeric_columns +') as '+ self.numeric_columns + '__max').tolist()
        query_parts += query_max

        query_avg = ('avg(' + self.numeric_columns +') as '+ self.numeric_columns + '__mean').tolist()
        query_parts += query_avg

        query_std = ('stddev(' + self.numeric_columns +') as '+ self.numeric_columns + '__standard_dev').tolist()
        query_parts += query_std

        query_null_count = ('sum(case when '+ self.all_columns +' is null then 1 else 0 end) as ' + self.all_columns +'__null_count').tolist()
        query_parts += query_null_count

        query_unique_count = ('count(distinct '+ self.all_columns +') as ' + self.all_columns +'__unique_values').tolist()
        query_parts += query_unique_count

        query = """select {parts} from {schema}.{table}""".format(
            parts = ', '.join(query_parts),
            schema = self.table_schema,
            table = self.table_name)

        df = self.redshift.execute(query)

        row_count = df.at[0,'row_count']

        if not self.max_rows_for_percentiles or row_count <= self.max_rows_for_percentiles:
            percentiles = self.get_percentiles()
            df = pd.concat([df,percentiles], axis=1)
        else:
            if row_count > self.max_rows_for_percentiles:
                self.messages.append('Skipping percentile calculations because row count exceeded the threshold of '+str(self.max_rows_for_percentiles))

        df = pd.wide_to_long(df,
            stubnames = self.all_columns,
            i = 'row_count',
            j = 'profile',
            sep = '__',
            suffix = '\\w+').reset_index()

        del df['row_count']

        df = df.set_index('profile').transpose()
        del df.columns.name

        histograms = self.get_histograms(df)

        df = pd.concat([df,histograms], axis=1, sort=False)

        return row_count, df

    def get_percentiles(self, percentiles=[25,50,75]):
        """
        Calculates 25 percentiles, 50 percentile (median), 75 percentile
        """
        query_parts = []
        for percentile in percentiles:
            parts = ('percentile_disc('+str(percentile/100)+') within group (order by '+self.numeric_columns+') over() as '+self.numeric_columns+'__'+str(percentile)+'_percentile').tolist()
            query_parts += parts

        query = """select distinct {parts} from {schema}.{table}""".format(
            parts = ', '.join(query_parts),
            schema = self.table_schema,
            table = self.table_name)

        df = self.redshift.execute(query)
        return df

    def get_histograms(self, statistics):

        self.categorical_columns = statistics[statistics['unique_values']<=self.histogram_bin_count].index

        histograms = []

        for col in self.categorical_columns:
            query = """select {col}, count({col}) as frequency from {schema}.{table} group by {col} order by {col}""".format(
                col = col,
                schema = self.table_schema,
                table = self.table_name)

            df = self.redshift.execute(query)
            

            dirname = 'tmp/plot/'
            fname = self.table_schema + '_' + self.table_name + '_' + col + '_histogram__' + uid() + '.png'
            fpath = path.abspath(dirname + fname)

            plt.figure()
            plot = seaborn.barplot(x=df['frequency'], y=df[col], orient='h', color='blue').get_figure()
            plot.savefig(fpath, dpi=600 ,bbox_inches='tight')

            histograms.append((col,
            safe_html('<a href="' + fpath + '" target="_blank"><img src="' + fpath + '" height=60 width=150 /></a>')
            ))

        df = pd.DataFrame(histograms, columns=['column','histogram']).set_index('column')
        del df.index.name

        return df


    def get_html(self):
        """
        Returns html of `profiling_dfs`
        """
        html_str = '<h1 align="center">Redshift Profiler</h1>'

        for title, df in self.profiling_dfs:
            html_str += '<h2>'+title+'</h2>'
            buf = StringIO()
            df.to_html(buf, justify='center', index=True)
            html_str += buf.getvalue() + '<br>'

        return valid_html(html_str)





