from src.services import Service
from api.utils import inspect_delimiter
from api.vendor import S3


class ReadSample(Service):
    def __init__(self, project, source_bucket, source_object, header_object, data_header, delimiter='', count_rows=5,
                 **kwargs):
        super().__init__()
        self.s3 = S3(project)
        self.source_bucket = source_bucket
        self.source_object = source_object
        self.header_object = header_object
        self.data_header = data_header
        self.delimiter = delimiter
        self.count_rows = count_rows

    def execute(self):
        header_row = self.s3.read(self.source_bucket + '/' + self.header_object, 1)[0]
        if self.delimiter == '':
            self.delimiter = inspect_delimiter(header_row)

        file = self.s3.explorer(self.source_bucket, self.source_object, 1)[0]
        sample_records = self.s3.read(self.source_bucket + '/' + file, self.count_rows)
        if self.data_header:
            sample_records = sample_records[1:]

        self.add_response('header', header_row.split(self.delimiter))
        self.add_response('sample_records', list(map(lambda x: x.split(self.delimiter), sample_records)))
        self.add_response('delimiter', self.delimiter)