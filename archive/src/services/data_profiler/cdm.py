from src.services import Service
from api.vendor import Catalog, Database, Storage


class CDM(Service):
    def __init__(self, catalog: Catalog, redshift: Database, s3: Storage, cdm_id: str):
        super().__init__()
        self.catalog = catalog
        self.redshift = redshift
        self.s3 = s3
        self.cdm_id = cdm_id

    def execute(self):
        pass
