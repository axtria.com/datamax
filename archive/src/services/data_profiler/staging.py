from src.services import Service
from api.vendor import Catalog, Database, Storage


class Staging(Service):
    def __init__(self, catalog: Catalog, redshift: Database, s3: Storage, staging_id: str):
        super().__init__()
        self.catalog = catalog
        self.redshift = redshift
        self.s3 = s3
        self.staging_id = staging_id

    def execute(self):
        # generate pivots which make sense for the given standard object
        # using the knowledge base.
        pass
