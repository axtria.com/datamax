# datamax/src/services/data_ingestion/staging.py

from src.services import Service
from api.vendor import Catalog, Storage, Database


class Staging(Service):
    def __init__(self, catalog: Catalog, s3: Storage, redshift: Database, source_id: str):
        super().__init__()
        self.catalog = catalog
        self.s3 = s3
        self.redshift = redshift
        self.source_id = source_id

    def execute(self):
        pass
