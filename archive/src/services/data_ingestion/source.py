# datamax/src/services/data_ingestion/source.py

from src.entity.node import Table
from src.services import Service
from api.services.data_movement import S3ToRedshift
from api.vendor import Catalog
from api.vendor import Database
from api.vendor import Storage


class Source(Service):
    """Data ingested from the S3 layer to the Redshift layer called `source`.

    Data ingestion involves the movement of the file after reading the first line of the file with the help of
    library `s3fs` which enables the read operation without downloading the complete file at application server.


    Assumptions
    -----------
    S3 credentials are already present in the INI file which can be controlled with the `profile` argument of the
    respective vendor.

    Request syntax
    --------------
    service = Source(
        catalog=Neptune(),
        redshift=Redshift(),
        s3=S3(),
        project='string',
        source_bucket='string',
        source_object='string',
        header_object='string',
        delimiter='string',
        file_format='PARQUET'|'RCFILE'|'SEQUENCEFILE'|'TEXTFILE'|'ORC'|'AVRO'
    )

    Parameters
    ----------
    * catalog (Catalog) -- [REQUIRED]
    * redshift (Database) -- [REQUIRED]
    * s3 (Storage) -- [REQUIRED]
    * project (string) -- [REQUIRED]
    * source_bucket (string) -- [REQUIRED]
    * source_object (string) -- [REQUIRED]
    * header_object (string) -- [REQUIRED]
    * delimiter (string) -- [Optional]
    * file_format (string) -- [Optional]

    Return type
    -----------
    dict

    Response syntax
    ---------------
    {
        "source_id" : "string",
        "child": {
            // S3ToRedshift response
        }
    }

    Response structure
    ------------------
    * dict
        * source_id (string) -- unique node id generated in the catalog which is used to reference for future
        processing.
        * child (dict) --
            response of an alternative service called within the current service

    Notes
    -----
    * add a catalog call to register the source table
    * use the node object to add a vertex in catalog
    * add the response to facilitate profiler to work on the given node id.
    * addition of links will be taken care by the wrapper functions defined in catalog class.

    See Also
    --------
    S3ToRedshift

    """

    def __init__(self, catalog: Catalog, redshift: Database, s3: Storage, project: str, source_bucket: str,
                 source_object: str, header_object, delimiter=None, file_format='TEXTFILE', data_header=False):
        super().__init__()
        self.redshift = redshift
        self.catalog = catalog
        self.s3 = s3
        self.project = project
        self.source_bucket = source_bucket
        self.source_object = source_object
        self.header_object = header_object
        self.delimiter = delimiter
        self.file_format = file_format
        self.data_header = data_header
        self.schema = 'source'

    def execute(self):
        s3_to_redshift = S3ToRedshift(self.s3, self.redshift, self.source_bucket, self.source_object, self.schema,
                                      self.header_object, None, self.delimiter, self.file_format, self.data_header)
        s3_to_redshift.invoke()

        source_table = s3_to_redshift.destination_table
        self.delimiter = s3_to_redshift.delimiter

        # replace by logbook to store the queries being fired at a given request.
        self.add_response('child', s3_to_redshift.response)

        # add a catalog call to register the source table
        # use the node object to add a vertex in catalog
        # add the response to facilitate profiler to work on the given node id.
        # addition of links will be taken care by the wrapper functions defined in catalog class.
        table = Table(name=source_table, alias='', schema=self.schema, project=self.project,
                      s3_bucket=self.source_bucket, s3_path=self.source_object, delimiter=self.delimiter,
                      header_object=self.header_object, file_format=self.file_format, data_has_header=self.data_header)
        self.catalog.register_source(table)
        self.add_response('source_id', table.id)
