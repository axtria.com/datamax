import logging

logging.basicConfig(level=logging.DEBUG, handlers=[logging.FileHandler("system.log"), logging.StreamHandler()],
                    format='%(asctime)s  - %(levelname)s  - %(name)s - %(message)s')

logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('s3transfer').setLevel(logging.CRITICAL)
logging.getLogger('urllib3').setLevel(logging.CRITICAL)
logging.getLogger('s3fs').setLevel(logging.CRITICAL)
logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
logging.getLogger('sqlalchemy.engine.base.Engine').setLevel(logging.CRITICAL)