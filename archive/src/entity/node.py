import uuid


class Node:
    def __init__(self, name, alias):
        self.id = str(uuid.uuid4())
        self.name = name
        self.alias = alias

    def __repr__(self):
        return str(self.__dict__)


class Table(Node):
    def __init__(self, *, name=None, alias=None, schema=None, project=None, s3_bucket=None, s3_path=None,
                 header_object=None, delimiter=None, file_format=None, data_has_header=None, null_count=None,
                 duplicate_count=None, record_count=None, std_obj=None, std_obj_profile=None):
        super().__init__(name, alias)
        self.type = 'table'
        self.schema = schema
        self.project = project
        self.s3_bucket = s3_bucket
        self.s3_path = s3_path
        self.header_object = header_object
        self.delimiter = delimiter
        self.file_format = file_format
        self.data_has_header = data_has_header
        self.null_count = null_count
        self.duplicate_count = duplicate_count
        self.record_count = record_count
        self.std_obj = std_obj
        self.std_obj_profile = std_obj_profile


class Column(Node):
    def __init__(self, *, name=None, alias=None, sum=None, min=None, max=None, avg=None, stddev=None, null_count=None,
                 unique_count=None, percentile_25=None, percentile_50=None, percentile_75=None, ordinal_position=None,
                 is_nullable=None, data_type=None, character_maximum_length=None, numeric_precision=None,
                 numeric_precision_radix=None, numeric_scale=None, **kwargs):
        super().__init__(name, alias)
        self.type = 'column'
        self.sum = sum
        self.min = min
        self.max = max
        self.avg = avg
        self.stddev = stddev
        self.null_count = null_count
        self.unique_count = unique_count
        self.percentile_25 = percentile_25
        self.percentile_50 = percentile_50
        self.percentile_75 = percentile_75
        self.ordinal_position = ordinal_position
        self.is_nullable = is_nullable
        self.data_type = data_type
        self.character_maximum_length = character_maximum_length
        self.numeric_precision = numeric_precision
        self.numeric_precision_radix = numeric_precision_radix
        self.numeric_scale = numeric_scale


class Value(Node):
    def __init__(self, *, name=None, alias=None):
        super().__init__(name, alias)
        self.type = 'value'
