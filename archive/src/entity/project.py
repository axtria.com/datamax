from sqlalchemy import Table, Column, Integer, String, Date, Boolean, MetaData

from src.entity import Entity

meta = MetaData()
t_project = Table('project', meta,
                  Column('id', Integer, primary_key=True),
                  Column('name', String),
                  Column('description', String),
                  Column('created_by', String),
                  Column('creation_date', Date),
                  Column('modified_by', String),
                  Column('modification_date', Date),
                  Column('is_active', Boolean)
                  )


class Project(Entity):
    def __init__(self, id, name, description, created_by, creation_date, modified_by, modification_date, is_active):
        super().__init__()
        self.id = id
        self.name = name
        self.description = description
        self.created_by = created_by
        self.creation_date = creation_date
        self.modified_by = modified_by
        self.modification_date = modification_date
        self.is_active = is_active
