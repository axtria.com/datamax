import datetime

from pandas import DataFrame
from sqlalchemy import Table, Column, Integer, String, Date, JSON, Boolean, DateTime, Sequence, MetaData

from api.utils import ConfigInstanceNotFound, ConfigNotFound, RuleDefinitionNotFound, RuleInstanceNotFound

meta = MetaData()

config_instance_id_seq = Sequence('config_instance_id_seq', metadata=meta)

t_rules = Table('rules', meta,
                Column('id', Integer, primary_key=True),
                Column('name', String),
                Column('description', String),
                Column('redshiftquery', String),
                Column('pyfunctionname', String),
                Column('createdby', String),
                Column('creationdate', Date),
                Column('modifiedby', String),
                Column('modificatindate', Date),
                Column('parameters', JSON),
                Column('level', String),
                Column('is_active', Boolean),
                Column('datatype', String),
                Column('ui_description', String)
                )

t_config = Table('config', meta,
                 Column('id', Integer, primary_key=True),
                 Column('name', String),
                 Column('params', JSON),
                 Column('std_object', String),
                 Column('version', Integer, default=1),
                 Column('is_active', Boolean, default=True),
                 Column('created_by', String),
                 Column('creation_date', DateTime, default=datetime.datetime.utcnow),
                 Column('modified_by', String),
                 Column('modification_date', DateTime, default=datetime.datetime.utcnow)
                 )

t_config_instance = Table('config_instance', meta,
                          Column('id', Integer, config_instance_id_seq,
                                 server_default=config_instance_id_seq.next_value()),
                          Column('config_id', Integer),
                          Column('rule_type', String),
                          Column('target_table', String),
                          Column('target_schema', String),
                          Column('detail_table_name', String),
                          Column('detail_schema_name', String),
                          Column('success_table_name', String),
                          Column('success_schema_name', String),
                          Column('failure_table_name', String),
                          Column('failure_schema_name', String),
                          Column('created_by', String),
                          Column('start_date', DateTime, default=datetime.datetime.utcnow),
                          Column('end_date', DateTime),
                          Column('success_count', Integer),
                          Column('fail_count', Integer),
                          Column('total_count', Integer),
                          Column('status', String),
                          Column('stage', Integer)
                          )

t_rule_instance = Table('rule_instance', meta,
                        Column('id', Integer, primary_key=True),
                        Column('config_instance_id', Integer),
                        Column('rule_id', Integer),
                        Column('query', String),
                        Column('end_date', DateTime),
                        Column('start_date', DateTime, default=datetime.datetime.utcnow),
                        Column('fail_count', Integer),
                        Column('status', String),
                        Column('stage', Integer)
                        )


class Config:
    def __init__(self, id: int, name: str, params: dict, std_object: str, version: int, is_active: bool,
                 created_by: str, creation_date: datetime.datetime.utcnow, modified_by: str,
                 modification_date: datetime.datetime.utcnow):
        self.id = id
        self.name = name
        self.params = params
        self.std_object = std_object
        self.version = version
        self.is_active = is_active
        self.created_by = created_by
        self.creation_date = creation_date
        self.modified_by = modified_by
        self.modification_date = modification_date

    @classmethod
    def initialise(cls, df: DataFrame):
        try:
            kwargs = df.to_dict(orient='records')[0]
            return cls(**kwargs)
        except IndexError as e:
            print(e)
            raise ConfigNotFound

    def __repr__(self):
        return str(self.__dict__)

    def __str__(self):
        return str(self.__dict__)


class ConfigInstance:
    def __init__(self, id: int, config_id, rule_type, target_table, target_schema,
                 detail_table_name, detail_schema_name, success_table_name, success_schema_name,
                 failure_table_name, failure_schema_name, created_by,
                 start_date, end_date, success_count, fail_count, total_count, status, stage):
        self.id = int(id) if id is not None else None
        self.config_id = config_id
        self.target_table = target_table
        self.target_schema = target_schema
        self.rule_type = rule_type
        self.detail_table_name = detail_table_name
        self.detail_schema_name = detail_schema_name
        self.success_table_name = success_table_name
        self.success_schema_name = success_schema_name
        self.failure_table_name = failure_table_name
        self.failure_schema_name = failure_schema_name
        self.created_by = created_by
        self.start_date = start_date
        self.end_date = end_date
        self.success_count = success_count
        self.fail_count = fail_count
        self.total_count = total_count
        self.status = status
        self.stage = stage

    @classmethod
    def initialise(cls, df: DataFrame):
        try:
            kwargs = df.to_dict(orient='records')[0]
            return cls(**kwargs)
        except IndexError as e:
            print(e)
            raise ConfigInstanceNotFound


class Rule:
    def __init__(self, id, name, description, redshiftquery, pyfunctionname, createdby, creationdate, modifiedby,
                 modificatindate, parameters, level, is_active, datatype, ui_description):
        self.id = id
        self.name = name
        self.description = description
        self.redshiftquery = redshiftquery
        self.pyfunctionname = pyfunctionname
        self.createdby = createdby
        self.creationdate = creationdate
        self.modifiedby = modifiedby
        self.modificatindate = modificatindate
        self.parameters = parameters
        self.level = level
        self.is_active = is_active
        self.datatype = datatype
        self.ui_description = ui_description

    @classmethod
    def initialise(cls, df: DataFrame):
        try:
            kwargs = df.to_dict(orient='records')[0]
            return cls(**kwargs)
        except IndexError as e:
            print(e)
            raise RuleDefinitionNotFound


class RuleInstance:
    def __init__(self, id: int, config_instance_id, rule_id, query, end_date, start_date, fail_count, status,
                 stage):
        self.id = int(id)
        self.config_instance_id = config_instance_id
        self.rule_id = rule_id
        self.query = query
        self.end_date = end_date
        self.start_date = start_date
        self.fail_count = fail_count
        self.status = status
        self.stage = stage

    @classmethod
    def initialise(cls, df: DataFrame):
        try:
            kwargs = df.to_dict(orient='records')[0]
            return cls(**kwargs)
        except IndexError as e:
            print(e)
            raise RuleInstanceNotFound
