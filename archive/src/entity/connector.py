from sqlalchemy import MetaData, Table, Integer, Column, String, Date, JSON, Boolean

meta = MetaData()

t_connector = Table('connector', meta,
                  Column('id', Integer, primary_key=True),
                  Column('name', String),
                  Column('description', String),
                  Column('project_id', Integer),
                  Column('connection', JSON),
                  Column('created_by', String),
                  Column('creation_date', Date),
                  Column('modified_by', String),
                  Column('modification_date', Date),
                  Column('is_active', Boolean)
                  )
