from pandas import DataFrame

from api.utils import ConfigNotFound


class Entity:

    def __init__(self, **kwargs):
        pass

    @classmethod
    def initialise(cls, df: DataFrame):
        try:
            kwargs = df.to_dict(orient='records')[0]
            return cls(**kwargs)
        except IndexError as e:
            print(e)
            raise ConfigNotFound

    def __repr__(self):
        return str(self.__dict__)

    def __str__(self):
        return str(self.__dict__)
