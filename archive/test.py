import unittest
import sys
import os


from psycopg2.extras import json
from redshift_validate import Refshift_Validate
sys.path.append(os.getcwd())

from api.vendor import Postgres
from api.vendor import Redshift
from api.vendor import Neptune


class Test(unittest.TestCase):

        
    def test_validateSO(self):
        # source = "SIQ"
        # so = "so.Axtria.SalesIQ.CallPlan" 
        # "so.Axtria SalesIQ.Hierarchy"  

        neptune = Neptune()
        table = neptune.get_table('f9fd81ab-411b-4521-8a50-ceb0067968df')
        
        """ source = "SHS"
        so = "so.SHS.Retail Sales"
        tablename = "s3_67ea2d14ac9e459d8a8854ba564e4ce4_bk"
        schema = "source" """
        source = 'SHS'
        so = table.std_obj
        tablename = table.name
        schema = table.schema
        Refshift_Validate(Redshift(), Postgres()).validate(source,so,tablename,schema)
        
        self.assertEqual(1,1)

    """ def test_getSOValidationRules(self):
        so = "so.Axtria SalesIQ.Hierarchy"
        df = Postgres().getSOValidationRules(so)
        print(json.dumps(df))
        self.assertGreater(len(df),0) """

    def test_populateRedshiftQuery(self):
        tablename = "rd_table_name"
        schema = 'public'
        config = json.loads('[{"parameters": [{"param": "startdate", "value": "Effective Start Date"}, {"param": "enddate", "value": "Effective End Date"}, {"param": "operator", "value": ">"}, {"param": "dateformat", "value": "YYYY-MM-DD"},{"param": "tablename", "value": "XXX"}], "redshiftquery": "select TO_DATE({startdate},{dateformat}) {operator}  TO_DATE({enddate},{dateformat}) from {tablename}", "id": 1, "ruleid": 1002, "source": "SIQ", "so": "so.Axtria SalesIQ.Hierarchy", "isexcluded": false, "prerequisites": null}]')
        Refshift_Validate(Redshift(), Postgres()).populateRedShiftQueryParam(tablename,config,schema)
        self.assertEqual(1,1)

    """ def test_insertNullRecord(self):
        Postgres().InsertProcess(None, None,'InProgress',None) """

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(Test("test_validateSO"))
    runner = unittest.TextTestRunner()
    runner.run(suite)
    # unittest.main()
    