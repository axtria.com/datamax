import re

class Validations():

    def __init__(self, redshift, postgresdb):
        self.redshift = redshift
        self.postgresdb = postgresdb


    def mandatoryColumnCheck( self, config ):
        query = 'select final.mandatory as missingcolumn  from (select * from {columnset} s left join ( SELECT "column"  FROM PG_TABLE_DEF pgtd where tablename= {tablename} ) o on s.c = o.column ) final where final.column is null'
        tablename = ''
        columns = ''
        for param in config["parameters"]:
            if ( param["param"] == "tablename"):
                tablename = param["value"]
            if ( param["param"]== "colnames"):
                columns = param["value"]

        columns = [' select ' +   '"' + col + '" '  + ' as mandatory ' + ' union' for col in columns]
        
        columnset = re.sub('union$','',(" ".join(columns)).strip())
        query = query.format(columnset = columnset, tablename = tablename )
        retdf = self.redshift.execute(query) 
        missingmandatorycols = ",".join(list(retdf.iloc[0,:]))
        return ( retdf, missingmandatorycols )



    