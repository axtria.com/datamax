"""
This code is dependent on the format of data reference library. In case, the reference is in the graph,

"""

# inputs
from collections import Counter

g = ""
columns = ''

# output
tables = []

# process
for column in columns:
    table = g.V().has('alias', column).out('TC').values('alias')
    tables.append(table)

closest_matched_file = Counter(tables).most_common(1)
