from glob import glob
import json
import pandas as pd


def alias_match(raw_cols_of_new_file):
#################
    col_to_obj_map = []
    
    for so_path in glob('knowledgebase/*.json'):
        so = json.load(open(so_path,'r'))
        df = pd.DataFrame(so['attributes'])
        df['src_name'] = so['src_name']
        df['object_name'] = so['object_name']
        df['path'] = so_path
        col_to_obj_map.append(df)
        
    col_to_obj_map = pd.concat(col_to_obj_map).reset_index(drop=True)
    ##################
    
    alias_to_cols = json.load(open('aliasDict.json','r'))
    col_to_alias = {k:i for i,j in alias_to_cols.items() for k in j}
    
    #################
    # raw_cols_of_new_file = col_to_obj_map['actual_col_name'].head(65).tolist() + col_to_obj_map['actual_col_name'].tail(5).tolist()
    #################
    df = pd.DataFrame({'raw_col':raw_cols_of_new_file})
    df['alias'] = df['raw_col'].map(col_to_alias)
    
    df = pd.merge(df,col_to_obj_map, left_on='alias',right_on='actual_col_name', how ='left')
    out = df.groupby(['src_name','object_name'])['path'].count().reset_index().sort_values('path',ascending=False)
    out['confidence'] = out['path']*100/out['path'].sum()
    out['match_percentage'] = out['path']*100/len(raw_cols_of_new_file)
    out.rename(columns={'path':'col_match_count'}, inplace=True)
    
    return out

