# datamax/api/catalog_viewer.py


from flask import Blueprint, render_template, request, jsonify

from api.vendor import Neptune

catalog_viewer = Blueprint('catalog_viewer', __name__)

# Vendors
catalog = Neptune()


@catalog_viewer.route('/')
def index():
    context = {"json": {"name": "hello world"}}
    return render_template('catalog_viewer/index.html', **context)


@catalog_viewer.route('/table')
def table():
    response = catalog.get_table(request.args.get("id")).__dict__
    return jsonify(response)


@catalog_viewer.route('/status')
def status():
    response = catalog.get_status()
    return response


@catalog_viewer.route('/connected_columns')
def connected_columns():
    response = catalog.get_columns(request.args.get("id"))
    return response


@catalog_viewer.route('/get_column')
def column():
    response = catalog.get_column(request.args.get("id")).__dict__
    return jsonify(response)


@catalog_viewer.route('/query')
def query():
    return catalog.connection.make_signed_request('GET', 'gremlin', request.args.get("q"))
