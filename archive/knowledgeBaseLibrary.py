import pandas as pd
from collections import OrderedDict
import json

objs = pd.read_csv('Master_SRC_Library.csv')
cols = pd.read_csv('Master_SRC_Column_Library.csv')

objs_rename = {
        
        'src_nm':'src_name',
        'obj_nm':'object_name',
        'obj_desc':'decription',
        'data_freq':'frequency',
}

cols_rename = {
        
        'act_col_nm':'actual_col_name',
        'business_nm':'business_name',
        'business_desc':'description',
}


objs.rename(columns = objs_rename, inplace=True)
cols.rename(columns = cols_rename, inplace=True)

objs_cols = ['src_name','object_name','decription','frequency']
cols_cols = ['actual_col_name','business_name','description','data_type']

for i,obj in objs.iterrows():
    
    x = obj[objs_cols].to_dict(OrderedDict)
    y = cols.loc[cols['object_id'] == obj['object_id'], cols_cols].to_dict(orient='records')
    x['attributes'] = y

    with open('knowledgeBase/so.'+obj['src_name']+'.'+obj['object_name']+'.json','w') as f:
        f.write(json.dumps(x, indent=4).replace('NaN','null'))
