import pandas as pd
import json

pd.set_option('display.max_columns', 500)

objs = pd.read_csv('Master_SRC_Library.csv', usecols=['object_id','obj_nm'])
cols = pd.read_csv('Master_SRC_Column_Library.csv', usecols=['object_id','act_col_nm'])

cols = cols.merge(objs, on='object_id')

x = cols.groupby('act_col_nm').agg({'obj_nm':lambda x: x.tolist()})
y = x.to_dict()['obj_nm']

with open('reverseDict.json','w') as f:
    f.write(json.dumps(y, indent=4))
    

####################

cols['alias'] = cols['act_col_nm'].apply(lambda x: [x])
d = dict(zip(cols['act_col_nm'],cols['alias']))

with open('aliasDict.json','w') as f:
    f.write(json.dumps(d, indent=4))
