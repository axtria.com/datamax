from flask import Flask, redirect

app = Flask(__name__)

@app.route('/DataValidatorTool/')
def func():
	return redirect("http://salesiqservices.axtria.com:82")

if __name__ == '__main__':
	app.run()