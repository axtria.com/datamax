import os
from flask import render_template, request, redirect,Flask
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_table as dt
from flask import send_file
import pandas_profiling as pdp
from uuid import uuid4 as UID
from glob import glob
from zipfile import ZipFile
import pandas as pd


import src.shared as shared
from src.Validate import Validate
from src.filereader import readDF

UPLOAD_DIRECTORY =  os.path.dirname(os.path.realpath(__file__)) + '/data/'

def folderWalk(ID):
    files = glob(UPLOAD_DIRECTORY+ID+'/**/*.*',recursive=True)
    VALID_FORMATS = ('csv','psv','xlsx')
    filtered_files = list(filter(lambda x: x.split('.')[-1] in VALID_FORMATS, files))
    label = lambda x: x.replace('\\','/').split('/')[-1]
    filtered_files = [{'label':label(f),'value':f.replace('\\','/')} for f in filtered_files]
    return filtered_files

def getTable(pathOrDf, nrows=100):
    try:
        if isinstance(pathOrDf,str):
            df = readDF(pathOrDf, nrows=nrows)
        else: 
            df = pathOrDf
        return dt.DataTable(data=df.to_dict('records') ,columns=[{'name':i,'id':i} for i in df.columns.tolist()] ,style_table={'overflowX': 'scroll'}, style_header = {'text-align':'left', 'font-weight':'bold'} ,style_data={'text-align':'left'})
    except:
        return ''

server = Flask(__name__)        
app = dash.Dash(server=server)
app.scripts.config.serve_locally = True
app.title = 'Data Validator'

                   
HORIZONTAL_LINE = html.Hr(style={'margin':5})

EMPTY_LINE = html.Br()

HEADER = (
          
    html.A(html.Img(src='static/logo2.png', style={'height':50,'width':235}),href='/'),
    html.Div([html.A('Config Template',href='/download_config'),html.I('ws',style={'color':'white'}), html.A('Help', href='/help', target='_blank')], style={'float':'right'}),
    HORIZONTAL_LINE, 
)
 
SESSION_INFO = (
                
    html.Div([html.B('Current Dataset Key: '), html.B(id='dataset-id', style={'color':'red'})], style={'margin-top':5,'margin-bottom':5}),
    HORIZONTAL_LINE,                                            
)
        
UPLOAD = (
          
    html.B('Step 1:'),
    EMPTY_LINE,
    html.Div([html.A('Upload data and configs',href='/upload'),html.Span(['. Supported file formats are: ',html.B('.csv, .psv, .xlsx, and .zip'),' (Only .csv, .psv, .xlsx are allowed inside a .zip file)'], style={'font-size':12})],style={'width': '100%','height': '60px','lineHeight': '60px','borderWidth': '1px','borderStyle': 'dashed','borderRadius': '5px','textAlign': 'center','margin-bottom':5}),
    dcc.Input(id='dataset-id-input',placeholder='Or Enter Dataset Key...',type='text',value=''),
    html.Button('Load Data by Dataset Key', id='load-button', style={'color':'green'}),
    HORIZONTAL_LINE,          
)
        

DATA_DROPDOWN = (
                    
    html.B('Step 2:', id='step2-label', style={'margin-top':4,'margin-bottom':10}),
    html.P(['Select data file:', html.Span(id='more-than-2-file-error')]),
    dcc.Dropdown(id='dropdown1', options=[], multi=True),
    html.Center(html.A(html.Button('Data Visualization', id='data-visualization-button', n_clicks_timestamp='0' ),id='data-visualization-link', target='_blank')),
    HORIZONTAL_LINE,                 
)                   

CONFIG_DROPDOWN = (
                   
    html.B('Step 3:', style={'margin-top':4,'margin-bottom':10}),
    html.P('Select config file:'),
    dcc.Dropdown(id='dropdown2', options=[]),
    HORIZONTAL_LINE,
)

VALIDATE_BUTTON = (
        
    HORIZONTAL_LINE,               
    html.Center([
                html.Button('Validate', id='validate-button', n_clicks_timestamp='0'),
                html.Button('File Compare', id='file-comparision-button', n_clicks_timestamp='0'),
                ]),
    HORIZONTAL_LINE
)
                   
app.layout = html.Div([
        
    dcc.Location(id='url',refresh=False),
    html.Div(HEADER),
    html.Div(SESSION_INFO),
    html.Div(UPLOAD),
    html.Div(DATA_DROPDOWN),
    html.Div(id='data-sample-display'),
    html.Div(CONFIG_DROPDOWN),
    html.Div(id='config-display'),
    html.Div(VALIDATE_BUTTON),
    html.Div(id='final-output'),
    EMPTY_LINE, EMPTY_LINE, EMPTY_LINE

], style={'margin':5})


@app.callback(Output('dataset-id','children'),
              [Input('url','pathname'),Input('load-button', 'n_clicks')],
              [State('dataset-id-input', 'value')])
def dataset_id_update(pathname,n_clicks,dataset_id):
    if n_clicks and dataset_id:
        shared.session_id = dataset_id
        return shared.session_id
    shared.session_id = str(pathname).split('/')[-1]
    return shared.session_id if len(shared.session_id) > 0 else 'N/A'


@app.callback(Output('dropdown1','options'),
              [Input('url','pathname'),Input('load-button', 'n_clicks')],
              [State('dataset-id-input', 'value')])
def dropdown1_update(pathname,n_clicks,dataset_id):
    if n_clicks and dataset_id:
        shared.session_id = dataset_id
        return folderWalk(shared.session_id)
    shared.session_id = str(pathname).split('/')[-1]
    return folderWalk(shared.session_id) if len(shared.session_id) > 0 else []


@app.callback(Output('dropdown2','options'),
              [Input('url','pathname'),Input('load-button', 'n_clicks')],
              [State('dataset-id-input', 'value')])
def dropdown2_update(pathname,n_clicks,dataset_id):
    if n_clicks and dataset_id:
        shared.session_id = dataset_id
        return folderWalk(shared.session_id)
    shared.session_id = str(pathname).split('/')[-1]
    return folderWalk(shared.session_id) if len(shared.session_id) > 0 else []


@app.callback(Output('data-sample-display','children'),
              [Input('dropdown1','value')])
def data_sample_display(value):
    if value:
        shared.dataset = value
        return [html.Center(html.H5(value[-1].replace('\\','/').split('/')[-1]+ ' Sample')),getTable(value[-1], nrows=5)]

        
@app.callback(Output('config-display','children'),
              [Input('dropdown2','value')])
def config_display(value):
    shared.config = value
    return getTable(value)

    
@app.callback(Output('validate-button','style'),
              [Input('dropdown1','value'),Input('dropdown2','value')])
def update_validate_button_style(v1,v2):
    if v2:
        if len(v1) == 1:
            return {'color':'green'}
    return {'color':'red', 'pointer-events': 'none'}


@app.callback(Output('file-comparision-button','style'),
              [Input('dropdown1','value'),Input('dropdown2','value')])
def update_file_comparision_style(v1,v2):
    if v2:
        if len(v1) == 2:
            return {'color':'green'}
    return {'color':'red', 'pointer-events': 'none'}


@app.callback(Output('data-visualization-button','style'),
              [Input('dropdown1','value')])
def update_visualization_button_style(v1):
    if v1:
        if len(v1) >= 1:
            return {'color':'green'}
    return {'color':'red', 'pointer-events': 'none'}


@app.callback(Output('data-visualization-link','href'),
              [Input('dropdown1','value')])
def update_visualization_link(v1):
    if v1:
        if len(v1) >= 1:
            return '/statistics'
            
@app.callback(Output('data-visualization-link','style'),
              [Input('dropdown1','value')])
def update_visualization_link_style(v1):
    if not v1:
        return {'pointer-events': 'none'}
    if len(v1) >= 1:
        return {}
    return {'pointer-events': 'none'}

            
@app.callback(Output('more-than-2-file-error','children'),
              [Input('dropdown1','value')])
def max_2_data_files_error(v):
    if v:
        if len(v) > 2:
            return html.B(' (You cannot select more than 2 files.)', style={'color':'red'})

        
@app.callback(Output('final-output', 'children'),[Input('validate-button','n_clicks_timestamp'),Input('file-comparision-button','n_clicks_timestamp')])
def validate_action(validate_click, file_compare_click):
    
    validate_click = int(validate_click)
    file_compare_click = int(file_compare_click)
    
    if validate_click + file_compare_click:
        if validate_click > file_compare_click:
            #Validate Button Action
            print('inside file comparision')
            dpath = shared.dataset[0]
            cpath = shared.config
            fname = dpath.replace('\\','/').split('/')[-1]
            data = readDF(dpath)
            config = readDF(cpath)
            
            shared.result = Validate({fname:data},config)
            
            if shared.result['ruleexecutionstatus']:
                return html.Center('No violation of rules is found. :)',style={'color':'green','font-size':16, 'font-weight':'bold'})
            
            view = [
            
            html.Center(html.H2('Validation Summary')),
            getTable(shared.result['summary']), 
            html.Center(html.A(html.Button('Download Validation Details'), href='/download_validation_detail')),
            
            ]
            return view
        #File Compare Button Action    
        d1path = shared.dataset[0]
        d2path = shared.dataset[1]
        f1name = d1path.replace('\\','/').split('/')[-1]
        f2name = d2path.replace('\\','/').split('/')[-1]
        cpath = shared.config
        data1 = readDF(d1path)
        data2 = readDF(d2path)
        config = readDF(cpath)
        
        shared.result = Validate({f1name:data1,f2name:data2},config)
        
        if shared.result['ruleexecutionstatus']:
            return html.Center('No violation of file comparision rule is found. :)',style={'color':'green','font-size':16, 'font-weight':'bold'})
        
        view = [
        
        html.Center(html.H2('File Comparision Summary')),
        getTable(shared.result['summary']), 
        html.Center(html.A(html.Button('Download Validation Details'), href='/download_validation_detail')),
        
        ]
        return view


        
@app.server.route('/statistics') 
def show_statistics():
    fpath = shared.dataset[-1]
    fname = fpath.replace('\\','/').split('/')[-1]
    html_report = pdp.ProfileReport(readDF(fpath)).to_html()
    html_report = html_report.replace('<h1>Overview</h1>','<h1>'+fname+' Overview</h1>')
    return html_report
        


@app.server.route('/download_validation_detail')
def download_validation_detail():
    
    if len(shared.dataset) == 1:
        fpath = shared.dataset[0].split('.')[0] + '__output.xlsx'
    elif len(shared.dataset) == 2:
        fpath = shared.dataset[0].split('.')[0] + '__FileCompare_output.xlsx'

    fname = fpath.replace('\\','/').split('/')[-1]
    
    xl = pd.ExcelWriter(fpath) 
    shared.result['summary'].to_excel(xl,'summary',index=None)
    
    if len(shared.result['details']) == 1:
        key = list(shared.result['details'].keys())[0]
        shared.result['details'][key].to_excel(xl,'details',index=None)
    else:
        for sname, df in shared.result['details'].items():
            sname = sname.split('_')[-1] if sname.endswith('_edited') or sname.endswith('_deleted') or sname.endswith('_inserted') else sname
            df.to_excel(xl, sname, index=None)
        
    xl.save()
    
    return send_file(fpath,
                     mimetype='text/csv',
                     attachment_filename=fname,
                     as_attachment=True) 
    
    
@app.server.route('/download_config') 
def download_config(): 
    return send_file('static/config.csv',
                     mimetype='text/csv',
                     attachment_filename='configuration_sample.csv',
                     as_attachment=True)    

    
@app.server.route('/upload') 
def upload():
    shared.session_id = UID().hex[::3]
    return render_template('upload.htm',dataset_id=shared.session_id)

@app.server.route('/help') 
def helpf():
    return render_template('help.htm')
    
@app.server.route('/uploader', methods=['POST'])
def uploader():
    if request.method == 'POST':
        files = request.files.getlist('file')
        dataset_id = request.form['dataset_id']
        if files:
            os.makedirs(UPLOAD_DIRECTORY+dataset_id, exist_ok=True)
        for f in files:
            f.save(UPLOAD_DIRECTORY+dataset_id+'/'+f.filename)
            if f.filename.endswith('.zip'):
                ZipFile(UPLOAD_DIRECTORY+dataset_id+'/'+f.filename,'r').extractall(UPLOAD_DIRECTORY+dataset_id)
        return redirect('/'+dataset_id)  
            
if __name__ == '__main__':
    app.run_server(debug=True)
