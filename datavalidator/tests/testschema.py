import unittest
import pandas as pd
import os
import sys

import yaml

sys.path.append(os.getcwd())

import src.Validate as Validate


with open( os.path.abspath(os.path.join(os.path.join(os.getcwd(),"tests"),"testconfig.yml")), 'r') as ymlfile:
    cfg = yaml.load(ymlfile)


class TestValidation(unittest.TestCase):

    def setUp(self):
        self.test_config_01 = "data/config01.csv"
        self.datadirectorypath = os.path.abspath(os.path.join(os.path.join(os.getcwd(),"tests"),"data"))
        
    def test_01(self):
        self.assertEqual(True, True)

    def test_schema(self): 
        methoddirectorypath = os.path.join(self.datadirectorypath , "test_schema")
        accounts = pd.read_csv(os.path.join(methoddirectorypath,cfg['test_schema']['file']), sep=cfg['test_schema']['delimiter'])
        config = pd.read_csv(os.path.join(methoddirectorypath,cfg['test_schema']['config']))
        result = Validate.Validate({'file':accounts},config)
        self.assertEqual(True,result['ruleexecutionstatus'])
        self.assertEqual(True, True)

    def test_multiplefiles(self):   
        methoddirectorypath = os.path.join(self.datadirectorypath , "test_multiplefiles")   
        file_01_name = cfg['test_multiplefiles']['file_01']
        file_02_name =  cfg['test_multiplefiles']['file_02']       
        file_01 = pd.read_csv(os.path.join(methoddirectorypath,file_01_name), sep=cfg['test_multiplefiles']['delimiter'])
        file_02 = pd.read_csv(os.path.join(methoddirectorypath,file_02_name), sep=cfg['test_multiplefiles']['delimiter'])
        config = pd.read_csv(os.path.join(methoddirectorypath,cfg['test_multiplefiles']['config']))
        result = Validate.Validate({file_01_name:file_01,file_02_name:file_02},config)
        self.assertEqual(True,result['ruleexecutionstatus'])
        print(os.path.abspath(os.path.join(os.path.join(os.getcwd(),"tests"),"data")))
        self.assertEqual(True, True)

    def test_groupby_blankcount(self):        
        methoddirectorypath = os.path.join(self.datadirectorypath , "test_groupby_blankcount")   
        filename = cfg['test_groupby_blankcount']['file']  
        dfFile = pd.read_csv(os.path.join(methoddirectorypath,filename), sep=cfg['test_groupby_blankcount']['delimiter'])
        config = pd.read_csv(os.path.join(methoddirectorypath,cfg['test_groupby_blankcount']['config']))
        result = Validate.Validate({filename:dfFile},config)
        self.assertEqual(True,result['ruleexecutionstatus'])
        self.assertEqual(True, True)
if __name__ == '__main__':
    unittest.main()


