import re
import numpy as np
import pandas as pd


'''
Returns the object as key if not found in the dictionary
'''
class DictionaryToKey(dict):
    def __missing__(self, key):
        return key


class CSVParser:
    '''
    user provided configuration file
    '''
    def __init__(self, dsconfig):
        self.dsvalidation = dsconfig
        self.rules = []
        self.__parse__()


    '''
    TODO :better error handling
    '''    
    def __parse__(self):
        errormessage = ''
        try:
            for index,row in self.dsvalidation.iterrows():
                self.rules.append({'id': str(row['Id']), 'param':row['Parmaters']})
        except Exception as e:
            errormessage = str(e)
           
        return errormessage
  
    '''
    parses the string ((AccountNumber,External_Account_Number__c,Speciality__c)) or
    ((AccountNumber,External_Account_Number__c,Speciality__c), Speciality__c) and returns
    array as columnlist
    e.g. here (AccountNumber,External_Account_Number__c,Speciality__c)
    '''
    def parse_param_columnlist(self,arg):
#       below code parsers ((billingdate, date, billingstate)) to return [billingdate, date, billingstate]   
#       param = re.findall("^\(\([\w\s\d,]*\)\)$", arg)[0]
#       below code can parse ((billingdate,date,billingdate)) as well as ((billingdate, date , billingdate),state)
#        to return the initial column list as [billingdate, date, billingdate]
        param = re.findall("(^\(\([\w\s\d,]*\))", arg)[0]
        param = param.replace("((","").replace("))","").replace(")","").split(",")
        param = [col.strip() for col in param]
        print(param)
        return param
 
    '''
    parses the string "<=1000", returns two arguments "<=", 1000
    '''
    def parse_param_rowcount(self, arg):
        param = re.findall("(<=|<|>=|>|=)(\\d+)",arg)[0]  
        param = [val.strip() for val in param]
        return param

    '''
    parses the string "(Speciality__c, (Oncology, Neurology , Pediatrics),Y)", returns
    col - Speciality__c,
    vallist = (Oncology, Neurology , Pediatrics)
    ignorecase = 'Y'
    '''
    def parse_param_values(self, arg):
        param =  arg.split(",")
        col = param[0].replace("(","").replace(")","").strip()
        vallist = [x.replace("(","").replace(")","").strip() for x in param[1:-1]]
        ignorecase = param[-1].replace("(","").replace(")","").strip()
        return (col, vallist, ignorecase)
    
    '''
    parses the string (BillingDate, %d/%m/%Y,<=30/10/2018)
    col = BillingDate
    dateformat = %d/%m/%Y
    operator = <=
    dateparam = 30/10/2018
    '''    
    def parse_param_checkdate(self,arg):
        param = arg.split(",")
        col = param[0].replace("(","").replace(")","").strip()
        dateformat = param[1].replace("(","").replace(")","").strip()
        operator, dateparam = re.findall("(<=|<|>=|>|=)([\\d\-\\\/]*)",param[2])[0]
        return (col, dateformat, operator, dateparam)
 
    '''
    parses the string ((BillingDate),%m/%d/%Y)
    returns collist = [BillingDate] python array
    dateformat = %m/%d/%Y
    '''
    def parse_param_checkdateformat(self,arg):
        collist = self.parse_param_columnlist(arg)
        dateformat = arg.split(",")[-1].replace(")","").strip()
        return (collist, dateformat)
      
    '''
    parses the string ((BillingDate),%d/%m/%Y,01/01/2018,30/10/2018)
    returns collist = [BillingDate] python array
    dateformat = %d/%m/%Y
    startdate = 01/01/2018
    enddate = 30/10/2018
    '''    
    def parse_param_daterange(self,arg):
        collist = self.parse_param_columnlist(arg)
        dateformat, startdate, enddate = [ x.replace(")","").strip() for x in arg.split(",")[-3:]]
        return (collist,dateformat,startdate,enddate)

    '''
    parses (BillingDate, >= , BillingDate1, %m/%d/%Y) returns
    startcolumn = BillingDate
    operator =   >= 
    endcolumn = BillingDate1 
    dateformat = %m/%d/%Y
    '''
    def parse_param_datecolumnorder(self,arg):
        startcolumn, operator, endcolumn, dateformat =  [ x.replace("(","").replace(")","").strip() for x in arg.split(",")]
        return (startcolumn,operator,endcolumn,dateformat)
     
    '''
    pases string (AccountNumber, <, External_Account_Number__c)
    col1 = AccountNumber
    operator = <
    col2 = External_Account_Number_c
    '''    
    def parse_param_comparecolumns(self,arg):
        param = arg.split(",")
        col1 = param[0].replace("(","").replace(")","").strip()
        operator = re.findall("(<=|<|>=|>|=)",param[1])[0]
        col2 = param[0].replace("(","").replace(")","").strip()
        return (col1,operator,col2)        

    '''
    parses string ((AccountNumber),<=10) , returns
    collist =[AccountNumber] python array
    param = ['<=', 10]
    '''    
    def parse_param_columnvaluelength(self,arg):
        collist = self.parse_param_columnlist(arg)
        param = self.parse_param_rowcount(arg.split(",")[-1].replace(",","").strip())
        return (collist,param[0],param[1])
 
       
    '''
    parses string ((file1,file2),(S.No.,Lead),(skills,jobcode)) , returns
    files = [file1,file2] python array
    compositecollist =[S.No.,Lead] python array
    columnstocompare = [skills,jobcode]
    '''    
    def parse_param_filecompare(self,arg):
        regex = '\(([^)]+)\\)'
        param = re.findall(regex,arg)  
        param = [ val.replace("(","").replace(")","") for val in param]
        return [[temp.strip() for temp in val.split(",")] for val in param]
               
#        files = [col.strip() for col in param[0].split(",")]
#        compositecollist = [col.strip() for col in param[1].split(",")]
#        return (files,compositecollist)  

    def parse_param_reference(self,arg):
        '''"((file1,file2),((id,name),(id,name)))",returns files = [file1,file2],
        file1_cols = ['id','name'], file2_cols= ['id','name']'''
        regex = '\(([^)]+)\\)'
        param = re.findall(regex,arg)  
        param = [ val.replace("(","").replace(")","") for val in param]
        files = [col.strip() for col in param[0].split(",")]
        file1_cols = [col.strip() for col in param[1].split(",")]
        file2_cols = [col.strip() for col in param[2].split(",")]
        return (files,file1_cols,file2_cols) 
    
    def parse_param_groupbyblankcount(self,arg):
        '''((Account_number,id),Territory,<=10), returns  [[Account_number,id],Territory, <=, 10]
        
        :param arg: ((Account_number,id),<=10)
        :type arg: string
        :return: returns collist, blankcolumn, operator, threshold
        :rtype: list
        '''
        param = self.parse_param_rowcount(arg.split(",")[-1].replace(",","").strip())
        collist = self.parse_param_columnlist(arg)
        blankcolumn = arg.split(",")[-2]
        return ( collist, blankcolumn, param[0],param[1])
        

        

     