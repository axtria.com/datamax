import sys
import datetime

import re
import numpy as np
import pandas as pd

from src.logger import LOGGER
from src.ruledictionary import RuleDictionary


'''
    Schema Level Validation
    Input is the dataset
    TODO : log file configuration
'''

class SchemaValiation:
    '''
    initialize the class. dsfile is data file e.g. Account.csv, dsuserconfig is rules provided by user in csv file
    '''

    def __init__(self, dsfile, parser):

        self.dsvalidation = dsfile
        self.parser = parser
        self.dsvalidationsummary = pd.DataFrame(
            columns=['status', 'description'])
        self.dsvalidationDetails = pd.DataFrame()
 
    
    def IsUnique(self, collist):
        '''Returns True if given columns contain unique values       
        :param collist: [description]
        :type collist: [type]
        :return: [description]
        :rtype: [type]
        '''

        rowcount = self.dsvalidation.shape[0]
        collist = self.parser.parse_param_columnlist(collist)
        param = []
        nonuniquecols = [col for col in collist if(
            not (rowcount == len(self.dsvalidation[col].unique())))]

        for col in nonuniquecols:
            # if( not (rowcount == len(self.dsvalidation[col].unique()))):
            param.append(
                {'column': col, 'rows': self.dsvalidation[col].duplicated(keep=False) == False})

        retparam = {
            'status': len(nonuniquecols) == 0,
            'summary': {'key': 'Columns Not Unique - ', 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
            'param': param
        }

        
        return retparam

    def IsColumnCombinationUnique(self, collist):
        collist = self.parser.parse_param_columnlist(collist)
        param = []
        nonuniquerows = self.dsvalidation[collist].duplicated(keep=False)
        param.append(
                {'column': ",".join(collist), 'rows': nonuniquerows == False})

        retparam = {
            'status': nonuniquerows.sum() == 0,
            'summary': {'key': 'Columns Combination Not Unique - ', 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
            'param': param
        }
        return retparam
    '''
    Returns True if column exists in a given sequence
    '''

    def CheckColumnSequence(self, colsequence):
        colsequence = self.parser.parse_param_columnlist(colsequence)
        retparam = {'status': int(','.join(self.dsvalidation.columns) == re.sub('\s*,\s*', '', ','.join(colsequence))),
                    'summary': {'key': 'Column sequence is  -  ', 'value': ",".join(self.dsvalidation.columns)}
                   }

        return retparam
 
       
    '''
    Check all mandatory columns exist in a file
    '''

    def CheckMandatoryColumns(self, collist):
        collist = self.parser.parse_param_columnlist(collist)
        mandatorycolfailed = [col for col in collist if not (
            col in self.dsvalidation.columns)]
        retparam = {'status': len(mandatorycolfailed) == 0,
                    'summary': {'key': 'Missing Mandatory Columns are  -  ', 'value': ",".join(mandatorycolfailed)}
                   }   
        return retparam

    '''
    Returns True if column contains null values
    '''
    
    def IsNull(self, collist):
        collist = self.parser.parse_param_columnlist(collist)
        param = []
        nullcols = ((self.dsvalidation[collist].columns[
                    self.dsvalidation[collist].isnull().any()]).values)

        for col in nullcols:   
            param.append(
                {'column': col, 'rows': self.dsvalidation[col].isnull() == False})
            
        retparam = {'status': len(nullcols) == 0, 'summary': {'key': 'Columns with NULL values are - ', 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
                    'param': param}

       
        return retparam

    '''
    Returns True if the total row count matches the given count, or total row count is less than threshold value
    '''
#    def CheckRowCount(self, threshold, count):

    def CheckRowCount(self, args):
        args =  self.parser.parse_param_rowcount(args) 
        operator = args[0]        
        threshold = args[1]

        rowcount = self.dsvalidation.shape[0]
        if threshold is not None:
            rowcountcheck = eval("{0} {1} {2}".format(self.dsvalidation.shape[0], operator, str(threshold) ))  
        retparam = {'status': rowcountcheck,
                    'summary': {'key': 'Row count is - ', 'value': str(rowcount)}
                   }
        return retparam

    '''
    Returns True if all the column values matches with provided unique set of values
    '''

    def CheckColumnUniqueValues(self, args):
        col,values,ignorecase = self.parser.parse_param_values(args)
        param = []
        if(ignorecase.lower() == 'y'):
            values = set(str(val).lower() for val in values)
            param.append({'column': col, 'rows': self.dsvalidation[
                         col].apply(lambda x: str(x).lower()).isin(values)})
        else:
            param.append(
                {'column': col, 'rows': self.dsvalidation[col].isin(values)})

        retparam = {'status': param[0]['rows'].sum() == self.dsvalidation.shape[0],
                    'summary':
                    {'key': 'Column Unique Values - ', 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
                    'param': param}

        return retparam

    '''
    Returns True if only one column exist with the matching set of column values
        TODO : check the requirement once again, is it the column name repeated or two columns with different names, but values same?
    '''

    def CheckDuplicateColumn(self, collist):
        collist = self.parser.parse_param_columnlist(collist)
        duplicatecolumns = []
#       mngle_duplicate_columns support does not exist with 3.7 as a result the columns with same name is read by pandas
#       as <colname>.<int> where integer starts with 1. 
#       Hence the below line of code checks this pattern against each column name and returns true if found a match
#       self.dsvalidation.columns.str.contains(col+".\w+",na=False,regex=True) > 0)
        for col in collist:
               if((self.dsvalidation.loc[:, [col]].shape)[1] > 1 or self.dsvalidation.columns.str.contains(col+".\w+",na=False,regex=True).sum() > 0) :
                      duplicatecolumns.append(col)
                      
        retparam = {'status': len(duplicatecolumns) == 0, 'summary': {'key': 'Duplicate Column - ', 'value': ",".join(duplicatecolumns)}}

        return retparam
 
      

    '''
    Returns True if the column values are numeric
    '''
    def CheckNumeric(self, arg):
        collist = self.parser.parse_param_columnlist(arg)
        retparam = self.__check_format__(collist, "^[0-9]+.?[0-9]+$", "Check Numeric Format -")
        return retparam
    '''
    Returns true if the column values are alphanumeric
    '''

    def CheckAlphaNumeric(self, collist):
        collist = self.parser.parse_param_columnlist(collist)
        checkalphanumericfailed = []
        param = []
        checkalphanumericfailed = [col for col in collist if not (
            sum(self.dsvalidation[col].astype(str).str.contains(r'^[a-zA-Z0-9]*$',
                                                    na=False, regex=True)) == self.dsvalidation.shape[0])]

        for col in checkalphanumericfailed:
            param.append({'column': col, 'rows': self.dsvalidation[col].astype(str).str.contains(r'^[a-zA-Z0-9]*$',
                                                                                     na=False, regex=True)})

        retparam = {'status': len(checkalphanumericfailed) == 0,
                    'summary':
                                {'key': 'Non Alpha Numeric Check - ',
                                    'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])
                                },
                    'param': param}

        return retparam
    '''
    Returns True if the column contains valid email address
    '''

    def CheckEmailFormat(self, collist):
        collist = self.parser.parse_param_columnlist(collist)
        checkemailformatfailed = []
        param = []
        checkemailformatfailed = [col for col in collist if not (
            sum(self.dsvalidation[col].astype(str).str.contains(r'^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$',
                                                    na=False, regex=True)) == self.dsvalidation.shape[0])]

        for col in checkemailformatfailed:
            param.append({'column': col, 'rows': self.dsvalidation[col].astype(str).str.contains(r'^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$',
                                                                                     na=False, regex=True)})

        retparam = {'status': len(checkemailformatfailed) == 0,
                    'summary':
                    {'key': 'Not an Email- ', 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
                    'param': param}
        return retparam
       
    
    '''
    Check the column values are integer type
    '''
    def CheckInteger(self, arg):
        collist = self.parser.parse_param_columnlist(arg)
        retparam = self.__check_format__(collist, "^[0-9]+$", "Check Integer Format -")
        return retparam     

    '''
    Check the column values are character type
    '''
    def CheckCharacters(self, arg):
        collist = self.parser.parse_param_columnlist(arg)
        retparam = self.__check_format__(collist, "^[A-Za-z]+$", "Check Characters Format -")
        return retparam 
    
    '''
    Check column with uppercase alphabets
    '''
    def CheckUpperCase(self, arg):
        collist = self.parser.parse_param_columnlist(arg)
        retparam = self.__check_format__(collist, "^[A-Z]+$", "Check Characters Format -")
        return retparam  

    '''
    Check date format %d/%m/%Y
    '''

    def CheckDateFormat(self, arg):
        collist, dateformat = self.parser.parse_param_checkdateformat(arg)
        param = []
        result = [(col, self.dsvalidation[col].apply(self.__check_dateformat, dateformat= dateformat) )for col in collist]

        for val in result:
            if(val[1].sum() < self.dsvalidation.shape[0]): 
                   param.append({'column': val[0], 'rows': val[1]})
          
        retparam = {'status': len(param) == 0,
                    'summary':
                    {'key': "Check DateFormat - ", 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
                    'param': param}
        return retparam 
       

    '''
    Returns True if the column date is before/after the provided date
        # sample - dateformat :'%Y-%m-%d'
    '''
     
    def CheckDate(self, arg):
        col, dateformat, operator, dateparam = self.parser.parse_param_checkdate(arg)
#        dateparam = datetime.datetime.strptime(dateparam, dateformat)
        param = []
#       below line of code - compares the 
        values = self.dsvalidation[col].apply(lambda x : self.__compare_date__("datetime.datetime.strptime( '{3}','{0}') {1} datetime.datetime.strptime('{2}','{0}')".format( dateformat,operator,dateparam,x)))
        values = values.map(lambda x : x[0])
        param.append({'column': col, 'rows': values})
        retparam = {'status': values.sum() == self.dsvalidationsummary.shape[0],
                    'summary':
                    {'key': 'DateCheck - ', 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
                    'param': param}
        return retparam


    '''
    Returns True if the column date values are between startdate and enddate
    '''

    def CheckDateRange(self, arg):
        collist, dateformat, startdate, enddate = self.parser.parse_param_daterange(arg)
        checkdaterangefailed = []
        daterangefailedcolumns = []
        param = []
        startdate = datetime.datetime.strptime(startdate, dateformat)
        enddate = datetime.datetime.strptime(enddate, dateformat)

        checkdaterangefailed = [(col, self.dsvalidation[col].apply(lambda x: self.__compare_daterange__(dateformat, x, startdate, enddate))) for col in collist]

        for val in checkdaterangefailed:
            daterangefailedcolumns.extend(val[0])  
            param.append({'column': val[0], 'rows': val[1].map(lambda x : x[0])})

        retparam = {'status': len(daterangefailedcolumns) == 0,
                    'summary':
                    {'key': 'DateRangeCheck - ', 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
                    'param': param}
        return retparam

    '''
    Returns True if the colbeforedate is prior date to the colafterdate
    '''
    def CheckColumnsDateOrder(self, arg):
        
        colbeforedate, operator, colafterdate, dateformat =  self.parser.parse_param_datecolumnorder(arg)
        param = []

        param.append({'column': colbeforedate + "_" + colafterdate,
                      'rows': self.dsvalidation.apply(self.__compare_columndateorder__, colbefore = colbeforedate, colafter = colafterdate, dateformat = dateformat, operator = operator, axis=1)})
        retparam = {
            'status': param[0]['rows'].sum() == self.dsvalidation.shape[0],
            'summary': {
                'key': 'ColumnsDateOrder - ('+colbeforedate + " >  " + colafterdate + ' )',
                'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])
            },
            'param': param
        }

        return retparam

    '''
    Compares column values
    '''
    def CompareColumns(self, arg):
        param=[]
        col1,operator,col2 = self.parser.parse_param_comparecolumns(arg)
        rows = self.dsvalidation.apply(self.__compare_column__, col1=col1,col2=col2,operator=operator, axis=1)
        
        param.append({'column':  "'" + "{0} {1} {2}".format(col1,operator,col2) + "'", 'rows': rows})

        retparam = {
            'status': rows.sum() == self.dsvalidation.shape[0],
            'summary': {
                'key': 'Column Compare - ('+ "{0} {1} {2}".format(col1,operator,col2) + ' )',
                'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])
            },
            'param': param
        }
        return retparam        
 
    '''
    column value length check
    '''
    def ColumnValueLengthCheck(self, arg):
        
        param=[]
        collist,operator,length = self.parser.parse_param_columnvaluelength(arg)
        param = []
        result = [(col, self.dsvalidation[col].apply(self.__column_valuelength__, operator=operator, length=length) )for col in collist]

        for val in result:
            if(val[1].sum() < self.dsvalidation.shape[0]): 
                   param.append({'column': val[0], 'rows': val[1]})
          
        retparam = {'status': len(param) == 0,
                    'summary':
                    {'key': "Column Value Length Check - ", 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
                    'param': param}
        return retparam 
 
    def GroupbyblankCount(self,arg):
        collist, blankcolumn, operator, threshold = self.parser.parse_param_groupbyblankcount(arg)
        dsgroup = pd.DataFrame(( self.dsvalidation[( self.dsvalidation[blankcolumn] == '') | ( self.dsvalidation[blankcolumn].isna()) ][[collist]].groupby([collist]).size() >1).reset_index())

        final = pd.merge( self.dsvalidation, dsgroup, how='left',left_on =[collist], right_on=[collist],copy=False )
        final.loc[(final[0]) & ~((final[blankcolumn] == '') | (final[blankcolumn].isna())),[0] ] = False
        fail = True if final[0].sum() > 0 else False
        final.rename(index=str,columns={0:"1001_T"})

        
        


    def execute_rules(self):
        '''       executes the SchemaValidation rules provided by user in rule config '''
        for configrule in self.parser.rules: 
            ruletoexecute = [rule for rule in RuleDictionary if (
                rule['type'] == 'SchemaValidation' and rule['id'] == configrule['id'])]  
            if(len(ruletoexecute) == 1) :
                   ruletoexecute = ruletoexecute[0]
                   method = ruletoexecute['method']
                   LOGGER.info("Begin : {0} ".format(method))
                   try:
                          retparam = self.__getattribute__(method)(configrule['param'])
                   except:
                          LOGGER.error("Method :  {0}. Exception is : {1}. Rule Id :{2} . Parameters : {3}".format(method,sys.exc_info()[0],configrule['id'],configrule['param']))
                          retparam = {
                                 'status': 0,
                                 'summary': {
                                     'key': 'Rule Execution Error - ',
                                     'value': ''}}
                   LOGGER.info("End : {0} ".format(method))
                   self.__update_summary_detail(retparam, ruletoexecute['id'])
            else:
                   retparam = {
                                 'status': -1,
                                 'summary': {
                                     'key': '',
                                     'value': ''}}
                     
                   self.__update_summary_detail(retparam, configrule['param'])
    '''
    regular expression is passed into formatfailmsg
    '''
    def __check_format__(self, collist, dataformat, formatfailmsg):
        param = []
        result = [(col, self.dsvalidation[col].astype(str).str.contains(dataformat, na=False, regex=True) )for col in collist]

        for val in result:
            if(val[1].sum() < self.dsvalidation.shape[0]): 
                   param.append({'column': val[0], 'rows': val[1]})

        retparam = {'status': len(param) == 0,
                    'summary':
                    {'key': formatfailmsg + " - ", 'value': ", ".join(["{0}({1})".format(val['column'], (False ==val['rows']).sum()) for val in param ])},
                    'param': param}
        return retparam

        
    '''
    Returns the first argument the result of expression evaluation, second argument exception status: exception message in case of exception, True - no exception.
    '''
    def __compare_date__(self,expr):
           try:
                  ret = eval(expr)
           except Exception:
                  return (False,sys.exc_info()[0] )
           return (ret, True)
    
     
    def __check_dateformat(self,colvalue, dateformat):
           try:
                  datetime.datetime.strptime(colvalue, dateformat)
                  ret = True
           except :
                  ret = False
           return ret
                  

    def __compare_daterange__(self,dateformat, colvalue, startdate, enddate):
           try:
                  ret = datetime.datetime.strptime(colvalue, dateformat) >= startdate and datetime.datetime.strptime(colvalue, dateformat) <= enddate
           except Exception:
                  return (False,sys.exc_info()[0] )
           return (ret, True) 
           
 

    '''
    compares columndateorder
    '''   
    def __compare_columndateorder__(self,row, colbefore,colafter, dateformat, operator ):  
          try: 
                 if( operator == ">") :
                        ret = datetime.datetime.strptime(row[colbefore], dateformat) > datetime.datetime.strptime(row[colafter], dateformat) 
                 elif( operator == ">="):
                        ret = datetime.datetime.strptime(row[colbefore], dateformat) >= datetime.datetime.strptime(row[colafter], dateformat) 
                 elif( operator == "<="):
                        ret = datetime.datetime.strptime(row[colbefore], dateformat) <= datetime.datetime.strptime(row[colafter], dateformat) 
                 elif( operator == "<"):
                        ret = datetime.datetime.strptime(row[colbefore], dateformat) < datetime.datetime.strptime(row[colafter], dateformat)        
                 else:
                        ret = datetime.datetime.strptime(row[colbefore], dateformat) == datetime.datetime.strptime(row[colafter], dateformat)        
          except Exception:
                 ret = False
          return ret    


 
    '''
    compare two values
    '''
    def __compare_column__(self,row, col1, col2, operator):
           ret = False
           try: 
                 if( operator == ">") :
                        row[col1] > row[col2]
                        ret = True
                 elif( operator == ">="):
                        row[col1] >= row[col2]
                        ret = True
                 elif( operator == "<="):
                        row[col1] <= row[col2]
                        ret = True
                 elif( operator == "<"):
                        row[col1] < row[col2]
                        ret = True
                 else:
                        row[col1] == row[col2]
                        ret = True
           except Exception:
                 ret = False
           return ret 
           



    def __column_valuelength__(self,x,operator,length):
           ret=False
           try: 
                 if( operator == ">") :
                        str(x) > length
                        ret=True
                 elif( operator == ">="):
                        str(x) >= length
                        ret=True
                 elif( operator == "<="):
                        str(x) <= length
                        ret=True
                 elif( operator == "<"):
                        str(x) < length
                        ret=True
                 else:
                        str(x) == length 
                        ret=True
           except Exception:
                 ret = False
           return ret
    


    '''
    updates validationsummary and validation details datasets
    '''

    def __update_summary_detail(self,retparam, ruleid):
        status = ''
        if(retparam['status'] == 1):
               status = 'Success'
        elif(retparam['status'] == 0):
               status = 'Fail'
        else: 
               status = ''
        self.dsvalidationsummary = self.dsvalidationsummary.append({'status': status,
                                                                    'description': '' if(retparam['status']) == 1 else retparam['summary']['key'] +  
                                                                           retparam['summary']['value']
                                                                    }, ignore_index=True)
                
        if(('param' in retparam) and (retparam['status'] == 0))  :         
               for val in retparam['param']:
                   self.dsvalidationDetails[ruleid + '_' + val['column']] = val['rows'].apply(lambda x: '' if x==True else 'Fail')
        
        
   