import pandas as pd 

def readDF(path, nrows=None):
    extention = path.split('.')[-1]

    if extention == 'csv':
        return pd.read_csv(path, nrows=nrows)
    elif extention == 'xlsx' or extention == 'xls':
        return pd.read_excel(path, nrows=nrows)
    elif extention == 'psv':
        return pd.read_csv(path, sep='|', nrows=nrows)
    return None

