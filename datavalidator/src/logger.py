import logging
from logging.handlers import TimedRotatingFileHandler

# create logger with 'spam_application'
LOGGER = logging.getLogger('data validation')
LOGGER.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = TimedRotatingFileHandler("logs//datavalidation.log", when="midnight")
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
LOGGER.addHandler(fh)
LOGGER.addHandler(ch)