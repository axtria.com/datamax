import sys

import pandas as pd

from src.schemavalidation import SchemaValiation
from src.multiplefilevalidation import MultipleFileValidation
from src.csvparser import CSVParser
from src.logger import LOGGER

def Validate(dsfiles, dsconfig):
    try:
           issuccess = False
           errormessage=''
           LOGGER.info("Begin : CSVParser")
           parser = CSVParser(dsconfig)
           LOGGER.info("End : CSVParser")
           ret = {}
           details = {}
           if(len(dsfiles)==1):
                  param = [(key,val) for key,val in dsfiles.items()][0]
                  name, dsfile = param
                  LOGGER.info("Begin : SchemaValiation")
                  schemaval = SchemaValiation(dsfile, parser)
                  schemaval.execute_rules()
                  LOGGER.info("Begin : SchemaValiation")
                  dsfile = pd.merge(dsfile,schemaval.dsvalidationDetails,left_index=True,right_index=True)
                  dsconfig = pd.merge(dsconfig,schemaval.dsvalidationsummary,left_index=True,right_index=True)
                  details[name]= dsfile
                  issuccess = (schemaval.dsvalidationsummary['status'] == 'Fail').sum() == 0
           else:
                  LOGGER.info("Begin : MultipleFileValidation")
                  multifileval = MultipleFileValidation(dsfiles, parser)
                  multifileval.execute_rules()
                  LOGGER.info("Begin : MultipleFileValidation")
                  details = multifileval.dsvalidationDetails 
                  dsconfig = pd.merge(dsconfig,multifileval.dsvalidationsummary,left_index=True,right_index=True) 
                  issuccess = (multifileval.dsvalidationsummary['status'] == 'Fail').sum() == 0
    except :
           issuccess = False
           name, message, trace = sys.exc_info()
           errormessage = str(name.__name__) + " : "+ str(message )
           LOGGER.error(errormessage + " : " + str(trace) )
    ret = {'summary':dsconfig,'details':details,'ruleexecutionstatus':issuccess,'errormessage':errormessage }
    return ret
    


