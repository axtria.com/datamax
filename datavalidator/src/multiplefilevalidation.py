import sys
import datetime

import re
import numpy as np
import pandas as pd

from src.logger import LOGGER
from src.ruledictionary import RuleDictionary


'''
    Multiple File Validation
    Input is the dataset
'''

class MultipleFileValidation:
    '''
    initialize the class. dsfile is data file e.g. Account.csv, dsuserconfig is rules provided by user in csv file
    '''
    def __init__(self, dsfiles, parser):
        self.dsvalidations = dsfiles
        self.parser = parser
        self.dsvalidationsummary = pd.DataFrame(
            columns=['status', 'description'])
        self.dsvalidationDetails = {}
    
    def __compare_files_by_uniquekey__(self,oldData, newData, key,cols=None) :
           oldData = oldData.set_index(key)[cols] if cols else oldData.set_index(key)
           newData = newData.set_index(key)[cols] if cols else newData.set_index(key)
           oldDataIntColumns = [i for i in oldData.columns if oldData[i].dtype == 'int64']
           newDataIntColumns = [i for i in newData.columns if newData[i].dtype == 'int64']
       
           for col in oldDataIntColumns:
               oldData[col] = oldData[col].astype('float64')
               
           for col in newDataIntColumns:
               newData[col] = newData[col].astype('float64')     
       
           oldDataIndices =  set(oldData.index)
           newDataIndices =  set(newData.index)
           
           deleted  = oldData.loc[list(oldDataIndices - newDataIndices)].reset_index()
           inserted = newData.loc[list(newDataIndices - oldDataIndices)].reset_index()
       
           commonIndices = list(oldDataIndices & newDataIndices)
           
       
           
           oldData = oldData.loc[commonIndices].astype(str).replace('nan','<BLANK>')
           newData = newData.loc[commonIndices].astype(str).replace('nan','<BLANK>')
           
           mask = oldData != newData
       
           edited = oldData[mask].dropna(how='all') +' --> '+ newData[mask].dropna(how='all')
           edited = edited.reset_index().dropna(axis=1, how='all')
       
           return {'deleted':deleted, 'inserted':inserted, 'edited':edited}

    def CompareFiles(self,args):
        '''file comparision'''
        validationerrormsg = ''
        retparam = {}
        param=[]
        lstparam = self.parser.parse_param_filecompare(args)
        files = lstparam[0]
        compositecollist = lstparam[1]
        if len(lstparam) == 3 :
               columnstocompare = lstparam[2] 
        else:
               columnstocompare = list(set(self.dsvalidations[files[0]].columns).difference(set(compositecollist)))  
        
        if(len(set(columnstocompare).difference(set(self.dsvalidations[files[1]].columns))) != 0 ):
               validationerrormsg = 'columns to compare does not match'
        elif ((self.dsvalidations[files[0]].duplicated(subset=compositecollist).sum() > 0 )
               or (self.dsvalidations[files[1]].duplicated(subset=compositecollist).sum() > 0 )
               ):
               validationerrormsg = 'Unique Key does not have unique values'
        if(len(validationerrormsg )!=0):
               raise(ValueError(validationerrormsg))
               
        result = self.__compare_files_by_uniquekey__(self.dsvalidations[files[0]],self.dsvalidations[files[1]],compositecollist, columnstocompare )
        result['inserted']['difference'] = 'insert' 
        result['deleted']['difference']='delete'
        result['edited']['difference']='edit'
        if(result['inserted'].shape[0] > 0 ):
               param.append({'column':"_".join(files) + "_inserted", 'rows':result['inserted']})
        if(result['deleted'].shape[0] > 0):
               param.append({'column':"_".join(files) + "_deleted", 'rows':result['deleted']})
        if(result['edited'].shape[0] > 0):
               param.append({'column':"_".join(files) + "_edited", 'rows':result['edited']})
        
        retparam = {'status': len(param) == 0,
                    'summary':
                    {'key': "Multiple file comparision - ", 'value':  'Insert({0}), Delete({1}), Edit({2})'.format(result['inserted'].shape[0], result['deleted'].shape[0], result['edited'].shape[0])},
                    'param': param}
        return retparam     

    def CheckReference(self,args):
        '''Check master and mapping (child) reference exist'''
        retparam = {}
        param=[]
        files,file1_cols,file2_cols = self.parser.parse_param_filecompare(args)
        dsfile1 = self.dsvalidations[files[0]]
        dsfile2 = self.dsvalidations[files[1]]
        '''returns False if the column combination is not  present in the master else returns True'''
        mapping = dsfile1[file1_cols].astype(str).sum(axis=1)
        master = dsfile2[file2_cols].astype(str).sum(axis=1)
        
        rows = dsfile1.ix[ mapping.isin(master)==False]
        if(rows.shape[0] > 0):
               param.append({'column':"_".join(files) , 'rows':rows})    
        
        retparam = {'status': len(param) == 0,
                    'summary':
                    {'key': "Reference Check - ", 'value': "({0})".format(str(rows.shape[0]))},
                    'param': param}
        return retparam

    def execute_rules(self):
        '''executes the MultipleFileValidation rules provided by user in rule config'''
        for configrule in self.parser.rules:
            ruletoexecute = [rule for rule in RuleDictionary if (
                rule['type'] == 'MultipleFileValidation' and rule['id'] == configrule['id'])]
            if(len(ruletoexecute) == 1) :
                   ruletoexecute = ruletoexecute[0]
                   method = ruletoexecute['method']
                   LOGGER.info("Begin : {0} ".format(method))
                   try:
                          retparam = self.__getattribute__(method)(configrule['param'])
                   except ValueError as e:
                          LOGGER.error("Method :  {0}. Exception is : {1}. Rule Id :{2} . Parameters : {3}".format(method,str(e),configrule['id'],configrule['param']))
                          retparam = {
                                 'status': 0,
                                 'summary': {
                                     'key': str(e),
                                     'value': ''}}   
                   except:
                          LOGGER.error("Method :  {0}. Exception is : {1}. Rule Id :{2} . Parameters : {3}".format(method, sys.exc_info()[1],configrule['id'],configrule['param']))
                          retparam = {
                                 'status': 0,
                                 'summary': {
                                     'key': 'Rule Execution Error - ',
                                     'value': ''}}
                   LOGGER.info("End : {0} ".format(method))
                   self.__update_summary_detail(retparam, ruletoexecute['id'])
            else:
                   retparam = {
                                 'status': -1,
                                 'summary': {
                                     'key': '',
                                     'value': ''}}
                     
                   self.__update_summary_detail(retparam, configrule['id'])
    

    def __update_summary_detail(self,retparam, ruleid):
        '''updates validationsummary and validation details datasets''' 
        status = ''
        if(retparam['status'] == 1):
               status = 'Success'
        elif(retparam['status'] == 0):
               status = 'Fail'
        else: 
               status = ''
        self.dsvalidationsummary = self.dsvalidationsummary.append({'status': status,
                                                                    'description': '' if(retparam['status']) == 1 else retparam['summary']['key'] +  
                                                                           retparam['summary']['value']
                                                                    }, ignore_index=True)                               
        if(('param' in retparam) and (retparam['status'] == 0))  :         
               for val in retparam['param']:  
                   self.dsvalidationDetails[ruleid + '_' + val['column']] = val['rows']
        
        
                 