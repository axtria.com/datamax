-- ----------------------BEGIN : get_layers_v2 ------------------------------------------------------
delete from system_config.api_exchange_instnc where api_id in (select id from system_config.api_exchange_meta where name = 'get_layers_v2' and lower(status) = 'published' );
delete from system_config.api_exchange_meta where name = 'get_layers_v2' and lower(status) = 'published' ;

INSERT INTO system_config.api_exchange_meta (name,version,description,status,category,created_on) 
VALUES
  ('get_layers_v2',3,'to list the layers','published','administration',now());

INSERT INTO system_config.api_exchange_instnc(api_id,query,connector_id,sequence)

VALUES

  ((select id from system_config.api_exchange_meta where name = 'get_layers_v2' and lower(status) = 'published' limit 1),'SELECT l.DATA_LYR_ID AS layer_id, l.DATA_LYR_NAME as name, l.LYR_TYPE AS layer_type, l.description, l.s3_path, l.schema_name, l.CONN_ID AS connector_id, l.IS_ACTV AS is_active, u1.first_name || '' '' || u1.last_name as created_by, l.AUDIT_INSRT_DT AS created_date, u2.first_name || '' '' || u2.last_name as modified_by, l.AUDIT_UPDT_DT AS modified_date FROM system_config.data_layer_meta l LEFT JOIN system_config.user_meta u1 on u1.user_id = l.AUDIT_INSRT_ID LEFT JOIN system_config.user_meta u2 on u2.user_id = l.AUDIT_UPDT_ID WHERE l.is_actv = true and (NULLIF(%(layer_id)s,''null'') IS NULL OR CAST(NULLIF(%(layer_id)s,''null'') AS int) = l.DATA_LYR_ID) AND (NULLIF(%(name_v)s,''null'') IS NULL OR NULLIF(%(name_v)s,''null'') = l.DATA_LYR_NAME) AND (NULLIF(%(type)s,''null'') IS NULL OR NULLIF(%(type)s,''null'') = l.LYR_TYPE) AND (NULLIF(%(is_active)s,''null'') IS NULL OR CAST(NULLIF(%(is_active)s,''null'') AS boolean) = l.IS_ACTV) LIMIT CAST(coalesce( NULLIF(%(limit)s,''null''),''100'') AS int);',1,1);
-- ----------------------END : get_layers_v2 ------------------------------------------------------