-- ----------------------BEGIN : update_connection_sftp_v2 ------------------------------------------------------
delete from system_config.api_exchange_instnc where api_id in (select id from system_config.api_exchange_meta where name = 'update_connection_sftp_v2' and lower(status) = 'published' );
delete from system_config.api_exchange_meta where name = 'update_connection_sftp_v2' and lower(status) = 'published' ;

INSERT INTO system_config.api_exchange_meta (name,version,description,status,category,created_on) 
VALUES
  ('update_connection_sftp_v2',3,'Update sftp connection into the system','published','administration',now());

INSERT INTO system_config.api_exchange_instnc(api_id,query,connector_id,sequence)

VALUES

  ((select id from system_config.api_exchange_meta where name = 'update_connection_sftp_v2' and lower(status) = 'published' limit 1),'update system_config.connection_meta set name = %(c_name)s, description = %(description)s, connection = cast(concat(''{"USERNAME": "'', %(username)s, ''", "HOST": "'', %(host)s, ''", "PASSWORD": "'', %(password)s, ''", "PORT": "'', %(port)s, ''"}'') as json), is_active = ''true'', connection_type = ''sftp'',created_by = cast(%(loggedin_userid)s as int),creation_date = now(),modified_by = cast(%(loggedin_userid)s as int),modification_date = now() where is_active = true and id = %(connector_id)s',1,1);
-- ----------------------END : update_connection_sftp_v2 ------------------------------------------------------