-- ----------------------BEGIN : add_connection_snowflake_v2 ------------------------------------------------------
delete from system_config.api_exchange_instnc where api_id in (select id from system_config.api_exchange_meta where name = 'add_connection_snowflake_v2' and lower(status) = 'published' );
delete from system_config.api_exchange_meta where name = 'add_connection_snowflake_v2' and lower(status) = 'published' ;

INSERT INTO system_config.api_exchange_meta (name,version,description,status,category,created_on) 
VALUES
  ('add_connection_snowflake_v2',3,'Add snowflake connection into the system','published','administration',now());

INSERT INTO system_config.api_exchange_instnc(api_id,query,connector_id,sequence)

VALUES

  ((select id from system_config.api_exchange_meta where name = 'add_connection_snowflake_v2' and lower(status) = 'published' limit 1),'insert into system_config.connection_meta (id, name, description, connection, is_active, connection_type,created_by,creation_date,modified_by,modification_date) values( nextval(''system_config.connection_meta_id'') , %(c_name)s, %(description)s, cast(concat(''{"USER": "'', %(user)s, ''", "ACCOUNT_ID": "'', %(account_id)s, ''", "PASSWORD": "'', %(password)s, ''", "DATABASE": "'', %(database)s, ''" ,"WAREHOUSE": "'', %(warehouse)s, ''"}'') as json), ''true'', ''sf'',cast(%(loggedin_userid)s as int),now(),cast(%(loggedin_userid)s as int),now())',1,1);
-- ----------------------END : add_connection_snowflake_v2 ------------------------------------------------------