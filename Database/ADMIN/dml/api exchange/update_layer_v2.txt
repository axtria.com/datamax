-- ----------------------BEGIN : update_layer_v2 ------------------------------------------------------
delete from system_config.api_exchange_instnc where api_id in (select id from system_config.api_exchange_meta where name = 'update_layer_v2' and lower(status) = 'published' );
delete from system_config.api_exchange_meta where name = 'update_layer_v2' and lower(status) = 'published' ;

INSERT INTO system_config.api_exchange_meta (name,version,description,status,category,created_on) 
VALUES
  ('update_layer_v2',5,'to update layer data','published','administration',now());

INSERT INTO system_config.api_exchange_instnc(api_id,query,connector_id,sequence)

VALUES

  ((select id from system_config.api_exchange_meta where name = 'update_layer_v2' and lower(status) = 'published' limit 1),'update system_config.data_layer_meta set eff_end_dt = now()::date, audit_updt_dt = now() where data_lyr_id = cast(%(DATA_LYR_ID)s as int) and is_actv = true;
insert into system_config.data_layer_meta (DATA_LYR_ID,CONN_ID,TENANT_ID,DATA_LYR_NAME,LYR_TYPE,LYR_LOGICAL_TYPE,DESCRIPTION,S3_PATH,SCHEMA_NAME, AUDIT_INSRT_ID,IS_LYR_ACTV) select DATA_LYR_ID,CONN_ID,TENANT_ID,DATA_LYR_NAME,LYR_TYPE,LYR_LOGICAL_TYPE,DESCRIPTION,S3_PATH,SCHEMA_NAME, cast(%(loggedin_userid)s as int),IS_LYR_ACTV from system_config.data_layer_meta where data_lyr_id = cast(%(DATA_LYR_ID)s as int) and is_actv = true;
update system_config.data_layer_meta set is_actv = false,IS_LYR_ACTV = false  where  data_lyr_id = cast(%(DATA_LYR_ID)s as int) and is_actv = true and eff_end_dt <> ''2099-12-31''::date ;
update system_config.data_layer_meta set DESCRIPTION = case when nullif(%(DESCRIPTION)s ,''null'') is null then DESCRIPTION else %(DESCRIPTION)s end, IS_ACTV= case when nullif(%(IS_ACTV)s ,''null'') is null then IS_ACTV else cast(nullif(%(IS_ACTV)s ,''null'') as boolean) end, AUDIT_UPDT_ID = cast(%(loggedin_userid)s as int), AUDIT_UPDT_DT = now() where is_actv = true and cast(%(DATA_LYR_ID)s as int) = DATA_LYR_ID and %(IS_ACTV)s in (''null'',''true'',''false'') and 0 < ( select count(1) from system_config.user_meta u inner join system_config.usergrp_user ugp on ugp.user_id = u.user_id and u.user_id = cast(%(loggedin_userid)s as int) inner join system_config.usergrp_mdl_fnctnlty f on f.usergrp_id = ugp.usergrp_id inner join system_config.module_fnctnlty fm on fm.MODULE_FNCTNLTY_ID = f.MODULE_FNCTNLTY_ID and fm.FUNCTIONALITY_NAME = ''ADMN.CREATE_LAYER'' limit 1);
select l.DATA_LYR_ID, l.data_lyr_name, l.LYR_TYPE, l.DESCRIPTION,l.S3_PATH,l.SCHEMA_NAME,l.CONN_ID,l.IS_ACTV, u1.first_name || '' '' || u1.last_name as AUDIT_INSRT_ID,l.AUDIT_INSRT_DT, u2.first_name || '' '' || u2.last_name as AUDIT_UPDT_ID, l.AUDIT_UPDT_DT from system_config.data_layer_meta l left join system_config.user_meta u1 on u1.user_id = l.AUDIT_INSRT_ID left join system_config.user_meta u2 on u2.user_id = l.AUDIT_UPDT_ID where  (NULLIF(%(DATA_LYR_ID)s,''null'') is null or cast(NULLIF(%(DATA_LYR_ID)s,''null'') as int) = l.DATA_LYR_ID) and l.is_actv = true and l.is_lyr_actv = true;',1,1);
-- ----------------------END : update_layer_v2 ------------------------------------------------------