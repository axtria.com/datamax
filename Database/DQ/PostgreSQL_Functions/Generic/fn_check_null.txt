CREATE OR REPLACE FUNCTION system_config.fn_check_null(s text)
  RETURNS boolean
  LANGUAGE plpgsql
AS
$body$
--Creator Name : Shivam
--Creation Date : 17-Aug-2018
--Descritpion : Check in a | separated string if there is any null value
declare 
	out_val Boolean := False;
    rec record;
begin
	
	for rec in select regexp_split_to_table(s, '\|') as str
    loop
		if rec.str::text = '' then
			out_val := True;
		end if;
    end loop;
	return out_val;
end;
$body$
  VOLATILE
  COST 100;

COMMIT;
