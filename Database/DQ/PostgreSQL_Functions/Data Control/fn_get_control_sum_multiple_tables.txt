CREATE OR REPLACE FUNCTION system_config.fn_get_control_sum_multiple_tables(in_ruleid character varying, jobid character varying)
  RETURNS json
  LANGUAGE plpgsql
AS
$body$
----------------------------------------------------------------------------------------------------------
--CreatorName:- Umesh Jain
--CreationDate:- 05-Sep-2018
--Description:- To generate query for Control Sum query to sum up the single attribute for more than 1 table and compared with 1 column in target table.
--              in_rule_id : rule id, jobid : jobid
--              in_ruleid: '2', jobid : '2' ==> returns query which contains a text to
--              1/ insert record in postgreSQL   : dq_job_run (dq_run_id, dq_job_id, rule_id, rule_desc, obj_nm, run_start_date, run_end_date, rule_status)
--              2/ insert records in Snowflake    : dq_ctrl_tot_output (rule_id, dq_run_id, src_attr_nm, src_attr_val, src_bk_attr_nm, src_bk_attr_val, tgt_attr_nm, tgt_attr_val, tgt_bk_attr_nm, tgt_bk_attr_val, ct_rslt)
--              3/ delete record in Snowflake     : dq_job_run (dq_run_id, dq_job_id, rule_id, rule_desc, obj_nm, run_start_date, run_end_date, rule_status, fail_rcd_cnt, tot_rcd_cnt)
--              4/ insert record in Snowflake     : dq_job_run (dq_run_id, dq_job_id, rule_id, rule_desc, obj_nm, run_start_date, run_end_date, rule_status, fail_rcd_cnt, tot_rcd_cnt)

-----------------------------------------------------------------------------------------------------------
declare
                out_val text;
                ruleid text = in_ruleid::text;
                dq_job_run_before_query varchar(4000);
                ctrl_tot_output_insert_query varchar(4000);
                dq_job_run_after_query varchar(4000);
                PARM_RULE_DESC text;
                OBJECT_NAME text;
                runid integer;    
                rule_start_date text;
                temp text;
                calc_src_bk_col_names text; 
var_json_obj json;
rule_svrt text;
var_snapshot_data varchar;
db_schema_nm varchar(100);

begin

	select (connection->>'DATABASE') ||'.'|| (connection->>'SCHEMA') as db_schema_nm into db_schema_nm from system_config.connection_meta 
	where name = 'DQ Snowflake Conn' and is_active = true;
  
  select parm.rule_desc ,system_config.fn_get_attr(parm.tgt_obj_id::text, 'objname') into PARM_RULE_DESC, OBJECT_NAME from (select tgt_obj_id,rule_desc from system_config.dq_rule_ctrl_tot_parm where rule_id = in_ruleid and is_actv = true and is_rule_actv = true group by tgt_obj_id,rule_desc) parm limit 1;
  
  select distinct a.rule_svrt into rule_svrt from system_config.dq_rule_ctrl_tot_parm a where rule_id = in_ruleid and is_actv = true and is_rule_actv = true limit 1;
  
  insert into system_config.dq_job_run(dq_job_id,rule_id,rule_desc,obj_nm,run_start_date) values (jobid,in_ruleid,PARM_RULE_DESC,OBJECT_NAME, now());
                
  select max(dq_run_id) into runid from system_config.dq_job_run where dq_job_id = jobid and rule_id = in_ruleid;
  select run_start_date into rule_start_date from system_config.dq_job_run where rule_id = in_ruleid and dq_job_id = jobid and dq_run_id = runid;  

  temp := '';
  temp := 'insert into %8$s.dq_job_run(rule_id, run_start_date,dq_job_id,obj_nm,rule_desc,dq_run_id,run_status,rule_svrt) '
          ' select ''%1$s'', TIMESTAMP ''%7$s'',''%2$s'',''%3$s'',''%4$s'', %5$s, ''Started'', ''%6$s''';
                                  
  select FORMAT(temp, ruleid,jobid,OBJECT_NAME, PARM_RULE_DESC, runid,rule_svrt,rule_start_date,db_schema_nm ) into dq_job_run_before_query;

select distinct upper(src_bk_attr_id) as src_bk_attr_id into calc_src_bk_col_names from system_config.dq_rule_ctrl_tot_parm where rule_id = nullif(ruleid,'') and is_actv = true and is_rule_actv = true;


  temp := '';  
  temp := 'insert into %11$s.dq_ctrl_tot_output(rule_id,dq_run_id,src_attr_nm,src_attr_val,src_bk_attr_nm,src_bk_attr_val,tgt_attr_nm,tgt_attr_val,tgt_bk_attr_nm,tgt_bk_attr_val,ct_rslt)'
   ' select ''%1$s'' as rule_id, %6$s as dq_run_id,'
   '''%5$s'' as src_attr_nm,nvl(src_attr_val::text,''0'') as src_attr_val,case when ''%10$s'' = ''NONE'' then ''NONE'' else ''%9$s'' end as src_bk_attr_nm, case when src_bk_attr_val = ''1'' then ''NONE'' else nvl(src_bk_attr_val::text,''NOT PRESENT'') end as src_bk_attr_val,''%4$s'' as tgt_attr_nm,nvl(tgt_attr_val::text,''0'') as tgt_attr_val, case when ''%8$s'' = ''1'' then ''NONE'' else ''%8$s'' end as tgt_bk_attr_nm, case when tgt_bk_attr_val = ''1'' then ''NONE'' else nvl(tgt_bk_attr_val::text,''NOT PRESENT'') end as tgt_bk_attr_val,'
   'case when src_attr_val = tgt_attr_val then ''Pass'' else ''Fail'' end as ct_rslt'
   ' from'
   '(select src_attr_val,src_bk_attr_val,tgt_attr_val, tgt_bk_attr_val from (select  src_bk_attr_val, sum(src_attr_val) as src_attr_val from ( %2$s ) tab group by src_bk_attr_val ) lhs'
   ' full outer join (select  %7$s as tgt_bk_attr_val, %3$s group by %7$s) rhs'
   ' on lhs.src_bk_attr_val = rhs.tgt_bk_attr_val)';
            
select FORMAT(temp,  ruleid ,  dyn_src_sql, dyn_tgt_sql,tgt_attr_nm,src_attr_nm,runid,tgt_bk_grp_val,tgt_bk_col_names,src_bk_attr_nm,
               calc_src_bk_col_names,db_schema_nm
              )
   into ctrl_tot_output_insert_query from (
select dyn_src_sql,dyn_tgt_sql,tgt_attr_nm,src_attr_nm,tgt_bk_grp_val,tgt_bk_col_names,src_bk_attr_nm from (
select rule_id, string_agg(src_attr_nm, '|') as src_attr_nm,string_agg(src_bk_col_names,'|') as src_bk_attr_nm,
string_agg('(select ' ||src_bk_grp_val||' as src_bk_attr_val, '|| aggcol || ' as src_attr_val from ' || src_tbl_name ||' where '||src_fltr_con||' group by '||src_bk_grp_val|| ')', ' union all ') dyn_src_sql
from(
select rule_id, 'SUM(cast('|| system_config.fn_get_attr(src_attr_id::text,'name') || ' as numeric(20,3)))' aggcol,
system_config.fn_get_attr(src_obj_id::text,'objname') as src_tbl_name,src_fltr_con,system_config.fn_get_attr(src_attr_id::text,'name') src_attr_nm,
case when upper(src_bk_attr_id) = 'NONE' then '1' else system_config.fn_get_attr(src_bk_attr_id, 'bknames') end as src_bk_grp_val,case when upper(src_bk_attr_id) = 'NONE' then '1' else system_config.fn_get_attr(src_bk_attr_id, 'bkcolnames') end as src_bk_col_names
from system_config.dq_rule_ctrl_tot_parm where rule_id = nullif(ruleid,'') and is_actv = true and is_rule_actv = true
)x group by rule_id) lhs
inner join (
select distinct rule_id,system_config.fn_get_attr(tgt_attr_id::text,'name') as tgt_attr_nm, case when upper(tgt_bk_attr_id) = 'NONE' then '1' else system_config.fn_get_attr(tgt_bk_attr_id, 'bknames') end as tgt_bk_grp_val,case when upper(tgt_bk_attr_id) = 'NONE' then '1' else system_config.fn_get_attr(tgt_bk_attr_id, 'bkcolnames') end as tgt_bk_col_names,
'SUM(cast('||system_config.fn_get_attr(tgt_attr_id::text,'name')||' as numeric(20,3)))'||' as tgt_attr_val from '|| system_config.fn_get_attr(tgt_obj_id::text,'objname')||' where '||tgt_fltr_con as dyn_tgt_sql from system_config.dq_rule_ctrl_tot_parm where rule_id = nullif(ruleid,'') and is_actv = true and is_rule_actv = true) rhs
on lhs.rule_id = rhs.rule_id
) tab;

  temp := '';
  temp = 'delete from %8$s.dq_job_run where rule_id = ''%1$s'' and dq_job_id = ''%2$s'' and dq_run_id = %3$s and run_status = ''Started'' and run_end_date is null;'
         'insert into %8$s.dq_job_run(dq_run_id,dq_job_id,rule_id,rule_desc,obj_nm,run_start_date,run_end_date,run_status,fail_rcd_cnt,tot_rcd_cnt, rule_svrt)'
         ' select %3$s, ''%2$s'', ''%1$s'', ''%4$s'', ''%5$s'', TIMESTAMP ''%6$s'', current_date(), case when ct_rslt= ''FAIL'' then ''Fail'' else ''Pass'' end as run_status, fail_rcd_cnt as fail_rcd_cnt, tot_rcd_cnt as tot_rcd_cnt,''%7$s'' '
         ' from (with rcd_cnt as (select lhs.rule_id, lhs.dq_run_id, tot_rcd_cnt, CASE WHEN fail_rcd_cnt IS NULL THEN 0 ELSE fail_rcd_cnt END AS fail_rcd_cnt from (SELECT rule_id,dq_run_id,COUNT(1) tot_rcd_cnt'
                                     ' FROM %8$s.dq_ctrl_tot_output WHERE rule_id = ''%1$s'' AND   dq_run_id = %3$s GROUP BY rule_id,dq_run_id) lhs left outer join (SELECT rule_id,dq_run_id,COUNT(1) fail_rcd_cnt'
                                     ' FROM %8$s.dq_ctrl_tot_output WHERE UPPER(ct_rslt) = ''FAIL'' AND   rule_id = ''%1$s'' AND   dq_run_id = %3$s GROUP BY rule_id,dq_run_id) rhs'
                                     ' on lhs.rule_id = rhs.rule_id and lhs.dq_run_id = rhs.dq_run_id),'
                                     'rule_status as (select rule_id,dq_run_id,upper(ct_rslt) ct_rslt from ('
                                     'select rule_id,dq_run_id,ct_rslt, row_number() over (partition by rule_id,dq_run_id order by  ct_rslt ) as row_n from %8$s.dq_ctrl_tot_output where rule_id = ''%1$s'' and dq_run_id = %3$s) tab where row_n=1)'
                                     'select fail_rcd_cnt, tot_rcd_cnt, ct_rslt from rcd_cnt rc inner join rule_status rs on rc.rule_id = rs.rule_id and rc.dq_run_id = rs.dq_run_id) x;'
                                     'select * from %8$s.dq_job_run where rule_id = ''%1$s'' and dq_job_id = ''%2$s'' and dq_run_id = %3$s';
                                
   select FORMAT(temp, ruleid, jobid, runid , PARM_RULE_DESC, OBJECT_NAME,rule_start_date,rule_svrt,db_schema_nm) into dq_job_run_after_query;

   select FORMAT('%1$s; %2$s; %3$s;',dq_job_run_before_query, ctrl_tot_output_insert_query,dq_job_run_after_query) into out_val;
				 
 	 select	system_config.fn_get_snapshot_query(in_ruleid, jobid ,'DC', runid) into var_snapshot_data;
    var_json_obj := json_build_object('query',out_val, 'run_id', runid, 'rule_id',in_ruleid, 'jobid', jobid, 'snapshot', var_snapshot_data); 
    return var_json_obj;

end;
$body$
  VOLATILE
  COST 100;

COMMIT;
