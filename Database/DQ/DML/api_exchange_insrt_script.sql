insert into api_exchange
select nextval('api_id_seq'), 'executejob',1,'To execute DQ job, input parameter is jobid','published','validation',now();

insert into api_exchange_meta
select nextval('api_exchange_meta_id_seq'), (select id from api_exchange where description = 'To execute DQ job, input parameter is jobid' and name = 'executejob'
and version = (select max(version) from api_exchange where description = 'To execute DQ job, input parameter is jobid' and name = 'executejob' )) as api_id
,'SELECT Query FROM (
SELECT CASE
	   WHEN pg_fn_nm = ''fn_get_ri_query'' THEN system_config.fn_get_ri_query(rule_id,%(jobid)s)
	   WHEN pg_fn_nm = ''fn_get_kpi_query'' THEN system_config.fn_get_kpi_query(rule_id,%(jobid)s)
	   WHEN pg_fn_nm = ''fn_get_data_ctrl_sum_query'' THEN system_config.fn_get_data_ctrl_sum_query(rule_id,%(jobid)s)
	   WHEN pg_fn_nm = ''fn_get_data_ctrl_tot_query'' THEN system_config.fn_get_data_ctrl_tot_query(rule_id,%(jobid)s)
	   WHEN pg_fn_nm = ''fn_get_th_query_variation'' THEN system_config.fn_get_th_query_variation(rule_id,%(jobid)s)
	   WHEN pg_fn_nm = ''fn_get_th_query_trend'' THEN system_config.fn_get_th_query_trend(rule_id,%(jobid)s)
	   end as Query 
from (
SELECT cfg.rule_id,mstr_rule_id
FROM (SELECT *
  FROM system_config.dq_rule_job_map
  WHERE is_actv = true AND dq_job_id = %(jobid)s) cfg
INNER JOIN (SELECT *
		  FROM system_config.dq_job_config
		  WHERE is_actv = true and is_job_actv = true
		  AND   dq_job_id = %(jobid)s) job ON job.dq_job_id = cfg.dq_job_id
INNER JOIN (SELECT distinct rule_id,
   mstr_rule_id FROM system_config.dq_rule_ctrl_tot_parm WHERE is_actv = true AND is_rule_actv = true) ct_parm 
   ON ct_parm.rule_id = cfg.rule_id
UNION ALL
SELECT cfg.rule_id,mstr_rule_id
FROM (SELECT *
  FROM system_config.dq_rule_job_map
  WHERE is_actv = true AND dq_job_id = %(jobid)s) cfg
INNER JOIN (SELECT *
		  FROM system_config.dq_job_config
		  WHERE is_actv = true and is_job_actv = true
		  AND   dq_job_id = %(jobid)s) job ON job.dq_job_id = cfg.dq_job_id  
INNER JOIN (SELECT distinct rule_id,
   mstr_rule_id FROM system_config.dq_rule_kpi_parm WHERE is_actv = true AND is_rule_actv = true) kpi_parm 
   ON kpi_parm.rule_id = cfg.rule_id
UNION ALL
SELECT cfg.rule_id,mstr_rule_id
FROM (SELECT *
  FROM system_config.dq_rule_job_map
  WHERE is_actv = true AND dq_job_id = %(jobid)s) cfg
INNER JOIN (SELECT *
		  FROM system_config.dq_job_config
		  WHERE is_actv = true and is_job_actv = true
		  AND   dq_job_id = %(jobid)s) job ON job.dq_job_id = cfg.dq_job_id       
INNER JOIN (SELECT distinct rule_id,
   mstr_rule_id FROM system_config.dq_rule_ri_parm WHERE is_actv = true AND is_rule_actv = true) ri_parm 
   ON ri_parm.rule_id = cfg.rule_id
UNION ALL
SELECT cfg.rule_id,mstr_rule_id
FROM (SELECT *
  FROM system_config.dq_rule_job_map
  WHERE is_actv = true AND dq_job_id = %(jobid)s) cfg
INNER JOIN (SELECT *
		  FROM system_config.dq_job_config
		  WHERE is_actv = true and is_job_actv = true
		  AND   dq_job_id = %(jobid)s) job ON job.dq_job_id = cfg.dq_job_id       
INNER JOIN (SELECT distinct rule_id,
   mstr_rule_id FROM system_config.dq_rule_thrshld_parm WHERE is_actv = true AND is_rule_actv = true) th_parm 
   ON th_parm.rule_id = cfg.rule_id) rules
INNER JOIN (select * from system_config.mstr_dq_rule where is_actv = true and is_rule_actv = true) mstr
ON rules.mstr_rule_id = mstr.mstr_rule_id
UNION ALL
select  system_config.fn_get_data_validation_wrapper(%(jobid)s) as  Query
) TAB WHERE (query->''query'') IS NOT NULL AND (query->''query'')::text != ''""''',
(select id from system_config.connection_meta where name = 'DQ Snowflake Conn') as connector_id,1 as seq ;

COMMIT;
