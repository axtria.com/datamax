{
    "src_name": "HCOS",
    "object_name": "Business",
    "decription": "The dimension table Business lists all active and deactive healthcare businesses. It contains commonly used data elements about the business.",
    "frequency": "Weekly",
    "attributes": [
        {
            "actual_col_name": "ims_org_id",
            "business_name": "Ims Org Id",
            "description": "The unique alphanumeric identifier for the business.",
            "data_type": "Varchar(11)"
        },
        {
            "actual_col_name": "business_name",
            "business_name": "Business Name",
            "description": "A business_name is the legal name for a business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "dba_name",
            "business_name": "Dba Name",
            "description": "The doing business as name for the business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "care_of_location",
            "business_name": "Care Of Location",
            "description": "Care_of_location names a facility within which the organization is located. The initial implementation of this field will focus on the Retail Walk In Clinic market segment.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "address_id",
            "business_name": "Address Id",
            "description": "The unique identifier for the physical address of the business.",
            "data_type": "Varchar(10)"
        },
        {
            "actual_col_name": "stf_id",
            "business_name": "Stf Id",
            "description": "Attribute for identifying \u201cstreet front addresses\u201d. The field can be used to identify organizations sharing an address 1, yet, potentially have different address ids based on having distinct values for address 2.",
            "data_type": "Varchar(10)"
        },
        {
            "actual_col_name": "physical_addr_1",
            "business_name": "Physical Addr 1",
            "description": "Physical_addr_1 is the first line of the physical street address for a business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "physical_addr_2",
            "business_name": "Physical Addr 2",
            "description": "Physical_addr_2 is the second or supplemental line of the physical street address for a business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "physical_city",
            "business_name": "Physical City",
            "description": "Physical_city is the name of the city in which a business is physically located.",
            "data_type": "Varchar(30)"
        },
        {
            "actual_col_name": "physical_state",
            "business_name": "Physical State",
            "description": "Physical_state is the abbreviation for the state in which a business is physically located.",
            "data_type": "Varchar(2)"
        },
        {
            "actual_col_name": "physical_zip",
            "business_name": "Physical Zip",
            "description": "Physical_zip is the postal code for where a business is physically located.",
            "data_type": "Varchar(15)"
        },
        {
            "actual_col_name": "postal_addr_1",
            "business_name": "Postal Addr 1",
            "description": "Postal_addr_1 is the US Postal Service standardized first address line for a business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "postal_addr_2",
            "business_name": "Postal Addr 2",
            "description": "Postal_addr_2 is the US Postal Service standardized second or supplemental address line for a business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "postal_city",
            "business_name": "Postal City",
            "description": "Postal_city is the US Postal Service standardized city name.",
            "data_type": "Varchar(30)"
        },
        {
            "actual_col_name": "postal_state",
            "business_name": "Postal State",
            "description": "Postal_state is the US Postal Service standardized state abbreviation.",
            "data_type": "Varchar(2)"
        },
        {
            "actual_col_name": "postal_zip",
            "business_name": "Postal Zip",
            "description": "Postal_zip is the US Postal Service standardized postal code.",
            "data_type": "Varchar(15)"
        },
        {
            "actual_col_name": "phone",
            "business_name": "Phone",
            "description": "Phone is the main telephone number for a business.",
            "data_type": "Varchar(14)"
        },
        {
            "actual_col_name": "fax",
            "business_name": "Fax",
            "description": "Fax is the main or administration department fax number for a business.",
            "data_type": "Varchar(14)"
        },
        {
            "actual_col_name": "website",
            "business_name": "Website",
            "description": "Website displays the internet address (URL) for a business.",
            "data_type": "Varchar(100)"
        },
        {
            "actual_col_name": "resident_count",
            "business_name": "Resident Count",
            "description": "Resident Count",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "ttl_surgeries",
            "business_name": "Total Surgeries",
            "description": "Total Surgeries",
            "data_type": "Decimal(10,0)"
        },
        {
            "actual_col_name": "owner_status",
            "business_name": "Owner Status",
            "description": "Owner_status is the type of ownership relationship between a facility and its parent. Valid values include: Independent (no corporate parent exists), Not Independent (the business has a corporate parent) and NULL. If this field is NULL, it means that the business cannot have a corporate parent.",
            "data_type": "Varchar(25)"
        },
        {
            "actual_col_name": "profit_status",
            "business_name": "Profit Status",
            "description": "The profit status of the facility. Values include: For Profit, Not For Profit, Government, NULL (If data is unknown or Not Applicable).",
            "data_type": "Varchar(25)"
        },
        {
            "actual_col_name": "cmi",
            "business_name": "Cmi",
            "description": "CMI is the Case Mix Index for a business. This is a government-assigned measure of the complexity of medical and surgical care provided to Medicare inpatients by a hospital under the prospective payment system (PPS). It factors in a hospital\u2019s use of technology for patient care and medical services\u2019 level of acuity required by the patient population.",
            "data_type": "Decimal(6,4)"
        },
        {
            "actual_col_name": "primary_cot_id",
            "business_name": "Primary Cot Id",
            "description": "Primary_cot_id is the numeric code for the primary class of trade.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "cot_classification_id",
            "business_name": "Cot Classification Id",
            "description": "A cot_classification_id is the numeric identifier corresponding to a broad group of class-of-trade business classifications.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "cot_classification",
            "business_name": "Cot Classification",
            "description": "Cot_classification displays the description of a broad grouping of class-of-trade business classifications.",
            "data_type": "Varchar(50)"
        },
        {
            "actual_col_name": "cot_facility_type_id",
            "business_name": "Cot Facility Type Id",
            "description": "A cot_facility_type_id is the numeric identifier corresponding to the class-of-trade business facility type.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "cot_facility_type",
            "business_name": "Cot Facility Type",
            "description": "Cot_facility_type displays the description of a class-of-trade business facility type.",
            "data_type": "Varchar(50)"
        },
        {
            "actual_col_name": "cot_specialty_id",
            "business_name": "Cot Specialty Id",
            "description": "A cot_specialty_id is the numeric identifier corresponding to a class-of-trade business specialty.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "cot_specialty",
            "business_name": "Cot Specialty",
            "description": "Cot_specialty displays the description of the class-of-trade business specialty.",
            "data_type": "Varchar(50)"
        },
        {
            "actual_col_name": "record_type",
            "business_name": "Record Type",
            "description": "A record_type is a numeric indicator for a kind of business. It is used for legacy compatibility with older versions of the HCOS Health Industry Database. Valid values are: 01 \u2013 GPO or Contracting Org, 02 \u2013 Corporate Parent, 03 \u2013 Acute Care Hospital, 04 \u2013 Alternate Site, 05 \u2013 Distributors, 06 \u2013 Academic Medical Schools and 07 \u2013 Alliance.",
            "data_type": "Varchar(2)"
        },
        {
            "actual_col_name": "bed_cluster_id",
            "business_name": "Bed Cluster Id",
            "description": "A bed_cluster_id is the unique numeric identifier for a bed cluster.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "ttl_license_beds",
            "business_name": "Ttl License Beds",
            "description": "Ttl_license_beds displays the total number of licensed beds within a business.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "ttl_census_beds",
            "business_name": "Ttl Census Beds",
            "description": "Ttl_census_beds displays the total number of census beds within a business.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "ttl_staffed_beds",
            "business_name": "Ttl Staffed Beds",
            "description": "Ttl_staffed_beds displays the total number of staffed beds within a business.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "or_surgeries",
            "business_name": "Or Surgeries",
            "description": "Or_surgeries displays the total number of inpatient ICD-9-CM surgical procedures performed at a business, typically in an operating room.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "teaching_hosp",
            "business_name": "Teaching Hosp",
            "description": "Indicator as to whether the facility is affiliated with a teaching program. Y = Yes, N= No, NULL (If data is unknown or Not Applicable).",
            "data_type": "Char(1)"
        },
        {
            "actual_col_name": "resident_program",
            "business_name": "Resident Program",
            "description": "Resident Program",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "all_ddd",
            "business_name": "All Ddd",
            "description": "All_ddd lists the one or more DDD (IMS Outlet) numbers for a business. If a business has more than one DDD number, each number is separated by a carrot ( ^ ). This field facilitates reporting DDD numbers on one line in a report. The DDD number for a business also is available in the Identifier_Fact table.",
            "data_type": "Varchar(150)"
        },
        {
            "actual_col_name": "all_npi",
            "business_name": "All Npi",
            "description": "All_npi lists the one or more NPI (National Provider Identifier) numbers for a business. If a business has more than one NPI identifier,each number is separated by a carrot ( ^ ). This field facilitates reporting NPI identifiers on one line in a report. The NPI identifier for a business also is available in the Identifier_Fact table.",
            "data_type": "Varchar(150)"
        },
        {
            "actual_col_name": "hin",
            "business_name": "Hin",
            "description": "Hin displays the Health Industry Number for a business. This number is the concatenation of each of the HIN components in the following order: Base HIN (7 char), Facility Suffix (2 char), Market (2 char) and Subset (2 char).",
            "data_type": "Varchar(13)"
        },
        {
            "actual_col_name": "dea",
            "business_name": "Dea",
            "description": "Dea displays the Drug Enforcement Administration number for a business.",
            "data_type": "Varchar(9)"
        },
        {
            "actual_col_name": "mpn",
            "business_name": "Mpn",
            "description": "Mpn is the Medicare Provider Number for a business.",
            "data_type": "Varchar(6)"
        },
        {
            "actual_col_name": "mpn_order",
            "business_name": "Mpn Order",
            "description": "Mpn_order displays the order of priority for an MPN for those facilities that share an MPN. Valid values are: P \u2013 the MPN on a business record is the primary identifier for the business and O \u2013 the MPN is a secondary identifier. (Using P for the MPN supports aggregating clinical volumes and avoids double counting).",
            "data_type": "Varchar(1)"
        },
        {
            "actual_col_name": "msa",
            "business_name": "Msa",
            "description": "Msa is the Metropolitan Statistical Area for a business.",
            "data_type": "Varchar(5)"
        },
        {
            "actual_col_name": "fips_state",
            "business_name": "Fips State",
            "description": "Fips_state is the government-assigned Federal Information Processing Standards Publications state code for a business.",
            "data_type": "Varchar(2)"
        },
        {
            "actual_col_name": "fips_county",
            "business_name": "Fips County",
            "description": "Fips_county is the government-assigned Federal Information Processing Standards Publications county code for a business.",
            "data_type": "Varchar(3)"
        },
        {
            "actual_col_name": "num_of_providers",
            "business_name": "Num Of Providers",
            "description": "Num_of_providers displays the total number of distinct providers affiliated with a business.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "corp_parent_ims_org_id",
            "business_name": "Corp Parent Ims Org Id",
            "description": "The ims_org_id of the corporate parent of the business.",
            "data_type": "Varchar(11)"
        },
        {
            "actual_col_name": "corp_parent_name",
            "business_name": "Corp Parent Name",
            "description": "The legal name of the corporate parent of the business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "owner_sub_ims_org_id",
            "business_name": "Owner Sub Ims Org Id",
            "description": "The ims_org_id of the owner subsidiary that owns the business.",
            "data_type": "Varchar(11)"
        },
        {
            "actual_col_name": "owner_sub_name",
            "business_name": "Owner Sub Name",
            "description": "The legal name of the owner subsidiary that owns the business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "gpo_pharma_ims_org_id",
            "business_name": "Gpo Pharma Ims Org Id",
            "description": "The ims_org_id of the primary pharmaceutical GPO for the business.",
            "data_type": "Varchar(11)"
        },
        {
            "actual_col_name": "gpo_pharma_name",
            "business_name": "Gpo Pharma Name",
            "description": "The legal name of the primary pharmaceutical GPO for the business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "gpo_medsurg_ims_org_id",
            "business_name": "Gpo Medsurg Ims Org Id",
            "description": "The ims_org_id of the primary medical/surgical GPO for the business.",
            "data_type": "Varchar(11)"
        },
        {
            "actual_col_name": "gpo_medsurg_name",
            "business_name": "Gpo Medsurg Name",
            "description": "The legal name of the primary medical/surgical GPO for the business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "pharma_prov_ims_org_id",
            "business_name": "Pharma Prov Ims Org Id",
            "description": "The ims_org_id of the pharmacy provider for the business.",
            "data_type": "Varchar(11)"
        },
        {
            "actual_col_name": "pharma_prov_name",
            "business_name": "Pharma Prov Name",
            "description": "The legal name of the pharmacy provider for the business.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "formulary",
            "business_name": "Formulary",
            "description": "The formulary describes the business formulary as one of the following values: Yes-Open, Yes-Closed, Yes-Mixed, Yes-Unknown, Unknown, and No.",
            "data_type": "Varchar(20)"
        },
        {
            "actual_col_name": "electronic_med_rec",
            "business_name": "Electronic Med Rec",
            "description": "Electronic_med_rec tracks whether electronic medical records are mandated by a corporate parent organization or independent hospital. Valid values are Yes, No, Partial, and null.",
            "data_type": "Varchar(20)"
        },
        {
            "actual_col_name": "eprescribe",
            "business_name": "Eprescribe",
            "description": "Eprescribe tracks a prescriber ability to electronically send an accurate, error-free and understandable prescription directly to a pharmacy from the point-of-care. Valid values are Yes, No, Partial and null.",
            "data_type": "Varchar(20)"
        },
        {
            "actual_col_name": "payperform",
            "business_name": "Payperform",
            "description": "Payperform tracks pay for performance (P4P, also known as incentive pay) policies or practices that define criteria by which work is compensated for performance results rather than strictly for time worked. Valid values are Yes, No, Site-determined and null.",
            "data_type": "Varchar(20)"
        },
        {
            "actual_col_name": "genfirst",
            "business_name": "Genfirst",
            "description": "Genfirst tracks generics-first policies or practices that mandate or guide prescribers within healthcare systems to prescribe a generic pharmaceutical product before a brand name when clinically warranted. Valid values are Yes, No, Site-determined and null.",
            "data_type": "Varchar(20)"
        },
        {
            "actual_col_name": "srep_access",
            "business_name": "Srep Access",
            "description": "Identifies whether pharmaceutical sales- representative access is controlled or mandated at the IDN corporate level, is not mandated by corporate, is site driven under corporate or is unknown by authorized sources at the corporate level (or at site level if corporate reports that policy is site-driven). Valid values are Yes,No,Site-Determined, Unknown (the authorized source(s) were not able to answer if there was a corporate- determined policy in place).",
            "data_type": "Varchar(20)"
        },
        {
            "actual_col_name": "deactivation_reason",
            "business_name": "Deactivation Reason",
            "description": "The reason the business was deactivated.",
            "data_type": "Varchar(25)"
        },
        {
            "actual_col_name": "referback_ims_org_id",
            "business_name": "Referback Ims Org Id",
            "description": "The replacement active ims_org_id for a business that was deactivated.",
            "data_type": "Varchar(11)"
        },
        {
            "actual_col_name": "status_indicator",
            "business_name": "Status Indicator",
            "description": "Indicates where the business is active or deactive. A value of A is active. A value of D is deactive.",
            "data_type": "Varchar(1)"
        },
        {
            "actual_col_name": "ttl_procedures",
            "business_name": "Total Procedures",
            "description": "Total Procedures",
            "data_type": "Decimal(10,0)"
        }
    ]
}