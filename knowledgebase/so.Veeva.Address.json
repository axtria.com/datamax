{
    "src_name": "Veeva",
    "object_name": "Address",
    "decription": "HCP,HCO address detail information",
    "frequency": null,
    "attributes": [
        {
            "actual_col_name": "ASSMCA_vod__c",
            "business_name": "ASSMCA",
            "description": "The ASSMCA field stores the equivalent of the DEA # for Puerto Rican address records.",
            "data_type": "Varchar(100)"
        },
        {
            "actual_col_name": "Account_vod__c",
            "business_name": "Account",
            "description": "The Account field represents the Account that Address is related to.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Address_line_2_vod__c",
            "business_name": "Address line 2",
            "description": "Second line of street address. The first line is stored in the name field of the address record.",
            "data_type": "Varchar(100)"
        },
        {
            "actual_col_name": "Appt_Required_vod__c",
            "business_name": "Appt. Required",
            "description": "Check if an appointment is required for this address",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Best_Times_vod__c",
            "business_name": "Best Times",
            "description": "Used by the Office Best Times control to store the best time to visit this address.",
            "data_type": "Varchar(400)"
        },
        {
            "actual_col_name": "Billing_vod__c",
            "business_name": "Billing",
            "description": "Checkbox to determine if an Address serves as a billing address.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Brick_vod__c",
            "business_name": "Brick",
            "description": "The Brick that corresponds to the Address for use in International implementations.  The Brick value is utilized by the Territory Management process for alignments.",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "Business_vod__c",
            "business_name": "Business",
            "description": "Checkbox to determine if an Address serves as a business address.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "CDS_Expiration_Date_vod__c",
            "business_name": "CDS Expiration Date",
            "description": "The expiration date of the CDS registration number.",
            "data_type": "Date"
        },
        {
            "actual_col_name": "CDS_vod__c",
            "business_name": "CDS #",
            "description": "The CDS registration number for the corresponding state of the address.",
            "data_type": "Varchar(25)"
        },
        {
            "actual_col_name": "City_vod__c",
            "business_name": "City",
            "description": "City of the Address",
            "data_type": "Varchar(40)"
        },
        {
            "actual_col_name": "Comment_vod__c",
            "business_name": "Comment",
            "description": "Comments for this address.",
            "data_type": "Varchar(255)"
        },
        {
            "actual_col_name": "Controlled_Address_vod__c",
            "business_name": "Controlled Address",
            "description": "If true then this address is controlled by child account push.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Controlling_Address_vod__c",
            "business_name": "Controlling Address",
            "description": "Address that was used as a template to create the address by the child account push.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Country__c",
            "business_name": "Country",
            "description": "This field is only used by the Veeva Network integration and maps to sub_administrative area.",
            "data_type": "Varchar(50)"
        },
        {
            "actual_col_name": "Country_vod__c",
            "business_name": "Country",
            "description": "The Country field stores the Country of the Address record.  The picklist values should be the two digit code, such as US.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "CreatedById",
            "business_name": "Created By ID",
            "description": "Created By ID",
            "data_type": "Varchar(18)"
        },
        {
            "actual_col_name": "CreatedDate",
            "business_name": "Created Date",
            "description": "Created Date",
            "data_type": "Timestamp"
        },
        {
            "actual_col_name": "Customer_Master_Status_vod__c",
            "business_name": "Customer Master Status",
            "description": "Record ID",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "DEA_Address_vod__c",
            "business_name": "DEA Address?",
            "description": "Deleted",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "DEA_Expiration_Date_vod__c",
            "business_name": "DEA Expiration Date",
            "description": "DEA license expiration date.  The Expiration Date must be in the future if a Physician is to be allowed to receive Controlled Substance Samples.",
            "data_type": "Date"
        },
        {
            "actual_col_name": "DEA_License_Address_vod__c",
            "business_name": "DEA License Address",
            "description": "DEA License Address is the Address stored on the physical DEA License for the Account.  The DEA License Address field is compared to the Address fields for this specific record and is used in the DEA License validation for controlled substance sampling.",
            "data_type": "Varchar(1000)"
        },
        {
            "actual_col_name": "DEA_Schedule_vod__c",
            "business_name": "DEA Schedule",
            "description": "Comma delimited list of Schedules  of Drugs that a Physician is able to receive at this Address",
            "data_type": "Varchar(100)"
        },
        {
            "actual_col_name": "DEA_Status_vod__c",
            "business_name": "DEA Status",
            "description": "DEA Status for the Physician DEA License.  This field should be set to either Valid_vod or Invalid_vod.  Valid_vod will allow the Physician to receive Controlled Substances at this Address.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "DEA_vod__c",
            "business_name": "DEA #",
            "description": "DEA license number.  Determines if a Physician is able to receive Controlled Substance Samples at this Address.",
            "data_type": "Varchar(9)"
        },
        {
            "actual_col_name": "Entity_Reference_Id_vod__c",
            "business_name": "Entity Reference Id",
            "description": "This Veeva field ensures proper synchronization even after account merges or territory realignments.",
            "data_type": "Varchar(20)"
        },
        {
            "actual_col_name": "External_ID_vod__c",
            "business_name": "External ID",
            "description": "External ID is only used for data loading.",
            "data_type": "Varchar(120)"
        },
        {
            "actual_col_name": "Fax_2_vod__c",
            "business_name": "Fax 2",
            "description": "Alternate fax number for the Account at this Address.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Fax_vod__c",
            "business_name": "Fax",
            "description": "Fax number for the Account at this Address.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Home_vod__c",
            "business_name": "Home",
            "description": "Checkbox that determines if the Address serves as a home address.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Id",
            "business_name": "Record ID",
            "description": "Record ID",
            "data_type": "Varchar(18)"
        },
        {
            "actual_col_name": "Inactive_vod__c",
            "business_name": "Inactive",
            "description": "Deleted",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Include_in_Territory_Assignment_vod__c",
            "business_name": "Include in Territory Assignment",
            "description": "When this is checked the address will be included in the territory assignment process even if it is not a primary address.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "IsDeleted",
            "business_name": "Deleted",
            "description": "Deleted",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "IsLocked",
            "business_name": "Is Locked",
            "description": "Is Locked",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "LastModifiedById",
            "business_name": "Last Modified By ID",
            "description": "Last Modified By ID",
            "data_type": "Varchar(18)"
        },
        {
            "actual_col_name": "LastModifiedDate",
            "business_name": "Last Modified Date",
            "description": "Last Modified Date",
            "data_type": "Timestamp"
        },
        {
            "actual_col_name": "Latitude_vod__c",
            "business_name": "Latitude",
            "description": "Latitude of this address. Used for mapping. This value is set to blank via trigger whenever a relevant address field changes. If zero, the latitude is written from the MyMaps functionality when the user maps this address. This field can be pre-populated from an external program.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "License_Expiration_Date_vod__c",
            "business_name": "License Expiration Date",
            "description": "State License Expiration Date.  The State License Expiration Date is maintained across Addresses for an Account for the same State by the Address_trigger_vod trigger on the Address object.",
            "data_type": "Date"
        },
        {
            "actual_col_name": "License_Status_vod__c",
            "business_name": "License Status",
            "description": "State License Status.  The State License Status is maintained across Addresses for an Account for the same State by the Address_trigger_vod trigger on the Address object.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "License_Valid_To_Sample_vod__c",
            "business_name": "License Valid To Sample",
            "description": "License Valid to Sample formula defines if a License is Valid or Invalid and determines if an Account can receive and sign for Samples in the VBioPharma application.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "License_vod__c",
            "business_name": "License #",
            "description": "State License Number for the corresponding State of the Address.  The State License Number is maintained across Addresses for an Account for the same State by the Address_trigger_vod trigger on the Address object.",
            "data_type": "Varchar(25)"
        },
        {
            "actual_col_name": "Lock_vod__c",
            "business_name": "Lock",
            "description": "If the Locked field is set to true, only users with the Modify All Data privilege can edit the Address Line 1(Name) field or delete the record. If the Locked field is null, false or absent, then the trigger should do nothing. The Locked field would be best left off of Rep Maintained layouts.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Longitude_vod__c",
            "business_name": "Longitude",
            "description": "Longitude of this address. Used for mapping. This value is set to blank via trigger whenever a relevant address field changes. If zero, the longitude is written from the MyMaps functionality when the user maps this address. This field can be pre populated from an external program.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "Mailing_vod__c",
            "business_name": "Mailing",
            "description": "Checkbox that determines if an Address serves as a mailing address.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Map_vod__c",
            "business_name": "Map",
            "description": "Displays a hyperlink to view the address in Google maps.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Master_Align_Id_vod__c",
            "business_name": "Master Align Id",
            "description": "Globally unique identifier for this object. This Id is managed by Align",
            "data_type": "Varchar(36)"
        },
        {
            "actual_col_name": "MayEdit",
            "business_name": "May Edit",
            "description": "May Edit",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Mobile_ID_vod__c",
            "business_name": "Mobile ID",
            "description": "System field used by mobile products to aid synchronization.",
            "data_type": "Varchar(100)"
        },
        {
            "actual_col_name": "NET_address_ordinal__c",
            "business_name": "Network Address Rank",
            "description": "Use for Network integration only.",
            "data_type": "Integer"
        },
        {
            "actual_col_name": "NET_address_status__c",
            "business_name": "Network Status",
            "description": "Network Status",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Name",
            "business_name": "Street Address",
            "description": "Street Address",
            "data_type": "Varchar(80)"
        },
        {
            "actual_col_name": "Network_External_Id__c",
            "business_name": "Network External Id",
            "description": "Network External Id",
            "data_type": "Varchar(100)"
        },
        {
            "actual_col_name": "Network_Sample_Eligibility_vod__c",
            "business_name": "Sample Eligibility",
            "description": "Contains the eligibility of the HCP associated with this address to receive samples, as described in the customer master data",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "No_Address_Copy_vod__c",
            "business_name": "No Address Copy",
            "description": "Override automated push of address.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Office_Notes_vod__c",
            "business_name": "Office Notes",
            "description": "Area to keep notes for this address.",
            "data_type": "Varchar(32000)"
        },
        {
            "actual_col_name": "Phone_2_vod__c",
            "business_name": "Phone 2",
            "description": "Alternate phone number for the Account for this Address.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Phone_vod__c",
            "business_name": "Phone 1",
            "description": "Primary phone number for the Account for this Address.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Primary_vod__c",
            "business_name": "Primary",
            "description": "Address serves as the primary address for the associated account. A trigger insures that there is only one primary address.",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "RecordTypeId",
            "business_name": "Record Type ID",
            "description": "Record Type ID",
            "data_type": "Varchar(18)"
        },
        {
            "actual_col_name": "Sample_Send_Status_vod__c",
            "business_name": "Sample Send Status",
            "description": "Indicates the availability of an address record for selection in the Ship To Address field on the call report.\nPending_vod - Newly created address record that is not able to be shipped samples until address is verified\nValid_vod - Address record that is able to be shipped samples\nInvalid_vod - Address record that is not able to be shipped samples",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Sample_Status_vod__c",
            "business_name": "Sample Status",
            "description": "Sample Status defines the status of the State License and controls the Sample Status.  Sample Status of Valid represents a Green indicator.  Sample Status of Pending represents a Yellow Indicator.  Sample Status of Invalid represents a Red Indicator.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Shipping_vod__c",
            "business_name": "Shipping",
            "description": "Checkbox that determines if an Address serves as a shipping address",
            "data_type": "Char(4)"
        },
        {
            "actual_col_name": "Source_vod__c",
            "business_name": "Source",
            "description": "Source of the Address information.  The Source field should be set if a System Integration is utilized to create and maintain Addresses.",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "Staff_notes_vod__c",
            "business_name": "Staff notes",
            "description": "Area to keep notes about the staff at this address.",
            "data_type": "Varchar(32000)"
        },
        {
            "actual_col_name": "State_vod__c",
            "business_name": "State",
            "description": "State of the Address",
            "data_type": "Varchar(256)"
        },
        {
            "actual_col_name": "SystemModstamp",
            "business_name": "System Modstamp",
            "description": "System Modstamp",
            "data_type": "Timestamp"
        },
        {
            "actual_col_name": "Zip_4_vod__c",
            "business_name": "Zip + 4",
            "description": "ZIP + 4 of the Address",
            "data_type": "Varchar(4)"
        },
        {
            "actual_col_name": "Zip_vod__c",
            "business_name": "Zip",
            "description": "Zip code of the Address",
            "data_type": "Varchar(20)"
        }
    ]
}